import json
import time
from getpass import getpass

import requests
from pathlib import Path

from onboard_users import *

apiURLPort = os.environ['API_URL_PORT']
branch = os.environ['BRANCH']
cognito_user_pool_id = os.environ['COGNITO_USER_POOL_ID']
cognito_client_id = os.environ['COGNITO_CLIENT_ID']

customers_end_point = f"https://{apiURLPort}/payments-poc-{branch}-core/ui/customers"
processors_end_point = f"https://{apiURLPort}/payments-poc-{branch}-core/ui/processors"
services_end_point = f"https://{apiURLPort}/payments-poc-{branch}-core/ui/services"

create_inbound = None
create_outbound = None


def create_customer():
    print("\n** CREATING CUSTOMER **")
    global customer_name, r
    customer_name = input("Name of customer: ")
    print("Calling API to create customer " + customer_name)
    # r = requests.get(customers_end_point, headers=auth_header)
    # json_data = json.loads(r.text)
    # number_customers = str(len(json_data))
    # customer_id = "customer" + number_customers
    customer = {
        "createdBy": username,
        "id": customer_name,
        "name": customer_name
    }
    r = requests.post(customers_end_point, json=customer, headers=auth_header)
    if r.status_code == 200:
        print("\tCreated customer with id " + customer_name)
    else:
        print("Error creating customer: Details: \n" + r.json()["message"])


def check_sftp_user(customer_name):
    does_customer_exist = check_sftp_user_exists(customer_name)
    if does_customer_exist:
        response = input(
            f"The {customer_name} already exists on the SFTP site. Do you wish to delete it and regenerate keys? (y/n) ")
        if response == "y":
            delete_sftp_user(customer_name)
        else:
            print("Exiting...")
            exit()


def update_service():
    global service_name, folder_name, service

    print("\n** UPDATING SERVICE **")

    payment_rules_file = ""

    service_name = input("Name of service: ")
    r = requests.get(f"{services_end_point}/{service_name}", headers=auth_header)
    if r.status_code != 200:
        print("\tCould not find "+service_name)
        exit()

    json_data = json.loads(r.text)

    file_format = json_data['fileFormat']
    print("reading" + file_format)

    if file_format.upper() == "ILF":
        payment_rules_file = "payment_rules_ilf.txt"
    elif file_format.upper() == "CPS":
        payment_rules_file = "payment_rules_ssc.txt"

    rules = read_payment_rules_from_file(payment_rules_file)
    print(f"\tRead {len(rules)} rules from file {payment_rules_file}")

    json_data["paymentValidationRules"] = rules
    r = requests.post(services_end_point, json=json_data, headers=auth_header)
    if r.status_code == 200:
        print("\tUpdated service with name "+service_name)
    else:
        print("Error updating service: Details: \n" + r.json()["message"])


def create_service():
    global service_name, folder_name, service

    print("\n** CREATING SERVICE **")

    ledger = input("Ledger Code: ")

    metadata = {}

    if len(ledger) > 0:
        metadata = {
            "gl_code": ledger
        }

    file_format = ""
    rules = []
    payment_rules_file = ""

    service_name = input("Name of service: ")
    folder_name = service_name

    if create_outbound:
        service_type = input("Type of service (ILF/SSC): ").upper()
        if service_type == "ILF":
            payment_rules_file = "payment_rules_ilf.txt"
            file_format = "ILF"
        elif service_type == "SSC":
            payment_rules_file = "payment_rules_ssc.txt"
            file_format = "CPS"
        else:
            print("Invalid service type.")
            exit()

        payment_rules_file_path = Path(payment_rules_file)
        if not payment_rules_file_path.is_file():
            print("Could not read payment file")
            exit()

        check_sftp_user(service_name)
        rules = read_payment_rules_from_file(payment_rules_file)
        print(f"\tRead {len(rules)} rules from file {payment_rules_file}")

    service = {
        "folder": folder_name,
        "name": service_name,
        "fileFormat": file_format,
        "id": service_name,
        "customerId": customer_name,
        "paymentValidationRules": rules,
        "metadata" : metadata
    }
    r = requests.post(services_end_point, json=service, headers=auth_header)
    if r.status_code == 200:
        print("\tCreated service with name "+service_name)
    else:
        print("Error creating service: Details: \n" + r.json()["message"])


def create_payment_router():
    global r
    print("\n** CREATING PAYMENT ROUTER USER **")
    router_name = "test_router"
    payment_router = {
        "name": "test_router",
        "channels": [
            {
                "paymentChannel": "FasterPayments",
                "costExpression": "targetAmount.multiply(1)"
            }
        ]
    }
    r = requests.post(processors_end_point, json=payment_router, headers=auth_header)
    r.json()
    if r.status_code == 200:
        print("Created payment router: " + router_name)
    else:
        print("Error creating router: Details: \n" + r.json()["message"])


def read_payment_rules_from_file(file_name):
    rules = []
    f = open(file_name, "r")
    for index, line in enumerate(f):
        items = line.split(":")
        payment_rule = {
            "createdBy": "dev_user",
            "messageTemplate": items[0],
            "name": "rule" + str(index),
            "precedence": 0,
            "rule": items[1]
        }
        rules.append(payment_rule)
    return rules


def get_cognito_token():
    global cognito_token, auth_header, username
    username = input("Admin username:")
    password = getpass("Password: ")
    print()
    response = cognito_idp_client.admin_initiate_auth(
        AuthFlow="ADMIN_NO_SRP_AUTH",
        AuthParameters={
            "USERNAME": username,
            "PASSWORD": password
        },
        ClientId=cognito_client_id,
        UserPoolId=cognito_user_pool_id
    )
    cognito_token = response['AuthenticationResult']['IdToken']
    auth_header = {"Authorization": "Bearer " + cognito_token}


def create_cognito_group(service_name):
    group_name = f"{branch}_service:{service_name}:Write"
    groups = cognito_idp_client.list_groups(
        UserPoolId=cognito_user_pool_id,
        Limit=60
    )['Groups']
    filtered_groups = list(filter(lambda g: g['GroupName'] == group_name, groups))
    if len(filtered_groups) > 0:
        print(f"Group {group_name} already exists")
    else:
        print(f"Creating group {group_name}")
        cognito_idp_client.create_group(
            GroupName=group_name,
            UserPoolId=cognito_user_pool_id
        )
    return group_name


def get_user_pool_client(client_name):
    user_pools_clients = cognito_idp_client.list_user_pool_clients(
        UserPoolId=cognito_user_pool_id,
    )['UserPoolClients']
    filtered_clients = list(filter(lambda u: u['ClientName'] == client_name, user_pools_clients))
    if len(filtered_clients) > 0:
        return filtered_clients[0]
    else:
        return None


def create_cognito_app_client(service_name, resource_servers):

    for resource_server in resource_servers:

        desc = cognito_idp_client.describe_resource_server(
            UserPoolId=cognito_user_pool_id,
            Identifier=resource_server,
        )

        scopes = []
        if 'Scopes' in desc["ResourceServer"]:
            scopes = desc["ResourceServer"]["Scopes"]
        scopes.append({
                    'ScopeName': service_name,
                    'ScopeDescription': service_name + ' ' + resource_server
                })

        cognito_idp_client.update_resource_server(
            UserPoolId=cognito_user_pool_id,
            Identifier=resource_server,
            Name=resource_server,
            Scopes=scopes
        )

    allowed_oauth_scopes = list(map(lambda rs: rs + '/' + service_name, resource_servers))

    user_pool_client = get_user_pool_client(service_name + '-site')

    # Wait for cognito update
    time.sleep(5)

    if user_pool_client is None:
        user_pool_client = cognito_idp_client.create_user_pool_client(
            UserPoolId=cognito_user_pool_id,
            ClientName=service_name+"-site",
            GenerateSecret=True,
            AllowedOAuthFlows=['client_credentials'],
            AllowedOAuthScopes=allowed_oauth_scopes,
            AllowedOAuthFlowsUserPoolClient=True
        )['UserPoolClient']
    else:
        if 'AllowedOAuthFlows' not in user_pool_client:
            user_pool_client['AllowedOAuthFlows'] = ['client_credentials']
        elif 'client_credentials' not in user_pool_client['AllowedOAuthFlows']:
            user_pool_client['AllowedOAuthFlows'].append('client_credentials')
        if 'AllowedOAuthScopes' not in user_pool_client:
            user_pool_client['AllowedOAuthScopes'] = allowed_oauth_scopes
        else:
            user_pool_client['AllowedOAuthScopes'].extend(allowed_oauth_scopes)
        user_pool_client = cognito_idp_client.update_user_pool_client(**user_pool_client)['UserPoolClient']

    client_id = user_pool_client["ClientId"]
    client_secret = user_pool_client["ClientSecret"]

    print("\n** GENERATING APP CLIENT ** \n")
    print(" ** CLIENT ID **")
    print(client_id+"\n")
    print(" ** CLIENT SECRET **")
    print(client_secret+"\n")


def add_users_to_group(group_name):
    user_list = input("Users to add (as comma separated list):")
    for user in user_list.split(","):
        user_name = user.strip().lower()
        if not cognito_user_exists(user_name, cognito_user_pool_id):
            print(f"User {user_name} does not exist.")
        else:
            cognito_idp_client.admin_add_user_to_group(
                UserPoolId=cognito_user_pool_id,
                Username=user_name,
                GroupName=group_name
            )
            print(f"User {user_name} added to group {group_name}")


def should_create_service_user_input():

    while True:
        response = input("\nCreate a service? (y/n) ")
        if response.lower() == "y" or response.lower() == "yes":
            return True
        if response.lower() == "n" or response.lower() == "no":
            return False
        print("Invalid response entered. Please answer with y/n.")


def should_create_inbound_outbound():

    global create_outbound
    while create_outbound is None:
        response = input("\nSupport outbound payments? (y/n) ")
        if response.lower() == "y" or response.lower() == "yes":
            create_outbound = True
        elif response.lower() == "n" or response.lower() == "no":
            create_outbound = False
        else:
            print("Invalid response entered. Please answer with y/n.")

    global create_inbound
    while create_inbound is None:
        response = input("\nSupport inbound payments? (y/n) ")
        if response.lower() == "y" or response.lower() == "yes":
            create_inbound = True
        elif response.lower() == "n" or response.lower() == "no":
            create_inbound = False
        else:
            print("Invalid response entered. Please answer with y/n.")


def add_oauth_to_api_gateway(service_name, resource_servers):

    rest_api_id = get_resource_id_from_stack(stack_name, 'PaymentsPOCRestAPI')
    api_gateway_resource_id = get_resource_id_from_stack(stack_name, 'PaymentsPOCAPIProxyResource')
    patch_operations = list(map(
        lambda rs: {
            'op': 'add',
            'path': '/authorizationScopes',
            'value': rs + '/' + service_name
        },
        resource_servers))

    apigateway_client.update_method(
        restApiId=rest_api_id,
        resourceId=api_gateway_resource_id,
        httpMethod='ANY',
        patchOperations=patch_operations
    )


def run_onboarding():
    get_cognito_token()
    create_customer()
    while should_create_service_user_input():
        should_create_inbound_outbound()
        create_service()

        if create_outbound:
            print("\n** CREATING SFTP USER **")
            create_sftp_user_with_key(service_name, folder_name)
            os.putenv('SFTP_USER', customer_name)

        print("\n** CREATING COGNITO GROUP **")
        group_name = create_cognito_group(service_name)
        add_users_to_group(group_name)

        resource_servers = ['service']
        if create_inbound:
            resource_servers.append('inbound-payments')

        create_cognito_app_client(service_name, resource_servers)
        add_oauth_to_api_gateway(service_name, resource_servers)
