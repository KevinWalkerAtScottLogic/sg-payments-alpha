import time
from payments_poc_stack_env import *
from payment_poc_common import *

# Stack Details
template_file_path = "aws-stack-template.json"
db_template_file_path = "aws-db-stack-template.json"
availability_zones = os.environ['AVAILABILITY_ZONES']

# How long to wait for stack creation to end (in seconds)
create_wait_time = 120 * 60

# How often to test for stack creation ending (in seconds)
create_wait_interval = 60


def stack_create_in_progress(stack_name):
    return stack_is_in_state(
        stack_name,
        ['CREATE_IN_PROGRESS']
    )


def stack_create_complete(stack_name):
    return stack_is_in_state(
        stack_name,
        ['CREATE_COMPLETE']
    )


def create_stack():
    template = open(template_file_path, "r").read()
    result = cf_client.create_stack(
        StackName=stack_name,
        TemplateBody=template,
        Parameters=[
            {
                'ParameterKey': 'VpcAvailabilityZones',
                'ParameterValue': availability_zones
            },
            {
                'ParameterKey': 'DomainName',
                'ParameterValue': domain_name
            },
            {
                'ParameterKey': 'PaymentsPOCDomainCertificateArn',
                'ParameterValue': domain_certificate_arn
            },
            {
                'ParameterKey': 'PaymentsPOCCognitoUserPoolId',
                'ParameterValue': cognito_user_pool_id
            },
            {
                'ParameterKey': 'CloudFrontDomainCertainArn',
                'ParameterValue': cloudfront_domain_certificate_arn
            },
        ]
    )
    return result.get("StackId")


def create_db_stack(db_stack_name):
    template = open(db_template_file_path, "r").read()
    result = cf_client.create_stack(
        StackName=db_stack_name,
        TemplateBody=template,
        Parameters=[
            {
                'ParameterKey': 'VpcAvailabilityZones',
                'ParameterValue': availability_zones
            },
            {
                'ParameterKey': 'RDSMasterDBUser',
                'ParameterValue': rds_master_user
            },
            {
                'ParameterKey': 'RDSMasterUserPassword',
                'ParameterValue': rds_master_user_password
            },
            {
                'ParameterKey': 'RDSMasterDBName',
                'ParameterValue': rds_master_db_name
            },
        ]
    )
    return result.get("StackId")


def create_succeeded(stack_name):
    elapsed_time = 0
    while stack_create_in_progress(stack_name) and elapsed_time < create_wait_time:
        print(".", end='', flush=True)
        time.sleep(create_wait_interval)
        elapsed_time += create_wait_interval
    print()
    return stack_create_complete(stack_name)


def get_load_balancer_description():

    response = elb_client.describe_load_balancers(
        Names=[load_balancer_name]
    )

    if len(response['LoadBalancers']) == 0:
        raise Exception(f'Could not find load balancer for {load_balancer_name}')

    return response['LoadBalancers'][0]


def create_msk_cluster():

    load_balancer_description = get_load_balancer_description()
    subnet_ids = list(map(lambda az: az["SubnetId"], load_balancer_description['AvailabilityZones']))
    msk_security_group = get_resource_id_from_stack(stack_name, 'MskSecurityGroup')

    response = kafka_client.create_cluster(
        BrokerNodeGroupInfo={
            'BrokerAZDistribution': 'DEFAULT',
            'ClientSubnets': subnet_ids,
            'InstanceType': 'kafka.m5.large',
            'SecurityGroups': [msk_security_group],
            'StorageInfo': {
                'EbsStorageInfo': {
                    'VolumeSize': msk_volume_size_gb
                }
            }
        },
        ClusterName=kafka_cluster_name,
        EncryptionInfo={
            'EncryptionAtRest': {
                'DataVolumeKMSKeyId': 'alias/aws/kafka'
            }
        },
        EnhancedMonitoring='DEFAULT',
        KafkaVersion='2.1.0',
        NumberOfBrokerNodes=3
    )

    cluster_arn = response['ClusterArn']

    print(f"Started creation of Kafka cluster {cluster_arn}")

    elapsed_time = 0
    while get_kafka_cluster_state(kafka_cluster_name) == 'CREATING' and elapsed_time < create_wait_time:
        print(".", end='', flush=True)
        time.sleep(create_wait_interval)
        elapsed_time += create_wait_interval
    print()
    state = get_kafka_cluster_state(kafka_cluster_name)
    if state == 'FAILED' or state == 'Not found':
        raise Exception(f"Failed to create Kafka cluster {cluster_arn}")


def setup_db_security_group(ecs_security_group_logical_name, db_security_group_logical_name):

    vpc_security_group_id = get_resource_id_from_stack(stack_name, ecs_security_group_logical_name)
    db_security_group_id = get_resource_id_from_stack(db_stack_name, db_security_group_logical_name)
    peer_vpc_id = get_resource_id_from_stack(stack_name, 'Vpc')
    db_vpc_id = get_resource_id_from_stack(db_stack_name, 'Vpc')

    peering_connection_id = ec2_client.create_vpc_peering_connection(
        PeerVpcId=peer_vpc_id,
        VpcId=db_vpc_id,
    )['VpcPeeringConnection']['VpcPeeringConnectionId']

    ec2_client.accept_vpc_peering_connection(
        VpcPeeringConnectionId=peering_connection_id
    )

    ec2_client.authorize_security_group_ingress(
        GroupId=db_security_group_id,
        IpPermissions=[
            {
                "IpProtocol": "tcp",
                "FromPort": 1,
                "ToPort": 65535,
                "UserIdGroupPairs": [
                    {
                        "GroupId": vpc_security_group_id
                    }
                ]
            }
        ]
    )


def create_rest_of_infrastructure():

    print("Creating Kafka cluster")
    create_msk_cluster()
    print("Kafka cluster created OK")

    print("Checking if DB stack already exists")
    if not stack_exists(db_stack_name):
        print("Creating database stack")
        create_db_stack(db_stack_name)
        print("Waiting for DB stack creation to complete", flush=True)
        if not create_succeeded(db_stack_name):
            print("DB Stack creation failed")
            exit()

    print("Enabling access to DB")
    setup_db_security_group("EcsSecurityGroup", "DBSecurityGroup")

    print("Creating SFTP server")
    transfer_client.create_server(
        IdentityProviderType='SERVICE_MANAGED',
        Tags=[
            {
                'Key': 'Name',
                'Value': f"{stack_name}-ftp-server"
            },
            {
                'Key': 'Stack',
                'Value': stack_name
            },
        ]
    )
    print("SFTP server created OK")

    print("Adding API Gateway integrations")
    add_api_gateway_integration(
        stack_name,
        'PaymentsPOCRestAPI',
        'PaymentsPOCAPIProxyResource',
        load_balancer_name,
        {
            'integration.request.path.proxy': 'method.request.path.proxy',
            'integration.request.header.Authorization': 'method.request.header.Authorization'
        }
    )
    add_api_gateway_oauth_scopes(
        stack_name,
        'PaymentsPOCRestAPI',
        'PaymentsPOCAPIProxyResource',
        'inbound-payments'
    )
    add_api_gateway_integration(
        stack_name,
        'PaymentsPOCRestAPI',
        'PaymentsPOCUIProxyResource',
        load_balancer_name,
        {
            'integration.request.path.proxy': 'method.request.path.proxy',
            'integration.request.header.Authorization': 'method.request.header.Authorization'
        }
    )
    add_api_gateway_integration(
        stack_name,
        'PaymentsPOCRestAPI',
        'PaymentsPOCCardPaymentProxyResource',
        load_balancer_name,
        {
            'integration.request.path.proxy': 'method.request.path.proxy'
        },
        "/cardpayments"
    )
    add_api_gateway_integration(
        stack_name,
        'PaymentsPOCRestAPI',
        'PaymentsPOCStaticProxyResource',
        load_balancer_name,
        {
            'integration.request.path.proxy': 'method.request.path.proxy'
        },
        "/resources"
    )
    add_api_gateway_integration(
        stack_name,
        'KafkaRestAPI',
        'KafkaRestProxyResource',
        load_balancer_name,
        {
            'integration.request.path.proxy': 'method.request.path.proxy'
        }
    )
    print("API Gateway integrations added OK")


def create_full_infrastructure():
    if stack_exists(stack_name):
        print(f"Stack {stack_name} already exists")
    else:
        print(f"Creating stack {stack_name}")
        print(f"Stack id = {create_stack()}")
        print("Waiting for stack creation to complete", flush=True)
        if create_succeeded(stack_name):
            print("Stack creation was successful. Creating rest of infrastructure.")
            create_rest_of_infrastructure()
        else:
            print("Stack creation failed")


create_full_infrastructure()
