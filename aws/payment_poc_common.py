import hashlib
import os
import boto3

# Credentials & Region
access_key = os.environ['AWS_ACCESS_KEY_ID']
secret_key = os.environ['AWS_SECRET_ACCESS_KEY']
region = os.environ['AWS_REGION']
accountId = os.environ['AWS_ACCOUNT_ID']

cognito_user_pool_id = os.environ['COGNITO_USER_POOL_ID']

def create_boto3_client(service_name):
    return boto3.client(
        service_name=service_name,
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        region_name=region
    )


def create_boto3_resource(service_name):
    return boto3.resource(
        service_name=service_name,
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        region_name=region
    )


# AWS API Gateway client (V1 for REST)
apigateway_client = create_boto3_client('apigateway')

# AWS CloudFormation client
cf_client = create_boto3_client('cloudformation')

# AWS CloudFormation resource
cf_resource = create_boto3_resource('cloudformation')

# AWS Cognito Identity Provider client
cognito_idp_client = create_boto3_client('cognito-idp')

# AWS EC2 client
ec2_client = create_boto3_client('ec2')

# AWS ECS client
ecs_client = create_boto3_client('ecs')

# AWS Elastic load balancer client
elb_client = create_boto3_client('elbv2')

# AWS IAM client
iam_client = create_boto3_client('iam')

# AWS Kafka (MSK) client
kafka_client = create_boto3_client('kafka')

# AWS logs client
logs_client = create_boto3_client('logs')

# AWS RDS client
rds_client = create_boto3_client('rds')

# AWS Route 53 client
route53_client = create_boto3_client('route53')

# AWS S3 client
s3_client = create_boto3_client('s3')

# AWS S3 resource
s3_resource = create_boto3_resource('s3')

#AWS SNS client
sns_client = create_boto3_client('sns')

# AWS SQS client
sqs_client = create_boto3_client('sqs')

# AWS Systems Manager client
ssm_client = create_boto3_client('ssm')

# AWS Transfer for SFTP client
transfer_client = create_boto3_client('transfer')


def stack_is_in_state(stack_name, state_list):
    stacks = cf_client.list_stacks(
        StackStatusFilter=state_list
    )["StackSummaries"]
    filtered_stacks = list(filter(lambda s: s['StackName'] == stack_name, stacks))
    return len(filtered_stacks) > 0


def stack_exists(stack_name):
    return stack_is_in_state(
        stack_name,
        ['CREATE_IN_PROGRESS', 'CREATE_FAILED', 'CREATE_COMPLETE', 'ROLLBACK_IN_PROGRESS',
         'ROLLBACK_FAILED', 'ROLLBACK_COMPLETE', 'DELETE_IN_PROGRESS', 'DELETE_FAILED',
         'UPDATE_IN_PROGRESS', 'UPDATE_COMPLETE_CLEANUP_IN_PROGRESS',
         'UPDATE_COMPLETE', 'UPDATE_ROLLBACK_IN_PROGRESS', 'UPDATE_ROLLBACK_FAILED',
         'UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS', 'UPDATE_ROLLBACK_COMPLETE',
         'REVIEW_IN_PROGRESS']
    )


def get_resource_id_from_stack(stack_name, logical_id):
    stack_resource = cf_resource.StackResource(stack_name, logical_id)
    return stack_resource.physical_resource_id


def kafka_cluster_exists(kafka_cluster_name):

    clusters = kafka_client.list_clusters(
        ClusterNameFilter=kafka_cluster_name
    )['ClusterInfoList']
    filtered_clusters = list(filter(lambda c: c['ClusterName'] == kafka_cluster_name, clusters))
    return len(filtered_clusters) > 0


def get_kafka_cluster_state(kafka_cluster_name):
    clusters = kafka_client.list_clusters(
        ClusterNameFilter=kafka_cluster_name
    )['ClusterInfoList']
    filtered_clusters = list(filter(lambda c: c['ClusterName'] == kafka_cluster_name, clusters))
    if len(filtered_clusters) > 0:
        return filtered_clusters[0]['State']
    else:
        return 'Not found'


def get_service_status(cluster_name, service_name):
    response = ecs_client.describe_services(
        cluster=cluster_name,
        services=[service_name]
    )
    if len(response['services']) == 0:
        return 'Not found'
    else:
        return response['services'][0]['status']


def get_running_count(cluster_name, service_name):
    response = ecs_client.describe_services(
        cluster=cluster_name,
        services=[service_name]
    )
    if len(response['services']) == 0:
        return 'Not found'
    else:
        return response['services'][0]['runningCount']


def get_kafka_cluster(kafka_cluster_name):
    clusters = kafka_client.list_clusters(
        ClusterNameFilter=kafka_cluster_name
    )['ClusterInfoList']
    if len(clusters) == 0:
        raise Exception(f"Missing kafka cluster {kafka_cluster_name}")
    return clusters[0]


def get_kafka_brokers(kafka_cluster_name):

    cluster = get_kafka_cluster(kafka_cluster_name)
    brokers = kafka_client.get_bootstrap_brokers(
        ClusterArn=cluster['ClusterArn']
    )
    return brokers['BootstrapBrokerString']


def get_kafka_zookeepers(kafka_cluster_name):

    cluster = get_kafka_cluster(kafka_cluster_name)
    return cluster['ZookeeperConnectString']


def get_subnet_info(vpc_id):
    response = ec2_client.describe_subnets(
        Filters=[
            {
                'Name': 'vpc-id',
                'Values': [vpc_id]
            }
        ]
    )
    if len(response['Subnets']) == 0:
        raise Exception(f"'Could not find subnets for {vpc_id}")

    subnet_array = []
    for i in range(len(response['Subnets'])):
        subnet_id = response['Subnets'][i]['SubnetId']
        subnet_array.append(subnet_id)

    return subnet_array


def does_listener_exist(load_balancer_arn, listener_port):
    response = elb_client.describe_listeners(
        LoadBalancerArn=load_balancer_arn
    )
    for listener in response['Listeners']:
        # print("looking at " + pprint.pprint(listener))
        if listener['Port'] == listener_port:
            print(f"Found port {listener_port}")
            return True

    return False


def create_listener(load_balancer_arn, target_arn, listener_port):
    print(f"Creating listener for port {listener_port} with target group {target_arn}")
    elb_client.create_listener(
        LoadBalancerArn=load_balancer_arn,
        Protocol='HTTP',
        Port=listener_port,
        DefaultActions=[
            {
                'Type': 'forward',
                'TargetGroupArn': target_arn
            }
        ]
    )


def open_load_balancer_port(port, security_group_id):
    print(f"Opening port {port} on load balancer")
    ec2_client.authorize_security_group_ingress(
        CidrIp="0.0.0.0/0",
        FromPort=port,
        GroupId=security_group_id,
        IpProtocol="tcp",
        ToPort=port
    )


def does_open_port_exist(port, security_group_id):
    security_groups = ec2_client.describe_security_groups(
        GroupIds=[security_group_id]
    )['SecurityGroups']
    if len(security_groups) == 0:
        raise Exception(f"No such security group: {security_group_id}")
    ip_permissions = security_groups[0]['IpPermissions']
    matching_ports = list(filter(lambda p: p['FromPort'] == port, ip_permissions))
    return len(matching_ports) > 0


def create_log_group_if_not_exists(name):
    if not check_if_log_group_exists(name):
        create_log_group(name)


def check_if_log_group_exists(name):

    log_group_name = '/ecs/' + name
    print(f"Looking for log group {log_group_name}")

    response = logs_client.describe_log_groups(
        logGroupNamePrefix=log_group_name
    )

    log_groups = response['logGroups']

    if len(log_groups) == 0:
        print(f"Log group {log_group_name} not found")
        return False

    print(f"Log group {log_group_name} exists")
    return True


def create_log_group(name):

    log_group_name = '/ecs/' + name
    print(f"Creating log group {log_group_name}")
    logs_client.create_log_group(
        logGroupName=log_group_name
    )
    print(f"Created log group {log_group_name}")


def has_tag(tag_list, tag_key, tag_value):
    for tag in tag_list:
        if tag['Key'] == tag_key and tag['Value'] == tag_value:
            return True
    return False


def get_sftp_server_id(stack_name):
    servers = transfer_client.list_servers()["Servers"]
    for server in servers:
        tags = transfer_client.list_tags_for_resource(
            Arn=server['Arn']
        )
        if has_tag(tags['Tags'], 'Stack', stack_name):
            return server['ServerId']
    return None


def get_port(branch_name):

    print(branch_name)
    branch_name_utf = branch_name.encode('utf-8')
    port_number = int(hashlib.sha1(branch_name_utf).hexdigest(), 16) % 10000
    return port_number


def get_load_balancer(lb_name):
    response = elb_client.describe_load_balancers(
        Names=[lb_name]
    )
    if len(response['LoadBalancers']) == 0:
        raise Exception(f'Count not find load balancer for {lb_name}')
    return response['LoadBalancers'][0]


def add_api_gateway_integration(stack_name, rest_api_logical_name, resource_logical_name, lb_name, request_parameters, base_path = ""):

    rest_api_id = get_resource_id_from_stack(stack_name, rest_api_logical_name)
    resource_id = get_resource_id_from_stack(stack_name, resource_logical_name)
    load_balancer = get_load_balancer(lb_name)

    uri = "http://" + load_balancer['DNSName'] + ":${stageVariables.port}" + base_path + "/{proxy}"

    apigateway_client.put_integration(
        restApiId=rest_api_id,
        resourceId=resource_id,
        httpMethod='ANY',
        integrationHttpMethod='ANY',
        type='HTTP_PROXY',
        uri=uri,
        connectionType='INTERNET',
        passthroughBehavior='WHEN_NO_MATCH',
        requestParameters=request_parameters
    )


def add_api_gateway_deployment(stack_name, rest_api_logical_name):

    rest_api_id = get_resource_id_from_stack(stack_name, rest_api_logical_name)

    response = apigateway_client.create_deployment(
        restApiId=rest_api_id,
        stageName=""  # This is necessary, honest.
    )
    return response['id']


def add_api_gateway_stage(stack_name, rest_api_logical_name, stage_name, port):

    print(f"Creating deployment for {rest_api_logical_name}")
    deployment_id = add_api_gateway_deployment(stack_name, rest_api_logical_name)
    rest_api_id = get_resource_id_from_stack(stack_name, rest_api_logical_name)

    print(f"Creating stage \"{stage_name}\" for {rest_api_logical_name}")
    apigateway_client.create_stage(
        restApiId=rest_api_id,
        stageName=stage_name,
        deploymentId=deployment_id,
        variables={
            'port': str(port)
        }
    )


def get_api_gateway_stage(stack_name, rest_api_logical_name, stage_name):

    rest_api_id = get_resource_id_from_stack(stack_name, rest_api_logical_name)

    stages = apigateway_client.get_stages(
        restApiId=rest_api_id
    )['item']

    stage = list(filter(lambda s: s['stageName'] == stage_name, stages))
    if len(stage) > 0:
        return stage[0]
    else:
        return None


def cognito_user_exists(user_name, cognito_user_pool_id):
    users = cognito_idp_client.list_users(
        UserPoolId=cognito_user_pool_id
    )['Users']
    filtered_users = list(filter(lambda u: u['Username'] == user_name, users))
    return len(filtered_users) > 0


def get_cognito_oauth_scopes(resource_server_id):
    scopes = cognito_idp_client.describe_resource_server(
        UserPoolId=cognito_user_pool_id,
        Identifier=resource_server_id
        )['ResourceServer']['Scopes']
    return list(map(lambda s: s['ScopeName'], scopes))


def add_api_gateway_oauth_scopes(stack_name, rest_api_logical_name, api_gateway_resource_logical_name, cognito_resource_server_name):
    rest_api_id = get_resource_id_from_stack(stack_name, rest_api_logical_name)
    api_gateway_resource_id = get_resource_id_from_stack(stack_name, api_gateway_resource_logical_name)
    cognito_scopes = get_cognito_oauth_scopes(cognito_resource_server_name)
    for scope in cognito_scopes:
        apigateway_client.update_method(
            restApiId=rest_api_id,
            resourceId=api_gateway_resource_id,
            httpMethod='ANY',
            patchOperations=[
                {
                    'op': 'add',
                    'path': '/authorizationScopes',
                    'value': cognito_resource_server_name + '/' + scope
                }
            ]
        )
