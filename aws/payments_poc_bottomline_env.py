import os

# Container and tasks
bottomline_bacs_profile = os.environ['BOTTOMLINE_BACS_PROFILE_ID']
bottomline_fps_profile = os.environ['BOTTOMLINE_FPS_PROFILE_ID']
ptx_username = os.environ['PTX_USERNAME']
ptx_password = os.environ['PTX_PASSWORD']
ptx_endpoint = os.environ['PTX_ENDPOINT']

