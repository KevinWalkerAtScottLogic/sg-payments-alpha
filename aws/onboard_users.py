import sys
import subprocess
from payment_poc_common import *
from botocore.exceptions import ClientError
from getpass import getpass


stack_name = os.environ['APP_NAME']
file_upload_bucket_name = stack_name + "-file-upload-bucket"

# SFTP Transfer Users
sftp_user_policy_file_path = "sftp_user_policy.json"
sftp_user_role_arn = "arn:aws:iam::" + accountId + ":role/SftpUserRole"


def generate_keys_in_shell(username):
    print("Calling SSH KEY")
    if not os.path.exists('./keys'):
        os.mkdir('./keys')
    ssh_dir = "./keys/{}_id_rsa".format(username)
    command = "ssh-keygen -f {} -t rsa".format(ssh_dir)
    p = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    p.communicate()
    return ssh_dir + ".pub"


def get_masked_password():
    return getpass("Password: ")


def check_sftp_user_exists(username):
    server_id = get_sftp_server_id(stack_name)
    try:
        transfer_client.describe_user(
            ServerId=server_id,
            UserName=username
        )
        print(f"\tUser {username} already exists.")
        return True
    except ClientError:
        print(f"\tUser {username} does not exist, and will be created.")
        return False


def delete_sftp_user(username):
    server_id = get_sftp_server_id(stack_name)
    transfer_client.delete_user(
        ServerId=server_id,
        UserName=username
    )
    print(f"User {username} was deleted.")


def create_sftp_user_with_key(username, folder_name):

    server_id = get_sftp_server_id(stack_name)
    sftp_user_policy = open(sftp_user_policy_file_path, "r").read()
    print("CREATING KEY")
    public_key_location = generate_keys_in_shell(username)
    public_key = open(public_key_location, "r").read()

    transfer_client.create_user(
        HomeDirectory='/' + file_upload_bucket_name + '/' + folder_name,
        Policy=sftp_user_policy,
        Role=sftp_user_role_arn,
        ServerId=server_id,
        SshPublicKeyBody=public_key,
        UserName=username
    )
    print(f"Created SFTP user {username}. \nPublic key file:{public_key_location}")


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python " + sys.argv[0] + " user_name")
    else:
        create_sftp_user_with_key(sys.argv[1], sys.argv[1])
