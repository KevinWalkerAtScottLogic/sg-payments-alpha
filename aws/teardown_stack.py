import time
from payments_poc_stack_env import *
from payment_poc_common import *

# How long to wait for kafka cluster deletion to end (in seconds)
delete_wait_time = 25 * 60

# How often to test for kafka cluster deletion ending (in seconds)
delete_wait_interval = 15


def empty_s3_bucket(bucket_name):
    response = s3_client.list_object_versions(
        Bucket=bucket_name
    )
    if 'Versions' in response:
        for version in response['Versions']:
            s3_client.delete_object(
                Bucket=bucket_name,
                Key=version['Key'],
                VersionId=version['VersionId']
            )

    response = s3_client.list_object_versions(
        Bucket=bucket_name
    )
    if 'DeleteMarkers' in response:
        for delete_marker in response['DeleteMarkers']:
            s3_client.delete_object(
                Bucket=bucket_name,
                Key=delete_marker['Key'],
                VersionId=delete_marker['VersionId']
            )


def remove_sftp_server():
    server_id = get_sftp_server_id(stack_name)
    if server_id is not None:
        users = transfer_client.list_users(
            ServerId=server_id
        )['Users']
        for user in users:
            transfer_client.delete_user(
                ServerId=server_id,
                UserName=user['UserName']
            )
        transfer_client.delete_server(
            ServerId=server_id
        )


def delete_kafka_cluster():
    clusters = kafka_client.list_clusters(
        ClusterNameFilter=kafka_cluster_name
    )['ClusterInfoList']
    cluster_arns = []
    for cluster in clusters:
        cluster_arn = cluster['ClusterArn']
        cluster_arns.append(cluster_arn)
        kafka_client.delete_cluster(
            ClusterArn=cluster_arn
        )
    elapsed_time = 0
    for cluster_arn in cluster_arns:
        print("Waiting for cluster " + cluster_arn)
        while kafka_cluster_exists(kafka_cluster_name)\
                and get_kafka_cluster_state(kafka_cluster_name) == 'DELETING'\
                and elapsed_time < delete_wait_time:
            print(".", end='', flush=True)
            time.sleep(delete_wait_interval)
            elapsed_time += delete_wait_interval
        print()
    if kafka_cluster_exists(kafka_cluster_name):
        raise Exception(f"Failed to delete all Kafka clusters with name " + kafka_cluster_name)


def delete_vpc_peering_connection_id(db_vpc_id, peer_vpc_id):
    connections = ec2_client.describe_vpc_peering_connections(
        Filters=[
            {
                'Name': 'requester-vpc-info.vpc-id',
                'Values': [
                    db_vpc_id
                ]
            },
            {
                'Name': 'accepter-vpc-info.vpc-id',
                'Values': [
                    peer_vpc_id
                ]
            },
        ],
    )['VpcPeeringConnections']
    if len(connections) > 0:
        connection_id = connections[0]['VpcPeeringConnectionId']
        ec2_client.delete_vpc_peering_connection(
            VpcPeeringConnectionId=connection_id
        )


def detach_db_security_group(ecs_security_group_logical_name, db_security_group_logical_name):
    vpc_security_group_id = get_resource_id_from_stack(stack_name, ecs_security_group_logical_name)
    db_security_group_id = get_resource_id_from_stack(db_stack_name, db_security_group_logical_name)

    security_groups = ec2_client.describe_security_groups(
        GroupIds=[vpc_security_group_id]
    )['SecurityGroups']

    if len(security_groups) > 0:
        ip_permissions_egress = security_groups[0]['IpPermissionsEgress']
        for item in ip_permissions_egress:
            user_id_pairs = item['UserIdGroupPairs']
            for user_id_pair in user_id_pairs:
                if user_id_pair['GroupId'] == db_security_group_id:
                    ec2_client.revoke_security_group_ingress(
                        GroupId=db_security_group_id,
                        IpPermissions=[
                            {
                                "IpProtocol": "tcp",
                                "FromPort": 1,
                                "ToPort": 65535,
                                "UserIdGroupPairs": [
                                    {
                                        "GroupId": vpc_security_group_id
                                    }
                                ]
                            }
                        ]
                    )

    peer_vpc_id = get_resource_id_from_stack(stack_name, 'Vpc')
    db_vpc_id = get_resource_id_from_stack(db_stack_name, 'Vpc')

    delete_vpc_peering_connection_id(db_vpc_id, peer_vpc_id)


def teardown_rest_of_infrastructure():

    print("Detaching DB Security Group")
    detach_db_security_group("EcsSecurityGroup", "DBSecurityGroup")
    print("Emptying S3 buckets")
    empty_s3_bucket(file_upload_bucket_name)
    empty_s3_bucket(frontend_bucket_name)
    print("Emptied S3 buckets")
    print("Removing SFTP server")
    remove_sftp_server()
    print("SFTP server removed OK")
    print("Removing Kafka cluster")
    delete_kafka_cluster()
    print("Kafka cluster removed OK")


def teardown_stack():
    cf_client.delete_stack(
        StackName=stack_name
    )
    print("Waiting for stack teardown to complete", flush=True)
    elapsed_time = 0
    while stack_is_in_state(stack_name, ['DELETE_IN_PROGRESS']) and elapsed_time < delete_wait_time:
        print(".", end='', flush=True)
        time.sleep(delete_wait_interval)
        elapsed_time += delete_wait_interval
    print()


def teardown_full_infrastructure():
    if not stack_exists(stack_name):
        print(f"Stack {stack_name} does not exist")
    else:
        print(f"Tearing down surrounding infrastructure for stack {stack_name}")
        teardown_rest_of_infrastructure()
        print(f"Finished tearing down surrounding infrastructure for stack {stack_name}")
        print(f"Tearing down stack {stack_name}")
        teardown_stack()
        if not stack_exists(stack_name):
            print(f"Deleted stack {stack_name}")
        else:
            raise Exception(f"Failed to delete stack {stack_name}")


teardown_full_infrastructure()
