import time
from payments_poc_stack_env import *
from payment_poc_common import *

# How long to wait for service to drain (in seconds)
drain_wait_time = 10 * 60

# How often to test for service drain ending (in seconds)
drain_wait_interval = 15


def remove_target_group_and_listeners():
    lb_arn = get_loadbalancer_arn()

    listener_response = elb_client.describe_listeners(
        LoadBalancerArn=lb_arn
    )
    # Must get list of target groups before deleting the listeners
    target_group_response = elb_client.describe_target_groups(
        LoadBalancerArn=lb_arn
    )
    # Must delete listeners before target groups
    for listener in listener_response['Listeners']:
        print("Deleting listener " + listener['ListenerArn'])
        elb_client.delete_listener(
            ListenerArn=listener['ListenerArn']
        )
    target_groups = target_group_response['TargetGroups']
    for target_group in target_groups:
        print("Deleting target group " + target_group['TargetGroupName'])
        elb_client.delete_target_group(
            TargetGroupArn=target_group['TargetGroupArn']
        )


def get_loadbalancer_arn():
    response = elb_client.describe_load_balancers(
        Names=[load_balancer_name]
    )
    lb_arn = response['LoadBalancers'][0]['LoadBalancerArn']
    return lb_arn


def remove_sqs_queue_if_exists():
    print("Checking for SQS queue " + sqs_file_event_queue)
    response = sqs_client.list_queues(
        QueueNamePrefix=sqs_file_event_queue
    )
    if 'QueueUrls' in response and len(response['QueueUrls']) > 0:
        remove_sqs_queue()


def remove_sqs_queue():
    queue_url = sqs_client.get_queue_url(
        QueueName=sqs_file_event_queue
    )['QueueUrl']
    subscription = get_subscription_for_sqs_queue()
    if subscription is not None:
        subscription_arn = subscription['SubscriptionArn']
        print(f"Deleting SQS queue subscription {subscription_arn}")
        sns_client.unsubscribe(
            SubscriptionArn=subscription_arn
        )
    print(f"Deleting SQS queue {queue_url}")
    sqs_client.delete_queue(
        QueueUrl=queue_url
    )


def get_subscription_for_sqs_queue():
    topic_arn = f"arn:aws:sns:{region}:{accountId}:{sns_file_event_topic}"
    queue_arn = f"arn:aws:sqs:{region}:{accountId}:{sqs_file_event_queue}"
    subscriptions = sns_client.list_subscriptions_by_topic(
        TopicArn=topic_arn
    )['Subscriptions']
    subscription = list(filter(lambda s: 'Endpoint' in s and s['Endpoint'] == queue_arn, subscriptions))
    if len(subscription) > 0:
        return subscription[0]
    else:
        return None


def terminate_all_services():
    # List all task definitions and revisions
    response = ecs_client.list_services(
        cluster=ecs_cluster_name,
    )
    service_list = response["serviceArns"]
    service_names = list(map(lambda arn: arn.split('/').pop(), service_list))

    for name in service_names:
        # Set desired service count to 0 (obligatory to delete)
        print("Draining tasks in service " + name)
        ecs_client.update_service(
            cluster=ecs_cluster_name,
            service=name,
            desiredCount=0
        )

    print("Waiting for services to drain")
    elapsed_time = 0
    for name in service_names:
        while get_running_count(ecs_cluster_name, name) != 0 and elapsed_time < drain_wait_time:
            print(".", end='', flush=True)
            time.sleep(drain_wait_interval)
            elapsed_time += drain_wait_interval
    print("\nServices drained")

    for name in service_names:
        # Delete service
        print("Deleting service " + name)
        ecs_client.delete_service(
            cluster=ecs_cluster_name,
            service=name,
            force=True
        )

    print("Waiting for services to be deleted")
    elapsed_time = 0
    for name in service_names:
        while get_service_status(ecs_cluster_name, name) != 'Not found'\
                and get_service_status(ecs_cluster_name, name) != 'INACTIVE'\
                and elapsed_time < drain_wait_time:
            print(".", end='', flush=True)
            time.sleep(drain_wait_interval)
            elapsed_time += drain_wait_interval
    print("\nServices deleted")


def terminate_service(service_name):

    print("looking for " + service_name)
    try:
        # Set desired service count to 0 (obligatory to delete)
        print("Draining tasks in service " + service_name)
        ecs_client.update_service(
            cluster=ecs_cluster_name,
            service=service_name,
            desiredCount=0
        )
        # Wait for services to drain
        elapsed_time = 0
        print("Waiting for service to drain")
        while get_running_count(ecs_cluster_name, service_name) != 0 and elapsed_time < drain_wait_time:
            print(".", end='', flush=True)
            time.sleep(drain_wait_interval)
            elapsed_time += drain_wait_interval
        print("\nService drained")

        # Delete service
        print("Deleting service " + service_name)
        ecs_client.delete_service(
            cluster=ecs_cluster_name,
            service=service_name,
            force=True
        )
        print("Waiting for service to be deleted")
        elapsed_time = 0
        while get_service_status(ecs_cluster_name, service_name) != 'Not found'\
                and get_service_status(ecs_cluster_name, service_name) != 'INACTIVE'\
                and elapsed_time < drain_wait_time:
            print(".", end='', flush=True)
            time.sleep(drain_wait_interval)
            elapsed_time += drain_wait_interval
        print("\nService deleted")

    except Exception as e:
        print("Service not found/not active")
        print(e)


remove_target_group_and_listeners()
remove_sqs_queue_if_exists()
terminate_all_services()
