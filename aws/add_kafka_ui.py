from payment_poc_common import *

task_role_name = "PaymentsPocTaskRole"
execution_role_name = "ecsTaskExecutionRole"
stack_name = os.environ['APP_NAME']

health_check_grace_period_seconds = 300


def get_role_arn(role_name):
    return iam_client.get_role(
        RoleName=role_name
    )['Role']['Arn']


def register_task_definition(task_name, image_url, port, environment):
    print(f"Registering {task_name}, using image {image_url}")

    task_role_arn = get_role_arn(task_role_name)
    execution_role_arn = get_role_arn(execution_role_name)

    # Create a task definition
    response = ecs_client.register_task_definition(
        containerDefinitions=[
            {
                "logConfiguration": {
                    "logDriver": "awslogs",
                    "options": {
                        "awslogs-group": "/ecs/" + task_name,
                        "awslogs-region": region,
                        "awslogs-stream-prefix": "ecs"
                    }
                },
                "portMappings": [
                    {
                        "hostPort": port,
                        "protocol": "tcp",
                        "containerPort": port
                    }
                ],
                "memoryReservation": 512,
                "volumesFrom": [],
                "image": image_url,
                "name": task_name,
                "environment": environment
            }
        ],
        family=task_name,
        taskRoleArn=task_role_arn,
        cpu="256",
        memory="512",
        networkMode='awsvpc',
        requiresCompatibilities=['FARGATE'],
        executionRoleArn=execution_role_arn,
    )

    task_definition_arn = response['taskDefinition']['taskDefinitionArn']
    print("Registered task definition " + task_definition_arn)
    return task_definition_arn


def create_service(cluster_name, service_name, task_name, container_port, target_group_arn, subnets,
                   ecs_security_group):
    print(f"Creating service for task {task_name}...")

    response = ecs_client.create_service(
        cluster=cluster_name,
        serviceName=service_name,
        taskDefinition=task_name,
        desiredCount=1,
        deploymentConfiguration={
            'maximumPercent': 100,
            'minimumHealthyPercent': 0
        },
        launchType='FARGATE',
        loadBalancers=[
            {
                'targetGroupArn': target_group_arn,
                'containerName': task_name,
                'containerPort': container_port
            },
        ],
        networkConfiguration={
            'awsvpcConfiguration': {
                'subnets': subnets,
                'securityGroups': [
                    ecs_security_group
                ],
                'assignPublicIp': 'ENABLED'
            }
        },
        healthCheckGracePeriodSeconds=health_check_grace_period_seconds
    )

    print("Created service " + response['service']['serviceArn'])


def create_target_group(lb_target_group_name, host_port, vpc_id):
    created_target_arn = ""

    print(f"Creating {lb_target_group_name}...")
    response = elb_client.create_target_group(
        Name=lb_target_group_name,
        Protocol='HTTP',
        Port=host_port,
        HealthCheckProtocol='HTTP',
        HealthCheckPath="/",
        TargetType='ip',
        VpcId=vpc_id
    )
    print("Created target group...")

    for targetGroup in response['TargetGroups']:
        if targetGroup['TargetGroupName'] == lb_target_group_name:
            print(f"Found target group {lb_target_group_name}")
            created_target_arn = targetGroup['TargetGroupArn']

    return created_target_arn


def setup_target_group_if_not_exists(service_name, lb_arn, host_port, vpc_id, elb_security_group):
    lb_target_group_name = f"{stack_name}-{service_name}"[:32]
    if lb_target_group_name.endswith('-'):
        lb_target_group_name = lb_target_group_name[:-1]

    response = elb_client.describe_target_groups(
        LoadBalancerArn=lb_arn
    )

    target_arn = ""

    does_target_group_exist = False
    for targetGroup in response['TargetGroups']:
        if targetGroup['TargetGroupName'] == lb_target_group_name:
            print(f"Found target group {lb_target_group_name}")
            does_target_group_exist = True
            target_arn = targetGroup['TargetGroupArn']

    print("Creating target group...")
    if not does_target_group_exist:
        target_arn = create_target_group(lb_target_group_name, host_port, vpc_id)

    if not does_listener_exist(lb_arn, host_port):
        create_listener(lb_arn, target_arn, host_port)
        print(f"Created listener for target group {target_arn}")
    else:
        print(f"Listener for port {host_port} already exists")

    if not does_open_port_exist(host_port, elb_security_group):
        open_load_balancer_port(host_port, elb_security_group)
        print(f"Opened port {host_port} for the load balancer")
    else:
        print(f"Port {host_port} already open on the load balancer")

    return target_arn


def create_or_update_service(cluster_name, service_name, task_name, container_port, target_group_arn, subnets,
                             ecs_security_group):
    print("looking for service " + service_name)

    try:
        response = ecs_client.describe_services(
            cluster=cluster_name,
            services=[service_name]
        )
        if len(response['services']) == 0 or response['services'][0]['status'] == 'INACTIVE':
            print("Creating service")
            create_service(cluster_name, service_name, task_name, container_port, target_group_arn, subnets,
                           ecs_security_group)
        else:
            print("Updating service")
            ecs_client.update_service(
                cluster=cluster_name,
                service=service_name,
                taskDefinition=task_name,
                forceNewDeployment=True,
                healthCheckGracePeriodSeconds=health_check_grace_period_seconds
            )

    except Exception:
        raise Exception(f"Could not query cluster {cluster_name} to describe service")


def setup_service_and_task(service_name, image, port, environment):
    task_name = f"{stack_name}-{service_name}"
    vpc_id = get_resource_id_from_stack(stack_name, 'Vpc')
    vpc_subnets = get_subnet_info(vpc_id)
    ecs_security_group = get_resource_id_from_stack(stack_name, 'EcsSecurityGroup')
    elb_security_group = get_resource_id_from_stack(stack_name, 'ElbSecurityGroup')

    register_task_definition(task_name, image, port, environment)
    rest_target_group_arn = setup_target_group_if_not_exists(service_name, load_balancer_arn, port, vpc_id,
                                                             elb_security_group)
    create_log_group_if_not_exists(task_name)
    create_or_update_service(ecs_cluster_name, service_name, task_name, port, rest_target_group_arn, vpc_subnets,
                             ecs_security_group)


def get_kafka_rest_env(hostname, port):
    bootstrap_brokers = get_kafka_brokers(kafka_cluster_name)

    return [
        {
            "name": "KAFKA_REST_HOST_NAME",
            "value": hostname
        },
        {
            "name": "KAFKA_REST_LISTENERS",
            "value": f"http://0.0.0.0:{port}"
        },
        {
            "name": "KAFKA_REST_BOOTSTRAP_SERVERS",
            "value": bootstrap_brokers
        }
    ]


def get_kafka_topics_ui_env(hostname, ui_port, rest_port):
    return [
        {
            "name": "KAFKA_REST_PROXY_URL",
            "value": f"http://{hostname}:{rest_port}"
        },
        {
            "name": "PROXY",
            "value": "true"
        },
        {
            "name": "PORT",
            "value": str(ui_port)
        }
    ]


# Basic configuration

print(f"Using stack {stack_name}")

ecs_cluster_name = stack_name
kafka_cluster_name = stack_name

kafka_topics_ui_port = 8091

load_balancer_arn = get_resource_id_from_stack(stack_name, 'ElasticLoadBalancer')
load_balancer = elb_client.describe_load_balancers(
    LoadBalancerArns=[load_balancer_arn]
)['LoadBalancers'][0]

lb_hostname = load_balancer['DNSName']
lb_name = load_balancer['LoadBalancerName']


# Set up cp-kafka-rest service

kafka_rest_service_name = "cp-kafka-rest"
kafka_rest_image = "confluentinc/cp-kafka-rest"
kafka_rest_port = 8092
kafka_rest_env = get_kafka_rest_env(lb_hostname, kafka_rest_port)

setup_service_and_task(kafka_rest_service_name, kafka_rest_image, kafka_rest_port, kafka_rest_env)

add_api_gateway_stage(stack_name, 'KafkaRestAPI', "kafka", kafka_rest_port)

# Set up kafka-topics-ui service

kafka_topics_ui_service_name = "kafka-topics-ui"
kafka_topics_ui_image = "landoop/kafka-topics-ui"
kafka_topics_ui_env = get_kafka_topics_ui_env(lb_hostname, kafka_topics_ui_port, kafka_rest_port)

setup_service_and_task(kafka_topics_ui_service_name, kafka_topics_ui_image, kafka_topics_ui_port, kafka_topics_ui_env)
