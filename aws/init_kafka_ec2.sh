#!/bin/bash

## Use this script to set up an Amazon Linux EC2 instance with Kafka.
##
## Includes scripts to purge topic logs for a given branch, and to
## restore the configuration.

sudo yum update -y

# Update to Java 8, needed for Kafka
sudo yum install -y java-1.8.0
sudo yum remove -y java-1.7.0-openjdk

# Install Kafka
curl -o kafka.tgz "http://mirrors.ukfast.co.uk/sites/ftp.apache.org/kafka/2.2.0/kafka_2.12-2.2.0.tgz"
tar xvf kafka.tgz
export KAFKA_BIN=~/kafka_2.12-2.2.0/bin

# Create scripts to purge Kafka topics
cat > purge_topics.sh << EOF
#!/bin/bash

if [ \$# -ne 2 ]; then
    echo "Usage \$0 branch_name zookeeper"
    exit
fi

BRANCH="\$1"
ZOOKEEPER="\$2"

$KAFKA_BIN/kafka-topics.sh --zookeeper "\$ZOOKEEPER" --list | while read topic;
do
  if [[ \$topic == \$BRANCH*  ]];
    then
      echo "Purging \$topic"
      $KAFKA_BIN/kafka-configs.sh --zookeeper "\$ZOOKEEPER" -alter --entity-type topics --entity-name \$topic --add-config retention.ms=1000
  fi
done
EOF
chmod +x purge_topics.sh

cat > restore_topics.sh << EOF
#!/bin/bash

if [ \$# -ne 2 ]; then
    echo "Usage \$0 branch_name zookeeper"
    exit
fi

BRANCH="\$1"
ZOOKEEPER="\$2"

$KAFKA_BIN/kafka-topics.sh --zookeeper "\$ZOOKEEPER" --list | while read topic;
do
  if [[ \$topic == \$BRANCH*  ]];
    then
      echo "Restoring \$topic"
      $KAFKA_BIN/kafka-configs.sh --zookeeper "\$ZOOKEEPER" -alter --entity-type topics --entity-name \$topic --delete-config retention.ms
  fi
done
EOF
chmod +x restore_topics.sh
