#!/usr/bin/env bash

export AWS_ACCOUNT_ID={account_id}
export AWS_ACCESS_KEY_ID={access_key}
export AWS_SECRET_ACCESS_KEY={secret_key}
export AWS_REGION=eu-west-1
export API_URL_PORT="127.0.0.1:8080"
export APP_NAME=payments-poc

python -c 'from onboard_script import update_service; update_service()'
