#!/usr/bin/env bash

# Use this script to manually run the deployment or teardown scripts.
# Set the AWS_ACCOUNT_ID, AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY,
# CIRCLE_BRANCH_STR, DOMAIN_NAME, DOMAIN_CERTIFICATE_ARN,
# COGNITO_USER_POOL_ID, COGNITO_APP_CLIENT_ID and
# CLOUDFRONT_DOMAIN_CERTIFICATE_ARN as required.

# Set these to your required values
export AWS_ACCOUNT_ID=XXX
export AWS_ACCESS_KEY_ID=XXX
export AWS_SECRET_ACCESS_KEY=XXX
export CIRCLE_BRANCH_STR=develop
export AWS_REGION=eu-west-1
export DOMAIN_NAME=XXX
export DOMAIN_CERTIFICATE_ARN=XXX
export COGNITO_USER_POOL_ID=XXX
export COGNITO_DOMAIN=XXX
export CLOUDFRONT_DOMAIN_CERTIFICATE_ARN=XXX
export UI_COGNITO_APP_CLIENT_ID=XXX
export SERVICE_TO_SERVICE_COGNITO_APP_CLIENT_ID=XXX
export SERVICE_TO_SERVICE_COGNITO_APP_CLIENT_SECRET=XXX

# Change this if you want to create a different application in AWS
export APP_NAME=payments-poc

export RDS_MASTER_USER_PASSWORD=sysadminpwd
export IAM_EXECUTION_ROLE=ecsTaskExecutionRole
export LB_PROD_PORT=80
export LB_DEV_PORT=81
export AVAILABILITY_ZONES=eu-west-1a,eu-west-1b,eu-west-1c
export QUICK_SIGHT_ROLE_NAME=QuickSightDashboardUserRole

export BRANCH_TAG="${APP_NAME}-${CIRCLE_BRANCH_STR}"
export ECR_REPO_ARN="${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/payments-poc"
export BRANCH_DB_NAME=`echo ${CIRCLE_BRANCH_STR,,}db | tr -cd [:alnum:]`
export QUICK_SIGHT_ROLE_ARN="arn:aws:iam::${AWS_ACCOUNT_ID}:role/${QUICK_SIGHT_ROLE_NAME}"
export SQS_FILE_EVENT_QUEUE=${BRANCH_TAG}-file-event-queue

#########################################################################
# Uncomment one of the sections below.
#########################################################################

## Scripts to create the application
#python create_stack.py
#python register_task_definition.py core application
#python register_task_definition.py mock-bottomline mock-gateway
#python register_task_definition.py mock-accesspay mock-gateway

#python create_service.py core
#python create_service.py mock-bottomline
#python create_service.py mock-accesspay

## Script to setup kafka ui to monitor Kafka topics
#python add_kafka_ui.py

## Scripts to remove the application
#python teardown_service.py core
#python teardown_service.py mock-bottomline
#python teardown_service.py mock-accesspay
#python teardown_stack.py
