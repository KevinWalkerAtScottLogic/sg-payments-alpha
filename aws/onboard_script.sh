#!/usr/bin/env bash

export AWS_ACCOUNT_ID={account_id}
export AWS_ACCESS_KEY_ID={access_key}
export AWS_SECRET_ACCESS_KEY={secret_key}
export AWS_REGION=eu-west-1
export API_URL_PORT="127.0.0.1:8080"
export APP_NAME=payments-poc
export COGNITO_USER_POOL_ID={user_pool_id}
export COGNITO_CLIENT_ID={client_id}
export BRANCH=develop

python -c 'from onboard_script import run_onboarding; run_onboarding()'
