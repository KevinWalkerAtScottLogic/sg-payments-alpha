# Outgoing Payments Proof of Concept

This repository contains all the source code for payments proof of concept.

## Code Layout

- aws

Scripts related to building and deploying the application on AWS, as well as utility scripts for onboarding customers and services

- core

    - application
    
    Core application components
    
    - common
     
    Components and code that is shared between the core and gateway implementations
    
    - model
    
    Domain model classes shared by all components
    
- frontend

UI react application

- gateway
    - outbound
    
        - bottomline
        
        An outbound payment gateway executing payments via the bottomline PT-X API
        
        - core
        
        Core outbound payment classes, implementations of outbound payment gateways extend from components declared here
                    
        - mock-outbound
        
        A mock outbound payment gateway that can return success or failure based on a configured expression


## Build instructions

### Frontend

**Requirements**

Yarn(https://yarnpkg.com/en/docs/install). We have used 1.15.2, but it should probably be fine to use the latest stable release

Node JS (https://nodejs.org/en/download/). We use version 10.15.3. Latest stable should work, but newer versions cause issues 

**Instructions**

After checking out the repository open the ‘frontend’ folder in the editor of your choice. Navigate to the directory in the terminal and run 

`yarn install`

This will download all the latest version of modules into the node_modules folder.
You will need to update the .env.development file to point to the correct variables. This is used whenever the front end is 
being served locally in development mode and tells it where to call its endpoints.

This should point to the following environment variables:

`REACT_APP_COGNITO_APP_CLIENT_ID=<COGNITO_ID>`

`REACT_APP_API_ENDPOINT=http://localhost:8080` - or wherever the service you want to connect to is running

`REACT_APP_COGNITO_REDIRECT_URL=http://localhost:3000`

`REACT_APP_DYNAMIC_AUTH=false` - if you want to run against a local service without using cognito set this to true

`REACT_APP_STATIC_SITE=true`

The front end will only pick up variables if they are pre-pended with REACT_APP prefix.

To build the application run

`yarn test`

`yarn build`

### Backend

**Requirements**

JDK 11, with the JAVA_HOME added to the PATH

**Instructions**

The application is built using gradle.

After checking out the repository. Run `./gradlew clean build` (or `gradlew.bat clean build` on Windows)

This will build and test the application. This build can take a while due to integration tests that spin up a Kafka instance. 
To run without Kafka based tests pass `-PexcludeKafkaTests` as an additional parameter

### Local Packaging

**Requirements**

As per Building frontend and backend

**Instructions**

After checking out the repository. Run `./build` (or `build.bat` on Windows)

This will build the entire application (frontend and backend), mock gateways, and create a directory with all built artifacts including scripts to run the built application
the build artifacts will be located at `local/build/forRunningLocally`

## Running Locally (From Source)

### Frontend

**Requirements**

As per building the frontend

**Instructions**

To run the app use the command

`yarn start`

This will start the UI at localhost:3000.  

### Backend

**Requirements**

As per building the backend plus an IDE of your choice

Import the repository in to your IDE as a gradle project.

**Instructions**

The application can be run locally in either embedded or distributed mode. In both cases the main class is at local/src/main/java/gov/scot/payments/LocalApplication.java

- Embedded - run the core, kafka, DB, and outbound gateway all in the same process

Run the main class from your IDE of choice with the following program arguments `embedded -s3.path PATH/TO/FOLDER`

- Distributed - run the core standalone connecting to external kafka, and DB.

Run the main class from your IDE of choice with the following program arguments `distributed -s3.path PATH/TO/FOLDER -kafka <KAFKA_SERVERS -zk <ZOOKEEPER_SERVERS> -db <DB_JDBC_URL> -db.user DB_USER (optional)> -db.pwd <DB_USER (optional)>`

In all cases, to find out about other configuration options run the main class with the argument `--help`

## Running Locally (From Local Package)

**Requirements**

JDK 11, with the JAVA_HOME added to the PATH

A built local package as per the instructions on this page

**Instructions (Windows)**

Open a command prompt and navigate the the root of a local package (i.e. if you have followed the build instructions 
here go to local/build/forRunningLocally).

run `start-local-payments-poc.bat distributed` 

This will start the core application, Kafka, an in-memory DB, the mock customer site, and the specified gateways (see configuration options). 
Once the application is launched the default web browser will be opened with the following pages:
- The payments poc UI
- The interactive API page for the core application
- A web console for the embedded database
- The mock customer site home page

The URL's of these sites will also be printed to the console

To stop the application run `stop-local-payments-poc.bat`

**Instructions (Mac)**

Open Terminal and navigate the the root of a local package (i.e. if you have followed the build instructions 
here go to local/build/forRunningLocally).

run `./start-local-payments-poc distributed`

This will start the core application, Kafka, an in-memory DB, the mock customer site, and the specified gateways (see configuration options). 
Once the application is launched the default web browser will be opened with the following pages:
- The payments poc UI
- The interactive API page for the core application
- A web console for the embedded database
- The mock customer site home page

The URL's of these sites will also be printed to the console

To stop the application run `./stop-local-payments-poc.bat`

**Configuration options**

To find out what configuration options are available run `./start-local-payments-poc distributed --help` (or `start-local-payments-poc.bat distributed --help` on Windows) 

Configuration options are located in the file `payments-poc-distributed-config.txt` contained in the base of the local package directory. 
Each option should be provided on a new line. To see / edit the defaults open the configuration file in a txt editor (i.e. notepad.exe / TextEdit)

By default mock authentication is used, such that when you login to the UI you will be an admin user with permissions to access all data, 
if using cognito for authentication then authorization will be based on group membership in cognito

When using mock authentication the API can be accessed as admin by providing the following value as the API token:
`Bearer ew0KICAiZW1haWwiIDogImFkbWluIiwNCiAgInVzZXJOYW1lIiA6ICJhZG1pbiIsDQogICJyb2xlcyIgOiBbIHsNCiAgICAib3BlcmF0aW9uIiA6ICJXcml0ZSIsDQogICAgImVudGl0eVR5cGUiIDogIioiLA0KICAgICJlbnRpdHlJZCIgOiAiKiINCiAgfSBdDQp9`

By default a directory named `bucket` will be created in the base of the local package, and for each service that specifies a file format
a folder with the same name of the service will be created within the bucket. Files can be dropped here and will be picked up by the application.
