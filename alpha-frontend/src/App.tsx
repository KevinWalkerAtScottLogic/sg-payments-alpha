import "./App.scss";

import { Redirect, Switch } from "react-router";

import Home from "components/home/Home";
import { LOGIN_PATH } from "routes/routes";
import React from "react";

export class App extends React.Component {
    render = () => (
        <div className="app-parent">
            <Switch>
                <Redirect exact from="/" to={LOGIN_PATH} />
                <Home />
            </Switch>
        </div>
    );
}

export default App;
