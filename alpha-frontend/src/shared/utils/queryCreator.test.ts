import { createPageNumberQuery, createPageSizeQuery, createSortQuery } from "./queryCreator";

import { PagingFilter } from "components/paging/pagingFilter";
import { Sort } from "enums/sorting";

describe("Given the sorting query creator", () => {
    const queryObject: Sort<unknown> = { field: "", order: "asc" };

    describe("when given a sort argument", () => {
        const field = "test field";
        beforeEach(() => {
            queryObject.field = field;
        });

        it("it returns the correct query string", () => {
            expect(createSortQuery(queryObject)).toBe(`${queryObject.field},${queryObject.order}`);
        });
    });

    describe("when no sorting variable is passed", () => {
        const field = "test field";
        beforeEach(() => {
            queryObject.field = field;
            queryObject.order = "none";
        });

        it("it returns the correct query string", () => {
            expect(createSortQuery(queryObject)).toBe(null);
        });
    });
});

describe("Given the page size query creator", () => {
    let queryObject: PagingFilter;
    let queryResult: number;
    let expectedPageSize: number;

    describe("when given a page size argument", () => {
        beforeEach(() => {
            expectedPageSize = 50;
            queryObject = { pageSize: expectedPageSize };
        });

        it("the returned string contains '50'", () => {
            queryResult = createPageSizeQuery(queryObject.pageSize);
            expect(queryResult).toBe(`${expectedPageSize}`);
        });
    });
});

describe("Given the page number query creator", () => {
    let queryObject: PagingFilter;
    let queryResult: number;
    let expectedPageNumber: number;

    describe("when given a page number argument", () => {
        beforeEach(() => {
            expectedPageNumber = 2;
            queryObject = { pageNumber: expectedPageNumber };
        });

        it("the returned string contains 'pageNumber=2'", () => {
            queryResult = createPageNumberQuery(queryObject.pageNumber);
            expect(queryResult).toBe(`${expectedPageNumber}`);
        });
    });
});
