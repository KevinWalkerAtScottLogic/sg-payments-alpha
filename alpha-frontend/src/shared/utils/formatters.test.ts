import { Column, RowNode } from "ag-grid-community";
import { formatDate, formatDateTime, formatTooltipDate } from "./formatters";

import { FormatterParams } from "components/reusables/table/Table";
import moment from "moment";

const defaultDateFormatParams: FormatterParams<{}> = {
    data: {},
    value: null,
    node: new RowNode(),
    colDef: {},
    column: new Column({}, null, "colId", true),
    api: null,
    columnApi: null,
    context: null,
};

const createParams = <T>(value: unknown, data?: Partial<T>) => ({
    ...defaultDateFormatParams,
    ...{ value, data: { ...defaultDateFormatParams.data, ...data } },
});

describe("Given a call to formatDate", () => {
    it("then the correctly formatted date is returned", () => {
        expect(formatDate(createParams(moment("2020-01-02")))).toBe("2 Jan 2020");
    });
});

describe("Given a call to formateDateTime", () => {
    it("then the correctly formatted date and time is returned", () => {
        expect(formatDateTime(moment("2020-01-02 13:00"))).toBe("2 Jan 2020 at 01:00pm");
    });
});
