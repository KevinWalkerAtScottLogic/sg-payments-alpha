import { FormatterParams } from "components/reusables/table/Table";
import { ITooltipParams } from "ag-grid-community";
import moment from "moment";

export const formatDate = (params: FormatterParams<unknown> | ITooltipParams) => params.value.format("D MMM YYYY");

export const formatDateTime = (date: moment.Moment): string => date.format("D MMM YYYY [at] hh:mma");
