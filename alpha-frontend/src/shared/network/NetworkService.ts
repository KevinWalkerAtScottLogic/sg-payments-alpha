import axios, { AxiosInstance, Canceler } from "axios";

import state from "state";

const API_TIMEOUT_VALUE = 5000;

export class NetworkService {
    apiInstance: AxiosInstance;
    cancelerMap: Map<string, Canceler>;

    constructor() {
        this.apiInstance = axios.create({
            timeout: API_TIMEOUT_VALUE,
            baseURL: process.env.REACT_APP_API_ENDPOINT,
        });
        this.cancelerMap = new Map<string, Canceler>();
    }

    // I'm not sure if this is required. I put it in on the assumption that we would need to cancel
    // the previous request when a new filter was applied however I'm not sure you can resend a request
    // until the previous one has returned. We need to look at this when we do the filters and possibly
    // remove all this cancellation logic. Or if we do keep it then unit test it.
    private cancelPreviousCallToUrl(url: string) {
        const cancel = this.cancelerMap.get(url);
        if (cancel) {
            cancel();
        }
    }

    public async get<T>(url: string, params?: object): Promise<T | undefined> {
        this.cancelPreviousCallToUrl(url);
        return this.apiInstance
            .get(url, {
                params,
                headers: {
                    Authorization: `Bearer ${state.getState().loginState.idToken}`,
                    Accept: "application/hal+json",
                },
                cancelToken: new axios.CancelToken((c: Canceler) => {
                    this.cancelerMap.set(url, c);
                }),
            })
            .then(response => response.data)
            .catch(err => {
                if (axios.isCancel(err)) {
                    return undefined;
                }
                return Promise.reject(err);
            });
    }
}

export default new NetworkService();
