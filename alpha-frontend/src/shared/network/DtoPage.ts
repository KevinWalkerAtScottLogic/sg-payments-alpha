export interface DtoPage<T> {
    _embedded: T;
    page: {
        size: string;
        totalElements: string;
        totalPages: string;
        number: string;
    };
}
