export interface SVGProps {
    height: number;
    width: number;
    className?: string;
}
