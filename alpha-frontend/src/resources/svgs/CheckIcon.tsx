import React from "react";
import { SVGProps } from "./svgProps";

export class CheckIcon extends React.Component<SVGProps> {
    render = () => (
        <svg {...this.props} viewBox="0 0 24 24">
            <path d="M0 0h24v24H0z" fill="none" />
            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z" />
        </svg>
    );
}
