import React from "react";
import { SVGProps } from "./svgProps";

export class KeyboardReturnIcon extends React.Component<SVGProps> {
    render = () => (
        <svg {...this.props} viewBox="0 0 24 24">
            <path d="M0 0h24v24H0z" fill="none" />
            <path d="M19 7v4H5.83l3.58-3.59L8 6l-6 6 6 6 1.41-1.41L5.83 13H21V7z" />
        </svg>
    );
}
