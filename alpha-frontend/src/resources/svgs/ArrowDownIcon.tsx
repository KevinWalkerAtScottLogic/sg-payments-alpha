import React from "react";
import { SVGProps } from "./svgProps";

export class ArrowDown extends React.Component<SVGProps> {
    render = () => (
        <svg {...this.props} viewBox={`0 0 24 24`}>
            <path d="M7 10l5 5 5-5z" />
            <path d="M0 0h24v24H0z" fill="none" />
        </svg>
    );
}
