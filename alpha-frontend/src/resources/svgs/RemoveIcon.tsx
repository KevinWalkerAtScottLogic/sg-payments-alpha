import React from "react";
import { SVGProps } from "./svgProps";

export class RemoveIcon extends React.Component<SVGProps> {
    render = () => (
        <svg {...this.props} viewBox="0 0 24 24">
            <path d="M19 13H5v-2h14v2z" />
            <path d="M0 0h24v24H0z" fill="none" />
        </svg>
    );
}
