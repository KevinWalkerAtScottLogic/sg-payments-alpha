import "./Sandbox.scss";

import { AppDispatch, RootState } from "../state/rootReducer";

import { ArrowDown } from "resources/svgs/ArrowDownIcon";
import { BlockIcon } from "resources/svgs/BlockIcon";
import { Button } from "components/reusables/button/Button";
import { CancelIcon } from "resources/svgs/CancelIcon";
import { CheckIcon } from "resources/svgs/CheckIcon";
import { CloseIcon } from "resources/svgs/CloseIcon";
import { CompressIcon } from "resources/svgs/CompressIcon";
import { DateRangeIcon } from "resources/svgs/DateRangeIcon";
import { ErrorIcon } from "resources/svgs/ErrorIcon";
import { ErrorOutlineIcon } from "resources/svgs/ErrorOutlineIcon";
import { ExpandIcon } from "resources/svgs/ExpandIcon";
import { InfoIcon } from "resources/svgs/InfoIcon";
import { KeyboardReturnIcon } from "resources/svgs/KeyboardReturnIcon";
import { List } from "components/reusables/list/List";
import { LoadingButton } from "components/reusables/loading-button/LoadingButton";
import { PaginationButton } from "components/reusables/pagination-button/PaginationButton";
import React from "react";
import { RemoveIcon } from "../resources/svgs/RemoveIcon";
import { RowActionButton } from "components/reusables/row-action-button/RowActionButton";
import { SaveIcon } from "../resources/svgs/SaveIcon";
import { SearchIcon } from "../resources/svgs/SearchIcon";
import { SeeMoreHorizontalIcon } from "../resources/svgs/SeeMoreHorizontalIcon";
import { SeeMoreVerticalIcon } from "../resources/svgs/SeeMoreVerticalcon";
import { TextButton } from "components/reusables/text-button/TextButton";
import { TextField } from "../components/reusables/textfield/TextField";
import { TransactionRow } from "components/transactions/transactions-table/TransactionsTable";
import { connect } from "react-redux";
import { createStatusLabel } from "components/reusables/status-label/StatusLabelFactory";
import moment from "moment";
import { setString } from "./state/helloWorldActions";
import { transactionRowDef } from "components/reusables/list/row-definitions/transactionRowDef";

const mapStateToProps = (state: RootState) => ({
    helloWorldString: state.sandboxState.helloWorldString,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
    setString: (str: string) => dispatch(setString(str)),
});

type ConnectedSandboxProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;

interface SandboxState {
    text: string;
    pageNumber: number;
}

export const SANDBOX_PATH = "/sandbox";

const x = 24;

class Sandbox extends React.Component<ConnectedSandboxProps, SandboxState> {
    constructor(props: ConnectedSandboxProps) {
        super(props);
        this.state = {
            text: "",
            pageNumber: 1,
        };
    }

    private testObj: TransactionRow = {
        amount: "123",
        batchId: "batch1",
        id: "1",
        message: "message",
        product: "blah",
        status: "Cancelled",
        paymentDate: moment(),
        createdAt: moment().subtract(1, "day"),
        type: "Card",
    };

    private onClickPagination = (num: number) => {
        this.setState({ pageNumber: num });
    };

    private onClickButton = (): void => {
        this.props.setString("Hello James");
    };

    private onClickTextButton = (): void => {
        this.props.setString("Hello Benjamin");
    };

    private onChangeText = (str: string): void => {
        this.setState({ text: str });
    };

    render = () => (
        <div className="sandbox-parent-container">
            <span>{this.props.helloWorldString}</span>
            <div className="button-container">
                <Button display={"Primary"} isPrimary={true} onClick={this.onClickButton} />
                <Button display={"Secondary"} isPrimary={false} onClick={this.onClickButton} />
            </div>
            <div className="page-title">Page Title</div>

            <div className="header">Heading</div>

            <div className="data-table-result-number">1</div>

            <div className="data-table-col-header">Column header 1</div>

            <div className="general-font">Some text</div>
            <div className="svgs">
                <CloseIcon height={x} width={x} />
                <ArrowDown height={x} width={x} />
                <BlockIcon height={x} width={x} />
                <CancelIcon height={x} width={x} />
                <CheckIcon height={x} width={x} />
                <CompressIcon height={x} width={x} />
                <DateRangeIcon height={x} width={x} />
                <ErrorIcon height={x} width={x} />
                <ErrorOutlineIcon height={x} width={x} />
                <ExpandIcon height={x} width={x} />
                <InfoIcon height={x} width={x} />
                <KeyboardReturnIcon height={x} width={x} />
                <RemoveIcon height={x} width={x} />
                <SaveIcon height={x} width={x} />
                <SearchIcon height={x} width={x} />
                <SeeMoreHorizontalIcon height={x} width={x} />
                <SeeMoreVerticalIcon height={x} width={x} />
            </div>
            <div className="loader-buttons">
                <LoadingButton onClick={this.onClickTextButton} defaultText={"Submit"} loadingText={"Submitting"} />
                <LoadingButton onClick={this.onClickTextButton} defaultText={"Cancel"} loadingText={"Cancelling"} />
                <LoadingButton onClick={this.onClickTextButton} defaultText={"Filter"} loadingText={"Filtering"} />
                <LoadingButton onClick={this.onClickTextButton} defaultText={"Approve"} loadingText={"Approving"} />
            </div>
            <div className="textfields">
                <TextField
                    onChange={this.onChangeText}
                    label={"the Label"}
                    showLabel={true}
                    value={this.state.text}
                    placeholder="hello"
                />
                <TextField
                    onChange={this.props.setString}
                    label={"the Label"}
                    showLabel={true}
                    value={this.props.helloWorldString}
                />
                <TextField onChange={this.onChangeText} value={this.state.text} showLabel={false} label="no label" />
            </div>
            <div className="text-buttons">
                <TextButton
                    value={"Text1"}
                    onClick={this.onClickTextButton}
                    icon={<SaveIcon width={20} height={20} />}
                />
                <TextButton value={"Text2"} onClick={this.onClickTextButton} />
            </div>
            <div className="secondary-buttons">
                <RowActionButton selected={true} onClick={() => alert("Clicked")} />
                <RowActionButton selected={false} onClick={() => alert("Clicked")} />
            </div>
            <div className="pagination">
                <PaginationButton value={1} onClick={this.onClickPagination} selectedPage={this.state.pageNumber} />
                <PaginationButton value={2} onClick={this.onClickPagination} selectedPage={this.state.pageNumber} />
                <PaginationButton value={3} onClick={this.onClickPagination} selectedPage={this.state.pageNumber} />
                <PaginationButton value={4} onClick={this.onClickPagination} selectedPage={this.state.pageNumber} />
            </div>
            <div className="status-label">
                {createStatusLabel("Cancelled")}
                {createStatusLabel("Error")}
                {createStatusLabel("InProgress")}
                {createStatusLabel("Paid")}
                {createStatusLabel("AwaitingApproval")}
                {createStatusLabel("Returned")}
            </div>
            <div className="list">
                <List title={"Test List"} data={this.testObj} rows={transactionRowDef} />
            </div>
        </div>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(Sandbox);
