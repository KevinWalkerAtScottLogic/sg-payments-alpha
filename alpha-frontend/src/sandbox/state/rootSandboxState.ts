import { Reducer, combineReducers } from "redux";

import { helloWorldReducer } from "./helloWorld";

export interface SandboxState {
    helloWorldString: string;
}

export const sandboxReducer: Reducer<SandboxState> = combineReducers<SandboxState>({
    helloWorldString: helloWorldReducer,
});
