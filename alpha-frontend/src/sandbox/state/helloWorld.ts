import { Reducer } from "redux";
import { SET_STRING } from "./helloWorldActions";

export const helloWorldReducer: Reducer<string> = (state = "hello world", action) => {
    switch (action.type) {
        case SET_STRING:
            return action.payload;
        default:
            return state;
    }
};
