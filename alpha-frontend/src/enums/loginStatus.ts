export enum LoginStatus {
    loggedOut = "loggedOut",
    loggingIn = "loggingIn",
    loggedIn = "loggedIn",
}
