export type sortType = "asc" | "desc" | "none";

export interface Sort<T> {
    order: sortType;
    field: T;
}
