import { Action, AnyAction, Store, applyMiddleware, compose, createStore } from "redux";
import { AppDispatch, RootState, reducers } from "./rootReducer";
import thunk, { ThunkMiddleware } from "redux-thunk";

import { logger } from "redux-logger";

interface IReduxDevToolsExtendedWindow extends Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: typeof compose;
}

const storeEnhancers =
    ((window as unknown) as IReduxDevToolsExtendedWindow).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middlewares = [thunk as ThunkMiddleware<RootState, AnyAction, {}>];

if (process.env.NODE_ENV === "development") {
    middlewares.push(logger);
}

const store = createStore(reducers, storeEnhancers(applyMiddleware(...middlewares)));

export default store as Store<RootState, Action> & { dispatch: AppDispatch };
