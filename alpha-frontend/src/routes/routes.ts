export const LOGIN_PATH = "/login";
export const TRANSACTIONS_PATH = "/transactions";
export const SUBMISSIONS_PATH = "/submissions";

export const EXACT_TRANSACTION = TRANSACTIONS_PATH + "/:id";
