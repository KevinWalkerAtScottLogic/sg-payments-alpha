export const formatTransactionsNumber = (dataSize?: number): string =>
    `${dataSize || 0} transaction${dataSize === 1 ? "" : "s"}`;

export const formatTransactionPrice = (amount: string, currency: string): string => {
    if (currency === "GBP") {
        return `£${amount}`;
    } else {
        throw new Error(`${currency} is not supported for transactions. Only GBP is currently supported`);
    }
};
