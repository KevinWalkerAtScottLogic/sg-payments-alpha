import { DtoTransactions, mapTransactionsToState } from "./transactionsMapper";
import { createPageNumberQuery, createPageSizeQuery, createSortQuery } from "shared/utils/queryCreator";

import { DtoPage } from "shared/network/DtoPage";
import NetworkService from "shared/network/NetworkService";
import { ThunkResult } from "state/rootReducer";
import { TransactionsFilter } from "../transactions-filter/transactionsFilter";
import { constructTransactionFilterQuery } from "../transactions-filter/transactionsFilterMapper";

const transactionsUrl = "/transactions";
const transactionsUrlQuery = "/transactions/search";

export const SET_TRANSACTIONS_PAGE = "SET_TRANSACTIONS_PAGE";

const getUrl = (queryObject: TransactionsFilter): string => constructTransactionFilterQuery(queryObject) === null ? transactionsUrl : transactionsUrlQuery;

export const fetchTransactions = (queryObject: TransactionsFilter): ThunkResult<void> => dispatch => {
    NetworkService.get<DtoPage<DtoTransactions>>(getUrl(queryObject), {
        query: constructTransactionFilterQuery(queryObject),
        sort: createSortQuery(queryObject.sortBy),
        size: createPageSizeQuery(queryObject.pageSize),
        page: createPageNumberQuery(queryObject.pageNumber),
    }).then(json => {
        if (json) {
            dispatch({ type: SET_TRANSACTIONS_PAGE, payload: mapTransactionsToState(json) });
        }
    });
};
