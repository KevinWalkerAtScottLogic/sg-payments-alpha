import { DtoTransactions, mapTransactionsToState } from "./transactionsMapper";

import { DtoPage } from "shared/network/DtoPage";
import { Page } from "state/page";
import { TransactionRow } from "../transactions-table/TransactionsTable";
import moment from "moment";

describe("Given a transactions mapper", () => {
    const id1 = "1";
    const id2 = "2";
    const amount1 = "40.00";
    const amount2 = "60.00";
    const currency = "GBP";
    const status1 = "Returned";
    const status2 = "In progress";
    const message1 = "Message 1";
    const message2 = "message 2";
    const batchId1 = "batch1";
    const batchId2 = "batch2";
    const paymentDate1 = "2020-01-31T00:00:00.000000Z";
    const paymentDate2 = "2020-01-04T15:30:53.413794Z";
    const product1 = "product1";
    const product2 = "product2";
    const createdAt1 = "2020-01-31T00:00:00.000000Z";
    const createdAt2 = "2020-01-04T15:30:53.413794Z";
    const type1 = "EFT";
    const type2 = "Cheque";
    let dto: DtoPage<DtoTransactions>;

    beforeEach(() => {
        dto = {
            _embedded: {
                paymentSummaryList: [
                    {
                        id: id1,
                        amount: amount1,
                        currency: currency,
                        status: status1,
                        message: message1,
                        batchId: batchId1,
                        paymentDate: paymentDate1,
                        createdAt: createdAt1,
                        processingTime: "2020-01-31T00:00:00.000000Z",
                        type: type1,
                        product: product1,
                    },
                    {
                        id: id2,
                        amount: amount2,
                        currency: currency,
                        status: status2,
                        message: message2,
                        batchId: batchId2,
                        paymentDate: paymentDate2,
                        createdAt: createdAt2,
                        processingTime: "2020-01-31T00:00:00.000000Z",
                        type: type2,
                        product: product2,
                    },
                ],
            },
            page: {
                size: "2",
                totalElements: "2",
                totalPages: "75",
                number: "0",
            },
        };
    });

    describe("when mapTransactionsToState is called", () => {
        let result: Page<TransactionRow>;
        beforeEach(() => {
            result = mapTransactionsToState(dto);
        });

        it("then the page details are mapped", () => {
            expect(result.totalElements).toBe(2);
        });

        it("then the individual transaction details are mapped", () => {
            expect(result.content.length).toBe(2);
            const transaction1 = result.content[0];
            const transaction2 = result.content[1];

            // NOT TESTING FOR transaction product as this is not yet in the dto of backend
            expect(transaction1.id).toBe(id1);
            expect(transaction1.amount).toBe(`£${amount1}`);
            expect(transaction1.batchId).toBe(batchId1);
            expect(transaction1.message).toBe(message1);
            expect(transaction1.paymentDate).toEqual(moment(paymentDate1));
            expect(transaction1.status).toBe(status1);
            expect(transaction1.type).toBe(type1);
            expect(transaction1.createdAt).toEqual(moment(createdAt1));
            expect(transaction1.product).toBe(product1);

            expect(transaction2.id).toBe(id2);
            expect(transaction2.amount).toBe(`£${amount2}`);
            expect(transaction2.batchId).toBe(batchId2);
            expect(transaction2.message).toBe(message2);
            expect(transaction2.paymentDate).toEqual(moment(paymentDate2));
            expect(transaction2.status).toBe(status2);
            expect(transaction2.type).toBe(type2);
            expect(transaction2.createdAt).toEqual(moment(createdAt2));
            expect(transaction2.product).toBe(product2);
        });
    });
});
