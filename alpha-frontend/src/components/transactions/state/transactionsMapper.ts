import { DtoPage } from "shared/network/DtoPage";
import { Page } from "state/page";
import { Status } from "components/reusables/status-label/StatusLabel";
import { TransactionRow } from "../transactions-table/TransactionsTable";
import { formatTransactionPrice } from "../transactionsFormatters";
import moment from "moment";

export type TransactionType = "EFT" | "Cheque" | "PostalOrder" | "Card";

interface DtoTransaction {
    id: string;
    batchId: string;
    createdAt: string;
    status: Status;
    message: string;
    amount: string;
    currency: string;
    paymentDate: string;
    processingTime: string;
    type: TransactionType;
    product: string;
}

export interface DtoTransactions {
    paymentSummaryList: DtoTransaction[];
}

const mapTransaction = (dto: DtoTransaction): TransactionRow => ({
    id: dto.id,
    amount: formatTransactionPrice(dto.amount, dto.currency),
    batchId: dto.batchId,
    message: dto.message,
    paymentDate: moment(dto.paymentDate),
    createdAt: moment(dto.createdAt),
    product: dto.product,
    status: dto.status,
    type: dto.type,
});

export const mapTransactionsToState = (dto: DtoPage<DtoTransactions>): Page<TransactionRow> => ({
    content: dto._embedded ? dto._embedded.paymentSummaryList.map(mapTransaction) : [],
    totalElements: parseInt(dto.page.totalElements),
    pageNumber: parseInt(dto.page.number),
    pageSize: parseInt(dto.page.size),
    totalPages: parseInt(dto.page.totalPages),
});
