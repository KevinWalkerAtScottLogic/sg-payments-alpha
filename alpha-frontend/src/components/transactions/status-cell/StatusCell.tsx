import { CellRendererParams } from "components/reusables/table/Table";
import React from "react";
import { TransactionRow } from "../transactions-table/TransactionsTable";
import { createStatusLabel } from "components/reusables/status-label/StatusLabelFactory";

class StatusCell extends React.Component<CellRendererParams<TransactionRow>> {
    render = () => {
        return <div>{createStatusLabel(this.props.value)}</div>;
    };
}

export default StatusCell;
