import { formatTransactionPrice, formatTransactionsNumber } from "./transactionsFormatters";

describe("Given a call to format Transaction Number function", () => {
    let resultCount: number | undefined;

    describe("when the number of transactions is undefined", () => {
        it("it returns '0 transactions'", () => {
            expect(formatTransactionsNumber(resultCount)).toBe("0 transactions");
        });
    });

    describe("when the number of transaction is 1", () => {
        beforeEach(() => {
            resultCount = 1;
        });

        it("it returns '1 transaction'", () => {
            expect(formatTransactionsNumber(resultCount)).toBe("1 transaction");
        });
    });

    describe("when the number of transactions is more than 1", () => {
        beforeEach(() => {
            resultCount = 2;
        });

        it(`it returns "${resultCount} transactions"`, () => {
            expect(formatTransactionsNumber(resultCount)).toBe(`${resultCount} transactions`);
        });
    });
});

describe("Given a call to the format Transaction Price function", () => {
    const amount = "50.00";
    let currency: string;

    describe("when called with GBP as a currency", () => {
        beforeEach(() => {
            currency = "GBP";
        });

        it('it returns "£50.00"', () => {
            expect(formatTransactionPrice(amount, currency)).toBe("£50.00");
        });
    });

    describe("when called with a currency that is not GBP", () => {
        beforeEach(() => {
            currency = "EUR";
        });

        it("it throws an error", () => {
            const t = () => {
                formatTransactionPrice(amount, currency);
            };
            expect(t).toThrowError();
        });
    });
});
