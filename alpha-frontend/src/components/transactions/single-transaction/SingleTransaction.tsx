import React from "react";
import { RouteComponentProps } from "react-router-dom";

interface SingleTransactionProps {
    id?: string;
}

export class SingleTransaction extends React.Component<RouteComponentProps<SingleTransactionProps>> {
    render = () => (
        <div className="single-transaction-parent-container">
            This is a single Transaction for {this.props.match.params.id}
        </div>
    );
}
