import { PagingFilter } from "components/paging/pagingFilter";
import { Sort } from "enums/sorting";
import { Status } from "components/reusables/status-label/StatusLabel";
import { TransactionColumnFields } from "../transactions-table/TransactionsTable";

export interface BatchFilter {
    id: string;
    name: string;
}

// When adding filters, extend PagingFilter interface
export interface TransactionsFilter extends PagingFilter {
    exactBatch: BatchFilter | null;
    statuses: Status[];
    sortBy: Sort<TransactionColumnFields>;
}
