import {
    REMOVE_EXACT_BATCH_ID,
    REMOVE_TRANSACTION_STATUS,
    SET_TRANSACTIONS_FILTER,
    SET_TRANSACTIONS_SORT,
} from "./transactionFilterActions";

import { Reducer } from "redux";
import { TransactionsFilter } from "../transactionsFilter";

const initialState: TransactionsFilter = {
    exactBatch: null,
    statuses: [],
    sortBy: { order: "asc", field: "paymentDate" },
};

export const transactionsFilterReducer: Reducer<TransactionsFilter> = (state = initialState, action) => {
    switch (action.type) {
        case REMOVE_EXACT_BATCH_ID:
            return {
                ...state,
                exactBatch: null,
            };
        case REMOVE_TRANSACTION_STATUS:
            return {
                ...state,
                statuses: state.statuses.filter(status => status !== action.payload),
            };
        case SET_TRANSACTIONS_SORT:
            return {
                ...state,
                sortBy: action.payload,
            };
        case SET_TRANSACTIONS_FILTER:
            return action.payload;
        default:
            return state;
    }
};
