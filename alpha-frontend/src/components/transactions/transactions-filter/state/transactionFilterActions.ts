import { Sort } from "enums/sorting";
import { Status } from "components/reusables/status-label/StatusLabel";
import { ThunkResult } from "state/rootReducer";
import { TransactionColumnFields } from "components/transactions/transactions-table/TransactionsTable";
import { TransactionsFilter } from "../transactionsFilter";

export const REMOVE_EXACT_BATCH_ID = "REMOVE_EXACT_BATCH_ID";
export const SET_TRANSACTIONS_SORT = "SET_TRANSACTIONS_SORT";
export const SET_TRANSACTIONS_FILTER = "SET_TRANSACTIONS_FILTER";
export const REMOVE_TRANSACTION_STATUS = "REMOVE_TRANSACTION_STATUS";

const setTransactionsFilter = (payload: TransactionsFilter): ThunkResult<void> => dispatch =>
    dispatch({ type: SET_TRANSACTIONS_FILTER, payload });

export const setExactBatchFilter = (batchId: string, batchName: string, status?: Status): ThunkResult<void> =>
    setTransactionsFilter({
        exactBatch: { id: batchId, name: batchName },
        statuses: status ? [status] : [],
        sortBy: { order: "asc", field: "paymentDate" },
    });

export const setTransactionsSort = (sort: Sort<TransactionColumnFields>): ThunkResult<void> => dispatch =>
    dispatch({ type: SET_TRANSACTIONS_SORT, payload: sort });

export const removeExactBatchId = (): ThunkResult<void> => dispatch => dispatch({ type: REMOVE_EXACT_BATCH_ID });

export const removeTransactionStatus = (status: Status): ThunkResult<void> => dispatch =>
    dispatch({ type: REMOVE_TRANSACTION_STATUS, payload: status });
