import { Sort } from "enums/sorting";
import { TransactionColumnFields } from "../transactions-table/TransactionsTable";
import { TransactionsFilter } from "./transactionsFilter";
import { constructTransactionFilterQuery } from "./transactionsFilterMapper";

describe("Given the transaction query constructor", () => {
    const batchId1 = "id-number-1";
    const sort: Sort<TransactionColumnFields> = { order: "none", field: "amount" };
    let queryObject: TransactionsFilter;

    describe("when filtering by one batchId parameter", () => {
        beforeEach(() => {
            queryObject = { exactBatch: { id: batchId1, name: "test-name" }, statuses: [], sortBy: sort };
        });

        it("then it returns the correctly formatted string", () => {
            expect(constructTransactionFilterQuery(queryObject)).toBe(`batchId==${batchId1}`);
        });
    });

    describe("when a status is provided", () => {
        beforeEach(() => {
            queryObject = { exactBatch: null, statuses: ["Error"], sortBy: sort };
        });

        it("then it returns the correctly formatted string", () => {
            expect(constructTransactionFilterQuery(queryObject)).toBe(`status=in=(Error)`);
        });
    });

    describe("when multiple statuses are provided", () => {
        beforeEach(() => {
            queryObject = { exactBatch: null, statuses: ["Error", "Pending approval"], sortBy: sort };
        });

        it("then it returns the correctly formatted string", () => {
            expect(constructTransactionFilterQuery(queryObject)).toBe("status=in=(Error,Pending approval)");
        });
    });

    describe("when multiplefilter types are provided", () => {
        beforeEach(() => {
            queryObject = {
                exactBatch: { id: batchId1, name: "test-name" },
                statuses: ["Error", "Pending approval"],
                sortBy: sort,
            };
        });

        it("then it returns the correctly formatted string", () => {
            expect(constructTransactionFilterQuery(queryObject)).toBe(
                "batchId==id-number-1;status=in=(Error,Pending approval)",
            );
        });
    });
});
