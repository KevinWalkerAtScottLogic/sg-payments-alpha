import { ShallowWrapper, shallow } from "enzyme";

import { PaginationButton } from "./PaginationButton";
import React from "react";

describe("Given a pagination button", () => {
    const pageValue = 1;
    let jestFuntion: jest.Mock;
    let wrapper: ShallowWrapper;

    beforeEach(() => {
        jestFuntion = jest.fn();
        wrapper = shallow(<PaginationButton value={pageValue} onClick={jestFuntion} selectedPage={pageValue} />);
    });

    describe("that is not selected", () => {
        beforeEach(() => {
            wrapper.setProps({ selectedPage: 2 });
        });

        describe("when the button is clicked", () => {
            beforeEach(() => {
                wrapper.find(".pagination-button").simulate("click");
            });

            it("then it calls the function passed as props", () => {
                expect(jestFuntion).toBeCalledWith(pageValue);
            });
        });
    });

    describe("that is selected", () => {
        beforeEach(() => {
            wrapper.setProps({ selectedPage: pageValue });
        });

        describe("when the button is clicked", () => {
            beforeEach(() => {
                wrapper.find(".pagination-button").simulate("click");
            });

            it("then it does not call the callback function", () => {
                expect(jestFuntion).not.toBeCalled();
            });
        });
    });
});
