import "./TextButton.scss";

import React from "react";

interface TextButtonProps {
    value: string;
    onClick: () => void;
    icon?: React.ReactNode;
}

export class TextButton extends React.Component<TextButtonProps> {
    render = () => (
        <div className="text-button-parent-container">
            {this.props.icon && <div>{this.props.icon}</div>}
            <button className="text-button" onClick={this.props.onClick} aria-label={this.props.value}>
                {this.props.value}
            </button>
        </div>
    );
}
