import "./TextField.scss";

import { CancelIcon } from "resources/svgs/CancelIcon";
import React from "react";

interface TextfieldProps {
    onChange: (str: string) => void;
    value: string;
    label: string;
    showLabel: boolean;
    placeholder?: string;
}

export class TextField extends React.Component<TextfieldProps> {
    private inputRef: React.RefObject<HTMLInputElement>;

    constructor(props: TextfieldProps) {
        super(props);
        this.inputRef = React.createRef();
    }

    private onChange = (event: React.FormEvent<HTMLInputElement>) => {
        this.props.onChange(event.currentTarget.value);
    };

    private resetText = () => {
        this.inputRef.current?.focus();
        this.props.onChange("");
    };

    render = () => (
        <div className="textfield-parent-container">
            <label htmlFor={`textfield for ${this.props.label}`}>
                <span className={`${this.props.showLabel ? "visible-label" : "hidden-label"}`}>{this.props.label}</span>
            </label>
            <div className="input-and-icon">
                <input
                    id={`textfield for ${this.props.label}`}
                    className="text-input"
                    placeholder={this.props.placeholder}
                    onChange={this.onChange}
                    value={this.props.value}
                    ref={this.inputRef}
                />
                {this.props.value && (
                    <button
                        onClick={this.resetText}
                        className="svg-button"
                        aria-label={`Clear ${this.props.label} field`}
                    >
                        <CancelIcon height={20} width={20} />
                    </button>
                )}
            </div>
        </div>
    );
}
