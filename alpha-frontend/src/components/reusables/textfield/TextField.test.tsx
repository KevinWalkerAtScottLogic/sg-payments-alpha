import { ShallowWrapper, shallow } from "enzyme";

import React from "react";
import { TextField } from "./TextField";

describe("Given a text field", () => {
    const jestFunction: jest.Mock = jest.fn();
    let wrapper: ShallowWrapper;

    beforeEach(() => {
        wrapper = shallow(
            <TextField onChange={jestFunction} value="the string to change" label="test label" showLabel={true} />,
        );
    });

    describe("when the value changes", () => {
        const strInput = "test";
        beforeEach(() => {
            wrapper.find(".text-input").simulate("change", { currentTarget: { value: strInput } });
        });

        it("then the on change callback is called with the new value", () => {
            expect(jestFunction).toBeCalledWith(strInput);
        });
    });

    describe("when the reset button is clicked", () => {
        beforeEach(() => {
            wrapper.find(".svg-button").simulate("click", { currentTarget: { value: "test" } });
        });

        it("then the on change callback is called with an empty string", () => {
            expect(jestFunction).toBeCalledWith("");
        });
    });
});
