import "./RowActionButton.scss";

import React from "react";
import { SeeMoreVerticalIcon } from "resources/svgs/SeeMoreVerticalcon";

interface RowActionButtonProps {
    selected: boolean;
    onClick: () => void;
}

export class RowActionButton extends React.Component<RowActionButtonProps> {
    render = () => (
        <button
            className={`row-action-button ${
                this.props.selected ? "row-action-button-default" : "row-action-button-selected"
            }`}
            onClick={this.props.onClick}
            aria-label={`Show row actions`}
        >
            <SeeMoreVerticalIcon width={24} height={24} />
        </button>
    );
}
