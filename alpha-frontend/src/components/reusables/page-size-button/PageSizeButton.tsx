import "./PageSizeButton.scss";

import React from "react";
import { TextButton } from "../text-button/TextButton";

interface PageSizeButtonProps {
    value: number;
    isSelected: boolean;
    onChangePageSize: (pageNum: number) => void;
}

export class PageSizeButton extends React.Component<PageSizeButtonProps> {
    private changePageSize = () => {
        this.props.onChangePageSize(this.props.value);
    };

    render = () =>
        this.props.isSelected ? (
            <span className="paging-options-text">{`${this.props.value}`}</span>
        ) : (
            <span className="paging-text-button">
                <TextButton value={`${this.props.value}`} onClick={this.changePageSize} />
            </span>
        );
}
