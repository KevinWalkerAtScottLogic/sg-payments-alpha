import { ShallowWrapper, shallow } from "enzyme";

import { CompressIcon } from "resources/svgs/CompressIcon";
import { ExpandIcon } from "resources/svgs/ExpandIcon";
import React from "react";
import { Sort } from "enums/sorting";
import { SortableHeader } from "./SortableHeader";

describe("Given the CustomHeader component", () => {
    const name = "Test custom header";
    const field = "Test field";
    const currentSort: Sort<unknown> = { order: "asc", field: "unselected" };
    let wrapper: ShallowWrapper;
    let mockSort: jest.Mock;
    describe("that is not currently selected", () => {
        beforeEach(() => {
            mockSort = jest.fn();
            wrapper = shallow(
                <SortableHeader name={name} field={field} sort={mockSort} getCurrentSorting={() => currentSort} />,
            );
        });

        it("the sorting svg is not rendered", () => {
            expect(wrapper.find(CompressIcon)).toHaveLength(0);
            expect(wrapper.find(ExpandIcon)).toHaveLength(0);
        });

        it("calls the props function when clicked", () => {
            wrapper.find(".custom-header-parent-container").simulate("click");
            expect(mockSort).toBeCalledTimes(1);
        });
    });

    describe("that is currently selected in asc order", () => {
        beforeEach(() => {
            mockSort = jest.fn();
            currentSort.field = field;
            wrapper = shallow(
                <SortableHeader name={name} field={field} sort={mockSort} getCurrentSorting={() => currentSort} />,
            );
        });

        it("the asc sorting svg is not rendered", () => {
            expect(wrapper.find(CompressIcon)).toHaveLength(1);
        });
    });

    describe("that is currently selected in desc order", () => {
        beforeEach(() => {
            mockSort = jest.fn();
            currentSort.field = field;
            currentSort.order = "desc";
            wrapper = shallow(
                <SortableHeader name={name} field={field} sort={mockSort} getCurrentSorting={() => currentSort} />,
            );
        });

        it("the asc sorting svg is not rendered", () => {
            expect(wrapper.find(ExpandIcon)).toHaveLength(1);
        });
    });
});
