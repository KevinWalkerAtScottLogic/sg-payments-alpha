import "./SortableHeader.scss";

import { Sort, sortType } from "enums/sorting";

import { CompressIcon } from "resources/svgs/CompressIcon";
import { ExpandIcon } from "resources/svgs/ExpandIcon";
import React from "react";

interface OrderVariables {
    nextOrder: sortType;
    ariaLabel: string;
    icon: JSX.Element;
}

export interface SortableHeaderProps<K> {
    name: string;
    field: K;
    sort: (sort: Sort<K>) => void;
    getCurrentSorting: () => Sort<K>;
}

export class SortableHeader<K> extends React.Component<SortableHeaderProps<K>> {
    private getOrderLabel = (order: sortType): OrderVariables => {
        switch (order) {
            case "asc":
                return {
                    nextOrder: "desc",
                    ariaLabel: "in ascending order, press to apply desceding order",
                    icon: <CompressIcon width={24} height={24} className="svg-styling" />,
                };
            case "desc":
                return {
                    nextOrder: "none",
                    ariaLabel: "in descending order, press to not order",
                    icon: <ExpandIcon width={24} height={24} className="svg-styling" />,
                };
            default:
                return { nextOrder: "asc", ariaLabel: "not ordered, press to apply ascending order", icon: <div /> };
        }
    };

    private onClick = (): void => {
        const currentSort = this.props.getCurrentSorting();
        if (currentSort.field === this.props.field) {
            this.props.sort({
                order: this.getOrderLabel(currentSort.order).nextOrder,
                field: this.props.field,
            });
        } else {
            this.props.sort({ order: "asc", field: this.props.field });
        }
    };

    render = () => {
        const currentSort = this.props.getCurrentSorting();
        const label = this.getOrderLabel(currentSort.order);
        return (
            <button
                className="custom-header-parent-container"
                onClick={this.onClick}
                aria-label={`${this.props.name} column ${label.ariaLabel}`}
            >
                <span>{this.props.name}</span>
                {currentSort.field === this.props.field && label.icon}
            </button>
        );
    };
}

export default SortableHeader;
