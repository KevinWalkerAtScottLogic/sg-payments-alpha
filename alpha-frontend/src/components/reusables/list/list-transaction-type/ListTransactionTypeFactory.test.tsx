import { ShallowWrapper, shallow } from "enzyme";

import { TransactionType } from "components/transactions/state/transactionsMapper";
import { createTransactionType } from "./ListTransactionTypeFactory";

describe("Given the transaction type factory", () => {
    let wrapper: ShallowWrapper;
    let type: TransactionType;

    describe("when given the Card transaction type as parameter", () => {
        beforeEach(() => {
            type = "Card";
            wrapper = shallow(createTransactionType(type));
        });

        it("it contains the correct type", () => {
            expect(wrapper.text()).toContain(type);
        });

        it("it assigns Card payment as div title", () => {
            expect(wrapper.find(".info-icon").prop("title")).toBe("Card payment");
        });
    });

    describe("when given the Card transaction type as parameter", () => {
        beforeEach(() => {
            type = "Cheque";
            wrapper = shallow(createTransactionType(type));
        });

        it("it contains the correct type", () => {
            expect(wrapper.text()).toContain(type);
        });

        it("it assigns Card payment as div title", () => {
            expect(wrapper.find(".info-icon").prop("title")).toBe("Cheque payment");
        });
    });

    describe("when given the Card transaction type as parameter", () => {
        beforeEach(() => {
            type = "EFT";
            wrapper = shallow(createTransactionType(type));
        });

        it("it contains the correct type", () => {
            expect(wrapper.text()).toContain(type);
        });

        it("it assigns Card payment as div title", () => {
            expect(wrapper.find(".info-icon").prop("title")).toBe("Electronic funds transfer e.g. BACS");
        });
    });

    describe("when given the Card transaction type as parameter", () => {
        beforeEach(() => {
            type = "PostalOrder";
            wrapper = shallow(createTransactionType(type));
        });

        it("it contains the correct type", () => {
            expect(wrapper.text()).toContain(type);
        });

        it("it assigns Card payment as div title", () => {
            expect(wrapper.find(".info-icon").prop("title")).toBe("Postal payment");
        });
    });
});
