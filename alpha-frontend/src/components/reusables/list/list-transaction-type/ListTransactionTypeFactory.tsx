import { ListTransactionType } from "./ListTransactionType";
import React from "react";
import { TransactionType } from "components/transactions/state/transactionsMapper";

const getTransactionTypeTooltip = (type: TransactionType): string => {
    switch (type) {
        case "Card":
            return "Card payment";
        case "Cheque":
            return "Cheque payment";
        case "EFT":
            return "Electronic funds transfer e.g. BACS";
        case "PostalOrder":
            return "Postal payment";
    }
};

export const createTransactionType = (type: TransactionType): JSX.Element => (
    <ListTransactionType type={type} titleContent={getTransactionTypeTooltip(type)} />
);
