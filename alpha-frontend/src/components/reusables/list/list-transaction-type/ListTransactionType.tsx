import "./ListTransactionType.scss";

import { InfoIcon } from "resources/svgs/InfoIcon";
import React from "react";
import { TransactionType } from "components/transactions/state/transactionsMapper";

interface TransactionTypeProps {
    type: TransactionType;
    titleContent: string;
}

export class ListTransactionType extends React.Component<TransactionTypeProps> {
    render = () => (
        <div className="transaction-type-parent-container">
            <span className="transaction-type">{this.props.type}</span>
            <div className="info-icon" title={this.props.titleContent} tabIndex={0}>
                <InfoIcon height={24} width={24} />
            </div>
        </div>
    );
}
