import { RowDef } from "../List";
import { TransactionRow } from "components/transactions/transactions-table/TransactionsTable";
import { createStatusLabel } from "components/reusables/status-label/StatusLabelFactory";
import { createTransactionType } from "../list-transaction-type/ListTransactionTypeFactory";
import { formatDateTime } from "shared/utils/formatters";

export const transactionRowDef: RowDef<TransactionRow>[] = [
    {
        key: "Transaction ID",
        extractContent: (data: TransactionRow) => data.id,
    },
    {
        key: "Batch ID",
        extractContent: (data: TransactionRow) => data.batchId,
    },
    {
        key: "Date Created",
        extractContent: (data: TransactionRow) => formatDateTime(data.createdAt),
    },
    {
        key: "Payment Date",
        extractContent: (data: TransactionRow) => formatDateTime(data.paymentDate),
    },
    {
        key: "Product",
        extractContent: (data: TransactionRow) => data.product,
    },
    {
        key: "Amount",
        extractContent: (data: TransactionRow) => data.amount,
    },
    {
        key: "Type",
        extractContent: (data: TransactionRow) => createTransactionType(data.type),
    },
    {
        key: "Status",
        extractContent: (data: TransactionRow) => createStatusLabel(data.status),
    },
];
