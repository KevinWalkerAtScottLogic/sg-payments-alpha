import "./Pill.scss";

import { CloseIcon } from "resources/svgs/CloseIcon";
import React from "react";

interface PillProps<T> {
    content: string;
    dataToRemove: T;
    onClick: (data: T) => void;
}

export class Pill<T> extends React.Component<PillProps<T>> {
    private buttonClick = () => {
        this.props.onClick(this.props.dataToRemove);
    };

    render = () => (
        <div className="pill-parent-container">
            <span className="content">{this.props.content}</span>
            <button
                onClick={this.buttonClick}
                className="svg-button"
                tabIndex={0}
                aria-label={`Remove filter ${this.props.content}`}
            >
                <CloseIcon height={24} width={24} />
            </button>
        </div>
    );
}
