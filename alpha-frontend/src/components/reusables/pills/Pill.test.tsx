import { ShallowWrapper, shallow } from "enzyme";

import { Pill } from "./Pill";
import React from "react";

describe("Given the Pill component", () => {
    let wrapper: ShallowWrapper;
    let mockOnClick: jest.Mock;
    const dataToRemove = {};

    beforeEach(() => {
        mockOnClick = jest.fn();
        wrapper = shallow(<Pill content={""} dataToRemove={dataToRemove} onClick={mockOnClick} />);
    });

    it("When clicked, the props function is called with the correct arguments", () => {
        wrapper.find(".svg-button").simulate("click");
        expect(mockOnClick).toHaveBeenCalledWith(dataToRemove);
    });
});
