import "./Home.scss";

import { AppDispatch, RootState } from "state/rootReducer";
import { EXACT_TRANSACTION, LOGIN_PATH, SUBMISSIONS_PATH, TRANSACTIONS_PATH } from "routes/routes";
import { Redirect, Route, RouteComponentProps, Switch, withRouter } from "react-router-dom";
import { setLoggedIn, setLoggedOut, setLoggingIn } from "components/login/state/statusAction";

import { Footer } from "components/footer/Footer";
import Header from "components/header/Header";
import { LoggedIn } from "components/logged-in/LoggedIn";
import Login from "components/login/Login";
import { LoginStatus } from "enums/loginStatus";
import React from "react";
import { SANDBOX_PATH } from "sandbox/Sandbox";
import { connect } from "react-redux";
import { handleAutoLogin } from "components/login/login-service/loginService";

const mapStateToProps = (state: RootState) => ({
    loginStatus: state.loginState.status,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
    setLoggingIn: () => dispatch(setLoggingIn()),
    setLoggedIn: (idToken: string, accessToken: string) => dispatch(setLoggedIn(idToken, accessToken)),
    setLoggedOut: () => dispatch(setLoggedOut()),
});

type ConnectedHomeProps = ReturnType<typeof mapStateToProps> &
    ReturnType<typeof mapDispatchToProps> &
    RouteComponentProps;

class Home extends React.Component<ConnectedHomeProps> {
    componentDidMount = () => {
        handleAutoLogin(
            this.props.location.pathname,
            this.props.setLoggingIn,
            this.props.setLoggedIn,
            this.props.setLoggedOut,
        );
    };

    componentDidUpdate = (oldProps: ConnectedHomeProps) => {
        if (oldProps.loginStatus !== this.props.loginStatus && this.props.loginStatus === LoginStatus.loggedIn) {
            this.props.history.push(SUBMISSIONS_PATH);
        }
    };

    render = () => (
        <div className="home-parent-container">
            <Header />
            <div className="page-body">
                <Switch>
                    <Route
                        exact
                        path={LOGIN_PATH}
                        render={() =>
                            this.props.loginStatus !== LoginStatus.loggedIn ? (
                                <Login />
                            ) : (
                                <Redirect to={SUBMISSIONS_PATH} />
                            )
                        }
                    />
                    <Route path="/id_token=*" component={Login} />
                    <Route
                        exact
                        path={[SUBMISSIONS_PATH, TRANSACTIONS_PATH, EXACT_TRANSACTION]}
                        render={() =>
                            this.props.loginStatus === LoginStatus.loggedIn ? (
                                <LoggedIn />
                            ) : (
                                <Redirect to={LOGIN_PATH} />
                            )
                        }
                    />

                    {/*TO DELETE */}
                    <Route exact path={SANDBOX_PATH} component={LoggedIn} />
                </Switch>
            </div>
            <Footer />
        </div>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Home));
