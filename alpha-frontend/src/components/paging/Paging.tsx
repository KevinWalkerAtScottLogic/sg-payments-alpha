import "./Paging.scss";

import { shouldRenderPagingButtons, shouldRenderPagingSize } from "./pageUtils";

import { Page } from "state/page";
import { PagingButtons } from "./page-buttons/PagingButtons";
import { PagingSize } from "./page-size/PagingSize";
import React from "react";

/*This array needs to be sorted with the smallest element first*/
const pageSizes: number[] = [50, 100];

interface PagingProps {
    pageProperty: Page<unknown>;
    changePageSize: (size: number) => void;
    changePageNumber: (pageNumber: number) => void;
}

export class Paging extends React.Component<PagingProps> {
    render = () => (
        <div className="paging-parent-container">
            {shouldRenderPagingSize(pageSizes[0], this.props.pageProperty.totalElements) && (
                <PagingSize
                    changePageSize={this.props.changePageSize}
                    pageSizeOptions={pageSizes}
                    pageSizeSelected={this.props.pageProperty.pageSize}
                />
            )}
            {shouldRenderPagingButtons(this.props.pageProperty.totalPages) && (
                <PagingButtons
                    changePageNumber={this.props.changePageNumber}
                    totalPages={this.props.pageProperty.totalPages}
                    pageSelected={this.props.pageProperty.pageNumber}
                />
            )}
        </div>
    );
}
