import { createPagingQuery, getNewPageNumber, shouldRenderPagingButtons, shouldRenderPagingSize } from "./pageUtils";

import { Page } from "state/page";
import { PagingFilter } from "./pagingFilter";

const createEmptyPage = (): Page<unknown> => ({
    content: [],
    pageNumber: 0,
    pageSize: 0,
    totalElements: 0,
    totalPages: 0,
});

describe("Given the getNewPageNumber method", () => {
    const page: Page<unknown> = createEmptyPage();
    let newPageSize: number;
    let expectedPageNum: number;

    describe("When going from 50 to 100 page size on an odd page number", () => {
        beforeEach(() => {
            page.pageSize = 50;
            page.pageNumber = 3;
            newPageSize = 100;
        });

        it("The correct page number is calculated", () => {
            expectedPageNum = 2;
            expect(getNewPageNumber(page, newPageSize)).toBe(expectedPageNum);
        });
    });

    describe("When going from 50 to 100 page size on an even page number", () => {
        beforeEach(() => {
            page.pageSize = 50;
            page.pageNumber = 6;
            newPageSize = 100;
        });

        it("The correct page number is calculated", () => {
            expectedPageNum = 3;
            expect(getNewPageNumber(page, newPageSize)).toBe(expectedPageNum);
        });
    });

    describe("When going from 100 to 50 page size on an odd page number", () => {
        beforeEach(() => {
            page.pageSize = 100;
            page.pageNumber = 5;
            newPageSize = 50;
        });

        it("The correct page number is calculated", () => {
            expectedPageNum = 9;
            expect(getNewPageNumber(page, newPageSize)).toBe(expectedPageNum);
        });
    });

    describe("When going from 100 to 50 page size on an even page number", () => {
        beforeEach(() => {
            page.pageSize = 100;
            page.pageNumber = 4;
            newPageSize = 50;
        });

        it("The correct page number is calculated", () => {
            expectedPageNum = 7;
            expect(getNewPageNumber(page, newPageSize)).toBe(expectedPageNum);
        });
    });
});

describe("Given the shouldRenderPagingSize function", () => {
    const page: Page<unknown> = createEmptyPage();
    const minimumPageSize = 50;
    describe("When the page has less than 51 elements", () => {
        beforeEach(() => {
            page.totalElements = 50;
        });

        it("The page size will not be rendered", () => {
            expect(shouldRenderPagingSize(minimumPageSize, page.totalElements)).toBe(false);
        });
    });

    describe("When the page has more than 50 elements", () => {
        beforeEach(() => {
            page.totalElements = 60;
        });

        it("The page component will be rendered", () => {
            expect(shouldRenderPagingSize(minimumPageSize, page.totalElements)).toBe(true);
        });
    });
});

describe("Given the shouldRenderPagingButtons function", () => {
    const page: Page<unknown> = createEmptyPage();
    describe("When there are more than 1 total pages", () => {
        beforeEach(() => {
            page.totalPages = 2;
        });

        it("The page buttons will be rendered", () => {
            expect(shouldRenderPagingButtons(page.totalPages)).toBe(true);
        });
    });

    describe("When the page has 1 page", () => {
        beforeEach(() => {
            page.totalPages = 1;
        });

        it("The page component will be rendered", () => {
            expect(shouldRenderPagingButtons(page.totalPages)).toBe(false);
        });
    });
});
