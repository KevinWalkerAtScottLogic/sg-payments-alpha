import "./PagingSize.scss";

import React, { Fragment } from "react";

import { PageSizeButton } from "components/reusables/page-size-button/PageSizeButton";

interface PagingSizeProps {
    pageSizeSelected: number;
    pageSizeOptions: number[];
    changePageSize: (size: number) => void;
}

export class PagingSize extends React.Component<PagingSizeProps> {
    render = () => (
        <div className="paging-options-parent-container">
            <span className="paging-options-text">Results per page:</span>
            <div className="paging-options-buttons">
                {this.props.pageSizeOptions.map(size => (
                    <Fragment key={size}>
                        <PageSizeButton
                            value={size}
                            isSelected={size === this.props.pageSizeSelected}
                            onChangePageSize={this.props.changePageSize}
                        />
                        <hr className="vertical-line" />
                    </Fragment>
                ))}
            </div>
        </div>
    );
}
