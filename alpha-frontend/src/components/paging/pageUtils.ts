import { Page } from "state/page";

export const getNewPageNumber = <T>(page: Page<T>, newPageSize: number): number =>
    Math.floor((page.pageSize * (page.pageNumber - 1)) / newPageSize + 1);

export const shouldRenderPagingSize = (minPageSize: number, totalElements: number) => totalElements > minPageSize;

export const shouldRenderPagingButtons = (totalPages: number) => totalPages > 1;
