import { ShallowWrapper, shallow } from "enzyme";

import { Page } from "state/page";
import { Paging } from "./Paging";
import { PagingButtons } from "./page-buttons/PagingButtons";
import React from "react";

describe("Given the Paging component", () => {
    let wrapper: ShallowWrapper;
    let pageProperty: Page<unknown>;

    describe("When there is only one total page", () => {
        beforeEach(() => {
            pageProperty = { pageSize: 100, totalElements: 100, pageNumber: 1, totalPages: 1 };
            wrapper = shallow(<Paging pageProperty={pageProperty} />);
        });

        it("Then the PageButtons component is not rendered", () => {
            expect(wrapper.find(PagingButtons)).toHaveLength(0);
        });
    });

    describe("When there are 2 total pages", () => {
        beforeEach(() => {
            pageProperty = { pageSize: 100, totalElements: 101, pageNumber: 1, totalPages: 2 };
            wrapper = shallow(<Paging pageProperty={pageProperty} />);
        });

        it("Then the PageButtons component is rendered", () => {
            expect(wrapper.find(PagingButtons)).toHaveLength(1);
        });
    });
});
