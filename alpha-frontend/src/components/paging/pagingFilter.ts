export interface PagingFilter {
    pageSize?: number;
    pageNumber?: number;
}
