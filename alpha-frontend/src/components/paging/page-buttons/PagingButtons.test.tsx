import { ShallowWrapper, shallow } from "enzyme";

import { PaginationButton } from "components/reusables/pagination-button/PaginationButton";
import { PagingButtons } from "./PagingButtons";
import React from "react";
import { TextButton } from "components/reusables/text-button/TextButton";

describe("Given the Paging Button component", () => {
    let pageSelected: number, totalPages: number;
    let changePageNumber: jest.Mock;
    let wrapper: ShallowWrapper;

    describe("When the user is on the first page", () => {
        beforeEach(() => {
            pageSelected = 0;
        });

        describe("When there is only one page", () => {
            beforeEach(() => {
                totalPages = 1;
                wrapper = shallow(
                    <PagingButtons
                        pageSelected={pageSelected}
                        totalPages={totalPages}
                        changePageNumber={changePageNumber}
                    />,
                );
            });

            it("Only one page button is rendered", () => {
                expect(wrapper.find(PaginationButton)).toHaveLength(1);
            });
        });

        describe("When there are two total pages", () => {
            beforeEach(() => {
                totalPages = 2;
                wrapper = shallow(
                    <PagingButtons
                        pageSelected={pageSelected}
                        totalPages={totalPages}
                        changePageNumber={changePageNumber}
                    />,
                );
            });

            it("Two page buttons are rendered", () => {
                expect(wrapper.find(PaginationButton)).toHaveLength(2);
            });

            it("The button text 'Next' is rendered", () => {
                expect(wrapper.find(TextButton)).toHaveLength(1);
                expect(wrapper.find(TextButton).prop("value")).toBe("Next");
            });
        });

        describe("When there are more than two total pages", () => {
            beforeEach(() => {
                totalPages = 3;
                wrapper = shallow(
                    <PagingButtons
                        pageSelected={pageSelected}
                        totalPages={totalPages}
                        changePageNumber={changePageNumber}
                    />,
                );
            });

            it("two page buttons are rendered since the user is still on the first page", () => {
                expect(wrapper.find(PaginationButton)).toHaveLength(2);
            });

            it("The button text 'Next' is rendered and the three dots are rendered", () => {
                expect(wrapper.find(TextButton)).toHaveLength(1);
                expect(wrapper.find(TextButton).prop("value")).toBe("Next");
                expect(wrapper.find(".text-number-seperator")).toBeTruthy();
            });
        });
    });

    describe("When the user is on the second page", () => {
        beforeEach(() => {
            pageSelected = 1;
        });

        describe("When there are three total pages", () => {
            beforeEach(() => {
                totalPages = 3;
                wrapper = shallow(
                    <PagingButtons
                        pageSelected={pageSelected}
                        totalPages={totalPages}
                        changePageNumber={changePageNumber}
                    />,
                );
            });

            it("Three pagination buttons are rendered", () => {
                expect(wrapper.find(PaginationButton)).toHaveLength(3);
            });

            it("The button text 'Previous' and 'Next' are both rendered", () => {
                expect(wrapper.find(TextButton)).toHaveLength(2);
            });
        });
    });

    describe("When the user is on the third page", () => {
        beforeEach(() => {
            pageSelected = 3;
        });

        describe("When there are 4 total pages", () => {
            beforeEach(() => {
                totalPages = 5;
                wrapper = shallow(
                    <PagingButtons
                        pageSelected={pageSelected}
                        totalPages={totalPages}
                        changePageNumber={changePageNumber}
                    />,
                );
            });

            it("Three pagination buttons are rendered", () => {
                expect(wrapper.find(PaginationButton)).toHaveLength(3);
            });

            it("Two text number seperators are rendered", () => {
                expect(wrapper.find(".text-number-seperator")).toHaveLength(2);
            });

            it("Two text buttons are rendered", () => {
                expect(wrapper.find(TextButton)).toHaveLength(2);
            });
        });

        describe("When the user is on the last page", () => {
            beforeEach(() => {
                totalPages = 3;
                wrapper = shallow(
                    <PagingButtons
                        pageSelected={pageSelected}
                        totalPages={totalPages}
                        changePageNumber={changePageNumber}
                    />,
                );
            });

            it("It renders two pagination buttons", () => {
                expect(wrapper.find(PaginationButton)).toHaveLength(2);
            });

            it("It renders one text number seperator and one text button (Previous)", () => {
                expect(wrapper.find(".text-number-seperator")).toHaveLength(1);
                expect(wrapper.find(TextButton)).toHaveLength(1);
                expect(wrapper.find(TextButton).prop("value")).toBe("Previous");
            });
        });
    });
});
