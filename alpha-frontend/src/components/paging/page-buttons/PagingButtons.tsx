import "./PagingButtons.scss";

import React, { Fragment } from "react";

import { PaginationButton } from "components/reusables/pagination-button/PaginationButton";
import { TextButton } from "components/reusables/text-button/TextButton";

interface PagingButtonsProps {
    pageSelected: number;
    totalPages: number;
    changePageNumber: (pageNumber: number) => void;
}

export class PagingButtons extends React.Component<PagingButtonsProps> {
    private onPreviousClick = () => {
        this.props.changePageNumber(this.props.pageSelected - 1);
    };

    private onNextClick = () => {
        this.props.changePageNumber(this.props.pageSelected + 1);
    };

    render = () => (
        <div className="paging-buttons-parent-container">
            {this.props.pageSelected !== 0 && (
                <Fragment>
                    <TextButton onClick={this.onPreviousClick} value={"Previous"} />
                    {this.props.pageSelected > 1 && <span className="text-number-seperator">...</span>}
                    <PaginationButton
                        value={this.props.pageSelected}
                        onClick={this.props.changePageNumber}
                        selectedPage={this.props.pageSelected}
                    />
                </Fragment>
            )}
            <PaginationButton
                value={this.props.pageSelected + 1}
                onClick={this.props.changePageNumber}
                selectedPage={this.props.pageSelected}
            />
            {(this.props.pageSelected < this.props.totalPages -1) && (
                <Fragment>
                    <PaginationButton
                        value={this.props.pageSelected + 2}
                        onClick={this.props.changePageNumber}
                        selectedPage={this.props.pageSelected}
                    />
                    {this.props.pageSelected < this.props.totalPages - 1 && (
                        <span className="text-number-seperator">...</span>
                    )}
                    <TextButton onClick={this.onNextClick} value={"Next"} />
                </Fragment>
            )}
        </div>
    );
}
