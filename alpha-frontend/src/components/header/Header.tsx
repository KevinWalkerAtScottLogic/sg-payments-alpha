import "./Header.scss";

import { AppDispatch, RootState } from "state/rootReducer";

import { LoginStatus } from "enums/loginStatus";
import React from "react";
import { WebsiteLogo } from "resources/svgs/WebsiteLogo";
import { connect } from "react-redux";
import { setLoggedOut } from "components/login/state/statusAction";

const mapStateToProps = (state: RootState) => ({
    loggedInStatus: state.loginState.status,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
    setLoggedOut: () => dispatch(setLoggedOut()),
});

type ConnectedHeaderProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;

class Header extends React.Component<ConnectedHeaderProps> {
    render = () => (
        <div className="header-parent-container">
            <div className="left-container">
                <WebsiteLogo width={250} height={80} />
                <hr className="vertical-line" />
                <div className="title-website">Payments platform</div>
                {this.props.loggedInStatus !== LoginStatus.loggedIn && <span className="alpha-sign">ALPHA</span>}
            </div>
            <div className="right-container">
                {this.props.loggedInStatus === LoginStatus.loggedIn && (
                    <button
                        className="sign-out-button"
                        onClick={this.props.setLoggedOut}
                        aria-label={"Sign out"}
                        tab-index={0}
                    >
                        Sign out
                    </button>
                )}
            </div>
        </div>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
