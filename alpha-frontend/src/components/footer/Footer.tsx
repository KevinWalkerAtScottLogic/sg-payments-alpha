import "./Footer.scss";

import React from "react";
import { TextButton } from "components/reusables/text-button/TextButton";
import { WhiteLogo } from "resources/svgs/WhiteLogo";

export class Footer extends React.Component {
    private clickFunction = (): void => {
        "";
    };

    render = () => (
        <div className="footer-parent-container">
            <div className="feedback-container">
                <span className="alpha-sign">ALPHA</span>
                <span className="feedback-text">
                    This site is in alpha - your <a href="/">feedback</a> will help us improve it
                </span>
            </div>
            <div className="button-container">
                <TextButton value="About" onClick={this.clickFunction} />
                <TextButton value="Accessibility" onClick={this.clickFunction} />
                <TextButton value="Cookies" onClick={this.clickFunction} />
                <TextButton value="Feedback" onClick={this.clickFunction} />
                <TextButton value="Privacy" onClick={this.clickFunction} />
            </div>
            <div className="copyright-container">
                <span className="copyright-content">
                    © Crown copyright. All content is available under the Open Government Licence v3.0,
                    <br /> except for all graphic assets and otherwise stated
                </span>
                <div className="logo-footer">
                    <WhiteLogo width={200} height={70} />
                </div>
            </div>
        </div>
    );
}
