import "./Tabs.scss";

import { SUBMISSIONS_PATH, TRANSACTIONS_PATH } from "routes/routes";

import React from "react";
import { Tab } from "./single-tab/Tab";

enum Screens {
    Submissions = "Submissions",
    Transactions = "Transactions",
}

export class Tabs extends React.Component {
    render = () => (
        <div className="tabs-parent-container">
            <Tab label={Screens.Submissions} path={SUBMISSIONS_PATH} />
            <Tab label={Screens.Transactions} path={TRANSACTIONS_PATH} />
        </div>
    );
}
