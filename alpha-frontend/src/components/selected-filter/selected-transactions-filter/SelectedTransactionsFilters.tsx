import "./SelectedTransactionsFilters.scss";

import React from "react";
import TransactionExactFilter from "../transaction-exact-filter/TransactionExactFilter";
import TransactionStatusFilter from "../transaction-status-filter/TransactionStatusFilter";

class SelectedTransactionsFilters extends React.Component {
    render = () => (
        <div className="selected-transaction-filters-parent-container">
            <TransactionExactFilter />
            <TransactionStatusFilter />
        </div>
    );
}

export default SelectedTransactionsFilters;
