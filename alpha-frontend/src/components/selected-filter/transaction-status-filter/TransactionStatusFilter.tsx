import { AppDispatch, RootState } from "state/rootReducer";

import React from "react";
import { SelectedFilterRow } from "../selected-filter-row/SelectedFilterRow";
import { Status } from "components/reusables/status-label/StatusLabel";
import { connect } from "react-redux";
import { removeTransactionStatus } from "components/transactions/transactions-filter/state/transactionFilterActions";

const mapStateToProps = (state: RootState) => ({
    statuses: state.transactionsFilter.statuses,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
    removeStatusFilter: (status: Status) => dispatch(removeTransactionStatus(status)),
});

type ConnectedTransactionExactFilterProps = ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;

class TransactionStatusFilter extends React.Component<ConnectedTransactionExactFilterProps> {
    private extractName = (status: Status) => status;

    render = () => (
        <SelectedFilterRow
            filterTitle={"With status:"}
            filterContent={this.props.statuses}
            removeFilter={this.props.removeStatusFilter}
            extractName={this.extractName}
        />
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(TransactionStatusFilter);
