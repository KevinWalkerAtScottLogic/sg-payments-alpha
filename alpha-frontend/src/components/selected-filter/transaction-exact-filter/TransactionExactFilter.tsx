import { AppDispatch, RootState } from "state/rootReducer";

import { BatchFilter } from "components/transactions/transactions-filter/transactionsFilter";
import React from "react";
import { SelectedFilterRow } from "../selected-filter-row/SelectedFilterRow";
import { connect } from "react-redux";
import { removeExactBatchId } from "components/transactions/transactions-filter/state/transactionFilterActions";

const mapStateToProps = (state: RootState) => ({
    exactBatch: state.transactionsFilter.exactBatch,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
    removeExactBatchId: () => dispatch(removeExactBatchId()),
});

type ConnectedTransactionExactFilterProps = ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;

class TransactionExactFilter extends React.Component<ConnectedTransactionExactFilterProps> {
    private extractName = (batch: BatchFilter) => batch.name;

    render = () => (
        <SelectedFilterRow
            filterTitle={"Exact match:"}
            filterContent={this.props.exactBatch !== null ? [this.props.exactBatch] : []}
            removeFilter={this.props.removeExactBatchId}
            extractName={this.extractName}
        />
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(TransactionExactFilter);
