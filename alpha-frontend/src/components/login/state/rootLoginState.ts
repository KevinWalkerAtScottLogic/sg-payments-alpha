import { LoginStatus } from "enums/loginStatus";
import { Reducer } from "redux";
import { SET_LOGGED_IN_STATUS } from "./statusAction";

export interface LoginState {
    status: LoginStatus;
    idToken: string | null;
    accessToken: string | null;
}

const initialState: LoginState = {
    status: LoginStatus.loggedOut,
    idToken: null,
    accessToken: null,
};

export const loginReducer: Reducer<LoginState> = (state = initialState, action) => {
    switch (action.type) {
        case SET_LOGGED_IN_STATUS:
            return action.payload;
        default:
            return state;
    }
};
