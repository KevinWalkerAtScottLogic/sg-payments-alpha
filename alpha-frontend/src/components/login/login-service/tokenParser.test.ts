import { Tokens } from "./tokenParser";
import { parseToken } from "./tokenParser";

describe("the token parser", () => {
    let tokens: Tokens;
    let correctToken: string;
    const expectedIdToken = "expectedIdToken";
    const expectedAccessToken = "expectedAccessToken";

    describe("when the id token is the first parameter", () => {
        correctToken = `https://test.url.com?id_token=${expectedIdToken}&access_token=${expectedAccessToken}`;

        beforeEach(() => {
            tokens = parseToken(correctToken);
        });

        it("it returns the correct idToken", () => {
            expect(tokens.idToken).toEqual(expectedIdToken);
        });

        it("it returns the correct accessToken", () => {
            expect(tokens.accessToken).toEqual(expectedAccessToken);
        });
    });

    describe("when the access token is the first parameter", () => {
        correctToken = `https://test.url.com?access_token=${expectedAccessToken}&id_token=${expectedIdToken}`;

        beforeEach(() => {
            tokens = parseToken(correctToken);
        });

        it("it returns the correct idToken", () => {
            expect(tokens.idToken).toEqual(expectedIdToken);
        });

        it("it returns the correct accessToken", () => {
            expect(tokens.accessToken).toEqual(expectedAccessToken);
        });
    });

    describe("when there are multiple other query parameters", () => {
        correctToken = `https://test.url.com?filter=filterParams&userName=Steven&access_token=${expectedAccessToken}&something=yes&new_params=this&id_token=${expectedIdToken}`;

        beforeEach(() => {
            tokens = parseToken(correctToken);
        });

        it("it returns the correct idToken", () => {
            expect(tokens.idToken).toEqual(expectedIdToken);
        });

        it("it returns the correct accessToken", () => {
            expect(tokens.accessToken).toEqual(expectedAccessToken);
        });
    });
});
