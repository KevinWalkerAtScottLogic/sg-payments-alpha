import { parseToken } from "./tokenParser";

const getDeployedRedirectUrl = (): string => `${process.env.REACT_APP_COGNITO_REDIRECT_URL}/`;

const getCognitoAuthUrl = (): string =>
    `${process.env.REACT_APP_COGNITO_ENDPOINT}/login?response_type=token&client_id=${
        process.env.REACT_APP_UI_COGNITO_APP_CLIENT_ID
    }&redirect_uri=${getDeployedRedirectUrl()}`;

export const getAuthUrl = (): string =>
    process.env.REACT_APP_DYNAMIC_AUTH === "true"
        ? getCognitoAuthUrl()
        : `${process.env.REACT_APP_COGNITO_ENDPOINT}/oauth/token`;

const fetchTokenWithBasicAuth = (): Promise<{ access_token: string }> => {
    const user = {
        username: "admin_user",
        password: "adminpwd",
        grantType: "client_credentials",
    };

    return fetch(getAuthUrl(), {
        headers: {
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
            Authorization: "Basic " + Buffer.from("local:local").toString("base64"),
        },
        method: "POST",
        body: `username=${user.username}&password=${user.password}&grant_type=${user.grantType}`,
    }).then(response => (response.ok ? response.json() : Promise.reject()));
};

export const handleAutoLogin = (
    url: string,
    onLoggingIn: () => void,
    onLoggedIn: (idToken: string, accessToken: string) => void,
    onLoggedOut: () => void,
): Promise<void> => {
    if (process.env.REACT_APP_AUTO_LOGIN === "true") {
        onLoggingIn();
        return fetchTokenWithBasicAuth()
            .then((response: { access_token: string }) => {
                onLoggedIn(response.access_token, response.access_token);
            })
            .catch(err => {
                onLoggedOut();
                return Promise.reject(err);
            });
    } else if (url.includes("id_token")) {
        const { idToken, accessToken } = parseToken(url);
        onLoggedIn(idToken, accessToken);
    }
    return Promise.resolve();
};
