import { getAuthUrl, handleAutoLogin } from "./loginService";

import fetchMock from "fetch-mock";
import json from "./loginService.test.json";

describe("Given the login service", () => {
    const cognitoUrl = "https://test.cognito.endpoint.com";

    describe("when handleAutoLogin is called", () => {
        const currentUrl = "http://current.url.com/";
        let onLoggedInJestFun: jest.Mock;
        let onLoggingInJestFun: jest.Mock;
        let onLoggedOutInJestFun: jest.Mock;

        beforeEach(() => {
            onLoggedInJestFun = jest.fn();
            onLoggingInJestFun = jest.fn();
            onLoggedOutInJestFun = jest.fn();
        });

        afterEach(() => {
            fetchMock.reset();
        });

        describe("and auto login is set to true", () => {
            beforeEach(() => {
                process.env = Object.assign(process.env, {
                    REACT_APP_AUTO_LOGIN: "true",
                });
            });

            describe("and dynamic auth is false", () => {
                beforeEach(() => {
                    process.env = Object.assign(process.env, {
                        REACT_APP_DYNAMIC_AUTH: "false",
                        REACT_APP_COGNITO_ENDPOINT: cognitoUrl,
                    });
                });

                describe("and the authentication endpoint returns successfully", () => {
                    beforeEach(() => {
                        fetchMock.post(`${cognitoUrl}/oauth/token`, json);
                    });

                    it("then it calls onLoggingIn function first", () => {
                        handleAutoLogin(currentUrl, onLoggingInJestFun, onLoggedInJestFun, onLoggedOutInJestFun);
                        expect(onLoggingInJestFun).toBeCalledTimes(1);
                    });

                    it("then it calls onLoggedIn function when promise is returned", done => {
                        handleAutoLogin(currentUrl, onLoggingInJestFun, onLoggedInJestFun, onLoggedOutInJestFun).then(
                            () => {
                                expect(onLoggedInJestFun).toBeCalledWith(json.access_token, json.access_token);
                                done();
                            },
                        );
                    });

                    it("then the end point was called with the correct headers", done => {
                        handleAutoLogin(currentUrl, onLoggingInJestFun, onLoggedInJestFun, onLoggedOutInJestFun).then(
                            () => {
                                const lastCall = fetchMock.lastCall();
                                expect(lastCall).toBeDefined();
                                if (lastCall) {
                                    const headers: Record<string, string> = lastCall[1]?.headers as Record<
                                        string,
                                        string
                                    >;
                                    expect(headers["Content-Type"]).toBe(
                                        "application/x-www-form-urlencoded;charset=UTF-8",
                                    );
                                    expect(headers["Authorization"]).toBe("Basic bG9jYWw6bG9jYWw=");
                                }
                                done();
                            },
                        );
                    });

                    it("then authentication details are provided in the body", done => {
                        handleAutoLogin(currentUrl, onLoggingInJestFun, onLoggedInJestFun, onLoggedOutInJestFun).then(
                            () => {
                                const lastCall = fetchMock.lastCall();
                                expect(lastCall).toBeDefined();
                                if (lastCall) {
                                    expect(lastCall[1]?.body).toContain("username=");
                                    expect(lastCall[1]?.body).toContain("password=");
                                    expect(lastCall[1]?.body).toContain("grant_type=");
                                }
                                done();
                            },
                        );
                    });
                });

                describe("and the authentication endpoint fails", () => {
                    beforeEach(() => {
                        fetchMock.post(`${cognitoUrl}/oauth/token`, 500);
                    });

                    it("then the logged in function is not called", done => {
                        handleAutoLogin(currentUrl, onLoggingInJestFun, onLoggedInJestFun, onLoggedOutInJestFun)
                            .then(() => {
                                fail("Should not resolve");
                            })
                            .catch(() => {
                                expect(onLoggedInJestFun).toBeCalledTimes(0);
                                done();
                            });
                    });

                    it("then the logged out function is called", done => {
                        handleAutoLogin(currentUrl, onLoggingInJestFun, onLoggedInJestFun, onLoggedOutInJestFun)
                            .then(() => {
                                fail("Should not resolve");
                            })
                            .catch(() => {
                                expect(onLoggedOutInJestFun).toBeCalledTimes(1);
                                done();
                            });
                    });
                });
            });
        });

        describe("and auto login is set to false", () => {
            beforeEach(() => {
                process.env = Object.assign(process.env, {
                    REACT_APP_AUTO_LOGIN: "false",
                });
            });

            describe("and the url contains id token", () => {
                const expectedIdToken = "testIdToken";
                const expectedAccessToken = "testAccessToken";
                const currentUrlWithTokens = `${currentUrl}?id_token=${expectedIdToken}&access_token=${expectedAccessToken}`;
                beforeEach(() => {
                    process.env = Object.assign(process.env, {
                        REACT_APP_AUTO_LOGIN: "false",
                    });
                });

                it("then it calls onLoggedIn function", done => {
                    handleAutoLogin(
                        currentUrlWithTokens,
                        onLoggingInJestFun,
                        onLoggedInJestFun,
                        onLoggedOutInJestFun,
                    ).then(() => {
                        expect(onLoggedInJestFun).toBeCalledWith(expectedIdToken, expectedAccessToken);
                        done();
                    });
                });
            });
        });
    });

    describe("when getAuthUrl is called", () => {
        beforeEach(() => {
            process.env = Object.assign(process.env, {
                REACT_APP_COGNITO_ENDPOINT: cognitoUrl,
            });
        });

        describe("and dynamic auth is true", () => {
            const testAppId = "test app id";
            const host = "localhost";
            beforeEach(() => {
                process.env = Object.assign(process.env, {
                    REACT_APP_DYNAMIC_AUTH: "true",
                    REACT_APP_UI_COGNITO_APP_CLIENT_ID: testAppId,
                    REACT_APP_COGNITO_REDIRECT_URL: "https://" + host + "/alpha",
                });
            });

            it("then the expected url is returned", () => {
                expect(getAuthUrl()).toBe(
                    `${cognitoUrl}/login?response_type=token&client_id=${process.env.REACT_APP_UI_COGNITO_APP_CLIENT_ID}&redirect_uri=${process.env.REACT_APP_COGNITO_REDIRECT_URL}/`,
                );
            });
        });

        describe("and dynamic auth is false", () => {
            beforeEach(() => {
                process.env = Object.assign(process.env, {
                    REACT_APP_DYNAMIC_AUTH: "false",
                });
            });

            it("then the expected url is returned", () => {
                expect(getAuthUrl()).toBe(cognitoUrl + "/oauth/token");
            });
        });
    });
});
