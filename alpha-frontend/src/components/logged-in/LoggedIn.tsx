import { EXACT_TRANSACTION, SUBMISSIONS_PATH, TRANSACTIONS_PATH } from "routes/routes";
import { Route, Switch } from "react-router-dom";
import Sandbox, { SANDBOX_PATH } from "sandbox/Sandbox";

import React from "react";
import { SingleTransaction } from "components/transactions/single-transaction/SingleTransaction";
import Submissions from "components/submissions/Submissions";
import { Tabs } from "components/tabs/Tabs";
import Transactions from "components/transactions/Transactions";

export class LoggedIn extends React.Component {
    render = () => (
        <div className="logged-in-parent-container">
            <Tabs />
            <Switch>
                <Route exact path={SUBMISSIONS_PATH} component={Submissions} />
                <Route exact path={TRANSACTIONS_PATH} component={Transactions} />
                <Route path={EXACT_TRANSACTION} component={SingleTransaction} />

                {/* TO DELETE */}
                <Route exact path={SANDBOX_PATH} component={Sandbox} />
            </Switch>
        </div>
    );
}
