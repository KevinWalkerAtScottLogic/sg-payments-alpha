import { FormatterParams } from "components/reusables/table/Table";
import { SubmissionRow } from "./submissions-table/SubmissionsTable";

export const formatResultsCount = (resultsCount?: number) =>
    resultsCount ? `${resultsCount} result${resultsCount === 1 ? "" : "s"}` : "0 batch submissions";

export const formatErrorsCount = (errorCount: number) =>
    errorCount > 0 ? `${errorCount} error${errorCount === 1 ? "" : "s"}` : "";

export const formatPendingApprovalCount = (pendingCount: number) =>
    pendingCount > 0 ? `${pendingCount} pending approval` : "";

export const displayIfValid = (
    params: FormatterParams<SubmissionRow>,
    validFormatter: (params: FormatterParams<SubmissionRow>) => string = params => params.value,
) => (params.data.status !== "Failed" ? validFormatter(params) : "-");

export const formatProductArray = (params: FormatterParams<SubmissionRow>) =>
    displayIfValid(params, params => params.value.join(", "));

export const formatTotalAmount = (params: FormatterParams<SubmissionRow>) =>
    displayIfValid(params, params => `£${params.value}`);
