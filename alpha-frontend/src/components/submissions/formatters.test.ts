import { Column, RowNode } from "ag-grid-community";
import {
    displayIfValid,
    formatDate,
    formatErrorsCount,
    formatPendingApprovalCount,
    formatProductArray,
    formatResultsCount,
    formatTotalAmount,
} from "./formatters";

import { FormatterParams } from "components/reusables/table/Table";
import { SubmissionRow } from "./submissions-table/SubmissionsTable";
import moment from "moment";

const product1 = "testProduct1";
const product2 = "testProduct2";
const product3 = "testProduct3";
const defaultTestParams: FormatterParams<SubmissionRow> = {
    data: {
        batchId: "testId",
        createdOn: moment(),
        products: [product1, product2, product3],
        transactions: 150,
        totalAmount: "12.00",
        totalErrorPayments: 1,
        totalPendingPayments: 5,
        status: "New",
    },
    value: null,
    node: new RowNode(),
    colDef: {},
    column: new Column({}, null, "colId", true),
    api: null,
    columnApi: null,
    context: null,
};

const createParams = (value: unknown, data?: Partial<SubmissionRow>) => ({
    ...defaultTestParams,
    ...{ value, data: { ...defaultTestParams.data, ...data } },
});

describe("Given a call to formatDate", () => {
    it("then the correctly formatted date is returned", () => {
        expect(formatDate(createParams(moment("2020-01-02")))).toBe("2 Jan 2020");
    });
});

describe("Given a call to formatResultsCount", () => {
    let resultsCount: number | undefined;

    describe("when the resultsCount is undefined", () => {
        it('then "0 batch submissions" is returned', () => {
            expect(formatResultsCount(resultsCount)).toBe("0 batch submissions");
        });
    });

    describe("when the resultsCount is 0", () => {
        beforeEach(() => {
            resultsCount = 0;
        });

        it('then "0 batch submissions" is returned', () => {
            expect(formatResultsCount(resultsCount)).toBe("0 batch submissions");
        });
    });

    describe("when the resultsCount is 1", () => {
        beforeEach(() => {
            resultsCount = 1;
        });

        it('then "1 result" is returned', () => {
            expect(formatResultsCount(resultsCount)).toBe("1 result");
        });
    });

    describe("when the resultsCount is more than 1", () => {
        beforeEach(() => {
            resultsCount = 2;
        });

        it('then "n results" is returned', () => {
            expect(formatResultsCount(resultsCount)).toBe("2 results");
        });
    });
});

describe("Given a call to formatErrorsCount", () => {
    let errorsCount: number;

    describe("when the errorsCount is 0", () => {
        beforeEach(() => {
            errorsCount = 0;
        });

        it("then an empty string is returned", () => {
            expect(formatErrorsCount(errorsCount)).toBe("");
        });
    });

    describe("when the errorsCount is 1", () => {
        beforeEach(() => {
            errorsCount = 1;
        });

        it('then "1 error" is returned', () => {
            expect(formatErrorsCount(errorsCount)).toBe("1 error");
        });
    });

    describe("when the errorsCount is more than 1", () => {
        beforeEach(() => {
            errorsCount = 2;
        });

        it('then "n errors" is returned', () => {
            expect(formatErrorsCount(errorsCount)).toBe("2 errors");
        });
    });
});

describe("Given a call to formatPendingApprovalCount", () => {
    let pendingApprovalCount: number;

    describe("when the pendingApprovalCount is 0", () => {
        beforeEach(() => {
            pendingApprovalCount = 0;
        });

        it("then an empty string is returned", () => {
            expect(formatPendingApprovalCount(pendingApprovalCount)).toBe("");
        });
    });

    describe("when the pendingApprovalCount is more than 0", () => {
        beforeEach(() => {
            pendingApprovalCount = 2;
        });

        it('then "n pending approval" is returned', () => {
            expect(formatPendingApprovalCount(pendingApprovalCount)).toBe("2 pending approval");
        });
    });
});

describe("Given a call to displayIfValid", () => {
    let params: FormatterParams<SubmissionRow>;
    const value = "expectedValue";

    describe("when the status is valid", () => {
        beforeEach(() => {
            params = createParams(value, { status: "New" });
        });

        it("then the value is returned", () => {
            expect(displayIfValid(params)).toBe(value);
        });
    });

    describe("when the status is invalid", () => {
        beforeEach(() => {
            params = createParams(value, { status: "Failed" });
        });

        it("then a dash is returned", () => {
            expect(displayIfValid(params)).toBe("-");
        });
    });
});

describe("Given a call to formatProductArray", () => {
    let params: FormatterParams<SubmissionRow>;
    const value = [product1, product2, product3];

    describe("when the status is valid", () => {
        beforeEach(() => {
            params = createParams(value, { status: "New" });
        });

        it("then a comma separated string is returned", () => {
            expect(formatProductArray(params)).toBe(`${product1}, ${product2}, ${product3}`);
        });
    });

    describe("when the status is invalid", () => {
        beforeEach(() => {
            params = createParams(value, { status: "Failed" });
        });

        it("then a dash is returned", () => {
            expect(formatProductArray(params)).toBe("-");
        });
    });
});

describe("Given a call to formatTotalAmount", () => {
    let params: FormatterParams<SubmissionRow>;
    const value = "75.00";

    describe("when the status is valid", () => {
        beforeEach(() => {
            params = createParams(value, { status: "New" });
        });

        it("then the value is returned with a £ sign", () => {
            expect(formatTotalAmount(params)).toBe("£75.00");
        });
    });

    describe("when the status is invalid", () => {
        beforeEach(() => {
            params = createParams(value, { status: "Failed" });
        });

        it("then a dash is returned", () => {
            expect(formatTotalAmount(params)).toBe("-");
        });
    });
});
