import "./SubmissionsTable.scss";

import { AppDispatch, RootState } from "state/rootReducer";
import { CellRenderers, Column, Options, Table } from "components/reusables/table/Table";
import { displayIfValid, formatProductArray, formatTotalAmount } from "../submissionsFormatters";

import BatchIdCell from "./batch-id-cell/BatchIdCell";
import ErrorCell from "./error-cell/ErrorCell";
import React from "react";
import { Sort } from "enums/sorting";
import { SortableHeader } from "components/reusables/table/custom-header/sortable/SortableHeader";
import { SubmissionStatus } from "../state/submissionsMapper";
import { connect } from "react-redux";
import { formatDate } from "shared/utils/formatters";
import moment from "moment";
import { setSubmissionsSort } from "../submissions-filter/state/submissionsFilterAction";

export type SubmissionsColumnName = "Batch ID" | "Created on" | "Product" | "Transactions" | "Tot. amount";
export type SubmissionsColumnFields = "id" | "processingTime" | "products" | "totalNumberOfPayments" | "currencyTotals";

export interface SubmissionRow {
    batchId: string;
    name: string;
    createdOn: moment.Moment;
    products: string[];
    transactions: number;
    totalAmount: string;
    errorDisplay?: string;
    totalErrorPayments: number;
    totalPendingPayments: number;
    status: SubmissionStatus;
}

const cellRenderers: CellRenderers<SubmissionRow> = { batchIdRenderer: BatchIdCell, errorRenderer: ErrorCell };

const gridOptions: Options<SubmissionRow> = {
    rowClassRules: {
        "failed-submission": params => params.data.status === "Failed",
    },
};

const mapStateToProps = (state: RootState) => ({
    rowData: state.submissionsPage ? state.submissionsPage.content : [],
    submissionsSort: state.submissionsFilter.sortBy,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
    setSubmissionSort: (sort: Sort<SubmissionsColumnFields>) => dispatch(setSubmissionsSort(sort)),
});

type ConnectedSubmissionsTableProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;

class SubmissionsTable extends React.Component<ConnectedSubmissionsTableProps> {
    private getCurrentSorting = (): Sort<SubmissionsColumnFields> => this.props.submissionsSort;

    private subbmissionsColumns: Column<SubmissionRow, SubmissionsColumnFields>[] = [
        {
            field: "batchId",
            headerComponentFramework: SortableHeader,
            headerComponentParams: {
                name: "Batch ID",
                field: "id",
                sort: this.props.setSubmissionSort,
                getCurrentSorting: this.getCurrentSorting,
            },
            width: 300,
            cellRenderer: "batchIdRenderer",
        },
        {
            field: "createdOn",
            headerComponentFramework: SortableHeader,
            headerComponentParams: {
                name: "Created on",
                field: "processingTime",
                sort: this.props.setSubmissionSort,
                getCurrentSorting: this.getCurrentSorting,
            },
            tooltipValueGetter: formatDate,
            valueFormatter: formatDate,
            width: 150,
        },
        {
            field: "products",
            headerComponentFramework: SortableHeader,
            headerComponentParams: {
                name: "Product",
                field: "products",
                sort: this.props.setSubmissionSort,
                getCurrentSorting: this.getCurrentSorting,
            },
            valueFormatter: formatProductArray,
            width: 400,
        },
        {
            field: "transactions",
            headerComponentFramework: SortableHeader,
            headerComponentParams: {
                name: "Transactions",
                field: "totalNumberOfPayments",
                sort: this.props.setSubmissionSort,
                getCurrentSorting: this.getCurrentSorting,
            },
            valueFormatter: displayIfValid,
            width: 300,
        },
        {
            field: "totalAmount",
            headerComponentFramework: SortableHeader,
            headerComponentParams: {
                name: "Tot. amount",
                field: "currencyTotals",
                sort: this.props.setSubmissionSort,
                getCurrentSorting: this.getCurrentSorting,
            },
            valueFormatter: formatTotalAmount,
            width: 120,
        },
        {
            field: "errorDisplay",
            headerName: "",
            cellRenderer: "errorRenderer",
        },
    ];

    render = () => (
        <Table
            columns={this.subbmissionsColumns}
            rows={this.props.rowData}
            cellRenderers={cellRenderers}
            gridOptions={gridOptions}
            sort={this.props.submissionsSort}
        />
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(SubmissionsTable);
