import { SubmissionsFilter } from "./submissionsFilter";

export const constructSubmissionFilterQuery = (queryObject: SubmissionsFilter): string | null => {
    const query: string[] = [];

    return query.length > 0 ? query.join(";") : null;
};
