import { Reducer } from "redux";
import { SET_SUBMISSIONS_SORT } from "./submissionsFilterAction";
import { SubmissionsFilter } from "../submissionsFilter";

const initialState: SubmissionsFilter = {
    sortBy: { order: "asc", field: "processingTime" },
};

export const submissionsFilterReducer: Reducer<SubmissionsFilter> = (state = initialState, action) => {
    switch (action.type) {
        case SET_SUBMISSIONS_SORT:
            return {
                ...state,
                sortBy: action.payload,
            };
        default:
            return state;
    }
};
