import { DtoSubmissions, mapSubmissionsToState } from "./submissionsMapper";

import { DtoPage } from "shared/network/DtoPage";
import { Page } from "state/page";
import { SubmissionRow } from "../submissions-table/SubmissionsTable";
import moment from "moment";

describe("Given a submissions mapper", () => {
    const id1 = "id1";
    const id2 = "id2";
    const name1 = "name1";
    const name2 = "name2";
    const processingTime1 = "2020-01-04T15:30:53.413794Z";
    const processingTime2 = "2020-01-31T00:00:00.000000Z";
    const productArray1 = ["product1, product2"];
    const productArray2 = ["product3, product4", "product5"];
    const errorMessage1 = "errorMessage1";
    const errorMessage2 = "errorMessage2";
    const status1 = "New";
    const status2 = "Failed";
    let dto: DtoPage<DtoSubmissions>;

    beforeEach(() => {
        dto = {
            _embedded: {
                paymentBatchProjectionList: [
                    {
                        id: id1,
                        name: name1,
                        totalNumberOfPayments: "7",
                        currencyTotals: { GBP: "45.00", EUR: "12.00" },
                        totalErrorPayments: "1",
                        totalPendingPayments: "4",
                        status: status1,
                        errorMessage: errorMessage1,
                        products: productArray1,
                        processingTime: processingTime1,
                    },
                    {
                        id: id2,
                        name: name2,
                        totalNumberOfPayments: "42",
                        currencyTotals: { GBP: "752.00", USD: "10.00" },
                        totalErrorPayments: "3",
                        totalPendingPayments: "2",
                        status: status2,
                        errorMessage: errorMessage2,
                        products: productArray2,
                        processingTime: processingTime2,
                    },
                ],
            },
            page: {
                size: "2",
                totalElements: "150",
                totalPages: "75",
                number: "0",
            },
        };
    });
    describe("when mapSubmissionsToState is called", () => {
        let result: Page<SubmissionRow>;
        beforeEach(() => {
            result = mapSubmissionsToState(dto);
        });

        it("then the page details are mapped", () => {
            expect(result.totalElements).toBe(150);
        });

        it("then the individual submission details are mapped", () => {
            expect(result.content.length).toBe(2);
            const submission1 = result.content[0];
            expect(submission1.batchId).toBe(id1);
            expect(submission1.name).toBe(name1);
            expect(submission1.createdOn).toEqual(moment(processingTime1));
            expect(submission1.products).toEqual(productArray1);
            expect(submission1.transactions).toBe(7);
            expect(submission1.totalAmount).toBe("45.00");
            expect(submission1.errorDisplay).toBe(errorMessage1);
            expect(submission1.totalErrorPayments).toBe(1);
            expect(submission1.totalPendingPayments).toBe(4);
            expect(submission1.status).toBe(status1);

            const submission2 = result.content[1];
            expect(submission2.batchId).toBe(id2);
            expect(submission2.name).toBe(name2);
            expect(submission2.createdOn).toEqual(moment(processingTime2));
            expect(submission2.products).toEqual(productArray2);
            expect(submission2.transactions).toBe(42);
            expect(submission2.totalAmount).toBe("752.00");
            expect(submission2.errorDisplay).toBe(errorMessage2);
            expect(submission2.totalErrorPayments).toBe(3);
            expect(submission2.totalPendingPayments).toBe(2);
            expect(submission2.status).toBe(status2);
        });
    });
});
