import { DtoPage } from "shared/network/DtoPage";
import { Page } from "state/page";
import { SubmissionRow } from "../submissions-table/SubmissionsTable";
import moment from "moment";

export type SubmissionStatus = "New" | "Open" | "Closed" | "Failed";

interface DtoSubmission {
    id: string;
    name: string;
    totalNumberOfPayments: string;
    currencyTotals: { [index: string]: string };
    totalErrorPayments: string;
    totalPendingPayments: string;
    status: SubmissionStatus;
    errorMessage: string;
    products: string[];
    processingTime: string;
}

export interface DtoSubmissions {
    paymentBatchProjectionList: DtoSubmission[];
}

const mapSubmission = (dto: DtoSubmission): SubmissionRow => ({
    batchId: dto.id,
    name: dto.name,
    createdOn: moment(dto.processingTime),
    products: dto.products,
    transactions: parseInt(dto.totalNumberOfPayments),
    totalAmount: dto.currencyTotals["GBP"],
    errorDisplay: dto.errorMessage,
    totalErrorPayments: parseInt(dto.totalErrorPayments),
    totalPendingPayments: parseInt(dto.totalPendingPayments),
    status: dto.status,
});

export const mapSubmissionsToState = (dto: DtoPage<DtoSubmissions>): Page<SubmissionRow> => ({
    content: dto._embedded ? dto._embedded.paymentBatchProjectionList.map(mapSubmission) : [],
    totalElements: parseInt(dto.page.totalElements),
    pageNumber: parseInt(dto.page.number),
    pageSize: parseInt(dto.page.size),
    totalPages: parseInt(dto.page.totalPages),
});
