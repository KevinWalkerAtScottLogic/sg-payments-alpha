import { Page } from "state/page";
import { Reducer } from "redux";
import { SET_SUBMISSIONS_PAGE } from "./submissionsActions";
import { SubmissionRow } from "../submissions-table/SubmissionsTable";

export const submissionsReducer: Reducer<Page<SubmissionRow> | null> = (state = null, action) => {
    switch (action.type) {
        case SET_SUBMISSIONS_PAGE:
            return action.payload;
        default:
            return state;
    }
};
