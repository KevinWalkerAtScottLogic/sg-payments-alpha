import { ShallowWrapper, shallow } from "enzyme";

import React from "react";
import { Submissions } from "./Submissions";
import SubmissionsTable from "./submissions-table/SubmissionsTable";

describe("Given a submissions component", () => {
    let wrapper: ShallowWrapper;
    let mockFetch: jest.Mock;
    beforeEach(() => {
        mockFetch = jest.fn();
    });

    describe("when the datasize is undefined", () => {
        beforeEach(() => {
            wrapper = shallow(
                <Submissions submissionsPage={{ totalElements: undefined }} fetchSubmissions={mockFetch} />,
            );
        });

        it("then submissions are fetched on mount", () => {
            expect(mockFetch).toBeCalledTimes(1);
        });

        it("then the number of results is displayed", () => {
            expect(wrapper.find(".result-number").text()).toBe("0 batch submissions");
        });

        it("then hides the table and displays a custom message", () => {
            expect(wrapper.contains(<SubmissionsTable />)).toBe(false);
            expect(wrapper.find(".no-result-text").text()).toBe("No results match the search criteria");
        });
    });

    describe("when the datasize is defined", () => {
        const expectedDataSize = 7;
        beforeEach(() => {
            wrapper = shallow(
                <Submissions submissionsPage={{ totalElements: expectedDataSize }} fetchSubmissions={mockFetch} />,
            );
        });

        it("submissions are still fetched on mount", () => {
            expect(mockFetch).toBeCalledTimes(1);
        });

        it("then the number of results is displayed", () => {
            expect(wrapper.find(".result-number").text()).toBe("7 results");
        });
    });
});
