import React from "react";
import moment from "moment"
import * as PaymentError from "../PaymentErrorHelper"
import problem_symbol from '../../../assets/payment_problem.svg'; 
import ReactTooltip from 'react-tooltip'
import {getDisplayStatus} from "../constants"
import HoverComponent from "../../common/HoverComponent"

class InboundPayment extends HoverComponent {

    constructor(){
        super()
        this.hoverActiveClass = "selected";
        this.hoverInactiveClass = "payments-row-style";
    }

    dateFormat = (date) => {
        return moment(date).format("D MMM YYYY ");
    }
    
    getSymbolRow = (payment) => {
    
        if(PaymentError.isErrorColumn(payment.status)){
      
            return (
                <div>
                    <ReactTooltip />
                    <img data-tip= {payment.statusMessage} className = "symbol-img-problem" src={problem_symbol} alt="Problem payment" />
                </div>
            )
        
        } else {
            return <span className = "symbol-img-filler"  />
        }
    }

    onRowClick = () => {
        this.props.onRowClick(this.props.payment.id);
    }


    render(){
        return ( 
                <tr onMouseEnter={this.hoverOn} onMouseLeave={this.hoverOff} className ={this.getActiveClassName()}
                        onClick={this.onRowClick} >
                        <td className="symbol-row">{this.getSymbolRow(this.props.payment)}</td>
                        <td className="id-row"><div className="id-content">{this.props.payment.id}</div></td>
                        <td className="large-fixed-width-cell">{this.dateFormat(this.props.payment.timestamp)}</td>
                        <td className="fixed-width-cell">{this.props.payment.service}</td>
                        <td className="large-fixed-width-cell">{this.props.payment.ref}</td>
                        <td className="large-fixed-width-cell">{this.props.payment.channelType}</td>
                        <td className="fixed-width-cell">{getDisplayStatus(this.props.payment.status)}</td>
                        <td className="target-amount">{(Number(this.props.payment.amount.amount)).toFixed(2)}</td>
                </tr>
    )
    }
}

export default InboundPayment;