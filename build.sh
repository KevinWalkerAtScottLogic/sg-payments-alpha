#!/usr/bin/env bash

./gradlew clean
export REACT_APP_STATIC_SITE=false
export REACT_APP_DYNAMIC_AUTH=true
chmod +x local/scripts/*.sh
./gradlew assemble
cd frontend
yarn
yarn build
mkdir -p ../local/build/resources/main/static/ui
cp -r build/ ../local/build/resources/main/static/ui
cd ../
./gradlew assemble copyLocalJar --rerun-tasks
