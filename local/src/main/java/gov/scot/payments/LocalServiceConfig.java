package gov.scot.payments;

import com.beust.jcommander.SubParameter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import static gov.scot.payments.common.local.LocalConfig.q;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LocalServiceConfig {


    enum FileFormat {ILF,CPS}

    @SubParameter(order = 0)
    private String name;

    @SubParameter(order = 1)
    private String customer;

    @SubParameter(order = 2)
    private FileFormat fileFormat;

    public static LocalServiceConfig fromString(String s) {
        String[] components = s.split(";");
        if(components.length == 2){
            return new LocalServiceConfig(components[0],components[1],null);
        } else if(components.length == 3){
            return new LocalServiceConfig(components[0],components[1],FileFormat.valueOf(components[2]));
        } else{
            throw new IllegalArgumentException("service must be of format <NAME>;<CUSTOMER_ID>;(CPS|ILF)");
        }
    }

    public Tuple2<String, String> toProperty() {
        if(fileFormat != null){
            return Tuples.of("customer."+customer+".service."+name+".file-format",fileFormat.name());
        } else{
            return Tuples.of("customer."+customer+".service."+name,name);
        }
    }

    @Override
    public String toString(){
        String s = name + ";" + customer;
        if(fileFormat != null){
            s = s + ";" + fileFormat.name();
        }
        return  q(s);
    }

}
