package gov.scot.payments;

import com.beust.jcommander.JCommander;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.cardpayment.CardPaymentRepository;
import gov.scot.payments.common.gateway.inbound.cardpayment.CardPaymentGateway;
import gov.scot.payments.common.kafka.KafkaStreamsOverridingContext;
import gov.scot.payments.common.local.LocalApplicationConfig;
import gov.scot.payments.common.local.LocalConfig;
import gov.scot.payments.common.local.LocalHelpConfig;
import gov.scot.payments.customer.CustomerRepository;
import gov.scot.payments.gateway.outbound.AbstractOutboundGateway;
import gov.scot.payments.common.gateway.GatewayDetailsService;
import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.inboundpayment.TemporalInboundPayment;
import gov.scot.payments.inboundpayment.TemporalInboundPaymentRepository;
import gov.scot.payments.model.cardpayment.CardPayment;
import gov.scot.payments.model.customer.Customer;
import gov.scot.payments.customer.CustomerService;
import gov.scot.payments.customer.CustomerServiceService;
import gov.scot.payments.model.customer.PaymentValidationRule;
import gov.scot.payments.model.customer.Service;
import gov.scot.payments.model.paymentfile.PaymentFile;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import gov.scot.payments.model.report.Report;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.model.user.User;
import gov.scot.payments.paymentfile.PaymentFileRepository;
import gov.scot.payments.paymentinstruction.TemporalPayment;
import gov.scot.payments.paymentinstruction.TemporalPaymentRepository;
import gov.scot.payments.paymentprocessor.PaymentProcessorProxy;
import gov.scot.payments.report.ReportRepository;
import gov.scot.payments.security.UserRepository;
import gov.scot.payments.spring.CoreConfiguration;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.boot.context.properties.bind.Bindable;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.boot.web.context.WebServerApplicationContext;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.boot.web.context.WebServerPortFileWriter;
import org.springframework.cloud.aws.autoconfigure.context.ContextCredentialsAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.jdbc.AmazonRdsDatabaseAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.messaging.MessagingAutoConfiguration;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.cloud.client.serviceregistry.ServiceRegistry;
import org.springframework.cloud.zookeeper.discovery.ZookeeperDiscoveryProperties;
import org.springframework.cloud.zookeeper.discovery.ZookeeperInstance;
import org.springframework.cloud.zookeeper.serviceregistry.ServiceInstanceRegistration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.HttpMethod;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.core.context.SecurityContextHolder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static org.springframework.cloud.zookeeper.support.StatusConstants.INSTANCE_STATUS_KEY;
import static org.springframework.cloud.zookeeper.support.StatusConstants.STATUS_UP;

@SpringBootApplication(scanBasePackageClasses = LocalApplication.class
        ,exclude = {MessagingAutoConfiguration.class
        , AmazonRdsDatabaseAutoConfiguration.class
        , ContextResourceLoaderAutoConfiguration.class
        , ContextStackAutoConfiguration.class
        , ContextRegionProviderAutoConfiguration.class
        , ContextCredentialsAutoConfiguration.class
        , UserDetailsServiceAutoConfiguration.class
        , HypermediaAutoConfiguration.class
})
@Import({CoreConfiguration.class
        , EmbeddedKafkaConfiguration.class
        , MockS3Configuration.class
        , MockSqsConfiguration.class
        , S3Configuration.class
        , MockWebSecurityConfigurationCore.class
        , MockQuicksightConfiguration.class
        , AwsConfiguration.class
        , CoreBinding.class
        , EmbeddedGatewayBinding.class
        })
@ComponentScan(useDefaultFilters = false)
@EnableSpringDataWebSupport
@EntityScan(basePackageClasses = {
        PaymentFile.class
        , Service.class
        , TemporalPayment.class
        , Report.class
        , User.class
        , PaymentProcessor.class
        , TemporalInboundPayment.class
        , CardPayment.class})
@EnableJpaRepositories(basePackageClasses = {
        PaymentFileRepository.class
        , CustomerRepository.class
        , TemporalPaymentRepository.class
        , ReportRepository.class
        , UserRepository.class
        , PaymentProcessorRepository.class
        , TemporalInboundPaymentRepository.class
        , CardPaymentRepository.class})
@Slf4j
@EnableSwagger2
public class LocalApplication extends SpringApplication{

    static {
        //System.setProperty("h2.serializeJavaObject","false");
        System.setProperty("h2.javaObjectSerializer","gov.scot.payments.JacksonJavaObjectSerializer");
    }

    @Setter
    private LocalAppConfigBase config;

    @Setter
    private boolean browser;

    public LocalApplication(){
        super(LocalApplication.class);
        setApplicationContextClass(KafkaStreamsOverridingContext.class);
    }


    public static void main(String[] args) {
        LocalApplicationConfig app = new LocalApplicationConfig(true);
        DistributedLocalAppConfig distributedCommand = new DistributedLocalAppConfig(app);
        EmbeddedLocalAppConfig embeddedCommand = new EmbeddedLocalAppConfig(app);
        JCommander commander =JCommander.newBuilder()
                .addCommand("distributed",distributedCommand)
                .addCommand("embedded",embeddedCommand)
                .build();

        LocalHelpConfig helpConfig = new LocalHelpConfig();
        JCommander.newBuilder()
                .addObject(helpConfig)
                .acceptUnknownOptions(true)
                .build()
                .parse(args);
        if(helpConfig.isHelp()){
            commander.usage();
            System.exit(0);
        }

        commander.parse(args);
        String command = commander.getParsedCommand();
        LocalApplication application = new LocalApplication();
        if("distributed".equals(command)){
            application.setConfig(distributedCommand);
        } else {
            application.setConfig(embeddedCommand);
        }
        application.run(args);
    }

    @Override
    protected void configurePropertySources(ConfigurableEnvironment environment, String[] args) {
        MutablePropertySources sources = environment.getPropertySources();
        sources.addFirst(new MapPropertySource("commandLineProperties", this.config.getProperties()));
    }

    @Override
    public ConfigurableApplicationContext run(String... args) {
        Set<String> violations = config.validate();
        if(!violations.isEmpty()){
            throw new IllegalArgumentException(String.join("\n",violations));
        }
        if(config.getApp().getPidFile() != null){
            addListeners(new ApplicationPidFileWriter(config.getApp().getPidFile()));
        }
        if(config.getApp().getPortFile() != null){
            addListeners(new WebServerPortFileWriter(config.getApp().getPortFile()));
        }
        Set<String> profiles = config.getProfiles();
        setAdditionalProfiles(profiles.toArray(new String[0]));
        return super.run(args);
    }

    @Bean
    @Primary
    public Consumer<ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry> localEndpointCustomizer() {
        return r -> {
            r.antMatchers("/h2-console**","/h2-console/**","/auth**","/auth/**").permitAll()
                    .antMatchers(HttpMethod.GET,"/actuator/health**").permitAll()
                    .antMatchers("/resources/**").permitAll()
                    .antMatchers(HttpMethod.GET,"/cardpayments/*/form").permitAll()
                    .antMatchers(HttpMethod.POST,"/cardpayments/*/authorize","/cardpayments/*/submit","/cardpayments/*/cancel").permitAll();
        };
    }

    @Bean
    public TokenService tokenService(ObjectMapper mapper, Environment environment){
        return new TokenService(mapper, environment);
    }

    @Bean
    @Profile("embedded-gateway")
    public PaymentProcessorProxy paymentProcessorProxy(ServiceRegistry serviceRegistry
            , Map<String, GatewayDetailsService> gateways){
       return new MockPaymentProcessorProxy(serviceRegistry,gateways);
    }

    @Bean
    public ScheduledExecutorService registrationExecutor(){
        return Executors.newScheduledThreadPool(1);
    }

    @EventListener({WebServerInitializedEvent.class})
    public void onApplicationStarted(WebServerInitializedEvent event) throws IOException {
        WebServerApplicationContext context = event.getApplicationContext();
        registerOutboundGateways(context);
        registerInboundCardGateways(context);
        List<Customer> customers = registerCustomers(context);
        registerServices(context,customers);
        logBrokerDetails(context);
        logTokenDetails(context);
        log.info("APPLICATION PORT: {}",context.getEnvironment().getProperty("server.port"));
        openBrowser(context);
    }

    private void openBrowser(WebServerApplicationContext context)  {
        String port = context.getEnvironment().getProperty("server.port");
        String openBrowserProp = context.getEnvironment().getProperty("browser.open");
        boolean openBrowser = openBrowserProp != null && Boolean.valueOf(openBrowserProp);
        if(openBrowser){
            try {
                URI base = URI.create("http://localhost:"+port);
                Desktop.getDesktop().browse(base.resolve("/resources/swagger-ui.html"));
                Desktop.getDesktop().browse(base.resolve("/resources/ui/index.html"));
                if(Objects.equals(context.getEnvironment().getProperty("spring.h2.console.enabled"), "true")){
                    Desktop.getDesktop().browse(base.resolve("/h2-console"));
                }
            } catch (Exception e) {
                log.warn("Could not open browser");
            }
        }
    }

    private void logBrokerDetails(WebServerApplicationContext context) {
        List<String> activeProfiles = Arrays.asList(context.getEnvironment().getActiveProfiles());
        if(activeProfiles.contains("embedded-broker")){
            EmbeddedKafkaBroker broker = context.getBean(EmbeddedKafkaBroker.class);
            log.info("KAFKA BOOTSTRAP SERVERS: {}",broker.getBrokersAsString());
            log.info("ZK CONNECTION STRING: {}",broker.getZookeeperConnectionString());
        }
    }

    private void logTokenDetails(WebServerApplicationContext context) throws JsonProcessingException {
        List<String> activeProfiles = Arrays.asList(context.getEnvironment().getActiveProfiles());
        if(!activeProfiles.contains("cognito")){
            User admin = User.admin();
            ObjectMapper mapper = context.getBean(ObjectMapper.class);
            String token = Base64.getEncoder().encodeToString(mapper.writeValueAsBytes(admin));
            log.info("ADMIN USER ID TOKEN: {}",token);
        }
    }

    private void registerOutboundGateways(WebServerApplicationContext context) {
        String env = context.getEnvironment().getProperty("payments.environment");
        ServiceRegistry serviceRegistry = context.getBean(ServiceRegistry.class);
        Map<String,AbstractOutboundGateway> gateways = context.getBeansOfType(AbstractOutboundGateway.class);
        ZookeeperDiscoveryProperties properties = context.getBean(ZookeeperDiscoveryProperties.class);
        ConfigurableListableBeanFactory beanFactory = ((ConfigurableApplicationContext) context).getBeanFactory();
        for(AbstractOutboundGateway gateway : gateways.values()){
            String gatewayName = env+".gateway."+gateway.getName();
            Map<String, String> instanceStatusKey = new HashMap<>();
            instanceStatusKey.put(INSTANCE_STATUS_KEY, STATUS_UP);
            ZookeeperInstance zookeeperInstance = new ZookeeperInstance(gatewayName,
                    gatewayName, instanceStatusKey);
            Registration registration = ServiceInstanceRegistration.builder()
                    .address(properties.getInstanceHost())
                    .port(properties.getInstancePort())
                    .name(gatewayName)
                    .payload(zookeeperInstance)
                    .uriSpec(properties.getUriSpec())
                    .build();
            beanFactory.registerSingleton(gateway.getName()+"-registration",registration);
            serviceRegistry.register(registration);
        }
    }

    private void registerInboundCardGateways(WebServerApplicationContext context) {
        String env = context.getEnvironment().getProperty("payments.environment");
        ServiceRegistry serviceRegistry = context.getBean(ServiceRegistry.class);
        Map<String, CardPaymentGateway> gateways = context.getBeansOfType(CardPaymentGateway.class);
        ZookeeperDiscoveryProperties properties = context.getBean(ZookeeperDiscoveryProperties.class);
        ConfigurableListableBeanFactory beanFactory = ((ConfigurableApplicationContext) context).getBeanFactory();
        for(CardPaymentGateway gateway : gateways.values()){
            String gatewayName = env+".gateway."+gateway.getName();
            Map<String, String> instanceStatusKey = new HashMap<>();
            instanceStatusKey.put(INSTANCE_STATUS_KEY, STATUS_UP);
            ZookeeperInstance zookeeperInstance = new ZookeeperInstance(gatewayName,
                    gatewayName, instanceStatusKey);
            Registration registration = ServiceInstanceRegistration.builder()
                    .address(properties.getInstanceHost())
                    .port(properties.getInstancePort())
                    .name(gatewayName)
                    .payload(zookeeperInstance)
                    .uriSpec(properties.getUriSpec())
                    .build();
            beanFactory.registerSingleton(gateway.getName()+"-registration",registration);
            serviceRegistry.register(registration);
        }
    }

    private List<Customer> registerCustomers(WebServerApplicationContext context) {
        CustomerService customerService = context.getBean(CustomerService.class);
        List<Customer> customers = Binder.get(context.getEnvironment())
                .bind("customer", Bindable.mapOf(String.class, Object.class))
                .orElseGet(Collections::emptyMap)
                .entrySet()
                .stream()
                .map(subProperty -> {
                    String customerId = subProperty.getKey();
                    Object value = subProperty.getValue();
                    String customerName;
                    if(value instanceof String){
                        customerName = (String)value;
                    } else {
                        customerName = (String)((Map<String,Object>) value).get("name");
                    }
                    return Customer.builder()
                            .id(customerId)
                            .name(customerName)
                            .build();
                })
                .collect(Collectors.toList());

        registerEntities(context,customers,customerService::addCustomer);

        return customers;
    }

    private void registerServices(WebServerApplicationContext context, List<Customer> customers) {
        CustomerServiceService customerServiceService = context.getBean(CustomerServiceService.class);
        for(Customer customer : customers){
            List<Service> services = Binder.get(context.getEnvironment())
                    .bind("customer."+customer.getId()+".service", Bindable.mapOf(String.class, Object.class))
                    .orElseGet(Collections::emptyMap)
                    .entrySet()
                    .stream()
                    .map(subProperty -> {
                        String serviceId = subProperty.getKey();
                        Service service =  Service.builder()
                                .id(serviceId)
                                .customerId(customer.getId())
                                .build();
                        Object serviceDetails = subProperty.getValue();
                        if(serviceDetails instanceof Map){
                            Map<String, Object> serviceDetailsMap = (Map<String, Object>) serviceDetails;
                            String fileFormat = (String) serviceDetailsMap.get("file-format");
                            if(fileFormat != null){
                                service =  service.toBuilder()
                                        .folder(serviceId)
                                        .fileFormat(fileFormat)
                                        .build();
                            }
                            Map<String,Object> ruleDetails = (Map<String,Object>)serviceDetailsMap.get("rule");
                            if(ruleDetails != null){
                                PaymentValidationRule rule = PaymentValidationRule.builder()
                                        .messageTemplate((String)ruleDetails.get("message"))
                                        .name((String)ruleDetails.get("name"))
                                        .rule((String)ruleDetails.get("test"))
                                        .build();
                                service = service.toBuilder().addRule(rule).build();
                            }
                        }
                        return service;
                    })
                    .collect(Collectors.toList());

            registerEntities(context,services,customerServiceService::addService);
        }


    }

    private <T> void registerEntities(WebServerApplicationContext context, List<T> entities, BiConsumer<T, Client> registration) {
        ScheduledExecutorService executorService = context.getBean(ScheduledExecutorService.class);
        executorService.schedule(() -> {
            try{
                User applicationUser = User.builder()
                        .email("application")
                        .userName("application")
                        .withRole(Role.WILDCARD_ROLE)
                        .build();

                SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(applicationUser, "N/A", applicationUser.getAuthorities()));
                entities.forEach(e -> registration.accept(e,applicationUser));
            } finally {
                SecurityContextHolder.clearContext();
            }

        },5, TimeUnit.SECONDS);
    }

}
