package gov.scot.payments;

import com.beust.jcommander.JCommander;
import gov.scot.payments.broker.EmbeddedBrokerApplication;
import gov.scot.payments.broker.EmbeddedBrokerConfig;
import gov.scot.payments.broker.EmbeddedDatabaseApplication;
import gov.scot.payments.common.local.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.util.SocketUtils;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.*;

public class DistributedApplicationWrapper {

    public static void main(String[] args) throws IOException, SQLException {
        LocalConfig.parseHelp(DistributedLocalAppWrapperConfig::new,args);
        DistributedLocalAppWrapperConfig config = new DistributedLocalAppWrapperConfig();
        JCommander.newBuilder()
                .addObject(config)
                .build()
                .parse(args);
        Set<String> violations = config.validate();
        if(!violations.isEmpty()){
            throw new IllegalArgumentException(String.join("\n",violations));
        }

        String env = config.getApp().getEnvironment();

        int numPorts = 6 + config.getInbound().size() + config.getOutbound().size();
        ArrayList<Integer> ports = new ArrayList<>(SocketUtils.findAvailableTcpPorts(numPorts));

        int brokerPort = ports.remove(0);
        int dbWebPort = ports.remove(0);
        int dbServerPort = ports.remove(0);
        int corePort = ports.remove(0);
        int s3Port = ports.remove(0);
        int customerServicePort = ports.remove(0);
        List<Integer> inboundPorts = ports.subList(0,config.getInbound().size());
        config.setInboundPorts(new ArrayList<>(inboundPorts));
        inboundPorts.clear();
        List<Integer> outboundPorts = ports.subList(0,config.getOutbound().size());
        config.setOutboundPorts(new ArrayList<>(outboundPorts));
        outboundPorts.clear();

        //start broker
        EmbeddedBrokerApplication broker = startBroker(brokerPort,new File(config.getApp().getTmpDir(),"kafka-logs").getAbsolutePath(),config.getPrefixedTopics());
        LocalKafkaConfig kafkaConfig = new LocalKafkaConfig(broker.getKafkaBrokers());

        //start db
        LocalDatabaseConfig databaseConfig = startDb(dbWebPort,dbServerPort,env,new File(config.getApp().getTmpDir(),"db").getAbsolutePath());

        //start core
        DistributedLocalAppConfig coreConfig = config.toCoreConfig(new File(config.getApp().getTmpDir(),"core").getAbsolutePath(),corePort,s3Port,new LocalZookeeperConfig(false, broker.getZookeeperServers()), databaseConfig, kafkaConfig);
        startCore(coreConfig);

        //write gateway details in to files
        LocalZookeeperConfig zookeeperConfig = new LocalZookeeperConfig(true,broker.getZookeeperServers());
        config.getInboundGatewayConfigs(databaseConfig,zookeeperConfig)
                .forEach(c -> writeArgFile(c.getT1()+".inbound.args",c.getT2()));

        config.getOutboundGatewayConfigs(databaseConfig,zookeeperConfig,kafkaConfig)
                .forEach(c -> writeArgFile(c.getT1()+".outbound.args",c.getT2()));

        writeArgFile("customer-site.port",new String[]{customerServicePort+""});
    }

    private static void writeArgFile(String name, String[] content) {
        try {
            FileUtils.writeStringToFile(new File(name), String.join("\n",content), Charset.defaultCharset());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private static void startCore(DistributedLocalAppConfig coreConfig) {
        LocalApplication core = new LocalApplication();
        core.setConfig(coreConfig);
        core.run();
    }

    private static LocalDatabaseConfig startDb(int webPort, int serverPort,String env,String tmpDir) throws IOException, SQLException {
        EmbeddedDatabaseApplication db = new EmbeddedDatabaseApplication(webPort,serverPort,env,tmpDir);
        db.start();
        FileUtils.writeStringToFile(new File("db.port"),db.getWebPort(),Charset.defaultCharset());
        return new LocalDatabaseConfig(db.getUrl());
    }

    private static EmbeddedBrokerApplication startBroker(int port,String logDir,String[] config) {
        EmbeddedBrokerApplication broker = new EmbeddedBrokerApplication(port,logDir,config);
        broker.start();
        return broker;
    }

}
