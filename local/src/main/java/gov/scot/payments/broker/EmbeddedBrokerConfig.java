package gov.scot.payments.broker;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParametersDelegate;
import gov.scot.payments.common.local.LocalApplicationConfig;
import gov.scot.payments.common.local.LocalConfig;
import lombok.Data;

import java.util.*;

@Data
public class EmbeddedBrokerConfig implements LocalConfig {

    public static final List<String> TOPICS = Arrays.asList(
            "inboundPaymentReceivedEvents"
            ,"inboundPaymentAttemptFailedEvents"
            ,"inboundPaymentClearedEvents"
            ,"inboundPaymentFailedToClearEvents"
            ,"inboundPaymentSettledEvents"
            ,"paymentSubmitSuccessEvents"
            ,"paymentSubmitFailedEvents"
            ,"paymentAcceptedEvents"
            ,"paymentRejectedEvents"
            ,"paymentClearedEvents"
            ,"paymentFailedToClearEvents"
            ,"paymentSettledEvents"
            ,"paymentValidationSuccessEvents"
            ,"paymentValidationFailedEvents"
            ,"paymentRoutingFailedEvents"
            ,"paymentCreatedEvent"
    );

    @ParametersDelegate private LocalApplicationConfig app = new LocalApplicationConfig(true);

    @Parameter(names = "-topics", listConverter = SemiColonListConverter.class)
    private List<String> topics = TOPICS;

    @Override
    public Map<String, Object> getProperties() {
        return app.getProperties();
    }

    @Override
    public String[] toArgs() {
        return List.of("-topics",String.join(",",topics)).toArray(new String[0]);
    }

    public String[] getPrefixedTopics(){
        return topics.stream().map(t -> app.getEnvironment()+"-"+t).toArray(String[]::new);
    }

    public static class SemiColonListConverter implements IStringConverter<List<String>> {

        @Override
        public List<String> convert(String value) {
            return Arrays.asList(value.split(";"));
        }
    }
}
