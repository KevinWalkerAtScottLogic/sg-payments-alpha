package gov.scot.payments.broker;

import com.beust.jcommander.JCommander;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.h2.tools.Server;
import org.springframework.util.SocketUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.SortedSet;

@Slf4j
public class EmbeddedDatabaseApplication {

    static {
        //System.setProperty("h2.serializeJavaObject","false");
        System.setProperty("h2.javaObjectSerializer","gov.scot.payments.JacksonJavaObjectSerializer");
    }

    private final int webPort;
    private final int serverPort;
    private final String env;
    private final String tmpDir;

    private Server dbServer;
    private Server webServer;

    public EmbeddedDatabaseApplication(int webPort, int serverPort, String env, String tmpDir){
        this.env = env;
        this.webPort = webPort;
        this.serverPort = serverPort;
        this.tmpDir = tmpDir;
    }

    public EmbeddedDatabaseApplication(String env,String tmpDir) {
        this.env = env;
        SortedSet<Integer> availableTcpPorts = SocketUtils.findAvailableTcpPorts(2);
        webPort = availableTcpPorts.first();
        serverPort = availableTcpPorts.last();
        File tmpFile = new File(tmpDir,"db");
        tmpFile.deleteOnExit();
        this.tmpDir = tmpFile.getAbsolutePath();
    }

    public static void main(String[] args) throws IOException, SQLException {
        EmbeddedDatabaseConfig brokerConfig = new EmbeddedDatabaseConfig();
        JCommander.newBuilder()
                .addObject(brokerConfig)
                .build()
                .parse(args);
        String env = brokerConfig.getApp().getEnvironment();
        EmbeddedDatabaseApplication db = new EmbeddedDatabaseApplication(env,brokerConfig.getApp().getTmpDir());
        db.start();
    }

    public void start() throws IOException, SQLException {
        dbServer = Server.createTcpServer("-tcpAllowOthers","-tcpPort",serverPort+"","-ifNotExists","-baseDir",tmpDir).start();
        webServer = Server.createWebServer("-webPort",webPort+"","-ifNotExists").start();
        log.info("DB SERVER PORT: {}",dbServer.getPort());
        log.info("DB SERVER PATH: {}",getUrl());
        log.info("DB WEB PORT: {}",webServer.getPort());
    }

    public void stop(){
        dbServer.stop();
        webServer.stop();
    }

    public String getWebPort(){
        return webPort+"";
    }

    public String getUrl(){
        return "jdbc:h2:tcp://localhost:"+serverPort+"/mem:"+env+"db";
    }
}
