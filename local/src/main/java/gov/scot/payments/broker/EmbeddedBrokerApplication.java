package gov.scot.payments.broker;

import com.beust.jcommander.JCommander;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import kafka.server.KafkaConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.h2.tools.Server;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.util.SocketUtils;
import reactor.util.function.Tuple3;
import reactor.util.function.Tuples;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.concurrent.TimeUnit;

@Slf4j
public class EmbeddedBrokerApplication {

    private final EmbeddedKafkaBroker broker;

    public EmbeddedBrokerApplication(int port,String logDir,String[] topics){
        broker = new EmbeddedKafkaBroker(1, true, 1,topics);
        broker.brokerProperty(KafkaConfig.LogDirProp(),logDir);
        broker.kafkaPorts(port);
    }

    public EmbeddedBrokerApplication(String logDir,String[] topics){
        this(0,logDir,topics);
    }

    public void start(){
        broker.afterPropertiesSet();
        log.info("KAFKA BOOTSTRAP SERVERS: {}",broker.getBrokersAsString());
        log.info("ZK CONNECTION STRING: {}",broker.getZookeeperConnectionString());
    }

    public String getKafkaBrokers(){
        return broker.getBrokersAsString();
    }

    public String getZookeeperServers(){
        return broker.getZookeeperConnectionString();
    }

    public static void main(String[] args) throws Exception {
        EmbeddedBrokerConfig brokerConfig = new EmbeddedBrokerConfig();
        JCommander.newBuilder()
                .addObject(brokerConfig)
                .build()
                .parse(args);
        EmbeddedBrokerApplication broker = new EmbeddedBrokerApplication(brokerConfig.getApp().getTmpDir(),brokerConfig.getPrefixedTopics());
        broker.start();
    }
}
