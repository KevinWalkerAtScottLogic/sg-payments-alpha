package gov.scot.payments.broker;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParametersDelegate;
import gov.scot.payments.common.local.LocalApplicationConfig;
import gov.scot.payments.common.local.LocalConfig;
import lombok.Data;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Data
public class EmbeddedDatabaseConfig implements LocalConfig {

    @ParametersDelegate private LocalApplicationConfig app = new LocalApplicationConfig(true);

    @Override
    public Map<String, Object> getProperties() {
        return app.getProperties();
    }

    @Override
    public String[] toArgs() {
        return app.toArgs();
    }

}
