package gov.scot.payments;

import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.messaging.config.annotation.EnableSqs;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("s3")
@Import({ContextResourceLoaderAutoConfiguration.class})
@EnableSqs
public class S3Configuration {
}
