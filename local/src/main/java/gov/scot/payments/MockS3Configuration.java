package gov.scot.payments;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import io.findify.s3mock.S3Mock;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.util.SocketUtils;

import java.nio.file.Path;

@Configuration
@Profile("!s3")
public class MockS3Configuration {

    @Bean
    public Integer s3Port(@Value("${s3.port:}") String port){
        if(StringUtils.isEmpty(port)){
            return SocketUtils.findAvailableTcpPort();
        } else{
            return Integer.valueOf(port);
        }

    }

    @Bean(initMethod = "start", destroyMethod = "shutdown")
    public S3Mock s3Server(@Qualifier("s3Port") Integer s3Port
                            , @Value("${s3.file.path}") String path){
        return new S3Mock.Builder()
                .withPort(s3Port)
                .withFileBackend(Path.of(path).getParent().toString())
                .build();
    }

    @Bean
    public AmazonS3 localS3Client(S3Mock s3Server, @Qualifier("s3Port") Integer s3Port){
        AwsClientBuilder.EndpointConfiguration endpoint = new AwsClientBuilder.EndpointConfiguration("http://localhost:"+s3Port, "eu-west-1");
        return AmazonS3ClientBuilder
                .standard()
                .withPathStyleAccessEnabled(true)
                .withEndpointConfiguration(endpoint)
                .withCredentials(new AWSStaticCredentialsProvider(new AnonymousAWSCredentials()))
                .build();
    }

}
