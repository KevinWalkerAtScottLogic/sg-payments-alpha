package gov.scot.payments;

import com.beust.jcommander.Parameter;
import gov.scot.payments.common.local.LocalConfig;
import lombok.Data;

import java.io.File;
import java.util.*;

@Data
public class LocalS3Config implements LocalConfig{

    @Parameter(names = "-s3", description = "Specify if you wish to use S3 for file storage")
    private boolean s3 = false;

    @Parameter(names = "-s3.path"
            ,description = "Only required if not using S3, the file system location in which all service upload folders should be created")
    private String path;

    @Parameter(names = "-s3.port",hidden = true)
    private Integer port;

    @Parameter(names = "-s3.path.autocreate",hidden = true)
    private boolean autoCreate;

    @Override
    public Set<String> getProfiles(){
        if(s3){
            return Set.of("s3");
        }
        else{
            return Collections.emptySet();
        }
    }

    @Override
    public Map<String, Object> getProperties(){
        if(!s3){
            Map<String,Object> properties = new HashMap<>();
            properties.put("s3.file.path",new File(path).getAbsolutePath());
            if(port != null){
                properties.put("s3.port",port);
            }
            return properties;
        } else{
            return Collections.emptyMap();
        }
    }

    @Override
    public String[] toArgs() {
        List<String> args = new ArrayList<>();
        if(!s3){
            if(path != null){
                args.add("-s3.path");
                args.add(LocalConfig.q(path));
            }
            if(port != null){
                args.add("-s3.port");
                args.add(port+"");
            }
            if(autoCreate){
                args.add("-s3.path.autocreate");
            }
        }
        return args.toArray(new String[0]);
    }

    @Override
    public Set<String> validate() {
        if(s3){
            return Collections.emptySet();
        }
        Set<String> violations = new HashSet<>();
        if(path == null){
            violations.add("s3.path must be provided if using mock-s3");
        } else {
            File file = new File(path);
            if(!file.exists()){
                if(autoCreate){
                    file.mkdirs();
                } else{
                    violations.add("s3.path must exist");
                }
            } else {
                if(!file.isDirectory()){
                    violations.add("s3.path must be a directory");
                }
            }
        }
        return violations;
    }

    @Override
    public String toString() {
        return String.join(" ", toArgs());
    }
}
