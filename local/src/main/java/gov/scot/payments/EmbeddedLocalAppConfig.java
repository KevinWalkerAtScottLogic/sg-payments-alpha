package gov.scot.payments;

import com.beust.jcommander.DynamicParameter;
import com.beust.jcommander.Parameter;
import gov.scot.payments.broker.EmbeddedBrokerConfig;
import gov.scot.payments.common.local.LocalApplicationConfig;
import gov.scot.payments.common.local.LocalConfig;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.util.StringUtils;

import java.util.*;

@Data
@EqualsAndHashCode(callSuper = false)
public class EmbeddedLocalAppConfig extends LocalAppConfigBase {

    public EmbeddedLocalAppConfig(LocalApplicationConfig app){
        super(app);
    }

    private static final List<String> TOPICS;
    static {
        TOPICS = new ArrayList<>(EmbeddedBrokerConfig.TOPICS);
        TOPICS.addAll(Arrays.asList(
                "mock-accesspayPaymentEvents"
                ,"mock-bottomlinePaymentEvents"));
    }

    @Override
    public Set<String> getProfiles() {
        Set<String> profiles =  super.getProfiles();
        profiles.add("embedded-gateway");
        profiles.add("embedded-broker");
        profiles.add("mock-stripe");
        profiles.add("mock-worldpay");
        profiles.add("mock-accesspay");
        profiles.add("mock-bottomline");
        return profiles;
    }

    @Override
    public Map<String, Object> getProperties() {
        Map<String, Object> properties = super.getProperties();
        properties.put("payments.inbound.card.proxyMode","direct");
        properties.put("payments.router.cache-expiry-seconds",10);
        properties.put("kafka.embedded.topics",String.join(",",TOPICS));
        properties.put("spring.h2.console.enabled",true);
        properties.put("spring.h2.console.settings.web-allow-others",true);
        return properties;
    }

    @Override
    public Set<String> validate() {
        Set<String> violations = super.validate();
        if(services != null) {
            if (services.stream().map(LocalServiceConfig::fromString).map(LocalServiceConfig::getCustomer).anyMatch(s -> !customers.contains(s))) {
                violations.add("service must use a valid customer name");
            }
        }
        return violations;
    }

    @Override
    public String[] toArgs() {
        List<String> args = new ArrayList<>(Arrays.asList(super.toArgs()));
        return args.toArray(new String[0]);
    }
}
