package gov.scot.payments;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthRequest;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthResult;
import com.amazonaws.services.cognitoidp.model.AuthFlowType;
import com.amazonaws.services.cognitoidp.model.AuthenticationResultType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CognitoUtil {

    //environment variables needed:
    //COGNITO_APP_CLIENT_ID
    //COGNITO_USER_POOL_ID
    //AWS_ACCESS_KEY_ID
    //AWS_SECRET_KEY
    //AWS_REGION

    interface LineReader {
        String read();
        default String readPassword(){
            return read();
        }
    }

    interface LineWriter {
        void write(String s);
    }

    public static void main(String[] args) {
        String mode = args[0];
        String region = System.getenv("AWS_REGION");
        String appClientId = System.getenv("COGNITO_APP_CLIENT_ID");
        String domain = System.getenv("COGNITO_DOMAIN");
        String userPool = System.getenv("COGNITO_USER_POOL_ID");
        String issuer = "https://cognito-idp." + region + ".amazonaws.com/" + userPool;
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new Jdk8Module());
        mapper.registerModule(new JavaTimeModule());

        LineWriter out = getLineWriter();
        LineReader in = getLineReader();

        if(mode.equals("user")){
            final Map<String, String> authParams = userAuth(out,in);
            AuthenticationResultType auth = doAdminAuth(appClientId, userPool, region,authParams);
            out.write("Id Token: ");
            out.write(auth.getIdToken());
            out.write("");
            out.write("Access Token: ");
            out.write(auth.getAccessToken());
            out.write("");
            out.write("Expires At: ");
            out.write(LocalDateTime.now().plusSeconds(auth.getExpiresIn()).toString());
            out.write("");
            out.write("Access Token Claims: ");
            out.write(JwtHelper.decode(auth.getAccessToken()).getClaims());
            out.write("");
            out.write("Id Token Claims: ");
            out.write(JwtHelper.decode(auth.getIdToken()).getClaims());
            out.write("");
        } else{
            String appClientSecret = System.getenv("COGNITO_APP_CLIENT_SECRET");
            if(appClientSecret == null){
                Tuple2<String,String> authParams = apiAuth(out,in);
                appClientSecret = authParams.getT1();
            }
            RestOperations ops = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setBasicAuth(appClientId,appClientSecret);
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            String baseUri = "https://" + domain + ".auth." + region + ".amazoncognito.com";
            URI uri = URI.create(baseUri + "/oauth2/token");
            MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
            body.add("grant_type","client_credentials");
            //body.add("scope",authParams.getT2());
            RequestEntity<MultiValueMap<String, String>> entity = new RequestEntity<>(body,headers, HttpMethod.GET,uri);
            Map<String,Object> response = ops.postForEntity(uri,entity,Map.class).getBody();
            String token = response.get("access_token").toString();
            out.write("Access Token: ");
            out.write(token);
            out.write("");
            out.write("Expires At: ");
            out.write(LocalDateTime.now().plusSeconds((int)response.get("expires_in")).toString());
            out.write("");
            out.write("Claims: ");
            out.write(JwtHelper.decode(token).getClaims());

        }

    }

    private static Tuple2<String,String> apiAuth(LineWriter out, LineReader in) {
        String clientSecret = System.getenv("COGNITO_APP_CLIENT_SECRET");
        return Tuples.of(clientSecret,"");
    }

    private static Map<String, String> userAuth(LineWriter out, LineReader in) {
        out.write("Username: ");
        String username = in.read();
        out.write("Password: ");
        String password = in.readPassword();

        final Map<String, String> authParams = new HashMap<>();
        authParams.put("USERNAME", username);
        authParams.put("PASSWORD", password);
        return authParams;

    }

    private static AuthenticationResultType doAdminAuth(String appClientId, String userPool, String region, Map<String, String> authParams) {
        AWSCognitoIdentityProvider cognitoClient = AWSCognitoIdentityProviderClientBuilder.standard()
                .withCredentials(new DefaultAWSCredentialsProviderChain())
                .withRegion(region)
                .build();

        final AdminInitiateAuthRequest authRequest = new AdminInitiateAuthRequest();

        authRequest.withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
                .withClientId(appClientId)
                .withUserPoolId(userPool)
                .withAuthParameters(authParams);

        AdminInitiateAuthResult result = cognitoClient.adminInitiateAuth(authRequest);
        cognitoClient.shutdown();
        return result.getAuthenticationResult();
    }

    private static LineWriter getLineWriter() {
        if(System.console() == null){
            return System.out::println;
        } else {
            return s -> System.console().writer().println(s);
        }
    }

    private static LineReader getLineReader() {
        if(System.console() == null){
            Scanner scanner = new Scanner(System.in);
            return () -> scanner.next();
        } else{
            return new LineReader() {
                @Override
                public String read() {
                    return System.console().readLine();
                }

                @Override
                public String readPassword() {
                    return new String(System.console().readPassword());
                }
            };
        }
    }

}
