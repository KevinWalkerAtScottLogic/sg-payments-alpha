package gov.scot.payments;

import gov.scot.payments.common.gateway.GatewayDetailsService;
import gov.scot.payments.common.gateway.inbound.cardpayment.CardPaymentGateway;
import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.gateway.inbound.cardpayment.GatewayCardPaymentService;
import gov.scot.payments.gateway.inbound.cardpayment.mock.MockInboundCardPaymentGateway;
import gov.scot.payments.gateway.inbound.cardpayment.spring.AbstractCardPaymentGatewayConfiguration;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.web.bind.annotation.RequestMapping;

@Configuration
@Profile("mock-worldpay")
public class MockWorldpayConfiguration extends AbstractCardPaymentGatewayConfiguration {

    @Bean("mock-worldpay")
    public WorldpayGatewayDetailsService worldpayGatewayDetailsService(PaymentProcessorRepository paymentProcessorRepository){
        return new WorldpayGatewayDetailsService(paymentProcessorRepository,getGatewayName());
    }

    @Bean
    public WorldpayGatewayPaymentService worldpayGatewayPaymentService(@Qualifier("mockWorldpayGateway") CardPaymentGateway gateway){
        return new WorldpayGatewayPaymentService(gateway);
    }

    @Bean
    public MockInboundCardPaymentGateway mockWorldpayGateway(@Value("${mock-worldpay.submitExpression}") String submit
            , @Value("${mock-worldpay.authExpression}") String auth){
        ExpressionParser mockGatewayExpressionParser = new SpelExpressionParser();
        Expression authExpression = mockGatewayExpressionParser.parseExpression(auth);
        Expression submitExpression = mockGatewayExpressionParser.parseExpression(submit);
        return new MockInboundCardPaymentGateway(getGatewayName(),authExpression, submitExpression);
    }

    @Override
    protected String getGatewayName() {
        return "mock-worldpay";
    }

    @RequestMapping(value = "/mock-worldpay/details")
    public class WorldpayGatewayDetailsService extends GatewayDetailsService {
        public WorldpayGatewayDetailsService(PaymentProcessorRepository repository, String name) {
            super(repository, name);
        }
    }

    @RequestMapping(value = "/mock-worldpay")
    public class WorldpayGatewayPaymentService extends GatewayCardPaymentService {
        public WorldpayGatewayPaymentService(CardPaymentGateway gateway) {
            super(gateway);
        }
    }
}
