package gov.scot.payments;

import gov.scot.payments.common.gateway.GatewayDetailsService;
import gov.scot.payments.common.gateway.spring.AbstractGatewayConfiguration;
import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.gateway.outbound.PaymentInstructionBatcher;
import gov.scot.payments.gateway.outbound.PaymentInstructionBatchers;
import gov.scot.payments.gateway.outbound.mock.MockOutboundGatewayBase;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingSuccessEvent;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import gov.scot.payments.model.paymentinstruction.event.PaymentSubmitFailedEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentSubmitSuccessEvent;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.converter.CompositeMessageConverterFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Configuration
@Profile("mock-accesspay")
public class MockAccessPayConfiguration extends AbstractGatewayConfiguration {

    @Bean("mock-accesspay")
    public AccessPayGatewayDetailsService accessPayGatewayDetailsService(PaymentProcessorRepository paymentProcessorRepository){
        return new AccessPayGatewayDetailsService(paymentProcessorRepository,getGatewayName());
    }

    @Bean
    public MockAccessPay mockAccessPayGateway(@Qualifier("payment-accepted-events") MessageChannel acceptedEvents
            , @Qualifier("payment-rejected-events") MessageChannel rejectedEvents
            , @Qualifier("payment-cleared-events") MessageChannel clearedEvents
            , @Qualifier("payment-failedToClear-events") MessageChannel failedToClearEvents
            , @Qualifier("payment-settled-events") MessageChannel settledEvents
            , @Value("${mock-accesspay.submitExpression}") String submit
            , @Value("${mock-accesspay.acceptExpression}") String accept
            , @Value("${mock-accesspay.clearExpression}") String clear
            , Map<String,PaymentInstructionBatcher> batchers){
        ExpressionParser mockGatewayExpressionParser = new SpelExpressionParser();
        Expression submitExpression = mockGatewayExpressionParser.parseExpression(submit);
        Expression acceptExpression = mockGatewayExpressionParser.parseExpression(accept);
        Expression clearExpression = mockGatewayExpressionParser.parseExpression(clear);

        return new MockAccessPay(acceptedEvents
                ,rejectedEvents
                ,clearedEvents
                ,failedToClearEvents
                ,settledEvents
                , Executors.newScheduledThreadPool(1)
                , getGatewayName()
                ,submitExpression
                ,acceptExpression
                ,clearExpression
                ,batchers.entrySet().stream().filter(e -> e.getKey().startsWith("accesspay")).map(Map.Entry::getValue).toArray(PaymentInstructionBatcher[]::new));
    }

    @Bean
    PaymentInstructionBatcher accesspayFpBatcher(CompositeMessageConverterFactory compositeNonNativeSerde){
        return PaymentInstructionBatchers
                .<String,String>countWithTimeout(1,null)
                .batchNameExtractor(PaymentInstructionBatchers.paymentFileBatchNaming())
                .branchPredicate(PaymentChannel.FasterPayments.name(), PaymentInstructionBatchers.channelTypePredicate())
                .groupingFunction(PaymentInstructionBatchers.noGrouping(compositeNonNativeSerde))
                .build();
    }

    @Bean
    PaymentInstructionBatcher acccesspayBacsBatcher(CompositeMessageConverterFactory compositeNonNativeSerde){
        return PaymentInstructionBatchers
                .<String,String>countWithTimeout(1,null)
                .batchNameExtractor(PaymentInstructionBatchers.paymentFileBatchNaming())
                .branchPredicate(PaymentChannel.Bacs.name(), PaymentInstructionBatchers.channelTypePredicate())
                .groupingFunction(PaymentInstructionBatchers.noGrouping(compositeNonNativeSerde))
                .build();
    }

    @Override
    protected String getGatewayName() {
        return "mock-accesspay";
    }

    public interface MockAccessPayInputBinding {

        @Input("mock-accesspay-payment-events")
        KStream<?, PaymentRoutingSuccessEvent> paymentEvents();

        @Output("mock-accesspay-payment-submit-failure-events")
        KStream<?, PaymentSubmitFailedEvent> submitFailureEvents();

        @Output("mock-accesspay-payment-submit-success-events")
        KStream<?, PaymentSubmitSuccessEvent> submitSuccessEvents();
    }

    public class MockAccessPay extends MockOutboundGatewayBase<PaymentChannel> {

        public MockAccessPay(MessageChannel acceptedEvents
                , MessageChannel rejectedEvents
                , MessageChannel clearedEvents
                , MessageChannel failedToClearEvents
                , MessageChannel settledEvents
                , ScheduledExecutorService taskExecutor
                , String name
                , Expression submitExpression
                , Expression acceptExpression
                , Expression clearExpression
                , PaymentInstructionBatcher... batchers) {
            super(acceptedEvents
                    , rejectedEvents
                    , clearedEvents
                    , failedToClearEvents
                    , settledEvents
                    , taskExecutor
                    , name
                    , submitExpression
                    , acceptExpression
                    , clearExpression
                    , batchers);
        }

        @StreamListener
        @SendTo({"mock-accesspay-payment-submit-success-events","mock-accesspay-payment-submit-failure-events"})
        public KStream<?, ?>[] handleMockAccessPayPaymentEvents(@Input("mock-accesspay-payment-events") KStream<?, PaymentRoutingSuccessEvent> events) {
            return handlePaymentEvents(events);
        }
    }

    @RequestMapping(value = "/mock-accesspay/details")
    @ApiIgnore
    public class AccessPayGatewayDetailsService extends GatewayDetailsService {
        public AccessPayGatewayDetailsService(PaymentProcessorRepository repository, String name) {
            super(repository, name);
        }
    }
}
