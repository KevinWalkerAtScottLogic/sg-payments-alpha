package gov.scot.payments;

import com.amazonaws.services.quicksight.AmazonQuickSight;
import com.amazonaws.services.quicksight.model.GetDashboardEmbedUrlResult;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import gov.scot.payments.report.AmazonQuickSightClientFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Configuration
@Profile("!quicksight")
public class MockQuicksightConfiguration {

    @Bean
    public AmazonQuickSightClientFactory amazonQuickSightClientFactory(AmazonQuickSight amazonQuickSight){
        AmazonQuickSightClientFactory factory = mock(AmazonQuickSightClientFactory.class);
        when(factory.listUsersClient()).thenReturn(amazonQuickSight);
        when(factory.registerUserClient(any())).thenReturn(amazonQuickSight);
        when(factory.embedUrlClient(any())).thenReturn(amazonQuickSight);
        return factory;
    }

    @Bean
    public AmazonQuickSight amazonQuickSight(){
        AmazonQuickSight quickSight = mock(AmazonQuickSight.class);
        GetDashboardEmbedUrlResult result = new GetDashboardEmbedUrlResult();
        result.setStatus(200);
        result.setEmbedUrl("");
        when(quickSight.getDashboardEmbedUrl(any())).thenReturn(result);
        return quickSight;
    }
}
