package gov.scot.payments;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.NumberSerializers;
import gov.scot.payments.model.user.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import java.io.IOException;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.Set;

@RequestMapping("/auth")
public class TokenService {

    private final ObjectMapper mapper;
    private final Environment environment;

    public TokenService(ObjectMapper mapper, Environment environment) {
        this.mapper = mapper;
        this.environment = environment;
    }


    @RequestMapping(value = "/oauth_token"
            ,method = RequestMethod.POST
            ,consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    public Token getOauthToken(@RequestBody MultiValueMap<String,String> paramMap) throws JsonProcessingException {
        String token = getUserToken();
        return new Token(token,3600,"Bearer");
    }

    @RequestMapping(value = "/token"
            ,method = RequestMethod.GET
            ,produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public ResponseEntity<String> getToken(@RequestParam("redirect_uri") String uri) throws JsonProcessingException {
        String token = getUserToken();
        String uriToUse = String.format("%s#/id_token=%s&access_token=%s",uri,token,token);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create(uriToUse));
        return new ResponseEntity<>(headers, HttpStatus.FOUND);
    }

    @RequestMapping(produces = MediaType.TEXT_PLAIN_VALUE
            ,method = RequestMethod.GET)
    @ResponseBody
    public String getAuthMode(){
        if(Set.of(environment.getActiveProfiles()).contains("cognito")){
            return "cognito";
        } else {
            return "mock";
        }
    }

    private String getUserToken() throws JsonProcessingException {
        User user = User.admin();
        byte[] json = mapper.writeValueAsBytes(user);
        return Base64.getEncoder().encodeToString(json);
    }

    @Data
    @AllArgsConstructor
    public static final class Token {

        @JsonProperty("access_token")
        private String accessToken;

        @JsonProperty("expires_in")
        private Integer expiresIn;

        @JsonProperty("token_type")
        private String tokenType;

    }


}
