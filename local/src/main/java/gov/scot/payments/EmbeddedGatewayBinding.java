package gov.scot.payments;

import gov.scot.payments.cardpayment.spring.CardPaymentServiceBinding;
import gov.scot.payments.customer.spring.CustomerPersistenceBindings;
import gov.scot.payments.customer.spring.CustomerServiceBindings;
import gov.scot.payments.customer.spring.CustomerServiceServiceBindings;
import gov.scot.payments.customer.spring.ServicePersistenceBindings;
import gov.scot.payments.gateway.outbound.spring.CoreOutboundGatewayBinding;
import gov.scot.payments.inboundpayment.spring.InboundPaymentPersistenceBinding;
import gov.scot.payments.notification.spring.NotificationProcessorInputBinding;
import gov.scot.payments.paymentfile.spring.PaymentFilePersistenceProcessorInputBinding;
import gov.scot.payments.paymentfile.spring.PaymentFileProcessorInputBinding;
import gov.scot.payments.paymentfile.spring.PaymentFileProcessorOutputBinding;
import gov.scot.payments.paymentinstruction.spring.PaymentCreationBinding;
import gov.scot.payments.paymentinstruction.spring.PaymentPersistenceBinding;
import gov.scot.payments.router.spring.PaymentRouterBinding;
import gov.scot.payments.validation.spring.ValidationProcessorBinding;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

@EnableBinding({ PaymentFilePersistenceProcessorInputBinding.class
        , PaymentFileProcessorInputBinding.class
        , PaymentFileProcessorOutputBinding.class
        , CustomerPersistenceBindings.class
        , CustomerServiceBindings.class
        , CustomerServiceServiceBindings.class
        , ServicePersistenceBindings.class
        , ValidationProcessorBinding.class
        , NotificationProcessorInputBinding.class
        , PaymentRouterBinding.class
        , PaymentPersistenceBinding.class
        , PaymentCreationBinding.class
        , CoreOutboundGatewayBinding.class
        , InboundPaymentPersistenceBinding.class
        , CardPaymentServiceBinding.class
        , MockBottomlineConfiguration.MockBottomlineInputBinding.class
        , MockAccessPayConfiguration.MockAccessPayInputBinding.class})
@Configuration
@Profile("embedded-gateway")
@Import({MockAccessPayConfiguration.class
        , MockBottomlineConfiguration.class
        , MockStripeConfiguration.class
        , MockWorldpayConfiguration.class})
public class EmbeddedGatewayBinding {
}
