package gov.scot.payments;

import gov.scot.payments.common.gateway.GatewayDetailsService;
import gov.scot.payments.common.gateway.spring.AbstractGatewayConfiguration;
import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.gateway.outbound.PaymentInstructionBatcher;
import gov.scot.payments.gateway.outbound.PaymentInstructionBatchers;
import gov.scot.payments.gateway.outbound.mock.MockOutboundGatewayBase;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingSuccessEvent;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import gov.scot.payments.model.paymentinstruction.event.PaymentSubmitFailedEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentSubmitSuccessEvent;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.converter.CompositeMessageConverterFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Configuration
@Profile("mock-bottomline")
public class MockBottomlineConfiguration extends AbstractGatewayConfiguration {

    @Bean("mock-bottomline")
    public BottomlineGatewayDetailsService bottomlineGatewayDetailsService(PaymentProcessorRepository paymentProcessorRepository){
        return new BottomlineGatewayDetailsService(paymentProcessorRepository,getGatewayName());
    }

    @Bean
    public MockBottomline mockBottomlineGateway(@Qualifier("payment-accepted-events") MessageChannel acceptedEvents
            , @Qualifier("payment-rejected-events") MessageChannel rejectedEvents
            , @Qualifier("payment-cleared-events") MessageChannel clearedEvents
            , @Qualifier("payment-failedToClear-events") MessageChannel failedToClearEvents
            , @Qualifier("payment-settled-events") MessageChannel settledEvents
            , @Value("${mock-accesspay.submitExpression}") String submit
            , @Value("${mock-accesspay.acceptExpression}") String accept
            , @Value("${mock-accesspay.clearExpression}") String clear
            , Map<String,PaymentInstructionBatcher> batchers){
        ExpressionParser mockGatewayExpressionParser = new SpelExpressionParser();
        Expression submitExpression = mockGatewayExpressionParser.parseExpression(submit);
        Expression acceptExpression = mockGatewayExpressionParser.parseExpression(accept);
        Expression clearExpression = mockGatewayExpressionParser.parseExpression(clear);

        return new MockBottomline(acceptedEvents
                ,rejectedEvents
                ,clearedEvents
                ,failedToClearEvents
                ,settledEvents
                , Executors.newScheduledThreadPool(1)
                ,getGatewayName()
                ,submitExpression
                ,acceptExpression
                ,clearExpression
                ,batchers.entrySet().stream().filter(e -> e.getKey().startsWith("bottomline")).map(Map.Entry::getValue).toArray(PaymentInstructionBatcher[]::new));
    }

    @Bean
    PaymentInstructionBatcher bottomlineFpBatcher(CompositeMessageConverterFactory compositeNonNativeSerde){
        return PaymentInstructionBatchers
                .<String,String>countWithTimeout(1,null)
                .batchNameExtractor(PaymentInstructionBatchers.paymentFileBatchNaming())
                .branchPredicate(PaymentChannel.FasterPayments.name(), PaymentInstructionBatchers.channelTypePredicate())
                .groupingFunction(PaymentInstructionBatchers.noGrouping(compositeNonNativeSerde))
                .build();
    }

    @Bean
    PaymentInstructionBatcher bottomlineBacsBatcher(CompositeMessageConverterFactory compositeNonNativeSerde){
        return PaymentInstructionBatchers
                .<String,String>countWithTimeout(1,null)
                .batchNameExtractor(PaymentInstructionBatchers.paymentFileBatchNaming())
                .branchPredicate(PaymentChannel.Bacs.name(), PaymentInstructionBatchers.channelTypePredicate())
                .groupingFunction(PaymentInstructionBatchers.noGrouping(compositeNonNativeSerde))
                .build();
    }

    @Bean
    PaymentInstructionBatcher bottomlineChapsBatcher(CompositeMessageConverterFactory compositeNonNativeSerde){
        return PaymentInstructionBatchers
                .<String,String>countWithTimeout(1,null)
                .batchNameExtractor(PaymentInstructionBatchers.paymentFileBatchNaming())
                .branchPredicate(PaymentChannel.Chaps.name(),PaymentInstructionBatchers.channelTypePredicate())
                .groupingFunction(PaymentInstructionBatchers.noGrouping(compositeNonNativeSerde))
                .build();
    }

    @Override
    protected String getGatewayName() {
        return "mock-bottomline";
    }

    public interface MockBottomlineInputBinding {

        @Input("mock-bottomline-payment-events")
        KStream<?, PaymentRoutingSuccessEvent> paymentEvents();

        @Output("mock-bottomline-payment-submit-failure-events")
        KStream<?, PaymentSubmitFailedEvent> submitFailureEvents();

        @Output("mock-bottomline-payment-submit-success-events")
        KStream<?, PaymentSubmitSuccessEvent> submitSuccessEvents();
    }

    @RequestMapping(value = "/mock-bottomline/details")
    @ApiIgnore
    public class BottomlineGatewayDetailsService extends GatewayDetailsService {
        public BottomlineGatewayDetailsService(PaymentProcessorRepository repository, String name) {
            super(repository, name);
        }
    }

    public class MockBottomline extends MockOutboundGatewayBase<PaymentChannel> {

        public MockBottomline(MessageChannel acceptedEvents
                , MessageChannel rejectedEvents
                , MessageChannel clearedEvents
                , MessageChannel failedToClearEvents
                , MessageChannel settledEvents
                , ScheduledExecutorService taskExecutor
                , String name
                , Expression submitExpression
                , Expression acceptExpression
                , Expression clearExpression
                , PaymentInstructionBatcher... batchers) {
            super(acceptedEvents
                    , rejectedEvents
                    , clearedEvents
                    , failedToClearEvents
                    , settledEvents
                    , taskExecutor
                    , name
                    , submitExpression
                    , acceptExpression
                    , clearExpression
                    , batchers);
        }

        @StreamListener
        @SendTo({"mock-bottomline-payment-submit-success-events","mock-bottomline-payment-submit-failure-events"})
        public KStream<?, ?>[] handleMockBottomlinePaymentEvents(@Input("mock-bottomline-payment-events") KStream<?, PaymentRoutingSuccessEvent> events) {
            return handlePaymentEvents(events);
        }
    }
}
