
@echo off

SET DIR=%~dp0%

::download install.ps1
%systemroot%\System32\WindowsPowerShell\v1.0\powershell.exe -NoProfile -ExecutionPolicy Bypass -Command "((new-object net.webclient).DownloadFile('https://chocolatey.org/install.ps1','%DIR%install.ps1'))"
::run installer
%systemroot%\System32\WindowsPowerShell\v1.0\powershell.exe -NoProfile -ExecutionPolicy Bypass -Command "& '%DIR%install.ps1' %*"

choco install -y git.install
choco install -y docker-desktop
choco install -y python3 --version=3.7.5
choco install -y nodejs-lts --version=12.13.0
choco install -y yarn
choco install -y kubernetes-helm --version=2.16.1