#!/bin/bash

set -eo pipefail

kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep k8s-admin-token | awk '{print $1}')