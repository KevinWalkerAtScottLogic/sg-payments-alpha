#!/bin/sh

delete_helm_release() {
  if [[ $(helm ls -q) == *${1}* ]]
  then
    helm delete --purge ${1}
  else
    echo "Helm release ${1} not found, skipping"
  fi
}

set -eo pipefail

kubectl config use-context docker-desktop

delete_helm_release "payments-alpha-backend"
delete_helm_release "payments-adapters"
delete_helm_release "payments-transferwise"
delete_helm_release "payments-ui"
delete_helm_release "payments-alpha-ui"
delete_helm_release "payments-bacs-service"

kubectl delete secret -n localkube -l app=alpha-backend