#!/bin/sh

set -eo pipefail

cd ../../alpha-backend
chmod +x build_alpha_backend_local.sh
./build_alpha_backend_local.sh

cd ../alpha-frontend
chmod +x build_alpha_frontend_local.sh
./build_alpha_frontend_local.sh

cd ../local/alpha