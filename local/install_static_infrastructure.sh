#!/bin/bash

set -eo pipefail

#resource requirements
##SERVICE         MIN_MEMORY  MIN_CPU   MAX_MEMORY  MAX_CPU
#nginx            100         0.01      200         0.1
#dashboard        20          0.01      100         0.1
#metrics-server   20          0.01      100         0.2
#istio            100         0.01      N/A         N/A

pip3 install 'awscli>=1.16.308' --user --quiet

## Set up kubernetes cluster
## switch to using local k8
export BRANCH_TAG=local
kubectl config use-context docker-desktop

kubectl apply -f kafkastorageclass.yml

# Create namespace if it does not already exist
echo "Creating Namespace"
kubectl create namespace localkube -o yaml --dry-run | kubectl apply -f -

## add helm but don't return an error
echo "Initializing Helm"
kubectl create serviceaccount tiller -n kube-system
kubectl create clusterrolebinding tiller --clusterrole=cluster-admin --serviceaccount kube-system:tiller
helm init --upgrade --wait --service-account tiller --tiller-image gcr.io/kubernetes-helm/tiller:v2.16.1

echo "Installing Istio"
chmod +x ../infrastructure/aws/scripts/static/install_istio.sh
source ../infrastructure/aws/scripts/static/install_istio.sh
# The --logtostderr and --set installPackagePath args are needed to work around bugs in 1.4.0 on Windows. Should be fixed in 1.4.1.
istioctl manifest apply -f istio-customization-local.yml --logtostderr --set installPackagePath=${ISTIO_DIR}/install/kubernetes/operator/charts

echo "Labelling localkube for Istio injection"
echo "** Temporarily disabled istio-injection"
kubectl label --overwrite namespace localkube istio-injection=disabled
echo "Successfully installed Istio"

echo "Installing NGINX Controller"
helm upgrade --install  payments-nginx stable/nginx-ingress -f nginx-values.yml

echo "Adding metrics server"
helm upgrade --install --wait --namespace kube-system metrics-server stable/metrics-server -f metrics-server-values.yml

echo "Installing the Kubernetes dashboard..."
helm upgrade --install  --namespace kube-system dashboard stable/kubernetes-dashboard -f dashboard-values.yml
echo "Successfully installed the Kubernetes dashboard.\n"

echo "Installing storage class"
kubectl apply -f localstorageclass.yml
echo "Installed storage class"
