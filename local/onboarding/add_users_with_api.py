import argparse
import os
import requests
import subprocess
import sys
import yaml
from yaml import Loader

sys.path.insert(0, "../../infrastructure/aws")

from payments_alpha_stack_env import branch_name, environment_name
from token_generation import get_cognito_token


add_user_endpoint = "/command/addUser?reply=true"
add_scope_endpoint = "/command/addScope?reply=true"

users_url = f"https://{branch_name}-api.sgpayalpha.org.uk/users"

ci_username="ci_user"
ci_password= os.environ["CI_USER_PASSWORD"]
ci_token = get_cognito_token("ci_user", ci_password)
auth_header = {
    'content-type': 'application/json',
    'Authorization': 'Bearer '+ ci_token
}

def get_sftp_username(environment, username):
    hashcode = java_string_hashcode(environment)
    hashcode_stripped = abs(hashcode) % 100000;
    return str(hashcode_stripped)+"_"+username


def java_string_hashcode(s):
    h = 0
    for c in s:
        h = (31 * h + ord(c)) & 0xFFFFFFFF
    return ((h + 0x80000000) & 0xFFFFFFFF) - 0x80000000


def generate_keys_in_shell(username):
    print("Generating SSH KEY for paymentsfiles User", flush=True)
    if not os.path.exists('./keys'):
        os.mkdir('./keys')
    ssh_dir = "./keys/{}_id_rsa".format(username)
    if os.path.exists(ssh_dir):
        os.remove(ssh_dir)
        os.remove(ssh_dir+".pub")
    command = "ssh-keygen -f {} -t rsa".format(ssh_dir)
    p = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    p.communicate()
    print(f"\nSSH Key generated in file {ssh_dir}")
    public_key_file = ssh_dir + ".pub"
    public_key = open(public_key_file, 'r', encoding= "utf-8").read()
    print(f"\n\nPublic Key: \n {public_key}", flush=True)
    return public_key

def parse_role(role, roles, scopes):
    parsedRole = role.split("\\")
    scope = parsedRole[0]
    action = parsedRole[1].split(":")
    scopes.append({"name": scope})
    roles.append({
        "actions": [{
            "resource": action[0],
            "verb": action[1]
        }],
        "scope": {
            "name": scope
        }
    })

def __add_user(userconfig, default_email):

    rolesconfig = userconfig["roles"]
    roles = []
    scopes = []
    for role in rolesconfig:
        parse_role(role, roles, scopes)

    username = userconfig['name']
    print(f"\n *** CREATING User = {username} *** \n")

    user = {
        "name" : username,
        "email" : default_email,
        "groups" : scopes,
        "roles" : roles
    }

    if "generate-ssh-key" in userconfig:
        sftp_username = get_sftp_username(environment_name, username)
        key = generate_keys_in_shell(sftp_username)
        user['sshPublicKey'] = key
        print(f"SFTP User will be created with name {sftp_username}")


    r = requests.put(users_url+add_user_endpoint, json=user, headers=auth_header)
    if r.status_code == 200:
        print(f"\tCreated User {user['name']}")
    else:
        error = r.json().get("error")
        errorType = error.get("errorType")
        errorMessage = error.get("errorMessage")
        print(f"Error creating user: Details: {errorType} - {errorMessage}", flush=True)


def read_users_file(user, default_email):
    with open(user, 'r') as outfile:
        values = yaml.load(outfile, Loader=Loader)
        users = values['users']
        [__add_user(user, default_email) for user in users]

parser = argparse.ArgumentParser(description='Add Users with API')
parser.add_argument("--file", help='a yaml config file with SA/ACL set up information', required=True)
parser.add_argument("--email", help="the e-mail address to be associated with these users", required=True)

args = parser.parse_args()

filename = args.file
default_email = args.email
read_users_file(filename, default_email)





