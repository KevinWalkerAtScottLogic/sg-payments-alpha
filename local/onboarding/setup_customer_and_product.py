import os
import requests
import sys
import yaml
from yaml import Loader

sys.path.insert(0, "../../infrastructure/aws")

from payments_alpha_common import *
from payments_alpha_stack_env import branch_name
from token_generation import get_cognito_token

user_password = os.environ['USER_PASSWORD']

ci_token = get_cognito_token("cust_user", user_password)
cust_token = get_cognito_token("ci_user", user_password)

cust_user_header = {
    'content-type': 'application/json',
    'Authorization': 'Bearer '+ cust_token
}
ci_user_header = {
    'content-type': 'application/json',
    'Authorization': 'Bearer '+ ci_token
}

def __get_error_details(response):
    error = response.json().get("error")
    if error is not None:
        errorType = error.get("errorType")
        errorMessage = error.get("errorMessage")
        return  f"Details: {errorType} - {errorMessage}"
    else:
        error = response.json().get("reason")
        return f"Error: {response.status_code} Reason: {error}"

def __add_approve_product(product):

    payload = __map_product_config_to_payload(product)
    product_name = product['name']

    print(f"\n\n** CREATING PRODUCT {product_name} ** ", flush=True)
    url = f"https://{branch_name}-api.sgpayalpha.org.uk/customers/command/createProduct?reply=true"

    response = requests.request("PUT", url, headers=ci_user_header, json = payload)
    if(response.status_code != 200):
        errorDetails = __get_error_details(response)
        print(f"Error creating product: Details: {errorDetails}", flush=True)
    else:
        print("Product created successfully")
        print(response.text)

    print(f"\n** APPROVING PRODUCT {product_name} **", flush=True)

    product_approve = {
        "component0" : product['customerId'],
        "component1" : product_name
    }

    url = f"https://{branch_name}-api.sgpayalpha.org.uk/customers/command/approveCreateProduct?reply=true"
    print(url)
    response = requests.request("PUT", url, headers=cust_user_header, json = product_approve)
    if(response.status_code != 200):
        errorDetails = __get_error_details(response)
        print(f"Error approving product: Details: {errorDetails}", flush=True)
    else:
        print("Product approved successfully")

def __add_approve_customer(customer):

    print(f"\n** CREATING CUSTOMER {customer} ** ")
    payload = customer['name']
    url = f"https://{branch_name}-api.sgpayalpha.org.uk/customers/command/createCustomer?reply=true"
    print(url)
    response = requests.request("PUT", url, headers=cust_user_header, json = payload)
    if(response.status_code != 200):
        errorDetails = __get_error_details(response)
        print(f"Error creating customer: Details: {errorDetails}", flush=True)
    else:
        print("Customer created successfully")
        print(response.text)

    print(f"\n** APPROVING CUSTOMER {customer} **")

    url = f"https://{branch_name}-api.sgpayalpha.org.uk/customers/command/approveCreateCustomer?reply=true"
    print(url)
    response = requests.request("PUT", url, headers=ci_user_header, json = payload)
    if(response.status_code != 200):
        errorDetails = __get_error_details(response)
        print(f"Error approving customer: Details: {errorDetails}", flush=True)
    else:
        print("Customer approved successfully")


def __map_product_config_to_payload(product):

    payload = product

    payload['payer'] = {
        "name" : payload['customerId']
    }

    outbound_config = payload['outboundConfiguration']
    outbound_config['apiAccess'] = "false"
    outbound_config['source']['@type'] = "gov.scot.payments.payments.model.aggregate.UKBankAccount"
    for rules in outbound_config['validationRules']:
        rules ['evaluator'] = "spel"
        rules ['type'] = "Stateless"
    outbound_config['returns'] = payload['outboundConfiguration']['source']
    outbound_config['payer'] = {
        "name" : payload['customerId']
    }
    return payload

def read_customers_file(user):
    with open(user, 'r') as outfile:
        values = yaml.load(outfile, Loader=Loader)
        customers = values['customers']
        [__add_approve_customer(customer) for customer in customers]
        products = values['products']
        [__add_approve_product(product) for product in products]


config_file = sys.argv[1];
read_customers_file(config_file)
