#!/usr/bin/env bash
if [ "$1"=="--help" ]; then
    java -jar mock-customer-service.jar --help
else
    java -jar mock-customer-service.jar aws @payments-poc-mock-customer-site-aws-config.txt
fi
