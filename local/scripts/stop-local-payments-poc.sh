#!/usr/bin/env bash

shopt -s nullglob
for f in *.pid
do
  pid=`cat $f`
  kill -9 $pid
  rm $f
done

shopt -s nullglob
for f in *.port
do
  rm $f
done

shopt -s nullglob
for f in *.args
do
  rm $f
done

sleep 5

rm -rf logs

rm -rf tmp