@echo off
IF "%~1"=="--help" (
    java -jar mock-customer-service.jar --help
) ELSE (
    java -jar mock-customer-service.jar aws @payments-poc-mock-customer-site-aws-config.txt
)
