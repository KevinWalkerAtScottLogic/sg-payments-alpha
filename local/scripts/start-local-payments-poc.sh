#!/usr/bin/env bash

help=false
numFiles=`ls -1 *.pid 2>/dev/null | wc -l`

if [ $numFiles -gt 0 ]
then
   echo "application is running please run stop script"
   exit 1
fi

mkdir -p ./tmp
mkdir -p ./logs


if [ "$1" == "distributed" ]; then
    export LOADER_MAIN=gov.scot.payments.DistributedApplicationWrapper
    if [ "$2" == "--help" ]; then
        help=true
        java -jar local-application.jar --help
    else
        echo "Launching Core"
        java -Djava.io.tmpdir="$pwd/tmp" -jar local-application.jar @payments-poc-distributed-config.txt > logs/core.log 2>&1 &
    fi


else
    export LOADER_MAIN=gov.scot.payments.LocalApplication
    if [ "$1" == "--help" ]; then
        help=true
        java -jar local-application.jar --help
    else
        echo "Launching Core"
        java -Djava.io.tmpdir="$pwd/tmp" -jar local-application.jar embedded @payments-poc-embedded-config.txt > logs/core.log 2>&1 &
    fi
fi

if [ "$help" == "true" ]; then
    exit 0
fi


while ! [ -f core.port ];
do
    echo "Waiting for Core to start..."
    sleep 10
done

echo "Core started"
service_port=`cat core.port`
echo "core is running at $service_port"
echo "core is running at http://localhost:$service_port/resources/swagger-ui.html"
echo "core is running at http://localhost:$service_port/resources/ui/index.html"


open "http://localhost:$service_port/resources/swagger-ui.html"
open "http://localhost:$service_port/resources/ui/index.html"

if [ "$1" == "distributed" ]; then
    db_port=`cat db.port`
    echo "DB Console is running at http://localhost:$db_port"
    open "http://localhost:$db_port"
else
    echo "DB Console is running at http://localhost:$service_port/h2-console"
    open "http://localhost:$service_port/h2-console"
fi

shopt -s nullglob
for f in *.inbound.args
do
    echo "Launching Inbound Gateway $f"
    java -jar local-card-gateway.jar "@$f" > /dev/null 2>&1 &
done

shopt -s nullglob
for f in *.outbound.args
do
    echo "Launching Outbound Gateway $f"
    java -jar local-gateway.jar "@$f" > /dev/null 2>&1 &
done

echo "Launching Customer Site"

if [ "$1" == "distributed" ]; then
    customer_port=`cat customer-site.port`
    java -jar mock-customer-service.jar local @payments-poc-mock-customer-site-config.txt -service.port $service_port -port $customer_port > logs/customer-site.log 2>&1 &
else
    java -jar mock-customer-service.jar local @payments-poc-mock-customer-site-config.txt -service.port $service_port -portFile customer-site.port > logs/customer-site.log 2>&1 &
fi

while ! [ -f customer-site.pid ];
do
    echo "Waiting for Customer Site to start..."
    sleep 10
done
sleep 10
echo "Customer Site started"
customer_web_port=`cat customer-site.port`
echo "customer site is running at http://localhost:$customer_web_port"
open "http://localhost:$customer_web_port"