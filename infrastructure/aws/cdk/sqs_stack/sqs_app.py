#!/usr/bin/env python3
import sys

from aws_cdk import core
from sqs_stack import SQSStack

sys.path.insert(0, "../..")
from payments_alpha_stack_env import app_name, branch_name

app = core.App()
SQSStack(app, f"{app_name}-{branch_name}-sqs")
app.synth()
