import sys
from aws_cdk.core import Stack, Construct, Environment, Fn
from aws_cdk.aws_sqs import Queue, QueuePolicy

sys.path.insert(0, "../..")
from aws_access_env import account_id, region
from payments_alpha_stack_env import app_name, branch_name, sqs_queue_name

class SQSStack(Stack):
    def __init__(self, scope: Construct, stack_name: str) -> None:
        super().__init__(
            scope=scope,
            name=stack_name,
            env=Environment(region=region, account=account_id),
            tags={'Project': app_name, 'Branch': branch_name}
        )

        Queue(
            scope=self,
            id=f"{branch_name}-queue",
            queue_name=sqs_queue_name,

        )
