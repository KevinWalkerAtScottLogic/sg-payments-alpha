#!/usr/bin/env python3

from aws_cdk import core
from user_pool_stack import UserPoolStack

import sys
sys.path.insert(0, "../..")
from payments_alpha_stack_env import app_name, branch_name

app = core.App()
UserPoolStack(app, f"{app_name}-{branch_name}-user-pool")
app.synth()
