
echo "CDK version is $(cdk --version)"

if [[ "$(uname -s)" =~ "Linux" ]]; then
    BIN_PATH="./venv/bin"
elif [[ "$(uname -s)" =~ "MINGW" ]]; then
    BIN_PATH="./venv/Scripts"
else
    echo "Unknown environment: $(uname -s)"
    exit 1
fi

cd ${CI_PROJECT_DIR}/infrastructure/aws/cdk
python3 -m venv venv
. ${BIN_PATH}/activate
pip3 install -r requirements.txt
