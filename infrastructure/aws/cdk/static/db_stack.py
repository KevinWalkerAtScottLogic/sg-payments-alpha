import sys
from aws_cdk.core import CfnOutput, Construct, Duration, Environment, RemovalPolicy, SecretValue, Stack
import aws_cdk.aws_rds as rds
from aws_cdk.aws_ec2 import InstanceType, Port, SubnetConfiguration, SubnetSelection, SubnetType, Vpc
sys.path.insert(0, "..")

sys.path.insert(0, "..")
from static_resources_stack_env import aws_account_id, app_name, db_stack_name, region, rds_instance_name, rds_master_db_name, rds_master_user, rds_master_user_password

node_type = "t3.micro"
db_port = 5432
protect_db = False  # Prevent deletion of DB?

# Creates a Postgres DB instance. The DB's VPC has public subnets, so an Internet
# Gateway is created. Port 5432 is opened for external access to the DB.
class DBStack(Stack):

    def __init__(self, scope: Construct, stack_name: str) -> None:
        super().__init__(
            scope=scope,
            name=stack_name,
            env=Environment(region=region, account=aws_account_id),
            tags={'Project': app_name}
        )

        vpc = Vpc(
            scope=self,
            id=f"{db_stack_name}-VPC",
            cidr="10.1.0.0/16",
            subnet_configuration=[
                SubnetConfiguration(cidr_mask=24, name="database", subnet_type=SubnetType.PUBLIC)
            ]
        )

        db_instance = rds.DatabaseInstance(
            scope=self,
            id=db_stack_name,
            instance_identifier=rds_instance_name,
            engine=rds.DatabaseInstanceEngine.POSTGRES,
            allocated_storage=5,  # GB
            database_name=rds_master_db_name,
            master_username=rds_master_user,
            master_user_password=SecretValue.plain_text(rds_master_user_password),
            vpc=vpc,
            vpc_placement=SubnetSelection(subnet_type=SubnetType.PUBLIC),
            port=db_port,
            instance_class=InstanceType(node_type),
            storage_encrypted=True,
            allow_major_version_upgrade=False,
            auto_minor_version_upgrade=False,
            backup_retention=Duration.days(0),
            deletion_protection=protect_db,
            removal_policy=RemovalPolicy.RETAIN if protect_db else RemovalPolicy.DESTROY
        )

        # Open port 5432 to allow external access to the DB.
        db_instance.connections.allow_from_any_ipv4(Port.tcp(db_port))

        CfnOutput(
            scope=self,
            id=f"{db_stack_name}-security-group-output",
            export_name="DBSecurityGroup",
            value=db_instance.security_group_id,
            description=f"Security group created for the {db_stack_name} stack"
        )

        CfnOutput(
            scope=self,
            id=f"{db_stack_name}-vpc-output",
            export_name="DBVpc",
            value=vpc.vpc_id,
            description=f"VPC created for the {db_stack_name} stack"
        )
