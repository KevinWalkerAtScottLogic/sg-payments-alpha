import sys

from aws_cdk.core import Environment, CfnOutput, Construct, Stack
from aws_cdk.aws_ec2 import InstanceType
from aws_cdk.aws_ec2 import Port, SecurityGroup, SubnetConfiguration, SubnetType, Vpc
from aws_cdk.aws_eks import Cluster
from aws_cdk.aws_iam import Role, ManagedPolicy
from aws_cdk.aws_msk import CfnCluster as MSKCluster

sys.path.insert(0, "..")
from static_resources_stack_env import app_name, aws_account_id, eks_cluster_name, eks_node_group_name, eks_masters_role_arn, eks_node_type, region

eks_port=80
kafka_port=9092

class EKSStack(Stack):

    def __init__(self, scope: Construct, stack_name: str) -> None:
        super().__init__(
            scope=scope,
            name=stack_name,
            env=Environment(region=region, account=aws_account_id),
            tags={'Project': app_name, 'Stack': stack_name}
        )

        vpc = Vpc(
            scope=self,
            id=f"{stack_name}-VPC",
            cidr="10.0.0.0/16",
            max_azs=3,  # Align with MSK requirements
            subnet_configuration=[
                SubnetConfiguration(
                    cidr_mask=24,
                    name="eks-public",
                    subnet_type=SubnetType.PUBLIC
                ),
                SubnetConfiguration(
                    cidr_mask=24,
                    name="eks-private",
                    subnet_type=SubnetType.PRIVATE
                )
            ]
        )

        eks_masters_role = Role.from_role_arn(
            scope=self,
            id='eks_masters_role',
            role_arn=eks_masters_role_arn
        )

        cluster = Cluster(
            scope=self,
            id=eks_cluster_name,
            cluster_name=eks_cluster_name,
            default_capacity=0,
            masters_role=eks_masters_role,
            vpc=vpc,
            vpc_subnets=[SubnetConfiguration(name="eks-public", subnet_type=SubnetType.PUBLIC)]
        )

        cluster.add_capacity(
            id=eks_node_group_name,
            instance_type=InstanceType(eks_node_type),
            desired_capacity=2,
            min_capacity=1,
            max_capacity=3
        )

        eks_control_plane_security_group = SecurityGroup(
            scope=self,
            id=f"{stack_name}-ControlPlaneSecurityGroup",
            vpc=vpc
        )

        eks_control_plane_security_group.connections.allow_from_any_ipv4(
            port_range=Port.tcp(eks_port),
            description=f"Allow external connection on port {eks_port} to the ECS VPC"
        )

        ##TODO change from allow from vpc
        eks_control_plane_security_group.connections.allow_from_any_ipv4(
            port_range=Port.tcp(kafka_port),
            description=f"Allow external connection on port {kafka_port} to the ECS VPC"
        )


        CfnOutput(
            scope=self,
            id=f"{stack_name}-eks-security-group-output",
            export_name=f"{stack_name}-EksSecurityGroup",
            value=eks_control_plane_security_group.security_group_id,
            description=f"EKS Security Group created for the {stack_name} stack"
        )

        CfnOutput(
            scope=self,
            id=f"{stack_name}-vpc-output",
            export_name=f"{stack_name}-EksVpc",
            value=vpc.vpc_id,
            description=f"VPC created for the {stack_name} stack"
        )




