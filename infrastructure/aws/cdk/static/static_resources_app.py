#!/usr/bin/env python3

import os
from aws_cdk import core
from eks_stack import EKSStack
from db_stack import DBStack
import sys
sys.path.insert(0, "..")

app_name=os.environ['app_name']

app = core.App()
DBStack(app, f"{app_name}-db")
EKSStack(app, f"{app_name}-eks")
app.synth()
