#!/usr/bin/env python3
import sys

from aws_cdk import core
from frontend_stack import FrontEndStack

sys.path.insert(0, "../..")
from payments_alpha_stack_env import app_name, branch_name

app = core.App()
FrontEndStack(app, f"{app_name}-{branch_name}-frontend")
app.synth()
