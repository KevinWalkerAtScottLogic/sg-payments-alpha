import sys
import os

from aws_cdk.core import CfnOutput, Construct, Duration, Environment, RemovalPolicy, Stack
from aws_cdk.aws_cloudfront import AliasConfiguration, Behavior, CloudFrontAllowedMethods, CloudFrontWebDistribution, \
    CustomOriginConfig, OriginProtocolPolicy, PriceClass, SourceConfiguration, ViewerProtocolPolicy
from aws_cdk.aws_iam import AnyPrincipal, Effect, PolicyStatement
from aws_cdk.aws_route53 import HostedZone, RecordSet, RecordTarget, RecordType
from aws_cdk.aws_route53_targets import CloudFrontTarget
from aws_cdk.aws_s3 import BlockPublicAccess, Bucket, BucketAccessControl

sys.path.insert(0, "../..")

from aws_access_env import account_id, region
from payments_alpha_stack_env import app_name, branch_name, domain_name, front_end_bucket_name

branch_domain_name = f"{branch_name}.{domain_name}"
origin_domain_name = f"{front_end_bucket_name}.s3-website.{region}.amazonaws.com"
cloudfront_domain_certificate_arn = os.environ['CLOUDFRONT_DOMAIN_CERTIFICATE_ARN']

class FrontEndStack(Stack):

    def __init__(self, scope: Construct, stack_name: str) -> None:
        super().__init__(
            scope=scope,
            name=stack_name,
            env=Environment(region=region, account=account_id),
            tags={'Project': app_name, 'Branch': branch_name}
        )

        frontend_bucket = Bucket(
            scope=self,
            id=f"{branch_name}-frontend-bucket",
            bucket_name=front_end_bucket_name,
            access_control=BucketAccessControl.PUBLIC_READ,
            block_public_access=BlockPublicAccess(
                block_public_acls=True,
                block_public_policy=False,
                ignore_public_acls=True,
                restrict_public_buckets=False
            ),
            versioned=True,
            removal_policy=RemovalPolicy.DESTROY,  # or RETAIN?
            website_index_document="index.html",
            website_error_document="develop/error.html"
        )

        frontend_bucket.add_to_resource_policy(
            permission=PolicyStatement(
                actions=["s3:GetObject"],
                effect=Effect.ALLOW,
                resources=[frontend_bucket.arn_for_objects("*")],
                principals=[AnyPrincipal()]
            )
        )

        frontend_hosted_zone = HostedZone.from_lookup(
            scope=self,
            id=f"{stack_name}-hosted-zone",
            domain_name=domain_name
        )

        cloud_front = CloudFrontWebDistribution(
            scope=self,
            id=f"{branch_name}-cloudfront",
            origin_configs=[
                SourceConfiguration(
                    custom_origin_source=CustomOriginConfig(
                        domain_name=origin_domain_name,
                        origin_protocol_policy=OriginProtocolPolicy.HTTP_ONLY
                    ),
                    behaviors=[
                        Behavior(
                            allowed_methods=CloudFrontAllowedMethods.GET_HEAD,
                            path_pattern="/alpha"
                        ),
                        Behavior(
                            is_default_behavior=True,
                            min_ttl=Duration.days(0)
                        )
                    ]
                )
            ],
            price_class=PriceClass.PRICE_CLASS_100,
            viewer_protocol_policy=ViewerProtocolPolicy.ALLOW_ALL,
            alias_configuration=AliasConfiguration(
                names=[branch_domain_name],
                acm_cert_ref=cloudfront_domain_certificate_arn
            )
        )

        RecordSet(
            scope=self,
            id=f"{stack_name}-record-set",
            record_type=RecordType.A,
            record_name=branch_domain_name,
            zone=frontend_hosted_zone,
            target=RecordTarget.from_alias(CloudFrontTarget(distribution=cloud_front))
        )

        RecordSet(
            scope=self,
            id=f"{stack_name}-ipv6-record-set",
            record_type=RecordType.AAAA,
            record_name=branch_domain_name,
            zone=frontend_hosted_zone,
            target=RecordTarget.from_alias(CloudFrontTarget(distribution=cloud_front))
        )

        CfnOutput(
            scope=self,
            id=f"{stack_name}-cloudfront-dist-output",
            export_name=f"{stack_name}-CloudFrontDistributionId",
            value=cloud_front.distribution_id,
            description=f"CloudFront distribution created for the {stack_name} stack"
        )
