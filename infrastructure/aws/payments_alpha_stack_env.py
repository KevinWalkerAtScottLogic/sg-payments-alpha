import os

from scripts.utils.shorten_resource_name import shorten_name

def create_environment_name(name):
    name = name.replace("fix-", "")
    name = name.replace("feature-", "")
    name = name.replace("-", "")
    return name

# CI Details
branch_name = os.environ['CI_BRANCH_STR']
ci_project_directory = os.environ['CI_PROJECT_DIR']

# Stack Details
app_name = os.environ['APP_NAME']
stack_name = f"{app_name}-{branch_name}"
base_name = stack_name
environment_name = create_environment_name(branch_name)

# API Gateway Details
domain_name=os.environ['DOMAIN_NAME']

# Frontend Stack Details
front_end_bucket_name = shorten_name("sg-" + base_name, 54) + "-frontend"
frontend_stack_name = f"{app_name}-{branch_name}-frontend"

# User Pool Details
alpha_ui_client_name = f"{app_name}-ui"
user_pool_name = f"{branch_name}-user-pool"

# File Upload Details
file_upload_bucket_name = app_name + "-file-upload"
file_upload_bucket_id = f"{app_name}-file-upload-bucket"
file_upload_bucket_arn = f"arn:aws:s3:::{file_upload_bucket_name}"
lambda_name = f"{app_name}-s3-notification-lambda"

def sanitise_branch_name(name):
    name = name.replace('-', '')
    name = name.replace('feature', '')
    name = name.replace('fix', '')
    return name

sanitised_branch_name = sanitise_branch_name(branch_name)

# SQS Details
sqs_queue_name = f"{sanitised_branch_name}-file-event-queue"
policy_name = f"{branch_name}-sqs-policy-name"