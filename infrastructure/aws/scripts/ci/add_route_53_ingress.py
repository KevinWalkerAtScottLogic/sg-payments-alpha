import sys
import boto3
import os

sys.path.insert(0, "../..")

from aws_access_env import *


domain = os.environ['DOMAIN_NAME']
# AWS Elastic load balancer client
nginx_name =f"{os.environ['APP_NAME']}-nginx";


def create_boto3_client(service_name):
    return boto3.client(
        service_name=service_name,
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        region_name=region
    )


route53_client = create_boto3_client('route53')
elb_client = create_boto3_client('elb')

def get_nginx_controller_from_tag(nginx_name):
    value = f"default/{nginx_name}-nginx-ingress-controller"
    response = elb_client.describe_load_balancers()['LoadBalancerDescriptions'];
    for balancer in response:
        tags = elb_client.describe_tags(LoadBalancerNames=[balancer['LoadBalancerName']])['TagDescriptions'][0]['Tags']
        for tag in tags:
            if (tag['Key'] == 'kubernetes.io/service-name') & (tag['Value'] == value):
                return balancer;

def get_nginx_network_controller_from_tag(nginx_name):

    elbv2_client = create_boto3_client('elbv2')
    value = f"default/{nginx_name}-nginx-ingress-controller"
    response = elbv2_client.describe_load_balancers()['LoadBalancers'];
    for balancer in response:
        tags = elbv2_client.describe_tags(ResourceArns=[balancer['LoadBalancerArn']])['TagDescriptions'][0]['Tags']
        for tag in tags:
            if (tag['Key'] == 'kubernetes.io/service-name') & (tag['Value'] == value):
                return balancer;

def add_ip4_route_to_nginx(hosted_zone, api_path, load_balancer):
    change_route_to_api('UPSERT', hosted_zone, api_path, 'A', load_balancer)

def add_ip6_route_to_nginx(hosted_zone, api_path, load_balancer):
    change_route_to_api('UPSERT', hosted_zone, api_path, 'AAAA', load_balancer)

def change_route_to_api(action, hosted_zone, api_path, record_type, load_balancer):
    response = route53_client.change_resource_record_sets(
        HostedZoneId=hosted_zone,
        ChangeBatch={
            'Changes': [
                {
                    'Action': action,
                    'ResourceRecordSet': {
                        'Name': api_path,
                        'Type': record_type,
                        'AliasTarget': {
                            'HostedZoneId': load_balancer['CanonicalHostedZoneNameID'],
                            'DNSName': load_balancer['DNSName'],
                            'EvaluateTargetHealth': False
                        }
                    }
                },
            ]
        }
    )



def delete_records_for_api(hosted_zone, api_path):

    record_name = f"{api_path}.{domain}."

    response = route53_client.list_resource_record_sets(
        HostedZoneId=hosted_zone,
        StartRecordName=api_path
    )

    for record in response['ResourceRecordSets']:
        if(record['Name'] == record_name):
            print("Deleting record:")
            print(record)
            response = route53_client.change_resource_record_sets(
                HostedZoneId=hosted_zone,
                ChangeBatch={
                    'Changes': [
                        {
                            'Action': "DELETE",
                            'ResourceRecordSet': record
                        }
                    ]
                }
            )

def delete_ip4_route_to_nginx(hosted_zone, api_path, load_balancer):
    change_route_to_api('DELETE', hosted_zone, api_path, 'A', load_balancer)

def delete_ip6_route_to_nginx(hosted_zone, api_path, load_balancer):
    change_route_to_api('DELETE', hosted_zone, api_path, 'AAAA', load_balancer)

def create_nginx_dns_record(api_path):
    hosted_zone = get_hosted_zones(domain)
    lb = get_nginx_controller_from_tag(nginx_name)
    add_ip4_route_to_nginx(hosted_zone, api_path, lb)
    add_ip6_route_to_nginx(hosted_zone, api_path, lb)

def delete_nginx_dns_record(api_path):
    hosted_zone = get_hosted_zones(domain)
    delete_records_for_api(hosted_zone, api_path)

def get_hosted_zones(domain):
    zone = route53_client.list_hosted_zones_by_name(DNSName=domain)
    if(len(zone["HostedZones"]) == 0):
        print(f"Could not find hosted zone for {domain}")
        exit(1)
    return zone["HostedZones"][0]['Id']
