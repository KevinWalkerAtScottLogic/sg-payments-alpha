import sys

sys.path.insert(0, "../..")

from payments_alpha_stack_env import front_end_bucket_name
from payments_alpha_common import empty_s3_bucket


# Teardown any changes that would prevent the stack being destroyed
print("Emptying frontend S3 bucket")
empty_s3_bucket(front_end_bucket_name)
print("Emptied frontend S3 bucket")
