import sys

sys.path.insert(0, "..")
import boto3
import os

app_name=sys.argv[1]
region_name=os.environ['AWS_REGION']

# Identify all active environments by listing existing environment stacks
# The user-pool stack is the first backend stack that gets created in the create-environment stage
def list_active_environments():

    backend_stack_suffix = "user-pool"
    frontend_stack_suffix = "frontend"

    # List names of stacks that have the required prefix and suffixes
    cfn = boto3.resource('cloudformation', region_name)
    stacks = [stack.stack_name for stack in cfn.stacks.all() if stack.stack_name.startswith(app_name) and
              (stack.stack_name.endswith("user-pool") or stack.stack_name.endswith("frontend"))]

    if len(stacks) == 0:
        return stacks

    # Remove prefix and suffixes
    active_environments = list(map(lambda x: x.replace(f"{app_name}-","")
       .replace(f"-{backend_stack_suffix}","")
       .replace(f"-{frontend_stack_suffix}",""),stacks))

    filtered_environments = list(set(active_environments));
    if 'master' in filtered_environments:
        filtered_environments.remove('master')

    # Return a list of unique environment identifiers
    return  filtered_environments

active_environments = list_active_environments()
print(active_environments)
