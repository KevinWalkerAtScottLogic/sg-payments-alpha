import yaml
import sys
import requests
import boto3

from yaml import Loader

from payments_alpha_common import *
from payments_alpha_stack_env import app_name, branch_name
sys.path.insert(0, "../..")


add_user_endpoint = "/command/addUser"
add_scope_endpoint = "/command/addScope"

# TODO - update to point to users endpoint
base_url = "https://${CI_BRANCH_STR}-api.${DOMAIN_NAME}"
users_endpoint = "/alpha/users/users-ch-app"
users_url = base_url + users_endpoint


user_pool_stack_name = f"{app_name}-{branch_name}-user-pool"
cognito_user_pool_id = get_resource_id_from_stack_output(user_pool_stack_name, f"{branch_name}-UserPoolId")
cognito_client_id = get_resource_id_from_stack_output(user_pool_stack_name, f"{branch_name}-UserPoolUiClientId")

ci_username="ci_user"
ci_password= os.environ["CI_USER_PASSWORD"]

## This file allows the creation of users from a YAML file using our API.
## It supplies a default password assigned to each of the users

def get_cognito_token():
    global cognito_token, auth_header, username
    response = cognito_idp_client.admin_initiate_auth(
        AuthFlow="ADMIN_NO_SRP_AUTH",
        AuthParameters={
            "USERNAME": ci_username,
            "PASSWORD": ci_password
        },
        ClientId=cognito_client_id,
        UserPoolId=cognito_user_pool_id
    )

    cognito_token = response['AuthenticationResult']['IdToken']
    print(cognito_token)
    auth_header = {"Authorization": "Bearer " + cognito_token}

def __add_scope(scope):

    print(f"**** creating scope = {scope}")
    r = requests.put(users_url+add_scope_endpoint, json=scope, headers=auth_header)
    if r.status_code == 200:
        print("\tCreated scope with id " + scope['name'])
    else:
        print("Error creating customer: Details: \n" + r.json())


def parse_role(role, roles, scopes):
    parsedRole = role.split("\\")
    scope = parsedRole[0]
    action = parsedRole[1].split(":")
    scopes.append({"name": scope})
    roles.append({
        "actions": [{
            "resource": action[0],
            "verb": action[1]
        }],
        "scopes": [{
            "name": scope
        }]
    })

def __add_user(userconfig, default_email):

    rolesconfig = userconfig["roles"]
    roles = []
    scopes = []
    for role in rolesconfig:
        parse_role(role, roles, scopes)

    for scope in scopes:
        if(scope):
            __add_scope(scope)

    user = {
        "name" : userconfig['name'],
        "email" : default_email,
        "groups" : scopes,
        "roles" : roles
    }

    print(f"**** creating user = {user}")
    r = requests.put(users_url+add_user_endpoint, json=user, headers=auth_header)
    print(r)
    if r.status_code == 200:
        print("\tCreated user with id " + user['name'])
    else:
        print("Error creating customer: Details: \n" + r.json())


def read_users_file(user, default_email):
    with open(user, 'r') as outfile:
        values = yaml.load(outfile, Loader=Loader)
        users = values['users']
        [__add_user(user, default_email) for user in users]

        print(values)

filename = sys.argv[1]
default_email = sys.argv[2]

read_users_file(filename, default_email)
