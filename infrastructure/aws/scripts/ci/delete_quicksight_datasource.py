import sys
import yaml

sys.path.insert(0, "../..")

from aws_access_env import account_id
from payments_alpha_stack_env import environment_name
from payments_alpha_common import quicksight_client
from yaml import Loader


datasourcename=f"{environment_name}_datasource"


def delete_datasource(dsname):
    response = quicksight_client.delete_data_source(
        AwsAccountId=account_id,
        DataSourceId=dsname
    )

def delete_dataset(data_set_name):
    response = quicksight_client.delete_data_set(
        AwsAccountId=account_id,
        DataSetId=data_set_name
    )

def get_datasource_from_id(dataset_name):
    response = quicksight_client.describe_data_set(
        AwsAccountId=account_id,
        DataSetId=dataset_name
    )
    print(response)

def does_datasource_exists(dsname):
    try:
        response = quicksight_client.describe_data_source(
            AwsAccountId=account_id,
            DataSourceId=dsname
        )
        return True
    except:
        return False

def does_dataset_exist(dsname):
    try:
        response = quicksight_client.describe_data_set(
            AwsAccountId=account_id,
            DataSetId=dsname
        )
        return True
    except:
        return False

def get_dataset_id(name):
    return environment_name+"_"+name;

def delete_datasources_from_config(config_file):
    with open(config_file, 'r', encoding= "utf-8") as outfile:
        values = yaml.load(outfile, Loader=Loader)
        datasets =values['datasets']
        for dataset in datasets:
            name = get_dataset_id(dataset['name'])
            if does_dataset_exist(name):
                print(f"Deleting dataset {name}")
                delete_dataset(name)

file_name = sys.argv[1]

if(does_dataset_exist(datasourcename)):
    delete_datasource(datasourcename)
delete_datasources_from_config(file_name)
