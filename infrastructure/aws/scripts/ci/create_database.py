import sys
import psycopg2

sys.path.insert(0, "../..")

from payments_alpha_database_env import rds_instance_name, rds_master_db_name, rds_master_user, rds_master_user_password, \
    branch_db_name
from payments_alpha_common import rds_client
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


def get_tables(cur):
    cur.execute('SELECT datname FROM pg_database WHERE datistemplate = false')
    tables = list(map(lambda tup: tup[0], cur.fetchall()))
    return tables


def create_db_if_not_exists():
    instances = rds_client.describe_db_instances(
        DBInstanceIdentifier=rds_instance_name
    )['DBInstances']

    if len(instances) == 0:
        raise Exception("No RDS instance found")

    endpoint = instances[0]['Endpoint']

    conn = None
    try:
        conn = psycopg2.connect(
            host=endpoint['Address'],
            database=rds_master_db_name,
            user=rds_master_user,
            password=rds_master_user_password,
            port=endpoint['Port']
        )
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = conn.cursor()

        existing_tables = get_tables(cur)
        if branch_db_name in existing_tables:
            print(f"Databases {branch_db_name} already exists")
            print("Databases: " + ', '.join(existing_tables))
        else:
            print(f"Creating database {branch_db_name}")
            cur.execute(f"CREATE DATABASE {branch_db_name}")
            existing_tables = get_tables(cur)
            print("Databases: " + ', '.join(existing_tables))
            if branch_db_name in existing_tables:
                print(f"Databases {branch_db_name} created")
            else:
                raise Exception(f"Failed to create database {branch_db_name}")

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')


create_db_if_not_exists()
