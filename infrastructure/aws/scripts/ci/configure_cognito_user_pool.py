import sys

sys.path.insert(0, "../..")

from payments_alpha_common import cognito_idp_client
from payments_alpha_stack_env import user_pool_name, alpha_ui_client_name

required_email_schema = {
    'Mutable': False,
    'Name': "email",
    'Required': True
}

all_writable_attributes = ['address', 'birthdate', 'email', 'family_name', 'gender', 'given_name',
                           'locale', 'middle_name', 'name', 'nickname', 'phone_number',
                           'picture', 'preferred_username', 'profile', 'updated_at', 'website', 'zoneinfo']

readable_attributes_only = ['email_verified', 'phone_number_verified']


def get_user_pool_id_by_pool_name(pool_name):
    response = cognito_idp_client.list_user_pools(
        MaxResults=15
    )
    for user_pool in response['UserPools']:
        if user_pool['Name'] == pool_name:
            return user_pool['Id']
    return ""


def update_user_pool(user_pool_id):
    cognito_idp_client.update_user_pool(
        UserPoolId=user_pool_id,
        AdminCreateUserConfig={
            'AllowAdminCreateUserOnly': True
        },
        AutoVerifiedAttributes=['email']
    )


def get_user_pool_client_id_from_name(user_pool_id, app_client_name):
    response = cognito_idp_client.list_user_pool_clients(
        UserPoolId=user_pool_id
    )
    for pool_client in response['UserPoolClients']:
        if pool_client['ClientName'] == app_client_name:
            return pool_client['ClientId']
    return ""


def update_user_pool_client(user_pool_id, user_pool_client_id, give_all_permissions):
    cognito_idp_client.update_user_pool_client(
        UserPoolId=user_pool_id,
        ClientId=user_pool_client_id,
        ReadAttributes=all_writable_attributes + readable_attributes_only if give_all_permissions else [],
        WriteAttributes=all_writable_attributes if give_all_permissions else [],
        ExplicitAuthFlows=['ADMIN_NO_SRP_AUTH']
    )


user_pool_id = get_user_pool_id_by_pool_name(user_pool_name)
user_pool_client_id = get_user_pool_client_id_from_name(user_pool_id, alpha_ui_client_name)
update_user_pool(user_pool_id)
update_user_pool_client(user_pool_id, user_pool_client_id, True)
