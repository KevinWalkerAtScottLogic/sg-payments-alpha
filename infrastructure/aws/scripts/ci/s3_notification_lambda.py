import boto3
import json

sqs_client = boto3.resource('sqs')


def lambda_handler(event, context):
    created_file = event['Records'][0]['s3']['object']['key']
    s3_folder = created_file.split("/")[0]
    queue = sqs_client.get_queue_by_name(QueueName=f"{s3_folder}-file-event-queue")
    if queue is not None:
        queue.send_message(MessageBody=json.dumps(event))
