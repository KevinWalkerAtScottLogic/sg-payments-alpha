import sys

sys.path.insert(0, "../..")

from payments_alpha_common import s3_client
from payments_alpha_stack_env import file_upload_bucket_name, sanitised_branch_name


def does_folder_exist(bucket_name, folder_name):
    try:
        s3_client.get_object(
            Bucket=bucket_name,
            Key=folder_name
        )
        return True
    except s3_client.exceptions.ClientError:
        return False


def add_folder_to_s3_bucket(bucket_name, folder_name):
    s3_client.put_object(
        Bucket=bucket_name,
        Key=folder_name
    )

if not does_folder_exist(file_upload_bucket_name, f'{sanitised_branch_name}/'):
    add_folder_to_s3_bucket(file_upload_bucket_name, f'{sanitised_branch_name}/')
