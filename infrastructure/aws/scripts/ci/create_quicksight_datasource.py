import boto3
import os
import sys
import yaml

sys.path.insert(0, "../..")

from aws_access_env import account_id, access_key, secret_key
from payments_alpha_stack_env import environment_name
from payments_alpha_database_env import rds_instance_name, rds_master_user, rds_master_user_password, branch_db_name
from payments_alpha_common import quicksight_client
from yaml import Loader

datasourcename=f"{environment_name}_datasource"
quicksight_group_name = os.environ['QUICKSIGHT_GROUP_NAME']

quicksight_client_us = boto3.client(
    service_name='quicksight',
    aws_access_key_id=access_key,
    aws_secret_access_key=secret_key,
    region_name='us-east-1'
)

def list_data_sources():
    response = quicksight_client.list_data_sources(
        AwsAccountId=account_id
    )
    print(response)
    response = quicksight_client.list_data_sets(
    AwsAccountId=account_id
    )
    print(response)

def does_datasource_exists(dsname):
    try:
        response = quicksight_client.describe_data_source(
            AwsAccountId=account_id,
            DataSourceId=dsname
        )
        return True
    except:
        return False

def does_dataset_exist(dsname):
    try:
        response = quicksight_client.describe_data_set(
            AwsAccountId=account_id,
            DataSetId=dsname
        )
        return True
    except:
        return False

def delete_datasource(dsname):
    response = quicksight_client.delete_data_source(
        AwsAccountId=account_id,
        DataSourceId=dsname
    )

def delete_dataset(dsname):
    response = quicksight_client.delete_data_set(
        AwsAccountId=account_id,
        DataSetId=dsname
    )

def create_data_source_for_db(dsname, db_name):
    response = quicksight_client.create_data_source(
        AwsAccountId=account_id,
        DataSourceId=dsname,
        Name=dsname,
        Type='POSTGRESQL',
        DataSourceParameters={
            'RdsParameters': {
                'InstanceId': rds_instance_name,
                'Database': db_name
            },
        },
        Credentials={
            'CredentialPair': {
                'Username': rds_master_user,
                'Password': rds_master_user_password
            }
        },
        Permissions=[
            {
                "Principal": quicksight_group_name,
                "Actions": [
                    "quicksight:UpdateDataSourcePermissions",
                    "quicksight:DescribeDataSource",
                    "quicksight:DescribeDataSourcePermissions",
                    "quicksight:PassDataSource",
                    "quicksight:UpdateDataSource",
                    "quicksight:DeleteDataSource"

                ]
            },
        ]
    );
    dataset_arn = response["Arn"]
    print(f"Created datasource - {dataset_arn}")
    return dataset_arn

def create_datasource_if_not_exists(dsname, db_name):
    if(not does_datasource_exists(dsname)):
            return create_data_source_for_db(dsname, db_name)
    else:
        return get_datasource_arn(dsname)
        print(f"Datasource {dsname} already exists")

def get_datasource_arn(dsname):
    response = quicksight_client.describe_data_source(
        AwsAccountId=account_id,
        DataSourceId=dsname
    )
    print(response)
    return response['DataSource']['Arn']

def create_dataset_for_db_tables(name, datasource_arn, sqlquery, columns):

    response = quicksight_client.create_data_set(
        AwsAccountId=account_id,
        DataSetId= name,
        Name= name,
        PhysicalTableMap={
            'string': {
                'CustomSql': {
                    'DataSourceArn': datasource_arn,
                    'Name': 'Payment Batch Projection',
                    "SqlQuery": sqlquery,
                    'Columns': columns
                },

            }
        },
        Permissions=[
            {
                "Principal": quicksight_group_name,
                "Actions": [
                    "quicksight:DescribeDataSet",
                    "quicksight:DescribeDataSetPermissions",
                    "quicksight:PassDataSet",
                    "quicksight:DescribeIngestion",
                    "quicksight:ListIngestions"

                ]
            }
        ],
        ImportMode='DIRECT_QUERY'
    )
    print(response)

def list_users():
    response = quicksight_client_us.list_users(
        AwsAccountId=account_id,
        Namespace='default'
    )
    print(response)

def create_group():
    response = quicksight_client_us.create_group(
        GroupName='sg-payments-reports',
        Description='A group for sg-payments reporting',
        AwsAccountId=account_id,
        Namespace='default'
    )
    print(response)

def add_user_to_group(username):
    response = quicksight_client_us.create_group_membership(
        GroupName='sg-payments-reports',
        MemberName=username,
        AwsAccountId=account_id,
        Namespace='default'
    )
    print(response)

def get_datasource_permissions(dsname):
    response = quicksight_client.describe_data_source_permissions(
        AwsAccountId=account_id,
        DataSourceId=dsname
    )
    print(response)

def convert_to_input_column(column):
    return {
            'Name': column['name'],
            'Type': column['type']
        };

def get_dataset_id(name):
    return environment_name+"_"+name;

def create_datasources_from_config(config_file, datasource_arn):
    with open(config_file, 'r', encoding= "utf-8") as outfile:
        values = yaml.load(outfile, Loader=Loader)
        datasets =values['datasets']
        for dataset in datasets:
            name = get_dataset_id(dataset['name'])
            if does_dataset_exist(name):
                print(f"Dataset {name} already exists")
                continue
            query = dataset['query']
            inputcol = [convert_to_input_column(c) for c in dataset['columns'] ]
            create_dataset_for_db_tables(name, datasource_arn, query, inputcol)


file_name = sys.argv[1]
datasource_arn = create_datasource_if_not_exists(datasourcename, branch_db_name)
create_datasources_from_config(file_name, datasource_arn)

