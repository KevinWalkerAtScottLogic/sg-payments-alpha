import sys

def shorten_name(name: str, max_length: int):
    if len(name) > max_length:
        name = name.replace("payments-", "p-")
        name = name.replace("feature-", "f-")
        name = name.replace("/", "-")
    if len(name) > max_length:
        name = name[:max_length]
    if name.endswith("-"):
        name = name[:-1]
    return name.lower()

if __name__ == "__main__":
    if len(sys.argv) is not 3:
        raise ValueError("Incorrect Number of Arguments. Usage: " + sys.argv[0] + " <name> <max length>")
    else:
        print(shorten_name(sys.argv[1], int(sys.argv[2])))
