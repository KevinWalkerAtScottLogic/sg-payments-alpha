import sys
sys.path.insert(0, "../..")

from payments_alpha_common import get_resource_id_from_stack_output

stack_name = sys.argv[1]
output_name = sys.argv[2]

print(get_resource_id_from_stack_output(stack_name, output_name))
