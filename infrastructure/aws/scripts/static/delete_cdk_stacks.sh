#bin/sh

echo "Tearing down CDK Stacks"

cd ../../cdk/static
cdk destroy ${app_name}-db -f
cdk destroy ${app_name}-eks -f
cd ../../scripts/static
