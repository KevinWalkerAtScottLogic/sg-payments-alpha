#!/usr/bin/env bash

# Configure virtual environment
cd ../../cdk/static
source install_venv_static.sh
cd ../../scripts/static

# Configure environment variables
source static_resources_env.sh

# Configure AWS authentication
./configure-awscli.sh

# Deploy the db and Kubernetes stacks
./deploy_static_resource_stacks.sh

# Configure KubeCtl connection to the cluster
../../../kubernetes/scripts/configure-kubectl.sh

# Initialise tiller
kubectl apply -f ./helm-service-account.yml
helm init --service-account tiller --wait --history-max 200

# Deploy the Kubernetes dashboard
echo "Installing the Kubernetes metrics sever..."
helm upgrade --install --namespace kube-system metrics-server stable/metrics-server
echo "Successfully installed the Kubernetes metrics server."

echo "Installing the Kubernetes dashboard..."
helm upgrade --install  --namespace kube-system dashboard stable/kubernetes-dashboard --set fullnameOverride="kubernetes-dashboard"
echo "Successfully installed the Kubernetes dashboard.\n"

echo "Installing NGINX Controller"
helm upgrade --install --wait payments-nginx stable/nginx-ingress --set rbac.create=true -f ./nginx-values.yml \
  --set controller.service.annotations."service\.beta\.kubernetes\.io/aws-load-balancer-ssl-cert"=$DOMAIN_CERTIFICATE_ARN
echo "Successfully installed NGINX Controller.\n"

echo "Installing Istio"
./install_istio.sh
# The --logtostderr and --set installPackagePath args are needed to work around bugs in 1.4.0 on Windows. Should be fixed in 1.4.1.
istioctl manifest apply -f istio-customization.yml --logtostderr --set installPackagePath=${ISTIO_DIR}/install/kubernetes/operator/charts
echo "Successfully installed Istio"

echo "Installing storage class for Kafka"
kubectl apply -f ./kafkastorageclass.yml
echo "Successfully installed storage class for Kafka"

echo -e "\nFinished creating static resources."

echo "Installing Amazon EBS CSI Driver"
kubectl apply -k "github.com/kubernetes-sigs/aws-ebs-csi-driver/deploy/kubernetes/overlays/stable/?ref=master"

# Fetch the variables required for GitLab integration
./get-cluster-secrets.sh

cd ../../cdk/static
deactivate
cd ../../scripts/static