#!/usr/bin/env bash

export ISTIO_VERSION=1.4.0

# Configure virtual environment
cd ../../cdk/static
source install_venv_static.sh
cd ../../scripts/static

# Configure environment variables
source static_resources_env.sh

# Configure AWS authentication
./configure-awscli.sh

# Configure KubeCtl connection to the cluster
../../../kubernetes/scripts/configure-kubectl.sh

# Delete Kubernetes dashbaord
./delete_kubernetes_deployment.sh "kubernetes-dashboard"

# Delete Kubernetes metrics server
./delete_kubernetes_deployment.sh "metrics-server"

# Delete the nginx release
./delete_nginx.sh

# Delete the CloudFormation stacks
./delete_cdk_stacks.sh

# Delete CDK Toolkit
./delete_cdk_toolkit.sh

deactivate

echo "Finished tearing down static resources."
