#!/bin/bash


set -eo pipefail

APP_NAME="payments"

#Enter Aws Credentials
AWS_REGION=""
AWS_ACCOUNT_ID=""
AWS_ACCESS_KEY_ID=""
AWS_SECRET_ACCESS_KEY=""

#Enter Confluent Cloud Credentials
BOOTSTRAP_ENDPOINT=""
CONFLUENT_CLOUD_KEY=""
CONFLUENT_CLOUD_SECRET=""

EKS_MASTERS_ROLE_ARN="arn:aws:iam::${AWS_ACCOUNT_ID}:role/EKSMastersRole"
NAMESPACE="default"

aws configure set aws_access_key_id ${AWS_ACCESS_KEY_ID}
aws configure set aws_secret_access_key ${AWS_SECRET_ACCESS_KEY}
aws configure set region ${AWS_REGION}
aws --region ${AWS_REGION} eks update-kubeconfig --name ${APP_NAME}-eks-cluster --role-arn ${EKS_MASTERS_ROLE_ARN}

echo "Installing Confluent"
rm -rf confluent-operator
mkdir confluent-operator
tar xzf confluent-operator.tar.gz -C confluent-operator


helm upgrade --wait --install -f confluent-values.yml --namespace=${NAMESPACE} \
--set operator.enabled=true \
--set global.sasl.plain.username=${CONFLUENT_CLOUD_KEY} \
--set global.sasl.plain.password=${CONFLUENT_CLOUD_SECRET} \
--set global.provider.region=${AWS_REGION} \
--set global.provider.kubernetes.deployment.zones={${AWS_REGION}} \
confluent-operator confluent-operator/helm/confluent-operator

kubectl -n ${NAMESPACE} patch serviceaccount default -p '{"imagePullSecrets": [{"name": "confluent-docker-registry" }]}'
while [[ $(kubectl get pods -n ${NAMESPACE} -l app=cc-operator -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; do echo "waiting for cc-operator" && sleep 5; done
while [[ $(kubectl get pods -n ${NAMESPACE} -l app=cc-manager -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; do echo "waiting for cc-manager" && sleep 5; done

helm upgrade --wait --install -f confluent-values.yml --namespace=${NAMESPACE} \
--set schemaregistry.enabled=true \
--set global.sasl.plain.username=${CONFLUENT_CLOUD_KEY} \
--set global.sasl.plain.password=${CONFLUENT_CLOUD_SECRET} \
--set global.provider.region=${AWS_REGION} \
--set global.provider.kubernetes.deployment.zones={${AWS_REGION}} \
--set schemaregistry.dependencies.kafka.bootstrapEndpoint=${BOOTSTRAP_ENDPOINT} \
schemaregistry confluent-operator/helm/confluent-operator

while [[ $(kubectl get pods -n ${NAMESPACE} -l type=schemaregistry -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; do echo "waiting for schema registry" && sleep 5; done

rm -rf confluent-operator
echo "Installation Complete"


