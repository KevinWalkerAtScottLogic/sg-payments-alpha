#!/usr/bin/env bash

export ISTIO_VERSION=1.4.2
ISTIO_DIR=istio-${ISTIO_VERSION}

echo "Downloading Istio"
if [ "$(expr substr $(uname -s) 1 5)" == "Linux" ] || [ "$(uname -s)" == "Darwin" ]; then
    curl -L https://istio.io/downloadIstio | sh -
elif [ "$(expr substr $(uname -s) 1 5)" == "MINGW" ]; then
    curl -o istio.zip -L https://github.com/istio/istio/releases/download/${ISTIO_VERSION}/istio-${ISTIO_VERSION}-win.zip
    unzip -qu istio.zip
    rm istio.zip
else
    echo "Unknown environment: $(uname -s)"
    exit 1
fi

echo "Installing Istio"
export PATH=$PWD/${ISTIO_DIR}/bin:$PATH