export app_name="payments"
export APP_NAME=${app_name}

echo "Setting environment variables..."
# AWS Authentication Credentials
export AWS_ACCOUNT_ID=
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
export AWS_REGION="eu-west-2"
export DOMAIN_CERTIFICATE_ARN=
export EKS_MASTERS_ROLE_ARN="arn:aws:iam::${AWS_ACCOUNT_ID}:role/EKSMastersRole"

# Database Credentials
export rds_master_user_password=
export rds_master_user="sysadmin"