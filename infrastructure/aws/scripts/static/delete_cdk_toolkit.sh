#bin/sh

echo "Tearing down the CDK Toolkit..."

cdk_staging_bucket=$(aws s3 ls --region ${AWS_REGION} | grep cdktoolkit | awk '{print $3}')
aws s3 rb s3://${cdk_staging_bucket} --force
aws cloudformation delete-stack --stack-name CDKToolkit