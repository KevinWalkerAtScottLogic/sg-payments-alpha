
## CLUSTER

def get_service_account_from_output(output):
    print(f"Calling {name}")
    accounts = output.splitlines()[2:]
    for account in accounts:
        service_account = [x.strip() for x in account.split("|")]
        if(service_account[1] == name):
            return service_account[0]
    return ""

def get_cluster_id_from_output(output):
    # remove the header rows
    cluster_list = output.splitlines()[2:]
    for cluster in cluster_list:
        # split by columns
        cluster_info = [x.strip() for x in cluster.split("|")]
        if(cluster_info[1] == cluster_name):
            # remove indicating '*'
            return cluster_info[0].replace("*", "").strip()