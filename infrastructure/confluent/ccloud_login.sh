#!/bin/bash

set -eo pipefail

##################################################
# Log in to Confluent Cloud
##################################################

echo -e "\n# Login"
OUTPUT=$(
expect <<END
  log_user 1
  spawn ccloud login
  expect "Email: "
  send "$CCLOUD_EMAIL\r";
  expect "Password: "
  send "$CCLOUD_PASSWORD\r";
  expect "Logged in as "
  set result $expect_out(buffer)
END
)
echo "$OUTPUT"
if [[ ! "$OUTPUT" =~ "Logged in as" ]]; then
  echo "Failed to log into your cluster.  Please check all parameters and run again"
  exit 1
fi

