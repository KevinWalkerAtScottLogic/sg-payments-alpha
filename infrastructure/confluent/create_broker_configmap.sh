#!/usr/bin/env bash

set -eo pipefail

CLUSTER_NAME=sg-payments-alpha
ENVIRONMENT_NAME=sg-payments

echo "LOOKING AT KAFKA CLUSTER"
CLUSTER_ID=$(ccloud kafka cluster list | grep $CLUSTER_NAME | awk '{print $1;}')
echo "The cluster ID is $CLUSTER_ID"
ENVIRONMENT_ID=$(ccloud environment list | grep $ENVIRONMENT_NAME | awk -F '|' '{print $1;}' | awk '{print $2;}')
echo "The environment ID is $ENVIRONMENT_ID"

echo "Getting cluster details"
OUTPUT=$(ccloud kafka cluster describe $CLUSTER_ID )
BOOTSTRAP_SERVERS=$(echo "$OUTPUT" | grep "Endpoint" | grep SASL_SSL | awk '{print $4;}' | cut -c 12-)
echo "BOOTSTRAP_SERVERS: $BOOTSTRAP_SERVERS"

kubectl create configmap -n "$KUBE_NAMESPACE" cluster-brokers \
  --from-literal=kafka_brokers=$BOOTSTRAP_SERVERS \
  --from-literal=schema_registry=http://schemaregistry.default.svc.cluster.local:8081 \
  -o yaml --dry-run | kubectl apply -f -
