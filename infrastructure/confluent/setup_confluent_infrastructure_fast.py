import argparse
import hashlib
import os
import yaml
from subprocess import check_output
from yaml import Loader


## SERVICE ACCOUNT ##

def create_service_account_if_not_exists(prefix):
    name = "";
    sa_name = get_hashed_service_account_name(name)
    print(f"Checking if service account exists for {sa_name}")
    service_id = get_service_account_id(sa_name)
    if(service_id == None):
        sa_description = prefix;
        print(f"Creating service account {sa_name}")
        return create_service_account(sa_name, sa_description)
    return service_id.strip()

def get_service_account_ids(service_accounts):
    service_accounts_ids = {}
    for service_account in service_accounts:
        sa_name = get_hashed_service_account_name(service_account['name'])
        service_account_id = get_service_account_id(sa_name)
        service_accounts_ids[sa_name] = service_account_id
    return service_accounts_ids

def get_service_account_id(name):
    output = check_output("ccloud service-account list",shell=True).decode("utf-8")
    accounts = output.splitlines()[2:]
    for account in accounts:
        service_account = [x.strip() for x in account.split("|")]
        if(service_account[1] == name):
            return service_account[0]
    return None

def delete_api_key(api_key):
    print(f"ccloud api-key delete {api_key}")
    os.system(f"ccloud api-key delete {api_key}")

def delete_api_key_if_exists(service_account_id):
    key = get_api_key(service_account_id)
    if key is not None:
        delete_api_key(key)


def get_api_key(service_account_id):
    output = check_output("ccloud api-key list",shell=True).decode("utf-8")
    api_keys = output.splitlines()[2:]
    for api_key_row in api_keys:
        api_key_array = [x.strip() for x in api_key_row.split("|")]
        if(api_key_array[1] == service_account_id):
            return api_key_array[0]
    return None

## TOPIC ##

def does_topic_exist(topic_name):
    output = check_output("ccloud kafka topic list",shell=True).decode("utf-8")
    topic_list = [x.strip() for x in output.splitlines()[2:]]
    for topic in topic_list:
        if topic == topic_name:
            return True
    return False

def get_topic_list():
    output = check_output("ccloud kafka topic list",shell=True).decode("utf-8")
    accounts = list(map(lambda it: it.strip(), output.splitlines()))
    ## the first two element of our topic list are a header
    return accounts[2:]

## CLUSTER

def get_cluster_id_from_name(cluster_name):
    output = check_output("ccloud kafka cluster list",shell=True).decode("utf-8")
    # remove the header rows
    cluster_list = output.splitlines()[2:]
    for cluster in cluster_list:
        # split by columns
        cluster_info = [x.strip() for x in cluster.split("|")]
        if(cluster_info[1] == cluster_name):
            # remove indicating '*'
            return cluster_info[0].replace("*", "").strip()

## CREATION

## We have to limit the service account to 32 characters so create a hash of the branch
## name at the start
def get_hashed_service_account_name(name):
    hash_prefix = hashlib.sha1(prefix.encode("utf-8")).hexdigest()[:9]
    hashed_name = hash_prefix +"-"+hashlib.sha1(name.encode("utf-8")).hexdigest()[:21]
    return hashed_name

def create_service_account(name, description):
    print(f"ccloud service-account create {name} --description {description}")
    output = check_output(f"ccloud service-account create {name} --description {description}",shell=True).decode("utf-8")
    for line in output.splitlines()[1:-1]:
        # remove first and list "|" and then split into columns
        service_info = [x.strip() for x in line[1:-1].split("|")]
        if(service_info[0] == "Id"):
            return service_info[1]
    return None

def create_acl_for_topic(service_account_id, operation, topic, is_prefix):
    if is_prefix:
        print(f"ccloud kafka acl create --allow --service-account-id {service_account_id} --operation {operation} --prefix --topic {topic}")
        check_output(f"ccloud kafka acl create --allow --service-account-id {service_account_id} --operation {operation} --prefix --topic {topic}",shell=True)
    else:
        print(f"ccloud kafka acl create --allow --service-account-id {service_account_id} --operation {operation} --topic {topic}")
        check_output(f"ccloud kafka acl create --allow --service-account-id {service_account_id} --operation {operation} --topic {topic}",shell=True)

def create_acl_for_group(service_account_id, operation, group, is_prefix):
    if is_prefix:
        print(f"ccloud kafka acl create --allow --service-account-id {service_account_id} --operation {operation} --prefix --consumer-group {group}")
        check_output(f"ccloud kafka acl create --allow --service-account-id {service_account_id} --operation {operation} --prefix --consumer-group {group}",shell=True)
    else:
        print(f"ccloud kafka acl create --allow --service-account-id {service_account_id} --operation {operation} --consumer-group {group}")
        check_output(f"ccloud kafka acl create --allow --service-account-id {service_account_id} --operation {operation} --consumer-group {group}",shell=True)

def create_topic(topic, partitions, config):
    if config is not None:
        config_entries = config.items()
        config_list = [i[0] + '=' + i[1] for i in config_entries]
        config_params = ','.join(config_list)
        print(f"ccloud kafka topic create {topic} --partitions {partitions} --config {config_params}")
        check_output(f"ccloud kafka topic create {topic} --partitions {partitions} --config {config_params}",shell=True)
    else:
        print(f"ccloud kafka topic create {topic} --partitions {partitions}")
        check_output(f"ccloud kafka topic create {topic} --partitions {partitions}",shell=True)

def create_topic_if_not_exist(prefix, name, num_partitions, config):
    topic_name = prefix + "-" + name
    if(not does_topic_exist(topic_name)):
        create_topic(topic_name, num_partitions, config)

def create_topic_acls(service_account_ids, prefix, topic):
    acl = topic['access_list']
    topic_name = prefix + "-" + topic['name']
    is_prefix = 'prefix' in topic
    for service_access in acl:
        permissions, service_account_id = __get_sa_id_and_permissions(service_access, service_account_ids)
        [create_acl_for_topic(service_account_id, access, topic_name, is_prefix) for access in permissions]

def create_topic_acls_for_branch(service_account_id, prefix):
    create_acl_for_topic(service_account_id, "alter", prefix, True)
    create_acl_for_topic(service_account_id, "alter-configs", prefix, True)
    create_acl_for_topic(service_account_id, "create", prefix, True)
    create_acl_for_topic(service_account_id, "cluster-action", prefix, True)
    create_acl_for_topic(service_account_id, "delete", prefix, True)
    create_acl_for_topic(service_account_id, "describe", prefix, True)
    create_acl_for_topic(service_account_id, "describe-configs", prefix, True)
    create_acl_for_topic(service_account_id, "idempotent-write", prefix, True)
    create_acl_for_topic(service_account_id, "read", prefix, True)
    create_acl_for_topic(service_account_id, "write", prefix, True)


def create_group_acls_for_branch(service_account_id, prefix):

    create_acl_for_group(service_account_id, "alter", prefix, True)
    create_acl_for_group(service_account_id, "alter-configs", prefix, True)
    create_acl_for_group(service_account_id, "create", prefix, True)
    create_acl_for_group(service_account_id, "cluster-action", prefix, True)
    create_acl_for_group(service_account_id, "delete", prefix, True)
    create_acl_for_group(service_account_id, "describe", prefix, True)
    create_acl_for_group(service_account_id, "describe-configs", prefix, True)
    create_acl_for_group(service_account_id, "idempotent-write", prefix, True)
    create_acl_for_group(service_account_id, "read", prefix, True)
    create_acl_for_group(service_account_id, "write", prefix, True)



def __get_sa_id_and_permissions(service_access, service_account_ids):
    hashed_sa_name = get_hashed_service_account_name(service_access['service_account'])
    service_account_id = service_account_ids[hashed_sa_name]
    permissions = service_access['permissions']
    return permissions, service_account_id

def create_group_acls(service_account_ids, prefix, group):
    group_name = prefix + "-" + group['name']
    acl = group['access_list']
    is_prefix = 'prefix' in group
    for service_access in acl:
        permissions, service_account_id = __get_sa_id_and_permissions(service_access, service_account_ids)
        [create_acl_for_group(service_account_id, access, group_name, is_prefix) for access in permissions]

def create_access_control_for_branch(service_account_id, prefix):
    print(f"\n** Creating Topic Access Control Lists ** \n")
    create_topic_acls_for_branch(service_account_id, prefix)
    print(f"\n** Creating Group Access Control Lists ** \n")
    create_group_acls_for_branch(service_account_id, prefix)


def create_kubernetes_secret(api_info, kafka_key_name, namespace_name):

    print("\n** Creating Kubernetes Secret **\n")
    k8command = f"""kubectl create secret generic -n {namespace_name} {kafka_key_name} \
        --from-literal=kafka_key={api_info['key']} --from-literal=kafka_password={api_info['secret']} \
        --from-literal=schema_registry_key={os.environ['CCLOUD_SCHEMA_KEY']} --from-literal=schema_registry_password={os.environ['CCLOUD_SCHEMA_SECRET']} \
        -o yaml --dry-run | kubectl apply -f -"""
    check_output(k8command,shell=True)

def create_api_key(service_account_id, cluster_id):
    delete_api_key_if_exists(service_account_id)
    print(f"Creating API Key for {service_account_id}")
    print(f"ccloud api-key create --description {prefix} --service-account-id {service_account_id} --resource {cluster_id}")
    output = check_output(f"ccloud api-key create --service-account-id {service_account_id} --resource {cluster_id}",shell=True).decode("utf-8")
    key_output = output.splitlines()
    api_info = {}
    for line in key_output:
        # remove first and list "|" and then split into columns
        line_info = [x.strip() for x in line[1:-1].split("|")]
        if(len(line_info) == 1):
            continue
        if(line_info[0] == 'API Key'):
            api_info['key'] = line_info[1]
        if(line_info[0] == 'Secret'):
            api_info['secret'] = line_info[1]
    return api_info;



def create_sa_and_get_id_for_branch(prefix):
    print(f"\n** Creating Service Accounts ** \n")
    service_account_id = create_service_account_if_not_exists(prefix)
    return service_account_id


    api_info = create_api_key(service_account_id, cluster_id)
    for service_account in service_accounts:
        kafka_key_name = service_account['kafka_key_name']
        create_kubernetes_secret(api_info, kafka_key_name, k8_namespace)
    return service_account_id


def create_api_key_for_branch(service_account_id, cluster_id):
    api_info = create_api_key(service_account_id, cluster_id)
    return api_info


def create_topics_and_keys_for_file(filename, api_info, service_account_id):
    with open(f"{filename}", 'r', encoding= "utf-8") as outfile:
        print(f"\n** Creating TOPICS for {filename} ** \n")
        values = yaml.load(outfile, Loader=Loader)
        context = values['config']
        topics = context.get('topics')
        service_accounts = context['service_accounts']

        print(f"\n** Creating Topics ** \n")
        if topics is not None:
            for topic in topics:
                print(topic)
                create_topic_if_not_exist(prefix, topic['name'], topic['partitions'], topic.get('config'))

        print(f"\n** Creating Kafka Keys ** \n")
        for service_account in service_accounts:
            kafka_key_name = service_account['kafka_key_name']
            create_kubernetes_secret(api_info, kafka_key_name, kube_namespace)
        return service_account_id

def create_confluent_resources_from_directory(directory):

    cluster_id = get_cluster_id_from_name(cluster)
    print(f"\n** Looking at cluster_id {cluster_id} ** \n")
    check_output(f"ccloud kafka cluster use \"{cluster_id}\"",shell=True).decode("utf-8")

    print(f"\n** Creating Service Account **")
    service_account_id = create_service_account_if_not_exists(prefix)
    api_info = create_api_key_for_branch(service_account_id, cluster_id)

    print(f"\n** Creating ACL for branch ** \n")
    create_access_control_for_branch(service_account_id, prefix)

    for filename in os.listdir(directory):
        filepath = f"{directory}/{filename}"
        create_topics_and_keys_for_file(filepath, api_info, service_account_id)






parser = argparse.ArgumentParser(description='Create or Delete Confluent infrastructure.')

parser.add_argument("-prefix", help='used for prefixing topics and creating service-account')
parser.add_argument("-namespace", help='namespace to put kubernetes secrets in')
parser.add_argument("-file", help='a yaml config file with SA/ACL set up information')
parser.add_argument("-dir", help='a directory of files')
parser.add_argument("-cluster", help=" the name of the cluster in confluent setup")
group = parser.add_mutually_exclusive_group()
group.add_argument("--create", help=' this will create the confluent infrastructure', action="store_true")

args = parser.parse_args()

prefix = args.prefix
kube_namespace = args.namespace
file_name = args.file
directory = args.dir
cluster = args.cluster

if(args.create):
    create_confluent_resources_from_directory(directory)






