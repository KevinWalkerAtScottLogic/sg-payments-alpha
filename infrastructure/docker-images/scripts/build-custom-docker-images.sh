#!/usr/bin/env sh

set -eo pipefail

docker build -t sg-payments-alpha-python ../dockerfiles/python-image
docker build -t sg-payments-alpha-jdk ../dockerfiles/jdk-image
