#!/usr/bin/env bash

set -eo pipefail

echo "Creating secrets..."
if [[ "$CI_PROJECT_VISIBILITY" == "public" ]]; then
  return
fi

$(python3 populate_aws_env_variables.py)
# These calls replace the existing secrets without deleting them first, incase a pod is requesting the secret at time of replace
# This outputs a yaml file as a dry-run, and then calls kubectl replace to replace existing secret.

# Image Registry Secrets
kubectl create secret docker-registry image-registry-credentials --namespace="$KUBE_NAMESPACE" \
 --docker-server=${GITLAB_REGISTRY_URL} \
 --docker-username=${GITLAB_REGISTRY_USERNAME} \
 --docker-password=${GITLAB_DEPLOY_TOKEN} \
 --docker-email=${GITLAB_USER_EMAIL} \
   -o yaml --dry-run | kubectl apply -f -


#Cognito
kubectl create configmap -n "$KUBE_NAMESPACE" cognito-info \
  --from-literal=domain=$AUTH_SERVER_DOMAIN \
  --from-literal=user_pool_id=$COGNITO_USER_POOL \
  --from-literal=user_pool=$COGNITO_USER_POOL_URL \
  --from-literal=client_id=$COGNITO_CLIENT_ID \
  --from-literal=client_secret=$COGNITO_CLIENT_SECRET \
  -o yaml --dry-run | kubectl apply -f -

#SFTP
kubectl create configmap -n "$KUBE_NAMESPACE" sftp-info \
  --from-literal=bucket_name=$BUCKET_NAME \
  --from-literal=server_id=$SFTP_SERVER_ID \
  --from-literal=user_role_arn=$SFTP_USER_ROLE_ARN \
  --from-literal=sqs_queue_url=$SQS_QUEUE_URL \
  -o yaml --dry-run | kubectl apply -f -

 # RDS secrets
kubectl create secret generic -n "$KUBE_NAMESPACE" rds-credentials \
  --from-literal=rds_user_name=sysadmin \
  --from-literal=rds_password=$RDS_MASTER_USER_PASSWORD \
  --from-literal=rds_url=jdbc:postgresql://${RDS_INSTANCE_ENDPOINT}/${RDS_DB_NAME} \
  -o yaml --dry-run | kubectl apply -f -

 # AWS task secret
kubectl create secret generic -n "$KUBE_NAMESPACE" aws-credentials \
  --from-literal=aws_account_id=$AWS_ACCOUNT_ID \
  --from-literal=aws_access_key=$AWS_TASK_ACCESS_KEY_ID \
  --from-literal=aws_secret_key=$AWS_TASK_SECRET_ACCESS_KEY \
  --from-literal=aws_region=$AWS_REGION \
  -o yaml --dry-run | kubectl apply -f -

#Quicksight
kubectl create configmap -n "$KUBE_NAMESPACE" quicksight-info \
  --from-literal=account_id=$AWS_ACCOUNT_ID \
  --from-literal=role_arn=$QUICKSIGHT_GROUP_NAME \
  -o yaml --dry-run | kubectl apply -f -