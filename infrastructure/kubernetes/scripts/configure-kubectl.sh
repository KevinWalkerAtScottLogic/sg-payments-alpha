#!/usr/bin/env bash

echo "Configuring Kubectl ... "
aws --region ${AWS_REGION} eks update-kubeconfig --name ${APP_NAME}-eks-cluster --role-arn ${EKS_MASTERS_ROLE_ARN}