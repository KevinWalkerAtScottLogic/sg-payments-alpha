#!/usr/bin/env bash

NAMESPACE_NAME=$1
BRANCH_NAME=$2
echo "Creating namespace $NAMESPACE_NAME"
NAMESPACE_OUTPUT=$(kubectl get namespace $NAMESPACE_NAME -o=jsonpath='{.metadata.name}' 2>/dev/null )
if [[ -z "$NAMESPACE_OUTPUT" ]];
then
  kubectl create namespace $NAMESPACE_NAME
fi
kubectl label --overwrite namespaces $NAMESPACE_NAME branch=$BRANCH_NAME