#!/usr/bin/env bash

set -eo pipefail

# The environment variable {APP_NAME} has its own definition within Gradle files which overides its value.
# So we need to give it a different name.
export CI_APP_NAME="${APP_NAME}"

# Get the user pool ID and client ID which we need for getting an access token for the backend
cd ${CI_PROJECT_DIR}/infrastructure/aws/scripts/utils

export CI_BACKEND_URL="${CI_BRANCH_STR}-api.${DOMAIN_NAME}"

STACK_NAME="${APP_NAME}-${CI_BRANCH_STR}-user-pool"
USER_POOL_OUTPUT="${CI_BRANCH_STR}-UserPoolId"
USER_POOL_CLIENT_OUTPUT="${CI_BRANCH_STR}-UserPoolUiClientId"
COGNITO_USER_POOL_ID=$(python3 get_stack_output.py $STACK_NAME $USER_POOL_OUTPUT)
COGNITO_CLIENT_ID=$(python3 get_stack_output.py $STACK_NAME $USER_POOL_CLIENT_OUTPUT)

COGNITO_AUTH=$(aws cognito-idp admin-initiate-auth \
  --auth-flow ADMIN_NO_SRP_AUTH \
  --client-id "${COGNITO_CLIENT_ID}" \
  --user-pool-id "${COGNITO_USER_POOL_ID}" \
  --auth-parameters USERNAME="ci_user",PASSWORD="${CI_USER_PASSWORD}")

export ID_TOKEN=$(echo $COGNITO_AUTH | grep -Po '(?<="IdToken":[[:space:]]").[^"]+')

if [ $ID_TOKEN ]
then
  echo "Successfully obtained Cognito ID token."
fi

cd ${CI_PROJECT_DIR}/alpha-backend/e2e
npm install
npm run e2e-test
