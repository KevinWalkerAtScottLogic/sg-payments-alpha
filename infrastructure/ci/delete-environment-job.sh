#!/usr/bin/env bash

set -eo pipefail

sanatise_branch() {
  local BRANCH_STR=$1
  BRANCH_STR=${BRANCH_STR//feature/}
  BRANCH_STR=${BRANCH_STR//fix/}
  BRANCH_STR=${BRANCH_STR//-/}
  echo $BRANCH_STR
}

delete_helm_release() {
  if [[ $(helm ls -q) == *${1}* ]]
  then
    helm delete --purge ${1} || true
  else
    echo "Helm release ${1} not found, skipping"
  fi
}

echo -e "\nTearing down environment: ${CI_BRANCH_STR}"

# Configure Kubectl for EKS cluster
cd ${CI_PROJECT_DIR}/infrastructure/kubernetes/scripts
./configure-kubectl.sh

echo "Kube namespace is $KUBE_NAMESPACE"

if [[ -z "$KUBE_NAMESPACE" ]];
then
  NAMESPACE=$( kubectl get namespace --selector=branch=$CI_BRANCH_STR -o jsonpath='{.items[0].metadata.name}')
else
  NAMESPACE=${KUBE_NAMESPACE}
fi;
echo "Set the namespace to $NAMESPACE"
if [[ ! -z "$NAMESPACE" ]];
then
  echo "DELETING KUBERNETES RESOURCES"
  echo "Deleting Alpha helm release ${CI_BRANCH_STR}-payments-alpha"
  delete_helm_release "${CI_BRANCH_STR}-payments-alpha"
  echo "Deleting customer adapters helm release ${CI_BRANCH_STR}-adapters"
  delete_helm_release "${CI_BRANCH_STR}-adapters"
  echo "Deleting transferwise adapter helm release ${CI_BRANCH_STR}-transferwise"
  delete_helm_release "${CI_BRANCH_STR}-transferwise"
  echo "Deleting transferwise adapter helm release ${CI_BRANCH_STR}-transferwise"
  delete_helm_release "${CI_BRANCH_STR}-bacs-service"
  echo "Deleting transferwise adapter helm release ${CI_BRANCH_STR}-transferwise"
  delete_helm_release "${CI_BRANCH_STR}-latest"

  echo "Deleting kubernetes names and namespaces"
  kubectl delete --all roles --namespace ${NAMESPACE} || true
  kubectl delete --all rolebindings --namespace ${NAMESPACE} || true
  kubectl delete --all secrets --namespace ${NAMESPACE} || true
  kubectl delete --all configmaps --namespace ${NAMESPACE} || true
fi

## Login to CCloud
cd ${CI_PROJECT_DIR}/infrastructure/confluent
./ccloud_login.sh

echo "Deleting Alpha Kafka resources"
CLUSTER_NAME=sg-payments-alpha
cd ${CI_PROJECT_DIR}/infrastructure/confluent
ENVIRONMENT=$(sanatise_branch "${CI_PROJECT_DIR}")
delete-confluent-resources $ENVIRONMENT

kubectl delete namespace ${NAMESPACE} || true

cd ${CI_PROJECT_DIR}/infrastructure/aws/scripts/ci

# Delete the route53 records
API_PATH=$CI_BRANCH_STR-api.$DOMAIN_NAME;
echo "Deleting DNS Records: $API_PATH"
python3 -c "from add_route_53_ingress import delete_nginx_dns_record; delete_nginx_dns_record('$API_PATH')"

# Delete any frontend resources that will prevent the stacks being torn down
python3 delete_frontend_resources.py

# Destroy environment
. ${CI_PROJECT_DIR}/infrastructure/aws/cdk/activate_cdk.sh

cd ${CI_PROJECT_DIR}/infrastructure/aws/cdk/frontend_stack
cdk destroy --force payments-${CI_BRANCH_STR}-frontend

cd ${CI_PROJECT_DIR}/infrastructure/aws/cdk/sqs_stack
cdk destroy --force payments-${CI_BRANCH_STR}-sqs

cd ${CI_PROJECT_DIR}/infrastructure/aws/cdk/user_pool_stack
cdk destroy --force payments-${CI_BRANCH_STR}-user-pool

# Deactive the CDK venv
deactivate

# Delete the policy allowing lambda to send to SQS environment instance
cd ${CI_PROJECT_DIR}/infrastructure/aws/scripts/ci
python3 delete_lambda_policy.py
python3 delete_database.py
python3 delete_quicksight_datasource.py datasource-config.yml