#!/usr/bin/env bash

set -eo pipefail

# The environment variable {APP_NAME} has its own definition within Gradle files which overides its value.
# So we need to give it a different name.
export CI_APP_NAME="${APP_NAME}"

# Get the user pool client ID which we need for calling from the frontend
cd ${CI_PROJECT_DIR}/infrastructure/aws/scripts/utils
STACK_NAME=${APP_NAME}-${CI_BRANCH_STR}-user-pool
USER_POOL_OUTPUT=${CI_BRANCH_STR}-UserPoolUiClientId
export COGNITO_APP_CLIENT_ID=$(python3 get_stack_output.py $STACK_NAME $USER_POOL_OUTPUT)



# Set environment variables required by the React app
COGNITO_DOMAIN=$CI_BRANCH_STR
echo "Looking at endpoint " $COGNITO_DOMAIN
if [[ ${COGNITO_DOMAIN} == "master" ]]; then
  COGNITO_DOMAIN="mstr"
fi;

export PUBLIC_URL="https://sg-${APP_NAME}-${CI_BRANCH_STR}-frontend.s3.${AWS_REGION}.amazonaws.com/alpha"
export REACT_APP_DYNAMIC_AUTH="true"
export REACT_APP_COGNITO_REDIRECT_URL="https://${CI_BRANCH_STR}.${DOMAIN_NAME}/alpha"
export REACT_APP_CI_BRANCH_STR="${CI_BRANCH_STR}"
export REACT_APP_API_ENDPOINT="https://${CI_BRANCH_STR}-api.${DOMAIN_NAME}/"
export REACT_APP_COGNITO_ENDPOINT="https://sg-${APP_NAME}-${COGNITO_DOMAIN}.auth.${AWS_REGION}.amazoncognito.com"
export REACT_APP_UI_COGNITO_APP_CLIENT_ID="${COGNITO_APP_CLIENT_ID}"

echo REACT_APP_COGNITO_REDIRECT_URL $REACT_APP_COGNITO_REDIRECT_URL
echo REACT_APP_API_ENDPOINT $REACT_APP_API_ENDPOINT
echo REACT_APP_COGNITO_ENDPOINT $REACT_APP_COGNITO_ENDPOINT

# Build the frontend application
cd ${CI_PROJECT_DIR}/alpha-frontend

yarn install
yarn test
yarn build