#!/usr/bin/env bash

set -eo pipefail

cd ${CI_PROJECT_DIR}/infrastructure/aws/scripts/utils
STAGING_DISTRIBUTION_ID=$(python3 get_cloudfront_id.py)
STAGING_DISTRIBUTION_ID_VAL=${STAGING_DISTRIBUTION_ID}
BUCKET_NAME=$(python3 shorten_resource_name.py sg-${APP_NAME}-${CI_BRANCH_STR} 54)
BUCKET_PATH=s3://${BUCKET_NAME}-frontend

echo "Deploying frontend to ${BUCKET_PATH}/alpha"
aws s3 sync ${CI_PROJECT_DIR}/alpha-frontend/build/ ${BUCKET_PATH}/alpha --delete

echo "Invaliding distribution ${STAGING_DISTRIBUTION_ID_VAL}"
aws cloudfront create-invalidation --distribution-id ${STAGING_DISTRIBUTION_ID_VAL} --paths '/alpha/*'
