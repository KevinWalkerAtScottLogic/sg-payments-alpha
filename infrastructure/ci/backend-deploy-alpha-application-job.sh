#!/usr/bin/env bash

set -eo pipefail

sanatise_branch() {
  local BRANCH_STR=$1
  BRANCH_STR=${BRANCH_STR//feature/}
  BRANCH_STR=${BRANCH_STR//fix/}
  BRANCH_STR=${BRANCH_STR//-/}
  echo $BRANCH_STR
}

setup_confluent_infrastructure() {
    echo "Setting up confluent infrastructure from file $1"
    python3 setup_confluent_infrastructure.py --create -file "$1" -namespace "$2" -cluster "$3" -prefix "$4"
}

setup_confluent_infrastructure_fast() {
    echo "Setting up confluent infrastructure from file $1"
    python3 setup_confluent_infrastructure_fast.py --create -dir "$1" -namespace "$2" -cluster "$3" -prefix "$4"
}

CLUSTER_NAME=sg-payments-alpha
APP_BRANCH_NAME=$(sanatise_branch "$CI_BRANCH_STR")

cd ${CI_PROJECT_DIR}/infrastructure/kubernetes/scripts
./configure-kubectl.sh

cd ${CI_PROJECT_DIR}/infrastructure/confluent
./ccloud_login.sh


## THIS CREATES VALID ACL BUT IS MUCH SLOWER
#echo "Setting up command handler confluent infrastructure"
#if [ -d "${CI_PROJECT_DIR}/alpha-backend/application/build/confluent" ]; then
#  find ../../alpha-backend/application/build/confluent -type f -name "*-ch-app*" -print0 | while read -r -d '' f
#  do
#    setup_confluent_infrastructure $f "$KUBE_NAMESPACE" "$CLUSTER_NAME" "$APP_BRANCH_NAME"
#  done
#  #echo "Waiting for command handler confluent infrastructure setup to finish"
#  #wait
#  echo "command handler confluent infrastructure setup finished"
#  echo "Setting up remaining confluent infrastructure"
#  find ../../alpha-backend/application/build/confluent -type f -not -name "*-ch-app*" -print0 | while read -r -d '' f
#  do
#    setup_confluent_infrastructure $f "$KUBE_NAMESPACE" "$CLUSTER_NAME" "$APP_BRANCH_NAME"
#  done
#  #echo "Waiting for remaining confluent infrastructure setup to finish"
#  #wait
#  echo "remaining confluent infrastructure setup finished"
#fi

echo "Setting up command handler confluent infrastructure"
echo $APP_BRANCH_NAME
if [ -d "../../alpha-backend/application/build/confluent" ]; then
    setup_confluent_infrastructure_fast "../../alpha-backend/application/build/confluent" "$KUBE_NAMESPACE" "$CLUSTER_NAME" "$APP_BRANCH_NAME"
fi


#deploy app
cd ${CI_PROJECT_DIR}/alpha-backend/application/build/helm/output

helm upgrade --install \
--namespace=${KUBE_NAMESPACE} \
${CI_BRANCH_STR}-payments-alpha sg-payments-alpha.tgz \
--set global.ingressHost=${CI_BRANCH_STR}-api.${DOMAIN_NAME},global.namespace=${APP_BRANCH_NAME},\
pspadapter.transferwiseadapterapp.transferWiseToken=${TRANSFERWISE_TOKEN},\
pspadapter.transferwiseadapterapp.transferWiseProfileId=${TRANSFERWISE_PROFILE_ID}

# Deploy customer adapters service

ADAPTERS_IMAGE_URL=${IMAGE_REGISTRY_URL}/${CI_BRANCH_STR}-adapters:latest

helm upgrade --install \
--namespace=${KUBE_NAMESPACE} \
${CI_BRANCH_STR}-adapters ${CI_PROJECT_DIR}/alpha-backend/adapters/helm \
--set ingressHost=${CI_BRANCH_STR}-api.${DOMAIN_NAME},imageUrl=${ADAPTERS_IMAGE_URL}

