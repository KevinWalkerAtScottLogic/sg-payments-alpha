#!/usr/bin/env bash

set -eo pipefail

# The environment variable {APP_NAME} has its own definition within Gradle files which overides its value.
# So we need to give it a different name.
export CI_APP_NAME="${APP_NAME}"
export GRADLE_USER_HOME=`pwd`/.gradle

# Build the alpha backend
cd ${CI_PROJECT_DIR}/alpha-backend
./gradlew clean build -PexcludeKafkaTests
./gradlew jib