#!/usr/bin/env bash

set -eo pipefail

# Deploy the file-resources stack
. ${CI_PROJECT_DIR}/infrastructure/aws/cdk/activate_lambda_cdk.sh
cd ${CI_PROJECT_DIR}/infrastructure/aws/cdk/lambda_stack
cdk deploy ${APP_NAME}-lambda

# Deactive the CDK venv
deactivate
