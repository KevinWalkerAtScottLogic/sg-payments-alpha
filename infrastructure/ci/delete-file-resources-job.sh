#!/usr/bin/env bash

set -eo pipefail

# Teardown any changes that would prevent the stack being destroyed
cd ${CI_PROJECT_DIR}/infrastructure/aws/scripts/ci

python3 delete_file_resources.py
python3 delete_all_lambda_role_policies.py

# Destroy the lambda stack
. ${CI_PROJECT_DIR}/infrastructure/aws/cdk/activate_lambda_cdk.sh
cd ${CI_PROJECT_DIR}/infrastructure/aws/cdk/lambda_stack
cdk destroy --force ${APP_NAME}-lambda
# Deactive the CDK venv
deactivate

# Destroy the file-resources stack
. ${CI_PROJECT_DIR}/infrastructure/aws/cdk/activate_cdk.sh
cd ${CI_PROJECT_DIR}/infrastructure/aws/cdk/file_upload_stack
cdk destroy --force file-resources
# Deactive the CDK venv
deactivate
