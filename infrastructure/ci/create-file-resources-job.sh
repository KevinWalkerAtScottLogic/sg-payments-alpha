#!/usr/bin/env bash

set -eo pipefail

# Deploy the file-resources stack
. ${CI_PROJECT_DIR}/infrastructure/aws/cdk/activate_cdk.sh
cd ${CI_PROJECT_DIR}/infrastructure/aws/cdk/file_upload_stack
cdk deploy file-resources

# Deactive the CDK venv
deactivate
