CALL gradlew.bat clean
SET REACT_APP_STATIC_SITE=false
SET REACT_APP_DYNAMIC_AUTH=true
CALL gradlew.bat assemble
CD frontend
RMDIR build /S /Q
CALL yarn
CALL yarn build
ECHO D | XCOPY /s .\build ..\local\build\resources\main\static\ui
CD ../
CALL gradlew.bat assemble copyLocalJar --rerun-tasks