package gov.scot.payments.common.gateway;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import gov.scot.payments.MockWebSecurityConfiguration;
import gov.scot.payments.WithUser;
import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.common.kafka.KafkaNullConfiguration;
import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = GatewayDetailsServiceIntegrationTest.TestConfiguration.class, properties = {
        "debug=true",
        "payments.environment=test"
})
@AutoConfigureDataJpa
@AutoConfigureWebMvc
@AutoConfigureMockMvc
@DBRider
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@Tag("db")
@WithUser
public class GatewayDetailsServiceIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    PaymentProcessorRepository repository;

    @Test
    @DataSet("processors.yml")
    public void testGetProcessor() throws Exception {
        String response = "{}";
        mvc.perform(get("/details"))
                .andExpect(status().isOk())
                .andExpect(content().json(response));
    }

    @Configuration
    @EnableJpaRepositories(basePackageClasses = {PaymentProcessorRepository.class} )
    @EntityScan(basePackageClasses = {PaymentProcessor.class} )
    @Import({OptionalResponseControllerAdvice.class, KafkaNullConfiguration.class, MockWebSecurityConfiguration.class})
    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    public static class TestConfiguration {

        @Bean
        public GatewayDetailsService outboundGatewayDetailsService(PaymentProcessorRepository repository){
            return new GatewayDetailsService(repository,"test");
        }

    }
}
