package gov.scot.payments.common.local;

import com.beust.jcommander.Parameter;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;

@Data
@NoArgsConstructor
public class LocalKafkaConfig implements LocalConfig {

    @Parameter(names = "-kafka"
            , required = true
            , description = "The location of the kafka brokers to use")
    private String kafkaBrokers;

    public LocalKafkaConfig(String kafkaBrokers){
        this.kafkaBrokers = kafkaBrokers;
    }

    @Override
    public Set<String> getProfiles() {
        return Collections.singleton("msk");
    }

    @Override
    public Map<String, Object> getProperties(){
        return Collections.singletonMap("KAFKA_BROKERS",kafkaBrokers);
    }

    @Override
    public String[] toArgs() {
        List<String> args = new ArrayList<>();
        if(kafkaBrokers != null){
            args.add("-kafka");
            args.add(kafkaBrokers);
        }
        return args.toArray(new String[0]);
    }

    @Override
    public Set<String> validate() {
        Set<String> violations = new HashSet<>();
        if(kafkaBrokers == null){
            violations.add("kafka brokers required");
        }
        return violations;
    }

    @Override
    public String toString() {
        return String.join(" ", toArgs());
    }
}
