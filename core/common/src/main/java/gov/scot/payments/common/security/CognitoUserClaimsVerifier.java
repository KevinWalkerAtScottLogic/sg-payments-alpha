package gov.scot.payments.common.security;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.token.store.JwtClaimsSetVerifier;

import java.util.Map;

public class CognitoUserClaimsVerifier implements JwtClaimsSetVerifier {

    private final String issuer;
    private final String audience;

    public CognitoUserClaimsVerifier(String issuer, String audience) {
        this.issuer = issuer;
        this.audience = audience;
    }

    @Override
    public void verify(Map<String, Object> claims) throws InvalidTokenException {
        verifyIssuer(claims);
        if(claims.get("token_use").equals("id")){
            verifyAudience(claims);

        }
    }

    private void verifyIssuer(Map<String, Object> claims) {
        if(!issuer.equals(claims.get("iss"))){
            throw new InvalidTokenException("Invalid issuer");
        }
    }

    private void verifyAudience(Map<String, Object> claims) {
        if(!audience.equals(claims.get("aud"))){
            throw new InvalidTokenException("Invalid audience");
        }
    }
}
