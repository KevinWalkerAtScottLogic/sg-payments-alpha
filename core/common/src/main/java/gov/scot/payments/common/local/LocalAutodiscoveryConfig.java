package gov.scot.payments.common.local;

import com.beust.jcommander.IDefaultProvider;
import com.beust.jcommander.Parameter;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class LocalAutodiscoveryConfig {

    @Parameter(names = "-autodiscover")
    private Integer autoDiscover;

    public String[] trimArgs(String[] args) {
        int toTrim = 0;
        if(autoDiscover != null){
            toTrim+=2;
        }
        return Arrays.copyOfRange(args,toTrim,args.length);
    }

    @Override
    public String toString() {
        List<String> args = new ArrayList<>();
        if(autoDiscover != null){
            args.add("-autodiscover");
            args.add(autoDiscover+"");
        }
        return String.join(" ", args);
    }
}
