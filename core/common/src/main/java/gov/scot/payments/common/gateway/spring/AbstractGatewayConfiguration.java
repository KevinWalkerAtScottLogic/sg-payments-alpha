package gov.scot.payments.common.gateway.spring;

import gov.scot.payments.common.gateway.GatewayDetailsService;
import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import gov.scot.payments.model.paymentinstruction.PaymentChannelDirection;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import gov.scot.payments.model.paymentinstruction.PaymentProcessorChannel;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.bind.Bindable;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.boot.web.context.WebServerApplicationContext;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.cloud.zookeeper.discovery.ZookeeperDiscoveryProperties;
import org.springframework.cloud.zookeeper.discovery.ZookeeperInstance;
import org.springframework.cloud.zookeeper.serviceregistry.ServiceInstanceRegistration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.Map;

public abstract class AbstractGatewayConfiguration {

    protected abstract String getGatewayName();

    protected PaymentChannelDirection getGatewayDirection(){
        return PaymentChannelDirection.Outbound;
    }

    @Bean
    @Profile("!embedded-gateway")
    public GatewayDetailsService outboundGatewayDetailsService(PaymentProcessorRepository paymentProcessorRepository
            , @Value("${payments.environment}") String env){
        return new GatewayDetailsService(paymentProcessorRepository,getGatewayName());
    }

    @Bean
    @Profile("!embedded-gateway")
    public ServiceInstanceRegistration serviceInstanceRegistration(
            ApplicationContext context, ZookeeperDiscoveryProperties properties) {
        String appName = context.getEnvironment().getProperty("spring.application.name",
                "application");
        String host = properties.getInstanceHost();
        if (!StringUtils.hasText(host)) {
            throw new IllegalStateException("instanceHost must not be empty");
        }

        ZookeeperInstance zookeeperInstance = new ZookeeperInstance(appName,
                appName, properties.getMetadata());
        ServiceInstanceRegistration.RegistrationBuilder builder = ServiceInstanceRegistration.builder()
                .address(host)
                .port(properties.getInstancePort())
                .name(appName)
                .payload(zookeeperInstance)
                .uriSpec(properties.getUriSpec());

        if (properties.getInstanceSslPort() != null) {
            builder.sslPort(properties.getInstanceSslPort());
        }
        if (properties.getInstanceId() != null) {
            builder.id(properties.getInstanceId());
        }
        return builder.build();
    }

    @EventListener
    public void onApplicationStarted(WebServerInitializedEvent event){
        WebServerApplicationContext context = event.getApplicationContext();
        PaymentProcessorRepository repository = context.getBean(PaymentProcessorRepository.class);
        String gateway = getGatewayName();
        if(repository.findById(gateway).isEmpty()){
            Map<String, Object> subProperties = Binder.get(context.getEnvironment())
                    .bind(gateway+".channel", Bindable.mapOf(String.class, Object.class))
                    .orElseGet(Collections::emptyMap);
            PaymentProcessor.PaymentProcessorBuilder builder = PaymentProcessor.builder()
                    .name(gateway)
                    .createdBy("application");
            for (Map.Entry<String, Object> subProperty : subProperties.entrySet()) {
                String channelId = subProperty.getKey();
                String costExpression = (String)subProperty.getValue();
                builder.addChannel(PaymentProcessorChannel.builder()
                        .paymentChannel(PaymentChannel.valueOf(channelId))
                        .costExpression(costExpression)
                        .direction(getGatewayDirection())
                        .build());
            }
            repository.save(builder.build());
        }
    }

}
