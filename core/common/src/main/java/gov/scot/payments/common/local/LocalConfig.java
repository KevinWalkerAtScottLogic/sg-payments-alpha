package gov.scot.payments.common.local;

import com.beust.jcommander.JCommander;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

public interface LocalConfig {

    default Set<String> getProfiles(){
        return Collections.emptySet();
    }

    Map<String, Object> getProperties();

    String[] toArgs();

    default Set<String> validate(){
        return Collections.emptySet();
    }

    static <T extends LocalConfig> void parseHelp(Supplier<T> configSupplier, String[] args){
        LocalHelpConfig helpConfig = new LocalHelpConfig();
        JCommander.newBuilder()
                .addObject(helpConfig)
                .acceptUnknownOptions(true)
                .build()
                .parse(args);
        if(helpConfig.isHelp()){
            T config = configSupplier.get();
            JCommander.newBuilder()
                    .addObject(config)
                    .build()
                    .usage();
            System.exit(0);
        }
    }

    static <T extends LocalConfig> T parseArgs(Supplier<T> configSupplier, String[] args) throws Exception {
        parseHelp(configSupplier,args);
        LocalAutodiscoveryConfig discoveryClientConfig = new LocalAutodiscoveryConfig();
        JCommander.newBuilder()
                .addObject(discoveryClientConfig)
                .acceptUnknownOptions(true)
                .build()
                .parse(args);
        T config = configSupplier.get();
        String[] trimmedArgs = discoveryClientConfig.trimArgs(args);
        if(discoveryClientConfig.getAutoDiscover() != null){
            JCommander.newBuilder()
                    .defaultProvider(new ZookeeperDefaultProvider("localhost:"+discoveryClientConfig.getAutoDiscover()))
                    .addObject(config)
                    .acceptUnknownOptions(true)
                    .build()
                    .parse(trimmedArgs);
        } else{
            JCommander.newBuilder()
                    .addObject(config)
                    .build()
                    .parse(trimmedArgs);
        }
        Set<String> violations = config.validate();
        if(!violations.isEmpty()){
            throw new IllegalArgumentException(String.join("\n",violations));
        }
        return config;
    }

    static String q(String arg){
        if(StringUtils.containsWhitespace(arg)){
            return "\""+arg+"\"";
        }
        return arg;
    }

}
