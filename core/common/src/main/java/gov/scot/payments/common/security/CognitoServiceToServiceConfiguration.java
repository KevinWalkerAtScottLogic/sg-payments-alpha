package gov.scot.payments.common.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsAccessTokenProvider;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.web.client.RestOperations;

import java.util.function.Supplier;

@Configuration
@Profile("cognito")
public class CognitoServiceToServiceConfiguration {


    @Bean
    public Supplier<RestOperations> serviceToServiceRestOperations(@Value("${payments.service-to-service-auth.client-id}") String clientId
            , @Value("${payments.service-to-service-auth.client-secret}") String clientSecret
            , @Value("${cloud.aws.cognito.userPool}") String userPool
            , @Value("${cloud.aws.cognito.domain}") String domain){
        ClientCredentialsResourceDetails details = new ClientCredentialsResourceDetails();
        details.setAccessTokenUri(domain+"/oauth2/token");
        details.setClientId(clientId);
        details.setClientSecret(clientSecret);
        OAuth2RestTemplate template = new OAuth2RestTemplate(details);
        template.setAccessTokenProvider(new ClientCredentialsAccessTokenProvider());
        return () -> template;
    }
}
