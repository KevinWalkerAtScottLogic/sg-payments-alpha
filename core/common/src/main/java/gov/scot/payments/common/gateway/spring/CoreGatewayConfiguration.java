package gov.scot.payments.common.gateway.spring;

import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.common.security.CognitoWebSecurityConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({OptionalResponseControllerAdvice.class
        , GatewayWebSecurityConfiguration.class
        , CognitoWebSecurityConfiguration.class})
public class CoreGatewayConfiguration {

}
