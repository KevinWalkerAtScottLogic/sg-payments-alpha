package gov.scot.payments.common.security;

import gov.scot.payments.model.user.Client;

import java.util.Optional;

public interface UserRepository {

    Optional<Client> findById(String id);
    Client save(Client user);
}
