package gov.scot.payments.common;

import java.util.function.Function;

public interface Functions {

    static <T1, T2> T2 applyNullCoalescing(T1 target,
                                                  Function<T1, T2> f) {
        return target == null ? null : f.apply(target);
    }

    static <T1, T2, T3> T3 applyNullCoalescing(T1 target,
                                                      Function<T1, T2> f1, Function<T2, T3> f2) {
        return applyNullCoalescing(applyNullCoalescing(target, f1), f2);
    }
}
