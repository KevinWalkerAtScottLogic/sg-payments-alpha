package gov.scot.payments.common.local;

import com.beust.jcommander.DynamicParameter;
import com.beust.jcommander.Parameter;
import lombok.Data;
import org.springframework.util.SocketUtils;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

@Data
public class LocalApplicationConfig implements LocalConfig{

    @Parameter(names = "-tmpDir",hidden = true)
    private String tmpDir;

    @Parameter(names = "-env", hidden = true)
    private String environment = "test";

    @Parameter(names = "-port", description = "The port to launch the application on. By default a free port is chosen at random")
    private Integer port;

    @Parameter(names = "-browser", hidden = true)
    private boolean openBrowser;

    @Parameter(names = "-pidFile", hidden = true)
    private String pidFile;

    @Parameter(names = "-portFile", hidden = true)
    private String portFile;

    @DynamicParameter(names = "-D", description = "This argument can appear multiple times. a map of <SPRING_PROPERTY>=<VALUE>. Takes precedence over any other parameters",hidden = true)
    private Map<String, String> params = new HashMap<>();

    public LocalApplicationConfig(String tmpDir){
        this.tmpDir = tmpDir;
    }

    public LocalApplicationConfig(boolean createTmpDir){
        if(createTmpDir){
            Path tmpDir;
            try {
                tmpDir = Files.createTempDirectory(environment).toAbsolutePath();
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
            tmpDir.toFile().deleteOnExit();
            this.tmpDir = tmpDir.toString();
        }
    }

    @Override
    public Map<String, Object> getProperties(){
        if(port == null){
            port = SocketUtils.findAvailableTcpPort();
        }
        Map<String,Object> properties = new HashMap<>();
        properties.put("payments.environment",environment);
        properties.put("server.port",this.port);
        properties.put("browser.open",openBrowser);
        if(tmpDir != null){
            properties.put("payments.tmpDir",new File(tmpDir).getAbsolutePath());
        }
        properties.putAll(params);
        return properties;
    }

    @Override
    public String[] toArgs() {
        List<String> args = new ArrayList<>();
        if(environment != null){
            args.add("-env");
            args.add(environment);
        }
        if(openBrowser){
            args.add("-browser");
        }
        if(port != null){
            args.add("-port");
            args.add(port+"");
        }
        if(pidFile != null){
            args.add("-pidFile");
            args.add(pidFile);
        }
        if(portFile != null){
            args.add("-portFile");
            args.add(portFile);
        }
        params.forEach((k,v) -> args.add(LocalConfig.q("-D"+k+"="+ v)));
        return args.toArray(new String[0]);
    }

    @Override
    public String toString() {
        return String.join(" ", toArgs());
    }
}
