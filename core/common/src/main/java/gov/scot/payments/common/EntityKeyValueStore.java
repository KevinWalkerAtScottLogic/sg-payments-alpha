package gov.scot.payments.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.model.Entity;
import lombok.Setter;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.springframework.cloud.stream.binder.kafka.streams.InteractiveQueryService;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.*;

public class EntityKeyValueStore<T extends Entity> {

    private final InteractiveQueryService interactiveQueryService;
    private final ObjectMapper mapper;
    private final Class<T> entityClass;
    @Setter private String storeName;
    private ReadOnlyKeyValueStore<String, String> store;


    public EntityKeyValueStore(InteractiveQueryService interactiveQueryService, ObjectMapper mapper, Class<T> entityClass) {
        this.interactiveQueryService = interactiveQueryService;
        this.mapper = mapper;
        this.entityClass = entityClass;
    }

    public Optional<T> get(String key) {
        populateStateStore();
        String value = store.get(key);
        return Optional.ofNullable(value).map(v -> {
            try {
                return mapper.readValue(v,entityClass);
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        });
    }

    public Map<String, T> range(String from, String to) {
        populateStateStore();
        Map<String,T> values = new HashMap<>();
        store.range(from,to).forEachRemaining( kv -> {
            try {
                values.put(kv.key,mapper.readValue(kv.value,entityClass));
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        });
        return values;
    }

    public Map<String, T> all() {
        populateStateStore();
        Map<String,T> values = new HashMap<>();
        store.all().forEachRemaining( kv -> {
            try {
                values.put(kv.key,mapper.readValue(kv.value,entityClass));
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        });
        return values;
    }

    public long approximateNumEntries() {
        populateStateStore();
        return store.approximateNumEntries();
    }

    private void populateStateStore() {
        if(this.store == null){
            this.store = interactiveQueryService.getQueryableStore(storeName, QueryableStoreTypes.keyValueStore());
        }
    }

}
