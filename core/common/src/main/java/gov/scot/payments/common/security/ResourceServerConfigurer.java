package gov.scot.payments.common.security;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ResourceServerConfigurer extends ResourceServerConfigurerAdapter {

    private final TokenStore tokenStore;
    private final Consumer<ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry> pathCustomizer;

    public ResourceServerConfigurer(TokenStore tokenStore, Consumer<ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry> pathCustomizer){
        this.tokenStore = tokenStore;
        this.pathCustomizer = pathCustomizer;
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        DefaultTokenServices tokenServices = new DefaultTokenServices();
        tokenServices.setTokenStore(tokenStore);
        resources.tokenServices(tokenServices)
                .resourceId(null);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.headers().frameOptions().sameOrigin();
        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry expressionInterceptUrlRegistry = http
                .authorizeRequests();
        pathCustomizer.accept(expressionInterceptUrlRegistry);
        expressionInterceptUrlRegistry
                .anyRequest().authenticated()
                .and().cors().configurationSource(corsConfigurationSource())
                .and().csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration().applyPermitDefaultValues();
        config.setAllowedMethods(List.of(HttpMethod.GET, HttpMethod.PUT, HttpMethod.POST, HttpMethod.DELETE).stream().map(Enum::name).collect(Collectors.toList()));
        config.setAllowCredentials(true);
        //config.setAllowedHeaders(List.of(HttpHeaders.ORIGIN,HttpHeaders.CONTENT_TYPE,HttpHeaders.AUTHORIZATION,HttpHeaders.ACCEPT,HttpHeaders.ACCEPT_LANGUAGE,HttpHeaders.CONTENT_LANGUAGE));
        source.registerCorsConfiguration("/**", config);
        return source;
    }

}
