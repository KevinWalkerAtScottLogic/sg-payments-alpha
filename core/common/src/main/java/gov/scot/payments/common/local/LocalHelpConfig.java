package gov.scot.payments.common.local;

import com.beust.jcommander.Parameter;
import lombok.Data;

@Data
public class LocalHelpConfig {

    @Parameter(names = "--help", help = true)
    private boolean help;
}
