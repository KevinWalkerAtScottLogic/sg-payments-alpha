package gov.scot.payments.common.local;

import com.beust.jcommander.Parameter;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class LocalDatabaseConfig implements LocalConfig{

    @Parameter(names = "-db"
            , required = true
            , description = "The JDBC connection URL of the database to use, must be either a H2 or Postgres connection")
    private String db;
    @Parameter(names = "-db.user", description = "The database user to use when connecting (If required)")
    private String dbUser;
    @Parameter(names = "-db.pwd", description = "The database user password to use when connecting (If required)")
    private String dbPassword;

    public LocalDatabaseConfig(String db){
        this.db = db;
    }

    @Override
    public Map<String, Object> getProperties(){
        Map<String,Object> properties = new HashMap<>();
        properties.put("spring.datasource.url",db);
        if(dbUser != null){
            properties.put("spring.datasource.username",dbUser);
        }
        if(dbPassword != null){
            properties.put("spring.datasource.password",dbPassword);
        }
        return properties;
    }

    @Override
    public String[] toArgs() {
        List<String> args = new ArrayList<>();
        if(db != null){
            args.add("-db");
            args.add(db);
        }
        if(dbUser != null){
            args.add("-db.user");
            args.add(LocalConfig.q(dbUser));
        }
        if(dbPassword != null){
            args.add("-db.pwd");
            args.add(LocalConfig.q(dbPassword));
        }
        return args.toArray(new String[0]);
    }

    @Override
    public Set<String> validate() {
        Set<String> violations = new HashSet<>();
        if(db == null){
            violations.add("database url required");
        }
        return violations;
    }

    @Override
    public String toString() {
        return String.join(" ", toArgs());
    }
}
