package gov.scot.payments.common.local;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParametersDelegate;
import lombok.Data;

import java.util.*;

@Data
public class LocalCognitoConfig implements LocalConfig{

    @Parameter(names = "-cognito"
            , description = "Specify if you wish to use cognito for authentication. If not specified then dummy authentication will be used")
    private boolean cognito = false;
    @Parameter(names = "-cognito.pool"
            , description = "Only applicable if -cognito is specified. The cognito user pool id to use")
    private String userPool;
    @Parameter(names = "-cognito.appClient"
            , description = "Only applicable if -cognito is specified. The cognito app client id that the UI is configured to use")
    private String uiAppClient;
    @Parameter(names = "-cognito.domain"
            , description = "Only applicable if -cognito is specified. The cognito domain to use")
    private String domain = "sgpaypoc";

    @Override
    public Set<String> getProfiles(){
        if(cognito){
            return Set.of("cognito");
        }
        else{
            return Collections.emptySet();
        }
    }

    @Override
    public Map<String, Object> getProperties(){
        if(cognito){
            return Map.of("COGNITO_USER_POOL_ID",userPool,"UI_COGNITO_APP_CLIENT_ID",uiAppClient,"COGNITO_DOMAIN",domain);
        } else{
            return Collections.emptyMap();
        }
    }

    @Override
    public String[] toArgs() {
        List<String> args = new ArrayList<>();
        if(cognito){
            args.add("-cognito");
            if(userPool != null){
                args.add("-cognito.pool");
                args.add(LocalConfig.q(userPool));
            }
            if(uiAppClient != null){
                args.add("-cognito.appClient");
                args.add(LocalConfig.q(uiAppClient));
            }
            if(domain != null){
                args.add("-cognito.domain");
                args.add(LocalConfig.q(domain));
            }
        }
        return args.toArray(new String[0]);

    }

    @Override
    public Set<String> validate() {
        if(!cognito){
            return Collections.emptySet();
        }
        Set<String> violations = new HashSet<>();
        if(userPool == null){
            violations.add("cognito.pool must be provided");
        }
        if(uiAppClient == null){
            violations.add("cognito.appClient must be provided");
        }
        return violations;
    }

    @Override
    public String toString() {
        return String.join(" ", toArgs());
    }

}
