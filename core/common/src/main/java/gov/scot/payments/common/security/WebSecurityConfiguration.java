package gov.scot.payments.common.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableResourceServer
public class WebSecurityConfiguration {

    @Bean
    public ResourceServerConfigurer resourceServerConfigurerAdapter(@Autowired TokenStore tokenStore
            ,@Autowired Consumer<ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry> endpointCustomizer){
        return new ResourceServerConfigurer(tokenStore, endpointCustomizer);
    }

    @Bean
    @Profile("!(gateway-core | inbound-card-gateway-core)")
    public Consumer<ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry> endpointCustomizer() {
        return r -> {
            r.antMatchers(HttpMethod.GET,"/actuator/health**").permitAll()
                    .antMatchers("/resources/**").permitAll()
                    .antMatchers(HttpMethod.GET,"/cardpayments/*/form").permitAll()
                    .antMatchers(HttpMethod.POST,"/cardpayments/*/authorize","/cardpayments/*/submit","/cardpayments/*/cancel").permitAll();
        };
    }

}
