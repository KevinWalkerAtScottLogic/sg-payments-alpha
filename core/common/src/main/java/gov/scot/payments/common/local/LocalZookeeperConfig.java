package gov.scot.payments.common.local;

import com.beust.jcommander.Parameter;
import lombok.Data;

import java.util.*;

@Data
public class LocalZookeeperConfig implements LocalConfig{

    @Parameter(names = "-zk"
            , required = true
            , description = "The location of the zookeeper servers to use")
    private String zookeeperServers;

    private final boolean register;

    public LocalZookeeperConfig(){
        this(true);
    }

    public LocalZookeeperConfig(boolean register) {
        this.register = register;
    }

    public LocalZookeeperConfig(boolean register, String zookeeperServers) {
        this.register = register;
        this.zookeeperServers = zookeeperServers;
    }

    @Override
    public Set<String> getProfiles() {
        return Collections.singleton("msk");
    }

    @Override
    public Map<String, Object> getProperties(){
        return Map.of("ZOOKEEPER_SERVERS",zookeeperServers,"spring.cloud.zookeeper.discovery.register",register);
    }

    @Override
    public String[] toArgs() {
        List<String> args = new ArrayList<>();
        if(zookeeperServers != null){
            args.add("-zk");
            args.add(zookeeperServers);
        }
        return args.toArray(new String[0]);
    }

    @Override
    public Set<String> validate() {
        Set<String> violations = new HashSet<>();
        if(zookeeperServers == null){
            violations.add("zookeeper servers required");
        }
        return violations;
    }

    @Override
    public String toString() {
        return String.join(" ", toArgs());
    }
}
