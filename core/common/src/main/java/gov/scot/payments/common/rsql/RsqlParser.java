package gov.scot.payments.common.rsql;

import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.ast.Node;
import lombok.experimental.UtilityClass;
import org.springframework.data.jpa.domain.Specification;

@UtilityClass
public class RsqlParser {

    public static <T> Specification<T> parseQuery(String query) {
        final Node rootNode = new RSQLParser().parse(query);
        return rootNode.accept(new RsqlVisitor<>());
    }
}
