package gov.scot.payments.common.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsAccessTokenProvider;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;
import org.springframework.security.oauth2.provider.token.store.JwtClaimsSetVerifier;
import org.springframework.security.oauth2.provider.token.store.jwk.JwkTokenStore;
import org.springframework.web.client.RestOperations;

import java.util.Collections;
import java.util.function.Supplier;

@Configuration
@Profile("cognito")
public class CognitoWebSecurityConfiguration {

    @Bean
    public TokenStore tokenStore(@Value("${cloud.aws.cognito.userPool}") String issuer
        , JwtClaimsSetVerifier userJwtClaimsSetVerifier
        , UserAuthenticationConverter userAccessTokenConverter){
        String url = issuer+"/.well-known/jwks.json";
        DefaultAccessTokenConverter accessTokenConverter = new DefaultAccessTokenConverter();
        accessTokenConverter.setUserTokenConverter(userAccessTokenConverter);
        return new JwkTokenStore(url,accessTokenConverter,userJwtClaimsSetVerifier);
    }

    @Bean
    JwtClaimsSetVerifier userJwtClaimsSetVerifier(@Value("${UI_COGNITO_APP_CLIENT_ID}") String audience
            , @Value("${cloud.aws.cognito.userPool}") String issuer){
        return new CognitoUserClaimsVerifier(issuer, audience);
    }

    @Bean
    UserAuthenticationConverter userAccessTokenConverter( @Value("${payments.environment}") String environment){
        return new CognitoUserAuthenticationConverter(environment);
    }


}
