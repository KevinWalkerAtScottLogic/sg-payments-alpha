package gov.scot.payments.common.local;

import com.beust.jcommander.IDefaultProvider;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.springframework.cloud.zookeeper.ZookeeperProperties;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;

public class ZookeeperDefaultProvider implements IDefaultProvider {

    private final CuratorFramework zookeeper;

    public ZookeeperDefaultProvider(String zookeeperServers) throws InterruptedException {
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(50, 10, 500);
        CuratorFrameworkFactory.Builder builder = CuratorFrameworkFactory.builder();
        builder.connectString(zookeeperServers);
        CuratorFramework curator = builder.retryPolicy(retryPolicy).build();
        curator.start();
        curator.blockUntilConnected(10, TimeUnit.SECONDS);
        this.zookeeper = curator;
    }

    @Override
    public String getDefaultValueFor(String optionName) {
        try {
            return new String(zookeeper.getData().forPath("/config/"+optionName), Charset.forName("UTF-8"));
        } catch (Exception e) {
            return null;
        }
    }

}
