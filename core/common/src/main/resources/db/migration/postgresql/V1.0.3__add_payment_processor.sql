CREATE TABLE PAYMENT_PROCESSOR (
  name VARCHAR(255) PRIMARY KEY,
  created_at TIMESTAMP NOT NULL,
  created_by VARCHAR(255) NOT NULL
);

CREATE TABLE PAYMENT_PROCESSOR_CHANNELS (
    cost_expression VARCHAR(2048) NOT NULL,
    payment_channel VARCHAR(50) NOT NULL,
    payment_processor_name VARCHAR(255) NOT NULL REFERENCES PAYMENT_PROCESSOR (name),
    PRIMARY KEY (payment_processor_name,payment_channel)
);