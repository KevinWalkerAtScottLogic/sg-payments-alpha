CREATE TABLE CARD_PAYMENT (
    id UUID NOT NULL,
    amount DECIMAL NOT NULL,
    amount_currency CHAR(3) NOT NULL,
    status VARCHAR(20) NOT NULL,
    status_message TEXT,
    service VARCHAR(255) NOT NULL,
    reference VARCHAR(255),
    timestamp TIMESTAMP NOT NULL,
    processor_id VARCHAR(255),
    return_url VARCHAR(255) NOT NULL,
    created_by VARCHAR(255) NOT NULL,
    expected_cost DECIMAL NOT NULL,
    expected_cost_currency CHAR(3) NOT NULL,
    payment_ref UUID,
    auth JSONB,
    submission JSONB
);

ALTER TABLE INBOUND_PAYMENT RENAME COLUMN recipient TO service;
ALTER TABLE TEMPORAL_INBOUND_PAYMENT RENAME COLUMN recipient TO service;