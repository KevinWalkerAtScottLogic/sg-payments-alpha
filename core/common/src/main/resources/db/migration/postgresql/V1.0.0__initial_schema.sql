CREATE TABLE USERS (
  email VARCHAR(255) NOT NULL,
  user_name VARCHAR(255) PRIMARY KEY
);

CREATE INDEX user_email ON USERS(email);

CREATE TABLE USER_ROLES (
  operation VARCHAR(50) NOT NULL,
  entity_type VARCHAR(255) NOT NULL,
  entity_id VARCHAR(255) NOT NULL,
  user_user_name VARCHAR(255) NOT NULL REFERENCES USERS (user_name),
  PRIMARY KEY (user_user_name,entity_type,entity_id,operation)
);

CREATE TABLE PAYMENT_FILE(
  id UUID PRIMARY KEY,
  name VARCHAR(1000) NOT NULL,
  service VARCHAR(255) NOT NULL,
  status VARCHAR(15) NOT NULL,
  type VARCHAR(50) NOT NULL,
  url VARCHAR(2083) NOT NULL,
  status_message TEXT,
  created_at TIMESTAMP NOT NULL,
  timestamp TIMESTAMP NOT NULL
);

CREATE INDEX payment_file_service ON PAYMENT_FILE(service);
CREATE INDEX payment_file_name ON PAYMENT_FILE(name);
CREATE INDEX payment_file_status ON PAYMENT_FILE(status);
CREATE INDEX payment_file_type ON PAYMENT_FILE(type);
CREATE INDEX payment_file_timestamp ON PAYMENT_FILE(timestamp DESC);

CREATE TABLE CUSTOMER (
  id  VARCHAR(255) PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  created_by VARCHAR(255) NOT NULL,
  created_at TIMESTAMP NOT NULL
);
CREATE TABLE SERVICE (
  id  VARCHAR(255) PRIMARY KEY,
  file_format VARCHAR(50) NOT NULL,
  folder VARCHAR(255) NOT NULL,
  created_at TIMESTAMP NOT NULL,
  created_by VARCHAR(255) NOT NULL,
  customer_id VARCHAR(255) NOT NULL REFERENCES CUSTOMER (id)
);

CREATE INDEX service_folder ON SERVICE(folder);
CREATE INDEX service_file_format ON SERVICE(file_format);
CREATE INDEX service_customer_id ON SERVICE(customer_id);

CREATE TABLE SERVICE_PAYMENT_VALIDATION_RULES (
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255),
    message_template VARCHAR(2048) NOT NULL,
    name VARCHAR(255) NOT NULL,
    precedence INT NOT NULL,
    rule VARCHAR(2048) NOT NULL,
    service_id VARCHAR(255) NOT NULL REFERENCES SERVICE (id),
    PRIMARY KEY (service_id,name)
);

CREATE TABLE REPORT (
    name VARCHAR(255) PRIMARY KEY,
    dashboard_id VARCHAR(2048) NOT NULL,
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255) NOT NULL
);

CREATE TABLE TEMPORAL_PAYMENT (
    id UUID NOT NULL,
    version_id UUID PRIMARY KEY,
    timestamp TIMESTAMP NOT NULL,
    status VARCHAR(20) NOT NULL,
    service VARCHAR(255) NOT NULL,
    details JSONB NOT NULL
);

CREATE INDEX temporal_payment_id ON TEMPORAL_PAYMENT(id);
CREATE INDEX temporal_payment_status ON TEMPORAL_PAYMENT(status);
CREATE INDEX temporal_payment_timestamp ON TEMPORAL_PAYMENT(timestamp DESC);
CREATE INDEX temporal_payment_service ON TEMPORAL_PAYMENT(service);

CREATE TABLE PAYMENT (
    id UUID PRIMARY KEY,
    created_at TIMESTAMP NOT NULL,
    status VARCHAR(20) NOT NULL,
    status_message TEXT,
    batch_id TEXT,
    payment_file VARCHAR(255),
    service VARCHAR(255) NOT NULL,
    latest_target_settlement_date TIMESTAMP NOT NULL,
    earliest_target_settlement_date TIMESTAMP,
    target_settlement_currency CHAR(3) NOT NULL,
    target_channel_type VARCHAR(50) NOT NULL,
    target_amount DECIMAL NOT NULL,
    target_amount_currency CHAR(3) NOT NULL,
    recipient_sort_code CHAR(6),
    recipient_account_number CHAR(8),
    recipient_address TEXT,
    recipient_bic CHAR(11),
    recipient_email VARCHAR(255),
    recipient_iban CHAR(34),
    recipient_mobile CHAR(8),
    recipient_name VARCHAR(255),
    recipient_ref VARCHAR(255),
    validation_message TEXT,
    validation_status VARCHAR(20),
    validated_at TIMESTAMP,
    routing_status VARCHAR(20),
    routing_message TEXT,
    routed_via VARCHAR(50),
    routed_to VARCHAR(15),
    routed_via_queue VARCHAR(255),
    expected_cost DECIMAL,
    expected_cost_currency CHAR(3),
    routed_at TIMESTAMP,
    execution_status VARCHAR(20),
    execution_message TEXT,
    executed_via VARCHAR(255),
    executed_on VARCHAR(15),
    executed_amount DECIMAL,
    executed_amount_currency CHAR(3),
    executed_at TIMESTAMP,
    clearing_status VARCHAR(20),
    clearing_message TEXT,
    cleared_at TIMESTAMP,
    cleared_amount DECIMAL,
    cleared_amount_currency CHAR(3),
    settled_at TIMESTAMP,
    settled_amount DECIMAL,
    settled_amount_currency CHAR(3)
);

CREATE INDEX payment_status ON PAYMENT(status);
CREATE INDEX payment_service ON PAYMENT(service);
CREATE INDEX payment_created_at ON PAYMENT(created_at DESC);