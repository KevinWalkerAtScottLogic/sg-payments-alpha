CREATE domain IF NOT EXISTS jsonb AS other;

CREATE TABLE USERS (
  email VARCHAR(255) NOT NULL,
  user_name VARCHAR(255) NOT NULL
);

CREATE TABLE USER_ROLES (
  operation VARCHAR(50) NOT NULL,
  entity_type VARCHAR(255) NOT NULL,
  entity_id VARCHAR(255) NOT NULL,
  user_user_name VARCHAR(255) NOT NULL
);

CREATE TABLE PAYMENT_FILE(
  id UUID NOT NULL,
  name VARCHAR(255) NOT NULL,
  service VARCHAR(255) NOT NULL,
  status VARCHAR(15) NOT NULL,
  type VARCHAR(50) NOT NULL,
  url VARCHAR(2083) NOT NULL,
  status_message CLOB,
  created_at TIMESTAMP NOT NULL,
  created_by VARCHAR(255) NOT NULL,
  timestamp TIMESTAMP NOT NULL
);
CREATE TABLE CUSTOMER (
  id  VARCHAR(255) NOT NULL,
  name VARCHAR(255) NOT NULL,
  created_by VARCHAR(255) NOT NULL,
  created_at TIMESTAMP NOT NULL
);
CREATE TABLE SERVICE (
  id  VARCHAR(255) NOT NULL,
  file_format VARCHAR(50),
  folder VARCHAR(255),
  created_at TIMESTAMP NOT NULL,
  created_by VARCHAR(255) NOT NULL,
  customer_id VARCHAR(255) NOT NULL,
  inbound_account CHAR(8),
  outbound_account CHAR(8),
  inbound_sort_code CHAR(6),
  outbound_sort_code CHAR(6)
);

CREATE TABLE SERVICE_METADATA (
  key VARCHAR(255) NOT NULL,
  value VARCHAR(255) NOT NULL,
  service_id VARCHAR(255) NOT NULL
);

CREATE TABLE SERVICE_PAYMENT_VALIDATION_RULES (
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255),
    message_template VARCHAR(2048) NOT NULL,
    name VARCHAR(255) NOT NULL,
    precedence INT NOT NULL,
    rule VARCHAR(2048) NOT NULL,
    service_id VARCHAR(255) NOT NULL
);
CREATE TABLE REPORT (
    id VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,
    dashboard_id VARCHAR(2048) NOT NULL,
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255) NOT NULL
);
CREATE TABLE TEMPORAL_PAYMENT (
    id UUID NOT NULL,
    version_id UUID NOT NULL,
    timestamp TIMESTAMP NOT NULL,
    status VARCHAR(15) NOT NULL,
    service VARCHAR(255) NOT NULL,
    details jsonb NOT NULL
);

CREATE TABLE PAYMENT (
    id UUID NOT NULL,
    created_at TIMESTAMP NOT NULL,
    created_by VARCHAR(255) NOT NULL,
    status VARCHAR(20) NOT NULL,
    status_message CLOB,
    batch_id CLOB,
    payment_file VARCHAR(255),
    service VARCHAR(255) NOT NULL,
    latest_target_settlement_date TIMESTAMP NOT NULL,
    earliest_target_settlement_date TIMESTAMP,
    target_settlement_currency CHAR(3) NOT NULL,
    target_channel_type VARCHAR(50) NOT NULL,
    target_amount DECIMAL NOT NULL,
    target_amount_currency CHAR(3) NOT NULL,
    recipient_sort_code CHAR(6),
    recipient_account_number CHAR(8),
    recipient_address CLOB,
    recipient_bic CHAR(11),
    recipient_email VARCHAR(255),
    recipient_iban CHAR(34),
    recipient_mobile CHAR(8),
    recipient_name VARCHAR(255),
    recipient_ref VARCHAR(255),
    validation_message CLOB,
    validation_status VARCHAR(20),
    validated_at TIMESTAMP,
    routing_status VARCHAR(20),
    routing_message CLOB,
    routed_via VARCHAR(50),
    routed_to VARCHAR(15),
    routed_via_queue VARCHAR(255),
    expected_cost DECIMAL,
    expected_cost_currency CHAR(3),
    routed_at TIMESTAMP,
    execution_status VARCHAR(20),
    execution_message CLOB,
    executed_via VARCHAR(255),
    executed_on VARCHAR(15),
    executed_amount DECIMAL,
    executed_amount_currency CHAR(3),
    executed_at TIMESTAMP,
    clearing_status VARCHAR(20),
    clearing_message CLOB,
    cleared_at TIMESTAMP,
    cleared_amount DECIMAL,
    cleared_amount_currency CHAR(3),
    settled_at TIMESTAMP,
    settled_amount DECIMAL,
    settled_amount_currency CHAR(3)
);

CREATE TABLE PAYMENT_PROCESSOR (
  name VARCHAR(255) NOT NULL,
  created_at TIMESTAMP NOT NULL,
  created_by VARCHAR(255) NOT NULL
);

CREATE TABLE PAYMENT_PROCESSOR_CHANNELS (
    cost_expression VARCHAR(2048) NOT NULL,
    payment_channel VARCHAR(50) NOT NULL,
    direction VARCHAR(50) NOT NULL,
    payment_processor_name VARCHAR(255) NOT NULL
);

CREATE TABLE INBOUND_PAYMENT (
    id UUID NOT NULL,
    created_at TIMESTAMP NOT NULL,
    amount DECIMAL NOT NULL,
    amount_currency CHAR(3) NOT NULL,
    channel_type VARCHAR(20) NOT NULL,
    channel_id VARCHAR(255) NOT NULL,
    ref VARCHAR(255),
    status VARCHAR(20) NOT NULL,
    status_message CLOB,
    service VARCHAR(255) NOT NULL,
    cleared_amount DECIMAL,
    cleared_amount_currency CHAR(3),
    clearing_status VARCHAR(20),
    clearing_message CLOB,
    cleared_at TIMESTAMP,
    settled_amount DECIMAL,
    settled_amount_currency CHAR(3),
    settled_at TIMESTAMP,
    created_by VARCHAR(255) NOT NULL,
    details jsonb
);

CREATE TABLE TEMPORAL_INBOUND_PAYMENT (
    id UUID NOT NULL,
    service VARCHAR(255) NOT NULL,
    version_id UUID NOT NULL,
    timestamp TIMESTAMP NOT NULL,
    status VARCHAR(20) NOT NULL,
    channel_id VARCHAR(255) NOT NULL,
    details jsonb NOT NULL
);

CREATE TABLE CARD_PAYMENT (
    id UUID NOT NULL,
    amount DECIMAL NOT NULL,
    amount_currency CHAR(3) NOT NULL,
    status VARCHAR(20) NOT NULL,
    status_message CLOB,
    service VARCHAR(255) NOT NULL,
    reference VARCHAR(255),
    timestamp TIMESTAMP NOT NULL,
    processor_id VARCHAR(255),
    processor_ref VARCHAR(255),
    return_url VARCHAR(255) NOT NULL,
    created_by VARCHAR(255) NOT NULL,
    expected_cost DECIMAL NOT NULL,
    expected_cost_currency CHAR(3) NOT NULL,
    payment_ref UUID,
    auth jsonb,
    submission jsonb
);
