package gov.scot.payments;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.model.user.ApiClient;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.model.user.Scope;
import gov.scot.payments.model.user.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.jwt.crypto.sign.SignatureVerifier;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.implicit.ImplicitResourceDetails;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.client.RestOperations;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Base64;
import java.util.Map;
import java.util.function.Supplier;

@Configuration
@Profile("!cognito")
public class MockWebSecurityConfigurationCore {

    @Bean
    public TokenStore tokenStore(JwtAccessTokenConverter jwtTokenEnhancer){
        return new JwtTokenStore(jwtTokenEnhancer);
    }

    @Bean
    JwtAccessTokenConverter jwtTokenEnhancer(ObjectMapper mapper){
        JwtAccessTokenConverter converter =  new MockJwtAccessTokenConverter(mapper);
        converter.setAccessTokenConverter(new MockAccessTokenConverter(mapper));
        converter.setVerifier(new SignatureVerifier() {
            @Override
            public void verify(byte[] content, byte[] signature) {

            }

            @Override
            public String algorithm() {
                return "";
            }
        });
        return converter;
    }

    @Bean
    public Supplier<RestOperations> serviceToServiceRestOperations(ObjectMapper mapper){
        return () -> {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            DefaultOAuth2AccessToken token;
            if(authentication instanceof OAuth2Authentication) {
                OAuth2Authentication oauthAuthentication = (OAuth2Authentication) authentication;
                OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) oauthAuthentication.getDetails();
                token = new DefaultOAuth2AccessToken(details.getTokenValue());
                token.setTokenType(details.getTokenType());
            } else {
                Client client = User.admin();
                try{
                    String tokenValue = Base64.getEncoder().encodeToString(mapper.writeValueAsBytes(client));
                    token = new DefaultOAuth2AccessToken(tokenValue);
                } catch(IOException e){
                    throw new UncheckedIOException(e);
                }
            }
            return new OAuth2RestTemplate(new ImplicitResourceDetails(), new DefaultOAuth2ClientContext(token));
        };
    }

    private class MockJwtAccessTokenConverter extends JwtAccessTokenConverter {

        private final ObjectMapper mapper;

        public MockJwtAccessTokenConverter(ObjectMapper mapper) {
            this.mapper = mapper;
        }

        @Override
        protected Map<String, Object> decode(String token) {
            try {
                byte[] plainToken = Base64.getDecoder().decode(token);
                return mapper.readValue(plainToken,Map.class);
            } catch (Exception e) {
                throw new InvalidTokenException("Cannot convert access token to JSON", e);
            }
        }
    }

    private class MockAccessTokenConverter implements AccessTokenConverter {

        private final ObjectMapper mapper;

        public MockAccessTokenConverter(ObjectMapper mapper) {
            this.mapper = mapper;
        }

        @Override
        public Map<String, ?> convertAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {
            return null;
        }

        @Override
        public OAuth2AccessToken extractAccessToken(String value, Map<String, ?> map) {
           return new DefaultOAuth2AccessToken(value);
        }

        @Override
        public OAuth2Authentication extractAuthentication(Map<String, ?> map) {
            Client user;
            try {
                if(map.containsKey("scopes")){
                    user = mapper.readValue(mapper.writeValueAsString(map), ApiClient.class);
                } else{
                    user = mapper.readValue(mapper.writeValueAsString(map), User.class);
                }
            } catch (Exception e) {
                throw new InvalidTokenException("Cannot convert access token to JSON", e);
            }
            OAuth2Request storedRequest = new OAuth2Request(null,null,null,true,null,null,null,null,null);
            return new OAuth2Authentication(storedRequest,new UsernamePasswordAuthenticationToken(user, "N/A", user.getAuthorities()));
        }
    }


}
