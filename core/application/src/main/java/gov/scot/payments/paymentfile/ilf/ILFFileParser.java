package gov.scot.payments.paymentfile.ilf;


import gov.scot.payments.paymentfile.FileParseException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ILFFileParser {


    public final static CSVFormat ILF_CSV_FORMAT = CSVFormat.RFC4180
            .withTrim()
            .withNullString("")
            .withIgnoreEmptyLines(true);


    private final static int MAX_COLUMN_COUNT = 6;
    private final static int MIN_COLUMN_COUNT = 5;

    private boolean isEmptyCSVRecord(CSVRecord record) {

        for (String value : record) {
            if (value != null) {
                return false;
            }
        }
        return true;
    }

    private void checkNumberOfColumns(CSVRecord record) throws FileParseException {

        if (record.size() > MAX_COLUMN_COUNT || record.size() < MIN_COLUMN_COUNT) {

            String errorMessage = "Row " + record.getRecordNumber() + " has " + record.size() + " columns. " +
                    "Must be between " + MIN_COLUMN_COUNT + "-" + MAX_COLUMN_COUNT;
            throw new FileParseException(errorMessage);
        }
    }

    public List<ILFBACSPayment> generatePaymentList(InputStream paymentFileStream) throws IOException, FileParseException {

        var reader = new InputStreamReader(paymentFileStream);
        List<ILFBACSPayment> parsed = new ArrayList<>();

        try (var csvParser = new CSVParser(reader, ILF_CSV_FORMAT)) {
            for (CSVRecord csvRecord : csvParser) {
                if (isEmptyCSVRecord(csvRecord)) {
                    continue;
                }
                checkNumberOfColumns(csvRecord);
                try {
                    parsed.add(ILFBACSPayment.fromCSVRecord(csvRecord));
                } catch (InvalidCSVFieldException e){
                    throw new FileParseException(e.getMessage(),e);
                }
            }
        }
        return parsed;
    }

}
