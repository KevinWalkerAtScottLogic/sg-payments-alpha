package gov.scot.payments.paymentfile.ilf;


import lombok.Getter;

public class InvalidCSVFieldException extends Exception {

    @Getter
    private final long rowNumber;
    @Getter
    private final String fieldName;

    public InvalidCSVFieldException(long rowNumber, String fieldName, String errorMessage){
        super(errorMessage);
        this.rowNumber = rowNumber;
        this.fieldName = fieldName;
    }

    public InvalidCSVFieldException(long rowNumber, String fieldName, Throwable cause){
        super(cause.getMessage(),cause);
        this.rowNumber = rowNumber;
        this.fieldName = fieldName;
    }

    @Override
    public String getMessage() {
        return "[Row "+rowNumber+" Field Name:'"+fieldName+"'] Parsing error: "+ super.getMessage();
    }

}
