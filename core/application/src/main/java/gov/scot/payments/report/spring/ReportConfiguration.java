package gov.scot.payments.report.spring;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import gov.scot.payments.model.report.Report;
import gov.scot.payments.report.AmazonQuickSightClientFactory;
import gov.scot.payments.report.ReportRepository;
import gov.scot.payments.report.ReportService;
import gov.scot.payments.security.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.bind.Bindable;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.boot.web.context.WebServerApplicationContext;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.cloud.aws.core.region.RegionProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;

import java.util.Collections;
import java.util.Map;

@Configuration
@Slf4j
public class ReportConfiguration {

    @Bean
    @Profile("quicksight")
    public AmazonQuickSightClientFactory amazonQuickSightClientFactory(AWSCredentialsProvider credentialsProvider
            , RegionProvider regionProvider
            , @Value("${QUICK_SIGHT_ROLE_ARN:default}") String quickSightRoleArn){
        AWSSecurityTokenService stsClient = AWSSecurityTokenServiceClientBuilder
                .standard()
                .withRegion(regionProvider.getRegion().getName())
                .withCredentials(credentialsProvider)
                .build();
        return new AmazonQuickSightClientFactory(regionProvider,quickSightRoleArn, stsClient, credentialsProvider);
    }

    @EventListener
    public void onApplicationStarted(WebServerInitializedEvent event){
        WebServerApplicationContext context = event.getApplicationContext();
        ReportRepository repository = context.getBean(ReportRepository.class);
        String env = context.getEnvironment().getProperty("payments.environment");
        Map<String, Object> subProperties = Binder.get(context.getEnvironment())
                .bind("payments.report."+env, Bindable.mapOf(String.class, Object.class))
                .orElseGet(Collections::emptyMap);
        for (Map.Entry<String, Object> subProperty : subProperties.entrySet()) {
            String reportId = subProperty.getKey();
            Map<String,Object> reportDetails = (Map<String,Object>)subProperty.getValue();
            String reportName = (String)reportDetails.get("name");
            String dashboardId = (String)reportDetails.get("quicksight-dashboard");
            if(!repository.existsById(reportId)){
                log.info("Registering Report: {} with Quicksight Dashboard: {}",reportName,dashboardId);
                Report report = Report.builder()
                        .id(reportId)
                        .dashboardId(dashboardId)
                        .name(reportName)
                        .createdBy("application")
                        .build();
                repository.save(report);
            }
        }
    }

}
