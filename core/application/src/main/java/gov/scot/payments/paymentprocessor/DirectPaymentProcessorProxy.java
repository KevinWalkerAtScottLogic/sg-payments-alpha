package gov.scot.payments.paymentprocessor;

import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import org.apache.curator.x.discovery.ServiceDiscovery;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.cloud.client.serviceregistry.ServiceRegistry;
import org.springframework.cloud.zookeeper.discovery.ZookeeperInstance;
import org.springframework.cloud.zookeeper.serviceregistry.ServiceInstanceRegistration;
import org.springframework.cloud.zookeeper.serviceregistry.ZookeeperRegistration;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.springframework.cloud.zookeeper.support.StatusConstants.*;

public class DirectPaymentProcessorProxy implements PaymentProcessorProxy{

    private final PaymentProcessorRepository repository;
    private final ServiceRegistry serviceDiscovery;
    private final DiscoveryClient discoveryClient;
    private final String environment;

    public DirectPaymentProcessorProxy(PaymentProcessorRepository repository
            , ServiceRegistry serviceDiscovery
            , DiscoveryClient discoveryClient
            , String environment) {
        this.repository = repository;
        this.serviceDiscovery = serviceDiscovery;
        this.discoveryClient = discoveryClient;
        this.environment = environment;
    }

    @Override
    public String setGatewayStatus(String name, URI uri, boolean enabled) {
        ServiceInstance serviceInstance = discoveryClient.getInstances(environment+".gateway."+name)
                .stream()
                .findFirst()
                .orElseThrow(() -> new RuntimeException("No Service found: "+name));
        String status = enabled ? STATUS_UP : STATUS_OUT_OF_SERVICE;
        Registration registration = ServiceInstanceRegistration.builder()
                .payload((ZookeeperInstance) serviceInstance)
                .build();
        serviceDiscovery.setStatus(registration, status);
        return getStatus(name, uri);
    }

    @Override
    public String getStatus(String name, URI uri) {
        List<ServiceInstance> serviceInstances = discoveryClient.getInstances(environment+".gateway."+name);
        if(serviceInstances == null || serviceInstances.isEmpty()){
            return STATUS_OUT_OF_SERVICE;
        }
        return serviceInstances.stream()
                .findFirst()
                .map(ServiceInstance::getMetadata)
                .filter(Objects::nonNull)
                .map(m -> m.get(INSTANCE_STATUS_KEY))
                .map(s -> s == null ? STATUS_UP : s)
                .orElse(STATUS_OUT_OF_SERVICE);
    }

    @Override
    public PaymentProcessor updateDetails(URI uri, PaymentProcessor processor) {
        return repository.save(processor);
    }

    @Override
    public PaymentProcessor getDetails(String name, URI uri) {
        return repository.findById(name).orElse(null);
    }
}
