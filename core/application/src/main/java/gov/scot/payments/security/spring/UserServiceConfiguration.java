package gov.scot.payments.security.spring;

import gov.scot.payments.security.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserServiceConfiguration {

    @Bean
    public UserService userService(){
        return new UserService();
    }

}
