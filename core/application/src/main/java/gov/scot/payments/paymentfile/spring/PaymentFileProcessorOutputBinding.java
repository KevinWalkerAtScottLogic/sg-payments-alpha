package gov.scot.payments.paymentfile.spring;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface PaymentFileProcessorOutputBinding {

    @Output("file-uploaded-events")
    MessageChannel fileUploadedEvents();

}
