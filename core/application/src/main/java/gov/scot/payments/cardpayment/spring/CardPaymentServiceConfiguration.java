package gov.scot.payments.cardpayment.spring;

import gov.scot.payments.cardpayment.*;
import gov.scot.payments.common.gateway.inbound.cardpayment.CardPaymentGateway;
import gov.scot.payments.customer.CustomerServiceService;
import gov.scot.payments.customer.ServiceRepository;
import gov.scot.payments.paymentprocessor.PaymentProcessorService;
import gov.scot.payments.router.PaymentRouter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.messaging.MessageChannel;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.web.client.RestOperations;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriBuilderFactory;

import java.net.URI;
import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Configuration
public class CardPaymentServiceConfiguration {

    @Bean
    public CardPaymentService cardPaymentService(CardPaymentRepository cardPaymentRepository
            , PaymentRouter router
            , CardPaymentGatewayProxy cardPaymentGatewayProxy
            , ServiceRepository customerServiceService
            , @Qualifier("inbound-payment-received-events") MessageChannel inboundPaymentReceivedEvents
            , @Qualifier("inbound-payment-attemptFailed-events") MessageChannel inboundPaymentAttemptFailedEvents
            , @Value("${cardPayment.paymentTimeout:PT90M}") String paymentTimeout
            , @Value("${cardPayment.baseUrl}") String basePath ) {
        return new CardPaymentService(cardPaymentRepository
                ,router
                , cardPaymentGatewayProxy
                ,inboundPaymentReceivedEvents
                ,inboundPaymentAttemptFailedEvents
                , customerServiceService::findById
                , Duration.parse(paymentTimeout)
                , basePath);
    }

}
