package gov.scot.payments.inboundpayment;

import gov.scot.payments.model.inboundpayment.InboundPayment;
import gov.scot.payments.model.inboundpayment.PaymentStatus;
import gov.scot.payments.model.user.EntityOp;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.paymentinstruction.FlattenedPayment;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.*;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static gov.scot.payments.common.rsql.RsqlParser.parseQuery;
import static org.springframework.util.StringUtils.isEmpty;

@RequestMapping(value = "/inboundpayments")
@Slf4j
public class InboundPaymentQueryService {

    private final TemporalInboundPaymentRepository temporalInboundPaymentRepository;
    private final NonTemporalInboundPaymentRepository nonTemporalInboundPaymentRepository;


    public InboundPaymentQueryService(TemporalInboundPaymentRepository temporalInboundPaymentRepository,
                                      NonTemporalInboundPaymentRepository nonTemporalInboundPaymentRepository) {
        this.temporalInboundPaymentRepository = temporalInboundPaymentRepository;
        this.nonTemporalInboundPaymentRepository = nonTemporalInboundPaymentRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public PagedResources<Resource<InboundPayment>> getAllLatestPayments(@PageableDefault(size = 100,sort = "createdAt",direction = Sort.Direction.DESC) Pageable paging
            , PagedResourcesAssembler assembler
            , @ApiIgnore @AuthenticationPrincipal Client currentUser
    ){
        Page<FlattenedInboundPayment> payments = currentUser.hasWildcardAccess("service", EntityOp.Read)
                ? nonTemporalInboundPaymentRepository.findAll(paging) : nonTemporalInboundPaymentRepository.findAllWithAcl(paging);
        return assembler.toResource(payments.map(FlattenedInboundPayment::toPayment));
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Optional<InboundPayment> getPaymentById(@PathVariable("id") UUID id, @ApiIgnore @AuthenticationPrincipal Client currentUser) {
        Optional<FlattenedInboundPayment> byPaymentId = currentUser.hasWildcardAccess("service", EntityOp.Read)
                ? nonTemporalInboundPaymentRepository.findByPaymentId(id) : nonTemporalInboundPaymentRepository.findByPaymentIdWithAcl(id);
        return byPaymentId.map(FlattenedInboundPayment::toPayment);
    }

    @GetMapping("/{id}/history")
    @ResponseBody
    public List<InboundPayment> getPaymentHistoryById(@PathVariable("id") UUID id, @ApiIgnore @AuthenticationPrincipal Client currentUser){
        List<TemporalInboundPayment> allByPaymentId = currentUser.hasWildcardAccess("service", EntityOp.Read)
                ? temporalInboundPaymentRepository.findAllByInboundPaymentId(id) : temporalInboundPaymentRepository.findAllByInboundPaymentIdWithAcl(id);
        return allByPaymentId
                .stream()
                .map(TemporalInboundPayment::getDetails)
                .collect(Collectors.toList());
    }

    //TODO: could be streaming output
    @GetMapping(value="/query/download", produces ="text/csv")
    @ResponseBody
    public HttpEntity<byte[]> queryLatestPaymentsCSV(@RequestParam(value = "expression") String query,@ApiIgnore @AuthenticationPrincipal Client currentUser) throws IOException {
        Specification<FlattenedInboundPayment> spec = getFlattenedInboundPaymentSpecification(query, currentUser);
        try(CSVPrinter csv = new CSVPrinter(new StringBuilder(), CSVFormat.DEFAULT.withHeader(FlattenedInboundPayment.getCSVHeader()))){
            nonTemporalInboundPaymentRepository.findAll(spec)
                    .stream()
                    .map(FlattenedInboundPayment::toCSVRow)
                    .forEach( (Object[] p) -> {
                        try {
                            csv.printRecord(p);
                        } catch (IOException e) {
                            throw new UncheckedIOException(e);
                        }
                    });
            byte[] content =  csv.getOut().toString().getBytes();
            HttpHeaders header = new HttpHeaders();
            header.setContentType(MediaType.valueOf("text/csv"));
            header.setContentDisposition(ContentDisposition.builder("attachment").filename("inboundpayments.csv").build());
            header.setContentLength(content.length);
            return new HttpEntity<>(content, header);
        }
    }

    @GetMapping("/query")
    @ResponseBody
    public PagedResources<Resource<InboundPayment>> queryLatestPayments(@RequestParam(value = "expression") String query
            , @PageableDefault(size = 100,sort = "createdAt",direction = Sort.Direction.DESC) Pageable paging
            , PagedResourcesAssembler assembler
            , @ApiIgnore @AuthenticationPrincipal Client currentUser
    ){
        Specification<FlattenedInboundPayment> spec = getFlattenedInboundPaymentSpecification(query, currentUser);
        Page<FlattenedInboundPayment> payments = nonTemporalInboundPaymentRepository.findAll(spec,paging);
        return assembler.toResource(payments.map(FlattenedInboundPayment::toPayment));
    }

    private Specification<FlattenedInboundPayment> getFlattenedInboundPaymentSpecification(@RequestParam("expression") String query, @ApiIgnore @AuthenticationPrincipal Client currentUser) {
        if(isEmpty(query)){
            return null;
        }
        Specification<FlattenedInboundPayment> spec;
        try{
            spec = parseQuery(query);
        } catch(Exception exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Invalid query format, see: https://github.com/jirutka/rsql-parser for grammar", exception);
        }
        if(!currentUser.hasWildcardAccess("service", EntityOp.Read)){
            spec = spec.and(Specification.where( (root, q, builder) -> root.get("service").in(currentUser.getEntityIdsWithAccess("service",EntityOp.Read))));
        }
        return spec;
    }

    @GetMapping("/statuses")
    @ResponseBody
    public List<PaymentStatus> getPaymentStatuses(){
        return Arrays.asList(PaymentStatus.values());
    }
}
