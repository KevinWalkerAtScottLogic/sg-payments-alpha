package gov.scot.payments.customer.spring;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.customer.CustomerPersistenceProcessor;
import gov.scot.payments.customer.CustomerRepository;
import gov.scot.payments.customer.ServicePersistenceProcessor;
import gov.scot.payments.customer.ServiceRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomerPersistenceConfiguration {


    @Bean
    public CustomerPersistenceProcessor customerProcessor(CustomerRepository customerRepository){
        return new CustomerPersistenceProcessor(customerRepository);
    }

    @Bean
    public ServicePersistenceProcessor serviceProcessor(ServiceRepository serviceRepository, ObjectMapper mapper){
        return new ServicePersistenceProcessor(serviceRepository,mapper);
    }
}
