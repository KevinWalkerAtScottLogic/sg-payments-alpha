package gov.scot.payments.spring;

import gov.scot.payments.cardpayment.spring.CardPaymentServiceConfiguration;
import gov.scot.payments.customer.spring.CustomerServiceConfiguration;
import gov.scot.payments.inboundpayment.spring.InboundPaymentQueryServiceConfiguration;
import gov.scot.payments.paymentfile.spring.PaymentFileServiceConfiguration;
import gov.scot.payments.paymentinstruction.spring.PaymentQueryServiceConfiguration;
import gov.scot.payments.paymentprocessor.spring.PaymentProcessorConfiguration;
import gov.scot.payments.paymentprocessor.spring.PaymentProcessorServiceConfiguration;
import gov.scot.payments.report.spring.ReportConfiguration;
import gov.scot.payments.report.spring.ReportServiceConfiguration;
import gov.scot.payments.security.spring.UserServiceConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({CustomerServiceConfiguration.class
        , CardPaymentServiceConfiguration.class
        , InboundPaymentQueryServiceConfiguration.class
        , PaymentQueryServiceConfiguration.class
        , UserServiceConfiguration.class
        , ReportServiceConfiguration.class
        , PaymentProcessorServiceConfiguration.class
        , PaymentFileServiceConfiguration.class
        , SwaggerConfiguration.class})
public class ApiConfiguration {
}
