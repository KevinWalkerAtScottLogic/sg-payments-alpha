package gov.scot.payments.customer;

import gov.scot.payments.model.customer.event.CustomerCreationEvent;
import gov.scot.payments.model.customer.event.CustomerDeletionEvent;
import gov.scot.payments.model.customer.event.CustomerUpdateEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;

import javax.transaction.Transactional;

@Slf4j
public class CustomerPersistenceProcessor {

    public final CustomerRepository repository;

    public CustomerPersistenceProcessor(CustomerRepository repository){
        this.repository = repository;
    }

    //handler for upload queue
    @StreamListener
    @Transactional
    public void onCustomerUpdates(@Input("persist-customer-created-events") KStream<?,CustomerCreationEvent> serviceCreatedStream
            , @Input("persist-customer-updated-events") KStream<?,CustomerUpdateEvent> serviceUpdatedStream
            , @Input("persist-customer-deleted-events") KStream<?,CustomerDeletionEvent>  serviceDeleteStream){

        serviceCreatedStream.foreach( (k,v) -> {
            log.info("Creating customer {}", v.getPayload());
            repository.save(v.getPayload());
        });
        serviceUpdatedStream.foreach( (k,v) -> {
            log.info("Updating customer {}",v.getPayload());
            repository.save(v.getPayload());
        });
        serviceDeleteStream.foreach( (k,v) -> {
            log.info("Deleting customer {}",v.getPayload());
            repository.deleteById(v.getPayload());
        });
    }

}
