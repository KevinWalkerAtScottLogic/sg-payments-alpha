package gov.scot.payments.router.spring;

import gov.scot.payments.model.paymentfile.event.PaymentRoutingFailedEvent;
import gov.scot.payments.model.paymentfile.event.PaymentValidationSuccessEvent;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;

public interface PaymentRouterBinding {

    @Input("route-payment-validation-success-events")
    KStream<?, PaymentValidationSuccessEvent> routePaymentEvents();

    @Output("payment-route-failed-events")
    KStream<?, PaymentRoutingFailedEvent> routePaymentFailedEvents();

}
