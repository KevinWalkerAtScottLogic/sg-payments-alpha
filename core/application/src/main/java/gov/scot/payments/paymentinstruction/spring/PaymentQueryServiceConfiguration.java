package gov.scot.payments.paymentinstruction.spring;

import gov.scot.payments.customer.CustomerServiceService;
import gov.scot.payments.customer.ServiceRepository;
import gov.scot.payments.paymentinstruction.NonTemporalPaymentRepository;
import gov.scot.payments.paymentinstruction.PaymentPersistenceProcessor;
import gov.scot.payments.paymentinstruction.PaymentQueryService;
import gov.scot.payments.paymentinstruction.TemporalPaymentRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.MessageChannel;
import org.springframework.transaction.support.TransactionOperations;

@Configuration
public class PaymentQueryServiceConfiguration {

    @Bean
    public PaymentQueryService paymentQueryService(TemporalPaymentRepository temporalPaymentRepository
            , NonTemporalPaymentRepository nonTemporalPaymentRepository
            , ServiceRepository customerServiceService
            , @Qualifier("manual-payment-created-events") MessageChannel paymentCreatedEvents){
        return new PaymentQueryService(temporalPaymentRepository
                ,nonTemporalPaymentRepository
                ,paymentCreatedEvents
                , customerServiceService::findById);
    }
}
