package gov.scot.payments.notification;

import gov.scot.payments.model.inboundpayment.event.InboundPaymentAttemptFailedEvent;
import gov.scot.payments.model.inboundpayment.event.InboundPaymentFailedToClearEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentFailedToClearEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentRejectedEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentSubmitFailedEvent;
import gov.scot.payments.model.paymentfile.event.FileTranslationFailedEvent;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingFailedEvent;
import gov.scot.payments.model.paymentfile.event.PaymentValidationFailureEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;

@Slf4j
public class NotificationProcessor {

    private final Notifier notifier;

    public NotificationProcessor(Notifier notifier) {
        this.notifier = notifier;
    }

    @StreamListener
    public void notifyFailedFiles(
            @Input("notify-file-translation-failed-events") KStream<?, FileTranslationFailedEvent> failedFiles
            , @Input("notify-payment-validation-failed-events") KStream<?, PaymentValidationFailureEvent> failedValidations
            , @Input("notify-payment-routing-failed-events") KStream<?, PaymentRoutingFailedEvent> failedRoutingEvents
            , @Input("notify-payment-submission-failed-events")KStream<?, PaymentSubmitFailedEvent> failedSubmissionEvents
            , @Input("notify-payment-rejection-events") KStream<?, PaymentRejectedEvent> rejectedEvents
            , @Input("notify-payment-failedToClear-events") KStream<?, PaymentFailedToClearEvent> failedToClearEvents
            , @Input("notify-inbound-payment-failedToClear-events") KStream<?, InboundPaymentFailedToClearEvent> inboundFailedToClearEvents
            , @Input("notify-inbound-payment-attemptFailed-events") KStream<?, InboundPaymentAttemptFailedEvent> inboundAttemptFailedEvents
    ) {
        failedFiles.foreach((k,v) -> notifier.notify(v));
        failedValidations.foreach((k,v) -> notifier.notify(v));
        failedRoutingEvents.foreach((k,v) -> notifier.notify(v));
        failedSubmissionEvents.foreach((k,v) -> notifier.notify(v));
        rejectedEvents.foreach((k,v) -> notifier.notify(v));
        failedToClearEvents.foreach((k,v) -> notifier.notify(v));
        inboundFailedToClearEvents.foreach((k,v) -> notifier.notify(v));
        inboundAttemptFailedEvents.foreach((k,v) -> notifier.notify(v));
    }

}
