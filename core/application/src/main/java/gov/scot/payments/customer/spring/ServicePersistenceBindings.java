package gov.scot.payments.customer.spring;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.MessageChannel;

public interface ServicePersistenceBindings {

    @Input("persist-service-updated-events")
    MessageChannel serviceUpdatedEvents();

}
