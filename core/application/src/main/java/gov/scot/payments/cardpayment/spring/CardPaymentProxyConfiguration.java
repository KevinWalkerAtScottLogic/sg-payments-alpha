package gov.scot.payments.cardpayment.spring;

import gov.scot.payments.cardpayment.CardPaymentGatewayProxy;
import gov.scot.payments.cardpayment.DirectCardPaymentGatewayProxy;
import gov.scot.payments.cardpayment.RemoteLookupPaymentGatewayProxy;
import gov.scot.payments.common.gateway.inbound.cardpayment.CardPaymentGateway;
import gov.scot.payments.paymentprocessor.PaymentProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestOperations;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriBuilderFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Configuration
public class CardPaymentProxyConfiguration {

    @Bean
    CardPaymentGatewayProxy cardPaymentGatewayProxy(@Value("${payments.inbound.card.proxyMode:properties}") String proxyMode
            , @Autowired(required = false) Collection<CardPaymentGateway> cardPaymentGateways
            , PaymentProcessorService paymentProcessorService
            , Supplier<RestOperations> serviceToServiceRestOperations
            , @Value("${cardPayment.proxy.baseUrl}") String baseUrl){
        Collection<CardPaymentGateway> gateways = cardPaymentGateways == null ? Collections.emptyList() : cardPaymentGateways;
        UriBuilderFactory uriBuilderFactory = new DefaultUriBuilderFactory();
        switch(proxyMode){
            case "properties":
                return new RemoteLookupPaymentGatewayProxy(n -> Optional.of(uriBuilderFactory.uriString(baseUrl).build(Map.of("name",n))),serviceToServiceRestOperations,uriBuilderFactory);
            case "zookeeper":
                return new RemoteLookupPaymentGatewayProxy(n -> paymentProcessorService.getGateway(n).map(u -> u.resolve(n)),serviceToServiceRestOperations,uriBuilderFactory);
            case "direct":
                return new DirectCardPaymentGatewayProxy(gateways.stream().collect(Collectors.toMap(CardPaymentGateway::getName, v -> v)));
            default:
                throw new IllegalArgumentException("card payment gateway proxy mode not recognized: "+proxyMode);
        }
    }
}
