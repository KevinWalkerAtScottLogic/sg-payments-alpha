package gov.scot.payments.notification.spring;

import gov.scot.payments.notification.LoggingNotifier;
import gov.scot.payments.notification.NotificationProcessor;
import gov.scot.payments.notification.Notifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NotificationConfiguration {

    @Bean
    public Notifier notifier(){
        return new LoggingNotifier();
    }

    @Bean
    public NotificationProcessor notificationProcessor(Notifier notifier){
        return new NotificationProcessor(notifier);
    }

}
