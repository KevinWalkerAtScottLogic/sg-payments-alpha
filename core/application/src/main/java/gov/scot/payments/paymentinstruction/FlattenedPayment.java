package gov.scot.payments.paymentinstruction;

import gov.scot.payments.model.core.Clearing;
import gov.scot.payments.model.core.Settlement;
import gov.scot.payments.model.paymentinstruction.*;
import lombok.*;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;
import org.javamoney.moneta.Money;

import javax.money.CurrencyUnit;
import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static gov.scot.payments.common.Functions.applyNullCoalescing;

@Entity
@Table(name = "PAYMENT")
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode(of = "id")
public class FlattenedPayment {

    @NonNull private LocalDateTime createdAt;
    @NonNull @Id private UUID id;
    @NonNull @Enumerated(EnumType.STRING) private PaymentInstructionStatus status;
    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "sortCode",column = @Column(name = "recipient_sort_code"))
            , @AttributeOverride(name = "accountNumber",column = @Column(name = "recipient_account_number"))
            , @AttributeOverride(name = "address",column = @Column(name = "recipient_address"))
            , @AttributeOverride(name = "bic",column = @Column(name = "recipient_bic"))
            , @AttributeOverride(name = "email",column = @Column(name = "recipient_email"))
            , @AttributeOverride(name = "iban",column = @Column(name = "recipient_iban"))
            , @AttributeOverride(name = "mobile",column = @Column(name = "recipient_mobile"))
            , @AttributeOverride(name = "name",column = @Column(name = "recipient_name"))
            , @AttributeOverride(name = "ref",column = @Column(name = "recipient_ref"))})
    @NonNull private Recipient recipient;
    private String batchId;
    private String statusMessage;
    @NonNull private LocalDate latestTargetSettlementDate;
    private LocalDate earliestTargetSettlementDate;
    @NonNull
    @Type(type = "gov.scot.payments.model.core.MoneyType")
    @Columns(columns = {@Column(name = "target_amount"),@Column(name = "target_amount_currency")})
    private Money targetAmount;
    @Convert(converter = JpaCurrencyUnitConverter.class) @NonNull private CurrencyUnit targetSettlementCurrency;
    private String paymentFile;
    @NonNull @Enumerated(EnumType.STRING) private PaymentChannelType targetChannelType;
    @NonNull private String service;
    @NonNull private String createdBy;

    @AttributeOverrides({@AttributeOverride(name = "message",column = @Column(name = "validation_message"))
            , @AttributeOverride(name = "status",column = @Column(name = "validation_status"))})
    @Embedded private Validation validation;
    @AttributeOverrides({@AttributeOverride(name = "status",column = @Column(name = "routing_status"))
            , @AttributeOverride(name = "statusMessage",column = @Column(name = "routing_message"))
            , @AttributeOverride(name = "processor",column = @Column(name = "routed_via"))
            , @AttributeOverride(name = "channel",column = @Column(name = "routed_to"))
            , @AttributeOverride(name = "queue",column = @Column(name = "routed_via_queue"))})
    @Embedded  private Routing routing;
    @AttributeOverrides({@AttributeOverride(name = "status",column = @Column(name = "execution_status"))
            , @AttributeOverride(name = "statusMessage",column = @Column(name = "execution_message"))
            , @AttributeOverride(name = "processor",column = @Column(name = "executed_via"))
            , @AttributeOverride(name = "channel",column = @Column(name = "executed_on"))})
    @Embedded private Execution execution;
    @AttributeOverrides({@AttributeOverride(name = "status",column = @Column(name = "clearing_status"))
            , @AttributeOverride(name = "statusMessage",column = @Column(name = "clearing_message"))})
    @Embedded private Clearing clearing;
    @Embedded private Settlement settlement;

    public boolean isAfter(FlattenedPayment other) {
        if(getMaxTimestamp().isEqual(other.getMaxTimestamp())){
            return status.compareTo(other.status) > 0;
        } else{
            return getMaxTimestamp().isAfter(other.getMaxTimestamp());
        }
    }

    public LocalDateTime getMaxTimestamp() {
        return Stream.of(createdAt
                ,applyNullCoalescing(this,FlattenedPayment::getValidation,Validation::getValidatedAt)
                ,applyNullCoalescing(this,FlattenedPayment::getRouting,Routing::getRoutedAt)
                ,applyNullCoalescing(this,FlattenedPayment::getExecution,Execution::getExecutedAt)
                ,applyNullCoalescing(this,FlattenedPayment::getClearing,Clearing::getClearedAt)
                ,applyNullCoalescing(this,FlattenedPayment::getSettlement,Settlement::getSettledAt))
                .filter(Objects::nonNull)
                .max(Comparator.naturalOrder())
                .orElse(null);
    }

    public PaymentInstruction toPaymentInstruction() {
        return PaymentInstruction.builder()
                .timestamp(getMaxTimestamp())
                .id(getId())
                .status(getStatus())
                .recipient(getRecipient() == null ? Recipient.builder().build() : getRecipient())
                .batchId(getBatchId())
                .statusMessage(getStatusMessage())
                .latestTargetSettlementDate(getLatestTargetSettlementDate())
                .earliestTargetSettlementDate(getEarliestTargetSettlementDate())
                .targetAmount(getTargetAmount())
                .targetSettlementCurrency(getTargetSettlementCurrency())
                .paymentFile(getPaymentFile())
                .targetChannelType(getTargetChannelType())
                .service(getService())
                .validation(getValidation())
                .routing(getRouting())
                .executions(getExecution() == null ? Collections.emptyList() : Collections.singletonList(getExecution()))
                .clearing(getClearing())
                .settlement(getSettlement())
                .createdBy(getCreatedBy())
                .build();
    }


    public static FlattenedPayment from(PaymentInstruction payment) {
        LocalDateTime createdAt = Stream.concat(
                Stream.of(payment.getTimestamp()
                        ,applyNullCoalescing(payment,PaymentInstruction::getValidation,Validation::getValidatedAt)
                        ,applyNullCoalescing(payment,PaymentInstruction::getRouting,Routing::getRoutedAt)
                        ,applyNullCoalescing(payment,PaymentInstruction::getClearing,Clearing::getClearedAt)
                        ,applyNullCoalescing(payment,PaymentInstruction::getSettlement,Settlement::getSettledAt))
                ,applyNullCoalescing(payment,PaymentInstruction::getExecutions,s -> s.stream().map(Execution::getExecutedAt))
            )
                    .filter(Objects::nonNull)
                    .min(Comparator.naturalOrder())
                    .orElse(null);
        return FlattenedPayment.builder()
                .createdAt(createdAt)
                .id(payment.getId())
                .status(payment.getStatus())
                .recipient(payment.getRecipient())
                .batchId(payment.getBatchId())
                .statusMessage(payment.getStatusMessage())
                .latestTargetSettlementDate(payment.getLatestTargetSettlementDate())
                .earliestTargetSettlementDate(payment.getEarliestTargetSettlementDate())
                .targetAmount(payment.getTargetAmount())
                .targetSettlementCurrency(payment.getTargetSettlementCurrency())
                .paymentFile(payment.getPaymentFile())
                .targetChannelType(payment.getTargetChannelType())
                .service(payment.getService())
                .validation(payment.getValidation())
                .routing(payment.getRouting())
                .execution(payment.getExecutions() == null || payment.getExecutions().isEmpty() ? null : payment.getExecutions().get(payment.getExecutions().size()-1))
                .clearing(payment.getClearing())
                .settlement(payment.getSettlement())
                .createdBy(payment.getCreatedBy())
                .build();
    }

    public void merge(FlattenedPayment other) {
        if(!this.isAfter(other)){
            setStatus(other.status);
            setStatusMessage(other.statusMessage);
            switch (other.status){
                case New:
                    break;
                case Validated:
                case Invalid:
                    setValidation(other.validation);
                    break;
                case Routed:
                case FailedToRoute:
                    setRouting(other.routing);
                    break;
                case Submitted:
                case SubmissionFailed:
                case Accepted:
                case Rejected:
                    setExecution(other.execution);
                    break;
                case Cleared:
                case FailedToClear:
                    setClearing(other.clearing);
                    break;
                case Settled:
                    setSettlement(other.settlement);
                    break;
                default:
                    break;
            }
        }
    }

    public static String[] getCSVHeader(){
        return Stream.of(
                "createdAt",
                "id",
                "status",
                "recipient_sort_code",
                "recipient_account_number",
                "recipient_address",
                "recipient_bic",
                "recipient_email",
                "recipient_iban",
                "recipient_mobile",
                "recipient_name",
                "recipient_ref",
                "batchId",
                "statusMessage",
                "latestTargetSettlementDate",
                "earliestTargetSettlementDate",
                "targetAmount",
                "targetSettlementCurrency",
                "paymentFile",
                "targetChannelType",
                "service",
                "createdBy"
        )
        .map(String::toUpperCase)
        .toArray(String[]::new);
    }

    public String[] toCSVRow(){
        return new String[]{
                createdAt.toString(),
                id.toString(),
                status.toString(),
                recipient.getSortCode() != null ? recipient.getSortCode().getValue() : "",
                recipient.getAccountNumber() != null ? recipient.getAccountNumber().getValue() : "",
                recipient.getAddress() != null ? recipient.getAddress() : "",
                recipient.getBic() != null ? recipient.getBic() : "",
                recipient.getEmail() != null ? recipient.getEmail() : "",
                recipient.getIban() != null ? recipient.getIban() : "",
                recipient.getMobile() != null ? recipient.getMobile().toString() : "",
                recipient.getName() != null ? recipient.getName() : "",
                recipient.getRef() != null ? recipient.getRef() : "",
                batchId != null ? batchId : "",
                statusMessage != null ? statusMessage : "",
                latestTargetSettlementDate != null ? latestTargetSettlementDate.toString() : "",
                earliestTargetSettlementDate != null ? earliestTargetSettlementDate.toString() : "",
                targetAmount != null ? targetAmount.getNumberStripped().toPlainString() : "",
                targetSettlementCurrency != null ? targetSettlementCurrency.getCurrencyCode() : "",
                paymentFile != null ? paymentFile : "",
                targetChannelType != null ? targetChannelType.toString() : "",
                service != null ? service : "",
                createdBy != null ? createdBy : ""
        };
    }
}
