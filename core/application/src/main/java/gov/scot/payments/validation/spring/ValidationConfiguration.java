package gov.scot.payments.validation.spring;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.common.EntityKeyValueStore;
import gov.scot.payments.model.customer.Service;
import gov.scot.payments.validation.PaymentInstructionValidator;
import gov.scot.payments.validation.ValidationProcessor;
import org.springframework.cloud.stream.binder.kafka.streams.InteractiveQueryService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.spel.standard.SpelExpressionParser;

@Configuration
public class ValidationConfiguration {

    @Bean
    public PaymentInstructionValidator validator(){
        SpelExpressionParser parser = new SpelExpressionParser();
        return new PaymentInstructionValidator(parser);
    }

    @Bean
    public EntityKeyValueStore<Service> validationProcessorServiceKVStore(InteractiveQueryService interactiveQueryService, ObjectMapper objectMapper){
        return new EntityKeyValueStore<>(interactiveQueryService, objectMapper, Service.class);
    }

    @Bean
    public ValidationProcessor validationProcessor(PaymentInstructionValidator validator, EntityKeyValueStore<Service> validationProcessorServiceKVStore){
        return new ValidationProcessor(validator, validationProcessorServiceKVStore);
    }

}

