package gov.scot.payments.cardpayment.spring;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface CardPaymentServiceBinding {

    @Output("inbound-payment-received-events")
    MessageChannel inboundPaymentReceivedEvents();

    @Output("inbound-payment-attemptFailed-events")
    MessageChannel inboundPaymentAttemptFailedEvents();
}
