package gov.scot.payments.validation.spring;

import gov.scot.payments.model.customer.Service;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;

public interface ValidationProcessorBinding {

    @Input("validate-payment-created-events")
    KStream<?,?> paymentCreatedInputEvents();

    @Output("payment-validation-success-events")
    KStream<?,?> paymentValidationSuccessEvents();

    @Output("payment-validation-failed-events")
    KStream<?,?> paymentValidationFailedEvents();

    @Input("service-updated-events-validation")
    KTable<String, Service> serviceValidationStore();
}
