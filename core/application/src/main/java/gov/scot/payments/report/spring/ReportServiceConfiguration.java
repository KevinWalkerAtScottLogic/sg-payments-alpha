package gov.scot.payments.report.spring;

import gov.scot.payments.report.AmazonQuickSightClientFactory;
import gov.scot.payments.report.ReportRepository;
import gov.scot.payments.report.ReportService;
import gov.scot.payments.security.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ReportServiceConfiguration {

    @Bean
    public ReportService reportService(ReportRepository reportRepository
            , AmazonQuickSightClientFactory amazonQuickSightClientFactory
            , UserRepository userRepository
            , @Value("${AWS_ACCOUNT_ID:local}") String awsAccountId
            , @Value("${QUICK_SIGHT_ROLE_ARN:default}") String quickSightRoleArn
            , @Value("${QUICK_SIGHT_ROLE_NAME:default}") String quickSightRoleName){
        return new ReportService(reportRepository
                ,amazonQuickSightClientFactory
                ,userRepository
                ,awsAccountId
                ,quickSightRoleArn
                ,quickSightRoleName);
    }
}
