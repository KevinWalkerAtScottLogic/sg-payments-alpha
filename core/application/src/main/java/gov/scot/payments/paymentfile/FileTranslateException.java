package gov.scot.payments.paymentfile;

public class FileTranslateException extends Exception {

    public FileTranslateException(){
        super();
    }
  
    public FileTranslateException(String message) {
        super(message);
    }

    public FileTranslateException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileTranslateException(Throwable cause) {
        super(cause);
    }
}
