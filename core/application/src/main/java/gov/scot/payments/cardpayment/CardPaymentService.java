package gov.scot.payments.cardpayment;

import gov.scot.payments.model.cardpayment.*;
import gov.scot.payments.model.customer.Service;
import gov.scot.payments.model.inboundpayment.InboundPayment;
import gov.scot.payments.model.inboundpayment.PaymentStatus;
import gov.scot.payments.model.inboundpayment.event.InboundPaymentAttemptFailedEvent;
import gov.scot.payments.model.inboundpayment.event.InboundPaymentReceivedEvent;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import gov.scot.payments.model.paymentinstruction.PaymentProcessorChannel;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.model.user.EntityOp;
import gov.scot.payments.router.PaymentRouter;
import lombok.extern.slf4j.Slf4j;
import org.javamoney.moneta.Money;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.util.function.Tuple3;
import springfox.documentation.annotations.ApiIgnore;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;

@Slf4j
@RequestMapping(value = "/cardpayments")
public class CardPaymentService {

    private final CardPaymentRepository cardPaymentRepository;
    private final PaymentRouter router;
    private final Duration paymentTimeout;
    private final CardPaymentGatewayProxy cardPaymentGatewayProxy;
    private final MessageChannel inboundPaymentReceivedEvents;
    private final MessageChannel inboundPaymentAttemptFailedEvents;
    private final Function<String, Optional<Service>> serviceLookup;
    private final String basePath;

    public CardPaymentService(CardPaymentRepository cardPaymentRepository
            , PaymentRouter router
            , CardPaymentGatewayProxy cardPaymentGatewayProxy
            ,MessageChannel inboundPaymentReceivedEvents
            , MessageChannel inboundPaymentAttemptFailedEvents
            , Function<String, Optional<Service>> serviceLookup
            ,Duration paymentTimeout
            ,String basePath) {
        this.cardPaymentRepository = cardPaymentRepository;
        this.router = router;
        this.cardPaymentGatewayProxy = cardPaymentGatewayProxy;
        this.paymentTimeout = paymentTimeout;
        this.inboundPaymentReceivedEvents = inboundPaymentReceivedEvents;
        this.inboundPaymentAttemptFailedEvents = inboundPaymentAttemptFailedEvents;
        this.serviceLookup = serviceLookup;
        this.basePath = basePath;
    }


    @GetMapping("{id}")
    @ResponseBody
    @PreAuthorize("principal.hasAnyAccess('inbound-payments',T(gov.scot.payments.model.user.EntityOp).Read)")
    public Optional<CardPayment> getPayment(@PathVariable("id") UUID id, @ApiIgnore @AuthenticationPrincipal Client currentUser) {
        Optional<CardPayment> cardPayment = cardPaymentRepository.findById(id);
        if(cardPayment.isPresent() && !currentUser.hasAccess("inbound-payments",cardPayment.get().getService(), EntityOp.Read)){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN,"Access Denied");
        }
        return cardPayment;
    }

    @PostMapping
    @ResponseBody
    @PreAuthorize("principal.hasAccess('inbound-payments',#request.service,T(gov.scot.payments.model.user.EntityOp).Write)")
    public CardPayment createPayment(@RequestBody CreateCardPaymentRequest request, @ApiIgnore @AuthenticationPrincipal Client currentUser) {
        serviceLookup.apply(request.getService())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,String.format("No service %s exists",request.getService())));

        List<Tuple3<String, PaymentProcessorChannel, Money>> channels =  router.getValidInboundChannels(PaymentChannel.Card,request.getAmount());
        if(channels.isEmpty()){
            log.warn("No processors available to take card payments");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"No Card Payment Processors registered");
        }

        Tuple3<String, PaymentProcessorChannel, Money> channel = channels.get(0);

        var cardPayment = request.toCardPayment(channel.getT1(),currentUser.getUserName(),channel.getT3());
        log.info("Creating payment: {}",cardPayment);
        return cardPaymentRepository.save(cardPayment);
    }

    @GetMapping("{id}/form")
    public String getPaymentPage(@PathVariable("id") UUID id, Model model){
        Optional<CardPayment> byCardPaymentId = cardPaymentRepository.findById(id);
        var cardPayment = byCardPaymentId.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No card payment with given id"));
        if(cardPayment.getStatus() != CardPaymentStatus.Created){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Card payment is in incorrect state");
        }
        Service service = serviceLookup.apply(cardPayment.getService())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,String.format("No service %s exists",cardPayment.getService())));

        if(isExpired(cardPayment)){
            return getExpiryErrorPageAndHandleFailure(service, cardPayment, model, "Payment error");
        }

        var updated = cardPaymentRepository.save(cardPayment.toBuilder().status(CardPaymentStatus.Started).build());

        model.addAttribute("id", id);
        model.addAttribute("base", basePath);
        model.addAttribute("request", new AuthorizeCardPaymentRequest());
        return "CardPayment";
    }

    @PostMapping("{id}/cancel")
    public String cancelPayment(@PathVariable("id") UUID id, Model model) {
        Optional<CardPayment> byCardPaymentId = cardPaymentRepository.findById(id);
        var cardPayment = byCardPaymentId.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No card payment with given id"));
        if(!Set.of(CardPaymentStatus.Created,CardPaymentStatus.Started,CardPaymentStatus.Submitted).contains(cardPayment.getStatus())){
            log.error("Error cancelling payment {} Card payment is in incorrect state to cancel",cardPayment.getId());
            return getErrorPage(model, cardPayment, "Cancellation error", "An error has occurred cancelling payment");
        }
        Service service = serviceLookup.apply(cardPayment.getService())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,String.format("No service %s exists",cardPayment.getService())));


        if(Set.of(CardPaymentStatus.Created,CardPaymentStatus.Started).contains(cardPayment.getStatus())){
            CardPayment submitted = handleCancelResponse(service, cardPayment, CardPaymentGatewayCancelResponse.builder().build());
            addReturnUrlToModel(model, submitted);
            model.addAttribute("base", basePath);
            return "CardPaymentCancel";
        } else{
            CardPaymentGatewayCancelResponse response;
            try{
                CardPaymentGatewayCancelRequest gatewayRequest = cardPayment.toCancelRequest();
                log.info("Cancelling payment request for {}",cardPayment.getId());
                response = cardPaymentGatewayProxy.cancel(cardPayment.getProcessorId(),gatewayRequest);
                CardPayment submitted = handleCancelResponse(service, cardPayment,response);
                addReturnUrlToModel(model, submitted);
                model.addAttribute("base", basePath);
                return "CardPaymentCancel";
            } catch (Exception e){
                log.error("Error cancelling payment {}",cardPayment.getId(),e);
                var failed = handleFailed(service, cardPayment,CardPaymentStatus.Error,"Internal Error: "+e.getMessage());
                return getErrorPage(model, failed, "Cancellation error", "An error has occurred confirming payment");
            }
        }
    }

    @PostMapping(value = "/{id}/submit")
    public String submitPayment(@PathVariable("id") UUID id, Model model) {
        Optional<CardPayment> byCardPaymentId = cardPaymentRepository.findById(id);
        var cardPayment = byCardPaymentId.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No card payment with given id"));
        if(cardPayment.getStatus() != CardPaymentStatus.Submitted){
            return getErrorPage(model, cardPayment, "Submission error", "Payment in incorrect state to submit");
        }
        Service service = serviceLookup.apply(cardPayment.getService())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,String.format("No service %s exists",cardPayment.getService())));

        if(isExpired(cardPayment)){
            return getExpiryErrorPageAndHandleFailure(service, cardPayment, model, "Submission Error");
        }

        CardPaymentGatewaySubmitResponse response;
        try{
            CardPaymentGatewaySubmitRequest gatewayRequest = cardPayment.toSubmissionRequest();
            log.info("Submitting payment request for {}",cardPayment.getId());
            response = cardPaymentGatewayProxy.submit(cardPayment.getProcessorId(),gatewayRequest);
            CardPayment submitted = handleSubmitResponse(service, cardPayment,response);
            if(submitted.getStatus() == CardPaymentStatus.Success){
                addReturnUrlToModel(model, submitted);
                model.addAttribute("base", basePath);
                return "CardPaymentSuccess";
            } else {
                log.error("Failed to submit payment {}: {}",submitted.getId(),submitted.getStatusMessage());
                return getErrorPage(model, submitted, "Submission error", "An error has occurred submitting payment");
            }

        } catch (Exception e){
            log.error("Error submitting payment {}",cardPayment.getId(),e);
            var failed = handleFailed(service, cardPayment,CardPaymentStatus.Error,"Internal Error: "+e.getMessage());
            return getErrorPage(model, failed, "Submission error", "An error has occurred submitting payment");
        }
    }

    @PostMapping(value = "{id}/authorize")
    public String authorizePayment(@PathVariable("id") UUID id, @ModelAttribute AuthorizeCardPaymentRequest payment, Model model) {
        Optional<CardPayment> byCardPaymentId = cardPaymentRepository.findById(id);
        var cardPayment = byCardPaymentId.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No card payment with given id"));
        if(cardPayment.getStatus() != CardPaymentStatus.Started){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Card payment is not started");
        }
        Service service = serviceLookup.apply(cardPayment.getService())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,String.format("No service %s exists",cardPayment.getService())));


        if(isExpired(cardPayment)){
            return getExpiryErrorPageAndHandleFailure(service, cardPayment, model, "Authorisation error");
        }

        CardPaymentGatewayAuthResponse response;
        try{

            CardPaymentGatewayAuthRequest gatewayRequest = payment.toGatewayRequest(cardPayment);
            log.info("Submitting payment auth request for {}",cardPayment.getId());
            response = cardPaymentGatewayProxy.authorize(cardPayment.getProcessorId(),gatewayRequest);
            CardPayment submitted = handleAuthResponse(service, cardPayment,response);
            if(submitted.getStatus() == CardPaymentStatus.Submitted){
                var amount = submitted.getAmount().getNumberStripped().toPlainString()+" "+ submitted.getAmount().getCurrency().toString();
                model.addAttribute("amount", amount);
                model.addAttribute("service", submitted.getService());
                model.addAttribute("reference", submitted.getReference());
                model.addAttribute("base", basePath);
                return "CardPaymentConfirm";
            } else {
                log.error("Failed to authorize payment {} : {}",submitted.getId(),submitted.getStatusMessage());
                return getConfirmErrorPage(model, submitted);
            }
        } catch (Exception e){
            log.error("Error authorizing payment {}",cardPayment.getId(),e);
            var failed = handleFailed(service, cardPayment,CardPaymentStatus.Error,"Internal Error: "+e.getMessage());
            return getConfirmErrorPage(model, failed);
        }
    }

    private String getErrorPage(Model model, CardPayment cardPayment, String title, String message) {
        addReturnUrlToModel(model, cardPayment);
        model.addAttribute("errorTitle", title);
        model.addAttribute("errorText", message + " (ID "+cardPayment.getId() + ")");
        model.addAttribute("errorDetail", cardPayment.getStatusMessage());
        model.addAttribute("base", basePath);
        return "CardPaymentGenericError";
    }

    private void addReturnUrlToModel(Model model, CardPayment cardPayment) {
        model.addAttribute("service", cardPayment.getService());
        model.addAttribute("serviceUrl", cardPayment.getReturnUrl());
    }

    private String getConfirmErrorPage(Model model, CardPayment cardPayment) {
        addReturnUrlToModel(model, cardPayment);
        model.addAttribute("base", basePath);
        model.addAttribute("errorTitle", "Authorisation error");
        model.addAttribute("errorText", "An error has occurred authorising payment (ID "+ cardPayment.getId()+ ")");
        model.addAttribute("errorDetail", cardPayment.getStatusMessage());
        return "CardPaymentConfirmError";
    }

    @Scheduled(fixedRate = 60000L)
    @Transactional
    public void cleanUpExpiredPayments(){
        LocalDateTime expiry = LocalDateTime.now().minus(paymentTimeout);
        List<CardPayment> expired = this.cardPaymentRepository.findExpired(expiry);
        log.info("Expiring {} payments older than {}",expired.size(),expiry);
        for(CardPayment cardPayment : expired){
            if(cardPayment.getStatus() == CardPaymentStatus.Submitted){
                CardPaymentGatewayCancelRequest gatewayRequest = cardPayment.toCancelRequest();
                log.info("Cancelling expired but authorized payment {}",cardPayment.getId());
                cardPaymentGatewayProxy.cancel(cardPayment.getProcessorId(),gatewayRequest);
            }
            serviceLookup.apply(cardPayment.getService())
                    .map(s -> handleFailed(s,cardPayment,CardPaymentStatus.Expired,"Payment is expired"));
        }
    }

    private String getExpiryErrorPageAndHandleFailure(Service service, CardPayment cardPayment, Model model, String errorTitle) {
        CardPayment failed = handleFailed(service, cardPayment,CardPaymentStatus.Expired,"Payment page is expired");
        log.error("Error submitting payment {}",cardPayment.getId(), "Payment page is expired");
        return getErrorPage(model, failed, errorTitle, "The page for this payment has expired");
    }

    private boolean isExpired(CardPayment cardPayment) {
        return cardPayment.getTimestamp().isBefore(LocalDateTime.now().minus(paymentTimeout));
    }

    //TODO: should be transactional accross db and message channel
    private CardPayment handleFailed(Service service, CardPayment cardPayment, CardPaymentStatus status, String message) {
        CardPayment failed = cardPayment.toBuilder().status(status).statusMessage(message).build();
        InboundPayment payment = failed.toInboundPayment(service);
        failed = cardPaymentRepository.save(failed.toBuilder().paymentRef(payment.getId()).build());
        inboundPaymentAttemptFailedEvents.send(new GenericMessage<>(new InboundPaymentAttemptFailedEvent(payment)));
        return failed;
    }

    //TODO: should be transactional accross db and message channel
    private CardPayment handleSubmitResponse(Service service, CardPayment payment, CardPaymentGatewaySubmitResponse response){
        var cardPayment = payment.withSubmitResponse(response);
        InboundPayment inbound = cardPayment.toInboundPayment(service);
        CardPayment submitted = cardPaymentRepository.save(cardPayment.toBuilder().paymentRef(payment.getId()).build());
        if(inbound.getStatus() == PaymentStatus.New){
            inboundPaymentReceivedEvents.send(new GenericMessage<>(new InboundPaymentReceivedEvent(inbound)));
        } else{
            inboundPaymentAttemptFailedEvents.send(new GenericMessage<>(new InboundPaymentAttemptFailedEvent(inbound)));
        }
        return submitted;
    }

    //TODO: should be transactional accross db and message channel
    private CardPayment handleAuthResponse(Service service, CardPayment payment, CardPaymentGatewayAuthResponse response){
        var cardPayment = payment.withAuthResponse(response);
        if(response.getStatus() != CardPaymentStatus.Submitted){
            InboundPayment inbound = cardPayment.toInboundPayment(service);
            cardPayment = cardPayment.toBuilder().paymentRef(payment.getId()).build();
            inboundPaymentAttemptFailedEvents.send(new GenericMessage<>(new InboundPaymentAttemptFailedEvent(inbound)));
        }
        return cardPaymentRepository.save(cardPayment);
    }

    //TODO: should be transactional accross db and message channel
    private CardPayment handleCancelResponse(Service service, CardPayment payment, CardPaymentGatewayCancelResponse response){
        var cardPayment = payment.withCancelResponse(response);
        InboundPayment inbound = cardPayment.toInboundPayment(service);
        CardPayment submitted = cardPaymentRepository.save(cardPayment.toBuilder().paymentRef(payment.getId()).build());
        if(inbound.getStatus() != PaymentStatus.New){
            inboundPaymentAttemptFailedEvents.send(new GenericMessage<>(new InboundPaymentAttemptFailedEvent(inbound)));
        }
        return submitted;
    }

}
