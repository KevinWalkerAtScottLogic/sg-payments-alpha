package gov.scot.payments.customer;

import gov.scot.payments.model.customer.Customer;
import gov.scot.payments.model.customer.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ServiceRepository extends JpaRepository<Service, String> {

    @Query("select s from Service s where s.id in ?#{principal.getEntityIdsWithAccess('service',T(gov.scot.payments.model.user.EntityOp).Read)}")
    List<Service> findAllWithAcl();

    @Query("select s from Service s where s.customerId = ?1 and s.id in ?#{principal.getEntityIdsWithAccess('service',T(gov.scot.payments.model.user.EntityOp).Read)}")
    List<Service> findByCustomerIdWithAcl(String id);

    @Query("from Service s where s.folder = ?1")
    Optional<Service> findByFolder(String folder);

    @Query("from Service s where s.customerId = ?1")
    List<Service> findByCustomerId(String customerId);


}
