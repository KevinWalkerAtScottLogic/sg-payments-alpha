package gov.scot.payments.paymentfile.spring;

import gov.scot.payments.paymentfile.PaymentFileRepository;
import gov.scot.payments.paymentfile.PaymentFileService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PaymentFileServiceConfiguration {

    @Bean
    public PaymentFileService paymentFileService(PaymentFileRepository paymentFileRepository){
        return new PaymentFileService(paymentFileRepository);
    }

}
