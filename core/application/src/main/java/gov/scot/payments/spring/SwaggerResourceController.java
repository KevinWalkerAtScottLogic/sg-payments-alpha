package gov.scot.payments.spring;

import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;
import springfox.documentation.swagger.web.ApiResourceController;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

@ApiIgnore
@RequestMapping("/resources/swagger-resources")
public class SwaggerResourceController extends ApiResourceController {

    public SwaggerResourceController(SwaggerResourcesProvider swaggerResources) {
        super(swaggerResources);
    }
}
