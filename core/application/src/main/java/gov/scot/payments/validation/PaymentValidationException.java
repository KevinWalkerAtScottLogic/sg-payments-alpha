package gov.scot.payments.validation;

public class PaymentValidationException extends RuntimeException {

    public PaymentValidationException(String message){
        super(message);
    }

}
