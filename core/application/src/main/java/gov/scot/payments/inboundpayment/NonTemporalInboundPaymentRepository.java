package gov.scot.payments.inboundpayment;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface NonTemporalInboundPaymentRepository extends JpaRepository<FlattenedInboundPayment, UUID>, JpaSpecificationExecutor<FlattenedInboundPayment> {

    @Query("select p from FlattenedInboundPayment p where p.id = ?1 order by p.createdAt desc")
    Optional<FlattenedInboundPayment> findByPaymentId(UUID id);

    @Query("select p from FlattenedInboundPayment p where p.id = ?1 and p.service in ?#{principal.getEntityIdsWithAccess('service',T(gov.scot.payments.model.user.EntityOp).Read)} order by p.createdAt desc")
    Optional<FlattenedInboundPayment> findByPaymentIdWithAcl(UUID id);

    @Query("select p from FlattenedInboundPayment p where p.service in ?#{principal.getEntityIdsWithAccess('service',T(gov.scot.payments.model.user.EntityOp).Read)} order by p.createdAt desc")
    Page<FlattenedInboundPayment> findAllWithAcl(Pageable paging);
}
