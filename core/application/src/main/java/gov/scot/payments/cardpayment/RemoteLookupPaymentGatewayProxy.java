package gov.scot.payments.cardpayment;

import gov.scot.payments.model.cardpayment.*;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.web.client.RestOperations;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriBuilderFactory;

import java.net.URI;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

public class RemoteLookupPaymentGatewayProxy implements CardPaymentGatewayProxy {

    private final Function<String, Optional<URI>> lookup;
    private final Supplier<? extends RestOperations> restOperations;
    private final UriBuilderFactory uriBuilderFactory;

    public RemoteLookupPaymentGatewayProxy(Function<String, Optional<URI>> lookup
            , Supplier<? extends RestOperations> restOperations
            , UriBuilderFactory uriBuilderFactory) {
        this.lookup = lookup;
        this.restOperations = restOperations;
        this.uriBuilderFactory = uriBuilderFactory;
    }

    @Override
    public CardPaymentGatewayAuthResponse authorize(String name, CardPaymentGatewayAuthRequest gatewayRequest) {
        URI paymentSubmission = lookupUri(name,"cardPayment","authorize");
        return restOperations.get().postForEntity(paymentSubmission,gatewayRequest, CardPaymentGatewayAuthResponse.class).getBody();
    }

    @Override
    public CardPaymentGatewaySubmitResponse submit(String name, CardPaymentGatewaySubmitRequest gatewayRequest) {
        URI paymentSubmission = lookupUri(name,"cardPayment","submit");
        return restOperations.get().postForEntity(paymentSubmission,gatewayRequest, CardPaymentGatewaySubmitResponse.class).getBody();
    }

    @Override
    public CardPaymentGatewayCancelResponse cancel(String name, CardPaymentGatewayCancelRequest gatewayRequest) {
        URI paymentSubmission = lookupUri(name,"cardPayment","cancel");
        return restOperations.get().postForEntity(paymentSubmission,gatewayRequest, CardPaymentGatewayCancelResponse.class).getBody();
    }

    private URI lookupUri(String name,String... path) {
        Optional<URI> gatewayOpt = lookup.apply(name);
        return gatewayOpt
                .map(uri -> uriBuilderFactory.uriString(uri.toString()).pathSegment(path).build())
                .orElseThrow(() -> new IllegalArgumentException("No gateway named: " + name));
    }


}
