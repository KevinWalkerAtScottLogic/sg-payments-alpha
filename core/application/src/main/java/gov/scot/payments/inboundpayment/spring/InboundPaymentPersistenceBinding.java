package gov.scot.payments.inboundpayment.spring;

import gov.scot.payments.model.inboundpayment.event.*;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;

public interface InboundPaymentPersistenceBinding {

    @Input("persist-inbound-payment-received-events")
    KStream<?, InboundPaymentReceivedEvent> inboundPaymentReceivedEvents();

    @Input("persist-inbound-payment-attempt-failed-events")
    KStream<?, InboundPaymentAttemptFailedEvent> inboundPaymentAttemptFailedEvents();

    @Input("persist-inbound-payment-cleared-events")
    KStream<?, InboundPaymentClearedEvent> inboundPaymentClearedEvents();

    @Input("persist-inbound-payment-failed-to-clear-events")
    KStream<?, InboundPaymentFailedToClearEvent> inboundPaymentFailedToClearEvents();

    @Input("persist-inbound-payment-settled-events")
    KStream<?, InboundPaymentSettledEvent> inboundPaymentSettledEvents();

}
