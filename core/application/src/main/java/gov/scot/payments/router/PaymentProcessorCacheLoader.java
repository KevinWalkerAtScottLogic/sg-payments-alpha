package gov.scot.payments.router;

import com.google.common.cache.CacheLoader;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import gov.scot.payments.paymentprocessor.PaymentProcessorService;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

@Slf4j
public class PaymentProcessorCacheLoader extends CacheLoader<String, List<PaymentProcessor>> {

    private final PaymentProcessorService paymentProcessorService;
    private final boolean liveOnly;

    public PaymentProcessorCacheLoader(PaymentProcessorService paymentProcessorService, boolean liveOnly){
        this.paymentProcessorService = paymentProcessorService;
        this.liveOnly = liveOnly;
    }

    @Override
    public List<PaymentProcessor> load(String key) {
        return getAllProcessors();
    }

    @Override
    public Map<String, List<PaymentProcessor>> loadAll(Iterable<? extends String> keys) {
        return Map.of("processors", getAllProcessors());
    }

    private List<PaymentProcessor> getAllProcessors() {
        log.info("Refreshing payment processor cache {}",liveOnly ? "from service registry" : "from DB");
        List<PaymentProcessor> processors;
        if(liveOnly){
            processors = paymentProcessorService.getAllLiveProcessors();
        } else {
            processors = paymentProcessorService.getAllProcessors();
        }
        log.info("Reloaded payment processor cache, available processors is: {}",processors.size());
        return processors;
    }
}