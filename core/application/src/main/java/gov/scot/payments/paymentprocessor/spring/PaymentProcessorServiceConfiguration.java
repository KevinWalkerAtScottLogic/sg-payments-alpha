package gov.scot.payments.paymentprocessor.spring;

import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.paymentprocessor.PaymentProcessorProxy;
import gov.scot.payments.paymentprocessor.PaymentProcessorService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PaymentProcessorServiceConfiguration {

    @Bean
    public PaymentProcessorService paymentProcessorService(DiscoveryClient discoveryClient
            , PaymentProcessorProxy paymentProcessorProxy
            , PaymentProcessorRepository repository
            , @Value("${payments.environment}") String env){
        return new PaymentProcessorService(discoveryClient,paymentProcessorProxy,env, repository);
    }
}
