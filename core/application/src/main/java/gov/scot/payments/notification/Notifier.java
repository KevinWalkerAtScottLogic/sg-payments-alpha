package gov.scot.payments.notification;

import gov.scot.payments.model.NotificationEvent;

public interface Notifier {

    void notify(NotificationEvent event);

}
