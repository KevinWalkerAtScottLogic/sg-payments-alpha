package gov.scot.payments.report;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.STSAssumeRoleSessionCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.quicksight.AmazonQuickSight;
import com.amazonaws.services.quicksight.AmazonQuickSightClientBuilder;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.model.user.User;
import org.springframework.cloud.aws.core.region.RegionProvider;

public class AmazonQuickSightClientFactory {

    private final RegionProvider regionProvider;
    private final String quickSightRoleArn;
    private final AWSSecurityTokenService stsClient;
    private final AWSCredentialsProvider credentialsProvider;

    public AmazonQuickSightClientFactory(RegionProvider regionProvider
            , String quickSightRoleArn
            , AWSSecurityTokenService stsClient
            , AWSCredentialsProvider credentialsProvider) {
        this.regionProvider = regionProvider;
        this.quickSightRoleArn = quickSightRoleArn;
        this.stsClient = stsClient;
        this.credentialsProvider = credentialsProvider;
    }

    public AmazonQuickSight embedUrlClient(User currentUser) {
        AWSCredentialsProvider credentialsProvider = assumeRoleCredentialsProvider(currentUser);
        return AmazonQuickSightClientBuilder
                .standard()
                .withRegion(regionProvider.getRegion().getName())
                .withCredentials(credentialsProvider)
                .build();
    }

    public AmazonQuickSight listUsersClient() {
        return AmazonQuickSightClientBuilder
                .standard()
                .withRegion(Regions.US_EAST_1)
                .withCredentials(credentialsProvider)
                .build();
    }

    public AmazonQuickSight registerUserClient(User currentUser) {
        AWSCredentialsProvider credentialsProvider = assumeRoleCredentialsProvider(currentUser);
        return AmazonQuickSightClientBuilder
                .standard()
                .withRegion(Regions.US_EAST_1)
                .withCredentials(credentialsProvider)
                .build();
    }

    private AWSCredentialsProvider assumeRoleCredentialsProvider(User currentUser) {
        return new STSAssumeRoleSessionCredentialsProvider
                .Builder(quickSightRoleArn, currentUser.getEmail())
                .withStsClient(stsClient)
                .build();
    }
}
