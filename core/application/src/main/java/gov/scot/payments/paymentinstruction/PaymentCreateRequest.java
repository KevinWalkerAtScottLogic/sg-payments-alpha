package gov.scot.payments.paymentinstruction;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gov.scot.payments.model.core.MoneyDeserializer;
import gov.scot.payments.model.core.MoneySerializer;
import gov.scot.payments.model.customer.Service;
import gov.scot.payments.model.paymentinstruction.*;
import lombok.*;
import org.javamoney.moneta.Money;

import javax.money.CurrencyUnit;
import java.time.LocalDate;

import static gov.scot.payments.model.paymentinstruction.PaymentInstruction.GBP_CURRENCY;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
public class PaymentCreateRequest {

    @JsonDeserialize(using = MoneyDeserializer.class)
    @JsonSerialize(using = MoneySerializer.class)
    @NonNull
    private Money amount;
    @NonNull @Builder.Default private final LocalDate latestPayDate = LocalDate.now();
    private LocalDate earliestPayDate;
    @JsonDeserialize(using = CurrencyUnitDeserializer.class)
    @JsonSerialize(using = CurrencyUnitSerializer.class)
    @NonNull @Builder.Default private final CurrencyUnit payCurrency = GBP_CURRENCY;
    @NonNull @Builder.Default private final PaymentChannelType type = PaymentChannelType.EFT;
    @NonNull private String service;
    private String reference;
    @NonNull private String payee;
    @JsonDeserialize(using = AccountNumber.AccountNumberDeserializer.class)
    @JsonSerialize(using = AccountNumber.AccountNumberSerializer.class)
    @NonNull private AccountNumber accountNumber;

    @JsonDeserialize(using = SortCode.SortCodeDeserializer.class)
    @JsonSerialize(using = SortCode.SortCodeSerializer.class)
    @NonNull private SortCode sortCode;

    public PaymentInstruction toPaymentInstruction(Service service, String userName) {
        Recipient recipient = Recipient.builder()
                .ref(reference)
                .name(payee)
                .sortCode(sortCode)
                .accountNumber(accountNumber)
                .build();
        return PaymentInstruction.builder()
                .recipient(recipient)
                .latestTargetSettlementDate(latestPayDate)
                .earliestTargetSettlementDate(earliestPayDate)
                .targetSettlementCurrency(payCurrency)
                .targetChannelType(type)
                .targetAmount(amount)
                .service(this.service)
                .kvRef(service.getMetadata())
                .createdBy(userName)
                .build();
    }
}
