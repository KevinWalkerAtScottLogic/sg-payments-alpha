package gov.scot.payments.customer;

import gov.scot.payments.model.customer.Service;
import gov.scot.payments.model.customer.event.CustomerCreationEvent;
import gov.scot.payments.model.customer.event.CustomerDeletionEvent;
import gov.scot.payments.model.customer.event.CustomerUpdateEvent;
import gov.scot.payments.model.customer.Customer;
import gov.scot.payments.model.user.EntityOp;
import gov.scot.payments.model.user.Client;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RequestMapping(value = "/customers")
public class CustomerService {

    MessageChannel customerCreatedEventTopic;

    MessageChannel customerUpdatedEventsTopic;

    MessageChannel customerDeletedEventsTopic;

    private final CustomerRepository customerRepository;
    private final ServiceRepository serviceRepository;

    public CustomerService(MessageChannel customerCreatedEventTopic,
                           MessageChannel customerUpdatedEventsTopic,
                           MessageChannel customerDeletedEventsTopic,
                           CustomerRepository customerRepository,
                           ServiceRepository serviceRepository){
        this.customerCreatedEventTopic = customerCreatedEventTopic;
        this.customerUpdatedEventsTopic = customerUpdatedEventsTopic;
        this.customerDeletedEventsTopic = customerDeletedEventsTopic;
        this.customerRepository = customerRepository;
        this.serviceRepository = serviceRepository;
    }

    @RequestMapping(value = "/{customerId}", method = RequestMethod.GET)
    @ResponseBody
    @Transactional(readOnly = true)
    @PreAuthorize("principal.hasAccess('customer',#customerId,T(gov.scot.payments.model.user.EntityOp).Read)")
    public Optional<Customer> getCustomer(@PathVariable("customerId") String customerId, @ApiIgnore @AuthenticationPrincipal Client currentUser) {
        return customerRepository.findById(customerId).map(c -> enrichCustomerWithServices(currentUser, c));
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Transactional(readOnly = true)
    public List<Customer> getAllCustomers(@ApiIgnore @AuthenticationPrincipal Client currentUser) {
        List<Customer> customers = currentUser.hasWildcardAccess("customer", EntityOp.Read) ? customerRepository.findAll() : customerRepository.findAllWithAcl();
        return customers
                .stream()
                .map(c -> enrichCustomerWithServices(currentUser, c))
                .collect(Collectors.toList());
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("principal.hasWildcardAccess('customer',T(gov.scot.payments.model.user.EntityOp).Write)")
    public Customer addCustomer(@RequestBody Customer customer,@ApiIgnore @AuthenticationPrincipal Client currentUser) {
        customerRepository.findById(customer.getId()).ifPresent(p -> {throw new ResponseStatusException(HttpStatus.BAD_REQUEST,String.format("Customer %s already exists",p.getId()));});
        Customer toCreate = customer.toBuilder().createdBy(currentUser.getUserName()).build();
        log.info("Sending customer creation event for {}",toCreate);
        customerCreatedEventTopic.send(new GenericMessage<>(new CustomerCreationEvent(toCreate)));
        return toCreate;
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    @PreAuthorize("principal.hasAccess('customer',#customerId,T(gov.scot.payments.model.user.EntityOp).Write)")
    public Customer updateCustomer(@RequestBody Customer customer,@ApiIgnore @AuthenticationPrincipal Client currentUser) {
        if(!customerRepository.existsById(customer.getId())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,String.format("Customer %s does not exist",customer.getId()));
        }
        Customer toUpdate = customer.toBuilder().createdBy(currentUser.getUserName()).build();
        log.info("Sending customer updated event for {}",toUpdate);
        customerUpdatedEventsTopic.send(new GenericMessage<>(new CustomerUpdateEvent(toUpdate)));
        return toUpdate;
    }

    @RequestMapping(value = "/{customerId}", method = RequestMethod.DELETE)
    @PreAuthorize("principal.hasAccess('customer',#customerId,T(gov.scot.payments.model.user.EntityOp).Write)")
    public void deleteCustomer(@PathVariable String customerId) {
        if(!customerRepository.existsById(customerId)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,String.format("Customer %s does not exist",customerId));
        }
        log.info("Sending customer deletion event: {}",customerId);
        customerDeletedEventsTopic.send(new GenericMessage<>(new CustomerDeletionEvent(customerId)));
    }

    private Customer enrichCustomerWithServices(Client currentUser, Customer c) {
        List<Service> services = currentUser.hasWildcardAccess("service", EntityOp.Read) ?  serviceRepository.findByCustomerId(c.getId()) : serviceRepository.findByCustomerIdWithAcl(c.getId());
        return c.toBuilder()
                .services(services)
                .build();
    }

}
