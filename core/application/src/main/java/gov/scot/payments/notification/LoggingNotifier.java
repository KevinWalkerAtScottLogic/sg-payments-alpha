package gov.scot.payments.notification;

import gov.scot.payments.model.NotificationEvent;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoggingNotifier implements Notifier {

    @Override
    public void notify(NotificationEvent event) {
        log.error("Notification: {}",event);
    }
}
