package gov.scot.payments.paymentfile.ilf;

import gov.scot.payments.model.paymentfile.PaymentFile;
import gov.scot.payments.model.paymentinstruction.*;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import org.apache.commons.csv.CSVRecord;
import org.javamoney.moneta.Money;

import java.math.BigDecimal;
import java.util.Map;

@Value
@Builder(toBuilder = true)
public class ILFBACSPayment {

    @NonNull private final String recipient;
    @NonNull private final String reference;
    @NonNull private final SortCode sortCode;
    @NonNull private final AccountNumber accountNumber;
    @NonNull private final BigDecimal amount;

    private final static int RECIPIENT_INDEX = 0;
    private final static int REF_INDEX = 1;
    private final static int SORT_CODE_INDEX = 2;
    private final static int ACCOUNT_NUM_INDEX = 3;
    private final static int AMOUNT_INDEX = 4;

    private static final Map<Integer, String> COLUMN_INDEX_NAME_MAP =
            Map.of(RECIPIENT_INDEX, "Recipient",
                    REF_INDEX, "Reference",
                    SORT_CODE_INDEX, "Sort Code",
                    ACCOUNT_NUM_INDEX, "Account Number",
                    AMOUNT_INDEX, "Amount");

    private static String getColumnNameFromIndex(int index){
        if(COLUMN_INDEX_NAME_MAP.containsKey(index)){
            return COLUMN_INDEX_NAME_MAP.get(index);
        }
        return "";
    }

    static ILFBACSPayment fromCSVRecord(CSVRecord record) throws InvalidCSVFieldException {

        SortCode sortCode = createSortCodeFromCSV(record);
        AccountNumber accountNumber = createAccountNumberFromCSV(record);
        BigDecimal targetAmount = getTargetAmountFromCSV(record);
        String recipient = validateIndexValueAndGetFromCSV(record, RECIPIENT_INDEX);
        String reference = validateIndexValueAndGetFromCSV(record, REF_INDEX);

        return ILFBACSPayment.builder()
                .recipient(recipient)
                .reference(reference)
                .sortCode(sortCode)
                .accountNumber(accountNumber)
                .amount(targetAmount)
                .build();
    }

    private static void checkFieldPresent(CSVRecord record, int index) throws InvalidCSVFieldException {
        String fieldValue = record.get(index);
        if(fieldValue == null){
            throw new InvalidCSVFieldException(record.getRecordNumber(), getColumnNameFromIndex(index), "Field not found");
        }
    }

    private static AccountNumber createAccountNumberFromCSV(CSVRecord record) throws InvalidCSVFieldException {
        checkFieldPresent(record, ACCOUNT_NUM_INDEX);
        try {
            return AccountNumber.fromString(record.get(ACCOUNT_NUM_INDEX));
        } catch(InvalidPaymentFieldException e){
            throw new InvalidCSVFieldException(record.getRecordNumber(), getColumnNameFromIndex(ACCOUNT_NUM_INDEX), e);
        }
    }

    private static SortCode createSortCodeFromCSV(CSVRecord record) throws InvalidCSVFieldException {
        checkFieldPresent(record, SORT_CODE_INDEX);
        try {
            return SortCode.fromString(record.get(SORT_CODE_INDEX));
        } catch(InvalidPaymentFieldException e){
            throw new InvalidCSVFieldException(record.getRecordNumber(), getColumnNameFromIndex(SORT_CODE_INDEX), e);
        }
    }

    private static String validateIndexValueAndGetFromCSV(CSVRecord record, int index) throws InvalidCSVFieldException {
        checkFieldPresent(record, index);
        return record.get(index);
    }

    private static BigDecimal getTargetAmountFromCSV(CSVRecord record) throws InvalidCSVFieldException{
        checkFieldPresent(record, AMOUNT_INDEX);
        String amountStr = record.get(AMOUNT_INDEX);
        if(!checkNumericAndPositive(amountStr)){
            String errorMessage = "Value "+ amountStr + " not a valid positive number";
            throw new InvalidCSVFieldException(record.getRecordNumber(), getColumnNameFromIndex(AMOUNT_INDEX), errorMessage);
        }
        return new BigDecimal(amountStr);
    }

    private static boolean checkNumericAndPositive(String amountStr) {
        try{
            var bd = new BigDecimal(amountStr);
            if(bd.compareTo(BigDecimal.ZERO) < 0){
                return false;
            }
        } catch (NumberFormatException e){
            return false;
        }
        return true;
    }

    public PaymentInstruction toPaymentInstruction(PaymentFile paymentFile){

        var recipientObject = Recipient.builder()
                .accountNumber(accountNumber)
                .sortCode(sortCode)
                .ref(reference)
                .name(recipient)
                .build();

        Money amountObj = Money.of(amount, PaymentInstruction.GBP_CURRENCY);
        return PaymentInstruction.builder()
                .recipient(recipientObject)
                .targetAmount(amountObj)
                .timestamp(paymentFile.getTimestamp())
                .service(paymentFile.getService())
                .paymentFile(paymentFile.getName())
                .batchId(paymentFile.getName())
                .build();
    }


}
