package gov.scot.payments.inboundpayment.spring;

import gov.scot.payments.inboundpayment.InboundPaymentPersistenceProcessor;
import gov.scot.payments.inboundpayment.InboundPaymentQueryService;
import gov.scot.payments.inboundpayment.NonTemporalInboundPaymentRepository;
import gov.scot.payments.inboundpayment.TemporalInboundPaymentRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.support.TransactionOperations;

@Configuration
public class InboundPaymentQueryServiceConfiguration {

    @Bean
    public InboundPaymentQueryService inboundPaymentQueryService(TemporalInboundPaymentRepository temporalInboundPaymentRepository
            , NonTemporalInboundPaymentRepository nonTemporalInboundPaymentRepository){
        return new InboundPaymentQueryService(temporalInboundPaymentRepository,nonTemporalInboundPaymentRepository);
    }
}
