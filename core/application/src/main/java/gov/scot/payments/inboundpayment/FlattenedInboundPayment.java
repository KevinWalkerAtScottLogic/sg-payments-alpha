package gov.scot.payments.inboundpayment;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import gov.scot.payments.model.core.Clearing;
import gov.scot.payments.model.core.Settlement;
import gov.scot.payments.model.inboundpayment.ChannelType;
import gov.scot.payments.model.inboundpayment.InboundPayment;
import gov.scot.payments.model.inboundpayment.PaymentStatus;
import lombok.*;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.javamoney.moneta.Money;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static gov.scot.payments.common.Functions.applyNullCoalescing;

@Entity
@Table(name = "INBOUND_PAYMENT")
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode(of = "id")
@TypeDef(name = "jsonb",typeClass = JsonBinaryType.class)
public class FlattenedInboundPayment {

    @NonNull private LocalDateTime createdAt;
    @NonNull @Id private UUID id;
    @NonNull
    @Type(type = "gov.scot.payments.model.core.MoneyType")
    @Columns(columns = {@Column(name = "amount"),@Column(name = "amount_currency")})
    private Money amount;
    @NonNull @Enumerated(EnumType.STRING) private ChannelType channelType;
    @NonNull private String channelId;
    private String ref;
    @NonNull @Enumerated(EnumType.STRING) private PaymentStatus status;
    private String statusMessage;
    @NonNull private String service;
    @AttributeOverrides({@AttributeOverride(name = "status",column = @Column(name = "clearing_status"))
            , @AttributeOverride(name = "statusMessage",column = @Column(name = "clearing_message"))})
    @Embedded private Clearing clearing;
    @Embedded private Settlement settlement;
    @NonNull private String createdBy;

    @Type(type = "jsonb")
    private Object details;

    public boolean isAfter(FlattenedInboundPayment other) {
        if(getMaxTimestamp().isEqual(other.getMaxTimestamp())){
            return status.compareTo(other.status) > 0;
        } else{
            return getMaxTimestamp().isAfter(other.getMaxTimestamp());
        }
    }

    public LocalDateTime getMaxTimestamp() {
        return Stream.of(createdAt
                ,applyNullCoalescing(this, FlattenedInboundPayment::getClearing,Clearing::getClearedAt)
                ,applyNullCoalescing(this, FlattenedInboundPayment::getSettlement,Settlement::getSettledAt))
                .filter(Objects::nonNull)
                .max(Comparator.naturalOrder())
                .orElse(null);
    }

    public InboundPayment toPayment() {
        return InboundPayment.builder()
                .timestamp(getMaxTimestamp())
                .id(getId())
                .amount(getAmount())
                .channelType(getChannelType())
                .channelId(getChannelId())
                .ref(getRef())
                .status(getStatus())
                .statusMessage(getStatusMessage())
                .timestamp(getMaxTimestamp())
                .service(getService())
                .clearing(getClearing())
                .settlement(getSettlement())
                .createdBy(getCreatedBy())
                .details(getDetails())
                .build();
    }

    public static FlattenedInboundPayment from(InboundPayment inboundPayment) {
        LocalDateTime createdAt = Stream.of(inboundPayment.getTimestamp()
                        ,applyNullCoalescing(inboundPayment, InboundPayment::getClearing,Clearing::getClearedAt)
                        ,applyNullCoalescing(inboundPayment, InboundPayment::getSettlement,Settlement::getSettledAt))
                    .filter(Objects::nonNull)
                    .min(Comparator.naturalOrder())
                    .orElse(null);
        return FlattenedInboundPayment.builder()
                .createdAt(createdAt)
                .id(inboundPayment.getId())
                .amount(inboundPayment.getAmount())
                .channelType(inboundPayment.getChannelType())
                .channelId(inboundPayment.getChannelId())
                .ref(inboundPayment.getRef())
                .status(inboundPayment.getStatus())
                .statusMessage(inboundPayment.getStatusMessage())
                .service(inboundPayment.getService())
                .clearing(inboundPayment.getClearing())
                .settlement(inboundPayment.getSettlement())
                .createdBy(inboundPayment.getCreatedBy())
                .details(inboundPayment.getDetails())
                .build();
    }

    public void merge(FlattenedInboundPayment other) {
        if(!this.isAfter(other)){
            setStatus(other.status);
            setStatusMessage(other.statusMessage);
            switch (other.status){
                case New:
                    break;
                case Cleared:
                case FailedToClear:
                    setClearing(other.clearing);
                    break;
                case Settled:
                    setSettlement(other.settlement);
                    break;
                default:
                    break;
            }
        }
    }


    public static String[] getCSVHeader(){
        return Stream.of(
                "createdAt",
                "id",
                "status",
                "amount",
                "amountCurrency",
                "ref",
                "service",
                "createdBy"
        ).map(String::toUpperCase).toArray(String[]::new);
    }

    public String[] toCSVRow(){
        return new String[]{
                createdAt.toString(),
                id.toString(),
                status.toString(),
                amount != null ? amount.getNumberStripped().toPlainString() : "",
                amount != null ? amount.getCurrency().toString() : "",
                ref != null ? ref : "",
                service != null ? service : "",
                createdBy != null ? createdBy : ""
        };
    }
}
