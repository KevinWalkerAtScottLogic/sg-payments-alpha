package gov.scot.payments.paymentfile.cps;

import gov.scot.payments.model.paymentfile.PaymentFile;
import gov.scot.payments.paymentfile.FileParseException;
import gov.scot.payments.paymentfile.PaymentFileTranslator;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CpsPaymentFileTranslator implements PaymentFileTranslator {

    private CpsParser parser;

    public CpsPaymentFileTranslator(CpsParser parser) {
        this.parser = parser;
    }

    @Override
    public List<PaymentInstruction> generatePaymentInstructionsFromFile(PaymentFile paymentFile, InputStream paymentFileStream) throws FileParseException {

        var records = parser.readRecords(paymentFileStream);

        return records.clientPaymentRecords.values().stream()
                .map(cps -> cps.toPaymentInstruction(paymentFile, records.controlRecords.get(0)))
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getSupportedFileTypes() {
        return Collections.singletonList("CPS");
    }

}
