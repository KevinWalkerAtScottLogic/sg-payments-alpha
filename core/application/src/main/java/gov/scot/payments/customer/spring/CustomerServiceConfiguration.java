package gov.scot.payments.customer.spring;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.customer.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.MessageChannel;

@Configuration
public class CustomerServiceConfiguration {


    @Bean
    public CustomerService customerService( @Qualifier("customer-created-events") MessageChannel customerCreatedEventTopic,
                                               @Qualifier("customer-updated-events")MessageChannel customerUpdatedEventsTopic,
                                               @Qualifier("customer-deleted-events") MessageChannel customerDeletedEventsTopic,
                                               CustomerRepository customerRepository,
                                               ServiceRepository serviceRepository){
        return new CustomerService(customerCreatedEventTopic, customerUpdatedEventsTopic, customerDeletedEventsTopic, customerRepository,serviceRepository);
    }

    @Bean
    public CustomerServiceService customerServiceService(@Qualifier("service-updated-events") MessageChannel serviceUpdates
            , ServiceRepository serviceRepository){
        return new CustomerServiceService(serviceUpdates, serviceRepository);
    }

}
