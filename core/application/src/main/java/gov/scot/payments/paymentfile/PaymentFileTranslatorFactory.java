package gov.scot.payments.paymentfile;

import java.util.Map;
import java.util.Optional;

public class PaymentFileTranslatorFactory {

    private final Map<String, PaymentFileTranslator> delegates;

    public PaymentFileTranslatorFactory(Map<String, PaymentFileTranslator> delegates){
        this.delegates = delegates;
    }

    public Optional<PaymentFileTranslator> findTranslatorForFileType(String fileType){
        return Optional.ofNullable(delegates.get(fileType));
    }
}
