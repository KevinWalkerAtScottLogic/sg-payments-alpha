package gov.scot.payments.paymentprocessor;

import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.retry.RetryOperations;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.implicit.ImplicitResourceDetails;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import static org.springframework.cloud.zookeeper.support.StatusConstants.STATUS_OUT_OF_SERVICE;
import static org.springframework.cloud.zookeeper.support.StatusConstants.STATUS_UP;

@Slf4j
public class RemotePaymentProcessorProxy implements PaymentProcessorProxy {

    private final Supplier<RestOperations> authenticated;
    private final RetryOperations retryOperations;

    public RemotePaymentProcessorProxy(Supplier<RestOperations> authenticated, RetryOperations retryOperations){
        this.retryOperations = retryOperations;
        this.authenticated = authenticated;
    }

    public String setGatewayStatus(String name, URI uri, boolean enabled) {
        RestOperations restTemplate = getRestTemplate();
        URI endpoint = uri.resolve("/actuator/service-registry");
        String status = enabled ? STATUS_UP : STATUS_OUT_OF_SERVICE;
        String jsonStatus = String.format("{\"status\":\"%s\"}",status);
        log.info("Setting status of: {} at: {} to: {}",name,uri,status);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(jsonStatus, headers);
        retryOperations.execute(c -> restTemplate.postForEntity(endpoint,request,String.class));
        return getStatus(name, uri);
    }

    public String getStatus(String name, URI uri) {
        RestOperations restTemplate = getRestTemplate();
        URI endpoint = uri.resolve("/actuator/service-registry");
        log.info("Getting status of: {} at: {}",name,uri);
        return retryOperations.execute(c -> restTemplate.getForObject(endpoint,String.class));
    }

    public PaymentProcessor updateDetails(URI uri, PaymentProcessor processor) {
        RestOperations restTemplate = getRestTemplate();
        URI endpoint = uri.resolve("/details");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PaymentProcessor> request = new HttpEntity<>(processor, headers);
        log.info("Setting details of: {} at: {} to: {}",processor.getName(),uri,processor);
        retryOperations.execute(c -> {
            restTemplate.put(endpoint, request);
            return null;
        });
        return getDetails(processor.getName(), uri);
    }

    public PaymentProcessor getDetails(String name, URI uri) {
        RestOperations restTemplate = getRestTemplate();
        URI endpoint = uri.resolve("/details");
        log.info("Getting details of: {} at: {}",name,uri);
        return retryOperations.execute(c -> restTemplate.getForObject(endpoint,PaymentProcessor.class));
    }

    private RestOperations getRestTemplate(){
        return authenticated.get();
    }
}
