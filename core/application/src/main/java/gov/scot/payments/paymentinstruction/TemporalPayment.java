package gov.scot.payments.paymentinstruction;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionStatus;
import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
@TypeDef(name = "jsonb",typeClass = JsonBinaryType.class)
public class TemporalPayment {

    @NonNull @Id @Builder.Default private UUID versionId = UUID.randomUUID();
    @NonNull private UUID id;
    @NonNull private LocalDateTime timestamp;
    @NonNull private PaymentInstructionStatus status;
    @NonNull private String service;

    @Type(type = "jsonb")
    @NonNull private PaymentInstruction details;

    public static TemporalPayment from(PaymentInstruction payment) {
        return TemporalPayment.builder()
                .id(payment.getId())
                .status(payment.getStatus())
                .timestamp(payment.getTimestamp())
                .service(payment.getService())
                .details(payment)
                .build();
    }
}
