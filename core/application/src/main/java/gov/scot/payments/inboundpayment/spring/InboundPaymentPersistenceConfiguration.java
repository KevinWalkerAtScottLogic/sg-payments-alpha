package gov.scot.payments.inboundpayment.spring;

import gov.scot.payments.inboundpayment.InboundPaymentPersistenceProcessor;
import gov.scot.payments.inboundpayment.NonTemporalInboundPaymentRepository;
import gov.scot.payments.inboundpayment.TemporalInboundPaymentRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.support.TransactionOperations;

@Configuration
public class InboundPaymentPersistenceConfiguration {

    @Bean
    public InboundPaymentPersistenceProcessor inboundPaymentPersistenceProcessor(TemporalInboundPaymentRepository temporalPaymentRepository
            , NonTemporalInboundPaymentRepository nonTemporalPaymentRepository
            , TransactionOperations transactionTemplate){
        return new InboundPaymentPersistenceProcessor(temporalPaymentRepository,nonTemporalPaymentRepository,transactionTemplate);
    }
}
