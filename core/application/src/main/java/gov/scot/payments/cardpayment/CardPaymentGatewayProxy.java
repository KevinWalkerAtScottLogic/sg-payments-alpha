package gov.scot.payments.cardpayment;

import gov.scot.payments.model.cardpayment.*;

public interface CardPaymentGatewayProxy {

    CardPaymentGatewayAuthResponse authorize(String name, CardPaymentGatewayAuthRequest gatewayRequest);
    CardPaymentGatewaySubmitResponse submit(String name, CardPaymentGatewaySubmitRequest gatewayRequest);
    CardPaymentGatewayCancelResponse cancel(String name, CardPaymentGatewayCancelRequest gatewayRequest);
}
