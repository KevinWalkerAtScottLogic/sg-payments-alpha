package gov.scot.payments.paymentfile.spring;

import com.amazonaws.services.s3.AmazonS3;
import gov.scot.payments.customer.CustomerServiceService;
import gov.scot.payments.customer.ServiceRepository;
import gov.scot.payments.paymentfile.*;
import gov.scot.payments.paymentfile.cps.CpsParser;
import gov.scot.payments.paymentfile.cps.CpsPaymentFileTranslator;
import gov.scot.payments.paymentfile.ilf.ILFFileParser;
import gov.scot.payments.paymentfile.ilf.ILFFileTranslator;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.MessageChannel;

import java.util.Collection;
import java.util.stream.Collectors;

@Configuration
public class PaymentFileConfiguration {

    @Bean
    public ILFFileParser ilfFileParser(){
        return new ILFFileParser();
    }

    @Bean
    public ILFFileTranslator ilfFileTranslator(ILFFileParser ilfFileParser){
        return new ILFFileTranslator(ilfFileParser);
    }

    @Bean
    public CpsParser cpsFileParser(){
        return new CpsParser();
    }


    @Bean
    public CpsPaymentFileTranslator cpsFileTranslator(CpsParser cpsFileParser){
        return new CpsPaymentFileTranslator(cpsFileParser);
    }


    @Bean
    public PaymentFileTranslatorFactory paymentFileTranslatorFactory(Collection<PaymentFileTranslator> translators){
        return new PaymentFileTranslatorFactory(translators.stream().flatMap(t -> t.getSupportedFileTypes().stream().map(ft -> Pair.of(ft,t)))
                .collect(Collectors.toMap(Pair::getLeft, Pair::getRight)));
    }

    @Bean
    public PaymentFilePersistenceProcessor paymentFilePersistenceProcessor(PaymentFileRepository paymentFileRepository){
        return new PaymentFilePersistenceProcessor(paymentFileRepository);
    }

    @Bean
    public PaymentFileProcessor paymentFileProcessor(AmazonS3 s3Client
                                                    , PaymentFileTranslatorFactory paymentFileTranslatorFactory
                                                    , ServiceRepository customerServiceService
                                                    , @Qualifier("file-uploaded-events") MessageChannel fileUploadedEvents){
        return new PaymentFileProcessor(s3Client
                ,paymentFileTranslatorFactory::findTranslatorForFileType
                , customerServiceService::findByFolder
                , customerServiceService::findById
                , fileUploadedEvents);
    }

}
