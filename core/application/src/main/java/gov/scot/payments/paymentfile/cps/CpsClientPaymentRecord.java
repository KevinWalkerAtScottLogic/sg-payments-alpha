package gov.scot.payments.paymentfile.cps;

import gov.scot.payments.model.paymentfile.PaymentFile;
import gov.scot.payments.model.paymentinstruction.*;
import gov.scot.payments.paymentfile.FileParseException;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Value;
import org.javamoney.moneta.Money;

import javax.money.Monetary;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import static gov.scot.payments.paymentfile.cps.CpsClientPaymentRecord.DirectPaymentType.BANK;
import static gov.scot.payments.paymentfile.cps.CpsClientPaymentRecord.DirectPaymentType.BUILDING_SOCIETY;
import static java.time.format.DateTimeFormatter.BASIC_ISO_DATE;

/**
 * Represents a CPS client payment record.
 */
@Builder
@Value
class CpsClientPaymentRecord {

    private static final int ENTITLEMENT_MANAGEMENT_SYSTEM_FIELD = 1;
    private static final int SERVICE_IDENTIFIER_FIELD = 2;
    private static final int EMS_PAYMENT_REF_NO_FIELD = 3;
    private static final int NINO_FIELD = 4;
    private static final int SURNAME_FIELD = 5;
    private static final int COUNTRY_CODE_FIELD = 14;
    private static final int SORT_CODE_FIELD = 15;
    private static final int ACCOUNT_NUMBER_FIELD = 17;
    private static final int ROLL_NUMBER_FIELD = 20;
    private static final int DIRECT_PAYMENT_TYPE_FIELD = 21;
    private static final int NET_AMOUNT_FIELD = 22;
    private static final int ACCOUNT_NAME_FIELD = 23;
    private static final int DWP_BACS_REFERENCE_FIELD = 30;
    private static final int DWP_BANK_ACCOUNT_IDENTIFIER_FIELD = 33;
    private static final int ISSUING_OFFICE_FIELD = 35;
    private static final int METHOD_OF_PAYMENT_FIELD = 36;
    private static final int DUE_DATE_FIELD = 38;
    private static final int CURRENCY_CODE_FIELD = 39;
    private static final int PAYMENT_PERIOD_START_DATE_FIELD = 45;
    private static final int PAYMENT_PERIOD_END_DATE_FIELD = 46;
    private static final int PAYMENT_PRIORITY_FIELD = 47;

    private final long lineNum;
    private final String entitlementManagementSystem;
    private final String serviceIdentifier;
    private final String emsPaymentRefNo;
    private final String nino;
    private final String surname;
    private final String countryCode;
    private final SortCode sortCode;
    private final AccountNumber accountNumber;
    private final RollNumber rollNumber;
    private final DirectPaymentType directPaymentType;
    private final BigDecimal netAmount;
    private final String accountName;
    private final String dwpBacsReference;
    private final String dwpBankAccountIdentifier;
    private final String issuingOffice;
    private final String methodOfPayment;
    private final LocalDate dueDate;
    private final String currencyCode;
    private final LocalDate paymentPeriodStartDate;
    private final LocalDate paymentPeriodEndDate;
    private final String paymentPriority;

    @Default
    private List<CpsComponentRecord> components =  new ArrayList<>();

    /**
     * The valid direct payment types.
     */
    public enum DirectPaymentType {
        BANK,
        BUILDING_SOCIETY()
    }

    /**
     * Parses an array of fields (as strings) representing a CPS client payment record.
     *
     * @param fields the fields to parse into the record.
     * @param lineNum the line number of the line being parsed
     * @return the CPS client payment record matching the fields
     * @throws FileParseException if the record fields were invalid in some way
     */
    static CpsClientPaymentRecord fromFields(String[] fields, long lineNum) throws FileParseException, RecordParseException {

        AccountNumber accountNumber;
        try {
            accountNumber = AccountNumber.fromString(fields[ACCOUNT_NUMBER_FIELD]);
        } catch (InvalidPaymentFieldException cause) {
            throw new RecordParseException(lineNum, "account number", cause);
        }

        RollNumber rollNumber;
        try {
            rollNumber = RollNumber.fromString(fields[ROLL_NUMBER_FIELD]);
        } catch (InvalidPaymentFieldException cause) {
            throw new RecordParseException(lineNum, "roll number", cause);
        }

        SortCode sortCode;
        try {
            sortCode = SortCode.fromString(fields[SORT_CODE_FIELD]);
        } catch (InvalidPaymentFieldException cause) {
            throw new RecordParseException(lineNum, "sort code", cause);
        }

        BigDecimal netAmount;
        try {
            netAmount = new BigDecimal(new BigInteger(fields[NET_AMOUNT_FIELD]), 2);
        } catch (NumberFormatException cause) {
            throw new RecordParseException(lineNum, "net amount", cause);
        }

        DirectPaymentType directPaymentType;

        switch (fields[DIRECT_PAYMENT_TYPE_FIELD]) {
            case "1":
                directPaymentType = BANK;
                break;
            case "3":
                directPaymentType = BUILDING_SOCIETY;
                break;
            default:
                throw new RecordParseException(lineNum, "direct payment type");
        }

        LocalDate dueDate;
        try {
            dueDate = LocalDate.parse(fields[DUE_DATE_FIELD], BASIC_ISO_DATE);
        } catch (DateTimeParseException cause) {
            throw new RecordParseException(lineNum, "due date", cause);
        }

        LocalDate paymentPeriodStartDate;
        try {
            paymentPeriodStartDate = LocalDate.parse(fields[PAYMENT_PERIOD_START_DATE_FIELD], BASIC_ISO_DATE);
        } catch (DateTimeParseException cause) {
            throw new RecordParseException(lineNum, "payment period start date", cause);
        }

        LocalDate paymentPeriodEndDate;
        try {
            paymentPeriodEndDate = LocalDate.parse(fields[PAYMENT_PERIOD_START_DATE_FIELD], BASIC_ISO_DATE);
        } catch (DateTimeParseException cause) {
            throw new RecordParseException(lineNum, "payment period end date", cause);
        }

        var record = CpsClientPaymentRecord
                .builder()
                .lineNum(lineNum)
                .entitlementManagementSystem(fields[ENTITLEMENT_MANAGEMENT_SYSTEM_FIELD])
                .serviceIdentifier(fields[SERVICE_IDENTIFIER_FIELD])
                .emsPaymentRefNo(fields[EMS_PAYMENT_REF_NO_FIELD])
                .nino(fields[NINO_FIELD])
                .surname(fields[SURNAME_FIELD])
                .countryCode(fields[COUNTRY_CODE_FIELD])
                .sortCode(sortCode)
                .accountNumber(accountNumber)
                .rollNumber(rollNumber)
                .directPaymentType(directPaymentType)
                .netAmount(netAmount)
                .accountName(fields[ACCOUNT_NAME_FIELD])
                .dwpBacsReference(fields[DWP_BACS_REFERENCE_FIELD])
                .dwpBankAccountIdentifier(fields[DWP_BANK_ACCOUNT_IDENTIFIER_FIELD])
                .issuingOffice(fields[ISSUING_OFFICE_FIELD])
                .methodOfPayment(fields[METHOD_OF_PAYMENT_FIELD])
                .dueDate(dueDate)
                .currencyCode(fields[CURRENCY_CODE_FIELD])
                .paymentPeriodStartDate(paymentPeriodStartDate)
                .paymentPeriodEndDate(paymentPeriodEndDate)
                .paymentPriority(fields[PAYMENT_PRIORITY_FIELD])
                .build();

        if (record.getEmsPaymentRefNo().isBlank()) {
            throw new FileParseException("Line " + lineNum + ": Missing EMS Payment Reference No");
        }
        if (record.getSurname().isBlank()) {
            throw new FileParseException("Line " + lineNum + ": Missing Surname");
        }
        if (record.getDwpBacsReference().isBlank()) {
            throw new FileParseException("Line " + lineNum + ": Missing DWP BACS Reference");
        }

        // Check the direct payment type against the roll number
        if (record.getDirectPaymentType() == BANK) {
            if (!record.getRollNumber().isEmpty()) {
                throw new FileParseException("Line " + lineNum +  ": Roll number given when direct payment type = 1");
            }
        } else if (record.getDirectPaymentType() == BUILDING_SOCIETY) {
            if (record.getRollNumber().isEmpty()) {
                throw new FileParseException("Line " + lineNum +  ": Missing roll number when direct payment type = 3");
            }
        }

        return record;
    }

    public PaymentInstruction toPaymentInstruction(PaymentFile paymentFile, CpsControlRecord ctl){
        var ref = getDirectPaymentType() == BANK ? getDwpBacsReference() : getRollNumber().getValue();
        var recipient = Recipient.builder()
                .accountNumber(getAccountNumber())
                .sortCode(getSortCode())
                .ref(ref)
                .name(getAccountName())
                .build();

        return PaymentInstruction.builder()
                .recipient(recipient)
                .targetAmount(Money.of(getNetAmount(), PaymentInstruction.GBP_CURRENCY))
                .timestamp(paymentFile.getTimestamp())
                .earliestTargetSettlementDate(getDueDate())
                .latestTargetSettlementDate(getDueDate())
                .targetSettlementCurrency(Monetary.getCurrency(getCurrencyCode().toUpperCase()))
                .paymentFile(paymentFile.getName())
                .batchId(paymentFile.getName())
                .service(paymentFile.getService())
                .addRef("EmsPaymentRefNo", getEmsPaymentRefNo())
                .addRef("Interface Reference", ctl.getInterfaceReference())
                .addRef("Date of file", ctl.getDateOfFile().toString())
                .build();
    }

    /**
     * Checks that the total of the component amounts equals the payment net amount.
     *
     * @throws FileParseException if the component amounts total does not equal the payment net amount.
     */
    void checkComponentAmountTotal() throws FileParseException {

        BigDecimal componentTotal = components.stream()
                .map(CpsComponentRecord::getComponentAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        if (!netAmount.equals(componentTotal)) {
            throw new FileParseException(
                    String.format(
                            "Net amount does not equal total of component amounts for payment %s at line %d (Net Amount=%.2f, Component Total=%.2f)",
                            emsPaymentRefNo,
                            lineNum,
                            netAmount,
                            componentTotal
                    )
            );
        }
    }


}
