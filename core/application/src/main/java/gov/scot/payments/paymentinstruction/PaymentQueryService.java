package gov.scot.payments.paymentinstruction;

import gov.scot.payments.model.customer.Service;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionStatus;
import gov.scot.payments.model.paymentinstruction.event.PaymentInstructionCreatedEvent;
import gov.scot.payments.model.user.EntityOp;
import gov.scot.payments.model.user.Client;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.*;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import static gov.scot.payments.common.rsql.RsqlParser.parseQuery;
import static org.springframework.util.StringUtils.isEmpty;

@RequestMapping(value = "/payments")
@Slf4j
public class PaymentQueryService {

    private final MessageChannel createPaymentChannel;
    private final TemporalPaymentRepository temporalPaymentRepository;
    private final NonTemporalPaymentRepository nonTemporalPaymentRepository;
    private final Function<String, Optional<Service>> serviceLookup;

    public PaymentQueryService(TemporalPaymentRepository temporalPaymentRepository
            , NonTemporalPaymentRepository nonTemporalPaymentRepository
            , MessageChannel createPaymentChannel
            , Function<String, Optional<Service>> serviceLookup){
        this.temporalPaymentRepository = temporalPaymentRepository;
        this.nonTemporalPaymentRepository = nonTemporalPaymentRepository;
        this.createPaymentChannel = createPaymentChannel;
        this.serviceLookup = serviceLookup;
    }

    @GetMapping("/{id}/history")
    @ResponseBody
    public List<PaymentInstruction> getPaymentHistoryById(@PathVariable("id") UUID id, @ApiIgnore @AuthenticationPrincipal Client currentUser){
        List<TemporalPayment> allByPaymentId = currentUser.hasWildcardAccess("service", EntityOp.Read)
                ? temporalPaymentRepository.findAllByPaymentId(id) : temporalPaymentRepository.findAllByPaymentIdWithAcl(id);
        return allByPaymentId
                .stream()
                .map(TemporalPayment::getDetails)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Optional<PaymentInstruction> getPaymentById(@PathVariable("id") UUID id, @ApiIgnore @AuthenticationPrincipal Client currentUser){
        Optional<FlattenedPayment> byPaymentId = currentUser.hasWildcardAccess("service", EntityOp.Read)
                ? nonTemporalPaymentRepository.findByPaymentId(id) : nonTemporalPaymentRepository.findByPaymentIdWithAcl(id);
        return byPaymentId.map(FlattenedPayment::toPaymentInstruction);
    }

    @GetMapping(value="/query", produces ="application/json")
    @ResponseBody
    public PagedResources<Resource<PaymentInstruction>> queryLatestPayments(@RequestParam(value = "expression") String query
            , @PageableDefault(size = 100,sort = "createdAt",direction = Sort.Direction.DESC) Pageable paging
            , PagedResourcesAssembler assembler
            , @ApiIgnore @AuthenticationPrincipal Client currentUser
    ){
        Specification<FlattenedPayment> spec = getPaymentSpecificationFromQuery(query, currentUser);
        Page<FlattenedPayment> payments = nonTemporalPaymentRepository.findAll(spec,paging);
        return assembler.toResource(payments.map(FlattenedPayment::toPaymentInstruction));
    }


    //TODO: could be streaming output
    @GetMapping(value="/query/download", produces ="text/csv")
    @ResponseBody
    public HttpEntity<byte[]> queryLatestPaymentsCSV(@RequestParam(value = "expression") String query,@ApiIgnore @AuthenticationPrincipal Client currentUser) throws IOException {
        Specification<FlattenedPayment> spec = getPaymentSpecificationFromQuery(query, currentUser);
        try(CSVPrinter csv = new CSVPrinter(new StringBuilder(), CSVFormat.DEFAULT.withHeader(FlattenedPayment.getCSVHeader()))){
            nonTemporalPaymentRepository.findAll(spec)
                    .stream()
                    .map(FlattenedPayment::toCSVRow)
                    .forEach( (Object[] p) -> {
                        try {
                            csv.printRecord(p);
                        } catch (IOException e) {
                            throw new UncheckedIOException(e);
                        }
                    });
            byte[] content =  csv.getOut().toString().getBytes();
            HttpHeaders header = new HttpHeaders();
            header.setContentType(MediaType.valueOf("text/csv"));
            header.setContentDisposition(ContentDisposition.builder("attachment").filename("payments.csv").build());
            header.setContentLength(content.length);
            return new HttpEntity<>(content, header);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public PagedResources<Resource<PaymentInstruction>> getAllLatestPayments(@PageableDefault(size = 100,sort = "createdAt",direction = Sort.Direction.DESC) Pageable paging
            , PagedResourcesAssembler assembler
            , @ApiIgnore @AuthenticationPrincipal Client currentUser
    ){
        Page<FlattenedPayment> payments = currentUser.hasWildcardAccess("service", EntityOp.Read)
            ? nonTemporalPaymentRepository.findAll(paging) : nonTemporalPaymentRepository.findAllWithAcl(paging);
        return assembler.toResource(payments.map(FlattenedPayment::toPaymentInstruction));
    }

    @GetMapping("/statuses")
    @ResponseBody
    public List<PaymentInstructionStatus> getPaymentStatuses(){
        return Arrays.asList(PaymentInstructionStatus.values());
    }


    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("principal.hasSpecificAccess('payment',#createRequest.service,T(gov.scot.payments.model.user.EntityOp).Write)")
    public PaymentInstruction createPayment(@RequestBody PaymentCreateRequest createRequest, @ApiIgnore @AuthenticationPrincipal Client currentUser ){
        Service service = serviceLookup.apply(createRequest.getService())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,String.format("No service %s exists",createRequest.getService())));
        PaymentInstruction payment = createRequest.toPaymentInstruction(service, currentUser.getUserName());
        log.info("Creating Manual Payment: {}",payment);
        createPaymentChannel.send(new GenericMessage<>(new PaymentInstructionCreatedEvent(payment)));
        return payment;
    }


    private Specification<FlattenedPayment> getPaymentSpecificationFromQuery(String query, Client currentUser) {
        if(isEmpty(query)){
            return null;
        }
        Specification<FlattenedPayment> spec;
        try {
            spec = parseQuery(query);
        } catch (Exception exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid query format, see: https://github.com/jirutka/rsql-parser for grammar", exception);
        }
        if (!currentUser.hasWildcardAccess("service", EntityOp.Read)) {
            spec = spec.and(Specification.where((root, q, builder) -> root.get("service").in(currentUser.getEntityIdsWithAccess("service", EntityOp.Read))));
        }
        return spec;
    }

}
