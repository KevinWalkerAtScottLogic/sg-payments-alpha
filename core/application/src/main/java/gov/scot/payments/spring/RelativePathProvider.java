package gov.scot.payments.spring;

import springfox.documentation.spring.web.paths.AbstractPathProvider;

import static com.google.common.base.Strings.isNullOrEmpty;

public class RelativePathProvider extends AbstractPathProvider {

    public static final String ROOT = "/";
    private final String basePath;

    public RelativePathProvider(String basePath) {
        this.basePath = basePath;
    }

    @Override
    protected String applicationPath() {
        return isNullOrEmpty(basePath) ? ROOT : basePath;
    }

    @Override
    protected String getDocumentationPath() {
        return ROOT;
    }
}
