package gov.scot.payments.paymentfile.cps;

import lombok.Getter;

@Getter
class RecordParseException extends Exception {

    private long lineNum;
    private String fieldName;

    RecordParseException(long lineNum, String fieldName) {
        this(lineNum, fieldName, null);
    }

    RecordParseException(long lineNum, String fieldName, Throwable cause) {
        super(cause);
        this.lineNum = lineNum;
        this.fieldName = fieldName;
    }
}
