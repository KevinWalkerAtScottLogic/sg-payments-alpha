package gov.scot.payments.validation;

import gov.scot.payments.common.EntityKeyValueStore;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.customer.PaymentValidationRule;
import gov.scot.payments.model.customer.Service;
import gov.scot.payments.model.paymentfile.event.PaymentValidationFailureEvent;
import gov.scot.payments.model.paymentfile.event.PaymentValidationSuccessEvent;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionStatus;
import gov.scot.payments.model.paymentinstruction.Validation;
import gov.scot.payments.model.paymentinstruction.ValidationStatus;
import gov.scot.payments.model.paymentinstruction.event.PaymentInstructionCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Predicate;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.SendTo;

import java.util.List;
import java.util.Optional;

@Slf4j
public class ValidationProcessor {

    private final PaymentInstructionValidator validator;
    private final EntityKeyValueStore<Service> entityKeyValueStore;

    public ValidationProcessor(PaymentInstructionValidator validator, EntityKeyValueStore<Service> entityKeyValueStore ){
        this.validator = validator;
        this.entityKeyValueStore = entityKeyValueStore;
    }

    private Optional<List<PaymentValidationRule>> getCustomerRulesFromServiceId(String serviceId){
        return entityKeyValueStore.get(serviceId).map(Service::getPaymentValidationRules);
    }

    // this is just called at the start of running to populate the store-name
    @StreamListener
    public void dummyValidationStateStoreListener(@Input("service-updated-events-validation") KTable<String, Service> serviceProcessors){
        this.entityKeyValueStore.setStoreName(serviceProcessors.queryableStoreName());
    }

    @StreamListener("validate-payment-created-events")
    @SendTo({"payment-validation-success-events", "payment-validation-failed-events"})
    public KStream<?,?>[] handlePaymentCreatedEvents(KStream<?, PaymentInstructionCreatedEvent> instructionCreatedEvents) {

        Predicate<Object,Event> validationFailed = (k, v) -> PaymentValidationFailureEvent.class.isAssignableFrom(v.getClass());
        Predicate<Object,Event> validationSucceeded = (k,v) -> PaymentValidationSuccessEvent.class.isAssignableFrom(v.getClass());

        return instructionCreatedEvents
                .mapValues((key, value) -> validateInstruction(value))
                .branch(validationSucceeded, validationFailed);
    }

    private Event<PaymentInstruction> validateInstruction(PaymentInstructionCreatedEvent value) {
        PaymentInstruction payment = value.getPayload();
        log.info("Validating payment instruction of: {} from: {} to: {}", payment.getTargetAmount(),payment.getService(),payment.getRecipient().getName());
        var validationRules = getCustomerRulesFromServiceId(payment.getService());
        Event<PaymentInstruction> event;
        if(validationRules.isEmpty()){
            event = new PaymentValidationFailureEvent(createFailedPaymentInstruction(payment,Validation.builder()
                    .status(ValidationStatus.Invalid)
                    .message(String.format("Payment validation failed as could not load rules for service %s",payment.getService()))
                    .build()));
            event = new PaymentValidationSuccessEvent(createValidatedPaymentInstruction(payment,Validation.success()));
            log.warn("could not load validation rules for service: {}",event.getPayload());
        } else {
            var result = validator.validate(payment, validationRules.get());
            if(result.getStatus() == ValidationStatus.Valid){
                event = new PaymentValidationSuccessEvent(createValidatedPaymentInstruction(payment,result));
                log.info("Successfully validated payment: {}",event.getPayload());
            } else{
                event = new PaymentValidationFailureEvent(createFailedPaymentInstruction(payment,result));
                log.info("Payment validation failed: {}",event.getPayload());
            }
        }
        return event;
    }

    private PaymentInstruction createValidatedPaymentInstruction(PaymentInstruction paymentInstruction, Validation validation) {
        return paymentInstruction.toBuilder()
                .status(PaymentInstructionStatus.Validated)
                .validation(validation)
                .timestamp(validation.getValidatedAt())
                .build();
    }

    private PaymentInstruction createFailedPaymentInstruction(PaymentInstruction paymentInstruction, Validation validation) {
        return paymentInstruction.toBuilder()
                .status(PaymentInstructionStatus.Invalid)
                .validation(validation)
                .timestamp(validation.getValidatedAt())
                .build();
    }
}
