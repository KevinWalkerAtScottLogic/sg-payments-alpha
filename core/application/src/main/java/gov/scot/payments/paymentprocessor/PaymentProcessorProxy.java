package gov.scot.payments.paymentprocessor;

import gov.scot.payments.model.paymentinstruction.PaymentProcessor;

import java.net.URI;

public interface PaymentProcessorProxy {

    String setGatewayStatus(String name, URI uri, boolean enabled);
    String getStatus(String name, URI uri);
    PaymentProcessor updateDetails(URI uri, PaymentProcessor processor);
    PaymentProcessor getDetails(String name, URI uri);
}
