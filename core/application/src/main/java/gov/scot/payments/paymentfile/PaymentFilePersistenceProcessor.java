package gov.scot.payments.paymentfile;

import gov.scot.payments.model.paymentfile.event.FileTranslationFailedEvent;
import gov.scot.payments.model.paymentfile.event.FileTranslationSuccessEvent;
import gov.scot.payments.model.paymentfile.event.FileUploadedEvent;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.transaction.annotation.Transactional;

public class PaymentFilePersistenceProcessor {

    private final PaymentFileRepository repository;

    public PaymentFilePersistenceProcessor(PaymentFileRepository repository){
        this.repository = repository;

    }

    //handler for upload queue
    @StreamListener
    @Transactional
    public void onFileUploadEvent(@Input("persist-file-uploaded-events") KStream<?, FileUploadedEvent> fileUploadStream
            , @Input("persist-file-translation-success-events") KStream<?, FileTranslationSuccessEvent> translationSuccessStream
            , @Input("persist-file-translation-failed-events") KStream<?, FileTranslationFailedEvent>  translationFailureStream){
        fileUploadStream.foreach( (k,v) -> repository.save(v.getPayload()));
        translationSuccessStream.foreach( (k,v) -> repository.save(v.getPayload()));
        translationFailureStream.foreach( (k,v) -> repository.save(v.getPayload()));
    }

}
