package gov.scot.payments.paymentfile.spring;

import gov.scot.payments.model.paymentfile.event.FileTranslationFailedEvent;
import gov.scot.payments.model.paymentfile.event.FileTranslationSuccessEvent;
import gov.scot.payments.model.paymentfile.event.FileUploadedEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentInstructionCreatedEvent;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;

public interface PaymentFileProcessorInputBinding {

    @Input("process-file-uploaded-events")
    KStream<?, FileUploadedEvent> processFileUploadedEvents();

    @Output("file-translation-success-events")
    KStream<?, FileTranslationSuccessEvent> fileTranslationSuccessEvents();

    @Output("file-translation-failed-events")
    KStream<?, FileTranslationFailedEvent> fileTranslationFailedEvents();

    @Output("payment-created-events")
    KStream<?, PaymentInstructionCreatedEvent> paymentCreatedEvents();

}
