package gov.scot.payments.cardpayment;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gov.scot.payments.model.cardpayment.CardPayment;
import gov.scot.payments.model.core.MoneyDeserializer;
import gov.scot.payments.model.core.MoneySerializer;
import lombok.*;
import org.javamoney.moneta.Money;

import java.math.BigDecimal;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
public class CreateCardPaymentRequest {

    @JsonDeserialize(using = MoneyDeserializer.class)
    @JsonSerialize(using = MoneySerializer.class)
    @NonNull
    private Money amount;
    private String reference;
    private String returnURL;
    @NonNull private String service;

    public CardPayment toCardPayment(String processor,String user, Money expectedCost) {
        return CardPayment.builder()
                .amount(amount)
                .service(service)
                .reference(reference)
                .returnUrl(returnURL)
                .processorId(processor)
                .createdBy(user)
                .expectedCost(expectedCost)
                .build();
    }
}
