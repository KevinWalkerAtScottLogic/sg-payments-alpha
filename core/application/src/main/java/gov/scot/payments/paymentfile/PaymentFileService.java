package gov.scot.payments.paymentfile;

import gov.scot.payments.model.paymentfile.PaymentFile;
import gov.scot.payments.model.paymentfile.PaymentFileStatus;
import gov.scot.payments.model.user.EntityOp;
import gov.scot.payments.model.user.Client;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@RequestMapping(value = "/paymentFiles",method = RequestMethod.GET)
@Transactional(readOnly = true)
public class PaymentFileService {

    private final PaymentFileRepository repository;

    public PaymentFileService(PaymentFileRepository repository){
        this.repository = repository;
    }

    @GetMapping("/{name}")
    @ResponseBody
    public Optional<PaymentFile> getLatestPaymentFileByName(@PathVariable("name") String name
            , @RequestParam(value = "startsWith",defaultValue = "true") boolean startsWith
            ,@ApiIgnore @AuthenticationPrincipal Client currentUser){
        if(startsWith){
            return currentUser.hasWildcardAccess("service", EntityOp.Read) ? repository.findLatestByNameStartsWith(name) : repository.findLatestByNameStartsWithWithAcl(name);
        } else{
            return currentUser.hasWildcardAccess("service", EntityOp.Read) ? repository.findLatestByName(name) : repository.findLatestByNameWithAcl(name);
        }
    }

    @GetMapping("/{name}/history")
    @ResponseBody
    public List<PaymentFile> getPaymentFileHistoryByName(@PathVariable("name") String name
            , @RequestParam(value = "startsWith",defaultValue = "true") boolean startsWith
            , @ApiIgnore @AuthenticationPrincipal Client currentUser){
        if(startsWith){
            return currentUser.hasWildcardAccess("service", EntityOp.Read) ? repository.findAllByNameStartsWith(name) : repository.findAllByNameStartsWithWithAcl(name);
        } else{
            return currentUser.hasWildcardAccess("service", EntityOp.Read) ? repository.findAllByName(name) : repository.findAllByNameWithAcl(name);
        }
    }

    @GetMapping
    @ResponseBody
    public List<PaymentFile> queryLatestPaymentFiles(@RequestParam(value = "service", required = false) String serviceName
            , @RequestParam(value = "from", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime from
            , @RequestParam(value = "to", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to
            , @RequestParam(value = "status", required = false) PaymentFileStatus status
            , @ApiIgnore @AuthenticationPrincipal Client currentUser){
        LocalDateTime fromToUse = from == null ? LocalDateTime.of(LocalDate.EPOCH, LocalTime.MIN) : from;
        LocalDateTime toToUse = to == null ? LocalDateTime.now() : to;
        boolean wildcardAccess = currentUser.hasWildcardAccess("service", EntityOp.Read);
        List<PaymentFile> files;
        if(serviceName == null && status == null){
            files = wildcardAccess ? repository.findAllByTimeRange(fromToUse,toToUse) : repository.findAllByTimeRangeWithAcl(fromToUse,toToUse);
        } else if(serviceName != null && status == null){
            files = wildcardAccess ? repository.findAllByTimeRangeAndService(fromToUse,toToUse,serviceName) : repository.findAllByTimeRangeAndServiceWithAcl(fromToUse,toToUse,serviceName);
        } else if(serviceName == null){
            files = wildcardAccess ? repository.findAllByTimeRangeAndStatus(fromToUse,toToUse,status.name()) : repository.findAllByTimeRangeAndStatusWithAcl(fromToUse,toToUse,status.name());
        } else{
            files = wildcardAccess ? repository.findAllByTimeRangeServiceAndStatus(fromToUse,toToUse,serviceName,status.name()) : repository.findAllByTimeRangeServiceAndStatusWithAcl(fromToUse,toToUse,serviceName,status.name());
        }
        return files;
    }

}
