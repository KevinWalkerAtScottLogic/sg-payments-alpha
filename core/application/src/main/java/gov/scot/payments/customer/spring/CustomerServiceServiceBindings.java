package gov.scot.payments.customer.spring;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface CustomerServiceServiceBindings {

    @Output("service-updated-events")
    MessageChannel serviceUpdatedEvents();


}
