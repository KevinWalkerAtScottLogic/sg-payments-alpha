package gov.scot.payments.inboundpayment;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import gov.scot.payments.model.inboundpayment.InboundPayment;
import gov.scot.payments.model.inboundpayment.PaymentStatus;
import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
@TypeDef(name = "jsonb",typeClass = JsonBinaryType.class)
public class TemporalInboundPayment {

    @NonNull @Id @Builder.Default private UUID versionId = UUID.randomUUID();
    @NonNull private UUID id;
    @NonNull private LocalDateTime timestamp;
    @NonNull private PaymentStatus status;
    @NonNull private String channelId;
    @NonNull private String service;

    @Type(type = "jsonb")
    @NonNull private InboundPayment details;

    public static TemporalInboundPayment from(InboundPayment inboundPayment) {
        return TemporalInboundPayment.builder()
                .id(inboundPayment.getId())
                .status(inboundPayment.getStatus())
                .timestamp(inboundPayment.getTimestamp())
                .channelId(inboundPayment.getChannelId())
                .service(inboundPayment.getService())
                .details(inboundPayment)
                .build();
    }
}
