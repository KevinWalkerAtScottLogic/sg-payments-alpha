package gov.scot.payments.paymentinstruction;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface NonTemporalPaymentRepository extends JpaRepository<FlattenedPayment, UUID>, JpaSpecificationExecutor<FlattenedPayment> {

    @Query("select p from FlattenedPayment p where p.id = ?1 order by p.createdAt desc")
    Optional<FlattenedPayment> findByPaymentId(UUID id);

    @Query("select p from FlattenedPayment p where p.id = ?1 and p.service in ?#{principal.getEntityIdsWithAccess('service',T(gov.scot.payments.model.user.EntityOp).Read)} order by p.createdAt desc")
    Optional<FlattenedPayment> findByPaymentIdWithAcl(UUID id);


    @Query("select p from FlattenedPayment p where p.service in ?#{principal.getEntityIdsWithAccess('service',T(gov.scot.payments.model.user.EntityOp).Read)} order by p.createdAt desc")
    Page<FlattenedPayment> findAllWithAcl(Pageable paging);
}
