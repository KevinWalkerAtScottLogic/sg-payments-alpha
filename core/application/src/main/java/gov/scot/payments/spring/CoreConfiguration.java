package gov.scot.payments.spring;

import gov.scot.payments.cardpayment.spring.CardPaymentProxyConfiguration;
import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.common.kafka.KafkaNullConfiguration;
import gov.scot.payments.common.security.CognitoServiceToServiceConfiguration;
import gov.scot.payments.customer.spring.*;
import gov.scot.payments.customer.spring.CustomerPersistenceConfiguration;
import gov.scot.payments.inboundpayment.spring.InboundPaymentPersistenceConfiguration;
import gov.scot.payments.notification.spring.NotificationConfiguration;
import gov.scot.payments.paymentfile.spring.PaymentFileConfiguration;
import gov.scot.payments.paymentinstruction.spring.PaymentPersistenceConfiguration;
import gov.scot.payments.paymentprocessor.spring.PaymentProcessorConfiguration;
import gov.scot.payments.report.spring.ReportConfiguration;
import gov.scot.payments.router.spring.PaymentRoutingConfiguration;
import gov.scot.payments.common.security.CognitoWebSecurityConfiguration;
import gov.scot.payments.common.security.WebSecurityConfiguration;
import gov.scot.payments.validation.spring.ValidationConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import({OptionalResponseControllerAdvice.class
        , KafkaNullConfiguration.class
        , WebSecurityConfiguration.class
        , CognitoWebSecurityConfiguration.class
        , CognitoServiceToServiceConfiguration.class
        , CustomerServiceConfiguration.class
        , ApiConfiguration.class
        , ValidationConfiguration.class
        , NotificationConfiguration.class
        , PaymentRoutingConfiguration.class
        , PaymentFileConfiguration.class
        , PaymentProcessorConfiguration.class
        , ReportConfiguration.class
        , PaymentPersistenceConfiguration.class
        , InboundPaymentPersistenceConfiguration.class
        , CustomerPersistenceConfiguration.class
        , CardPaymentProxyConfiguration.class})
@Configuration
public class CoreConfiguration {

}
