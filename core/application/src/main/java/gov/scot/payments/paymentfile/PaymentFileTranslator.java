package gov.scot.payments.paymentfile;

import gov.scot.payments.model.paymentfile.PaymentFile;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;

import java.io.InputStream;
import java.util.List;

public interface PaymentFileTranslator {

    List<PaymentInstruction> generatePaymentInstructionsFromFile(PaymentFile paymentFile, InputStream paymentFileStream) throws FileParseException, FileTranslateException;

    List<String> getSupportedFileTypes();

}
