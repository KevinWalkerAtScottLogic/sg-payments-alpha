package gov.scot.payments.router.spring;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import gov.scot.payments.paymentprocessor.PaymentProcessorService;
import gov.scot.payments.router.PaymentProcessorCacheLoader;
import gov.scot.payments.router.PaymentRouter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.kafka.core.KafkaOperations;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

@Configuration
public class PaymentRoutingConfiguration {

    @Bean
    public Supplier<Collection<PaymentProcessor>> paymentProcessorRegistry(
            PaymentProcessorService paymentProcessorService
            , @Value("${payments.router.live-only}") boolean liveOnly
            , @Value("${payments.router.cache-expiry-seconds}") int cacheExpirySeconds){

        LoadingCache<String,List<PaymentProcessor>> cache = CacheBuilder.newBuilder()
                .expireAfterWrite(cacheExpirySeconds, TimeUnit.SECONDS)
                .build(new PaymentProcessorCacheLoader(paymentProcessorService, liveOnly));

        return () -> cache.getUnchecked("processors");
    }

    @Bean
    public PaymentRouter paymentRouterService(Supplier<Collection<PaymentProcessor>> paymentProcessorRegistry
            , KafkaOperations<?, ?> kafkaTemplate
            , ObjectMapper objectMapper
            , @Value("${payments.environment}") String environment){
        return new PaymentRouter(paymentProcessorRegistry
                ,(KafkaOperations<byte[], byte[]>)kafkaTemplate
                ,objectMapper
                ,environment
                ,new SpelExpressionParser());
    }

}
