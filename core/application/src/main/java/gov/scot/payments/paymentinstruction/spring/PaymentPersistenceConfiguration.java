package gov.scot.payments.paymentinstruction.spring;

import gov.scot.payments.paymentinstruction.NonTemporalPaymentRepository;
import gov.scot.payments.paymentinstruction.PaymentPersistenceProcessor;
import gov.scot.payments.paymentinstruction.TemporalPaymentRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.support.TransactionOperations;

@Configuration
public class PaymentPersistenceConfiguration {

    @Bean
    public PaymentPersistenceProcessor paymentPersistenceProcessor(TemporalPaymentRepository temporalPaymentRepository
            , NonTemporalPaymentRepository nonTemporalPaymentRepository
            , TransactionOperations transactionTemplate){
        return new PaymentPersistenceProcessor(temporalPaymentRepository,nonTemporalPaymentRepository,transactionTemplate);
    }

}
