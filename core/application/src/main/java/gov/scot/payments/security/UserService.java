package gov.scot.payments.security;

import gov.scot.payments.model.user.Client;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/users")
public class UserService {

    @GetMapping("/current")
    @ResponseBody
    public Client getCurrentUser(){
        return (Client)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
