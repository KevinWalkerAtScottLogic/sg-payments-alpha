package gov.scot.payments.paymentfile;

import gov.scot.payments.model.paymentfile.PaymentFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PaymentFileRepository extends JpaRepository<PaymentFile, UUID> {

    //https://stackoverflow.com/questions/17327043/how-can-i-select-rows-with-most-recent-timestamp-for-each-key-value

    @Query(nativeQuery = true
            , value = "SELECT DISTINCT ON(name) * FROM PAYMENT_FILE WHERE name = ?1 ORDER BY name, timestamp DESC")
    Optional<PaymentFile> findLatestByName(String name);

    @Query(nativeQuery = true
            , value = "SELECT DISTINCT ON (name) * FROM PAYMENT_FILE WHERE name like ?1% ORDER BY name, timestamp DESC")
    Optional<PaymentFile> findLatestByNameStartsWith(String name);

    @Query(nativeQuery = true
            , value = "SELECT DISTINCT ON(name) * FROM PAYMENT_FILE WHERE name = ?1 AND service IN ?#{principal.getEntityIdsWithAccess('service',T(gov.scot.payments.model.user.EntityOp).Read)} ORDER BY name, timestamp DESC")
    Optional<PaymentFile> findLatestByNameWithAcl(String name);

    @Query(nativeQuery = true
            , value = "SELECT DISTINCT ON (name) * FROM PAYMENT_FILE WHERE name like ?1% AND service IN ?#{principal.getEntityIdsWithAccess('service',T(gov.scot.payments.model.user.EntityOp).Read)} ORDER BY name, timestamp DESC")
    Optional<PaymentFile> findLatestByNameStartsWithWithAcl(String name);


    @Query("from PaymentFile p where p.name = ?1 order by p.timestamp desc")
    List<PaymentFile> findAllByName(String name);

    @Query("from PaymentFile p where p.name like ?1% order by p.timestamp desc")
    List<PaymentFile> findAllByNameStartsWith(String name);

    @Query("from PaymentFile p where p.name = ?1 and p.service in ?#{principal.getEntityIdsWithAccess('service',T(gov.scot.payments.model.user.EntityOp).Read)} order by p.timestamp desc")
    List<PaymentFile> findAllByNameWithAcl(String name);

    @Query("from PaymentFile p where p.name like ?1% and p.service in ?#{principal.getEntityIdsWithAccess('service',T(gov.scot.payments.model.user.EntityOp).Read)} order by p.timestamp desc")
    List<PaymentFile> findAllByNameStartsWithWithAcl(String name);


    @Query(nativeQuery = true
            , value = "SELECT DISTINCT ON (name) * FROM PAYMENT_FILE " +
                "WHERE timestamp >= ?1 and timestamp < ?2 " +
                "ORDER BY name, timestamp DESC")
    List<PaymentFile> findAllByTimeRange(LocalDateTime fromToUse, LocalDateTime toToUse);

    @Query(nativeQuery = true
            , value = "SELECT DISTINCT ON (name) * FROM PAYMENT_FILE " +
            "WHERE timestamp >= ?1 and timestamp < ?2 and service = ?3 " +
            "ORDER BY name, timestamp DESC")
    List<PaymentFile> findAllByTimeRangeAndService(LocalDateTime fromToUse, LocalDateTime toToUse, String serviceName);

    @Query(nativeQuery = true
            , value = "SELECT DISTINCT ON (name) * FROM PAYMENT_FILE " +
            "WHERE timestamp >= ?1 and timestamp < ?2 and status = ?3 " +
            "ORDER BY name, timestamp DESC")
    List<PaymentFile> findAllByTimeRangeAndStatus(LocalDateTime fromToUse, LocalDateTime toToUse, String status);

    @Query(nativeQuery = true
            , value = "SELECT DISTINCT ON (name) * FROM PAYMENT_FILE " +
            "WHERE timestamp >= ?1 and timestamp < ?2 and service = ?3 and status = ?4 " +
            "ORDER BY name, timestamp DESC")
    List<PaymentFile> findAllByTimeRangeServiceAndStatus(LocalDateTime fromToUse, LocalDateTime toToUse, String serviceName, String status);

    @Query(nativeQuery = true
            , value = "SELECT DISTINCT ON (name) * FROM PAYMENT_FILE " +
            "WHERE timestamp >= ?1 and timestamp < ?2 " +
            "AND service IN ?#{principal.getEntityIdsWithAccess('service',T(gov.scot.payments.model.user.EntityOp).Read)} " +
            "ORDER BY name, timestamp DESC")
    List<PaymentFile> findAllByTimeRangeWithAcl(LocalDateTime fromToUse, LocalDateTime toToUse);

    @Query(nativeQuery = true
            , value = "SELECT DISTINCT ON (name) * FROM PAYMENT_FILE " +
            "WHERE timestamp >= ?1 and timestamp < ?2 and service = ?3 " +
            "AND service IN ?#{principal.getEntityIdsWithAccess('service',T(gov.scot.payments.model.user.EntityOp).Read)} " +
            "ORDER BY name, timestamp DESC")
    List<PaymentFile> findAllByTimeRangeAndServiceWithAcl(LocalDateTime fromToUse, LocalDateTime toToUse, String serviceName);

    @Query(nativeQuery = true
            , value = "SELECT DISTINCT ON (name) * FROM PAYMENT_FILE " +
            "WHERE timestamp >= ?1 and timestamp < ?2 and status = ?3 " +
            "AND service IN ?#{principal.getEntityIdsWithAccess('service',T(gov.scot.payments.model.user.EntityOp).Read)} " +
            "ORDER BY name, timestamp DESC")
    List<PaymentFile> findAllByTimeRangeAndStatusWithAcl(LocalDateTime fromToUse, LocalDateTime toToUse, String status);

    @Query(nativeQuery = true
            , value = "SELECT DISTINCT ON (name) * FROM PAYMENT_FILE " +
            "WHERE timestamp >= ?1 and timestamp < ?2 and service = ?3 and status = ?4 " +
            "AND service IN ?#{principal.getEntityIdsWithAccess('service',T(gov.scot.payments.model.user.EntityOp).Read)} " +
            "ORDER BY name, timestamp DESC")
    List<PaymentFile> findAllByTimeRangeServiceAndStatusWithAcl(LocalDateTime fromToUse, LocalDateTime toToUse, String serviceName, String status);

}
