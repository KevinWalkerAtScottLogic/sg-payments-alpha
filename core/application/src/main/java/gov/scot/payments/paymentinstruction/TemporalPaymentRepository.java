package gov.scot.payments.paymentinstruction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TemporalPaymentRepository extends JpaRepository<TemporalPayment, UUID> {

    @Query("select p from TemporalPayment p where p.id = ?1 order by p.timestamp desc")
    List<TemporalPayment> findAllByPaymentId(UUID id);

    @Query("select p from TemporalPayment p where p.id = ?1 and p.service in ?#{principal.getEntityIdsWithAccess('service',T(gov.scot.payments.model.user.EntityOp).Read)} order by p.timestamp desc")
    List<TemporalPayment> findAllByPaymentIdWithAcl(UUID id);

}
