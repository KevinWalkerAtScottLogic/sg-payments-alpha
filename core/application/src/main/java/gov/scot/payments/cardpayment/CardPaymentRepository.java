package gov.scot.payments.cardpayment;

import gov.scot.payments.model.cardpayment.CardPayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CardPaymentRepository extends JpaRepository<CardPayment, UUID>, JpaSpecificationExecutor<CardPayment> {

    @Query("select p from CardPayment p where p.status in('Created','Started','Submitted') and p.timestamp < ?1 order by p.timestamp desc")
    List<CardPayment> findExpired(LocalDateTime expiryTime);

}
