package gov.scot.payments.customer.spring;

import gov.scot.payments.model.customer.event.CustomerCreationEvent;
import gov.scot.payments.model.customer.event.CustomerDeletionEvent;
import gov.scot.payments.model.customer.event.CustomerUpdateEvent;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;

public interface CustomerPersistenceBindings {

    @Input("persist-customer-created-events")
    KStream<?, CustomerCreationEvent> customerCreatedEvents();

    @Input("persist-customer-updated-events")
    KStream<?, CustomerUpdateEvent> customerUpdatedEvents();

    @Input("persist-customer-deleted-events")
    KStream<?, CustomerDeletionEvent> customerDeletedEvents();

}
