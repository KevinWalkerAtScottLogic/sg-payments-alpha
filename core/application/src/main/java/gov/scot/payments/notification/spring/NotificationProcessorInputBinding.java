package gov.scot.payments.notification.spring;

import gov.scot.payments.model.inboundpayment.event.InboundPaymentAttemptFailedEvent;
import gov.scot.payments.model.inboundpayment.event.InboundPaymentFailedToClearEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentFailedToClearEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentRejectedEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentSubmitFailedEvent;
import gov.scot.payments.model.paymentfile.event.FileTranslationFailedEvent;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingFailedEvent;
import gov.scot.payments.model.paymentfile.event.PaymentValidationFailureEvent;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;

public interface NotificationProcessorInputBinding {

    @Input("notify-file-translation-failed-events")
    KStream<?, FileTranslationFailedEvent> failedTranslationEvents();

    @Input("notify-payment-validation-failed-events")
    KStream<?, PaymentValidationFailureEvent> failedValidationEvents();

    @Input("notify-payment-routing-failed-events")
    KStream<?, PaymentRoutingFailedEvent> failedRoutingEvents();

    @Input("notify-payment-submission-failed-events")
    KStream<?, PaymentSubmitFailedEvent> failedSubmissionEvents();

    @Input("notify-payment-rejection-events")
    KStream<?, PaymentRejectedEvent> rejectedEvents();

    @Input("notify-payment-failedToClear-events")
    KStream<?, PaymentFailedToClearEvent> failedToClearEvents();

    @Input("notify-inbound-payment-failedToClear-events")
    KStream<?, InboundPaymentFailedToClearEvent> inboundFailedToClearEvents();

    @Input("notify-inbound-payment-attemptFailed-events")
    KStream<?, InboundPaymentAttemptFailedEvent> attemptFailedEvents();
}
