    

  $('#cc').on('input propertychange paste', function() {
    var value = $('#cc').val();
    var formattedValue = formatCardNumber(value);
    $('#cc').val(formattedValue);
  });

  function formatCVC(event) {
    var targetvalue = event.target.value;
    var value = targetvalue.replace(/\D/g, '');
    $('#cvv').val(value);
}

function formatName(event) {
    var targetvalue = event.target.value;
    var formattedValue = targetvalue.replace(/[^\x00-\x7F]/g, "");
    $('#name').val(formattedValue);

}

function formatMonth(event) {
    var targetvalue = event.target.value;
    console.log(targetvalue.match(/(0[1-9])|(1[0-2])/g));
    var formattedValue = targetvalue.match(/(0[1-9])|(1[0-2])/g) ? targetvalue : "";
    console.log(formattedValue)
    $('#month').val(formattedValue);
}

function formatYear(event) {
    var targetvalue = event.target.value;
    var formattedValue = targetvalue.match(/^(1[9])|(2\d))$/g) ? targetvalue : "";
    $('#year').val(formattedValue);
}
  
  function formatCardNumber(event) {
    var targetvalue = event.target.value;
    var value = targetvalue.replace(/\D/g, '');
    var formattedValue;
    var maxLength;
    // american express, 15 digits
    if ((/^3[47]\d{0,13}$/).test(value)) {
      formattedValue = value.replace(/(\d{4})/, '$1 ').replace(/(\d{4}) (\d{6})/, '$1 $2 ');
      maxLength = 17;
    } else if((/^3(?:0[0-5]|[68]\d)\d{0,11}$/).test(value)) { // diner's club, 14 digits
      formattedValue = value.replace(/(\d{4})/, '$1 ').replace(/(\d{4}) (\d{6})/, '$1 $2 ');
      maxLength = 16;
    } else if ((/^\d{0,16}$/).test(value)) { // regular cc number, 16 digits
      formattedValue = value.replace(/(\d{4})/, '$1 ').replace(/(\d{4}) (\d{4})/, '$1 $2 ').replace(/(\d{4}) (\d{4}) (\d{4})/, '$1 $2 $3 ');
      maxLength = 19;
    }
    console.log(formattedValue);
    $('#cc').attr('maxlength', maxLength);
    $('#cc').val(formattedValue);
  }