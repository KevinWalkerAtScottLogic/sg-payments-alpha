package gov.scot.payments.customer;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.AbstractKafkaTest;
import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.common.kafka.KafkaNullConfiguration;
import gov.scot.payments.model.Entity;
import gov.scot.payments.customer.spring.ServicePersistenceBindings;
import gov.scot.payments.model.customer.Service;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.aws.autoconfigure.context.ContextCredentialsAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.jdbc.AmazonRdsDatabaseAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.messaging.MessagingAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.KafkaNull;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.test.annotation.DirtiesContext;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@EnableAutoConfiguration(exclude = {MessagingAutoConfiguration.class
        , ContextStackAutoConfiguration.class
        , ContextResourceLoaderAutoConfiguration.class
        , ContextRegionProviderAutoConfiguration.class
        , ContextCredentialsAutoConfiguration.class
        , AmazonRdsDatabaseAutoConfiguration.class
})
@EmbeddedKafka(partitions = 1
        ,topics = {"test-serviceChangedEvents"})
@SpringBootTest(classes = ServicePersistenceIntegrationTest.TestConfiguration.class, properties = {
        "debug=true",
        "payments.environment=test",
        "spring.cloud.stream.bindings.service-updated-events-send.binder=kafka",
        "spring.cloud.stream.bindings.service-updated-events-send.destination=test-serviceChangedEvents"
})
@DirtiesContext
@Tag("kafka")
public class ServicePersistenceIntegrationTest extends AbstractKafkaTest{

    @Autowired
    @Qualifier("service-updated-events-send")
    MessageChannel serviceUpdatedEvent;

    @MockBean
    ServiceRepository repository;

    @Test
    public void testSaveServiceOnEvent() {

        Service service = createServiceObject();

        serviceUpdatedEvent.send(service.createMessage());
        sleep();

        ArgumentCaptor<Service> argumentCaptor = ArgumentCaptor.forClass(Service.class);
        verify(repository, times(1)).save(argumentCaptor.capture());
        assertTrue(argumentCaptor.getValue().equals(service));

    }

    private Service createServiceObject() {
        return Service.builder()
                    .folder("abc")
                    .fileFormat("ILF")
                    .id("service1")
                .createdBy("user")
                    .customerId("customer_1_1")
                    .paymentValidationRules(Collections.emptyList())
                    .build();
    }

    @Test
    public void testDeleteServiceOnEvent() throws Exception {
        //serviceUpdatedEvent.send(createServiceObject().toBuilder().id("ilf_customer").build().createMessage());
        serviceUpdatedEvent.send(new GenericMessage<>(KafkaNull.INSTANCE,Collections.singletonMap(KafkaHeaders.MESSAGE_KEY, Entity.stringToKey("ilf_customer"))));
        sleep();
        verify(repository, times(1)).deleteById("ilf_customer");
    }

    @EnableJpaRepositories(basePackageClasses = ServiceRepository.class)
    @EntityScan(basePackageClasses = Service.class)
    @Import({OptionalResponseControllerAdvice.class, KafkaNullConfiguration.class})
    @EnableBinding({ServicePersistenceBindings.class, ServiceTestBinding.class})
    public static class TestConfiguration extends AbstractKafkaTest.TestConfiguration {

        @Bean
        public ServicePersistenceProcessor getPersistenceProcessor(ServiceRepository serviceRepo, ObjectMapper mapper){
            return new ServicePersistenceProcessor(serviceRepo,mapper);
        }

    }

    public interface ServiceTestBinding {

        @Output("service-updated-events-send")
        MessageChannel serviceUpdatedEventsChannel();

    }




}
