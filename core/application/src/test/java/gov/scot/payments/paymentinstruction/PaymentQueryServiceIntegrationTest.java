package gov.scot.payments.paymentinstruction;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import gov.scot.payments.AbstractKafkaTest;
import gov.scot.payments.MockWebSecurityConfiguration;
import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.common.kafka.KafkaNullConfiguration;
import gov.scot.payments.customer.CustomerServiceService;
import gov.scot.payments.customer.ServiceRepository;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.customer.Service;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingFailedEvent;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingSuccessEvent;
import gov.scot.payments.model.paymentfile.event.PaymentValidationFailureEvent;
import gov.scot.payments.model.paymentfile.event.PaymentValidationSuccessEvent;
import gov.scot.payments.model.paymentinstruction.*;
import gov.scot.payments.model.paymentinstruction.event.*;
import gov.scot.payments.model.user.EntityOp;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.model.user.User;
import gov.scot.payments.paymentinstruction.spring.PaymentCreationBinding;
import gov.scot.payments.paymentinstruction.spring.PaymentPersistenceBinding;
import gov.scot.payments.paymentinstruction.spring.PaymentPersistenceConfiguration;
import gov.scot.payments.paymentinstruction.spring.PaymentQueryServiceConfiguration;
import gov.scot.payments.WithUser;
import org.hamcrest.Matchers;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.aws.autoconfigure.context.ContextCredentialsAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.jdbc.AmazonRdsDatabaseAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.messaging.MessagingAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.test.annotation.DirtiesContext;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;

import static gov.scot.payments.model.paymentinstruction.PaymentInstruction.GBP_CURRENCY;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@EnableAutoConfiguration(exclude = {MessagingAutoConfiguration.class
        , ContextStackAutoConfiguration.class
        , ContextResourceLoaderAutoConfiguration.class
        , ContextRegionProviderAutoConfiguration.class
        , ContextCredentialsAutoConfiguration.class
        , AmazonRdsDatabaseAutoConfiguration.class
        , HypermediaAutoConfiguration.class
})
@EmbeddedKafka(partitions = 1
        ,topics = {"test-bottomlinePaymentEvents"
        ,"test-accesspayPaymentEvents"
        ,"test-paymentSubmitSuccessEvents"
        ,"test-paymentSubmitFailedEvents"
        ,"test-paymentAcceptedEvents"
        ,"test-paymentRejectedEvents"
        ,"test-paymentClearedEvents"
        ,"test-paymentFailedToClearEvents"
        ,"test-paymentSettledEvents"
        ,"test-paymentValidationSuccessEvents"
        ,"test-paymentValidationFailedEvents"
        ,"test-paymentRoutingFailedEvents"
        ,"test-paymentCreatedEvents"})
@SpringBootTest(classes = PaymentQueryServiceIntegrationTest.TestConfiguration.class
        , properties = {"payments.environment=test"
        ,"spring.cloud.stream.bindings.send-bottomline-events.destination=test-bottomlinePaymentEvents"
        ,"spring.cloud.stream.bindings.send-bottomline-events.binder=kafka"
        ,"spring.cloud.stream.bindings.send-accesspay-events.destination=test-accesspayPaymentEvents"
        ,"spring.cloud.stream.bindings.send-accesspay-events.binder=kafka"
        ,"spring.cloud.stream.bindings.send-payment-created-events.destination=test-paymentCreatedEvents"
        ,"spring.cloud.stream.bindings.send-payment-created-events.binder=kafka"
        ,"spring.cloud.stream.bindings.send-payment-validation-success-events.destination=test-paymentValidationSuccessEvents"
        ,"spring.cloud.stream.bindings.send-payment-validation-success-events.binder=kafka"
        ,"spring.cloud.stream.bindings.send-payment-validation-failed-events.destination=test-paymentValidationFailedEvents"
        ,"spring.cloud.stream.bindings.send-payment-validation-failed-events.binder=kafka"
        ,"spring.cloud.stream.bindings.send-payment-routing-failed-events.destination=test-paymentRoutingFailedEvents"
        ,"spring.cloud.stream.bindings.send-payment-routing-failed-events.binder=kafka"
        ,"spring.cloud.stream.bindings.send-payment-submission-success-events.destination=test-paymentSubmitSuccessEvents"
        ,"spring.cloud.stream.bindings.send-payment-submission-success-events.binder=kafka"
        ,"spring.cloud.stream.bindings.send-payment-submission-failed-events.destination=test-paymentSubmitFailedEvents"
        ,"spring.cloud.stream.bindings.send-payment-submission-failed-events.binder=kafka"
        ,"spring.cloud.stream.bindings.send-payment-accepted-events.destination=test-paymentAcceptedEvents"
        ,"spring.cloud.stream.bindings.send-payment-accepted-events.binder=kafka"
        ,"spring.cloud.stream.bindings.send-payment-rejection-events.destination=test-paymentRejectedEvents"
        ,"spring.cloud.stream.bindings.send-payment-rejection-events.binder=kafka"
        ,"spring.cloud.stream.bindings.send-payment-cleared-events.destination=test-paymentClearedEvents"
        ,"spring.cloud.stream.bindings.send-payment-cleared-events.binder=kafka"
        ,"spring.cloud.stream.bindings.send-payment-failedToClear-events.destination=test-paymentFailedToClearEvents"
        ,"spring.cloud.stream.bindings.send-payment-failedToClear-events.binder=kafka"
        ,"spring.cloud.stream.bindings.send-payment-settled-events.destination=test-paymentSettledEvents"
        ,"spring.cloud.stream.bindings.send-payment-settled-events.binder=kafka"
        ,"spring.cloud.stream.bindings.persist-payment-routing-success-events.destination=test-bottomlinePaymentEvents,test-accesspayPaymentEvents"
        ,"spring.cloud.stream.kafka.streams.bindings.persist-payment-routing-success-events.consumer.destination-is-pattern=false"
        ,"h2.serializeJavaObject=false"})
@DBRider
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@Tag("kafka")
@Tag("db")
@WithUser
public class PaymentQueryServiceIntegrationTest extends AbstractKafkaTest {

    static {
        System.setProperty("h2.serializeJavaObject","false");
    }

    @Autowired
    @Qualifier("send-accesspay-events")
    MessageChannel accesspayEvents;

    @Autowired
    @Qualifier("send-bottomline-events")
    MessageChannel bottomlineEvents;

    @Autowired
    @Qualifier("send-payment-created-events")
    MessageChannel paymentCreatedEvents;

    @Autowired
    @Qualifier("send-payment-validation-success-events")
    MessageChannel successfulValidationEvents;

    @Autowired
    @Qualifier("send-payment-validation-failed-events")
    MessageChannel failedValidationEvents;

    @Autowired
    @Qualifier("send-payment-routing-failed-events")
    MessageChannel failedRoutingEvents;

    @Autowired
    @Qualifier("send-payment-submission-success-events")
    MessageChannel successfulSubmissionEvents;

    @Autowired
    @Qualifier("send-payment-submission-failed-events")
    MessageChannel failedSubmissionEvents;

    @Autowired
    @Qualifier("send-payment-accepted-events")
    MessageChannel acceptedEvents;

    @Autowired
    @Qualifier("send-payment-rejection-events")
    MessageChannel rejectedEvents;

    @Autowired
    @Qualifier("send-payment-cleared-events")
    MessageChannel clearedEvents;

    @Autowired
    @Qualifier("send-payment-failedToClear-events")
    MessageChannel failedToClearEvents;

    @Autowired
    @Qualifier("send-payment-settled-events")
    MessageChannel settledEvents;

    @Autowired
    TemporalPaymentRepository temporalPaymentRepository;

    @Autowired
    NonTemporalPaymentRepository nonTemporalPaymentRepository;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    ServiceRepository serviceRepository;

    @Test
    public void testPersistence() throws Exception {
        UUID id = UUID.randomUUID();
        sendPaymentEvent(paymentCreatedEvents, id, LocalDateTime.now(), PaymentInstructionStatus.New, PaymentInstructionCreatedEvent::new);
        verifyPaymentEvents(id, PaymentInstructionStatus.New,1);
        sendPaymentEvent(successfulValidationEvents, id, LocalDateTime.now(), PaymentInstructionStatus.Validated, PaymentValidationSuccessEvent::new);
        verifyPaymentEvents(id, PaymentInstructionStatus.Validated,2);
        sendPaymentEvent(accesspayEvents, id, LocalDateTime.now(), PaymentInstructionStatus.Routed, PaymentRoutingSuccessEvent::new);
        verifyPaymentEvents(id, PaymentInstructionStatus.Routed,3);
        sendPaymentEvent(successfulSubmissionEvents, id, LocalDateTime.now(), PaymentInstructionStatus.Submitted, PaymentSubmitSuccessEvent::new);
        verifyPaymentEvents(id, PaymentInstructionStatus.Submitted,4);
        sendPaymentEvent(acceptedEvents, id, LocalDateTime.now(), PaymentInstructionStatus.Accepted, PaymentAcceptedEvent::new);
        verifyPaymentEvents(id, PaymentInstructionStatus.Accepted,5);
        sendPaymentEvent(clearedEvents, id, LocalDateTime.now(), PaymentInstructionStatus.Cleared, PaymentClearedEvent::new);
        verifyPaymentEvents(id, PaymentInstructionStatus.Cleared,6);
        sendPaymentEvent(settledEvents, id, LocalDateTime.now(), PaymentInstructionStatus.Settled, PaymentSettledEvent::new);
        verifyPaymentEvents(id, PaymentInstructionStatus.Settled,7);
    }

    @Test
    public void testPersistenceOutOfOrder() throws Exception {
        UUID id = UUID.randomUUID();
        sendPaymentEvent(paymentCreatedEvents, id, LocalDateTime.now(), PaymentInstructionStatus.New, PaymentInstructionCreatedEvent::new);
        verifyPaymentEvents(id, PaymentInstructionStatus.New,1);
        sendPaymentEvent(failedValidationEvents, id, LocalDateTime.now(), PaymentInstructionStatus.Invalid, PaymentValidationFailureEvent::new);
        verifyPaymentEvents(id, PaymentInstructionStatus.Invalid,2);
        sendPaymentEvent(failedRoutingEvents, id, LocalDateTime.now(), PaymentInstructionStatus.FailedToRoute, PaymentRoutingFailedEvent::new);
        verifyPaymentEvents(id, PaymentInstructionStatus.FailedToRoute,3);
        sendPaymentEvent(failedSubmissionEvents, id, LocalDateTime.now(), PaymentInstructionStatus.SubmissionFailed, PaymentSubmitFailedEvent::new);
        verifyPaymentEvents(id, PaymentInstructionStatus.SubmissionFailed,4);
        sendPaymentEvent(rejectedEvents, id, LocalDateTime.now(), PaymentInstructionStatus.Rejected, PaymentRejectedEvent::new);
        verifyPaymentEvents(id, PaymentInstructionStatus.Rejected,5);
        sendPaymentEvent(failedToClearEvents, id, LocalDateTime.now().minusMinutes(10), PaymentInstructionStatus.FailedToClear, PaymentFailedToClearEvent::new);
        verifyPaymentEvents(id, PaymentInstructionStatus.Rejected,6);
    }

    @Test
    @DataSet("payments.yml")
    public void testQuery() throws Exception {
        //status (enum)
        mvc.perform(get("/payments/query").param("expression","status==New"))
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andExpect(jsonPath("$.content[0].id", Matchers.is("e50d0699-7b1d-452a-88e9-527e26c0180d")));

        //id (uuid)
        mvc.perform(get("/payments/query").param("expression","id==e50d0699-7b1d-452a-88e9-527e26c0180d"))
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andExpect(jsonPath("$.content[0].id", Matchers.is("e50d0699-7b1d-452a-88e9-527e26c0180d")));

        //currencyunit
        mvc.perform(get("/payments/query").param("expression","targetSettlementCurrency==USD"))
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andExpect(jsonPath("$.content[0].id", Matchers.is("e50d0699-7b1d-452a-88e9-527e26c0180d")));

        //date range
        mvc.perform(get("/payments/query").param("expression","latestTargetSettlementDate>2019-01-01 and latestTargetSettlementDate<=2019-01-03"))
                .andExpect(jsonPath("$.content", hasSize(2)));

        //date time range
        mvc.perform(get("/payments/query").param("expression","createdAt>2019-01-01T00:00:00 and createdAt<=2019-01-01T00:00:03"))
                .andExpect(jsonPath("$.content", hasSize(3)));

        //recipient name (nested, string, wildcard)
        mvc.perform(get("/payments/query").param("expression","recipient.name==*Smith"))
                .andExpect(jsonPath("$.content", hasSize(3)));

        //TODO money queries are not working as we use a UserType to persist them and this does not seem to work with the criteria builder api - possibly use a custom meatamodel?
        //amount (big decimal, nested in money)
        //mvc.perform(get("/payments/query").param("expression","targetAmount.number>11"))
         //       .andExpect(jsonPath("$.content", hasSize(3)));

        //service with acl
        User user = User.builder().userName("").email("").withRole(new Role(EntityOp.Read, "service", "123")).build();
        mvc.perform(get("/payments/query").param("expression","service==123 or service==456")
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(jsonPath("$.content", hasSize(3)));


    }

    @Test
    @DataSet("payments.yml")
    public void testGetAll() throws Exception {
        mvc.perform(get("/payments"))
                .andExpect(jsonPath("$.content", hasSize(5)))
                .andExpect(jsonPath("$.content[0].id", Matchers.is("a15bebb4-9846-43a0-9edb-97ea0e2bc0a9")));
    }

    @Test
    @DataSet("payments.yml")
    public void testSecurity() throws Exception {
        User user = User.builder().userName("").email("").withRole(new Role(EntityOp.Read, "service", "service1")).build();
        mvc.perform(get("/payments/e50d0699-7b1d-452a-88e9-527e26c0180d")
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(status().isNotFound());
        user = User.builder().userName("").email("").withRole(new Role(EntityOp.Read, "service", "123")).build();
        mvc.perform(get("/payments")
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(jsonPath("$.content", hasSize(3)));

    }

    @Test
    public void testCreate() throws Exception {
        doAnswer( i -> Optional.of(Service.builder()
                .fileFormat("ILF")
                .folder("ILF")
                .id("service1")
                .customerId("ILF")
                .createdBy("user")
                .paymentValidationRules(Collections.emptyList())
                .build()))
                .when(serviceRepository)
                .findById(anyString());

        PaymentCreateRequest request = PaymentCreateRequest.builder()
                .amount(Money.of(new BigDecimal("1"),"GBP"))
                .sortCode(SortCode.fromString("101010"))
                .accountNumber(AccountNumber.fromString("12345678"))
                .service("service1")
                .payee("dave")
                .build();
        User user = User.builder().userName("").email("").withRole(new Role(EntityOp.Read, "service", "service1")).build();
        mvc.perform(post("/payments")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(status().isForbidden());
        user = User.builder().userName("").email("").withRole(new Role(EntityOp.Write, "service", "service1")).build();
        mvc.perform(post("/payments")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(status().isForbidden());
        user = User.admin();
        mvc.perform(post("/payments")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(status().isForbidden());
        user = User.builder().userName("").email("").withRole(new Role(EntityOp.Write, "payment", "service1")).build();
        mvc.perform(post("/payments")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(status().isOk());
        verifyEvents("test-paymentCreatedEvents",l -> assertEquals(1,l.size()));

    }

    private void sendPaymentEvent(MessageChannel channel, UUID id, LocalDateTime timestamp, PaymentInstructionStatus status, Function<PaymentInstruction, Event<PaymentInstruction>> eventSupplier) {
        PaymentInstruction payment = PaymentInstruction.builder()
                .targetAmount(Money.of(new BigDecimal("10"),"GBP"))
                .timestamp(timestamp)
                .service("123")
                .recipient(Recipient.builder().build())
                .targetSettlementCurrency(GBP_CURRENCY)
                .latestTargetSettlementDate(LocalDate.parse("2020-01-01"))
                .status(status)
                .id(id)
                .build();
        channel.send(new GenericMessage<>(eventSupplier.apply(payment)));
        sleep(Duration.ofSeconds(4));
    }

    private void verifyPaymentEvents(UUID id, PaymentInstructionStatus latestStatus, int count) throws Exception {
        mvc.perform(get("/payments/{id}",id))
                .andExpect(jsonPath("$.status", Matchers.is(latestStatus.name())));
        mvc.perform(get("/payments/{id}/history",id))
                .andExpect(jsonPath("$", hasSize(count)));
    }

    @Configuration
    @Import({OptionalResponseControllerAdvice.class, KafkaNullConfiguration.class, PaymentPersistenceConfiguration.class,PaymentQueryServiceConfiguration.class, MockWebSecurityConfiguration.class})
    @EnableBinding({ PaymentPersistenceBinding.class, PaymentCreationBinding.class,TestBinding.class})
    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    public static class TestConfiguration extends AbstractKafkaTest.TestConfiguration{

    }

    public interface TestBinding {

        @Output("send-payment-created-events")
        MessageChannel paymentCreatedEvents();

        @Output("send-payment-validation-success-events")
        MessageChannel successfulValidationEvents();

        @Output("send-payment-validation-failed-events")
        MessageChannel failedValidationEvents();

        @Output("send-payment-routing-failed-events")
        MessageChannel failedRoutingEvents();

        @Output("send-payment-submission-success-events")
        MessageChannel successfulSubmissionEvents();

        @Output("send-payment-submission-failed-events")
        MessageChannel failedSubmissionEvents();

        @Output("send-payment-accepted-events")
        MessageChannel acceptedEvents();

        @Output("send-payment-rejection-events")
        MessageChannel rejectedEvents();

        @Output("send-payment-cleared-events")
        MessageChannel clearedEvents();

        @Output("send-payment-failedToClear-events")
        MessageChannel failedToClearEvents();

        @Output("send-payment-settled-events")
        MessageChannel settledEvents();

        @Output("send-accesspay-events")
        MessageChannel accesspayEvents();

        @Output("send-bottomline-events")
        MessageChannel bottomlineEvents();
    }
}
