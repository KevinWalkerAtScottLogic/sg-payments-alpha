package gov.scot.payments.inboundpayment;

import gov.scot.payments.model.core.Clearing;
import gov.scot.payments.model.inboundpayment.ChannelType;
import gov.scot.payments.model.inboundpayment.InboundPayment;
import gov.scot.payments.model.inboundpayment.PaymentStatus;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class InboundPaymentPersistenceProcessorTest {

    private InboundPaymentPersistenceProcessor processor;
    private TemporalInboundPaymentRepository temporalInboundPaymentRepository;
    private NonTemporalInboundPaymentRepository nonTemporalInboundPaymentRepository;
    private TransactionOperations transactionTemplate;

    @BeforeEach
    public void setUp(){
        temporalInboundPaymentRepository = mock(TemporalInboundPaymentRepository.class);
        nonTemporalInboundPaymentRepository = mock(NonTemporalInboundPaymentRepository.class);
        transactionTemplate = mock(TransactionTemplate.class);
        doAnswer(i -> {
            i.getArgument(0, TransactionCallback.class).doInTransaction(mock(TransactionStatus.class));
            return null;
        })
                .when(transactionTemplate)
                .execute(any());
        processor = new InboundPaymentPersistenceProcessor(temporalInboundPaymentRepository, nonTemporalInboundPaymentRepository,transactionTemplate);
    }

    @Test
    public void testPersistNew(){
        when(nonTemporalInboundPaymentRepository.findById(any())).thenReturn(Optional.empty());
        var payment = InboundPayment.builder()
                .timestamp(LocalDateTime.now())
                .amount(Money.of(new BigDecimal("10"),"GBP"))
                .channelType(ChannelType.BankTransfer)
                .channelId("bank_transfer_channel")
                .ref("A reference")
                .status(PaymentStatus.New)
                .statusMessage("A status message")
                .service("service")
                .clearing(Clearing.builder().build())
                .build();
        processor.persist(payment);
        verify(temporalInboundPaymentRepository,times(1)).save(any());
        verify(nonTemporalInboundPaymentRepository,times(1)).save(any());
        verify(nonTemporalInboundPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(temporalInboundPaymentRepository, nonTemporalInboundPaymentRepository);
    }

    @Test
    public void testPersistExistsOlder(){
        var existing = InboundPayment.builder()
                .timestamp(LocalDateTime.now().minusSeconds(10))
                .amount(Money.of(new BigDecimal("10"),"GBP"))
                .channelType(ChannelType.BankTransfer)
                .channelId("bank_transfer_channel")
                .ref("A reference")
                .status(PaymentStatus.New)
                .statusMessage("A status message")
                .service("service")
                .clearing(Clearing.builder().build())
                .build();
        var payment = InboundPayment.builder()
                .timestamp(LocalDateTime.now())
                .amount(Money.of(new BigDecimal("100"),"GBP"))
                .channelType(ChannelType.BankTransfer)
                .channelId("bank_transfer_channel")
                .ref("A reference")
                .status(PaymentStatus.New)
                .statusMessage("A status message")
                .service("service")
                .clearing(Clearing.builder().build())
                .build();
        when(nonTemporalInboundPaymentRepository.findById(any())).thenReturn(Optional.of(FlattenedInboundPayment.from(existing)));
        processor.persist(payment);
        verify(temporalInboundPaymentRepository,times(1)).save(any());
        verify(nonTemporalInboundPaymentRepository,times(1)).save(any());
        verify(nonTemporalInboundPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(temporalInboundPaymentRepository, nonTemporalInboundPaymentRepository);
    }

    @Test
    public void testPersistExistsNewer(){
        var existing = InboundPayment.builder()
                .timestamp(LocalDateTime.now())
                .amount(Money.of(new BigDecimal("10"),"GBP"))
                .channelType(ChannelType.BankTransfer)
                .channelId("bank_transfer_channel")
                .ref("A reference")
                .status(PaymentStatus.New)
                .statusMessage("A status message")
                .service("service")
                .clearing(Clearing.builder().build())
                .build();
        var payment = InboundPayment.builder()
                .timestamp(LocalDateTime.now().minusSeconds(10))
                .amount(Money.of(new BigDecimal("100"),"GBP"))
                .channelType(ChannelType.BankTransfer)
                .channelId("bank_transfer_channel")
                .ref("A reference")
                .status(PaymentStatus.New)
                .statusMessage("A status message")
                .service("service")
                .clearing(Clearing.builder().build())
                .build();
        when(nonTemporalInboundPaymentRepository.findById(any())).thenReturn(Optional.of(FlattenedInboundPayment.from(existing)));
        processor.persist(payment);
        verify(temporalInboundPaymentRepository,times(1)).save(any());
        verify(nonTemporalInboundPaymentRepository,times(1)).save(any());
        verify(nonTemporalInboundPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(temporalInboundPaymentRepository, nonTemporalInboundPaymentRepository);
    }
}
