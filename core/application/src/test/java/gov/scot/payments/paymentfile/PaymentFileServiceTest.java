package gov.scot.payments.paymentfile;

import gov.scot.payments.model.paymentfile.PaymentFileStatus;
import gov.scot.payments.model.user.EntityOp;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.model.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.mockito.Mockito.*;

public class PaymentFileServiceTest {

    private PaymentFileService service;
    private PaymentFileRepository repository;

    @BeforeEach
    public void setUp(){
        repository = mock(PaymentFileRepository.class);
        service = new PaymentFileService(repository);
    }

    @Test
    public void testGetLatestByFileNameStartsWith(){
        service.getLatestPaymentFileByName("",true, User.admin());
        verify(repository,times(1)).findLatestByNameStartsWith("");
    }

    @Test
    public void testGetLatestByFileNameStartsWithAndAcl(){
        service.getLatestPaymentFileByName("",true, User.builder()
                .userName("")
                .email("")
                .withRole(new Role(EntityOp.Read,"service","123"))
                .build());
        verify(repository,times(1)).findLatestByNameStartsWithWithAcl("");
    }

    @Test
    public void testGetLatestByFileName(){
        service.getLatestPaymentFileByName("",false, User.admin());
        verify(repository,times(1)).findLatestByName("");
    }

    @Test
    public void testGetLatestByFileNameAndAcl(){
        service.getLatestPaymentFileByName("",false, User.builder()
                .userName("")
                .email("")
                .withRole(new Role(EntityOp.Read,"service","123"))
                .build());
        verify(repository,times(1)).findLatestByNameWithAcl("");
    }

    @Test
    public void testGetAllByFileNameStartsWith(){
        service.getPaymentFileHistoryByName("",true, User.admin());
        verify(repository,times(1)).findAllByNameStartsWith("");
    }

    @Test
    public void testGetAllByFileNameStartsWithAndAcl(){
        service.getPaymentFileHistoryByName("",true, User.builder()
                .userName("")
                .email("")
                .withRole(new Role(EntityOp.Read,"service","123"))
                .build());
        verify(repository,times(1)).findAllByNameStartsWithWithAcl("");
    }

    @Test
    public void testGetAllByFileName(){
        service.getPaymentFileHistoryByName("",false, User.admin());
        verify(repository,times(1)).findAllByName("");
    }


    @Test
    public void testGetAllByFileNameAndAcl(){
        service.getPaymentFileHistoryByName("",false, User.builder()
                .userName("")
                .email("")
                .withRole(new Role(EntityOp.Read,"service","123"))
                .build());
        verify(repository,times(1)).findAllByNameWithAcl("");
    }

    @Test
    public void testQueryLatestTimeRange(){
        LocalDateTime from = LocalDateTime.now();
        LocalDateTime to = LocalDateTime.now();
        service.queryLatestPaymentFiles(null,from,to,null, User.admin());
        verify(repository,times(1)).findAllByTimeRange(from,to);
    }

    @Test
    public void testQueryLatestTimeRangeAndAcl(){
        LocalDateTime from = LocalDateTime.now();
        LocalDateTime to = LocalDateTime.now();
        service.queryLatestPaymentFiles(null,from,to,null, User.builder()
                .userName("")
                .email("")
                .withRole(new Role(EntityOp.Read,"service","123"))
                .build());
        verify(repository,times(1)).findAllByTimeRangeWithAcl(from,to);
    }


    @Test
    public void testQueryLatestTimeRangeNotSpecified(){
        service.queryLatestPaymentFiles(null,null,null,null, User.admin());
        verify(repository,times(1)).findAllByTimeRange(eq(LocalDateTime.of(LocalDate.EPOCH, LocalTime.MIN)),notNull());
    }

    @Test
    public void testQueryLatestByService(){
        LocalDateTime from = LocalDateTime.now();
        LocalDateTime to = LocalDateTime.now();
        service.queryLatestPaymentFiles("",from,to,null, User.admin());
        verify(repository,times(1)).findAllByTimeRangeAndService(from,to,"");
    }

    @Test
    public void testQueryLatestByServiceAndAcl(){
        LocalDateTime from = LocalDateTime.now();
        LocalDateTime to = LocalDateTime.now();
        service.queryLatestPaymentFiles("",from,to,null, User.builder()
                .userName("")
                .email("")
                .withRole(new Role(EntityOp.Read,"service","123"))
                .build());
        verify(repository,times(1)).findAllByTimeRangeAndServiceWithAcl(from,to,"");
    }



    @Test
    public void testQueryLatestByStatus(){
        LocalDateTime from = LocalDateTime.now();
        LocalDateTime to = LocalDateTime.now();
        service.queryLatestPaymentFiles(null,from,to, PaymentFileStatus.New, User.admin());
        verify(repository,times(1)).findAllByTimeRangeAndStatus(from,to,PaymentFileStatus.New.name());
    }


    @Test
    public void testQueryLatestByStatusAndAcl(){
        LocalDateTime from = LocalDateTime.now();
        LocalDateTime to = LocalDateTime.now();
        service.queryLatestPaymentFiles(null,from,to, PaymentFileStatus.New, User.builder()
                .userName("")
                .email("")
                .withRole(new Role(EntityOp.Read,"service","123"))
                .build());
        verify(repository,times(1)).findAllByTimeRangeAndStatusWithAcl(from,to,PaymentFileStatus.New.name());
    }

    @Test
    public void testQueryLatestByServiceAndStatus(){
        LocalDateTime from = LocalDateTime.now();
        LocalDateTime to = LocalDateTime.now();
        service.queryLatestPaymentFiles("",from,to,PaymentFileStatus.New, User.admin());
        verify(repository,times(1)).findAllByTimeRangeServiceAndStatus(from,to,"",PaymentFileStatus.New.name());
    }

    @Test
    public void testQueryLatestByServiceAndStatusAndAcl(){
        LocalDateTime from = LocalDateTime.now();
        LocalDateTime to = LocalDateTime.now();
        service.queryLatestPaymentFiles("",from,to,PaymentFileStatus.New, User.builder()
                .userName("")
                .email("")
                .withRole(new Role(EntityOp.Read,"service","123"))
                .build());
        verify(repository,times(1)).findAllByTimeRangeServiceAndStatusWithAcl(from,to,"",PaymentFileStatus.New.name());
    }
}
