package gov.scot.payments.paymentfile;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.event.S3EventNotification;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import gov.scot.payments.AbstractKafkaTest;
import gov.scot.payments.MockWebSecurityConfiguration;
import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.customer.ServiceRepository;
import gov.scot.payments.model.paymentfile.PaymentFile;
import gov.scot.payments.model.customer.Service;
import gov.scot.payments.paymentfile.spring.*;
import gov.scot.payments.WithUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.aws.autoconfigure.context.ContextCredentialsAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.jdbc.AmazonRdsDatabaseAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.messaging.MessagingAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@EnableAutoConfiguration(exclude = {MessagingAutoConfiguration.class
        , ContextStackAutoConfiguration.class
        , ContextResourceLoaderAutoConfiguration.class
        , ContextRegionProviderAutoConfiguration.class
        , ContextCredentialsAutoConfiguration.class
        , AmazonRdsDatabaseAutoConfiguration.class
})
@EmbeddedKafka(partitions = 1
        ,topics = {"test-fileUploadEvents","test-fileTranslationFailedEvents","test-fileTranslationSuccessEvents","test-paymentCreatedEvents"})
@SpringBootTest(classes = PaymentFileIntegrationTest.TestConfiguration.class
        , properties = {"payments.environment=test", "spring.config.additional-location=classpath:/channels/paymentfile.properties"})
@Tag("kafka")
@WithUser
public class PaymentFileIntegrationTest extends AbstractKafkaTest {

    @Autowired PaymentFileProcessor processor;
    @MockBean AmazonS3 s3Client;
    @MockBean
    ServiceRepository serviceRepository;


    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();
        doAnswer( i -> i.getArgument(0,String.class).equalsIgnoreCase("ilf") ? Optional.of(Service.builder()
                .fileFormat("ILF")
                .folder("ILF")
                .id("ILF")
                .customerId("ILF")
                .createdBy("user")
                .paymentValidationRules(Collections.emptyList())
                .build()) : Optional.empty())
        .when(serviceRepository)
        .findByFolder(anyString());
        doAnswer( i -> i.getArgument(0,String.class).equalsIgnoreCase("ilf") ? Optional.of(Service.builder()
                .fileFormat("ILF")
                .folder("ILF")
                .id("ILF")
                .customerId("ILF")
                .createdBy("user")
                .paymentValidationRules(Collections.emptyList())
                .build()) : Optional.empty())
                .when(serviceRepository)
                .findById(anyString());
    }
    //checks behavior when null returned from onS3event
    @Test
    public void testNoService() throws Exception{
        S3EventNotification notification = PaymentFileProcessorTest.generateNotification("ObjectCreated:Post","123","abc/def.csv");
        publishEvent(notification);
        sleep(Duration.ofSeconds(2));
        mvc.perform(get("/paymentFiles/def.csv/history"))
                .andExpect(jsonPath("$", hasSize(0)));
        verifyEvents("test-fileUploadEvents",events -> assertEquals(0,events.size()));
    }

    //tests failed events being generated and persisted
    @Test
    @Transactional
    public void testTranslationFail() throws Exception{
        S3EventNotification notification = PaymentFileProcessorTest.generateNotification("ObjectCreated:Post","123","ILF/def1.csv");
        S3Object s3Object = spy(new S3Object());
        when(s3Object.getObjectContent()).thenThrow(new RuntimeException("IO Error"));
        when(s3Client.getObject(any())).thenReturn(s3Object);
        publishEvent(notification);
        sleep(Duration.ofSeconds(5));
        verifyEvents("test-fileUploadEvents",events -> assertEquals(1,events.size()));
        verifyEvents("test-fileTranslationFailedEvents",events -> assertEquals(1,events.size()));
        verifyEvents("test-fileTranslationSuccessEvents",events -> assertEquals(0,events.size()));
        verifyEvents("test-paymentCreatedEvents",events -> assertEquals(0,events.size()));
        mvc.perform(get("/paymentFiles/def1.csv/history"))
                .andExpect(jsonPath("$", hasSize(2)));
    }

    //tests success events being generated and persisted, and payment events being created
    @Test
    @Transactional
    public void testTranslationSuccess() throws Exception{
        S3EventNotification notification = PaymentFileProcessorTest.generateNotification("ObjectCreated:Post","123","ILF/d e f2.csv");
        S3Object s3Object = spy(new S3Object());
        s3Object.setObjectContent(getClass().getClassLoader().getResourceAsStream("ilf_examples/ILF_test.csv"));
        when(s3Client.getObject(any())).thenReturn(s3Object);
        publishEvent(notification);
        sleep(Duration.ofSeconds(5));
        ArgumentCaptor<GetObjectRequest> captor = ArgumentCaptor.forClass(GetObjectRequest.class);
        verify(s3Client,times(1)).getObject(captor.capture());
        GetObjectRequest request = captor.getValue();
        assertEquals("ILF/d e f2.csv",request.getKey());
        verifyEvents("test-fileUploadEvents",events -> assertEquals(1,events.size()));
        verifyEvents("test-fileTranslationFailedEvents",events -> assertEquals(0,events.size()));
        verifyEvents("test-fileTranslationSuccessEvents",events -> assertEquals(1,events.size()));
        verifyEvents("test-paymentCreatedEvents",events -> assertEquals(3,events.size()));
        mvc.perform(get("/paymentFiles/d e f2.csv/history"))
                .andExpect(jsonPath("$", hasSize(2)));
    }

    private void publishEvent(S3EventNotification notification){
        new Thread(() -> processor.onS3Event(notification)).run();
    }

    @EnableJpaRepositories(basePackageClasses = PaymentFileRepository.class)
    @EntityScan(basePackageClasses = PaymentFile.class)
    @Import({OptionalResponseControllerAdvice.class, PaymentFileServiceConfiguration.class,PaymentFileConfiguration.class, MockWebSecurityConfiguration.class})
    @EnableBinding({ PaymentFilePersistenceProcessorInputBinding.class
            , PaymentFileProcessorInputBinding.class
            , PaymentFileProcessorOutputBinding.class})
    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    public static class TestConfiguration extends AbstractKafkaTest.TestConfiguration {

    }

}
