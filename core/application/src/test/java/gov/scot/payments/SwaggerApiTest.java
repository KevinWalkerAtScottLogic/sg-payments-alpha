package gov.scot.payments;

import gov.scot.payments.cardpayment.CardPaymentGatewayProxy;
import gov.scot.payments.cardpayment.CardPaymentRepository;
import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.customer.CustomerRepository;
import gov.scot.payments.customer.ServiceRepository;
import gov.scot.payments.inboundpayment.NonTemporalInboundPaymentRepository;
import gov.scot.payments.inboundpayment.TemporalInboundPayment;
import gov.scot.payments.inboundpayment.TemporalInboundPaymentRepository;
import gov.scot.payments.model.cardpayment.CardPayment;
import gov.scot.payments.model.customer.Service;
import gov.scot.payments.model.paymentfile.PaymentFile;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import gov.scot.payments.model.report.Report;
import gov.scot.payments.model.user.User;
import gov.scot.payments.paymentfile.PaymentFileRepository;
import gov.scot.payments.paymentinstruction.NonTemporalPaymentRepository;
import gov.scot.payments.paymentinstruction.TemporalPayment;
import gov.scot.payments.paymentinstruction.TemporalPaymentRepository;
import gov.scot.payments.paymentprocessor.PaymentProcessorProxy;
import gov.scot.payments.report.AmazonQuickSightClientFactory;
import gov.scot.payments.report.ReportRepository;
import gov.scot.payments.router.PaymentRouter;
import gov.scot.payments.security.UserRepository;
import gov.scot.payments.spring.ApiConfiguration;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityFilterAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.aws.autoconfigure.context.ContextCredentialsAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.jdbc.AmazonRdsDatabaseAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.messaging.MessagingAutoConfiguration;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.zookeeper.ZookeeperAutoConfiguration;
import org.springframework.cloud.zookeeper.discovery.ZookeeperDiscoveryAutoConfiguration;
import org.springframework.cloud.zookeeper.serviceregistry.ZookeeperServiceRegistryAutoConfiguration;
import org.springframework.cloud.zookeeper.support.CuratorServiceDiscoveryAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.http.MediaType;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(secure = false, controllers = SwaggerApiTest.TestConfiguration.class,
        properties = {"payments.environment=test"
                ,"debug=true"
                ,"spring.hateoas.use-hal-as-default-json-media-type=false"
                ,"spring.test.mockmvc.secure=false"
                ,"SERVICE_TO_SERVICE_COGNITO_APP_CLIENT_SECRET=secret"
                ,"SERVICE_TO_SERVICE_COGNITO_APP_CLIENT_ID=id"
                ,"COGNITO_USER_POOL_ID=id"
                ,"AWS_ACCESS_KEY_ID=id"
                ,"AWS_SECRET_ACCESS_KEY=secret"})
@EnableSwagger2
@EnableHypermediaSupport(type = {})
@ActiveProfiles({"core","cognito","aws"})
public class SwaggerApiTest {

    @Autowired private MockMvc mockMvc;
    @MockBean PaymentProcessorProxy paymentProcessorProxy;
    @MockBean DiscoveryClient discoveryClient;
    @MockBean AmazonQuickSightClientFactory amazonQuickSightClientFactory;
    @MockBean PaymentRouter paymentRouter;
    @MockBean CardPaymentGatewayProxy cardPaymentGatewayProxy;
    @MockBean PaymentFileRepository paymentFileRepository;
    @MockBean ReportRepository reportRepository;
    @MockBean UserRepository userRepository;
    @MockBean PaymentProcessorRepository paymentProcessorRepository;
    @MockBean CardPaymentRepository cardPaymentRepository;
    @MockBean CustomerRepository customerRepository;
    @MockBean ServiceRepository serviceRepository;
    @MockBean TemporalPaymentRepository temporalPaymentRepository;
    @MockBean NonTemporalPaymentRepository nonTemporalPaymentRepository;
    @MockBean TemporalInboundPaymentRepository temporalInboundPaymentRepository;
    @MockBean NonTemporalInboundPaymentRepository nonTemporalInboundPaymentRepository;
    @MockBean(name = "manual-payment-created-events") MessageChannel paymentCreatedEvents;
    @MockBean(name = "customer-created-events") MessageChannel customerCreatedEventTopic;
    @MockBean(name = "customer-updated-events")MessageChannel customerUpdatedEventsTopic;
    @MockBean(name = "customer-deleted-events") MessageChannel customerDeletedEventsTopic;
    @MockBean(name = "inbound-payment-received-events") MessageChannel inboundPaymentReceivedEvents;
    @MockBean(name = "inbound-payment-attemptFailed-events") MessageChannel inboundPaymentAttemptFailedEvents;
    @MockBean(name = "service-updated-events") MessageChannel serviceUpdates;

    @Test
    public void swaggerJsonExists() throws Exception {
        String contentAsString = mockMvc
                .perform(get("/resources/v2/api-docs")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse().getContentAsString();
        try (Writer writer = new FileWriter(new File("build/generated/sources/swagger.json"))) {
            IOUtils.write(contentAsString, writer);
        }
    }

    @Configuration
    @Import({ApiConfiguration.class})
    public static class TestConfiguration {

    }
}
