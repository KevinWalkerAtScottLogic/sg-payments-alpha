package gov.scot.payments.paymentfile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import gov.scot.payments.MockWebSecurityConfiguration;
import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.model.paymentfile.PaymentFile;
import gov.scot.payments.model.user.EntityOp;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.WithUser;
import gov.scot.payments.model.user.User;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = PaymentFilePersistenceIntegrationTest.TestConfiguration.class)
@AutoConfigureDataJpa
@AutoConfigureWebMvc
@AutoConfigureMockMvc
@DBRider
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@Tag("db")
@WithUser
public class PaymentFilePersistenceIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    PaymentFileRepository repository;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    @DataSet("payment_files.yml")
    public void testFindLatest() throws Exception {
        mvc.perform(get("/paymentFiles/tes").param("startsWith","false")).andExpect(status().isNotFound());
        mvc.perform(get("/paymentFiles/test").param("startsWith","false")).andExpect(status().isOk()).andExpect(content().json("{\"status\":\"Translated\",\"service\":\"service\",\"createdAt\":\"2019-01-01T10:23:54\",\"timestamp\":\"2019-01-02T10:23:54\",\"id\":\"08df5471-0606-409d-8bcf-8c0c18dd1686\",\"name\":\"test\",\"type\":\"ILF\",\"url\":\"s3://path/to/file.csv\"}"));
        User user = User.builder().userName("").email("").withRole(new Role(EntityOp.Read, "service", "service1")).build();
        mvc.perform(get("/paymentFiles/test").param("startsWith","false").with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities())))).andExpect(status().isNotFound());
    }

    @Test
    @DataSet("payment_files.yml")
    public void testFindLatestStartsWith() throws Exception {
        String response = "{\"status\":\"Translated\",\"service\":\"service\",\"createdAt\":\"2019-01-01T10:23:54\",\"timestamp\":\"2019-01-02T10:23:54\",\"id\":\"08df5471-0606-409d-8bcf-8c0c18dd1686\",\"name\":\"test\",\"type\":\"ILF\",\"url\":\"s3://path/to/file.csv\"}";
        mvc.perform(get("/paymentFiles/test1234")).andExpect(status().isNotFound());
        mvc.perform(get("/paymentFiles/tes")).andExpect(status().isOk()).andExpect(content().json(response));
        mvc.perform(get("/paymentFiles/test")).andExpect(status().isOk()).andExpect(content().json(response));
        User user = User.builder().userName("").email("").withRole(new Role(EntityOp.Read, "service", "service1")).build();
        mvc.perform(get("/paymentFiles/test").with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities())))).andExpect(status().isNotFound());
    }

    @Test
    @DataSet("payment_files.yml")
    public void testFindAllStartsWith() throws Exception {
        String response = "[{\"status\":\"Translated\",\"service\":\"service\",\"createdAt\":\"2019-01-01T10:23:54\",\"timestamp\":\"2019-01-02T10:23:54\",\"id\":\"08df5471-0606-409d-8bcf-8c0c18dd1686\",\"name\":\"test\",\"type\":\"ILF\",\"url\":\"s3://path/to/file.csv\"},{\"status\":\"New\",\"service\":\"service\",\"createdAt\":\"2019-01-01T10:23:54\",\"timestamp\":\"2019-01-01T10:23:54\",\"id\":\"b5b21bbe-fbdf-436c-ab17-8cd2ac17e71f\",\"name\":\"test\",\"type\":\"ILF\",\"url\":\"s3://path/to/file.csv\"}]";
        mvc.perform(get("/paymentFiles/tes/history")).andExpect(status().isOk()).andExpect(content().json(response));
        mvc.perform(get("/paymentFiles/test/history")).andExpect(status().isOk()).andExpect(content().json(response));
        User user = User.builder().userName("").email("").withRole(new Role(EntityOp.Read, "service", "service1")).build();
        mvc.perform(get("/paymentFiles/test/history").with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities())))).andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    @DataSet("payment_files.yml")
    public void testFindAll() throws Exception {
        String response = "[{\"status\":\"Translated\",\"service\":\"service\",\"createdAt\":\"2019-01-01T10:23:54\",\"timestamp\":\"2019-01-02T10:23:54\",\"id\":\"08df5471-0606-409d-8bcf-8c0c18dd1686\",\"name\":\"test\",\"type\":\"ILF\",\"url\":\"s3://path/to/file.csv\"},{\"status\":\"New\",\"service\":\"service\",\"createdAt\":\"2019-01-01T10:23:54\",\"timestamp\":\"2019-01-01T10:23:54\",\"id\":\"b5b21bbe-fbdf-436c-ab17-8cd2ac17e71f\",\"name\":\"test\",\"type\":\"ILF\",\"url\":\"s3://path/to/file.csv\"}]";
        mvc.perform(get("/paymentFiles/test1234/history").param("startsWith","false")).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(0)));
        mvc.perform(get("/paymentFiles/tes/history").param("startsWith","false")).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(0)));
        mvc.perform(get("/paymentFiles/test/history").param("startsWith","false")).andExpect(status().isOk()).andExpect(content().json(response));
        User user = User.builder().userName("").email("").withRole(new Role(EntityOp.Read, "service", "service1")).build();
        mvc.perform(get("/paymentFiles/test/history").param("startsWith","false").with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities())))).andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    @DataSet("payment_files.yml")
    public void testFindByTimeRange() throws Exception {
        mvc.perform(get("/paymentFiles")
                .param("to", "2019-03-18T00:00:00"))
                .andExpect(jsonPath("$", hasSize(4)));

        User user = User.builder().userName("").email("").withRole(new Role(EntityOp.Read, "service", "service1")).build();
        mvc.perform(get("/paymentFiles")
                .param("to", "2019-03-18T00:00:00")
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    @DataSet("payment_files.yml")
    public void testFindByTimeRangeAndStatus() throws Exception {
        mvc.perform(get("/paymentFiles")
                .param("to", "2019-03-18T00:00:00")
                .param("status","New"))
                .andExpect(jsonPath("$", hasSize(2)));

        User user = User.builder().userName("").email("").withRole(new Role(EntityOp.Read, "service", "service1")).build();
        mvc.perform(get("/paymentFiles")
                .param("to", "2019-03-18T00:00:00")
                .param("status","New")
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    @DataSet("payment_files.yml")
    public void testFindByTimeRangeAndService() throws Exception {
        mvc.perform(get("/paymentFiles")
                .param("service","service1")
                .param("to", "2019-03-18T00:00:00"))
                .andExpect(jsonPath("$", hasSize(2)));

        User user = User.builder().userName("").email("").withRole(new Role(EntityOp.Read, "service", "service2")).build();
        mvc.perform(get("/paymentFiles")
                .param("service","service1")
                .param("to", "2019-03-18T00:00:00")
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    @DataSet("payment_files.yml")
    public void testFindByTimeRangeStatusAndService() throws Exception {
        mvc.perform(get("/paymentFiles")
                .param("service","service1")
                .param("to", "2019-03-18T00:00:00")
                .param("status","New"))
                .andExpect(jsonPath("$", hasSize(1)));

        User user = User.builder().userName("").email("").withRole(new Role(EntityOp.Read, "service", "service1")).build();
        mvc.perform(get("/paymentFiles")
                .param("service","service1")
                .param("to", "2019-03-18T00:00:00")
                .param("status","New")
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    public void testPersistence() throws Exception {
        var paymentFile = PaymentFile.builder()
                .type("test")
                .name("abc")
                .service("123")
                .url(URI.create("s3://path/to/file.csv"))
                .timestamp(LocalDateTime.now().truncatedTo(ChronoUnit.MILLIS))
                .createdAt(LocalDateTime.now().truncatedTo(ChronoUnit.MILLIS))
                .build();
        repository.save(paymentFile);
        String response = objectMapper.writeValueAsString(paymentFile);
        mvc.perform(get("/paymentFiles/abc")).andExpect(status().isOk()).andExpect(content().json(response));

    }


    @Configuration
    @EnableJpaRepositories(basePackageClasses = PaymentFileRepository.class)
    @EntityScan(basePackageClasses = PaymentFile.class)
    @Import({OptionalResponseControllerAdvice.class, MockWebSecurityConfiguration.class})
    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    public static class TestConfiguration{

        @Bean
        public PaymentFileService paymentFileService(PaymentFileRepository paymentFileRepository){
            return new PaymentFileService(paymentFileRepository);
        }


    }
}
