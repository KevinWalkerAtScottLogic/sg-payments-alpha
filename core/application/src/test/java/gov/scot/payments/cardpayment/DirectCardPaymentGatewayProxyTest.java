package gov.scot.payments.cardpayment;

import gov.scot.payments.common.gateway.inbound.cardpayment.CardPaymentGateway;
import gov.scot.payments.model.cardpayment.CardPaymentGatewayAuthRequest;
import gov.scot.payments.model.cardpayment.CardPaymentGatewayCancelRequest;
import gov.scot.payments.model.cardpayment.CardPaymentGatewaySubmitRequest;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class DirectCardPaymentGatewayProxyTest {

    private CardPaymentGatewayProxy proxy;
    private CardPaymentGateway delegate;

    @BeforeEach
    public void setUp(){
        delegate = mock(CardPaymentGateway.class);
        proxy = new DirectCardPaymentGatewayProxy(Collections.singletonMap("worldpay",delegate));
    }

    @Test
    public void testAuthNotPresent(){
        var request = CardPaymentGatewayAuthRequest.builder()
                .amount(Money.of(1,"GBP"))
                .paymentRef(UUID.randomUUID())
                .build();
        assertThrows(IllegalArgumentException.class,() -> proxy.authorize("stripe",request));
    }

    @Test
    public void testSubmitNotPresent(){
        var request = CardPaymentGatewaySubmitRequest.builder()
                .ref("123")
                .paymentRef(UUID.randomUUID())
                .build();
        assertThrows(IllegalArgumentException.class,() -> proxy.submit("stripe",request));
    }

    @Test
    public void testCancelNotPresent(){
        var request = CardPaymentGatewayCancelRequest.builder()
                .paymentRef(UUID.randomUUID())
                .ref("123")
                .build();
        assertThrows(IllegalArgumentException.class,() -> proxy.cancel("stripe",request));
    }

    @Test
    public void testAuthPresent(){
        var request = CardPaymentGatewayAuthRequest.builder()
                .amount(Money.of(1,"GBP"))
                .paymentRef(UUID.randomUUID())
                .build();
        proxy.authorize("worldpay",request);
        verify(delegate,times(1)).authorize(request);
    }

    @Test
    public void testCancelPresent(){
        var request = CardPaymentGatewayCancelRequest.builder()
                .paymentRef(UUID.randomUUID())
                .ref("123")
                .build();
        proxy.cancel("worldpay",request);
        verify(delegate,times(1)).cancel(request);
    }

    @Test
    public void testSubmitPresent(){
        var request = CardPaymentGatewaySubmitRequest.builder()
                .ref("123")
                .paymentRef(UUID.randomUUID())
                .build();
        proxy.submit("worldpay",request);
        verify(delegate,times(1)).submit(request);
    }

}
