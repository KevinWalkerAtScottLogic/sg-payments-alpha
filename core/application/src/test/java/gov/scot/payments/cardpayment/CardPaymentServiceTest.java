package gov.scot.payments.cardpayment;

import gov.scot.payments.model.cardpayment.*;
import gov.scot.payments.model.customer.Service;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import gov.scot.payments.model.paymentinstruction.PaymentChannelDirection;
import gov.scot.payments.model.paymentinstruction.PaymentProcessorChannel;
import gov.scot.payments.model.user.ApiClient;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.model.user.Scope;
import gov.scot.payments.router.PaymentRouter;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.messaging.MessageChannel;
import org.springframework.ui.Model;
import org.springframework.web.server.ResponseStatusException;
import reactor.util.function.Tuples;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static gov.scot.payments.CustomMatchers.isDateTimeInRange;
import static gov.scot.payments.model.cardpayment.CardPaymentStatus.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class CardPaymentServiceTest {

    private CardPaymentService cardPaymentService;
    private CardPaymentRepository cardPaymentRepository;
    private PaymentRouter router;
    private CardPaymentGatewayProxy paymentGatewayProxy;
    private MessageChannel received;
    private MessageChannel failed;
    private Client user;
    private Model model;

    @BeforeEach
    void setUp() {
        cardPaymentRepository = mock(CardPaymentRepository.class);
        doAnswer(i -> i.getArgument(0,CardPayment.class)).when(cardPaymentRepository).save(any());
        router = mock(PaymentRouter.class);
        PaymentProcessorChannel channel = PaymentProcessorChannel.builder()
                .paymentChannel(PaymentChannel.Card)
                .costExpression("")
                .direction(PaymentChannelDirection.Inbound)
                .build();
        when(router.getValidInboundChannels(any(),any())).thenReturn(Collections.singletonList(Tuples.of("processor", channel,Money.of(1,"GBP"))));
        paymentGatewayProxy = mock(CardPaymentGatewayProxy.class);
        received = mock(MessageChannel.class);
        failed = mock(MessageChannel.class);
        cardPaymentService = new CardPaymentService(cardPaymentRepository
                ,router
                ,paymentGatewayProxy
                ,received
                ,failed
                ,s -> Optional.of(Service.builder().id("service").customerId("customer").fileFormat("").folder("").build())
                , Duration.ofMinutes(5)
                ,"");
        user = ApiClient.builder()
                .userName("service")
                .withScope(new Scope("inbound-payments",Scope.WILDCARD))
                .build();
        model = mock(Model.class);
    }


    @Test
    void testGetPaymentNotFound(){
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.empty());
        assertFalse(cardPaymentService.getPayment(UUID.randomUUID(),user).isPresent());
    }

    @Test
    void testGetPaymentInvalidUser(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service1")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .expectedCost(Money.of(1,"GBP"))
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        assertThrows(ResponseStatusException.class,() -> cardPaymentService.getPayment(payment.getId(),ApiClient.builder()
                .userName("service")
                .withScope(new Scope("inbound-payments","service"))
                .build()));
    }

    @Test
    void testGetPaymentValid(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .expectedCost(Money.of(1,"GBP"))
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        assertEquals(payment,cardPaymentService.getPayment(payment.getId(),user).get());
    }

    @Test
    void testCreateCardPaymentNoChannels() {
        when(router.getValidInboundChannels(any(),any())).thenReturn(Collections.emptyList());
        var request = CreateCardPaymentRequest.builder()
                .amount(Money.of(987,"GBP"))
                .reference("a reference")
                .returnURL("https://source.com/next/page")
                .service("service")
                .build();
        assertThrows(ResponseStatusException.class,() -> cardPaymentService.createPayment(request,user));
    }

    @Test
    void testCreateCardPaymentValid() {
        var before = LocalDateTime.now();
        var request = CreateCardPaymentRequest.builder()
                .amount(Money.of(987,"GBP"))
                .reference("a reference")
                .returnURL("https://source.com/next/page")
                .service("service")
                .build();
        var cardPayment = cardPaymentService.createPayment(request, user);
        var after = LocalDateTime.now();

        assertThat(cardPayment.getId(), notNullValue());
        assertThat(cardPayment.getTimestamp(), isDateTimeInRange(before, after));
        assertThat(cardPayment.getAmount(), is(Money.of(987, "GBP")));
        assertThat(cardPayment.getStatus(), is(Created));
        assertThat(cardPayment.getService(), is("service"));
        assertThat(cardPayment.getReference(), is("a reference"));
        assertThat(cardPayment.getReturnUrl(), is("https://source.com/next/page"));
        assertThat(cardPayment.getProcessorId(), is("processor"));
        verify(cardPaymentRepository, times(1)).save(any(CardPayment.class));
    }

    @Test
    void testGetPaymentPageNotFound(){
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.empty());
        assertThrows(ResponseStatusException.class,() -> cardPaymentService.getPaymentPage(UUID.randomUUID(), model));
    }

    @Test
    void testGetPaymentPageSuccess(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Created)
                .expectedCost(Money.of(1,"GBP"))
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        Assertions.assertEquals("CardPayment", cardPaymentService.getPaymentPage(payment.getId(), model));
        verify(model, times(1)).addAttribute("id", payment.getId());
    }

    @Test
    void testGetPaymentPageWrongState(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(CardPaymentStatus.Cancelled)
                .expectedCost(Money.of(1,"GBP"))
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        assertThrows(ResponseStatusException.class,() -> cardPaymentService.getPaymentPage(payment.getId(), model));
    }

    @Test
    void testAuthorizeNotFound(){
        var request = AuthorizeCardPaymentRequest.builder().build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.empty());
        assertThrows(ResponseStatusException.class,() -> cardPaymentService.authorizePayment(UUID.randomUUID(),request, model));
        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    @Test
    void testAuthorizeWrongState(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Created)
                .expectedCost(Money.of(1,"GBP"))
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        var request = AuthorizeCardPaymentRequest.builder().build();
        assertThrows(ResponseStatusException.class,() -> cardPaymentService.authorizePayment(payment.getId(),request, model));
        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    @Test
    void testAuthorizeExpired(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Started)
                .timestamp(LocalDateTime.now().minusMinutes(10))
                .expectedCost(Money.of(1,"GBP"))
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        var request = AuthorizeCardPaymentRequest.builder().build();

        assertEquals("CardPaymentGenericError",  cardPaymentService.authorizePayment(payment.getId(), request, model));
        verify(model, times(1)).addAttribute("service", "service");
        verify(model, times(1)).addAttribute("serviceUrl", "b");
        verify(model, times(1)).addAttribute("errorTitle", "Authorisation error");
        verify(model, times(1)).addAttribute("errorText", "The page for this payment has expired (ID "+payment.getId()+")");

        verify(cardPaymentRepository,times(1)).save(any());
        verify(failed,times(1)).send(any());
        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    @Test
    void testAuthorizeErrorSubmitting(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Started)
                .expectedCost(Money.of(1,"GBP"))
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        when(paymentGatewayProxy.authorize(anyString(),any())).thenThrow(new RuntimeException());
        var request = AuthorizeCardPaymentRequest.builder().build();

        assertEquals("CardPaymentConfirmError",  cardPaymentService.authorizePayment(payment.getId(), request, model));
        verify(model, times(1)).addAttribute("errorTitle", "Authorisation error");
        verify(model, times(1)).addAttribute("errorText", "An error has occurred authorising payment (ID "+payment.getId()+")");

        verify(cardPaymentRepository,times(1)).save(any());
        verify(failed,times(1)).send(any());
        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    @Test
    void testAuthorizeFailedToSubmit(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Started)
                .expectedCost(Money.of(1,"GBP"))
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        var response = CardPaymentGatewayAuthResponse.builder()
                .ref("123")
                .status(CardPaymentStatus.Error)
                .build();
        when(paymentGatewayProxy.authorize(anyString(),any())).thenReturn(response);
        var request = AuthorizeCardPaymentRequest.builder().build();
        cardPaymentService.authorizePayment(payment.getId(),request, model);
        verify(cardPaymentRepository,times(1)).save(any());
        verify(failed,times(1)).send(any());
        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    @Test
    void testAuthorizeSuccess(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Started)
                .expectedCost(Money.of(1,"GBP"))
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        var response = CardPaymentGatewayAuthResponse.builder()
                .ref("123")
                .build();
        when(paymentGatewayProxy.authorize(anyString(),any())).thenReturn(response);
        var request = AuthorizeCardPaymentRequest.builder()
                .cardNumber("1234123412341234")
                .cvv("123")
                .year(22)
                .month(02)
                .name("test")
                .build();
        cardPaymentService.authorizePayment(payment.getId(),request, model);
        verify(cardPaymentRepository,times(1)).save(any());
        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    //submit
    @Test
    void testSubmitNotFound(){
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.empty());
        assertThrows(ResponseStatusException.class,() -> cardPaymentService.submitPayment(UUID.randomUUID(), model));
        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    @Test
    void testSubmitWrongState(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Created)
                .expectedCost(Money.of(1,"GBP"))
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        assertEquals("CardPaymentGenericError",  cardPaymentService.submitPayment(payment.getId(), model));
        verify(model, times(1)).addAttribute("service", "service");
        verify(model, times(1)).addAttribute("serviceUrl", "b");
        verify(model, times(1)).addAttribute("errorTitle", "Submission error");
        verify(model, times(1)).addAttribute("errorText", "Payment in incorrect state to submit (ID "+payment.getId()+")");


        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    @Test
    void testSubmitExpired(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Submitted)
                .timestamp(LocalDateTime.now().minusMinutes(10))
                .expectedCost(Money.of(1,"GBP"))
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));

        cardPaymentService.submitPayment(payment.getId(), model);
        verify(model, times(1)).addAttribute("service", "service");
        verify(model, times(1)).addAttribute("serviceUrl", "b");
        //verify(model, times(1)).addAttribute("errorTitle", "Submission error");
        verify(model, times(1)).addAttribute("errorText", "The page for this payment has expired (ID "+payment.getId()+")");

        verify(cardPaymentRepository,times(1)).save(any());
        verify(failed,times(1)).send(any());
        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    @Test
    void testSubmitErrorSubmitting(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Submitted)
                .expectedCost(Money.of(1,"GBP"))
                .auth(CardPaymentGatewayAuthResponse.builder()
                        .ref("123")
                        .build())
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        when(paymentGatewayProxy.submit(anyString(),any())).thenThrow(new RuntimeException());
        assertEquals("CardPaymentGenericError",  cardPaymentService.submitPayment(payment.getId(), model));
        verify(model, times(1)).addAttribute("service", "service");
        verify(model, times(1)).addAttribute("serviceUrl", "b");
        verify(model, times(1)).addAttribute("errorTitle", "Submission error");
        verify(model, times(1)).addAttribute("errorText", "An error has occurred submitting payment (ID "+payment.getId()+")");
        verify(cardPaymentRepository,times(1)).save(any());
        verify(failed,times(1)).send(any());
        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    @Test
    void testSubmitFailedToSubmit(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Submitted)
                .expectedCost(Money.of(1,"GBP"))
                .auth(CardPaymentGatewayAuthResponse.builder()
                        .ref("123")
                        .build())
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        var response = CardPaymentGatewaySubmitResponse.builder()
                .ref("123")
                .status(CardPaymentStatus.Error)
                .build();
        when(paymentGatewayProxy.submit(anyString(),any())).thenReturn(response);
        cardPaymentService.submitPayment(payment.getId(), model);
        verify(cardPaymentRepository,times(1)).save(any());
        verify(failed,times(1)).send(any());
        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    @Test
    void testSubmitSuccess(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Submitted)
                .expectedCost(Money.of(1,"GBP"))
                .auth(CardPaymentGatewayAuthResponse.builder()
                        .ref("123")
                        .build())
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        var response = CardPaymentGatewaySubmitResponse.builder()
                .ref("123")
                .build();
        when(paymentGatewayProxy.submit(anyString(),any())).thenReturn(response);
        cardPaymentService.submitPayment(payment.getId(), model);
        verify(cardPaymentRepository,times(1)).save(any());
        verify(received,times(1)).send(any());
        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    //cancel
    @Test
    void testCancelNotFound(){
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.empty());
        assertThrows(ResponseStatusException.class,() -> cardPaymentService.cancelPayment(UUID.randomUUID(), model));
        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    @Test
    void testCancelWrongState(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Success)
                .expectedCost(Money.of(1,"GBP"))
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        assertEquals("CardPaymentGenericError", cardPaymentService.cancelPayment(payment.getId(), model));
        verify(model, times(1)).addAttribute("service", "service");
        verify(model, times(1)).addAttribute("serviceUrl", "b");
        verify(model, times(1)).addAttribute("errorTitle", "Cancellation error");
        verify(model, times(1)).addAttribute("errorText", "An error has occurred cancelling payment (ID "+payment.getId()+")");

        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    @Test
    void testCancelErrorWhenStarted(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Started)
                .expectedCost(Money.of(1,"GBP"))
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        when(paymentGatewayProxy.cancel(anyString(),any())).thenThrow(new RuntimeException());
        assertEquals("CardPaymentCancel",  cardPaymentService.cancelPayment(payment.getId(), model));
        verify(cardPaymentRepository,times(1)).save(any());
        verify(failed,times(1)).send(any());
        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    @Test
    void testCancelErrorSubmitting(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Submitted)
                .auth(CardPaymentGatewayAuthResponse.builder().ref("ref").build())
                .expectedCost(Money.of(1,"GBP"))
                .build();

        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        when(paymentGatewayProxy.cancel(anyString(),any())).thenThrow(new RuntimeException());
        assertEquals("CardPaymentGenericError",  cardPaymentService.cancelPayment(payment.getId(), model));
        verify(model, times(1)).addAttribute("service", "service");
        verify(model, times(1)).addAttribute("serviceUrl", "b");
        verify(model, times(1)).addAttribute("errorTitle", "Cancellation error");
        verify(model, times(1)).addAttribute("errorText", "An error has occurred confirming payment (ID "+payment.getId()+")");

        verify(cardPaymentRepository,times(1)).save(any());
        verify(failed,times(1)).send(any());
        verify(cardPaymentRepository,times(1)).findById(any());
        verify(paymentGatewayProxy, times(1)).cancel(any(), any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    @Test
    void testCancelFailedToCancel(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Started)
                .expectedCost(Money.of(1,"GBP"))
                .auth(CardPaymentGatewayAuthResponse.builder()
                        .ref("123")
                        .build())
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        var response = CardPaymentGatewayCancelResponse.builder()
                .ref("123")
                .status(CardPaymentStatus.Error)
                .build();
        when(paymentGatewayProxy.cancel(anyString(),any())).thenReturn(response);
        cardPaymentService.cancelPayment(payment.getId(), model);
        verify(cardPaymentRepository,times(1)).save(any());
        verify(failed,times(1)).send(any());
        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    @Test
    void testCancelSuccess(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Started)
                .expectedCost(Money.of(1,"GBP"))
                .auth(CardPaymentGatewayAuthResponse.builder()
                        .ref("123")
                        .build())
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        var response = CardPaymentGatewayCancelResponse.builder()
                .ref("123")
                .build();
        when(paymentGatewayProxy.cancel(anyString(),any())).thenReturn(response);
        cardPaymentService.cancelPayment(payment.getId(), model);
        verify(cardPaymentRepository,times(1)).save(any());
        verify(failed,times(1)).send(any());
        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed);
    }

    @Test
    void testCancelCreatedSuccess(){
        var payment = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Created)
                .expectedCost(Money.of(1,"GBP"))
                .build();
        when(cardPaymentRepository.findById(any())).thenReturn(Optional.of(payment));
        cardPaymentService.cancelPayment(payment.getId(), model);
        verify(cardPaymentRepository,times(1)).save(any());
        verify(failed,times(1)).send(any());
        verify(cardPaymentRepository,times(1)).findById(any());
        verifyNoMoreInteractions(cardPaymentRepository,received,failed,paymentGatewayProxy);
    }

    @Test
    void testExpire(){
        var payment1 = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Created)
                .expectedCost(Money.of(1,"GBP"))
                .build();

        var payment2 = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Started)
                .expectedCost(Money.of(1,"GBP"))
                .build();

        var payment3 = CardPayment.builder()
                .amount(Money.of(1,"GBP"))
                .service("service")
                .reference("a")
                .returnUrl("b")
                .processorId("worldpay")
                .createdBy(user.getUserName())
                .status(Submitted)
                .expectedCost(Money.of(1,"GBP"))
                .auth(CardPaymentGatewayAuthResponse.builder().ref("123").build())
                .build();
        when(cardPaymentRepository.findExpired(any())).thenReturn(List.of(payment1,payment2,payment3));
        cardPaymentService.cleanUpExpiredPayments();
        verify(failed,times(3)).send(any());
        verify(paymentGatewayProxy,times(1)).cancel(any(),any());
        verifyNoMoreInteractions(failed,received,paymentGatewayProxy);
    }

}