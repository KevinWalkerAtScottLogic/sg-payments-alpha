package gov.scot.payments.cardpayment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import gov.scot.payments.AbstractKafkaTest;
import gov.scot.payments.MockWebSecurityConfiguration;
import gov.scot.payments.WithUser;
import gov.scot.payments.cardpayment.spring.CardPaymentServiceBinding;
import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.model.cardpayment.*;
import gov.scot.payments.model.customer.Service;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import gov.scot.payments.model.paymentinstruction.PaymentChannelDirection;
import gov.scot.payments.model.paymentinstruction.PaymentProcessorChannel;
import gov.scot.payments.model.user.ApiClient;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.model.user.Scope;
import gov.scot.payments.model.user.*;
import gov.scot.payments.router.PaymentRouter;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.aws.autoconfigure.context.ContextCredentialsAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.jdbc.AmazonRdsDatabaseAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.messaging.MessagingAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.MediaType;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.messaging.MessageChannel;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.test.annotation.DirtiesContext;
import reactor.util.function.Tuples;

import java.time.Duration;
import java.util.Collections;
import java.util.Optional;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import reactor.util.function.Tuples;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static gov.scot.payments.CustomMatchers.isDBTimeCloseTo;
import static gov.scot.payments.CustomMatchers.isDateTimeStringInRange;
import static gov.scot.payments.model.cardpayment.CardPaymentStatus.Created;
import static gov.scot.payments.model.cardpayment.CardPaymentStatus.Submitted;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@EmbeddedKafka(partitions = 1
        ,topics = {"test-inboundPaymentReceivedEvents","test-inboundPaymentAttemptFailedEvents"})
@SpringBootTest(classes = CardPaymentIntegrationTest.TestConfiguration.class,properties = {"payments.environment=test","h2.serializeJavaObject=false"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@Tag("db")
@Tag("kafka")
@DBRider
@EnableAutoConfiguration(exclude = {MessagingAutoConfiguration.class
        , ContextStackAutoConfiguration.class
        , ContextResourceLoaderAutoConfiguration.class
        , ContextRegionProviderAutoConfiguration.class
        , ContextCredentialsAutoConfiguration.class
        , AmazonRdsDatabaseAutoConfiguration.class
        , UserDetailsServiceAutoConfiguration.class
})
public class CardPaymentIntegrationTest extends AbstractKafkaTest {

    static {
        System.setProperty("h2.serializeJavaObject","false");
    }

    @Autowired
    CardPaymentRepository repository;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    PaymentRouter router;

    @MockBean
    CardPaymentGatewayProxy cardPaymentGatewayProxy;

    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();
        PaymentProcessorChannel channel = PaymentProcessorChannel.builder()
                .paymentChannel(PaymentChannel.Card)
                .costExpression("")
                .direction(PaymentChannelDirection.Inbound)
                .build();
        when(router.getValidInboundChannels(any(),any())).thenReturn(Collections.singletonList(Tuples.of("worldpay", channel,Money.of(1,"GBP"))));

        doAnswer(i -> {
            CardPaymentGatewayAuthRequest req = i.getArgument(1, CardPaymentGatewayAuthRequest.class);
            return CardPaymentGatewayAuthResponse.builder()
                    .ref("worldpay_1")
                    .amount(req.getAmount())
                    .build();
        }).when(cardPaymentGatewayProxy).authorize(anyString(),any());

        doAnswer(i -> {
            CardPaymentGatewaySubmitRequest req = i.getArgument(1, CardPaymentGatewaySubmitRequest.class);
            return CardPaymentGatewaySubmitResponse.builder()
                    .ref("worldpay_1")
                    .metadata(req.getMetadata())
                    .build();
        }).when(cardPaymentGatewayProxy).submit(anyString(),any());
    }

    @Test
    @WithUser(entityType = "inbound-payments",entityId = "service")
    void testCreateCardPayment() throws Exception {
        var request = CreateCardPaymentRequest.builder()
                .amount(Money.of(123,"GBP"))
                .reference("a reference")
                .returnURL("https://source.com/next/page")
                .service("service")
                .build();

        mvc.perform(post("/cardpayments").content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").isNotEmpty())
                .andExpect(content().json("{\"amount\":{\"amount\":\"123\",\"currency\":\"GBP\"}}"))
                .andExpect(content().json("{\"status\":\"Created\"}"))
                .andExpect(content().json("{\"service\":\"service\"}"))
                .andExpect(content().json("{\"reference\":\"a reference\"}"))
                .andExpect(content().json("{\"returnUrl\":\"https://source.com/next/page\"}"));

        verifyEvents("test-inboundPaymentReceivedEvents", l -> assertTrue(l.isEmpty()));
        verifyEvents("test-inboundPaymentAttemptFailedEvents", l -> assertTrue(l.isEmpty()));
    }

    @Test
    @WithUser(entityType = "inbound-payments",entityId = "service1")
    void testCreateCardPaymentWithoutAccessRights() throws Exception {
        var request = CreateCardPaymentRequest.builder()
                .amount(Money.of(987,"GBP"))
                .reference("a reference")
                .returnURL("https://source.com/next/page")
                .service("service")
                .build();
        mvc.perform(post("/cardpayments").content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @DataSet("cardpayments.yml")
    @WithUser(entityType = "inbound-payments",entityId = "service1")
    void testGetCardPaymentWithoutAccessRights() throws Exception {
        mvc.perform(get("/cardpayments/51170532-d926-491b-8760-57233f63c2f0"))
                .andExpect(status().isForbidden());
    }

    @Test
    @DataSet("cardpayments.yml")
    @WithUser(entityType = "inbound-payments",entityId = "service")
    void testGetCardPayment() throws Exception {
        mvc.perform(get("/cardpayments/51170532-d926-491b-8760-57233f63c2f0"))
                .andExpect(status().isOk());
    }

    @Test
    @DataSet("cardpayments.yml")
    void testMakeAPayment() throws Exception {
        Client user = ApiClient.builder().userName("service").withScope(new Scope("inbound-payments", "service")).build();
        var request = CreateCardPaymentRequest.builder()
                .amount(Money.of(123,"GBP"))
                .reference("a reference 1")
                .returnURL("https://source.com/next/page")
                .service("service")
                .build();
        CardPayment created = objectMapper.readValue(mvc.perform(post("/cardpayments")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andReturn()
                .getResponse()
                .getContentAsString(), CardPayment.class);

        mvc.perform(get("/cardpayments/{id}/form",created.getId()))
                .andExpect(status().isOk());

        mvc.perform(post("/cardpayments/{id}/authorize",created.getId())
                .contentType(APPLICATION_FORM_URLENCODED) //from MediaType
                .param("year", "23")
                .param("month", "11")
                .param("cvv", "123")
                .param("cardNumber", "1234567812345678")
                .param("name", "James Bond"))
                .andExpect(status().isOk());
        mvc.perform(post("/cardpayments/{id}/submit",created.getId()))
                .andExpect(status().isOk());
        CardPayment complete = objectMapper.readValue(mvc.perform(get("/cardpayments/{id}",created.getId())
                .content(objectMapper.writeValueAsString(request))
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andReturn()
                .getResponse()
                .getContentAsString(), CardPayment.class);
        assertEquals(repository.findById(created.getId()).get(),complete);
        verifyEvents("test-inboundPaymentReceivedEvents", l -> assertEquals(1,l.size()));
        verifyEvents("test-inboundPaymentAttemptFailedEvents", l -> assertTrue(l.isEmpty()));
        assertEquals("worldpay",complete.getProcessorId());
        assertEquals("service",complete.getService());
        assertEquals("a reference 1",complete.getReference());
        assertEquals("https://source.com/next/page",complete.getReturnUrl());
        assertEquals(CardPaymentStatus.Success,complete.getStatus());
        assertEquals(Money.of(123,"GBP"),complete.getAmount());
        assertEquals(Money.of(1,"GBP"),complete.getExpectedCost());
        assertEquals("worldpay_1",complete.getAuth().getRef());
        assertEquals("worldpay_1",complete.getSubmission().getRef());
    }


    @Configuration
    @EnableJpaRepositories(basePackageClasses = CardPaymentRepository.class)
    @EntityScan(basePackageClasses = CardPayment.class)
    @Import({OptionalResponseControllerAdvice.class, MockWebSecurityConfiguration.class})
    @EnableBinding({CardPaymentServiceBinding.class})
    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    public static class TestConfiguration extends AbstractKafkaTest.TestConfiguration{

        @Bean
        public CardPaymentService cardPaymentService(CardPaymentRepository cardPaymentRepository
                ,PaymentRouter router
                , CardPaymentGatewayProxy cardPaymentGatewayProxy
                , @Qualifier("inbound-payment-received-events") MessageChannel inboundPaymentReceivedEvents
                , @Qualifier("inbound-payment-attemptFailed-events") MessageChannel inboundPaymentAttemptFailedEvents) {
            return new CardPaymentService(cardPaymentRepository
                    ,router
                    ,cardPaymentGatewayProxy
                    ,inboundPaymentReceivedEvents
                    ,inboundPaymentAttemptFailedEvents
                    ,s -> Optional.of(Service.builder().id("service").customerId("customer").fileFormat("").folder("").build())
                    , Duration.ofMinutes(5)
                    ,"");
        }
    }

}
