package gov.scot.payments.paymentfile.cps;

import gov.scot.payments.paymentfile.cps.CpsParser.Records;
import gov.scot.payments.paymentfile.FileParseException;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;

import static gov.scot.payments.paymentfile.cps.CpsParser.FIELD_SEPARATOR;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Test CpsParser")
class CpsParserTest {

    private CpsParser cpsParser;

    private static final String EMS_PAYMENT_REF_NO_1 = "12LM111147A00001";
    private static final String CLIENT_PAYMENT_LINE_1 = "CLN|12|SCO1|12LM111147A00001|LM111147A|Surname37|||||||||SCO|901487||11212200||||1|60000|Personal Bank Acc|||||||LM111147A SSS BSG|||1000000030||500001|03||20180627|GBP||||||20180621|20180621|99||||||||||||||||";
    private static final String COMPONENT_LINE_1_1 = "CMP|12|SCO1|12LM111147A00001|70:31|500001|11|0|0|20180621|20180621|60000";
    private static final String CONTROL_LINE = "CTL|12|SCO1|12SCO1001020180621|20180621|2|60000|0|0|60000";

    private static final String EMS_PAYMENT_REF_NO_2 = "12LM111147A00002";
    private static final String CLIENT_PAYMENT_LINE_2 = "CLN|12|SCO1|12LM111147A00002|LM111147A|Surname37|||||||||SCO|901487||11212200||||1|60000|Personal Bank Acc|||||||LM111147A SSS BSG|||1000000030||500001|03||20180627|GBP||||||20180621|20180621|99||||||||||||||||";
    private static final String COMPONENT_LINE_2_1 = "CMP|12|SCO1|12LM111147A00002|70:31|500001|21|0|0|20180621|20180621|40000";
    private static final String COMPONENT_LINE_2_2 = "CMP|12|SCO1|12LM111147A00002|70:31|500001|22|0|0|20180621|20180621|20000";

    private static final String EMS_PAYMENT_REF_NO_3 = "12LM111147A00003";
    private static final String CLIENT_PAYMENT_LINE_3 = "CLN|12|SCO1|12LM111147A00003|LM111147A|Surname37|||||||||SCO|901487||11212200||||1|60000|Personal Bank Acc|||||||LM111147A SSS BSG|||1000000030||500001|03||20180627|GBP||||||20180621|20180621|99||||||||||||||||";
    private static final String COMPONENT_LINE_3_1 = "CMP|12|SCO1|12LM111147A00003|70:31|500001|31|0|0|20180621|20180621|30000";
    private static final String COMPONENT_LINE_3_2 = "CMP|12|SCO1|12LM111147A00003|70:31|500001|32|0|0|20180621|20180621|20000";
    private static final String COMPONENT_LINE_3_3 = "CMP|12|SCO1|12LM111147A00003|70:31|500001|33|0|0|20180621|20180621|10000";

    private static final String CONTROL_LINE_THREE_PAYMENTS = "CTL|12|SCO1|12SCO1001020180621|20180621|2|180000|0|0|180000";

    private static final String TPP_LINE = "TPP|12|SCO1|12LM111147A00001|70:31|500001|41010|0|0|20180621|20180621|60000";
    private static final String RVY_LINE = "RVY|12|SCO1|12LM111147A00001|70:31|500001|41010|0|0|20180621|20180621|60000";
    private static final String UNKNOWN_TYPE_LINE = "XXX|12|SCO1|12LM111147A00001|70:31|500001|41010|0|0|20180621|20180621|60000";

    @BeforeEach
    void setupParser() {
        cpsParser = new CpsParser();
    }

    @Nested
    @DisplayName("Parse Record")
    class ParseRecord {

        private Records records;

        @BeforeEach
        void setupRecords() {
            records = new Records();
        }

        @Test
        @DisplayName("When parsing a control record line then a control record is added")
        void whenParsingControlRecordLineThenControlRecordIsAdded() throws FileParseException, RecordParseException {

            cpsParser.parseRecord(CONTROL_LINE, records, 1);

            assertThat(
                    records.controlRecords.get(0),
                    is(CpsControlRecord.fromFields(CONTROL_LINE.split(FIELD_SEPARATOR),1))
            );
        }

        @Test
        @DisplayName("When parsing a client payment record line then a client payment record is added")
        void whenParsingClientPaymentRecordLineThenClientPaymentRecordIsAdded() throws FileParseException, RecordParseException {

            cpsParser.parseRecord(CLIENT_PAYMENT_LINE_1, records, 1);

            assertThat(
                    records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_1),
                    is(CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 1))
            );
        }

        @Test
        @DisplayName("When parsing a component record line then a component record is added")
        void whenParsingComponentRecordLineThenComponentRecordIsAdded() throws FileParseException, RecordParseException {

            cpsParser.parseRecord(COMPONENT_LINE_1_1, records, 1);

            assertThat(
                    records.componentRecords.get(0),
                    is(CpsComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), 1))
            );
        }

        @Test
        @DisplayName("When parsing a TPP line then exception is thrown")
        void whenParsingTPPLineThenExceptionIsThrown() {

            assertThrows(
                    FileParseException.class,
                    () -> cpsParser.parseRecord(TPP_LINE, records, 1)
            );
        }

        @Test
        @DisplayName("When parsing a RVY line then exception is thrown")
        void whenParsingRVYLineThenExceptionIsThrown() {

            assertThrows(
                    FileParseException.class,
                    () -> cpsParser.parseRecord(RVY_LINE, records, 1)
            );
        }

        @Test
        @DisplayName("When parsing a line with unknown type exception is thrown")
        void whenParsingLineWithUnknownTypeThenExceptionIsThrown() {

            assertThrows(
                    FileParseException.class,
                    () -> cpsParser.parseRecord(UNKNOWN_TYPE_LINE, records, 1)
            );
        }
    }

    @Nested
    @DisplayName("Parse Records")
    class ParseRecords {

        @Test
        @DisplayName("When parsing file with single client payment record and single component then correct records are returned")
        void whenParsingFileWithSingleClientPaymentRecordAndSingleComponentThenCorrectRecordsAreReturned() throws FileParseException, RecordParseException {

            val lines =
                    "" +
                            CLIENT_PAYMENT_LINE_1 + "\n" +
                            COMPONENT_LINE_1_1 + "\n" +
                            CONTROL_LINE + "\n";

            val inputStream = new ByteArrayInputStream(lines.getBytes());

            var lineNum = 1L;

            CpsClientPaymentRecord clientPaymentRecord = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), lineNum++);
            CpsComponentRecord componentRecord = CpsComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), lineNum++);
            CpsControlRecord controlRecord = CpsControlRecord.fromFields(CONTROL_LINE.split(FIELD_SEPARATOR), lineNum);

            val records = cpsParser.parseRecords(inputStream);

            assertThat(records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_1), is(clientPaymentRecord));
            assertThat(records.componentRecords.get(0), is(componentRecord));
            assertThat(records.controlRecords.get(0), is(controlRecord));
        }

        @Test
        @DisplayName("When parsing file with blank lines then correct records are returned")
        void whenParsingFileWithBlankLinesThenCorrectRecordsAreReturned() throws FileParseException, RecordParseException {


            val lines =
                    "\n" +
                            CLIENT_PAYMENT_LINE_1 + "\n" +
                            "   \n" +
                            COMPONENT_LINE_1_1 + "\n" +
                            "    \n" +
                            CONTROL_LINE + "\n" +
                            "\n\n";


            val inputStream = new ByteArrayInputStream(lines.getBytes());
            var lineNum = 1L;

            lineNum++;
            CpsClientPaymentRecord clientPaymentRecord = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), lineNum++);
            lineNum++;
            CpsComponentRecord componentRecord = CpsComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), lineNum++);
            lineNum++;
            CpsControlRecord controlRecord = CpsControlRecord.fromFields(CONTROL_LINE.split(FIELD_SEPARATOR), lineNum);

            val records = cpsParser.parseRecords(inputStream);

            assertThat(records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_1), is(clientPaymentRecord));
            assertThat(records.componentRecords.get(0), is(componentRecord));
            assertThat(records.controlRecords.get(0), is(controlRecord));
        }

        @Test
        @DisplayName("When parsing file with lines of unhandled types then exception is thrown")
        void whenParsingFileWithLinesOfUnhandledTypesThenExceptionIsThrown() {

            val lines =
                    "" +
                            CLIENT_PAYMENT_LINE_1 + "\n" +
                            TPP_LINE + "\n" +
                            COMPONENT_LINE_1_1 + "\n" +
                            RVY_LINE + "\n" +
                            UNKNOWN_TYPE_LINE + "\n" +
                            CONTROL_LINE + "\n";

            val inputStream = new ByteArrayInputStream(lines.getBytes());

            assertThrows(
                    FileParseException.class,
                    () -> cpsParser.parseRecords(inputStream)
            );
        }

        @Test
        @DisplayName("When parsing file with multiple client payment records and multiple components then correct records are returned")
        void whenParsingFileWithMultipleClientPaymentRecordsAndMultipleComponentThenCorrectRecordsAreReturned() throws FileParseException, RecordParseException {

            val lines =
                    "" +
                            CLIENT_PAYMENT_LINE_1 + "\n" +
                            COMPONENT_LINE_1_1 + "\n" +
                            CLIENT_PAYMENT_LINE_2 + "\n" +
                            COMPONENT_LINE_2_1 + "\n" +
                            COMPONENT_LINE_2_2 + "\n" +
                            CLIENT_PAYMENT_LINE_3 + "\n" +
                            COMPONENT_LINE_3_1 + "\n" +
                            COMPONENT_LINE_3_2 + "\n" +
                            COMPONENT_LINE_3_3 + "\n" +
                            CONTROL_LINE + "\n";

            val inputStream = new ByteArrayInputStream(lines.getBytes());
            var lineNum = 1L;

            CpsClientPaymentRecord clientPaymentRecord1 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), lineNum++);

            CpsComponentRecord componentRecord11 = CpsComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), lineNum++);

            CpsClientPaymentRecord clientPaymentRecord2 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_2.split(FIELD_SEPARATOR), lineNum++);

            CpsComponentRecord componentRecord21 = CpsComponentRecord.fromFields(COMPONENT_LINE_2_1.split(FIELD_SEPARATOR), lineNum++);

            CpsComponentRecord componentRecord22 = CpsComponentRecord.fromFields(COMPONENT_LINE_2_2.split(FIELD_SEPARATOR), lineNum++);

            CpsClientPaymentRecord clientPaymentRecord3 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_3.split(FIELD_SEPARATOR), lineNum++);

            CpsComponentRecord componentRecord31 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_1.split(FIELD_SEPARATOR), lineNum++);

            CpsComponentRecord componentRecord32 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_2.split(FIELD_SEPARATOR), lineNum++);

            CpsComponentRecord componentRecord33 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_3.split(FIELD_SEPARATOR), lineNum++);

            CpsControlRecord controlRecord = CpsControlRecord.fromFields(CONTROL_LINE.split(FIELD_SEPARATOR), lineNum);

            val records = cpsParser.parseRecords(inputStream);

            assertThat(records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_1), is(clientPaymentRecord1));
            assertThat(records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_2), is(clientPaymentRecord2));
            assertThat(records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_3), is(clientPaymentRecord3));

            assertThat(records.componentRecords,
                    containsInAnyOrder(
                            componentRecord11,
                            componentRecord21,
                            componentRecord22,
                            componentRecord31,
                            componentRecord32,
                            componentRecord33
                    ));

            assertThat(records.controlRecords.get(0), is(controlRecord));
        }
    }

    @Nested
    @DisplayName("Link Records")
    class LinkRecords {

        @Test
        @DisplayName("When linking with single client payment record and single component then correct link is made")
        void whenLinkingWithSingleClientPaymentRecordAndSingleComponentThenCorrectLinkIsMade() throws FileParseException, RecordParseException {

            var records = new Records();
            val clientPayment = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 99);
            val component = CpsComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), 100);
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_1, clientPayment);
            records.componentRecords.add(component);

            records = cpsParser.linkRecords(records);

            assertThat(records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_1).getComponents(), contains(component));
        }

        @Test
        @DisplayName("When linking with single client payment record and mismatched single component then exception is thrown")
        void whenLinkingWithSingleClientPaymentRecordAndMismatchedComponentThenExceptionIsThrown() throws RecordParseException, FileParseException {

            var records = new Records();
            val clientPayment = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 99);
            val component = CpsComponentRecord.fromFields(COMPONENT_LINE_2_1.split(FIELD_SEPARATOR), 100);
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_1, clientPayment);
            records.componentRecords.add(component);

            assertThrows(
                    FileParseException.class,
                    () -> cpsParser.linkRecords(records)
            );
        }

        @Test
        @DisplayName("When linking with multiple client payment records and multiple components then correct links are made")
        void whenLinkingWithMultipleClientPaymentRecordsAndMultipleComponentsThenCorrectLinksAreMade() throws FileParseException, RecordParseException {

            var records = new Records();
            val clientPayment1 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 100);
            val component11 = CpsComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), 101);
            val clientPayment2 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_2.split(FIELD_SEPARATOR), 102);
            val component21 = CpsComponentRecord.fromFields(COMPONENT_LINE_2_1.split(FIELD_SEPARATOR), 103);
            val component22 = CpsComponentRecord.fromFields(COMPONENT_LINE_2_2.split(FIELD_SEPARATOR), 104);
            val clientPayment3 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_3.split(FIELD_SEPARATOR), 105);
            val component31 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_1.split(FIELD_SEPARATOR), 106);
            val component32 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_2.split(FIELD_SEPARATOR), 107);
            val component33 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_3.split(FIELD_SEPARATOR), 108);

            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_1, clientPayment1);
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_2, clientPayment2);
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_3, clientPayment3);
            records.componentRecords.add(component32);
            records.componentRecords.add(component31);
            records.componentRecords.add(component33);
            records.componentRecords.add(component11);
            records.componentRecords.add(component21);
            records.componentRecords.add(component22);

            records = cpsParser.linkRecords(records);

            assertThat(
                    records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_1).getComponents(),
                    contains(component11)
            );
            assertThat(
                    records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_2).getComponents(),
                    containsInAnyOrder(component21, component22)
            );
            assertThat(
                    records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_3).getComponents(),
                    containsInAnyOrder(component31, component32, component33)
            );
        }
    }

    @Nested
    @DisplayName("Validate Records")
    class ValidateRecords {

        @Test
        @DisplayName("When validating a valid set of records then no exception is thrown")
        void whenValidatingAValidSetOfRecordsThenNoExceptionIsThrown() throws FileParseException, RecordParseException {

            var records = new Records();
            val controlRecord = CpsControlRecord.fromFields(CONTROL_LINE_THREE_PAYMENTS.split(FIELD_SEPARATOR), 99);
            val clientPayment1 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 100);
            val component11 = CpsComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), 101);
            val clientPayment2 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_2.split(FIELD_SEPARATOR), 102);
            val component21 = CpsComponentRecord.fromFields(COMPONENT_LINE_2_1.split(FIELD_SEPARATOR), 103);
            val component22 = CpsComponentRecord.fromFields(COMPONENT_LINE_2_2.split(FIELD_SEPARATOR), 104);
            val clientPayment3 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_3.split(FIELD_SEPARATOR), 105);
            val component31 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_1.split(FIELD_SEPARATOR), 106);
            val component32 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_2.split(FIELD_SEPARATOR), 107);
            val component33 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_3.split(FIELD_SEPARATOR), 108);

            records.controlRecords.add(controlRecord);
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_1, clientPayment1);
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_2, clientPayment2);
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_3, clientPayment3);
            clientPayment1.getComponents().add(component11);
            clientPayment2.getComponents().addAll(asList(component21, component22));
            clientPayment3.getComponents().addAll(asList(component31, component32, component33));

            cpsParser.validateRecords(records);
        }

        @Test
        @DisplayName("When validating a set of records with a missing component then an exception is thrown")
        void whenValidatingASetOfRecordsWithAMissingComponentThenAnExceptionIsThrown() throws FileParseException, RecordParseException {

            var records = new Records();
            val controlRecord = CpsControlRecord.fromFields(CONTROL_LINE_THREE_PAYMENTS.split(FIELD_SEPARATOR), 99);
            val clientPayment1 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 100);
            val component11 = CpsComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), 101);
            val clientPayment2 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_2.split(FIELD_SEPARATOR), 102);
            val component21 = CpsComponentRecord.fromFields(COMPONENT_LINE_2_1.split(FIELD_SEPARATOR), 103);
            val component22 = CpsComponentRecord.fromFields(COMPONENT_LINE_2_2.split(FIELD_SEPARATOR), 104);
            val clientPayment3 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_3.split(FIELD_SEPARATOR), 105);
            val component31 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_1.split(FIELD_SEPARATOR), 106);
            val component33 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_3.split(FIELD_SEPARATOR), 107);

            records.controlRecords.add(controlRecord);
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_1, clientPayment1);
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_2, clientPayment2);
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_3, clientPayment3);
            clientPayment1.getComponents().add(component11);
            clientPayment2.getComponents().addAll(asList(component21, component22));
            clientPayment3.getComponents().addAll(asList(component31, component33));

            assertThrows(
                    FileParseException.class,
                    () -> cpsParser.validateRecords(records)
            );
        }

        @Test
        @DisplayName("When validating a set of records with incorrect hash net amount then an exception is thrown")
        void whenValidatingASetOfRecordsWithInvalidHashNetAmountThenAnExceptionIsThrown() throws FileParseException, RecordParseException {

            var records = new Records();
            val controlRecord = CpsControlRecord.fromFields(CONTROL_LINE.split(FIELD_SEPARATOR), 99);
            val clientPayment1 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 100);
            val component11 = CpsComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), 101);
            val clientPayment2 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_2.split(FIELD_SEPARATOR), 102);
            val component21 = CpsComponentRecord.fromFields(COMPONENT_LINE_2_1.split(FIELD_SEPARATOR), 103);
            val component22 = CpsComponentRecord.fromFields(COMPONENT_LINE_2_2.split(FIELD_SEPARATOR), 104);
            val clientPayment3 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_3.split(FIELD_SEPARATOR), 105);
            val component31 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_1.split(FIELD_SEPARATOR), 106);
            val component32 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_2.split(FIELD_SEPARATOR), 107);
            val component33 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_3.split(FIELD_SEPARATOR), 108);

            records.controlRecords.add(controlRecord);
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_1, clientPayment1);
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_2, clientPayment2);
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_3, clientPayment3);
            clientPayment1.getComponents().add(component11);
            clientPayment2.getComponents().addAll(asList(component21, component22));
            clientPayment3.getComponents().addAll(asList(component31, component32, component33));

            assertThrows(
                    FileParseException.class,
                    () -> cpsParser.validateRecords(records)
            );
        }

        @Test
        @DisplayName("When validating a set of records with no control record then an exception is thrown")
        void whenValidatingASetOfRecordsWithNoControlRecordThenAnExceptionIsThrown() throws FileParseException, RecordParseException {

            var records = new Records();
            val clientPayment1 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 100);
            val component11 = CpsComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), 101);
            val clientPayment2 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_2.split(FIELD_SEPARATOR), 102);
            val component21 = CpsComponentRecord.fromFields(COMPONENT_LINE_2_1.split(FIELD_SEPARATOR), 103);
            val component22 = CpsComponentRecord.fromFields(COMPONENT_LINE_2_2.split(FIELD_SEPARATOR), 104);
            val clientPayment3 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_3.split(FIELD_SEPARATOR), 105);
            val component31 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_1.split(FIELD_SEPARATOR), 106);
            val component33 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_3.split(FIELD_SEPARATOR), 107);

            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_1, clientPayment1);
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_2, clientPayment2);
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_3, clientPayment3);
            clientPayment1.getComponents().add(component11);
            clientPayment2.getComponents().addAll(asList(component21, component22));
            clientPayment3.getComponents().addAll(asList(component31, component33));

            assertThrows(
                    FileParseException.class,
                    () -> cpsParser.validateRecords(records)
            );
        }

        @Test
        @DisplayName("When validating a set of records with multiple control records then an exception is thrown")
        void whenValidatingASetOfRecordsWithMultipleControlRecordsThenAnExceptionIsThrown() throws FileParseException, RecordParseException {

            var records = new Records();
            val controlRecord1 = CpsControlRecord.fromFields(CONTROL_LINE.split(FIELD_SEPARATOR), 99);
            val controlRecord2 = CpsControlRecord.fromFields(CONTROL_LINE_THREE_PAYMENTS.split(FIELD_SEPARATOR), 99);
            val clientPayment1 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 100);
            val component11 = CpsComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), 101);
            val clientPayment2 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_2.split(FIELD_SEPARATOR), 102);
            val component21 = CpsComponentRecord.fromFields(COMPONENT_LINE_2_1.split(FIELD_SEPARATOR), 103);
            val component22 = CpsComponentRecord.fromFields(COMPONENT_LINE_2_2.split(FIELD_SEPARATOR), 104);
            val clientPayment3 = CpsClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_3.split(FIELD_SEPARATOR), 105);
            val component31 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_1.split(FIELD_SEPARATOR), 106);
            val component33 = CpsComponentRecord.fromFields(COMPONENT_LINE_3_3.split(FIELD_SEPARATOR), 107);

            records.controlRecords.addAll(asList(controlRecord1, controlRecord2));
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_1, clientPayment1);
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_2, clientPayment2);
            records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_3, clientPayment3);
            clientPayment1.getComponents().add(component11);
            clientPayment2.getComponents().addAll(asList(component21, component22));
            clientPayment3.getComponents().addAll(asList(component31, component33));

            assertThrows(
                    FileParseException.class,
                    () -> cpsParser.validateRecords(records)
            );
        }
    }
}
