package gov.scot.payments.customer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import gov.scot.payments.AbstractKafkaTest;
import gov.scot.payments.MockWebSecurityConfiguration;
import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.common.kafka.KafkaNullConfiguration;
import gov.scot.payments.customer.spring.CustomerServiceBindings;
import gov.scot.payments.model.customer.Customer;
import gov.scot.payments.WithUser;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.aws.autoconfigure.context.ContextCredentialsAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.jdbc.AmazonRdsDatabaseAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.messaging.MessagingAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.MediaType;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.messaging.MessageChannel;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureDataJpa
@AutoConfigureWebMvc
@AutoConfigureMockMvc
@DBRider
@EnableAutoConfiguration(exclude = {MessagingAutoConfiguration.class
        , ContextStackAutoConfiguration.class
        , ContextResourceLoaderAutoConfiguration.class
        , ContextRegionProviderAutoConfiguration.class
        , ContextCredentialsAutoConfiguration.class
        , AmazonRdsDatabaseAutoConfiguration.class
        , UserDetailsServiceAutoConfiguration.class
})
@EmbeddedKafka(partitions = 1
        ,topics = {"test-customerCreatedEvents","test-customerUpdatedEvents","test-customerDeletedEvents"})
@SpringBootTest(classes = CustomerServiceIntegrationTest.TestConfiguration.class, properties = {
        "debug=true",
        "payments.environment=test"
})
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@Tag("kafka")
@Tag("db")
@WithUser
public class CustomerServiceIntegrationTest extends AbstractKafkaTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    @DataSet("customers.yml")
    public void testGetAllCustomers() throws Exception {
        mvc.perform(get("/customers"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(6)));
     }

    @Test
    @DataSet("customers.yml")
    @WithUser(entityType = "customer", entityId = "customer_1_2")
    public void testGetAllCustomersWithAcl() throws Exception {
        mvc.perform(get("/customers"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    @DataSet("customers.yml")
    public void testGetCustomer() throws Exception {

        String response = "{\"createdAt\":\"2019-01-01T15:23:54\",\"createdBy\":\"test_user2\",\"id\":\"customer_4_1\",\"name\":\"Customer Test 4\"}";
        mvc.perform(get("/customers/customer_4_1"))
                .andExpect(status().isOk())
                .andExpect(content().json(response));
    }

    @Test
    @DataSet("customers.yml")
    @WithUser(entityType = "customer", entityId = "customer_1_2")
    public void testGetCustomerWithAcl() throws Exception {
        String response = "{\"createdAt\":\"2019-01-01T15:23:54\",\"createdBy\":\"test_user2\",\"id\":\"customer_4_1\",\"name\":\"Customer Test 4\"}";
        mvc.perform(get("/customers/customer_4_1"))
                .andExpect(status().isForbidden());
    }

    @Test
    @DataSet("customers.yml")
    public void testAddCustomer() throws Exception {

        var customerObj = Customer.builder()
                .id("customer_6_1")
                .name("test_2")
                .createdBy("testuser")
                .createdAt(LocalDateTime.parse("2020-01-01T10:10:10"))
                .build();
        var json = objectMapper.writeValueAsString(customerObj);

        mvc.perform(post("/customers")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        sleep();

        verifyEvents("test-customerCreatedEvents", list -> assertEquals(1, list.size()));

    }

    @Test
    @DataSet("customers.yml")
    @WithUser(entityType = "customer", entityId = "customer_1_2")
    public void testAddCustomerNotAdmin() throws Exception {
        var customerObj = Customer.builder()
                .id("customer_6_1")
                .name("test_2")
                .createdBy("testuser")
                .createdAt(LocalDateTime.parse("2020-01-01T10:10:10"))
                .build();
        var json = objectMapper.writeValueAsString(customerObj);

        mvc.perform(post("/customers")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @DataSet("customers.yml")
    public void testPutCustomer() throws Exception {


        mvc.perform(get("/customers/customer_1_1"))
                .andExpect(status().isOk());

        var customerObj = Customer.builder()
                .id("customer_1_1")
                .name("test_2")
                .createdBy("testuser_changed")
                .createdAt(LocalDateTime.parse("2023-01-01T10:10:10"))
                .build();
        var json = objectMapper.writeValueAsString(customerObj);

        mvc.perform(put("/customers")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        verifyEvents("test-customerUpdatedEvents", list -> assertEquals(1, list.size()));

    }

    @Test
    @DataSet("customers.yml")
    @WithUser(entityType = "customer", entityId = "customer_1_2")
    public void testPutCustomerNotAdmin() throws Exception {
        var customerObj = Customer.builder()
                .id("customer_1_1")
                .name("test_2")
                .createdBy("testuser_changed")
                .createdAt(LocalDateTime.parse("2023-01-01T10:10:10"))
                .build();
        var json = objectMapper.writeValueAsString(customerObj);

        mvc.perform(put("/customers")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @DataSet("customers.yml")
    public void testDeleteCustomer() throws Exception {

        mvc.perform(delete("/customers/customer_2_1")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        verifyEvents("test-customerDeletedEvents", list -> assertEquals(1, list.size()));
    }

    @Test
    @DataSet("customers.yml")
    @WithUser(entityType = "customer", entityId = "customer_1_2")
    public void testDeleteCustomerNotAdmin() throws Exception {
        mvc.perform(delete("/customers/customer_2_1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());

    }


    @Configuration
    @EnableJpaRepositories(basePackageClasses = {CustomerRepository.class} )
    @EntityScan(basePackageClasses = {Customer.class} )
    @Import({OptionalResponseControllerAdvice.class, KafkaNullConfiguration.class, MockWebSecurityConfiguration.class})
    @EnableBinding({ CustomerServiceBindings.class})
    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    public static class TestConfiguration extends AbstractKafkaTest.TestConfiguration {

        @Bean
        public CustomerService customerServiceTest(@Qualifier("customer-created-events") MessageChannel customerCreatedEventTopic,
                                                   @Qualifier("customer-updated-events")MessageChannel customerUpdatedEventsTopic,
                                                   @Qualifier("customer-deleted-events") MessageChannel customerDeletedEventsTopic,
                                                   CustomerRepository customerRepository,
                                                   ServiceRepository serviceRepository){
            return new CustomerService(customerCreatedEventTopic, customerUpdatedEventsTopic, customerDeletedEventsTopic, customerRepository,serviceRepository);
        }

    }

}
