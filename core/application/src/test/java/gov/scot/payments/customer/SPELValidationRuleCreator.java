package gov.scot.payments.customer;

import org.javamoney.moneta.Money;

public class SPELValidationRuleCreator {

    public static String lessThanMoneyRule(String fieldName, Money amount){
        return fieldName + ".isLessThan(T(org.javamoney.moneta.Money).of("+amount.getNumberStripped().toPlainString()+",'"+amount.getCurrency()+"'))";
    }
    public static String greaterThanMoneyRule(String fieldName, Money amount){
        return fieldName + ".isGreaterThan(T(org.javamoney.moneta.Money).of("+amount.getNumberStripped().toPlainString()+",'"+amount.getCurrency()+"'))";
    }

    public static String isBeforeDateRule(String fieldName, String dateStr){
        return fieldName + ".isBefore(T(java.time.LocalDate).parse('"+dateStr+"'))";
    }
    public static String isAfterDateRule(String fieldName, String dateStr){
        return fieldName + ".isAfter(T(java.time.LocalDate).parse('"+dateStr+"'))";
    }

}
