package gov.scot.payments.paymentfile.cps;

import gov.scot.payments.paymentfile.FileParseException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static gov.scot.payments.paymentfile.cps.CpsParser.FIELD_SEPARATOR;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Component record tests")
class CpsComponentRecordTest {

    @Nested
    @DisplayName("from fields")
    class FromFields {

        @Test
        @DisplayName("with example component line then record is returned")
        void withExampleComponentLineThenRecordIsReturned() throws FileParseException, RecordParseException {

            var line = "CMP|12|SCO1|12LM111147A00001|70:31|500001|41010|0|0|20180621|20180621|60000";
            var fields = line.split(FIELD_SEPARATOR);

            var record = CpsComponentRecord.fromFields(fields, 1);

            assertThat(record.getEntitlementManagementSystem(), is("12"));
            assertThat(record.getServiceIdentifier(), is("SCO1"));
            assertThat(record.getEmsPaymentRefNo(), is("12LM111147A00001"));
            assertThat(record.getExecutiveAgency(), is("70:31"));
            assertThat(record.getIssuingOffice(), is("500001"));
            assertThat(record.getBenefitCode(), is("41010"));
            assertThat(record.getBenefitCodeStartDate(), is(LocalDate.of(2018, 6, 21)));
            assertThat(record.getBenefitCodeEndDate(), is(LocalDate.of(2018, 6, 21)));
            assertThat(record.getComponentAmount(), is(new BigDecimal("600.00")));
        }

        @Test
        @DisplayName("with invalid benefit start date then exception is thrown")
        void withInvalidBenefitCodeStartDateThenExceptionIsThrown() {

            var line = "CMP|12|SCO1|12LM111147A00001|70:31|500001|41010|0|0|20180621X|20180621|60000";
            var fields = line.split(FIELD_SEPARATOR);

            assertThrows(
                    RecordParseException.class,
                    () -> CpsComponentRecord.fromFields(fields, 1)
            );
        }

        @Test
        @DisplayName("with invalid benefit end date then exception is thrown")
        void withInvalidBenefitCodeEndDateThenExceptionIsThrown() {

            var line = "CMP|12|SCO1|12LM111147A00001|70:31|500001|41010|0|0|20180621|20180621X|60000";
            var fields = line.split(FIELD_SEPARATOR);

            assertThrows(
                    RecordParseException.class,
                    () -> CpsComponentRecord.fromFields(fields, 1)
            );
        }

        @Test
        @DisplayName("with invalid component amount then exception is thrown")
        void withInvalidComponentAmountThenExceptionIsThrown() {

            var line = "CMP|12|SCO1|12LM111147A00001|70:31|500001|41010|0|0|20180621|20180621|60000X";
            var fields = line.split(FIELD_SEPARATOR);

            assertThrows(
                    RecordParseException.class,
                    () -> CpsComponentRecord.fromFields(fields, 1)
            );
        }
    }
}
