package gov.scot.payments.paymentfile.cps;

import gov.scot.payments.paymentfile.FileParseException;
import gov.scot.payments.model.paymentfile.PaymentFile;
import gov.scot.payments.model.paymentinstruction.AccountNumber;
import gov.scot.payments.model.paymentinstruction.InvalidPaymentFieldException;
import gov.scot.payments.model.paymentinstruction.SortCode;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("CPS payment file translator")
class CpsPaymentFileTranslatorTest {

    @Test
    @DisplayName("generates correct payment instructions from the example file")
    void generatePaymentInstructionsFromExampleFile() throws IOException, FileParseException, InvalidPaymentFieldException, URISyntaxException {

        var paymentFile = PaymentFile.builder()
                .service("service")
                .name("file1")
                .type("CPS")
                .url(new URI("s3://path/to/file"))
                .build();

        var path = getClass().getClassLoader().getResource("cps_payment_files/example_payment_file.csv").getFile();
        try (var inputStream = new FileInputStream(path)) {

            CpsParser cpsParser = new CpsParser();
            CpsPaymentFileTranslator translator = new CpsPaymentFileTranslator(cpsParser);

            var paymentInstructions = translator.generatePaymentInstructionsFromFile(paymentFile,inputStream);

            assertThat(paymentInstructions.size(), is(1));

            var paymentInstruction = paymentInstructions.get(0);
            assertThat(paymentInstruction.getRecipient().getAccountNumber(), is(AccountNumber.fromString("11212200")));
            assertThat(paymentInstruction.getRecipient().getSortCode(), is(SortCode.fromString("901487")));
            assertThat(paymentInstruction.getRecipient().getRef(), is("LM111147A SSS BSG"));
            assertThat(paymentInstruction.getRecipient().getName(), is("Personal Bank Acc"));
            assertThat(paymentInstruction.getTargetAmount(), is(Money.of(600, "GBP")));
            assertThat(paymentInstruction.getTimestamp(), is(greaterThan(LocalDateTime.now().minusMinutes(1))));
            assertThat(paymentInstruction.getTimestamp(), is(lessThanOrEqualTo(LocalDateTime.now())));
            assertThat(paymentInstruction.getEarliestTargetSettlementDate().toString(), is("2018-06-27"));
            assertThat(paymentInstruction.getLatestTargetSettlementDate().toString(), is("2018-06-27"));
            assertThat(paymentInstruction.getKvRef().get("EmsPaymentRefNo"), is("12LM111147A00001"));
            assertThat(paymentInstruction.getKvRef().get("Interface Reference"), is("12SCO1001020180621"));
            assertThat(paymentInstruction.getKvRef().get("Date of file"), is("2018-06-21"));
            assertEquals("service", paymentInstruction.getService());
            assertEquals("file1", paymentInstruction.getPaymentFile());
            assertEquals(paymentFile.getTimestamp(), paymentInstruction.getTimestamp());
        }
    }

    @Test
    @DisplayName("generates correct payment instructions from file with three client payments")
    void generatePaymentInstructionsFromThreePaymentsFile() throws IOException, FileParseException, InvalidPaymentFieldException, URISyntaxException {
        var paymentFile = PaymentFile.builder()
                .service("service")
                .name("file1")
                .type("CPS")
                .url(new URI("s3://path/to/file"))
                .build();

        var path = getClass().getClassLoader().getResource("cps_payment_files/three_payment_file.csv").getFile();
        try (var inputStream = new FileInputStream(path)) {

            CpsParser cpsParser = new CpsParser();
            CpsPaymentFileTranslator translator = new CpsPaymentFileTranslator(cpsParser);

            var paymentInstructions = translator.generatePaymentInstructionsFromFile(paymentFile,inputStream);

            assertThat(paymentInstructions.size(), is(3));

            var paymentInstruction = paymentInstructions.get(0);
            assertThat(paymentInstruction.getRecipient().getAccountNumber(), is(AccountNumber.fromString("11212201")));
            assertThat(paymentInstruction.getRecipient().getSortCode(), is(SortCode.fromString("901487")));
            assertThat(paymentInstruction.getRecipient().getRef(), is("LM111147A SSS BSG"));
            assertThat(paymentInstruction.getRecipient().getName(), is("Personal Bank Acc"));
            assertThat(paymentInstruction.getTargetAmount(), is(Money.of(600, "GBP")));
            assertThat(paymentInstruction.getTimestamp(), is(greaterThan(LocalDateTime.now().minusMinutes(1))));
            assertThat(paymentInstruction.getTimestamp(), is(lessThanOrEqualTo(LocalDateTime.now())));
            assertThat(paymentInstruction.getEarliestTargetSettlementDate().toString(), is("2018-06-27"));
            assertThat(paymentInstruction.getLatestTargetSettlementDate().toString(), is("2018-06-27"));
            assertThat(paymentInstruction.getKvRef().get("EmsPaymentRefNo"), is("12LM111147A00001"));
            assertThat(paymentInstruction.getKvRef().get("Interface Reference"), is("12SCO1001020180621"));
            assertThat(paymentInstruction.getKvRef().get("Date of file"), is("2018-06-21"));
            assertEquals("service", paymentInstruction.getService());
            assertEquals("file1", paymentInstruction.getPaymentFile());
            assertEquals(paymentFile.getTimestamp(), paymentInstruction.getTimestamp());

            paymentInstruction = paymentInstructions.get(1);
            assertThat(paymentInstruction.getRecipient().getAccountNumber(), is(AccountNumber.fromString("11212202")));
            assertThat(paymentInstruction.getRecipient().getSortCode(), is(SortCode.fromString("901487")));
            assertThat(paymentInstruction.getRecipient().getRef(), is("LM111147A SSS BSG"));
            assertThat(paymentInstruction.getRecipient().getName(), is("Personal Bank Acc"));
            assertThat(paymentInstruction.getTargetAmount(), is(Money.of(300, "GBP")));
            assertThat(paymentInstruction.getTimestamp(), is(greaterThan(LocalDateTime.now().minusMinutes(1))));
            assertThat(paymentInstruction.getTimestamp(), is(lessThanOrEqualTo(LocalDateTime.now())));
            assertThat(paymentInstruction.getEarliestTargetSettlementDate().toString(), is("2018-06-27"));
            assertThat(paymentInstruction.getLatestTargetSettlementDate().toString(), is("2018-06-27"));
            assertThat(paymentInstruction.getKvRef().get("EmsPaymentRefNo"), is("12LM111147A00002"));
            assertThat(paymentInstruction.getKvRef().get("Interface Reference"), is("12SCO1001020180621"));
            assertThat(paymentInstruction.getKvRef().get("Date of file"), is("2018-06-21"));
            assertEquals("service", paymentInstruction.getService());
            assertEquals("file1", paymentInstruction.getPaymentFile());
            assertEquals(paymentFile.getTimestamp(), paymentInstruction.getTimestamp());

            paymentInstruction = paymentInstructions.get(2);
            assertThat(paymentInstruction.getRecipient().getAccountNumber(), is(AccountNumber.fromString("11212203")));
            assertThat(paymentInstruction.getRecipient().getSortCode(), is(SortCode.fromString("901487")));
            assertThat(paymentInstruction.getRecipient().getRef(), is("LM111147A SSS BSG"));
            assertThat(paymentInstruction.getRecipient().getName(), is("Personal Bank Acc"));
            assertThat(paymentInstruction.getTargetAmount(), is(Money.of(600, "GBP")));
            assertThat(paymentInstruction.getTimestamp(), is(greaterThan(LocalDateTime.now().minusMinutes(1))));
            assertThat(paymentInstruction.getTimestamp(), is(lessThanOrEqualTo(LocalDateTime.now())));
            assertThat(paymentInstruction.getEarliestTargetSettlementDate().toString(), is("2018-06-27"));
            assertThat(paymentInstruction.getLatestTargetSettlementDate().toString(), is("2018-06-27"));
            assertThat(paymentInstruction.getKvRef().get("EmsPaymentRefNo"), is("12LM111147A00003"));
            assertThat(paymentInstruction.getKvRef().get("Interface Reference"), is("12SCO1001020180621"));
            assertThat(paymentInstruction.getKvRef().get("Date of file"), is("2018-06-21"));
            assertEquals("service", paymentInstruction.getService());
            assertEquals("file1", paymentInstruction.getPaymentFile());
            assertEquals(paymentFile.getTimestamp(), paymentInstruction.getTimestamp());
        }
    }

    @Test
    @DisplayName("reading invalid file throws an exception")
    void readingInvalidPaymentsFileThrowsException() throws IOException, URISyntaxException {
        var paymentFile = PaymentFile.builder()
                .service("service")
                .name("file1")
                .type("CPS")
                .url(new URI("s3://path/to/file"))
                .build();

        try (var inputStream = getClass().getClassLoader().getResourceAsStream("cps_payment_files/invalid_payment_file.csv")) {

            CpsParser cpsParser = new CpsParser();
            CpsPaymentFileTranslator translator = new CpsPaymentFileTranslator(cpsParser);

            assertThrows(
                    FileParseException.class,
                    () ->translator.generatePaymentInstructionsFromFile(paymentFile,inputStream)
            );
        }
    }
}