package gov.scot.payments.validation;

import gov.scot.payments.model.customer.PaymentValidationRule;
import gov.scot.payments.model.paymentinstruction.*;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static gov.scot.payments.customer.SPELValidationRuleCreator.*;
import static gov.scot.payments.model.paymentinstruction.PaymentInstruction.GBP_CURRENCY;
import static org.assertj.core.api.Assertions.assertThat;

class PaymentInstructionValidatorTest {

    private final SpelExpressionParser expressionParser = new SpelExpressionParser();
    private final PaymentInstructionValidator validator = new PaymentInstructionValidator(expressionParser);

    private PaymentValidationRule createRuleFromString(String rule){
        return createRuleWithPrecedenceFromString(rule, 0);
    }

    private PaymentValidationRule createRuleWithPrecedenceFromString(String rule, int precedence){
        return PaymentValidationRule.builder()
                .name("test")
                .precedence(precedence)
                .rule(rule)
                .messageTemplate("This failed the rule "+ rule)
                .build();
    }

    @Test
    void invalidEventGeneratedTestForSingleRule() {

        var money = Money.of(new BigDecimal("1048.23"), "GBP");
        PaymentInstruction instruction = createPaymentInstruction(money);
        var rules = List.of(createRuleFromString("targetAmount.getNumber() < 1000.0"));
        Validation validation = validator.validate(instruction, rules);
        Assertions.assertEquals(ValidationStatus.Invalid, validation.getStatus());
        Assertions.assertEquals("This failed the rule targetAmount.getNumber() < 1000.0", validation.getMessage());
    }

    @Test
    void whenInvalidRuleThenThrowsExceptionTest() {

        var money = Money.of(new BigDecimal("1048.23"), "GBP");
        PaymentInstruction instruction = createPaymentInstruction(money);
        var rules = List.of(createRuleFromString("thisWillFailParsing < 1000.0"));

        Validation validation = validator.validate(instruction, rules);
        Assertions.assertEquals(ValidationStatus.Invalid, validation.getStatus());
        assertThat(validation.getMessage()).startsWith("An unexpected error occurred validating rule 'thisWillFailParsing < 1000.0'");
    }

    @Test
    void validEventGeneratedTestForSingleRule() {

        var money = Money.of(new BigDecimal("1048.23"), "GBP");
        PaymentInstruction instruction = createPaymentInstruction(money);
        var lessThanValue = Money.of(new BigDecimal("1000.00"), "GBP");
        var lessThanSPELRule = greaterThanMoneyRule("targetAmount", lessThanValue);
        var rules = List.of(createRuleFromString(lessThanSPELRule));

        Validation validation = validator.validate(instruction, rules);
        Assertions.assertEquals(ValidationStatus.Valid, validation.getStatus());
    }

    @Test
    void validEventGeneratedTestForComplicatedRule() {

        var recipientObject = Recipient.builder()
                .accountNumber(new AccountNumber("20202020"))
                .sortCode(new SortCode("800120"))
                .ref("ILF 12345")
                .name("Dorothy Ltd.")
                .build();

        String latestDate = "2020-01-01";
        var money = Money.of(new BigDecimal("1048.23"), "GBP");
        var instruction = PaymentInstruction.builder()
                .recipient(recipientObject)
                .targetAmount(money)
                .timestamp(LocalDateTime.now())
                .service("")
                .paymentFile("")
                .targetSettlementCurrency(GBP_CURRENCY)
                .latestTargetSettlementDate(LocalDate.parse(latestDate))
                .build();

        var recipientObject2 = Recipient.builder()
                .accountNumber(new AccountNumber("20202020"))
                .sortCode(new SortCode("801020"))
                .ref("ILF 12345")
                .name("Rich")
                .build();

        money = Money.of(new BigDecimal("20048.23"), "GBP");
        var instruction2 = PaymentInstruction.builder()
                .recipient(recipientObject2)
                .targetAmount(money)
                .timestamp(LocalDateTime.now())
                .service("")
                .paymentFile("")
                .targetSettlementCurrency(GBP_CURRENCY)
                .latestTargetSettlementDate(LocalDate.parse(latestDate))
                .build();


        var rule = "targetAmount.getNumber () < 10000 || T(java.util.Arrays).asList('Rich', 'Richard', 'Rachel').contains(recipient.getName())";
        var rule2 = "!(recipient.getName()).contains('Ltd.') && !(recipient.getName()).contains('PLC') ";
        var rules = List.of(createRuleFromString(rule2));

        Validation validation = validator.validate(instruction, rules);
        Assertions.assertEquals(ValidationStatus.Invalid, validation.getStatus());

        validation = validator.validate(instruction2, rules);
        Assertions.assertEquals(ValidationStatus.Valid, validation.getStatus());
    }

    @Test
    void invalidEventGeneratedTestForMultipleRules() {

        var money = Money.of(new BigDecimal("2380.23"), "GBP");
        PaymentInstruction instruction = createPaymentInstruction(money);
        var greaterThanValue = Money.of(new BigDecimal("1000.00"), "GBP");
        var targetCurrencyRule = "targetSettlementCurrency.getCurrencyCode().equals('USD')";
        var greaterThanSPELRule = greaterThanMoneyRule("targetAmount", greaterThanValue);

        var rules = List.of(createRuleFromString(targetCurrencyRule),
                            createRuleFromString(greaterThanSPELRule));

        var usdInstruction = instruction.toBuilder().targetSettlementCurrency(GBP_CURRENCY).build();

        Validation validation = validator.validate(usdInstruction, rules);
        Assertions.assertEquals(ValidationStatus.Invalid, validation.getStatus());
        Assertions.assertEquals("This failed the rule " + targetCurrencyRule, validation.getMessage());
    }

    @Test
    void validEventGeneratedForMultipleRules() {

        var money = Money.of(new BigDecimal("56.11"), "GBP");

        PaymentInstruction instruction = createPaymentInstruction(money);

        var greaterThanValue = Money.of(new BigDecimal("20.00"), "GBP");
        var greaterThanSPELRule = greaterThanMoneyRule("targetAmount", greaterThanValue);

        var lessThanValue = Money.of(new BigDecimal("90.00"), "GBP");
        var lessThanSPELRule = lessThanMoneyRule("targetAmount", lessThanValue);

        var targetCurrencyRule = "targetSettlementCurrency.getCurrencyCode().equals('GBP')";
        var latestDateRule = isBeforeDateRule("latestTargetSettlementDate", "2020-02-02");

        var rules = List.of(createRuleFromString(targetCurrencyRule),
                createRuleFromString(greaterThanSPELRule),
                createRuleFromString(latestDateRule),
                createRuleFromString(lessThanSPELRule));

        Validation validation = validator.validate(instruction, rules);
        Assertions.assertEquals(ValidationStatus.Valid, validation.getStatus());
    }

    @Test
    void testPrecendenceForMultipleRules() {

        var money = Money.of(new BigDecimal("16.11"), "GBP");

        PaymentInstruction instruction = createPaymentInstruction(money);

        var greaterThanValue = Money.of(new BigDecimal("20.00"), "GBP");
        var greaterThanSPELRule = greaterThanMoneyRule("targetAmount", greaterThanValue);

        var greaterThanValue2 = Money.of(new BigDecimal("90.00"), "GBP");
        var greaterThanSPELRule2 = greaterThanMoneyRule("targetAmount", greaterThanValue2);

        var targetCurrencyRule = "targetSettlementCurrency.getCurrencyCode().equals('GBP')";
        var latestDateRule = isBeforeDateRule("latestTargetSettlementDate", "2002-02-02");

        var rules = List.of(createRuleWithPrecedenceFromString(targetCurrencyRule, 1),
                createRuleWithPrecedenceFromString(greaterThanSPELRule,4),
                createRuleWithPrecedenceFromString(greaterThanSPELRule2, 3),
                createRuleWithPrecedenceFromString(latestDateRule, 2));

        Validation validation = validator.validate(instruction, rules);
        Assertions.assertEquals(ValidationStatus.Invalid, validation.getStatus());
        Assertions.assertEquals("This failed the rule " + latestDateRule + ", This failed the rule " + greaterThanSPELRule2 + ", This failed the rule " + greaterThanSPELRule, validation.getMessage());
    }


    @Test
    void invalidEventGeneratedForMultipleRules() {

        var money = Money.of(new BigDecimal("56.11"), "GBP");

        PaymentInstruction instruction = createPaymentInstruction(money);

        var greaterThanValue = Money.of(new BigDecimal("20.00"), "GBP");
        var greaterThanSPELRule = greaterThanMoneyRule("targetAmount", greaterThanValue);

        var lessThanValue = Money.of(new BigDecimal("90.00"), "GBP");
        var lessThanSPELRule = greaterThanMoneyRule("targetAmount", lessThanValue);

        var targetCurrencyRule = "targetSettlementCurrency.getCurrencyCode().equals('GBP')";
        //this rule will fail
        var latestDateRule = isBeforeDateRule("latestTargetSettlementDate", "2018-02-01");

        var rules = List.of(createRuleFromString(targetCurrencyRule),
                createRuleFromString(greaterThanSPELRule),
                createRuleFromString(latestDateRule),
                createRuleFromString(lessThanSPELRule));

        Validation validation = validator.validate(instruction, rules);
        Assertions.assertEquals(ValidationStatus.Invalid, validation.getStatus());
        Assertions.assertEquals("This failed the rule " + latestDateRule + ", This failed the rule " + lessThanSPELRule,validation.getMessage());
    }


    private PaymentInstruction createPaymentInstruction(Money money) {
        var recipientObject = Recipient.builder()
                .accountNumber(new AccountNumber("20202020"))
                .sortCode(new SortCode("202020"))
                .ref("ref")
                .name("test2")
                .build();

        String latestDate = "2020-01-01";

        return PaymentInstruction.builder()
                .recipient(recipientObject)
                .targetAmount(money)
                .timestamp(LocalDateTime.now())
                .service("")
                .paymentFile("")
                .targetSettlementCurrency(GBP_CURRENCY)
                .latestTargetSettlementDate(LocalDate.parse(latestDate))
                .build();
    }
}