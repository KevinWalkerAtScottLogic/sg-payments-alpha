package gov.scot.payments.paymentfile.cps;

import gov.scot.payments.paymentfile.FileParseException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static gov.scot.payments.paymentfile.cps.CpsParser.FIELD_SEPARATOR;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Control record")
class CpsControlRecordTest {

    @Nested
    @DisplayName("from fields")
    class FromFields {

        @Test
        @DisplayName("with example control line then record returned")
        void withExampleControlLineThenRecordReturned() throws FileParseException, RecordParseException {

            var line = "CTL|12|SCO1|12SCO1001020180621|20180621|2|60000|0|0|60000";
            var fields = line.split(FIELD_SEPARATOR);

            var record = CpsControlRecord.fromFields(fields, 1);

            assertThat(record.getEntitlementManagementSystem(), is("12"));
            assertThat(record.getServiceIdentifier(), is("SCO1"));
            assertThat(record.getInterfaceReference(), is("12SCO1001020180621"));
            assertThat(record.getDateOfFile(), is(LocalDate.of(2018, 6, 21)));
            assertThat(record.getTotalNumberOfRecords(), is(2L));
            assertThat(record.getHashGrossAmount(), is(new BigDecimal("600.00")));
            assertThat(record.getHashNetAmount(), is(new BigDecimal("600.00")));
        }

        @Test
        @DisplayName("with invalid date of file then exception is thrown")
        void withInvalidDateOfFileThenExceptionIsThrown() {

            var line = "CTL|12|SCO1|12SCO1001020180621|20180621X|2|60000|0|0|60000";
            var fields = line.split(FIELD_SEPARATOR);

            assertThrows(
                    RecordParseException.class,
                    () -> CpsControlRecord.fromFields(fields, 1)
            );
        }

        @Test
        @DisplayName("with invalid hash gross amount then exception is thrown")
        void withInvalidHashGrossAmountThenExceptionIsThrown() {

            var line = "CTL|12|SCO1|12SCO1001020180621|20180621|2|60000X|0|0|60000";
            var fields = line.split(FIELD_SEPARATOR);

            assertThrows(
                    RecordParseException.class,
                    () -> CpsControlRecord.fromFields(fields, 1)
            );
        }

        @Test
        @DisplayName("with invalid hash net amount then exception is thrown")
        void withInvalidHashNetAmountThenExceptionIsThrown() {

            var line = "CTL|12|SCO1|12SCO1001020180621|20180621|2|60000|0|0|60000X";
            var fields = line.split(FIELD_SEPARATOR);

            assertThrows(
                    RecordParseException.class,
                    () -> CpsControlRecord.fromFields(fields, 1)
            );
        }
    }
}
