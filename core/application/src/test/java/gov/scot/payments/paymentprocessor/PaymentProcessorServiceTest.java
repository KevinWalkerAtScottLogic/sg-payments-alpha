package gov.scot.payments.paymentprocessor;

import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.model.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;

import java.io.IOException;
import java.net.URI;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class PaymentProcessorServiceTest {

    private DiscoveryClient discoveryClient;
    private PaymentProcessorProxy proxy;
    private PaymentProcessorService service;
    private PaymentProcessorRepository repository;

    @BeforeEach
    public void setUp(){
        discoveryClient = mock(DiscoveryClient.class);
        proxy = mock(PaymentProcessorProxy.class);
        repository = mock(PaymentProcessorRepository.class);
        service = new PaymentProcessorService(discoveryClient,proxy,"test", repository);

    }

    @Test
    public void testGet() throws IOException {
        PaymentProcessor processor = PaymentProcessor.builder()
                .name("123")
                .createdBy("admin")
                .build();
        ServiceInstance si = mock(ServiceInstance.class);
        when(si.getUri()).thenReturn(URI.create("http://123"));
        when(si.getMetadata()).thenReturn(Collections.singletonMap("instance_status","UP"));
        when(discoveryClient.getInstances("test.gateway.123")).thenReturn(Collections.singletonList(si));
        when(proxy.getDetails(anyString(),any())).thenReturn(processor);
        assertTrue(service.getLiveProcessor("456").isEmpty());
        assertEquals(processor,service.getLiveProcessor("123").get());
    }

    @Test
    public void testGetAll(){
        PaymentProcessor processor = PaymentProcessor.builder()
                .name("123")
                .createdBy("admin")
                .build();

        ServiceInstance si = mock(ServiceInstance.class);
        when(si.getUri()).thenReturn(URI.create("http://123"));
        when(si.getMetadata()).thenReturn(Collections.singletonMap("instance_status","UP"));
        when(discoveryClient.getServices()).thenReturn(Collections.singletonList("test.gateway.123"));
        when(discoveryClient.getInstances("test.gateway.123")).thenReturn(Collections.singletonList(si));
        when(proxy.getDetails(anyString(),any())).thenReturn(processor);
        assertEquals(1,service.getAllLiveProcessors().size());
    }

    @Test
    public void testDisable() throws IOException {
        ServiceInstance si = mock(ServiceInstance.class);
        when(si.getUri()).thenReturn(URI.create("http://123"));
        when(si.getMetadata()).thenReturn(Collections.singletonMap("instance_status","UP"));
        when(discoveryClient.getInstances("test.gateway.123")).thenReturn(Collections.singletonList(si));
        when(proxy.setGatewayStatus(anyString(),any(),anyBoolean())).thenReturn("OUT_OF_SERVICE");
        assertEquals("OUT_OF_SERVICE",service.disableLiveProcessor("123").get());

    }

    @Test
    public void testUpdate(){
        PaymentProcessor processor = PaymentProcessor.builder()
                .name("123")
                .createdBy("admin")
                .build();
        ServiceInstance si = mock(ServiceInstance.class);
        when(si.getUri()).thenReturn(URI.create("http://123"));
        when(si.getMetadata()).thenReturn(Collections.singletonMap("instance_status","UP"));
        when(discoveryClient.getInstances("test.gateway.123")).thenReturn(Collections.singletonList(si));
        when(proxy.updateDetails(any(),any())).thenReturn(processor);
        assertEquals(processor,service.updateLiveProcessor(processor, User.admin()).get());

    }
}
