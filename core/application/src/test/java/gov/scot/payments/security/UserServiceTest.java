package gov.scot.payments.security;

import gov.scot.payments.model.user.Client;
import gov.scot.payments.model.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserServiceTest {

    private UserService userService;

    @BeforeEach
    public void setUp(){
        userService = new UserService();
    }

    @Test
    public void testGetUser(){
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(User.admin(),"", User.admin().getAuthorities()));
        Client user = userService.getCurrentUser();
        assertEquals(User.admin(),user);

    }
}
