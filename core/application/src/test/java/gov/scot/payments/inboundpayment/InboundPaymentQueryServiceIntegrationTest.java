package gov.scot.payments.inboundpayment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import gov.scot.payments.AbstractKafkaTest;
import gov.scot.payments.MockWebSecurityConfiguration;
import gov.scot.payments.WithUser;
import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.common.kafka.KafkaNullConfiguration;
import gov.scot.payments.inboundpayment.spring.InboundPaymentPersistenceBinding;
import gov.scot.payments.inboundpayment.spring.InboundPaymentPersistenceConfiguration;
import gov.scot.payments.inboundpayment.spring.InboundPaymentQueryServiceConfiguration;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.inboundpayment.ChannelType;
import gov.scot.payments.model.inboundpayment.InboundPayment;
import gov.scot.payments.model.inboundpayment.PaymentStatus;
import gov.scot.payments.model.inboundpayment.event.*;
import gov.scot.payments.model.user.EntityOp;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Client;
import gov.scot.payments.model.user.User;
import org.hamcrest.Matchers;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.aws.autoconfigure.context.ContextCredentialsAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.jdbc.AmazonRdsDatabaseAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.messaging.MessagingAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.function.Function;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.notNull;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@EnableAutoConfiguration(exclude = {MessagingAutoConfiguration.class
        , ContextStackAutoConfiguration.class
        , ContextResourceLoaderAutoConfiguration.class
        , ContextRegionProviderAutoConfiguration.class
        , ContextCredentialsAutoConfiguration.class
        , AmazonRdsDatabaseAutoConfiguration.class
        , HypermediaAutoConfiguration.class
})
@EmbeddedKafka(partitions = 1
        ,topics = {"test-inboundPaymentReceivedEvents"
        ,"test-inboundPaymentAttemptFailedEvents"
        ,"test-inboundPaymentClearedEvents"
        ,"test-inboundPaymentFailedToClearEvents"
        ,"test-inboundPaymentSettledEvent"})
@SpringBootTest(classes = InboundPaymentQueryServiceIntegrationTest.TestConfiguration.class
        , properties = {"payments.environment=test"
        ,"spring.cloud.stream.bindings.send-inbound-payment-received-events.destination=test-inboundPaymentReceivedEvents"
        ,"spring.cloud.stream.bindings.send-inbound-payment-received-events.binder=kafka"
        ,"spring.cloud.stream.bindings.send-inbound-payment-attempt-failed-events.destination=test-inboundPaymentAttemptFailedEvents"
        ,"spring.cloud.stream.bindings.send-inbound-payment-attempt-failed-events.binder=kafka"
        ,"spring.cloud.stream.bindings.send-inbound-payment-cleared-events.destination=test-inboundPaymentClearedEvents"
        ,"spring.cloud.stream.bindings.send-inbound-payment-cleared-events.binder=kafka"
        ,"spring.cloud.stream.bindings.send-inbound-payment-failed-to-clear-events.destination=test-inboundPaymentFailedToClearEvents"
        ,"spring.cloud.stream.bindings.send-inbound-payment-failed-to-clear-events.binder=kafka"
        ,"spring.cloud.stream.bindings.send-inbound-payment-settled-events.destination=test-inboundPaymentSettledEvents"
        ,"spring.cloud.stream.bindings.send-inbound-payment-settled-events.binder=kafka"
        ,"h2.serializeJavaObject=false"})
@DBRider
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@Tag("kafka")
@Tag("db")
@WithUser
public class InboundPaymentQueryServiceIntegrationTest extends AbstractKafkaTest {

    static {
        System.setProperty("h2.serializeJavaObject","false");
    }

    @Autowired
    protected MockMvc mvc;

    @Autowired
    @Qualifier("send-inbound-payment-received-events")
    MessageChannel inboundPaymentReceivedEvents;

    @Autowired
    @Qualifier("send-inbound-payment-attempt-failed-events")
    MessageChannel inboundPaymentAttemptFailedEvents;

    @Autowired
    @Qualifier("send-inbound-payment-cleared-events")
    MessageChannel inboundPaymentClearedEvents;

    @Autowired
    @Qualifier("send-inbound-payment-failed-to-clear-events")
    MessageChannel inboundPaymentFailedToClearEvents;

    @Autowired
    @Qualifier("send-inbound-payment-settled-events")
    MessageChannel inboundPaymentSettledEvents;

    @Autowired
    TemporalInboundPaymentRepository temporalInboundPaymentRepository;

    @Autowired
    NonTemporalInboundPaymentRepository nonTemporalInboundPaymentRepository;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    public void testPersistence() throws Exception {
        UUID id = UUID.randomUUID();
        sendInboundPaymentEvent(inboundPaymentReceivedEvents, id, LocalDateTime.now(), PaymentStatus.New, InboundPaymentReceivedEvent::new);
        verifyInboundPaymentEvents(id, PaymentStatus.New,1);
        sendInboundPaymentEvent(inboundPaymentClearedEvents, id, LocalDateTime.now(), PaymentStatus.Cleared, InboundPaymentClearedEvent::new);
        verifyInboundPaymentEvents(id, PaymentStatus.Cleared,2);
        sendInboundPaymentEvent(inboundPaymentSettledEvents, id, LocalDateTime.now(), PaymentStatus.Settled, InboundPaymentSettledEvent::new);
        verifyInboundPaymentEvents(id, PaymentStatus.Settled,3);
    }

    @Test
    public void testPersistenceOutOfOrder() throws Exception {
        UUID id = UUID.randomUUID();
        sendInboundPaymentEvent(inboundPaymentReceivedEvents, id, LocalDateTime.now(), PaymentStatus.New, InboundPaymentReceivedEvent::new);
        verifyInboundPaymentEvents(id, PaymentStatus.New,1);
        sendInboundPaymentEvent(inboundPaymentAttemptFailedEvents, id, LocalDateTime.now(), PaymentStatus.Failed, InboundPaymentAttemptFailedEvent::new);
        verifyInboundPaymentEvents(id, PaymentStatus.Failed,2);
        sendInboundPaymentEvent(inboundPaymentFailedToClearEvents, id, LocalDateTime.now(), PaymentStatus.FailedToClear, InboundPaymentFailedToClearEvent::new);
        verifyInboundPaymentEvents(id, PaymentStatus.FailedToClear,3);
    }

    @Test
    @DataSet("inboundpayments.yml")
    public void testQuery() throws Exception {
        //status (enum)
        mvc.perform(get("/inboundpayments/query").param("expression","status==New"))
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andExpect(jsonPath("$.content[0].id", Matchers.is("e50d0699-7b1d-452a-88e9-527e26c0180d")));

        //id (uuid)
        mvc.perform(get("/inboundpayments/query").param("expression","id==e50d0699-7b1d-452a-88e9-527e26c0180d"))
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andExpect(jsonPath("$.content[0].id", Matchers.is("e50d0699-7b1d-452a-88e9-527e26c0180d")));

        //channel type
        mvc.perform(get("/inboundpayments/query").param("expression","channelType==Card_Online"))
                .andExpect(jsonPath("$.content", hasSize(3)));

        //date time range
        mvc.perform(get("/inboundpayments/query").param("expression","createdAt>2019-01-01T00:00:00 and createdAt<=2019-01-01T00:00:03"))
                .andExpect(jsonPath("$.content", hasSize(3)));

        //date range
        mvc.perform(get("/inboundpayments/query").param("expression","clearing.clearedAt>2019-01-02T00:00:00 and clearing.clearedAt<=2019-01-03T00:00:00"))
                .andExpect(jsonPath("$.content", hasSize(2)));

        //date range
        mvc.perform(get("/inboundpayments/query").param("expression","settlement.settledAt>2019-01-01T00:00:00 and settlement.settledAt<=2019-01-03T00:00:05"))
                .andExpect(jsonPath("$.content", hasSize(2)));


        //recipient name (nested, string, wildcard)
        mvc.perform(get("/inboundpayments/query").param("expression","service==*Smith"))
                .andExpect(jsonPath("$.content", hasSize(2)));

        //TODO money queries are not working as we use a UserType to persist them and this does not seem to work with the criteria builder api - possibly use a custom meatamodel?
        //amount (big decimal, nested in money)
        //mvc.perform(get("/payments/query").param("expression","targetAmount.number>11"))
         //       .andExpect(jsonPath("$.content", hasSize(3)));

        //service with acl
        User user = User.builder().userName("").email("").withRole(new Role(EntityOp.Read, "service", "123abc")).build();
        mvc.perform(get("/inboundpayments/query").param("expression","channelType==Card_Online")
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(jsonPath("$.content", hasSize(2)));
    }

    @Test
    @DataSet("inboundpayments.yml")
    public void testGetAll() throws Exception {
        mvc.perform(get("/inboundpayments"))
                .andExpect(jsonPath("$.content", hasSize(4)))
                .andExpect(jsonPath("$.content[0].id", Matchers.is("79a8d612-d2b6-4441-a85d-5ea3701930ed")));
    }

    @Test
    @DataSet("inboundpayments.yml")
    public void testSecurity() throws Exception {
        User user = User.builder().userName("").email("").withRole(new Role(EntityOp.Read, "service", "service1")).build();
        mvc.perform(get("/payments/e50d0699-7b1d-452a-88e9-527e26c0180d")
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(status().isNotFound());
        user = User.builder().userName("").email("").withRole(new Role(EntityOp.Read, "service", "123abc")).build();
        mvc.perform(get("/inboundpayments")
                .with(authentication(new UsernamePasswordAuthenticationToken(user,"N/A",user.getAuthorities()))))
                .andExpect(jsonPath("$.content", hasSize(2)));

    }

    private void sendInboundPaymentEvent(MessageChannel channel, UUID id, LocalDateTime timestamp, PaymentStatus status, Function<InboundPayment, Event<InboundPayment>> eventSupplier) {
        InboundPayment inboundPayment = InboundPayment.builder()
                .id(id)
                .timestamp(timestamp)
                .amount(Money.of(new BigDecimal("10"),"GBP"))
                .channelType(ChannelType.BankTransfer)
                .channelId("bank_transfer_channel")
                .ref("A reference")
                .status(status)
                .statusMessage("A status message")
                .service("service")
                .build();
        channel.send(new GenericMessage<>(eventSupplier.apply(inboundPayment)));
        sleep(Duration.ofSeconds(4));
    }

    private void verifyInboundPaymentEvents(UUID id, PaymentStatus latestStatus, int count) throws Exception {
        mvc.perform(get("/inboundpayments/{id}",id))
                .andExpect(jsonPath("$.status", Matchers.is(latestStatus.name())));
        mvc.perform(get("/inboundpayments/{id}/history",id))
                .andExpect(jsonPath("$", hasSize(count)));
    }

    @Configuration
    @Import({OptionalResponseControllerAdvice.class, KafkaNullConfiguration.class, InboundPaymentQueryServiceConfiguration.class, InboundPaymentPersistenceConfiguration.class, MockWebSecurityConfiguration.class})
    @EnableBinding({ InboundPaymentPersistenceBinding.class, TestBinding.class})
//    @EntityScan(basePackageClasses = {FlattenedInboundPayment.class, Service.class})
    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    public static class TestConfiguration extends AbstractKafkaTest.TestConfiguration{

    }

    public interface TestBinding {

        @Output("send-inbound-payment-received-events")
        MessageChannel inboundPaymentReceivedEvents();

        @Output("send-inbound-payment-attempt-failed-events")
        MessageChannel inboundPaymentAttemptFailedEvents();

        @Output("send-inbound-payment-cleared-events")
        MessageChannel inboundPaymentClearedEvents();

        @Output("send-inbound-payment-failed-to-clear-events")
        MessageChannel inboundPaymentFailedToClearEvents();

        @Output("send-inbound-payment-settled-events")
        MessageChannel inboundPaymentSettledEvents();
    }
}
