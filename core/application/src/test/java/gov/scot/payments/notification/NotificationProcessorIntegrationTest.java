package gov.scot.payments.notification;

import gov.scot.payments.AbstractKafkaTest;
import gov.scot.payments.model.NotificationEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentFailedToClearEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentRejectedEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentSubmitFailedEvent;
import gov.scot.payments.notification.spring.NotificationProcessorInputBinding;
import gov.scot.payments.model.paymentfile.event.FileTranslationFailedEvent;
import gov.scot.payments.model.paymentfile.PaymentFile;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.Recipient;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingFailedEvent;
import gov.scot.payments.model.paymentfile.event.PaymentValidationFailureEvent;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.aws.autoconfigure.context.ContextCredentialsAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.jdbc.AmazonRdsDatabaseAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.messaging.MessagingAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.test.annotation.DirtiesContext;

import java.math.BigDecimal;
import java.net.URI;

import static org.mockito.Mockito.verify;

@EnableAutoConfiguration(exclude = {MessagingAutoConfiguration.class
        , ContextStackAutoConfiguration.class
        , ContextResourceLoaderAutoConfiguration.class
        , ContextRegionProviderAutoConfiguration.class
        , ContextCredentialsAutoConfiguration.class
        , AmazonRdsDatabaseAutoConfiguration.class
})
@EmbeddedKafka(partitions = 1
        ,topics = {"test-fileTranslationFailedEvents"
        ,"test-paymentValidationFailedEvents"
        ,"test-paymentRoutingFailedEvents"
        ,"test-paymentSubmitFailedEvents"
        ,"test-paymentRejectedEvents"
        ,"test-paymentFailedToClearEvents"})
@SpringBootTest(classes = NotificationProcessorIntegrationTest.TestConfiguration.class, properties = {
        "debug=true",
        "payments.environment=test",
        "spring.cloud.stream.bindings.payment-validation-fail-event-send.binder=kafka",
        "spring.cloud.stream.bindings.payment-validation-fail-event-send.destination=test-paymentValidationFailedEvents",
        "spring.cloud.stream.bindings.file-translation-fail-event-send.binder=kafka",
        "spring.cloud.stream.bindings.file-translation-fail-event-send.destination=test-fileTranslationFailedEvents",
        "spring.cloud.stream.bindings.payment-routing-fail-event-send.binder=kafka",
        "spring.cloud.stream.bindings.payment-routing-fail-event-send.destination=test-paymentRoutingFailedEvents",
        "spring.cloud.stream.bindings.payment-submission-fail-event-send.binder=kafka",
        "spring.cloud.stream.bindings.payment-submission-fail-event-send.destination=test-paymentSubmitFailedEvents",
        "spring.cloud.stream.bindings.payment-rejection-event-send.binder=kafka",
        "spring.cloud.stream.bindings.payment-rejection-event-send.destination=test-paymentRejectedEvents",
        "spring.cloud.stream.bindings.payment-failedToClear-event-send.binder=kafka",
        "spring.cloud.stream.bindings.payment-failedToClear-event-send.destination=test-paymentFailedToClearEvents"
})
@DirtiesContext
@Tag("kafka")
public class NotificationProcessorIntegrationTest extends AbstractKafkaTest {

    @Autowired
    @Qualifier("file-translation-fail-event-send")
    MessageChannel fileTranslationFailed;

    @Autowired
    @Qualifier("payment-validation-fail-event-send")
    MessageChannel paymentValidationFailed;

    @Autowired
    @Qualifier("payment-routing-fail-event-send")
    MessageChannel paymentRoutingFailed;

    @Autowired
    @Qualifier("payment-submission-fail-event-send")
    MessageChannel paymentSubmissionFailed;

    @Autowired
    @Qualifier("payment-rejection-event-send")
    MessageChannel paymentRejection;

    @Autowired
    @Qualifier("payment-failedToClear-event-send")
    MessageChannel paymentFailedToClear;

    @MockBean
    Notifier notifier;

    @Test
    public void testNotificationFailure() throws Exception {

        var paymentFile = PaymentFile.builder()
                .service("service")
                .name("file1")
                .type("ILF")
                .url(new URI("s3://path/to/file"))
                .build();

        sendAndVerifyEvent(fileTranslationFailed,new FileTranslationFailedEvent(paymentFile),"test-fileTranslationFailedEvents");
    }

    @Test
    public void testValidationFailure() throws Exception {
        sendAndVerifyEvent(paymentValidationFailed,new PaymentValidationFailureEvent(createPayment()),"test-paymentValidationFailedEvents");
    }

    @Test
    public void testRoutingFailure() throws Exception {
        sendAndVerifyEvent(paymentRoutingFailed,new PaymentRoutingFailedEvent(createPayment()),"test-paymentRoutingFailedEvents");
    }

    @Test
    public void testSubmissionFailure() throws Exception {
        sendAndVerifyEvent(paymentSubmissionFailed,new PaymentSubmitFailedEvent(createPayment()),"test-paymentSubmitFailedEvents");
    }

    @Test
    public void testRejection() throws Exception {
        sendAndVerifyEvent(paymentRejection,new PaymentRejectedEvent(createPayment()),"test-paymentRejectedEvents");
    }

    @Test
    public void testFailedToClear() throws Exception {
        sendAndVerifyEvent(paymentFailedToClear,new PaymentFailedToClearEvent(createPayment()),"test-paymentFailedToClearEvents");
    }

    private <T extends NotificationEvent> void sendAndVerifyEvent(MessageChannel channel,T event, String topic){
        channel.send(new GenericMessage<>(event));
        sleep();
        verifyEvents(topic,events -> Assertions.assertEquals(1,events.size()));
        verify(notifier).notify(Mockito.any(event.getClass()));
    }

    private PaymentInstruction createPayment() {
        return PaymentInstruction.builder()
                    .recipient(Recipient.builder().build())
                    .targetAmount(Money.of(new BigDecimal("200"), "GBP"))
                    .paymentFile("payment_file_test")
                    .service("service1")
                    .build();
    }

    @EnableBinding({ NotificationProcessorInputBinding.class, NotificationTestBinding.class})
    public static class TestConfiguration extends AbstractKafkaTest.TestConfiguration {

        @Bean
        public NotificationProcessor getNotificationProcessor(Notifier notifier){
            return new NotificationProcessor(notifier);
        }
    }

    public interface NotificationTestBinding {

        @Output("file-translation-fail-event-send")
        MessageChannel failedFileChannel();

        @Output("payment-validation-fail-event-send")
        MessageChannel failedPaymentChannel();

        @Output("payment-routing-fail-event-send")
        MessageChannel failedRoutingEvents();

        @Output("payment-submission-fail-event-send")
        MessageChannel failedSubmissionEvents();

        @Output("payment-rejection-event-send")
        MessageChannel rejectedEvents();

        @Output("payment-failedToClear-event-send")
        MessageChannel failedToClearEvents();
    }


}
