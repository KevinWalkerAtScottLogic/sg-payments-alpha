package gov.scot.payments.router;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingFailedEvent;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingSuccessEvent;
import gov.scot.payments.model.paymentinstruction.*;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.kafka.core.KafkaOperations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static gov.scot.payments.router.PaymentRouter.NULL_PLACEHOLDER;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class PaymentRouterTest {

    private PaymentRouter router;
    private KafkaOperations kafkaOperations;
    private ExpressionParser parser;
    private ObjectMapper mapper;
    private Supplier<Collection<PaymentProcessor>> paymentProcessorRegistry;

    @BeforeEach
    public void setUp(){
        mapper = new ObjectMapper();
        mapper.registerModule(new Jdk8Module());
        mapper.registerModule(new JavaTimeModule());
        parser = new SpelExpressionParser();
        kafkaOperations = mock(KafkaOperations.class);
        paymentProcessorRegistry = mock(Supplier.class);
        router = new PaymentRouter(paymentProcessorRegistry,kafkaOperations,mapper,"test",parser);
    }

    @Test
    public void testSendEvent(){
        PaymentInstruction instruction = PaymentInstruction.builder()
                .recipient(Recipient.builder().build())
                .targetAmount(Money.of(new BigDecimal("100"),"GBP"))
                .service("service1")
                .routing(Routing.builder()
                        .queue("test-processor1")
                        .processor("processor")
                        .build())
                .build();
        router.sendSuccessEvent(new PaymentRoutingFailedEvent(instruction));
        router.sendSuccessEvent(new PaymentRoutingSuccessEvent(instruction));
        ArgumentCaptor<ProducerRecord> captor = ArgumentCaptor.forClass(ProducerRecord.class);
        verify(kafkaOperations,times(1)).send(captor.capture());
        ProducerRecord record = captor.getValue();
        assertEquals("test-processor1",record.topic());
    }

    @Test
    public void testCalculateChannelCost(){
        PaymentInstruction instruction = PaymentInstruction.builder()
                .recipient(Recipient.builder().build())
                .targetAmount(Money.of(new BigDecimal("100"),"GBP"))
                .service("service1")
                .build();
        Money cost = router.calculateOutboundChannelCost(instruction, PaymentProcessorChannel.builder()
                .paymentChannel(PaymentChannel.Bacs)
                .costExpression("targetAmount.multiply(0.01)")
                .build());
        assertEquals(BigDecimal.ONE,cost.getNumberStripped());

        cost = router.calculateOutboundChannelCost(instruction, PaymentProcessorChannel.builder()
                .paymentChannel(PaymentChannel.Bacs)
                .costExpression("targetAmount.aaa(0.01)")
                .build());
        assertSame(NULL_PLACEHOLDER, cost);
    }

    @Test
    public void testMapEventNoChannels(){
        PaymentInstruction instruction = PaymentInstruction.builder()
                .recipient(Recipient.builder().build())
                .targetAmount(Money.of(new BigDecimal("100"),"GBP"))
                .service("service1")
                .build();
        when(paymentProcessorRegistry.get()).thenReturn(List.of());
        Event<PaymentInstruction> event = router.mapEvent(instruction);
        assertTrue(PaymentRoutingFailedEvent.class.isAssignableFrom(event.getClass()));
    }

    @Test
    public void testMapEventNoChannelsOfType(){
        PaymentInstruction instruction = PaymentInstruction.builder()
                .recipient(Recipient.builder().build())
                .latestTargetSettlementDate(LocalDate.now().plusDays(7))
                .targetChannelType(PaymentChannelType.Cheque)
                .targetAmount(Money.of(new BigDecimal("100"),"GBP"))
                .service("service1")
                .build();
        PaymentProcessor processor = PaymentProcessor.builder()
                .name("123")
                .createdBy("admin")
                .addChannel(PaymentProcessorChannel.builder()
                        .costExpression("targetAmount.multiply(0.01)")
                        .paymentChannel(PaymentChannel.Bacs)
                        .build())
                .build();
        when(paymentProcessorRegistry.get()).thenReturn(List.of(processor));

        Event<PaymentInstruction> event = router.mapEvent(instruction);
        assertTrue(PaymentRoutingFailedEvent.class.isAssignableFrom(event.getClass()));
    }

    @Test
    public void testMapEventNoChannelsWithLimit(){
        PaymentInstruction instruction = PaymentInstruction.builder()
                .recipient(Recipient.builder().build())
                .targetChannelType(PaymentChannelType.EFT)
                .latestTargetSettlementDate(LocalDate.now().plusDays(7))
                .targetAmount(Money.of(new BigDecimal("300000"),"GBP"))
                .service("service1")
                .build();
        PaymentProcessor processor = PaymentProcessor.builder()
                .name("123")
                .createdBy("admin")
                .addChannel(PaymentProcessorChannel.builder()
                        .costExpression("targetAmount.multiply(0.01)")
                        .paymentChannel(PaymentChannel.Bacs)
                        .build())
                .build();
        when(paymentProcessorRegistry.get()).thenReturn(List.of(processor));

        Event<PaymentInstruction> event = router.mapEvent(instruction);
        assertTrue(PaymentRoutingFailedEvent.class.isAssignableFrom(event.getClass()));
    }

    @Test
    public void testMapEventNoChannelsWithSettlementCycle(){
        PaymentInstruction instruction = PaymentInstruction.builder()
                .recipient(Recipient.builder().build())
                .targetChannelType(PaymentChannelType.EFT)
                .latestTargetSettlementDate(LocalDate.now())
                .targetAmount(Money.of(new BigDecimal("200000"),"GBP"))
                .service("service1")
                .build();
        PaymentProcessor processor = PaymentProcessor.builder()
                .name("123")
                .createdBy("admin")
                .addChannel(PaymentProcessorChannel.builder()
                        .costExpression("targetAmount.multiply(0.01)")
                        .paymentChannel(PaymentChannel.Bacs)
                        .build())
                .build();
        when(paymentProcessorRegistry.get()).thenReturn(List.of(processor));

        Event<PaymentInstruction> event = router.mapEvent(instruction);
        assertTrue(PaymentRoutingFailedEvent.class.isAssignableFrom(event.getClass()));
    }

    @Test
    public void testMapEventNoChannelsWithFutureSettlementCycle(){
        PaymentInstruction instruction = PaymentInstruction.builder()
                .recipient(Recipient.builder().build())
                .targetChannelType(PaymentChannelType.EFT)
                .latestTargetSettlementDate(LocalDate.now().plusDays(31))
                .targetAmount(Money.of(new BigDecimal("200000"),"GBP"))
                .service("service1")
                .build();
        PaymentProcessor processor = PaymentProcessor.builder()
                .name("123")
                .createdBy("admin")
                .addChannel(PaymentProcessorChannel.builder()
                        .costExpression("targetAmount.multiply(0.01)")
                        .paymentChannel(PaymentChannel.Bacs)
                        .build())
                .build();
        when(paymentProcessorRegistry.get()).thenReturn(List.of(processor));

        Event<PaymentInstruction> event = router.mapEvent(instruction);
        assertTrue(PaymentRoutingFailedEvent.class.isAssignableFrom(event.getClass()));
    }

    @Test
    public void testMapEventNoChannelsWithValidCostExpression(){
        PaymentInstruction instruction = PaymentInstruction.builder()
                .recipient(Recipient.builder().build())
                .targetChannelType(PaymentChannelType.EFT)
                .latestTargetSettlementDate(LocalDate.now().plusDays(7))
                .targetAmount(Money.of(new BigDecimal("200000"),"GBP"))
                .service("service1")
                .build();
        PaymentProcessor processor = PaymentProcessor.builder()
                .name("123")
                .createdBy("admin")
                .addChannel(PaymentProcessorChannel.builder()
                        .costExpression("targetAmount.mully(0.01)")
                        .paymentChannel(PaymentChannel.Bacs)
                        .build())
                .build();
        when(paymentProcessorRegistry.get()).thenReturn(List.of(processor));

        Event<PaymentInstruction> event = router.mapEvent(instruction);
        assertTrue(PaymentRoutingFailedEvent.class.isAssignableFrom(event.getClass()));
    }

    @Test
    public void testMapEventSuccess(){
        PaymentInstruction instruction = PaymentInstruction.builder()
                .recipient(Recipient.builder().build())
                .targetChannelType(PaymentChannelType.EFT)
                .latestTargetSettlementDate(LocalDate.now().plusDays(7))
                .targetAmount(Money.of(new BigDecimal("200000"),"GBP"))
                .service("service1")
                .build();
        PaymentProcessor processor1 = PaymentProcessor.builder()
                .name("123")
                .createdBy("admin")
                .addChannel(PaymentProcessorChannel.builder()
                        .costExpression("targetAmount.multiply(0.01)")
                        .paymentChannel(PaymentChannel.Bacs)
                        .build())
                .build();
        PaymentProcessor processor2 = PaymentProcessor.builder()
                .name("456")
                .addChannel(PaymentProcessorChannel.builder()
                        .costExpression("targetAmount.multiply(0.02)")
                        .paymentChannel(PaymentChannel.FasterPayments)
                        .build())
                .build();
        when(paymentProcessorRegistry.get()).thenReturn(List.of(processor1,processor2));

        Event<PaymentInstruction> event = router.mapEvent(instruction);
        assertTrue(PaymentRoutingSuccessEvent.class.isAssignableFrom(event.getClass()));
        Routing routing = event.getPayload().getRouting();
        assertEquals(PaymentChannel.Bacs,routing.getChannel());
        assertEquals(Money.of(new BigDecimal("2000"),"GBP"),routing.getExpectedCost());
        assertEquals("123",routing.getProcessor());
        assertEquals("test-123PaymentEvents",routing.getQueue());
        assertEquals(RoutingStatus.Success,routing.getStatus());
    }

    private class ListKeyValueIterator implements KeyValueIterator<String,String> {

        private Iterator<KeyValue<String,String>> delegate;

        public ListKeyValueIterator(ObjectMapper mapper, PaymentProcessor... processors) {
            delegate = Arrays.asList(processors).stream()
                    .map(p -> {
                        try {
                            return new KeyValue<>(p.getName(),mapper.writeValueAsString(p));
                        } catch (JsonProcessingException e) {
                            throw new RuntimeException(e);
                        }
                    })
                    .collect(Collectors.toList())
                    .iterator();
        }

        @Override
        public void close() {
        }

        @Override
        public String peekNextKey() {
            return null;
        }

        @Override
        public boolean hasNext() {
           return delegate.hasNext();
        }

        @Override
        public KeyValue<String,String> next() {
            return delegate.next();
        }
    }

}
