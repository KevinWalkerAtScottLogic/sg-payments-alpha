package gov.scot.payments.paymentprocessor;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.AbstractKafkaTest;
import gov.scot.payments.MockWebSecurityConfiguration;
import gov.scot.payments.WithUser;
import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.common.kafka.KafkaNullConfiguration;
import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import gov.scot.payments.model.paymentinstruction.PaymentProcessorChannel;
import gov.scot.payments.paymentprocessor.spring.PaymentProcessorConfiguration;
import gov.scot.payments.paymentprocessor.spring.PaymentProcessorServiceConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.cloud.aws.autoconfigure.context.ContextCredentialsAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.jdbc.AmazonRdsDatabaseAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.messaging.MessagingAutoConfiguration;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.cloud.client.serviceregistry.ServiceRegistry;
import org.springframework.cloud.zookeeper.discovery.ZookeeperInstance;
import org.springframework.cloud.zookeeper.serviceregistry.ServiceInstanceRegistration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.MediaType;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.cloud.zookeeper.discovery.ZookeeperDiscoveryProperties.DEFAULT_URI_SPEC;
import static org.springframework.cloud.zookeeper.support.StatusConstants.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@EnableAutoConfiguration(exclude = {MessagingAutoConfiguration.class
        , ContextStackAutoConfiguration.class
        , ContextResourceLoaderAutoConfiguration.class
        , ContextRegionProviderAutoConfiguration.class
        , ContextCredentialsAutoConfiguration.class
        , AmazonRdsDatabaseAutoConfiguration.class
})
@EmbeddedKafka(partitions = 1)
@SpringBootTest(classes = PaymentProcessorServiceIntegrationTest.TestConfiguration.class
        , properties = {"payments.environment=test" }, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Tag("db")
@Tag("kafka")
@WithUser
public class PaymentProcessorServiceIntegrationTest extends AbstractKafkaTest {

    @LocalServerPort
    int port;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    ServiceRegistry registry;

    @Autowired
    MockGatewayService service;

    @BeforeEach
    public void SetUp(){
        String gatewayName = "test.gateway.123";
        Map<String, String> instanceStatusKey = new HashMap<>();
        instanceStatusKey.put(INSTANCE_STATUS_KEY, STATUS_UP);
        ZookeeperInstance zookeeperInstance = new ZookeeperInstance(gatewayName,
                gatewayName, instanceStatusKey);
        Registration registration = ServiceInstanceRegistration.builder()
                .address("localhost")
                .port(port)
                .name(gatewayName)
                .payload(zookeeperInstance)
                .uriSpec(DEFAULT_URI_SPEC)
                .build();
        registry.register(registration);
        service.setRegistration(registration);

    }

    @Test
    public void testPersistence() throws Exception {
        PaymentProcessor updated = PaymentProcessor.builder()
                .name("123")
                .createdBy("admin")
                .addChannel(PaymentProcessorChannel.builder()
                        .costExpression("")
                        .paymentChannel(PaymentChannel.Bacs)
                        .build())
                .build();

        //get all
        mvc.perform(get("/processors/live"))
                .andExpect(jsonPath("$", hasSize(1)));

        //send update
        mvc.perform(put("/processors/live").content(objectMapper.writeValueAsString(updated))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        //  get
        mvc.perform(get("/processors/live/123"))
                .andExpect(content().json("{\"name\":\"123\",\"channels\":[{\"paymentChannel\":\"Bacs\",\"costExpression\":\"\"}]}"));

        //get status

        //disable
        mvc.perform(delete("/processors/live/123"))
                .andExpect(status().isOk());

    }

    @Test
    @WithUser(entityType = "customer", entityId = "customer_1_2")
    public void testAcl() throws Exception {
        PaymentProcessor initial = PaymentProcessor.builder()
                .name("789")
                .createdBy("admin")
                .build();
        mvc.perform(put("/processors/live")
                .content(objectMapper.writeValueAsString(initial))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
        mvc.perform(delete("/processors/live/123"))
                .andExpect(status().isForbidden());
        mvc.perform(post("/processors/live/123"))
                .andExpect(status().isForbidden());
    }

    @Configuration
    @Import({OptionalResponseControllerAdvice.class, KafkaNullConfiguration.class, PaymentProcessorServiceConfiguration.class,PaymentProcessorConfiguration.class, MockWebSecurityConfiguration.class})
    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    @EnableJpaRepositories(basePackageClasses = PaymentProcessorRepository.class)
    @EntityScan(basePackageClasses = PaymentProcessor.class)
    public static class TestConfiguration extends AbstractKafkaTest.TestConfiguration {

        @Bean
        public MockGatewayService mockGatewayService(ServiceRegistry registry){
            return new MockGatewayService(registry);
        }

        @Bean
        @Primary
        public PaymentProcessorProxy testPaymentProcessorProxy(MockGatewayService service){
            return new PaymentProcessorProxy() {
                @Override
                public String setGatewayStatus(String name, URI uri, boolean enabled) {
                    String status = enabled ? STATUS_UP : STATUS_OUT_OF_SERVICE;
                    service.setStatus(status);
                    return service.getStatus();
                }

                @Override
                public String getStatus(String name, URI uri) {
                    return service.getStatus();
                }

                @Override
                public PaymentProcessor updateDetails(URI uri, PaymentProcessor processor) {
                    return service.updateDetails(processor);
                }

                @Override
                public PaymentProcessor getDetails(String name, URI uri) {
                    return service.getDetails();
                }
            };
        }

    }

    @RequestMapping
    public static class MockGatewayService {

        private PaymentProcessor details;
        private final ServiceRegistry registry;
        private Registration registration;

        public MockGatewayService(ServiceRegistry registry){
            this.registry = registry;
            details = PaymentProcessor.builder()
                    .name("123")
                    .build();

        }

        public void setRegistration(Registration registration){
            this.registration = registration;
        }

        @RequestMapping("/details")
        @ResponseBody
        public PaymentProcessor getDetails(){
            return details;
        }

        @RequestMapping(value = "/details", method = RequestMethod.PUT)
        @ResponseBody
        public PaymentProcessor updateDetails(@RequestBody PaymentProcessor details){
            this.details = details;
            return this.details;
        }

        @RequestMapping("/actuator/service-registry")
        @ResponseBody
        public String getStatus(){
            return (String)registry.getStatus(this.registration);
        }

        @RequestMapping(value = "/actuator/service-registry", method = RequestMethod.POST)
        @ResponseBody
        public void setStatus(@RequestBody String status){
            registry.setStatus(this.registration,status);
        }

    }


}
