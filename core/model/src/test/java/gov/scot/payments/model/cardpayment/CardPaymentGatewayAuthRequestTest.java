package gov.scot.payments.model.cardpayment;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;

class CardPaymentGatewayAuthRequestTest {

    @Test
    void whenToStringCalledThenCardNumberAndCVVAreNotIncluded() {

        String name = "the name";
        String cardNumber = "1234 5678 9012 3456";
        String cvv = "246";
        Integer year = 1999;
        Double amount = 987.65;
        String currency = "GBP";
        UUID paymentRef = UUID.randomUUID();


        var authRequest = CardPaymentGatewayAuthRequest.builder()
                .name(name)
                .cardNumber(cardNumber)
                .cvv(cvv)
                .expiry(LocalDate.of(year, 12, 28))
                .amount(Money.of(amount, currency))
                .paymentRef(paymentRef)
                .build();

        var toString = authRequest.toString();

        assertThat(toString, not(containsString(name)));
        assertThat(toString, not(containsString(cardNumber)));
        assertThat(toString, not(containsString(cvv)));
        assertThat(toString, not(containsString(year.toString())));
        assertThat(toString, containsString(amount.toString()));
        assertThat(toString, containsString(currency));
        assertThat(toString, containsString(paymentRef.toString()));
    }
}
