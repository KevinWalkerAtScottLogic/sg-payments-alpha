package gov.scot.payments.model.paymentinstruction;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SortCodeTest {

    @Test
    void fromStringTest() throws InvalidPaymentFieldException {
        SortCode sortCode = SortCode.fromString("800008");
        Assertions.assertEquals("800008", sortCode.getValue());
    }

    @Test
    void fromStringInvalidLengthTest() {
        var exception = Assertions.assertThrows(InvalidPaymentFieldException.class, () -> SortCode.fromString("80008"));
        Assertions.assertEquals("Invalid sort-code number 80008 - must be 6 digits", exception.getMessage());
    }

    @Test
    void fromStringInvalidNumberTest()  {
        var exception = Assertions.assertThrows(InvalidPaymentFieldException.class, () -> SortCode.fromString("80b008"));
        Assertions.assertEquals("Invalid sort-code number 80b008 - must be numeric",exception.getMessage());
    }
}