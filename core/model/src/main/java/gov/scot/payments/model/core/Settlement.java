package gov.scot.payments.model.core;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gov.scot.payments.model.core.MoneyDeserializer;
import gov.scot.payments.model.core.MoneySerializer;
import lombok.*;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;
import org.javamoney.moneta.Money;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.LocalDateTime;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
@Embeddable
public class Settlement {

    @NonNull @Builder.Default private final LocalDateTime settledAt = LocalDateTime.now();
    @JsonDeserialize(using = MoneyDeserializer.class)
    @JsonSerialize(using = MoneySerializer.class)
    @Type(type = "gov.scot.payments.model.core.MoneyType")
    @Columns(columns = {@Column(name = "settled_amount"),@Column(name = "settled_amount_currency")})
    @NonNull private Money settledAmount;
}
