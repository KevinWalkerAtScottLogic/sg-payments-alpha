package gov.scot.payments.model.paymentinstruction;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.io.IOException;

public class CurrencyUnitDeserializer extends JsonDeserializer<CurrencyUnit> {

    @Override
    public CurrencyUnit deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        var oc = p.getCodec();
        JsonNode node = oc.readTree(p);
        String code = node.asText();
        return Monetary.getCurrency(code);
    }
}
