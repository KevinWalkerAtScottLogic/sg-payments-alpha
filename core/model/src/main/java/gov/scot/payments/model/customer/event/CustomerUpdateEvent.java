package gov.scot.payments.model.customer.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.customer.Customer;
import lombok.NonNull;
import lombok.Value;

@Value
public class CustomerUpdateEvent implements Event<Customer> {

    @JsonCreator
    public CustomerUpdateEvent(@JsonProperty("payload") Customer payload) {
        this.payload = payload;
    }

    @NonNull
    private final Customer payload;
}
