package gov.scot.payments.model.paymentinstruction;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.javamoney.moneta.Money;

import javax.money.CurrencyUnit;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import static gov.scot.payments.model.paymentinstruction.PaymentInstruction.GBP_CURRENCY;

@AllArgsConstructor
@Getter
public enum PaymentChannel {

    Chaps(PaymentChannelType.EFT,null,Set.of(GBP_CURRENCY),0,0 ),
    Bacs(PaymentChannelType.EFT,Money.of(new BigDecimal("250000"),"GBP"),Set.of(GBP_CURRENCY),2,30),
    FasterPayments(PaymentChannelType.EFT,Money.of(new BigDecimal("250000"),"GBP"),Set.of(GBP_CURRENCY),0,0 ),
    Cheque(PaymentChannelType.Cheque,null,Set.of(GBP_CURRENCY),4,-1),
    PostalOrder(PaymentChannelType.PostalOrder,Money.of(new BigDecimal("250"),"GBP"),Set.of(GBP_CURRENCY),4,-1),
    Card(PaymentChannelType.Card,null,Set.of(GBP_CURRENCY),1,0);

    private PaymentChannelType type;
    private Money limit;
    private Set<CurrencyUnit> supportedCurrencies;
    private int settlementCycleDays;
    private int maxFutureDays;

    public boolean isAbleToPay(Money amount, CurrencyUnit targetCurrency){
        return supportedCurrencies.contains(targetCurrency) && (limit == null || limit.isGreaterThanOrEqualTo(amount));
    }

    //TODO: this would need to be smarter in the real system, i.e. taking in to account cut off times etc..
    public LocalDate getEarliestSettlementDate(LocalDateTime now) {
        return now.toLocalDate().plusDays(settlementCycleDays);
    }

    //TODO: this would need to be smarter in the real system, i.e. taking in to account cut off times etc..
    public LocalDate getLatestSettlementDate(LocalDateTime now) {
        if(maxFutureDays < 0){
            return null;
        }
        return now.toLocalDate().plusDays(maxFutureDays);
    }
}
