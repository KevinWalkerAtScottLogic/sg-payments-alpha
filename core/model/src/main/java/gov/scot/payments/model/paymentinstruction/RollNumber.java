package gov.scot.payments.model.paymentinstruction;

import lombok.Value;

@Value
public class RollNumber {

    private static int ROLL_NUMBER_MAX_LENGTH = 18;

    private final String value;

    public static RollNumber fromString(String rollNumberStr) throws InvalidPaymentFieldException {
        if(rollNumberStr.length() > ROLL_NUMBER_MAX_LENGTH){
            throw new InvalidPaymentFieldException("Invalid roll number "+ rollNumberStr + " - must be no longer than 18 characters");
        }
        return new RollNumber(rollNumberStr);
    }

    public boolean isEmpty() {
        return value.isEmpty();
    }
}
