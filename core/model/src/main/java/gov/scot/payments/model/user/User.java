package gov.scot.payments.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "USERS")
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode(callSuper = false)
public class User extends Client{

    @NonNull private String email;
    @NonNull @Id
    private String userName;
    @ElementCollection(fetch = FetchType.EAGER) @NonNull @Singular("withRole") private List<Role> roles;

    @JsonIgnore
    @Transient
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles.stream().map(Role::toAuthority).collect(Collectors.toList());
    }

    @Transient
    @JsonIgnore
    public boolean isAdmin(){
        return roles.contains(Role.WILDCARD_ROLE);
    }

    @Override
    public boolean hasSpecificAccess(String entityType, String entityId, EntityOp operation){
        return roles.stream().anyMatch(r -> r.allowsSpecificAccess(entityType,entityId,operation));
    }

    @Override
    public boolean hasAccess(String entityType, String entityId, EntityOp operation){
        return isAdmin() || roles.stream().anyMatch(r -> r.allowsAccess(entityType,entityId,operation));
    }

    @Override
    public boolean hasAnyAccess(String entityType, EntityOp operation) {
        return hasWildcardAccess(entityType, operation) || roles.stream()
                .anyMatch(r -> r.getEntityType().equals(entityType) && r.getOperation().ordinal() >= operation.ordinal());
    }

    @Override
    public boolean hasWildcardAccess(String entityType, EntityOp operation) {
        return roles.stream()
                .anyMatch(r -> r.allowsWildcardAccess(entityType,operation));
    }

    @Override
    public List<String> getEntityIdsWithAccess(String entityType, EntityOp operation){
        List<String> entities = roles.stream()
                .filter(r -> r.getEntityType().equals(entityType))
                .filter(r -> !r.getEntityId().equals(Role.WILDCARD))
                .filter(r -> r.getOperation().ordinal() >= operation.ordinal())
                .map(Role::getEntityId)
                .collect(Collectors.toList());
        return entities.isEmpty() ? Collections.singletonList(""): entities;
    }

    public static User admin(){
        return User.builder()
                .email("admin")
                .userName("admin")
                .withRole(Role.WILDCARD_ROLE)
                .build();
    }

    public static User standard() {
        return User.builder()
                .email("standard@user")
                .userName("standard")
                .build();
    }
}
