package gov.scot.payments.model.paymentfile;

public enum PaymentFileStatus {

    New, Translated, Failed, Deleted
}
