package gov.scot.payments.model.inboundpayment.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.inboundpayment.InboundPayment;
import lombok.NonNull;
import lombok.Value;

@Value
public class InboundPaymentClearedEvent implements Event<InboundPayment> {

    @JsonCreator
    public InboundPaymentClearedEvent(@JsonProperty("payload") InboundPayment payload) {
        this.payload = payload;
    }

    @NonNull
    private final InboundPayment payload;
}
