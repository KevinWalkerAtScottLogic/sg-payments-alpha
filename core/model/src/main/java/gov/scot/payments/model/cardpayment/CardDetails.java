package gov.scot.payments.model.cardpayment;

import lombok.*;

import java.time.LocalDate;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
public class CardDetails {

    private String issuer;
    private String type;
    private LocalDate expiry;
    private String maskedCardNumber;
    private CardAddress address;
    private String name;

}
