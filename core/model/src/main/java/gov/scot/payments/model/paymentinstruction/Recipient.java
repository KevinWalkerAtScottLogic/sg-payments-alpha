package gov.scot.payments.model.paymentinstruction;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

import javax.persistence.Convert;
import javax.persistence.Embeddable;


@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
@Embeddable
public class Recipient {

    @JsonDeserialize(using = AccountNumber.AccountNumberDeserializer.class)
    @JsonSerialize(using = AccountNumber.AccountNumberSerializer.class)
    @Convert(converter = AccountNumber.JpaAccountNumberConverter.class)
    private AccountNumber accountNumber;

    @JsonDeserialize(using = SortCode.SortCodeDeserializer.class)
    @JsonSerialize(using = SortCode.SortCodeSerializer.class)
    @Convert(converter = SortCode.JpaSortCodeConverter.class)
    private SortCode sortCode;
    private String address;
    private String bic;
    private String email;
    private String iban;
    private Integer mobile;
    private String name;
    private String ref;

}
