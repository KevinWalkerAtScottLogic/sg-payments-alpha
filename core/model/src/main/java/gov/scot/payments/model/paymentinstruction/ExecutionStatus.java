package gov.scot.payments.model.paymentinstruction;

public enum ExecutionStatus {

    Submitted, SubmissionFailed, Accepted, Rejected
}
