package gov.scot.payments.model.paymentinstruction;

public enum PaymentInstructionStatus {

    New,
    Validated,
    Invalid,
    Routed,
    FailedToRoute,
    Submitted,
    SubmissionFailed,
    Accepted,
    Rejected,
    Cleared,
    FailedToClear,
    Settled,
    RecallRequested,
    Recalled,
    Cancelled,
    CancelRequested

}
