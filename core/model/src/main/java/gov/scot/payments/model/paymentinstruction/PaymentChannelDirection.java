package gov.scot.payments.model.paymentinstruction;

public enum PaymentChannelDirection {
    Outbound, Inbound;
}
