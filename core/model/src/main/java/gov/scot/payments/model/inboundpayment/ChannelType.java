package gov.scot.payments.model.inboundpayment;

public enum ChannelType {
    Cheque, Cash, PostalOrder, SMS, Card_IVR, Card_Online, BankTransfer, PayPoint
}
