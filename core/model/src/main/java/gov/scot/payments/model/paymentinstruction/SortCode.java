package gov.scot.payments.model.paymentinstruction;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import lombok.*;
import org.springframework.util.StringUtils;

import javax.persistence.AttributeConverter;
import java.io.IOException;
import java.util.regex.Pattern;

@Value
@EqualsAndHashCode
public class SortCode {

    private static int SORT_CODE_LENGTH = 6;

    private static final Pattern regexNumeric = Pattern.compile("^[0-9]*$");

    private String value;

    public static SortCode fromString(String sortCodeStr) throws InvalidPaymentFieldException {
        if(sortCodeStr.length() != SORT_CODE_LENGTH){
            throw new InvalidPaymentFieldException("Invalid sort-code number "+ sortCodeStr + " - must be 6 digits");
        }
        boolean isNumeric = regexNumeric.matcher(sortCodeStr).matches();
        if(!isNumeric){
            throw new InvalidPaymentFieldException("Invalid sort-code number "+ sortCodeStr + " - must be numeric");
        }

        return new SortCode(sortCodeStr);
    }

    @Override
    public String toString() {
        return value;
    }

    public String toFormattedString(){
        return String.format("%s-%s-%s",value.substring(0,2),value.substring(2,4),value.substring(4,6));
    }

    public static class SortCodeSerializer extends JsonSerializer<SortCode> {


        @Override
        public void serialize(SortCode value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeString(value.toString());
        }
    }

    public static class SortCodeDeserializer extends JsonDeserializer<SortCode> {
        @Override
        public SortCode deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            try {
                return SortCode.fromString(p.getText());
            } catch (InvalidPaymentFieldException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static class JpaSortCodeConverter implements AttributeConverter<SortCode, String> {

        @Override
        public String convertToDatabaseColumn(SortCode attribute) {
            return attribute == null ? null : attribute.toString();
        }

        @Override
        public SortCode convertToEntityAttribute(String dbData) {
            return StringUtils.hasLength(dbData) ? new SortCode(dbData) : null;
        }

    }

}
