package gov.scot.payments.model.paymentinstruction;

import lombok.*;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.List;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
@Entity
public class PaymentProcessor {

    @NonNull @Id private String name;
    @NonNull @ElementCollection(fetch = FetchType.EAGER) @Singular("addChannel") private List<PaymentProcessorChannel> channels;
    private String createdBy;
    @Builder.Default @NonNull private LocalDateTime createdAt = LocalDateTime.now();

}
