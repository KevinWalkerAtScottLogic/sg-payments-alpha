package gov.scot.payments.model.paymentfile;

import lombok.*;

import javax.persistence.*;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.UUID;

@Builder(toBuilder = true)
@Entity
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
public class PaymentFile {

    @NonNull @Enumerated(EnumType.STRING) @Builder.Default private PaymentFileStatus status = PaymentFileStatus.New;
    @NonNull private String service;
    @NonNull @Builder.Default private LocalDateTime createdAt = LocalDateTime.now();
    @NonNull @Builder.Default private LocalDateTime timestamp = LocalDateTime.now();
    @NonNull @Builder.Default @Id private UUID id = UUID.randomUUID();
    @NonNull private String name;
    @NonNull private String type;
    @Convert(converter = UriPersistenceConverter.class)
    @NonNull private URI url;
    @NonNull @Builder.Default private String createdBy = "application";
    private String statusMessage;



}
