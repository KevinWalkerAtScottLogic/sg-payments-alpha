package gov.scot.payments.model.customer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.Embeddable;
import java.time.LocalDateTime;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@Embeddable
@ToString
@EqualsAndHashCode
public class PaymentValidationRule {

    @JsonCreator
    public PaymentValidationRule(@JsonProperty("createdBy") String createdBy
            , @JsonProperty("messageTemplate") String messageTemplate
            , @JsonProperty("name") String name
            , @JsonProperty("precedence") Integer precedence
            , @JsonProperty("rule") String rule ){
        this.createdBy = createdBy;
        this.messageTemplate = messageTemplate;
        this.name = name;
        this.precedence = precedence;
        this.rule = rule;
        this.createdAt = LocalDateTime.now();
    }

    @NonNull @Builder.Default private LocalDateTime createdAt = LocalDateTime.now();
    private String createdBy;
    @NonNull private String messageTemplate;
    @NonNull private String name;
    private int precedence;
    @NonNull private String rule;
}
