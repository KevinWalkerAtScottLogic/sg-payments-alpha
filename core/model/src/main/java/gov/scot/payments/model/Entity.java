package gov.scot.payments.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

import java.nio.charset.Charset;
import java.util.Collections;

public interface Entity {

    String MESSAGE_KEY = "kafka_messageKey";

    @JsonIgnore
    byte[] getKey();

    default Message<? extends Entity> createMessage(){
        return new GenericMessage<>(this, Collections.singletonMap(MESSAGE_KEY,getKey()));
    }

    static byte[] stringToKey(String str){
        return str.getBytes(Charset.defaultCharset());
    }
}
