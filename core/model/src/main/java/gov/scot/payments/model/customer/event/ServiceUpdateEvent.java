package gov.scot.payments.model.customer.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.customer.Service;
import lombok.NonNull;
import lombok.Value;

@Value
public class ServiceUpdateEvent implements Event<Service> {

    @JsonCreator
    public ServiceUpdateEvent(@JsonProperty("payload") Service payload) {
        this.payload = payload;
    }

    @NonNull
    private final Service payload;
}
