package gov.scot.payments.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Transient;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode(callSuper = false)
public class ApiClient extends Client {

    @NonNull private String userName;
    @NonNull @Singular("withScope") private List<Scope> scopes;

    @Override
    public boolean hasSpecificAccess(String entityType, String entityId, EntityOp operation){
        return scopes.stream().anyMatch(r -> r.allowsSpecificAccess(entityType,entityId));
    }

    @Override
    public boolean hasAccess(String entityType, String entityId, EntityOp operation){
        return scopes.stream().anyMatch(r -> r.allowsAccess(entityType,entityId));
    }

    @Override
    public boolean hasAnyAccess(String entityType, EntityOp operation) {
        return hasWildcardAccess(entityType, operation) || scopes.stream()
                .anyMatch(r -> r.getEntityType().equals(entityType));
    }

    @Override
    public boolean hasWildcardAccess(String entityType, EntityOp operation) {
        return scopes.stream()
                .anyMatch(r -> r.allowsWildcardAccess(entityType));
    }

    @Override
    public List<String> getEntityIdsWithAccess(String entityType, EntityOp operation) {
        List<String> entities = scopes.stream()
                .filter(r -> r.getEntityType().equals(entityType))
                .filter(r -> !r.getEntityId().equals(Role.WILDCARD))
                .map(Scope::getEntityId)
                .collect(Collectors.toList());
        return entities.isEmpty() ? Collections.singletonList(""): entities;
    }

    @JsonIgnore
    @Transient
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return scopes.stream().map(Scope::toAuthority).collect(Collectors.toList());
    }
}
