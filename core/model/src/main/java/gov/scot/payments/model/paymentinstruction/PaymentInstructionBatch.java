package gov.scot.payments.model.paymentinstruction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Predicate;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString(exclude = "payments")
@EqualsAndHashCode
@NoArgsConstructor
@Builder(toBuilder = true)
public class PaymentInstructionBatch {

    @NonNull @Builder.Default private String name = UUID.randomUUID().toString();
    @NonNull @Builder.Default private UUID id = UUID.randomUUID();
    @NonNull @Singular("addPayment") private List<PaymentInstruction> payments = new ArrayList<>();

    public PaymentInstructionBatch addPayment(PaymentInstruction payment) {
        return this.toBuilder()
                .addPayment(payment)
                .build();
    }

    public PaymentInstructionBatch merge(PaymentInstructionBatch batch) {
        Set<PaymentInstruction> payments = new HashSet<>(this.payments);
        payments.addAll(batch.getPayments());
        return this.toBuilder()
                .clearPayments()
                .payments(payments)
                .build();
    }

    @JsonIgnore
    public int size() {
        return payments.size();
    }

    @JsonIgnore
    public Optional<PaymentInstruction> getLatestPayment() {
        return payments.stream().max(Comparator.comparing(PaymentInstruction::getTimestamp));
    }

    @JsonIgnore
    public Optional<PaymentInstruction> getEarliestPayment() {
        return payments.stream().min(Comparator.comparing(PaymentInstruction::getTimestamp));
    }

    @JsonIgnore
    public BigDecimal getTotalTargetValue() {
        return payments.stream().map(p -> p.getTargetAmount().getNumberStripped()).reduce(BigDecimal.ZERO,BigDecimal::add);
    }

    public boolean allMatch(Predicate<PaymentInstruction> test) {
        return payments.stream().allMatch(test);
    }

    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    @Getter
    @ToString
    @EqualsAndHashCode
    public class Payment {

        private UUID id;
        private String service;
        private String batchId;
        private String paymentFile;

        public Payment(PaymentInstruction instruction){
            this.id = instruction.getId();
            this.service = instruction.getService();
            this.batchId = instruction.getBatchId();
            this.paymentFile = instruction.getPaymentFile();
        }

    }
}
