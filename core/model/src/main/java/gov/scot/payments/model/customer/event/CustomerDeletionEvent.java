package gov.scot.payments.model.customer.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.model.Event;
import lombok.NonNull;
import lombok.Value;

@Value
public class CustomerDeletionEvent implements Event<String> {

    @JsonCreator
    public CustomerDeletionEvent(@JsonProperty("payload") String payload) {
        this.payload = payload;
    }

    @NonNull
    private final String payload;
}
