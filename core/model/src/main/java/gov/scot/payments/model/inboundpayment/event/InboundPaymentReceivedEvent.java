package gov.scot.payments.model.inboundpayment.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.inboundpayment.InboundPayment;
import lombok.NonNull;
import lombok.Value;

@Value
public class InboundPaymentReceivedEvent implements Event<InboundPayment> {

    @JsonCreator
    public InboundPaymentReceivedEvent(@JsonProperty("payload") InboundPayment payload) {
        this.payload = payload;
    }

    @NonNull
    private final InboundPayment payload;
}
