package gov.scot.payments.model.paymentinstruction;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gov.scot.payments.model.core.MoneyDeserializer;
import gov.scot.payments.model.core.MoneySerializer;
import lombok.*;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;
import org.javamoney.moneta.Money;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
@Embeddable
public class Routing {

    @NonNull @Builder.Default @Enumerated(EnumType.STRING) private final RoutingStatus status = RoutingStatus.Success;
    private String statusMessage;
    private String processor;
    @Enumerated(EnumType.STRING) private PaymentChannel channel;
    private String queue;
    @JsonDeserialize(using = MoneyDeserializer.class)
    @JsonSerialize(using = MoneySerializer.class)
    @Type(type = "gov.scot.payments.model.core.MoneyType")
    @Columns(columns = {@Column(name = "expected_cost"),@Column(name = "expected_cost_currency")})
    private Money expectedCost;
    @NonNull @Builder.Default private final LocalDateTime routedAt = LocalDateTime.now();
}
