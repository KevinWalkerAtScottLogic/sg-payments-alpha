package gov.scot.payments.model.inboundpayment.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.model.NotificationEvent;
import gov.scot.payments.model.inboundpayment.InboundPayment;
import lombok.NonNull;
import lombok.Value;

@Value
public class InboundPaymentAttemptFailedEvent implements NotificationEvent<InboundPayment> {

    @JsonCreator
    public InboundPaymentAttemptFailedEvent(@JsonProperty("payload") InboundPayment payload) {
        this.payload = payload;
    }

    @NonNull
    private final InboundPayment payload;

    @Override
    public String getNotificationMessage() {
        return String.format("Error occurred attempting payment %s", payload);
    }
}
