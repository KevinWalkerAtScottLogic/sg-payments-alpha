package gov.scot.payments.model.report;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Builder(toBuilder = true)
@Entity
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
public class Report {

    @NonNull @Id private String id;
    @NonNull private String name;
    @NonNull private String dashboardId;
    @NonNull private String createdBy;
    @NonNull @Builder.Default private LocalDateTime createdAt = LocalDateTime.now();
}
