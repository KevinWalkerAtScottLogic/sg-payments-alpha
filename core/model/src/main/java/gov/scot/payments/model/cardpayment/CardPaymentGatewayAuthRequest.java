package gov.scot.payments.model.cardpayment;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gov.scot.payments.model.core.MoneyDeserializer;
import gov.scot.payments.model.core.MoneySerializer;
import lombok.*;
import org.javamoney.moneta.Money;

import java.time.LocalDate;
import java.util.UUID;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
public class CardPaymentGatewayAuthRequest {

    @ToString.Exclude private String name;
    @ToString.Exclude private String cardNumber;
    @ToString.Exclude private String cvv;
    @ToString.Exclude private LocalDate expiry;
    @JsonDeserialize(using = MoneyDeserializer.class)
    @JsonSerialize(using = MoneySerializer.class)
    @NonNull private Money amount;
    @NonNull private UUID paymentRef;
}
