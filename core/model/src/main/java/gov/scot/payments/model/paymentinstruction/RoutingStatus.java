package gov.scot.payments.model.paymentinstruction;

public enum RoutingStatus {

    Success, Failure
}
