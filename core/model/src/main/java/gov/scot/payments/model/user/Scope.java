package gov.scot.payments.model.user;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
public class Scope {

    public static final String WILDCARD = "*";

    @NonNull private String entityType;
    @NonNull private String entityId;

    public boolean allowsSpecificAccess(String entityType, String entityId){
        return this.entityType.equals(entityType) && this.entityId.equals(entityId);
    }

    public boolean allowsWildcardAccess(String entityType){
        return this.entityType.equals(entityType) && this.entityId.equals(WILDCARD);
    }

    public boolean allowsAccess(String entityType, String entityId){
        return this.entityType.equals(entityType) && (this.entityId.equals(WILDCARD) || this.entityId.equals(entityId));
    }

    public GrantedAuthority toAuthority() {
        return new SimpleGrantedAuthority(toString());
    }

    @Override
    public String toString(){
        return String.format("%s/%s",entityType,entityId);
    }

    public static Scope fromString(String scope) {
        String[] components = scope.split("/");
        return new Scope(components[0],components[1]);
    }



}
