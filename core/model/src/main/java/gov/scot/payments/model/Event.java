package gov.scot.payments.model;

public interface Event<T> {

    T getPayload();
}
