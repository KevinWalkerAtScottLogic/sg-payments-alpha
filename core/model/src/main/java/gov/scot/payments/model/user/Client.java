package gov.scot.payments.model.user;

import lombok.EqualsAndHashCode;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;

@EqualsAndHashCode
public abstract class Client {

    //get user name
    public abstract String getUserName();
    public abstract boolean hasSpecificAccess(String entityType, String entityId, EntityOp operation);
    public abstract boolean hasAccess(String entityType, String entityId, EntityOp operation);
    public abstract boolean hasAnyAccess(String entityType, EntityOp operation);
    public abstract boolean hasWildcardAccess(String entityType, EntityOp operation);
    public abstract List<String> getEntityIdsWithAccess(String entityType, EntityOp operation);
    public abstract Collection<? extends GrantedAuthority> getAuthorities();
}
