package gov.scot.payments.model.paymentinstruction.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import lombok.NonNull;
import lombok.Value;

@Value
public class PaymentClearedEvent implements Event<PaymentInstruction> {

    @JsonCreator
    public PaymentClearedEvent(@JsonProperty("payload") PaymentInstruction payload) {
        this.payload = payload;
    }

    @NonNull
    private final PaymentInstruction payload;
}
