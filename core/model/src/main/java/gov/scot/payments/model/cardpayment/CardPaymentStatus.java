package gov.scot.payments.model.cardpayment;

import gov.scot.payments.model.inboundpayment.PaymentStatus;

public enum CardPaymentStatus {
    Created //new payment created - initial state
    , Started   //used once the form is loaded
    , Submitted //after authorized
    , Success   //user payment submit success
    , Failed    //user payment failed at any stage
    , Cancelled // user cancelled payment
    , Expired //older than timeout period
    , Error //internal error
    ;

    public PaymentStatus toInboundPaymentStatus() {
        switch (this){
            case Success:
                return PaymentStatus.New;
            case Failed:
            case Cancelled:
            case Expired:
            case Error:
                return PaymentStatus.Failed;
            case Created:
            case Started:
            case Submitted:
            default:
                throw new IllegalStateException("can not report in progress card payment");
        }

    }
}
