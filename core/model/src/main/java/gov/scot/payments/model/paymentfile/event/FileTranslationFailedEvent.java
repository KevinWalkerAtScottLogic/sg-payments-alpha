package gov.scot.payments.model.paymentfile.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.model.NotificationEvent;
import gov.scot.payments.model.paymentfile.PaymentFile;
import lombok.NonNull;
import lombok.Value;

@Value
public class FileTranslationFailedEvent implements NotificationEvent<PaymentFile> {

    @JsonCreator
    public FileTranslationFailedEvent(@JsonProperty("payload") PaymentFile payload) {
        this.payload = payload;
    }

    @NonNull
    private final PaymentFile payload;

    @Override
    public String getNotificationMessage() {
        return String.format("Error occurred for payment file %s, Details: %s",payload.getName(),payload.getStatusMessage());
    }
}
