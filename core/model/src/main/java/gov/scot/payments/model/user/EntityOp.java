package gov.scot.payments.model.user;

public enum EntityOp {
    Read, Write
}
