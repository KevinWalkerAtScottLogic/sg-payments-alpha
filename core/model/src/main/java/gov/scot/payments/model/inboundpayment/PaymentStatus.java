package gov.scot.payments.model.inboundpayment;

public enum PaymentStatus {
    New, Failed, Cleared, Settled, FailedToClear
}
