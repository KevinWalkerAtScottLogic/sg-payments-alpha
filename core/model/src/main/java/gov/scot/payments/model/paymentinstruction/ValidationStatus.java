package gov.scot.payments.model.paymentinstruction;

public enum ValidationStatus {
    Valid,
    Invalid
}
