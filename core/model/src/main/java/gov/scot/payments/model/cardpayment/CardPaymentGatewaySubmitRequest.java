package gov.scot.payments.model.cardpayment;

import lombok.*;
import org.javamoney.moneta.Money;

import java.time.LocalDate;
import java.util.Map;
import java.util.UUID;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
public class CardPaymentGatewaySubmitRequest {

    @NonNull private String ref;
    @NonNull private UUID paymentRef;
    private Map<String,String> metadata;
}
