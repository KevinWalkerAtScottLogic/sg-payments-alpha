package gov.scot.payments.model.paymentfile.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import lombok.NonNull;
import lombok.Value;

@Value
public class PaymentRoutingSuccessEvent implements Event<PaymentInstruction> {

    @JsonCreator
    public PaymentRoutingSuccessEvent(@JsonProperty("payload") PaymentInstruction payload) {
        this.payload = payload;
    }

    @NonNull
    private final PaymentInstruction payload;
}
