package gov.scot.payments.model.user;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
public class Role {

    public static final String WILDCARD = "*";
    public static final Role WILDCARD_ROLE = new Role(EntityOp.Write,WILDCARD,WILDCARD);

    @NonNull @Enumerated(value = EnumType.STRING) private EntityOp operation;
    @NonNull private String entityType;
    @NonNull private String entityId;

    public boolean allowsSpecificAccess(String entityType, String entityId, EntityOp operation){
        return this.entityType.equals(entityType)
                && this.entityId.equals(entityId)
                && this.operation.ordinal() >= operation.ordinal();
    }

    public boolean allowsAccess(String entityType, String entityId, EntityOp operation){
        return (this.entityType.equals(WILDCARD) || this.entityType.equals(entityType))
                && (this.entityId.equals(WILDCARD) || this.entityId.equals(entityId))
                && (this.operation.ordinal() >= operation.ordinal());
    }

    public boolean allowsWildcardAccess(String entityType, EntityOp operation) {
        return entityId.equals(Role.WILDCARD)
                && (this.entityType.equals(WILDCARD) || this.entityType.equals(entityType))
                && this.operation.ordinal() >= operation.ordinal();
    }

    public GrantedAuthority toAuthority() {
        return new SimpleGrantedAuthority(toString());
    }

    @Override
    public String toString(){
        return String.format("%s:%s:%s",entityType,entityId,operation);
    }

    public static Role fromString(String role) {
        String[] components = role.split(":");
        EntityOp operation = EntityOp.valueOf(components[2]);
        return new Role(operation,components[0],components[1]);
    }



}
