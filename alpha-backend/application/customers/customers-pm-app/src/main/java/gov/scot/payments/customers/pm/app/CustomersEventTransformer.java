package gov.scot.payments.customers.pm.app;

import gov.scot.payments.application.component.processmanager.EventTransformer;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.customers.model.aggregate.CustomerStatus;
import gov.scot.payments.customers.model.aggregate.Product;
import gov.scot.payments.customers.model.aggregate.ProductStatus;
import gov.scot.payments.customers.model.command.*;
import gov.scot.payments.customers.model.event.DeleteCustomerApprovedEvent;
import gov.scot.payments.customers.model.event.ProductDeletedEvent;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.Event;
import gov.scot.payments.payments.model.event.PaymentValidEvent;
import gov.scot.payments.paymentfiles.model.event.FileUploadedEvent;
import gov.scot.payments.users.model.UserFileResourcesDeletedEvent;
import gov.scot.payments.users.model.UserFileResourcesUpdatedEvent;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;

public class CustomersEventTransformer extends EventTransformer.PatternMatching {

    @Override
    protected void handlers(PatternMatcher.Builder<Event, List<Command>> builder) {
        builder
                .match(UserFileResourcesUpdatedEvent.class, this::finalizeProductCreation)
                .match(UserFileResourcesDeletedEvent.class, this::finalizeProductDeletion)
                .match(DeleteCustomerApprovedEvent.class, this::beginCustomerDeletion)
                .match(ProductDeletedEvent.class, this::finalizeCustomerDeletion)
                .match(FileUploadedEvent.class, this::beginFileParsing)
                .match(PaymentValidEvent.class, this::verifyPayment)
                .orElse(List.empty());
    }

    private List<Command> verifyPayment(PaymentValidEvent e) {
        return List.of(new VerifyPaymentCommand(e.getCarriedState(),false));
    }

    List<Command> finalizeProductCreation(UserFileResourcesUpdatedEvent e) {
        /*
         * Using UserFileResourcesUpdatedEvent as the event signalling that the various actions
         * triggered by a CreateProductApprovedEvent have completed (at least enough to move on the
         * Product status).
         */

        if (e.getScope().getParent() == null){
            // on creating a customer we do not want to issue a FinalizeCreateProductCommand.
            return List.empty();
        }

        var finalizeCreateProductCommand = FinalizeCreateProductCommand.builder()
                .productId(new CompositeReference(e.getScope().getParent().getName(), e.getScope().getName()))
                .reply(false)
                .build();

        return List.of(finalizeCreateProductCommand);
    }

    List<Command> finalizeProductDeletion(UserFileResourcesDeletedEvent e) {

        /*
         * Using UserFileResourcesDeletedEvent as the event signalling that the various actions
         * triggered by a DeleteProductApprovedEvent have completed (at least enough to move on the
         * Product status).
         */

        var finalizeDeleteProductCommand = FinalizeDeleteProductCommand.builder()
                .productId(new CompositeReference(e.getScope().getParent().getName(), e.getScope().getName()))
                .reply(false)
                .build();

        return List.of(finalizeDeleteProductCommand);
    }

    List<Command> beginCustomerDeletion(DeleteCustomerApprovedEvent e) {

        if (e.getCarriedState().getStatus() != CustomerStatus.Deleting) {
            throw new IllegalStateException(
                    String.format("Customer \"%s\" is in illegal state for beginCustomerDeletion, should be \"%s\", was \"%s\"",
                            e.getCarriedState().getName(), CustomerStatus.Deleting, e.getCarriedState().getStatus())
            );
        }

        var products = e.getCarriedState().getProducts();
        if (products == null) {
            return List.empty();
        }

        // Send commands to delete all products that are not already finalized
        return products
                .filter(this::productNotFinalized)
                .map(p -> (Command) ApproveDeleteProductCommand.builder()
                        .productId(new CompositeReference(e.getCarriedState().getName(), p.getName()))
                        .forceDelete(true)
                        .user(e.getUser())
                        .roles(HashSet.empty())
                        .build())
                .toList();
    }

    List<Command> finalizeCustomerDeletion(ProductDeletedEvent e) {

        var products = e.getCarriedState().getProducts();
        if (products == null) {
            return List.empty();
        }

        if (e.getCarriedState().getStatus() == CustomerStatus.Deleting) {
            if (products.find(this::productNotFinalized).isEmpty()) {
                return List.of(
                        FinalizeDeleteCustomerCommand.builder()
                                .customerId(e.getCarriedState().getName())
                                .build()
                );
            }
        }

        return List.empty();
    }

    List<Command> beginFileParsing(FileUploadedEvent e) {
        var parseFileCommand = ParseFileCommand.builder()
                .file(e.getPath())
                .product(e.getProduct())
                .user(e.getUser())
                .createdAt(e.getCreatedAt())
                .reply(false)
                .build();

        return List.of(parseFileCommand);
    }


    private boolean productNotFinalized(Product product) {
        var status = product.getStatus();
        return status != ProductStatus.Deleted && status != ProductStatus.Rejected;
    }
}
