package gov.scot.payments.customers.pm.app;

import gov.scot.payments.customers.model.aggregate.*;
import gov.scot.payments.payments.model.aggregate.*;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import lombok.SneakyThrows;

import java.net.URL;

import static java.time.Instant.now;

public class CustomersPMTestBase {

    static final String CUSTOMER = "test-customer";
    static final String PRODUCT_1 = "test-product-1";
    static final String PRODUCT_2 = "test-product-2";
    static final String PRODUCT_3 = "test-product-3";
    static final String USER = "test-user";

    Customer.CustomerBuilder testCustomerBuilder() {
        return Customer.builder()
                .name(CUSTOMER)
                .createdAt(now())
                .createdBy(USER)
                .lastModifiedBy(USER)
                .status(CustomerStatus.Deleting);
    }

    @SneakyThrows
    Product testProduct(String name, ProductStatus status) {

        String location = "http:\\\\some/service/endpoint";
        var bankAccount = UKBankAccount.builder()
                .name("test-bank-account")
                .currency("GBP")
                .sortCode(SortCode.fromString("112233"))
                .accountNumber(UKAccountNumber.fromString("12345678"))
                .build();

        return Product.builder()
                .name(name)
                .createdAt(now())
                .status(status)
                .productConfiguration(
                        ProductConfiguration.builder()
                                .adapter(ProductAdapter.builder()
                                        .type(ProductAdapterType.internal)
                                        .location(location)
                                        .build())
                                .outboundConfiguration(OutboundConfiguration.builder()
                                        .source(bankAccount)
                                        .returns(bankAccount)
                                        .defaultPaymentMethods(List.of(PaymentMethod.Bacs))
                                        .payer(PartyIdentification.builder().name("cust").build())
                                        .validationRules(
                                                HashSet.empty()
                                        )
                                        .build())
                                .build())
                .build();
    }
}
