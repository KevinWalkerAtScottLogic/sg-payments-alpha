package gov.scot.payments.customers.pm.app;

import gov.scot.payments.customers.model.aggregate.CustomerStatus;
import gov.scot.payments.customers.model.aggregate.ProductStatus;
import gov.scot.payments.customers.model.command.*;
import gov.scot.payments.customers.model.event.DeleteCustomerApprovedEvent;
import gov.scot.payments.customers.model.event.ProductDeletedEvent;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.user.Scope;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.event.PaymentValidEvent;
import gov.scot.payments.users.model.UserFileResourcesDeletedEvent;
import gov.scot.payments.users.model.UserFileResourcesUpdatedEvent;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CustomersEventTransformerTest extends CustomersPMTestBase {

    @Test
    @DisplayName("When finalizeProductCreation called then a FinalizeCreateProductCommand is returned")
    void testFinalizeProductCreation() {
        var triggerEvent = UserFileResourcesUpdatedEvent.builder()
                .scope(new Scope(PRODUCT_1, new Scope(CUSTOMER)))
                .build();

        var result = new CustomersEventTransformer().finalizeProductCreation(triggerEvent);

        assertThat(result.size(), is(1));
        assertThat(result.get(0).getClass(), is(FinalizeCreateProductCommand.class));
        var command = (FinalizeCreateProductCommand)result.get(0);
        assertThat(command.getProductId(), is(new CompositeReference(CUSTOMER, PRODUCT_1)));
        assertThat(command.isReply(), is(false));
    }

    @Test
    @DisplayName("When finalizeProductDeletion called then a FinalizeDeleteProductCommand is returned")
    void testFinalizeProductDeletion() {
        var triggerEvent = UserFileResourcesDeletedEvent.builder()
                .scope(new Scope(PRODUCT_1, new Scope(CUSTOMER)))
                .build();

        var result = new CustomersEventTransformer().finalizeProductDeletion(triggerEvent);

        assertThat(result.size(), is(1));
        assertThat(result.get(0).getClass(), is(FinalizeDeleteProductCommand.class));
        var command = (FinalizeDeleteProductCommand)result.get(0);
        assertThat(command.getProductId(), is(new CompositeReference(CUSTOMER, PRODUCT_1)));
        assertThat(command.isReply(), is(false));
    }

    @Test
    @DisplayName("Given Customer status is not Deleting when beginCustomerDeletion called then an exception is thrown")
    void testBeginCustomerDeletionWithInvalidCustomerStatus() {
        var triggerEvent = DeleteCustomerApprovedEvent.builder()
                .carriedState(testCustomerBuilder()
                        .status(CustomerStatus.Live)
                        .build())
                .user(USER)
                .stateVersion(99L)
                .build();

        var e = assertThrows(IllegalStateException.class, () -> new CustomersEventTransformer().beginCustomerDeletion(triggerEvent));

        assertThat(e.getMessage(), is(String.format(
                "Customer \"%s\" is in illegal state for beginCustomerDeletion, should be \"%s\", was \"%s\"",
                CUSTOMER, CustomerStatus.Deleting, CustomerStatus.Live)
        ));
    }

    @Test
    @DisplayName("Given no products when beginCustomerDeletion called then no commands are returned")
    void testBeginCustomerDeletionWithNoProducts() {
        var triggerEvent = DeleteCustomerApprovedEvent.builder()
                .carriedState(testCustomerBuilder().build())
                .user(USER)
                .stateVersion(99L)
                .build();

        var result = new CustomersEventTransformer().beginCustomerDeletion(triggerEvent);

        assertThat(result.size(), is(0));
    }

    @Test
    @DisplayName("Given all products in final status when beginCustomerDeletion called then no commands are returned")
    void testBeginCustomerDeletionWithOnlyFinalizedProducts() {

        var products = HashSet.of(
                testProduct(PRODUCT_1, ProductStatus.Deleted),
                testProduct(PRODUCT_2, ProductStatus.Rejected),
                testProduct(PRODUCT_3, ProductStatus.Deleted)
        );

        var triggerEvent = DeleteCustomerApprovedEvent.builder()
                .carriedState(testCustomerBuilder().products(products).build())
                .user(USER)
                .stateVersion(99L)
                .build();

        var result = new CustomersEventTransformer().beginCustomerDeletion(triggerEvent);

        assertThat(result.size(), is(0));
    }

    @Test
    @DisplayName("Given some products not in final status when beginCustomerDeletion called then some commands are returned")
    void testBeginCustomerDeletionWithSomeNonFinalizedProducts() {

        var products = HashSet.of(
                testProduct(PRODUCT_1, ProductStatus.Live),
                testProduct(PRODUCT_2, ProductStatus.Rejected),
                testProduct(PRODUCT_3, ProductStatus.Deleting)
        );

        var triggerEvent = DeleteCustomerApprovedEvent.builder()
                .carriedState(testCustomerBuilder().products(products).build())
                .user(USER)
                .stateVersion(99L)
                .build();

        var result = new CustomersEventTransformer().beginCustomerDeletion(triggerEvent);

        assertThat(result.size(), is(2));
        assertThat(result.get(0).getClass(), is(ApproveDeleteProductCommand.class));
        assertThat(result.get(1).getClass(), is(ApproveDeleteProductCommand.class));
        ApproveDeleteProductCommand product_1_command;
        ApproveDeleteProductCommand product_3_command;
        if (((ApproveDeleteProductCommand)result.get(0)).getProductId().getComponent1().equals(PRODUCT_1)) {
            product_1_command = (ApproveDeleteProductCommand)result.get(0);
            product_3_command = (ApproveDeleteProductCommand)result.get(1);
        } else {
            product_1_command = (ApproveDeleteProductCommand)result.get(1);
            product_3_command = (ApproveDeleteProductCommand)result.get(0);
        }
        assertThat(product_1_command.getProductId().getComponent0(), is(CUSTOMER));
        assertThat(product_1_command.getProductId().getComponent1(), is(PRODUCT_1));
        assertThat(product_1_command.isForceDelete(), is(true));
        assertThat(product_1_command.getUser(), is(USER));
        assertThat(product_1_command.getRoles().size(), is(0));
        assertThat(product_3_command.getProductId().getComponent0(), is(CUSTOMER));
        assertThat(product_3_command.getProductId().getComponent1(), is(PRODUCT_3));
        assertThat(product_3_command.isForceDelete(), is(true));
        assertThat(product_3_command.getUser(), is(USER));
        assertThat(product_3_command.getRoles().size(), is(0));
    }

    @Test
    @DisplayName("Given no products when finalizeCustomerDeletion called then no commands are returned")
    void testFinalizeCustomerDeletionWithNoProducts() {
        var triggerEvent = ProductDeletedEvent.builder()
                .productId(PRODUCT_1)
                .carriedState(testCustomerBuilder().build())
                .user(USER)
                .build();

        var result = new CustomersEventTransformer().finalizeCustomerDeletion(triggerEvent);

        assertThat(result.size(), is(0));
    }

    @Test
    @DisplayName("Given some unfinalized products when finalizeCustomerDeletion called then no commands are returned")
    void testFinalizeCustomerDeletionWithUnfinalizedProducts() {

        var products = HashSet.of(
                testProduct(PRODUCT_1, ProductStatus.Live),
                testProduct(PRODUCT_2, ProductStatus.Rejected),
                testProduct(PRODUCT_3, ProductStatus.Deleting)
        );

        var triggerEvent = ProductDeletedEvent.builder()
                .productId(PRODUCT_1)
                .carriedState(testCustomerBuilder().products(products).build())
                .user(USER)
                .build();

        var result = new CustomersEventTransformer().finalizeCustomerDeletion(triggerEvent);

        assertThat(result.size(), is(0));
    }

    @Test
    @DisplayName("Given only finalized products when finalizeCustomerDeletion called then a FinalizeDeleteCustomerCommand is returned")
    void testFinalizeCustomerDeletionWithOnlyFinalizedProducts() {

        var products = HashSet.of(
                testProduct(PRODUCT_1, ProductStatus.Deleted),
                testProduct(PRODUCT_2, ProductStatus.Rejected),
                testProduct(PRODUCT_3, ProductStatus.Deleted)
        );

        var triggerEvent = ProductDeletedEvent.builder()
                .productId(PRODUCT_1)
                .carriedState(testCustomerBuilder().products(products).build())
                .user(USER)
                .build();

        var result = new CustomersEventTransformer().finalizeCustomerDeletion(triggerEvent);

        assertThat(result.size(), is(1));
        assertThat(result.get(0).getClass(), is(FinalizeDeleteCustomerCommand.class));
        var command = (FinalizeDeleteCustomerCommand)result.get(0);
        assertThat(command.getCustomerId(), is(CUSTOMER));
    }

    @Test
    void testVerifyPayment() {

        var event = PaymentValidEvent.builder()
                .carriedState(Payment.builder()
                        .createdBy("user")
                        .allowedMethods(List.of(PaymentMethod.Bacs))
                        .creditor(PartyIdentification.builder().name("").build())
                        .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                     .debtor(PartyIdentification.builder().name("person").build())
                                     .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                     .amount(Money.parse("GBP 1.00"))
                        .latestExecutionDate(Instant.now())
                        .product(new CompositeReference(CUSTOMER, PRODUCT_1))
                        .build())
                .build();

        var result = new CustomersEventTransformer().apply(event);

        assertThat(result.size(), is(1));
        assertThat(result.get(0).getClass(), is(VerifyPaymentCommand.class));
        var command = (VerifyPaymentCommand)result.get(0);
        assertThat(command.getKey(), is(CUSTOMER));
    }

}
