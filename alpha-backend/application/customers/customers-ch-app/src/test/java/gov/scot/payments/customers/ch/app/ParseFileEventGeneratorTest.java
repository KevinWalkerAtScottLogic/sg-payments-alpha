package gov.scot.payments.customers.ch.app;

import gov.scot.payments.customers.model.aggregate.*;
import gov.scot.payments.customers.model.api.CustomerPaymentBatch;
import gov.scot.payments.customers.model.command.ParseFileCommand;
import gov.scot.payments.customers.model.event.CustomerPaymentCreatedEvent;
import gov.scot.payments.customers.model.event.PaymentFileParsingEndEvent;
import gov.scot.payments.customers.model.event.PaymentFileParsingFailedEvent;
import gov.scot.payments.customers.model.event.PaymentFileParsingStartEvent;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.api.CreatePaymentRequest;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import software.amazon.awssdk.core.sync.ResponseTransformer;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.time.Instant;
import java.util.UUID;
import javax.money.Monetary;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ParseFileEventGeneratorTest {

    private String customerName = "customerName";
    private String productName = "customerName";
    private String batchId = "5Ft45";
    private String bucketUrl = "bucketUrl";
    private String versionId = "versionId";
    private String key = "dummy-branch/customer/product/fileName.csv";


    private CustomerWebClientAdapter customerWebClientAdapter;
    private S3Client s3Client;
    private ParseFileEventGenerator parseFileEventGenerator;
    private CompositeReference compositeReference;
    private URI uri;


    @BeforeEach
    public void setUp() {
        customerWebClientAdapter = mock(CustomerWebClientAdapter.class);
        s3Client = mock(S3Client.class);
        parseFileEventGenerator = new ParseFileEventGenerator(customerWebClientAdapter, s3Client);
        compositeReference = new CompositeReference(customerName, productName);
        String urlString = String.format("s3://%s/%s#%s", bucketUrl, key, versionId);
        uri = URI.create(urlString);
    }

    @Test
    @DisplayName("test getObjectRequest contains the correct values")
    public void testFileRequestToAwsContainsCorrectValues() {
        ArgumentCaptor<GetObjectRequest> argumentCaptor = ArgumentCaptor.forClass(GetObjectRequest.class);
        parseFileEventGenerator.getFileFromUri(uri);
        verify(s3Client, times(1)).getObject(argumentCaptor.capture(), any(ResponseTransformer.class));

        var getObjectRequest = argumentCaptor.getValue();
        assertEquals(versionId, getObjectRequest.versionId());
        assertEquals(bucketUrl, getObjectRequest.bucket());
        assertEquals(key, getObjectRequest.key());
    }

    @Test
    @DisplayName("convertPaymentsToEventList with no payments in batch returns start event and end event only")
    public void testEmptyPaymentsListReturnsStartAndEndEvents() throws MalformedURLException {

        var customer = generateCustomer();
        var command = generateParseFileCommand();
        var createdEvents = parseFileEventGenerator.convertPaymentsToEventList(batchId, List.empty(), customer, command);

        assertEquals(2, createdEvents.size());
        assertTrue(createdEvents.get(0) instanceof PaymentFileParsingStartEvent);
        assertTrue(createdEvents.get(1) instanceof PaymentFileParsingEndEvent);

    }

    @Test
    @DisplayName("convertPaymentsToEventList returns events in the correct order")
    public void testEventMethodsAreReturnedInCorrectOrder() throws MalformedURLException {

        var customer = generateCustomer();
        var command = generateParseFileCommand();
        var payment = createPayment(UUID.randomUUID());
        var createdEvents = parseFileEventGenerator.convertPaymentsToEventList(batchId, List.of(payment), customer, command);

        assertEquals(3, createdEvents.size());
        assertTrue(createdEvents.get(0) instanceof PaymentFileParsingStartEvent);
        assertTrue(createdEvents.get(1) instanceof CustomerPaymentCreatedEvent);
        assertTrue(createdEvents.get(2) instanceof PaymentFileParsingEndEvent);
    }

    @Test
    @DisplayName("PaymentFileParsingStartEvent contains a list of payment Ids of the payments")
    public void testParseStartEventContainsPaymentIds() throws MalformedURLException {
        var customer = generateCustomer();
        var command = generateParseFileCommand();
        UUID uuid = UUID.randomUUID();
        UUID uuid2 = UUID.randomUUID();
        UUID uuid3 = UUID.randomUUID();

        var paymentList = List.of(createPayment(uuid), createPayment(uuid2), createPayment(uuid3));
        var createdEvents = parseFileEventGenerator.convertPaymentsToEventList(batchId, paymentList, customer, command);
        var parsingStartEvent = (PaymentFileParsingStartEvent) createdEvents.get(0);
        var paymentIds = parsingStartEvent.getPaymentIds();
        assertEquals(3, paymentIds.size());
        assertTrue(paymentIds.contains(uuid));
        assertTrue(paymentIds.contains(uuid2));
        assertTrue(paymentIds.contains(uuid3));
    }

    @Test
    @DisplayName("Test customer payment batch are converted into the Events with correct headers")
    public void testParseFileEventsAreGeneratedCorrectly() throws IOException {
        var customer = generateCustomer();
        var command = generateParseFileCommand();
        String batchId = "12345";
        String batchId2 = "12333";
        String batchId3 = "12444";

        var batch = List.of(generateRequest(batchId), generateRequest(batchId2));
        var batch2 = List.of(generateRequest(batchId));
        var batch3 = List.of(generateRequest(batchId), generateRequest(batchId2), generateRequest(batchId3));

        var custBatch = new CustomerPaymentBatch(batchId, batch);
        var custBatch2 = new CustomerPaymentBatch(batchId2, batch2);
        var custBatch3 = new CustomerPaymentBatch(batchId3, batch3);

        InputStream is = new ByteArrayInputStream("test string".getBytes());
        when(s3Client.getObject(any(GetObjectRequest.class), any(ResponseTransformer.class))).thenReturn(is);
        when(customerWebClientAdapter.sendFileToAdapter(any(InputStream.class), anyString(), any(CompositeReference.class), anyString()))
                .thenReturn(List.of(custBatch, custBatch2, custBatch3));

        List<EventWithCauseImpl> paymentEvents = parseFileEventGenerator.createEventsFromFile(command, customer);
        assertEquals(12, paymentEvents.length());

        var startEvent = (PaymentFileParsingStartEvent) paymentEvents.get(0);
        var startEvent2 = (PaymentFileParsingStartEvent) paymentEvents.get(4);
        var startEvent3 = (PaymentFileParsingStartEvent) paymentEvents.get(7);

        assertEquals(command.getFile(), startEvent.getUri());
        assertEquals(command.getFile(), startEvent2.getUri());
        assertEquals(command.getFile(), startEvent3.getUri());

        assertEquals(2, startEvent.getPaymentIds().size());
        assertEquals(1, startEvent2.getPaymentIds().size());
        assertEquals(3, startEvent3.getPaymentIds().size());

        assertEquals(batchId, startEvent.getCustomerPaymentBatchId());
        assertEquals(batchId2, startEvent2.getCustomerPaymentBatchId());
        assertEquals(batchId3, startEvent3.getCustomerPaymentBatchId());
    }

    @Test
    @DisplayName("Non unique batch ids results in a PaymentFileParsingFailedEvent being returned")
    public void testNonUniqueBatchIdsResultsInFailedEvent() throws IOException {
        var customer = generateCustomer();
        var command = generateParseFileCommand();
        String batchId = "12345";
        String batchId2 = "12333";
        String batchId3 = "12444";

        var batch = List.of(generateRequest(batchId), generateRequest(batchId2));
        var batch2 = List.of(generateRequest(batchId));
        var batch3 = List.of(generateRequest(batchId), generateRequest(batchId2), generateRequest(batchId3));

        var custBatch = new CustomerPaymentBatch(batchId, batch);
        var custBatch2 = new CustomerPaymentBatch(batchId, batch2);
        var custBatch3 = new CustomerPaymentBatch(batchId, batch3);

        InputStream is = new ByteArrayInputStream("test string".getBytes());
        when(s3Client.getObject(any(GetObjectRequest.class), any(ResponseTransformer.class))).thenReturn(is);
        when(customerWebClientAdapter.sendFileToAdapter(any(InputStream.class), anyString(), any(CompositeReference.class), anyString()))
                .thenReturn(List.of(custBatch, custBatch2, custBatch3));

        List<EventWithCauseImpl> events = parseFileEventGenerator.createEventsFromFile(command, customer);
        assertEquals(1, events.size());
        assertTrue(events.get(0) instanceof PaymentFileParsingFailedEvent);

        var parsingFailedEvent = (PaymentFileParsingFailedEvent) events.get(0);
        assertEquals(command.getFile(), parsingFailedEvent.getUri());
        assertEquals(command.getProduct().getComponent1(), parsingFailedEvent.getProductId());
    }

    private Customer generateCustomer() throws MalformedURLException {
        var approval = createCustApproval();
        return Customer.builder()
                .name("customer name")
                .createdAt(Instant.now())
                .status(CustomerStatus.Live)
                .products(HashSet.of(createProduct()))
                .createdBy("")
                .lastModifiedBy("")
                .deleteApproval(approval)
                .approval(approval)
                .build();
    }

    private ParseFileCommand generateParseFileCommand() {
        return ParseFileCommand.builder()
                .file(uri)
                .product(compositeReference)
                .createdAt(Instant.now())
                .user("user name")
                .build();
    }

    private Payment createPayment(UUID id) {
        var bankAccount = new UKBankAccount(SortCode.fromString("012345"), UKAccountNumber.fromString("12345678"));
        return Payment.builder()
                .id(id)
                .createdBy("")
                .allowedMethods(List.empty())
                .creditorAccount(bankAccount)
                .creditor(new PartyIdentification())
                      .debtorAccount(bankAccount)
                      .debtor(new PartyIdentification())
                .amount(Money.zero(Monetary.getCurrency("USD")))
                .latestExecutionDate(Instant.now())
                .product(compositeReference)
                .build();
    }

    private CreatePaymentRequest generateRequest(String batchId) {
        var bankAccount = new UKBankAccount(SortCode.fromString("012345"), UKAccountNumber.fromString("12345678"));
        return CreatePaymentRequest.builder()
                .batchId(batchId)
                .allowedMethods(List.empty())
                .creditorAccount(bankAccount)
                .creditor(new PartyIdentification())
                .amount(Money.zero(Monetary.getCurrency("USD")))
                .latestExecutionDate(Instant.now())
                .product(compositeReference)
                .build();
    }

    private Product createProduct() throws MalformedURLException {
        var approval = createProdApproval();
        var config = createConfiguration();
        return Product.builder()
                .name(productName)
                .status(ProductStatus.Live)
                .createdAt(Instant.now())
                .productConfiguration(config)
                .pendingProductConfiguration(config)
                .approval(approval)
                .deleteApproval(approval)
                .build();
    }

    private CustomerApproval createCustApproval() {
        return CustomerApproval.builder()
                .user("dummy user")
                .at(Instant.now())
                .message("approval message")
                .approved(true)
                .build();
    }

    private ProductApproval createProdApproval() {
        return ProductApproval.builder()
                .user("")
                .message("")
                .at(Instant.now())
                .build();
    }


    private ProductConfiguration createConfiguration() throws MalformedURLException {
        var adapter = ProductAdapter.builder()
                .type(ProductAdapterType.external)
                .location("https://testadapter.com")
                .build();
        var bankAccount = new UKBankAccount(SortCode.fromString("012345"), UKAccountNumber.fromString("12345678"));
        return ProductConfiguration.builder()
                .adapter(adapter)
                .outboundConfiguration(OutboundConfiguration.builder()
                                                            .fileImports(true)
                                                            .defaultPaymentMethods(List.of(PaymentMethod.Bacs))
                                                            .source(bankAccount)
                                                            .returns(bankAccount)
                                                            .payer(new PartyIdentification())
                                                            .build())
                .build();
    }

}
