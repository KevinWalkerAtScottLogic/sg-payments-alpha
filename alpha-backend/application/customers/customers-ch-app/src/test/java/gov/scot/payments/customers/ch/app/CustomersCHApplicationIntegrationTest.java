package gov.scot.payments.customers.ch.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.component.commandhandler.stateful.EventGeneratorFunction;
import gov.scot.payments.customers.model.aggregate.*;
import gov.scot.payments.customers.model.aggregate.Validation;
import gov.scot.payments.customers.model.api.CustomerPaymentBatch;
import gov.scot.payments.customers.model.api.CustomerRejectRequest;
import gov.scot.payments.customers.model.api.ProductRejectRequest;
import gov.scot.payments.customers.model.api.ProductRequest;
import gov.scot.payments.customers.model.command.*;
import gov.scot.payments.customers.model.event.*;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.api.CreatePaymentRequest;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import gov.scot.payments.testing.security.MockSubjectAuthentication;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import org.apache.kafka.streams.KafkaStreams;
import org.checkerframework.checker.units.qual.C;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.client.WebClient;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.core.sync.ResponseTransformer;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import javax.money.Monetary;

import static gov.scot.payments.testing.matchers.ISODateMatcher.isWithin;
import static gov.scot.payments.testing.security.MockSubjectAuthentication.user;
import static java.time.Instant.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockAuthentication;

@ApplicationIntegrationTest(classes = {CustomersCHApplicationIntegrationTest.TestApplication.class}
        , componentType = ApplicationIntegrationTest.ComponentType.COMMAND_HANDLER
        , properties = {"cloud.aws.credentials.accessKey=test"
        , "cloud.aws.credentials.secretKey=test"
        , "cloud.aws.region.static=eu-west-2"
        , "file.event.queue=test"})
public class CustomersCHApplicationIntegrationTest {

    private static final String APPROVAL_USER = "approvalUser";
    private static final String TEST_USER = "testUser";
    private static final String TEST_CUSTOMER = "testCustomer";
    private static final String PRODUCT_1 = "Product1";
    private static final String PRODUCT_2 = "Product2";
    private static final String PRODUCT_1_LOCATION = "https:\\\\product1/parseFile";
    private static final String PRODUCT_2_LOCATION = "https:\\\\product2/parseFile";
    private static final String BANK_ACCOUNT_1 = "BankAccount1";
    private static final String BANK_ACCOUNT_1B = "BankAccount1b";
    private static final String BANK_ACCOUNT_2 = "BankAccount2";
    private static final String BANK_ACCOUNT_2B = "BankAccount2b";
    private static final String GBP = "GBP";
    private static final String SORTCODE_1 = "111111";
    private static final String SORTCODE_1B = "111199";
    private static final String SORTCODE_2 = "222222";
    private static final String SORTCODE_2B = "222299";
    private static final String ACCOUNT_NUMBER_1 = "11111111";
    private static final String ACCOUNT_NUMBER_1B = "11111199";
    private static final String ACCOUNT_NUMBER_2 = "22222222";
    private static final String ACCOUNT_NUMBER_2B = "22222299";
    private static final String RULE_A = "RuleA";
    private static final String MESSAGE_TEMPLATE_1 = "Msg1";
    private static final String CUSTOMER_REJECT_REASON = "Rejecting Customer";
    private static final String PRODUCT_REJECT_REASON = "Rejecting Product";

    private static final String COMMANDS_TOPIC = "commands";
    public static final String EVENTS_TOPIC = "events";

    @MockBean
    S3Client s3Client;

    @MockBean
    CustomerWebClientAdapter customerWebClientAdapter;

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, EmbeddedBrokerClient brokerClient, @Value("${namespace}") String namespace) {
        brokerClient.getBroker().addTopicsIfNotExists(namespace + "-customers-command-errors");
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
    }

    @Test
    void testUnknownCommand(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient) {
        client
                .mutateWith(mockAuthentication(user(TEST_USER, Role.parse("customers/customers:CreateCustomer"))))
                .put()
                .uri(uri -> uri.path("/command/reticulateSplines")
                        .queryParam("reply", true)
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(jsonString(TEST_CUSTOMER))
                .exchange()
                .expectStatus()
                .isNotFound()
        ;
    }

    @Test
    public void endToEndTest(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient) throws MalformedURLException {

        // Request to create a customer

        var response = client
                .mutateWith(mockAuthentication(user(TEST_USER, Role.parse("customers/customers:CreateCustomer"))))
                .put()
                .uri(uri -> uri.path("/command/createCustomer")
                        .queryParam("reply", true)
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(jsonString(TEST_CUSTOMER))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(TEST_USER)
                .jsonPath("$.stateVersion").isEqualTo("1")
                .jsonPath("$.customerId").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(5).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.New.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval").doesNotExist()
                .jsonPath("$.carriedState.products").doesNotExist()
                .returnResult();

        var stateVersion = response.getResponseHeaders().getETag();

        // Try to approve own create request => access denied
        client
                .mutateWith(mockAuthentication(user(TEST_USER, Role.parse("customers/customers:ApproveCustomerModification"))))
                .put()
                .uri(uri -> uri.path("/command/approveCreateCustomer")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(jsonString(TEST_CUSTOMER))
                .exchange()
                .expectStatus()
                .isForbidden()
                .expectBody()
                .jsonPath("$.reason").isEqualTo("Access Denied")
        ;

        // Reject request to create a customer

        var rejectRequest = CustomerRejectRequest.builder().customerId(TEST_CUSTOMER).reason(CUSTOMER_REJECT_REASON).build();

        response = client
                .mutateWith(mockAuthentication(user(APPROVAL_USER, Role.parse("customers/customers:ApproveCustomerModification"))))
                .put()
                .uri(uri -> uri.path("/command/rejectCreateCustomer")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(rejectRequest)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.stateVersion").isEqualTo(incrStateVersion(stateVersion))
                .jsonPath("$.reason").isEqualTo(CUSTOMER_REJECT_REASON)
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(5).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.Rejected.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval.approved").isEqualTo(false)
                .jsonPath("$.carriedState.approval.at").value(isWithin(5).secondsOf(now()))
                .jsonPath("$.carriedState.products").doesNotExist()
                .returnResult()
        ;
        stateVersion = response.getResponseHeaders().getETag();

        // Resend request to create a customer
        response = client
                .mutateWith(mockAuthentication(user(TEST_USER, Role.parse("customers/customers:CreateCustomer"))))
                .put()
                .uri(uri -> uri.path("/command/createCustomer")
                        .queryParam("reply", true)
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(jsonString(TEST_CUSTOMER))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(TEST_USER)
                .jsonPath("$.stateVersion").isEqualTo(incrStateVersion(stateVersion))
                .jsonPath("$.customerId").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(5).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.New.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval").doesNotExist()
                .jsonPath("$.carriedState.products").doesNotExist()
                .returnResult()
        ;
        stateVersion = response.getResponseHeaders().getETag();

        // Approve request to create a customer

        response = client
                .mutateWith(mockAuthentication(user(APPROVAL_USER, Role.parse("customers/customers:ApproveCustomerModification"))))
                .put()
                .uri(uri -> uri.path("/command/approveCreateCustomer")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(jsonString(TEST_CUSTOMER))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.stateVersion").isEqualTo(incrStateVersion(stateVersion))
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(10).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.Live.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.approval.at").value(isWithin(5).secondsOf(now()))
                .jsonPath("$.carriedState.products").doesNotExist()
                .returnResult()
        ;
        stateVersion = response.getResponseHeaders().getETag();

        // Request to add product 1

        var account1 = UKBankAccount.builder()
                .name(BANK_ACCOUNT_1)
                .accountNumber(UKAccountNumber.fromString(ACCOUNT_NUMBER_1))
                .currencyUnit(Monetary.getCurrency(GBP))
                .sortCode(new SortCode(SORTCODE_1))
                .build();

        Map<String, PaymentMethodConfiguration> methodProperties = new HashMap<>();
        Map<String, String> bacsProperties = new HashMap<>();
        bacsProperties.put(PaymentMethodConfiguration.BACS_SERVICE_USER_NUMBER, "SUNVAL");
        bacsProperties.put(PaymentMethodConfiguration.BACS_SERVICE_ACCOUNT_NAME, "SAVAL");
        methodProperties.put(PaymentMethod.Bacs.name(), new PaymentMethodConfiguration(bacsProperties));


        var product1 = ProductRequest.builder()
                .name(PRODUCT_1)
                .customerId(TEST_CUSTOMER)
                .adapter(ProductAdapter.builder()
                        .location(PRODUCT_1_LOCATION)
                        .type(ProductAdapterType.internal)
                        .build())
                .outboundConfiguration(OutboundConfiguration.builder()
                        .source(account1)
                        .returns(account1)
                        .payer(PartyIdentification.builder().name("cust").build())
                        .defaultPaymentMethods(List.of(PaymentMethod.Bacs))
                        .paymentMethodConfiguration(methodProperties)
                        .validationRules(
                                HashSet.of(
                                        Validation.builder()
                                                .rule(RULE_A)
                                                .errorMessage(MESSAGE_TEMPLATE_1)
                                                .action(ValidationAction.RequiresApprovalAction)
                                                .build()
                                ))
                        .build())
                .build();

        response = client
                .mutateWith(mockAuthentication(user(TEST_USER, Role.parse("customers/customers:ModifyCustomer"))))
                .put()
                .uri(uri -> uri.path("/command/createProduct")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(product1)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(TEST_USER)
                .jsonPath("$.stateVersion").isEqualTo(incrStateVersion(stateVersion))
                .jsonPath("$.productId").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.ModifyRequested.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.approval.at").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products").isArray()
                .jsonPath("$.carriedState.products[0].name").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.products[0].createdAt").value(isWithin(5).secondsOf(now()))
                .jsonPath("$.carriedState.products[0].status").isEqualTo(ProductStatus.New.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.location").isEqualTo(PRODUCT_1_LOCATION)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .returnResult()
        ;
        stateVersion = response.getResponseHeaders().getETag();

        // Request to add product 2 (should fail)

        var account2 = UKBankAccount.builder()
                .name(BANK_ACCOUNT_2)
                .accountNumber(UKAccountNumber.fromString(ACCOUNT_NUMBER_2))
                .currencyUnit(Monetary.getCurrency(GBP))
                .sortCode(new SortCode(SORTCODE_2))
                .build();

        var product2 = ProductRequest.builder()
                .name(PRODUCT_2)
                .customerId(TEST_CUSTOMER)
                .adapter(ProductAdapter.builder()
                        .location(PRODUCT_2_LOCATION)
                        .type(ProductAdapterType.internal)
                        .build())
                .outboundConfiguration(OutboundConfiguration.builder()
                        .source(account2)
                        .returns(account2)
                        .payer(PartyIdentification.builder().name("cust").build())
                        .defaultPaymentMethods(List.of(PaymentMethod.Bacs))
                        .validationRules(
                                HashSet.of(
                                        Validation.builder()
                                                .rule(RULE_A)
                                                .errorMessage(MESSAGE_TEMPLATE_1)
                                                .action(ValidationAction.RequiresApprovalAction)
                                                .build()
                                ))
                        .build())
                .build();

        client
                .mutateWith(mockAuthentication(user(TEST_USER, Role.parse("customers/customers:ModifyCustomer"))))
                .put()
                .uri(uri -> uri.path("/command/createProduct")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(product2)
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectBody()
                .jsonPath("$.key").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.error.errorType").isEqualTo("IllegalStateException")
                .jsonPath("$.error.errorMessage").isEqualTo("Command \"CreateProductCommand\" cannot be carried out as customer testCustomer has status ModifyRequested instead of Live")
                .returnResult()
        ;

        // Approve request to add product 1

        response = client
                .mutateWith(mockAuthentication(user(APPROVAL_USER, Role.parse("customers/customers:ApproveProductModification"))))
                .put()
                .uri(uri -> uri.path("/command/approveCreateProduct")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(new CompositeReference(TEST_CUSTOMER, PRODUCT_1))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.stateVersion").isEqualTo(incrStateVersion(stateVersion))
                .jsonPath("$.productId").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.ModifyRequested.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.approval.at").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products").isArray()

                .jsonPath("$.carriedState.products[0].name").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.products[0].createdAt").value(isWithin(10).secondsOf(now()))
                .jsonPath("$.carriedState.products[0].status").isEqualTo(ProductStatus.Creating.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.location").isEqualTo(PRODUCT_1_LOCATION)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[0].approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[0].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[0].approval.message").doesNotExist()
                .jsonPath("$.carriedState.products[0].approval.at").value(isWithin(5).secondsOf(now()))
                .returnResult()
        ;
        stateVersion = response.getResponseHeaders().getETag();

        // Finalize creating product 1

        var finalizeCreateProductMsg = KeyValueWithHeaders.msg(TEST_CUSTOMER,
                FinalizeCreateProductCommand.builder()
                        .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_1))
                        .reply(true)
                        .build()
        );

        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(finalizeCreateProductMsg));

        var events = brokerClient.readAllFromDestination(EVENTS_TOPIC, Event.class);
        assertEquals(ProductCreatedEvent.class, events.last().getClass(), "Expect latest event to be a ProductCreatedEvent");
        var productCreatedEvent = (ProductCreatedEvent) events.last();

        var expectedCustomer = Customer.builder()
                .name(TEST_CUSTOMER)
                .createdAt(now())
                .createdBy(TEST_USER)
                .lastModifiedBy(TEST_USER)
                .status(CustomerStatus.Live)
                .products(HashSet.empty()) // Check product separately
                .build();

        assertThat(productCreatedEvent).isEqualToIgnoringGivenFields(
                ProductCreatedEvent.builder()
                        .productId(PRODUCT_1)
                        .carriedState(expectedCustomer)
                        .user(TEST_USER)
                        .stateVersion(Long.valueOf(incrStateVersion(stateVersion)))
                        .executionCount(1)
                        .reply(true)
                        .build(),
                "messageId", "timestamp", "correlationId");
        var productFromEvent = productCreatedEvent.getCarriedState().getProduct(PRODUCT_1).get();
        assertEquals(PRODUCT_1, productFromEvent.getName());
        assertEquals(ProductStatus.Live, productFromEvent.getStatus());

        stateVersion = productCreatedEvent.getStateVersion().toString();

        // Request to add product 2

        response = client
                .mutateWith(mockAuthentication(user(TEST_USER, Role.parse("customers/customers:ModifyCustomer"))))
                .put()
                .uri(uri -> uri.path("/command/createProduct")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(product2)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(TEST_USER)
                .jsonPath("$.stateVersion").isEqualTo(incrStateVersion(stateVersion))
                .jsonPath("$.productId").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.ModifyRequested.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.approval.at").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products").isArray()

                .jsonPath("$.carriedState.products[0].name").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.products[0].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[0].status").isEqualTo(ProductStatus.Live.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.location").isEqualTo(PRODUCT_1_LOCATION)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[0].approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[0].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[0].approval.message").doesNotExist()
                .jsonPath("$.carriedState.products[0].approval.at").value(isWithin(60).secondsOf(now()))

                .jsonPath("$.carriedState.products[1].name").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.products[1].createdAt").value(isWithin(5).secondsOf(now()))
                .jsonPath("$.carriedState.products[1].status").isEqualTo(ProductStatus.New.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.location").isEqualTo(PRODUCT_2_LOCATION)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[1].approval").doesNotExist()

                .returnResult()
        ;
        stateVersion = response.getResponseHeaders().getETag();

        // Reject request to add product 2

        var productRejectRequest = ProductRejectRequest.builder()
                .productReference(new CompositeReference(TEST_CUSTOMER, PRODUCT_2))
                .reason(PRODUCT_REJECT_REASON)
                .build();

        response = client
                .mutateWith(mockAuthentication(user(APPROVAL_USER, Role.parse("customers/customers:ApproveProductModification"))))
                .put()
                .uri(uri -> uri.path("/command/rejectCreateProduct")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(productRejectRequest)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.stateVersion").isEqualTo(incrStateVersion(stateVersion))
                .jsonPath("$.productId").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.Live.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.approval.at").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products").isArray()

                .jsonPath("$.carriedState.products[0].name").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.products[0].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[0].status").isEqualTo(ProductStatus.Live.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.location").isEqualTo(PRODUCT_1_LOCATION)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[0].approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[0].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[0].approval.message").doesNotExist()
                .jsonPath("$.carriedState.products[0].approval.at").value(isWithin(60).secondsOf(now()))

                .jsonPath("$.carriedState.products[1].name").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.products[1].createdAt").value(isWithin(20).secondsOf(now()))
                .jsonPath("$.carriedState.products[1].status").isEqualTo(ProductStatus.Rejected.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.location").isEqualTo(PRODUCT_2_LOCATION)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[1].approval.approved").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[1].approval.message").isEqualTo(PRODUCT_REJECT_REASON)
                .jsonPath("$.carriedState.products[1].approval.at").value(isWithin(5).secondsOf(now()))

                .returnResult()
        ;
        stateVersion = response.getResponseHeaders().getETag();

        // Request to add product 2

        response = client
                .mutateWith(mockAuthentication(user(TEST_USER, Role.parse("customers/customers:ModifyCustomer"))))
                .put()
                .uri(uri -> uri.path("/command/createProduct")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(product2)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(TEST_USER)
                .jsonPath("$.stateVersion").isEqualTo(incrStateVersion(stateVersion))
                .jsonPath("$.productId").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.ModifyRequested.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.approval.at").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products").isArray()

                .jsonPath("$.carriedState.products[0].name").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.products[0].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[0].status").isEqualTo(ProductStatus.Live.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.location").isEqualTo(PRODUCT_1_LOCATION)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[0].approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[0].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[0].approval.message").doesNotExist()
                .jsonPath("$.carriedState.products[0].approval.at").value(isWithin(60).secondsOf(now()))

                .jsonPath("$.carriedState.products[1].name").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.products[1].createdAt").value(isWithin(5).secondsOf(now()))
                .jsonPath("$.carriedState.products[1].status").isEqualTo(ProductStatus.New.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.location").isEqualTo(PRODUCT_2_LOCATION)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[1].approval").doesNotExist()

                .returnResult()
        ;
        stateVersion = response.getResponseHeaders().getETag();

        // Approve request to add product 2

        response = client
                .mutateWith(mockAuthentication(user(APPROVAL_USER, Role.parse("customers/customers:ApproveProductModification"))))
                .put()
                .uri(uri -> uri.path("/command/approveCreateProduct")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(new CompositeReference(TEST_CUSTOMER, PRODUCT_2))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.stateVersion").isEqualTo(incrStateVersion(stateVersion))
                .jsonPath("$.productId").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.ModifyRequested.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.approval.at").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products").isArray()

                .jsonPath("$.carriedState.products[0].name").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.products[0].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[0].status").isEqualTo(ProductStatus.Live.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.location").isEqualTo(PRODUCT_1_LOCATION)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[0].approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[0].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[0].approval.message").doesNotExist()
                .jsonPath("$.carriedState.products[0].approval.at").value(isWithin(60).secondsOf(now()))

                .jsonPath("$.carriedState.products[1].name").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.products[1].createdAt").value(isWithin(20).secondsOf(now()))
                .jsonPath("$.carriedState.products[1].status").isEqualTo(ProductStatus.Creating.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.location").isEqualTo(PRODUCT_2_LOCATION)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[1].approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[1].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[1].approval.message").doesNotExist()
                .jsonPath("$.carriedState.products[1].approval.at").value(isWithin(5).secondsOf(now()))

                .returnResult()
        ;
        stateVersion = response.getResponseHeaders().getETag();

        // Finalize creating product 2

        finalizeCreateProductMsg = KeyValueWithHeaders.msg(TEST_CUSTOMER,
                FinalizeCreateProductCommand.builder()
                        .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_2))
                        .reply(true)
                        .build()
        );

        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(finalizeCreateProductMsg));

        events = brokerClient.readAllFromDestination(EVENTS_TOPIC, Event.class);
        assertEquals(ProductCreatedEvent.class, events.last().getClass(), "Expect latest event to be a ProductCreatedEvent");
        productCreatedEvent = (ProductCreatedEvent) events.last();

        expectedCustomer = Customer.builder()
                .name(TEST_CUSTOMER)
                .createdAt(now())
                .createdBy(TEST_USER)
                .lastModifiedBy(TEST_USER)
                .status(CustomerStatus.Live)
                .products(HashSet.empty()) // Check product separately
                .build();

        assertThat(productCreatedEvent).isEqualToIgnoringGivenFields(
                ProductCreatedEvent.builder()
                        .productId(PRODUCT_2)
                        .carriedState(expectedCustomer)
                        .user(TEST_USER)
                        .stateVersion(Long.valueOf(incrStateVersion(stateVersion)))
                        .executionCount(1)
                        .reply(true)
                        .build(),
                "messageId", "timestamp", "correlationId");
        productFromEvent = productCreatedEvent.getCarriedState().getProduct(PRODUCT_2).get();
        assertEquals(PRODUCT_2, productFromEvent.getName());
        assertEquals(ProductStatus.Live, productFromEvent.getStatus());

        stateVersion = productCreatedEvent.getStateVersion().toString();

        // Request to modify product 1

        var modifiedAccount1 = UKBankAccount.builder()
                .name(BANK_ACCOUNT_1B)
                .accountNumber(UKAccountNumber.fromString(ACCOUNT_NUMBER_1B))
                .currencyUnit(Monetary.getCurrency(GBP))
                .sortCode(new SortCode(SORTCODE_1B))
                .build();

        var modifiedProduct1 = ProductRequest.builder()
                .name(PRODUCT_1)
                .customerId(TEST_CUSTOMER)
                .adapter(ProductAdapter.builder()
                        .location(PRODUCT_1_LOCATION)
                        .type(ProductAdapterType.internal)
                        .build())
                .outboundConfiguration(OutboundConfiguration.builder()
                        .source(modifiedAccount1)
                        .returns(modifiedAccount1)
                        .payer(PartyIdentification.builder().name("cust").build())
                        .defaultPaymentMethods(List.of(PaymentMethod.Bacs))
                        .validationRules(
                                HashSet.of(
                                        Validation.builder()
                                                .rule(RULE_A)
                                                .errorMessage(MESSAGE_TEMPLATE_1)
                                                .action(ValidationAction.RequiresApprovalAction)
                                                .build()
                                ))
                        .build())
                .build();

        response = client
                .mutateWith(mockAuthentication(user(TEST_USER, Role.parse("customers/customers:ModifyProduct"))))
                .put()
                .uri(uri -> uri.path("/command/modifyProduct")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(modifiedProduct1)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(TEST_USER)
                .jsonPath("$.stateVersion").isEqualTo(incrStateVersion(stateVersion))
                .jsonPath("$.productId").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.ModifyRequested.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.approval.at").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products").isArray()

                .jsonPath("$.carriedState.products[0].name").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.products[0].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[0].status").isEqualTo(ProductStatus.ModifyRequested.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.location").isEqualTo(PRODUCT_1_LOCATION)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())

                .jsonPath("$.carriedState.products[0].pendingProductConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[0].pendingProductConfiguration.adapter.location").isEqualTo(PRODUCT_1_LOCATION)
                .jsonPath("$.carriedState.products[0].pendingProductConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].pendingProductConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].pendingProductConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_1B)
                .jsonPath("$.carriedState.products[0].pendingProductConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].pendingProductConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_1B)
                .jsonPath("$.carriedState.products[0].pendingProductConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_1B)
                .jsonPath("$.carriedState.products[0].pendingProductConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_1B)
                .jsonPath("$.carriedState.products[0].pendingProductConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].pendingProductConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_1B)
                .jsonPath("$.carriedState.products[0].pendingProductConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_1B)
                .jsonPath("$.carriedState.products[0].pendingProductConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[0].pendingProductConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[0].pendingProductConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())

                .jsonPath("$.carriedState.products[0].approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[0].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[0].approval.message").doesNotExist()
                .jsonPath("$.carriedState.products[0].approval.at").value(isWithin(60).secondsOf(now()))

                .jsonPath("$.carriedState.products[1].name").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.products[1].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[1].status").isEqualTo(ProductStatus.Live.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.location").isEqualTo(PRODUCT_2_LOCATION)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[1].approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[1].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[1].approval.message").doesNotExist()
                .jsonPath("$.carriedState.products[1].approval.at").value(isWithin(60).secondsOf(now()))

                .returnResult()
        ;
        stateVersion = response.getResponseHeaders().getETag();

        // Approve request to modify product 1

        response = client
                .mutateWith(mockAuthentication(user(APPROVAL_USER, Role.parse("customers/customers:ApproveProductModification"))))
                .put()
                .uri(uri -> uri.path("/command/approveModifyProduct")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(new CompositeReference(TEST_CUSTOMER, PRODUCT_1))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.stateVersion").isEqualTo(incrStateVersion(stateVersion))
                .jsonPath("$.productId").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.Live.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.approval.at").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products").isArray()

                .jsonPath("$.carriedState.products[0].name").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.products[0].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[0].status").isEqualTo(ProductStatus.Live.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.location").isEqualTo(PRODUCT_1_LOCATION)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[0].approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[0].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[0].approval.message").doesNotExist()
                .jsonPath("$.carriedState.products[0].approval.at").value(isWithin(60).secondsOf(now()))

                .jsonPath("$.carriedState.products[1].name").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.products[1].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[1].status").isEqualTo(ProductStatus.Live.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.location").isEqualTo(PRODUCT_2_LOCATION)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[1].approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[1].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[1].approval.message").doesNotExist()
                .jsonPath("$.carriedState.products[1].approval.at").value(isWithin(60).secondsOf(now()))

                .returnResult()
        ;
        stateVersion = response.getResponseHeaders().getETag();

        // Request to modify product 2

        var modifiedAccount2 = UKBankAccount.builder()
                .name(BANK_ACCOUNT_2B)
                .accountNumber(UKAccountNumber.fromString(ACCOUNT_NUMBER_2B))
                .currencyUnit(Monetary.getCurrency(GBP))
                .sortCode(new SortCode(SORTCODE_2B))
                .build();

        var modifiedProduct2 = ProductRequest.builder()
                .name(PRODUCT_2)
                .customerId(TEST_CUSTOMER)
                .adapter(ProductAdapter.builder()
                        .location(PRODUCT_2_LOCATION)
                        .type(ProductAdapterType.internal)
                        .build())
                .outboundConfiguration(OutboundConfiguration.builder()
                        .source(modifiedAccount2)
                        .returns(modifiedAccount2)
                        .payer(PartyIdentification.builder().name("cust").build())
                        .defaultPaymentMethods(List.of(PaymentMethod.Bacs))
                        .validationRules(
                                HashSet.of(
                                        Validation.builder()
                                                  .rule(RULE_A)
                                                  .errorMessage(MESSAGE_TEMPLATE_1)
                                                  .action(ValidationAction.RequiresApprovalAction)
                                                  .build()
                                ))
                        .build())
                .build();

        response = client
                .mutateWith(mockAuthentication(user(TEST_USER, Role.parse("customers/customers:ModifyProduct"))))
                .put()
                .uri(uri -> uri.path("/command/modifyProduct")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(modifiedProduct2)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(TEST_USER)
                .jsonPath("$.stateVersion").isEqualTo(incrStateVersion(stateVersion))
                .jsonPath("$.productId").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.ModifyRequested.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.approval.at").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products").isArray()

                .jsonPath("$.carriedState.products[0].name").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.products[0].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[0].status").isEqualTo(ProductStatus.Live.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.location").isEqualTo(PRODUCT_1_LOCATION)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[0].approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[0].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[0].approval.message").doesNotExist()
                .jsonPath("$.carriedState.products[0].approval.at").value(isWithin(60).secondsOf(now()))

                .jsonPath("$.carriedState.products[1].name").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.products[1].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[1].status").isEqualTo(ProductStatus.ModifyRequested.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.location").isEqualTo(PRODUCT_2_LOCATION)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())

                .jsonPath("$.carriedState.products[1].pendingProductConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[1].pendingProductConfiguration.adapter.location").isEqualTo(PRODUCT_2_LOCATION)
                .jsonPath("$.carriedState.products[1].pendingProductConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].pendingProductConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].pendingProductConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_2B)
                .jsonPath("$.carriedState.products[1].pendingProductConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].pendingProductConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_2B)
                .jsonPath("$.carriedState.products[1].pendingProductConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_2B)
                .jsonPath("$.carriedState.products[1].pendingProductConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_2B)
                .jsonPath("$.carriedState.products[1].pendingProductConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].pendingProductConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_2B)
                .jsonPath("$.carriedState.products[1].pendingProductConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_2B)
                .jsonPath("$.carriedState.products[1].pendingProductConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[1].pendingProductConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[1].pendingProductConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())

                .jsonPath("$.carriedState.products[1].approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[1].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[1].approval.message").doesNotExist()
                .jsonPath("$.carriedState.products[1].approval.at").value(isWithin(60).secondsOf(now()))

                .returnResult()
        ;
        stateVersion = response.getResponseHeaders().getETag();

        // Reject request to modify product 2

        productRejectRequest = ProductRejectRequest.builder()
                .productReference(new CompositeReference(TEST_CUSTOMER, PRODUCT_2))
                .reason(PRODUCT_REJECT_REASON)
                .build();

        response = client
                .mutateWith(mockAuthentication(user(APPROVAL_USER, Role.parse("customers/customers:ApproveProductModification"))))
                .put()
                .uri(uri -> uri.path("/command/rejectModifyProduct")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(productRejectRequest)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.stateVersion").isEqualTo(incrStateVersion(stateVersion))
                .jsonPath("$.productId").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.Live.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.approval.at").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products").isArray()

                .jsonPath("$.carriedState.products[0].name").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.products[0].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[0].status").isEqualTo(ProductStatus.Live.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.location").isEqualTo(PRODUCT_1_LOCATION)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[0].approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[0].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[0].approval.message").doesNotExist()
                .jsonPath("$.carriedState.products[0].approval.at").value(isWithin(60).secondsOf(now()))

                .jsonPath("$.carriedState.products[1].name").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.products[1].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[1].status").isEqualTo(ProductStatus.Live.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.location").isEqualTo(PRODUCT_2_LOCATION)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[1].approval.approved").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[1].approval.message").isEqualTo(PRODUCT_REJECT_REASON)
                .jsonPath("$.carriedState.products[1].approval.at").value(isWithin(5).secondsOf(now()))

                .returnResult()
        ;
        stateVersion = response.getResponseHeaders().getETag();

        // Request to delete product 2

        response = client
                .mutateWith(mockAuthentication(user(TEST_USER, Role.parse("customers/customers:ModifyProduct"))))
                .put()
                .uri(uri -> uri.path("/command/deleteProduct")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(new CompositeReference(TEST_CUSTOMER, PRODUCT_2))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(TEST_USER)
                .jsonPath("$.stateVersion").isEqualTo(incrStateVersion(stateVersion))
                .jsonPath("$.productId").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.ModifyRequested.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.approval.at").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products").isArray()

                .jsonPath("$.carriedState.products[0].name").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.products[0].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[0].status").isEqualTo(ProductStatus.Live.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.location").isEqualTo(PRODUCT_1_LOCATION)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[0].approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[0].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[0].approval.message").doesNotExist()
                .jsonPath("$.carriedState.products[0].approval.at").value(isWithin(60).secondsOf(now()))

                .jsonPath("$.carriedState.products[1].name").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.products[1].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[1].status").isEqualTo(ProductStatus.DeleteRequested.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.location").isEqualTo(PRODUCT_2_LOCATION)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[1].approval.approved").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[1].approval.message").isEqualTo(PRODUCT_REJECT_REASON)
                .jsonPath("$.carriedState.products[1].approval.at").value(isWithin(60).secondsOf(now()))

                .returnResult()
        ;
        stateVersion = response.getResponseHeaders().getETag();

        // Approve request to delete product 2

        response = client
                .mutateWith(mockAuthentication(user(APPROVAL_USER, Role.parse("customers/customers:ApproveProductModification"))))
                .put()
                .uri(uri -> uri.path("/command/approveDeleteProduct")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(new CompositeReference(TEST_CUSTOMER, PRODUCT_2))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.stateVersion").isEqualTo(incrStateVersion(stateVersion))
                .jsonPath("$.productId").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.ModifyRequested.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.approval.at").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products").isArray()

                .jsonPath("$.carriedState.products[0].name").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.products[0].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[0].status").isEqualTo(ProductStatus.Live.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.location").isEqualTo(PRODUCT_1_LOCATION)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[0].approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[0].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[0].approval.message").doesNotExist()
                .jsonPath("$.carriedState.products[0].approval.at").value(isWithin(60).secondsOf(now()))

                .jsonPath("$.carriedState.products[1].name").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.products[1].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[1].status").isEqualTo(ProductStatus.Deleting.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.location").isEqualTo(PRODUCT_2_LOCATION)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[1].approval.approved").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[1].approval.message").isEqualTo(PRODUCT_REJECT_REASON)
                .jsonPath("$.carriedState.products[1].approval.at").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[1].deleteApproval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[1].deleteApproval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[1].deleteApproval.message").doesNotExist()
                .jsonPath("$.carriedState.products[1].deleteApproval.at").value(isWithin(5).secondsOf(now()))

                .returnResult()
        ;
        stateVersion = response.getResponseHeaders().getETag();

        // Finalize deleting product 2

        var finalizeDeleteProductMsg = KeyValueWithHeaders.msg(TEST_CUSTOMER,
                FinalizeDeleteProductCommand.builder()
                        .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_2))
                        .reply(true)
                        .build()
        );

        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(finalizeDeleteProductMsg));

        events = brokerClient.readAllFromDestination(EVENTS_TOPIC, Event.class);
        assertEquals(ProductDeletedEvent.class, events.last().getClass(), "Expect latest event to be a ProductDeletedEvent");
        var productDeletedEvent = (ProductDeletedEvent) events.last();

        expectedCustomer = Customer.builder()
                .name(TEST_CUSTOMER)
                .createdAt(now())
                .createdBy(TEST_USER)
                .lastModifiedBy(TEST_USER)
                .status(CustomerStatus.Live)
                .products(HashSet.empty()) // Check product separately
                .build();

        assertThat(productDeletedEvent).isEqualToIgnoringGivenFields(
                ProductDeletedEvent.builder()
                        .productId(PRODUCT_2)
                        .carriedState(expectedCustomer)
                        .user(TEST_USER)
                        .stateVersion(Long.valueOf(incrStateVersion(stateVersion)))
                        .executionCount(1)
                        .reply(true)
                        .build(),
                "messageId", "timestamp", "correlationId");
        productFromEvent = productDeletedEvent.getCarriedState().getProduct(PRODUCT_2).get();
        assertEquals(PRODUCT_2, productFromEvent.getName());
        assertEquals(ProductStatus.Deleted, productFromEvent.getStatus());

        stateVersion = productDeletedEvent.getStateVersion().toString();

        // Request to delete customer

        response = client
                .mutateWith(mockAuthentication(user(APPROVAL_USER, Role.parse("customers/customers:ModifyCustomer"))))
                .put()
                .uri(uri -> uri.path("/command/deleteCustomer")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(jsonString(TEST_CUSTOMER))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.stateVersion").isEqualTo(incrStateVersion(stateVersion))
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.DeleteRequested.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.approval.at").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.deleteApproval.approved").doesNotExist()
                .jsonPath("$.carriedState.deleteApproval.at").doesNotExist()
                .jsonPath("$.carriedState.products").isArray()

                .jsonPath("$.carriedState.products[0].name").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.products[0].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[0].status").isEqualTo(ProductStatus.Live.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.location").isEqualTo(PRODUCT_1_LOCATION)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[0].approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[0].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[0].approval.message").doesNotExist()
                .jsonPath("$.carriedState.products[0].approval.at").value(isWithin(60).secondsOf(now()))

                .jsonPath("$.carriedState.products[1].name").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.products[1].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[1].status").isEqualTo(ProductStatus.Deleted.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.location").isEqualTo(PRODUCT_2_LOCATION)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[1].approval.approved").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[1].approval.message").isEqualTo(PRODUCT_REJECT_REASON)
                .jsonPath("$.carriedState.products[1].approval.at").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[1].deleteApproval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[1].deleteApproval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[1].deleteApproval.message").doesNotExist()
                .jsonPath("$.carriedState.products[1].deleteApproval.at").value(isWithin(5).secondsOf(now()))
                .returnResult()
        ;
        stateVersion = response.getResponseHeaders().getETag();

        // Approve request to delete customer

        response = client
                .mutateWith(mockAuthentication(user(APPROVAL_USER, Role.parse("customers/customers:ApproveCustomerModification"))))
                .put()
                .uri(uri -> uri.path("/command/approveDeleteCustomer")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, stateVersion)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(jsonString(TEST_CUSTOMER))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.stateVersion").isEqualTo(incrStateVersion(stateVersion))
                .jsonPath("$.carriedState.name").isEqualTo(TEST_CUSTOMER)
                .jsonPath("$.carriedState.createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.status").isEqualTo(CustomerStatus.Deleting.toString())
                .jsonPath("$.carriedState.createdBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.lastModifiedBy").isEqualTo(TEST_USER)
                .jsonPath("$.carriedState.approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.approval.at").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.deleteApproval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.deleteApproval.at").value(isWithin(5).secondsOf(now()))
                .jsonPath("$.carriedState.products").isArray()

                .jsonPath("$.carriedState.products[0].name").isEqualTo(PRODUCT_1)
                .jsonPath("$.carriedState.products[0].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[0].status").isEqualTo(ProductStatus.Live.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[0].productConfiguration.adapter.location").isEqualTo(PRODUCT_1_LOCATION)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_1B)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[0].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[0].approval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[0].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[0].approval.message").doesNotExist()
                .jsonPath("$.carriedState.products[0].approval.at").value(isWithin(60).secondsOf(now()))

                .jsonPath("$.carriedState.products[1].name").isEqualTo(PRODUCT_2)
                .jsonPath("$.carriedState.products[1].createdAt").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[1].status").isEqualTo(ProductStatus.Deleted.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.type").isEqualTo(ProductAdapterType.internal.toString())
                .jsonPath("$.carriedState.products[1].productConfiguration.adapter.location").isEqualTo(PRODUCT_2_LOCATION)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.fileImports").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.apiAccess").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.source.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.name").isEqualTo(BANK_ACCOUNT_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.currency").isEqualTo(GBP)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.sortCode").isEqualTo(SORTCODE_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.returns.accountNumber").isEqualTo(ACCOUNT_NUMBER_2)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].rule").isEqualTo(RULE_A)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].errorMessage").isEqualTo(MESSAGE_TEMPLATE_1)
                .jsonPath("$.carriedState.products[1].productConfiguration.outboundConfiguration.validationRules[0].action").isEqualTo(ValidationAction.RequiresApprovalAction.toString())
                .jsonPath("$.carriedState.products[1].approval.approved").isEqualTo(false)
                .jsonPath("$.carriedState.products[1].approval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[1].approval.message").isEqualTo(PRODUCT_REJECT_REASON)
                .jsonPath("$.carriedState.products[1].approval.at").value(isWithin(60).secondsOf(now()))
                .jsonPath("$.carriedState.products[1].deleteApproval.approved").isEqualTo(true)
                .jsonPath("$.carriedState.products[1].deleteApproval.user").isEqualTo(APPROVAL_USER)
                .jsonPath("$.carriedState.products[1].deleteApproval.message").doesNotExist()
                .jsonPath("$.carriedState.products[1].deleteApproval.at").value(isWithin(5).secondsOf(now()))

                .returnResult()
        ;
        stateVersion = response.getResponseHeaders().getETag();

        // Delete product 1 (as if from process manager, using ApproveDeleteProductCommand)

        var approveDeleteProductMsg = KeyValueWithHeaders.msg(TEST_CUSTOMER,
                ApproveDeleteProductCommand.builder()
                        .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_1))
                        .forceDelete(true)
                        .user(APPROVAL_USER)
                        .roles(HashSet.of(Role.parse("customers/customers:ApproveProductModification")))
                        .stateVersion(Long.valueOf(stateVersion))
                        .reply(true)
                        .build()
        );

        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(approveDeleteProductMsg));

        events = brokerClient.readAllFromDestination(EVENTS_TOPIC, Event.class);
        assertEquals(DeleteProductApprovedEvent.class, events.last().getClass(), "Expect latest event to be a DeleteProductApprovedEvent");
        var deleteProductApprovedEvent = (DeleteProductApprovedEvent) events.last();

        expectedCustomer = Customer.builder()
                .name(TEST_CUSTOMER)
                .createdAt(now())
                .createdBy(TEST_USER)
                .lastModifiedBy(TEST_USER)
                .status(CustomerStatus.Deleting)
                .products(HashSet.empty()) // Check product separately
                .build();

        assertThat(deleteProductApprovedEvent).isEqualToIgnoringGivenFields(
                deleteProductApprovedEvent.builder()
                        .productId(PRODUCT_1)
                        .carriedState(expectedCustomer)
                        .user(APPROVAL_USER)
                        .stateVersion(Long.valueOf(incrStateVersion(stateVersion)))
                        .executionCount(1)
                        .reply(true)
                        .build(),
                "messageId", "timestamp", "correlationId");
        productFromEvent = deleteProductApprovedEvent.getCarriedState().getProduct(PRODUCT_1).get();
        assertEquals(PRODUCT_1, productFromEvent.getName());
        assertEquals(ProductStatus.Deleting, productFromEvent.getStatus());

        stateVersion = productCreatedEvent.getStateVersion().toString();
    }

    @Test
    @Disabled("there are issues with how this test sets up state, meaning it is dependent on the order of execution")
    public void testCustomerPaymentBatchesAreConvertedToEvents(EmbeddedBrokerClient brokerClient) throws IOException {
        String customerId = "customerName";
        String productId = "productName";
        var compositeReference = new CompositeReference(customerId, productId);
        URI uri = URI.create("s3://bucketUrl/dummy-branch/customer/product/fileName.csv#versionId");

        setUpCustomerAndProduct(brokerClient, compositeReference);

        InputStream is = new ByteArrayInputStream("teststring".getBytes());
        when(s3Client.getObject(any(GetObjectRequest.class), any(ResponseTransformer.class))).thenReturn(is);

        String batchId = "12345";
        String batchId2 = "12333";
        String batchId3 = "12444";

        var batch = List.of(generateRequest(batchId, compositeReference), generateRequest(batchId2, compositeReference));
        var batch2 = List.of(generateRequest(batchId, compositeReference));
        var batch3 = List.of(generateRequest(batchId, compositeReference), generateRequest(batchId2, compositeReference), generateRequest(batchId3, compositeReference));

        var custBatch = new CustomerPaymentBatch(batchId, batch);
        var custBatch2 = new CustomerPaymentBatch(batchId2, batch2);
        var custBatch3 = new CustomerPaymentBatch(batchId3, batch3);

        when(customerWebClientAdapter.sendFileToAdapter(any(), any(), any(), any()))
                .thenReturn(List.of(custBatch, custBatch2, custBatch3));

        var command = generateParseFileCommand(compositeReference, uri);
        var commandWithKey = KeyValueWithHeaders.msg("1", command);
        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(commandWithKey));

        List<EventWithCauseImpl> createdEvents = brokerClient.readAllFromDestination(EVENTS_TOPIC, EventWithCauseImpl.class);

        assertEquals(13, createdEvents.size());
        assertTrue(createdEvents.get(1) instanceof PaymentFileParsingStartEvent);
        assertTrue(createdEvents.get(2) instanceof CustomerPaymentCreatedEvent);
        assertTrue(createdEvents.get(3) instanceof CustomerPaymentCreatedEvent);
        assertTrue(createdEvents.get(4) instanceof PaymentFileParsingEndEvent);

        var paymentFileParsingStartEvent = (PaymentFileParsingStartEvent) createdEvents.get(1);
        assertEquals(2, paymentFileParsingStartEvent.getPaymentIds().size());
        assertEquals(uri, paymentFileParsingStartEvent.getUri());

        ArgumentCaptor<GetObjectRequest> reqArg = ArgumentCaptor.forClass(GetObjectRequest.class);
        ArgumentCaptor<ResponseTransformer> respTransArg = ArgumentCaptor.forClass(ResponseTransformer.class);
        verify(s3Client).getObject(reqArg.capture(), respTransArg.capture());

    }

    @Test
    public void s3ExceptionIsConvertedToParsingFailedEvent(EmbeddedBrokerClient brokerClient) throws IOException {
        String customerId = "customerName";
        String productId = "productName";
        var compositeReference = new CompositeReference(customerId, productId);
        URI uri = URI.create("s3://bucketUrl/dummy-branch/customer/product/fileName.csv#versionId");

        setUpCustomerAndProduct(brokerClient, compositeReference);

        when(s3Client.getObject(any(GetObjectRequest.class), any(ResponseTransformer.class))).thenThrow(SdkClientException.class);

        String batchId = "12345";
        var batch = List.of(generateRequest(batchId, compositeReference), generateRequest(batchId, compositeReference));
        var custBatch = new CustomerPaymentBatch(batchId, batch);

        when(customerWebClientAdapter.sendFileToAdapter(any(), any(), any(), any()))
                .thenReturn(List.of(custBatch));

        var command = generateParseFileCommand(compositeReference, uri);
        var commandWithKey = KeyValueWithHeaders.msg("1", command);
        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(commandWithKey));

        List<EventWithCauseImpl> createdEvents = brokerClient.readAllFromDestination(EVENTS_TOPIC, EventWithCauseImpl.class);

        //5 events from creating the customer and product + 1 failure event
        assertEquals(6, createdEvents.size());
        assertTrue(createdEvents.get(5) instanceof PaymentFileParsingFailedEvent);
        var fileParsingFailedEvent = (PaymentFileParsingFailedEvent) createdEvents.get(5);
        assertEquals(fileParsingFailedEvent.getUri(), uri);
        assertEquals(fileParsingFailedEvent.getProductId(), compositeReference.getComponent1());

    }

    private void setUpCustomerAndProduct(EmbeddedBrokerClient brokerClient, CompositeReference product) throws MalformedURLException {
        // this creates 5 events
        var createCustomerCmd = new CreateCustomerCommand(product.getComponent0(), "Test User", HashSet.of(Role.parse("customers/customers:CreateCustomer")), null, false);
        var custCmdWithKey = KeyValueWithHeaders.msg("1", createCustomerCmd);
        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(custCmdWithKey));

        var approveCustomerCmd = new ApproveCreateCustomerCommand(product.getComponent0(), "Test User Two", HashSet.of(Role.parse("customers/customers:ApproveCustomerModification")), null, false);
        var approveCmdWithKey = KeyValueWithHeaders.msg("1", approveCustomerCmd);
        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(approveCmdWithKey));

        var adapter = ProductAdapter.builder()
                .type(ProductAdapterType.external)
                .location("https://testadapter.com")
                .build();
        var account1 = UKBankAccount.builder()
                                    .name(BANK_ACCOUNT_1)
                                    .accountNumber(UKAccountNumber.fromString(ACCOUNT_NUMBER_1))
                                    .currencyUnit(Monetary.getCurrency(GBP))
                                    .sortCode(new SortCode(SORTCODE_1))
                                    .build();
        var outboundConfig = OutboundConfiguration.builder()
                .source(account1)
                .returns(account1)
                .payer(PartyIdentification.builder().name("cust").build())
                .defaultPaymentMethods(List.of(PaymentMethod.Bacs))
                .validationRules(
                        HashSet.of(
                                Validation.builder()
                                          .rule(RULE_A)
                                          .errorMessage(MESSAGE_TEMPLATE_1)
                                          .action(ValidationAction.RequiresApprovalAction)
                                          .build()
                        ))
                .build();
        var productReq = new ProductRequest(product.getComponent1(), product.getComponent0(), adapter, outboundConfig);
        var createProdCmd = new CreateProductCommand(productReq, "Test User", HashSet.of(Role.parse("customers/customers:ModifyCustomer")), null, false);
        var createProdWithKey = KeyValueWithHeaders.msg("1", createProdCmd);
        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(createProdWithKey));

        var approveProductCmd = new ApproveCreateProductCommand(product, "Test User Two", HashSet.of(Role.parse("customers/customers:ApproveProductModification")), null, false);
        var approveProdWithKey = KeyValueWithHeaders.msg("1", approveProductCmd);
        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(approveProdWithKey));

        var finaliseCreateProduct = new FinalizeCreateProductCommand(product, false);
        var finaliseProdWithKey = KeyValueWithHeaders.msg("1", finaliseCreateProduct);
        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(finaliseProdWithKey));
    }

    private ParseFileCommand generateParseFileCommand(CompositeReference product, URI uri) {
        return ParseFileCommand.builder()
                .file(uri)
                .product(product)
                .createdAt(Instant.now())
                .user("test user name")
                .build();
    }

    private CreatePaymentRequest generateRequest(String batchId, CompositeReference compositeReference) {
        var bankAccount = UKBankAccount.builder()
                                       .sortCode(SortCode.fromString("012345"))
                                       .accountNumber(UKAccountNumber.fromString("12345678"))
                                        .name("act")
                .currency("GBP")
                .build();
        return CreatePaymentRequest.builder()
                .batchId(batchId)
                .allowedMethods(List.empty())
                .creditorAccount(bankAccount)
                .creditor(PartyIdentification.builder().name("creditor").build())
                .amount(Money.zero(Monetary.getCurrency("USD")))
                .latestExecutionDate(now())
                .product(compositeReference)
                .build();
    }

    private String incrStateVersion(String stateVersion) {
        return String.valueOf(Integer.parseInt(stateVersion) + 1);
    }

    private String jsonString(String str) {
        return '"' + str + '"';
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends CustomersCHApplication {

    }
}