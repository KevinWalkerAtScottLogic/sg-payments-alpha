package gov.scot.payments.customers.ch.app.verification;

import gov.scot.payments.customers.model.aggregate.*;
import gov.scot.payments.customers.model.aggregate.Validation;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StatelessPaymentVerifierTest {

    private StatelessPaymentVerifier verifier;
    private ValidationRuleEvaluator evaluator;

    @BeforeEach
    public void setUp(){
        evaluator = mock(ValidationRuleEvaluator.class);
        verifier = new StatelessPaymentVerifier(HashMap.of("spel",evaluator));
    }

    @Test
    public void testSuccess(){
        Validation stateful = Validation.builder()
                                        .type(Validation.Type.Batch)
                                        .rule("stateful")
                                        .errorMessage("stateful")
                                        .build();
        Validation notSpel = Validation.builder()
                .evaluator("notSpel")
                .rule("notSpel")
                .errorMessage("notSpel")
                .build();
        Validation success = Validation.builder()
                .rule("success")
                .errorMessage("success")
                .build();
        var rules = List.of(stateful,notSpel,success);
        when(evaluator.evaluate(eq("success"),any())).thenReturn(ValidationRuleEvaluationResult.success());
        var result = verifier.verifyPayment(makePayment(),rules);
        assertEquals(PaymentVerificationResult.Status.Verified,result.getStatus());
    }

    @Test
    public void testApproval(){
        Validation stateful = Validation.builder()
                .type(Validation.Type.Batch)
                .rule("stateful")
                .errorMessage("stateful")
                .build();
        Validation notSpel = Validation.builder()
                .evaluator("notSpel")
                .rule("notSpel")
                .errorMessage("notSpel")
                .build();
        Validation approval = Validation.builder()
                .rule("approval")
                .errorMessage("approval")
                .action(ValidationAction.RequiresApprovalAction)
                .build();
        var rules = List.of(stateful,notSpel,approval);
        when(evaluator.evaluate(eq("approval"),any())).thenReturn(ValidationRuleEvaluationResult.failure("hello"));
        var result = verifier.verifyPayment(makePayment(),rules);
        assertEquals(PaymentVerificationResult.Status.ApprovalRequired,result.getStatus());
        assertEquals("approval",result.getMessage());
    }

    @Test
    public void testFailure(){
        Validation stateful = Validation.builder()
                .type(Validation.Type.Batch)
                .rule("stateful")
                .errorMessage("stateful")
                .build();
        Validation notSpel = Validation.builder()
                .evaluator("notSpel")
                .rule("notSpel")
                .errorMessage("notSpel")
                .build();
        Validation approval = Validation.builder()
                .rule("approval")
                .errorMessage("approval")
                .action(ValidationAction.RequiresApprovalAction)
                .build();
        Validation reject = Validation.builder()
                .rule("reject")
                .errorMessage("reject")
                .build();
        var rules = List.of(stateful,notSpel,approval,reject);
        when(evaluator.evaluate(eq("approval"),any())).thenReturn(ValidationRuleEvaluationResult.failure("hello"));
        when(evaluator.evaluate(eq("reject"),any())).thenReturn(ValidationRuleEvaluationResult.failure("world"));
        var result = verifier.verifyPayment(makePayment(),rules);
        assertEquals(PaymentVerificationResult.Status.Rejected,result.getStatus());
        assertEquals("approval, reject",result.getMessage());
    }

    //approval + success = approval
    //approval + reject + success = reject

    private Payment makePayment() {
        return Payment.builder()
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.Bacs))
                .creditor(PartyIdentification.builder().name("").build())
                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .debtor(PartyIdentification.builder().name("").build())
                .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .amount(Money.parse("GBP 1.00"))
                .latestExecutionDate(Instant.now())
                .product(new CompositeReference("",""))
                .build();
    }

}
