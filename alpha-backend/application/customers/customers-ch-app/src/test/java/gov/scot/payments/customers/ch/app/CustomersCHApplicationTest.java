package gov.scot.payments.customers.ch.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import gov.scot.payments.application.component.commandhandler.stateful.*;
import gov.scot.payments.application.kafka.MessageHeaderEnricher;
import gov.scot.payments.application.kafka.MessageInfo;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.customers.ch.app.verification.StatelessPaymentVerifier;
import gov.scot.payments.customers.model.aggregate.*;
import gov.scot.payments.customers.model.api.ProductRequest;
import gov.scot.payments.customers.model.command.*;
import gov.scot.payments.customers.model.event.*;
import gov.scot.payments.model.*;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.event.PaymentValidEvent;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.Function3;
import io.vavr.Function5;
import io.vavr.collection.*;
import io.vavr.jackson.datatype.VavrModule;
import lombok.Value;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.Stores;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.support.JacksonUtils;
import org.springframework.kafka.support.converter.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.messaging.MessageHeaders;
import software.amazon.awssdk.services.s3.S3Client;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.util.UUID;

import static java.time.Instant.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@DisplayName("Customer Context Command Handler tests")
class CustomersCHApplicationTest {

    private KafkaStreamsTestHarness harness;
    private CustomerStateUpdateFunc stateUpdateHandler;
    private ParseFileEventGenerator parseFileEventGenerator;
    private CustomerEventGenerator customerEventGenerator;
    private CustomerWebClientAdapter customerWebClientAdapter;
    private S3Client s3Client;


    private static final String TEST_CUSTOMER = "testCustomer";
    private static final String LAST_CHANGE_USER = "lastChangeUser";
    private static final String NEW_CHANGE_USER = "newChangeUser";
    private static final String APPROVAL_USER = "approvalUser";
    private static final String KEY = "999";
    private static final String PRODUCT_NAME_1 = "Product One";
    private static final String PRODUCT_NAME_2 = "Product Two";
    private static final String PRODUCT_NAME_3 = "Product Three";
    private static final long CURRENT_STATE_VERSION = 23L;
    private static final String REASON = "Rejected!";
    private static final String ACCOUNT_NUMBER = "12345678";
    private static final String ACCOUNT_NAME_1 = "Account1";
    private static final String ACCOUNT_NAME_2 = "Account2";
    private static final String ACCOUNT_NAME_3 = "Account3";
    private static final SortCode SORT_CODE = SortCode.fromString("112233");
    private static final CurrencyUnit CURRENCY = Monetary.getCurrency("GBP");
    private static final String ACCESS_DENIED_EXCEPTION = "AccessDeniedException";
    private static final String ACCESS_DENIED_MSG = "Access Denied";

    private static final GenericErrorEvent GENERIC_ERROR_EVENT = new GenericErrorEvent(
            UUID.randomUUID(), // ignored
            Instant.now(),     // ignored
            UUID.randomUUID(), // ignored
            1,
            true,
            TEST_CUSTOMER,
            new CommandFailureInfo() // ignored
    );

    private ProductAdapter productAdapter;
    private StatelessPaymentVerifier verifier;

    @BeforeEach
    void setUp() {
        customerWebClientAdapter = mock(CustomerWebClientAdapter.class);
        s3Client = mock(S3Client.class);
        parseFileEventGenerator = new ParseFileEventGenerator(customerWebClientAdapter, s3Client);
        verifier = new StatelessPaymentVerifier(HashMap.empty());
        stateUpdateHandler = spy(new CustomerStateUpdateFunc());
        customerEventGenerator = spy(new CustomerEventGenerator(parseFileEventGenerator,verifier));
        Function5<SerializedMessage, CommandFailureInfo, Customer, Customer, Long, CustomerState> aggregateStateSupplier = CustomerState::new;
        Function3<Command, Customer, Throwable, List<EventWithCauseImpl>> errorHandler = spy(new TestErrorHandler<>());

        String productUrl= "https:\\\\somewhere.org/product/endpoint";


        productAdapter = ProductAdapter.builder()
                .type(ProductAdapterType.internal)
                .location(productUrl)
                .build();

        List<Class<? extends Command>> commandClasses = List.of(
                CreateCustomerCommand.class,
                ApproveCreateCustomerCommand.class,
                RejectCreateCustomerCommand.class,
                DeleteCustomerCommand.class,
                ApproveDeleteCustomerCommand.class,
                RejectDeleteCustomerCommand.class,
                FinalizeDeleteCustomerCommand.class,
                CreateProductCommand.class,
                ApproveCreateProductCommand.class,
                RejectCreateProductCommand.class,
                FinalizeCreateProductCommand.class,
                ModifyProductCommand.class,
                ApproveModifyProductCommand.class,
                RejectModifyProductCommand.class,
                DeleteProductCommand.class,
                ApproveDeleteProductCommand.class,
                RejectDeleteProductCommand.class,
                FinalizeDeleteProductCommand.class,
                VerifyPaymentCommand.class
        );

        List<Class<? extends EventWithCauseImpl>> eventClasses = List.of(
                CreateCustomerRequestedEvent.class,
                CreateCustomerApprovedEvent.class,
                CreateCustomerRejectedEvent.class,
                DeleteCustomerRequestedEvent.class,
                DeleteCustomerApprovedEvent.class,
                DeleteCustomerRejectedEvent.class,
                CustomerDeletedEvent.class,
                CreateProductRequestedEvent.class,
                CreateProductApprovedEvent.class,
                CreateProductRejectedEvent.class,
                ProductCreatedEvent.class,
                ModifyProductRequestedEvent.class,
                ModifyProductApprovedEvent.class,
                ModifyProductRejectedEvent.class,
                DeleteProductRequestedEvent.class,
                DeleteProductApprovedEvent.class,
                DeleteProductRejectedEvent.class,
                ProductDeletedEvent.class,
                CustomerPaymentVerifiedEvent.class,
                CustomerPaymentApprovalRequiredEvent.class,
                CustomerPaymentRejectedEvent.class
        );

        harness = setupTestHarness(Customer.class, CustomerState.class, commandClasses, eventClasses);
        setupProcessor(Customer.class, CustomerState.class, commandClasses, eventClasses
                , aggregateStateSupplier, stateUpdateHandler, customerEventGenerator, errorHandler
                , harness);
    }

    @Nested
    @DisplayName("CreateCustomerCommand")
    class CreateCustomerCommandTests {

        @Test
        @DisplayName("Given no existing customer when creating a customer then the customer status is New")
        public void testCreateCustomerCommand() {

            var expectedStateVersion = 1L;

            var result = sendCommand(
                    CreateCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .user(LAST_CHANGE_USER)
                            .roles(getRoles("customers/customers:CreateCustomer"))
                            .stateVersion(null)
                            .reply(true)
                            .build());

            assertEquals(CreateCustomerRequestedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    new CreateCustomerRequestedEvent(
                            testCustomer(CustomerStatus.New),
                            LAST_CHANGE_USER,
                            expectedStateVersion,
                            UUID.randomUUID(),
                            1,
                            true),
                    "messageId", "timestamp", "correlationId");

            checkStateStore(result.getState(), testCustomer(CustomerStatus.New, LAST_CHANGE_USER), expectedStateVersion);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given an existing customer not in Rejected state when creating a customer then an error is returned")
        public void testCreateCustomerCommandWhenCustomerExists() {

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live),
                    CreateCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .user(LAST_CHANGE_USER)
                            .roles(getRoles("customers/customers:CreateCustomer"))
                            .stateVersion(null)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            var errorMsg = "Command \"CreateCustomerCommand\" cannot be carried out as customer testCustomer already exists";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.Live, LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given an existing customer in Rejected state when creating a customer then customer status is New")
        public void testCreateCustomerCommandWhenRejectedCustomerExists() {

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Rejected),
                    CreateCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .user(LAST_CHANGE_USER)
                            .roles(getRoles("customers/customers:CreateCustomer"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(CreateCustomerRequestedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    new CreateCustomerRequestedEvent(
                            testCustomer(CustomerStatus.New),
                            LAST_CHANGE_USER,
                            CURRENT_STATE_VERSION + 1,
                            UUID.randomUUID(),
                            1,
                            true),
                    "messageId", "timestamp", "correlationId");

            checkStateStore(result.getState(), testCustomer(CustomerStatus.New, LAST_CHANGE_USER), CURRENT_STATE_VERSION + 1);
            checkExecutionCounts();
        }
    }

    @Nested
    @DisplayName("ApproveCreateCustomerCommand")
    class ApproveCreateCustomerCommandTests {

        @Test
        @DisplayName("Given a customer with New status when approving a customer then the customer status becomes Live")
        public void testApproveCreateCustomerCommand() {

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.New),
                    ApproveCreateCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveCustomerModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(CreateCustomerApprovedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    CreateCustomerApprovedEvent.builder()
                            .carriedState(testCustomer(CustomerStatus.Live))
                            .user(APPROVAL_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");

            var expectedCustomer = testCustomer(CustomerStatus.Live, LAST_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a customer with New status when same user approves a customer then an error is returned")
        public void testApproveCreateCustomerCommandWithSameUserFails() {

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.New),
                    ApproveCreateCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .user(LAST_CHANGE_USER)
                            .roles(getRoles("customers/customers:ApproveCustomerModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            checkAccessDeniedEvent(result.getEvent());
            checkStateStore(result.getState(), testCustomer(CustomerStatus.New, LAST_CHANGE_USER), CURRENT_STATE_VERSION, ACCESS_DENIED_EXCEPTION, ACCESS_DENIED_MSG);
            checkExecutionCounts(1, 0);
        }

        @Test
        @DisplayName("Given a customer not with New status when approving a customer then an error is returned")
        public void testApproveCustomerCommandWhenCustomerStatusNotNew() {

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live),
                    ApproveCreateCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveCustomerModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"ApproveCreateCustomerCommand\" cannot be carried out as customer testCustomer has status Live instead of New";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.Live, LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkExecutionCounts();
        }
    }

    @Nested
    @DisplayName("RejectCreateCustomerCommand")
    class RejectCreateCustomerCommandTests {

        @Test
        @DisplayName("Given a customer with New status when rejecting a customer then the customer status becomes Rejected")
        public void testRejectCreateCustomerCommand() {

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.New),
                    RejectCreateCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .reason(REASON)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveCustomerModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(CreateCustomerRejectedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    CreateCustomerRejectedEvent.builder()
                            .carriedState(testCustomer(CustomerStatus.Rejected))
                            .reason(REASON)
                            .user(APPROVAL_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");

            var expectedCustomer = testCustomer(CustomerStatus.Rejected, LAST_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(false, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a customer with New status when same user rejects a customer then then an error is returned")
        public void testRejectCreateCustomerCommandWithSameUserFails() {

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.New),
                    RejectCreateCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .reason(REASON)
                            .user(LAST_CHANGE_USER)
                            .roles(getRoles("customers/customers:ApproveCustomerModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            checkAccessDeniedEvent(result.getEvent());
            checkStateStore(result.getState(), testCustomer(CustomerStatus.New, LAST_CHANGE_USER), CURRENT_STATE_VERSION, ACCESS_DENIED_EXCEPTION, ACCESS_DENIED_MSG);
            checkExecutionCounts(1, 0);
        }

        @Test
        @DisplayName("Given a customer not with New status when rejecting a customer then an error is returned")
        public void testRejectCustomerCommandWhenCustomerStatusNotNew() {

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live),
                    RejectCreateCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .reason(REASON)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ModifyCustomer"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"RejectCreateCustomerCommand\" cannot be carried out as customer testCustomer has status Live instead of New";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.Live, LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkExecutionCounts();
        }
    }

    @Nested
    @DisplayName("DeleteCustomerCommand")
    class DeleteCustomerCommandTests {

        @Test
        @DisplayName("Given a customer with Live status when deleting a customer then the customer status becomes DeleteRequested")
        public void testDeleteCustomerCommand() {

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live),
                    DeleteCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:ModifyCustomer"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(DeleteCustomerRequestedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    DeleteCustomerRequestedEvent.builder()
                            .carriedState(testCustomer(CustomerStatus.DeleteRequested))
                            .user(NEW_CHANGE_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");

            checkStateStore(result.getState(), testCustomer(CustomerStatus.DeleteRequested, NEW_CHANGE_USER), CURRENT_STATE_VERSION + 1);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a customer not with Live status when deleting a customer then an error is returned")
        public void testDeleteCustomerCommandWhenCustomerStatusNotLive() {

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.New),
                    DeleteCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:ModifyCustomer"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"DeleteCustomerCommand\" cannot be carried out as customer testCustomer has status New instead of Live";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.New, LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkExecutionCounts();
        }
    }

    @Nested
    @DisplayName("ApproveDeleteCustomerCommand")
    class ApproveDeleteCustomerCommandTests {

        @Test
        @DisplayName("Given a customer with DeleteRequested status when approving deleting a customer then the customer status becomes Deleting")
        public void testApproveDeleteCustomerCommand() {

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.DeleteRequested),
                    ApproveDeleteCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveCustomerModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(DeleteCustomerApprovedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    DeleteCustomerApprovedEvent.builder()
                            .carriedState(testCustomer(CustomerStatus.Deleting))
                            .user(APPROVAL_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");

            var expectedCustomer = testCustomer(CustomerStatus.Deleting, APPROVAL_USER).toBuilder()
                    .deleteApproval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a customer with DeleteRequested status when same user approves delete then an error is returned")
        public void testApproveDeleteCustomerCommandWithSameUserFails() {

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.DeleteRequested),
                    ApproveDeleteCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .user(LAST_CHANGE_USER)
                            .roles(getRoles("customers/customers:ApproveCustomerModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            checkAccessDeniedEvent(result.getEvent());
            checkStateStore(result.getState(), testCustomer(CustomerStatus.DeleteRequested, LAST_CHANGE_USER), CURRENT_STATE_VERSION, ACCESS_DENIED_EXCEPTION, ACCESS_DENIED_MSG);
            checkExecutionCounts(1, 0);
        }

        @Test
        @DisplayName("Given a customer not with DeleteRequested status when approving deleting a customer then an error is returned")
        public void testApproveDeleteCustomerCommandWhenCustomerStatusNotDeleteRequested() {

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live),
                    ApproveDeleteCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveCustomerModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"ApproveDeleteCustomerCommand\" cannot be carried out as customer testCustomer has status Live instead of DeleteRequested";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.Live, LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkExecutionCounts();
        }
    }

    @Nested
    @DisplayName("FinalizeDeleteCustomerCommand")
    class FinalizeDeleteCustomerCommandTests {

        @Test
        @DisplayName("Given a customer with Deleting status when finalizing deleting a customer then the customer status becomes Deleted")
        public void testFinalizeDeleteCustomerCommand() {

            var currentCustomer = testCustomer(CustomerStatus.Deleting)
                    .toBuilder()
                    .deleteApproval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            var expectedCustomer = currentCustomer.toBuilder().status(CustomerStatus.Deleted).build();

            var result = setStateAndSendCommand(
                    currentCustomer,
                    FinalizeDeleteCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(CustomerDeletedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    CustomerDeletedEvent.builder()
                            .carriedState(testCustomer(CustomerStatus.Deleted))
                            .user(LAST_CHANGE_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");

            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a customer not with Deleting status when finalizing deleting a customer then an error is returned")
        public void testFinalizeDeleteCustomerCommandWhenCustomerStatusNotDeleteRequested() {

            var currentCustomer = testCustomer(CustomerStatus.Live)
                    .toBuilder()
                    .deleteApproval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();

            var result = setStateAndSendCommand(
                    currentCustomer,
                    FinalizeDeleteCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"FinalizeDeleteCustomerCommand\" cannot be carried out as customer testCustomer has status Live instead of Deleting";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), currentCustomer, CURRENT_STATE_VERSION, errorMsg);
            checkExecutionCounts();
        }
    }

    @Nested
    @DisplayName("RejectDeleteCustomerCommand")
    class RejectDeleteCustomerCommandTests {

        @Test
        @DisplayName("Given a customer with DeleteRequested status when reject deleting a customer then the customer status becomes Live")
        public void testRejectDeleteCustomerCommand() {

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.DeleteRequested),
                    RejectDeleteCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .reason(REASON)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveCustomerModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(DeleteCustomerRejectedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    DeleteCustomerRejectedEvent.builder()
                            .carriedState(testCustomer(CustomerStatus.Live))
                            .reason(REASON)
                            .user(APPROVAL_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");

            var expectedCustomer = testCustomer(CustomerStatus.Live, LAST_CHANGE_USER).toBuilder()
                    .deleteApproval(new CustomerApproval(false, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a customer with DeleteRequested status when same user reject deleting the customer then an error is returned")
        public void testRejectDeleteCustomerCommandWithSameUserFails() {

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.DeleteRequested),
                    RejectDeleteCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .reason(REASON)
                            .user(LAST_CHANGE_USER)
                            .roles(getRoles("customers/customers:ApproveCustomerModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            checkAccessDeniedEvent(result.getEvent());
            checkStateStore(result.getState(), testCustomer(CustomerStatus.DeleteRequested, LAST_CHANGE_USER), CURRENT_STATE_VERSION, ACCESS_DENIED_EXCEPTION, ACCESS_DENIED_MSG);
            checkExecutionCounts(1, 0);
        }

        @Test
        @DisplayName("Given a customer not with DeleteRequested status when reject deleting a customer then an error is returned")
        public void testRejectDeleteCustomerCommandWhenCustomerStatusNotDeleteRequested() {

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live),
                    RejectDeleteCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .reason(REASON)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveCustomerModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"RejectDeleteCustomerCommand\" cannot be carried out as customer testCustomer has status Live instead of DeleteRequested";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.Live, LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkExecutionCounts();
        }
    }

    @Nested
    @DisplayName("CreateProductCommand")
    class CreateProductCommandTests {

        @Test
        @DisplayName("Given a customer with Live status when creating a product then the statuses become ModifyRequested/ModifyProductRequested")
        public void testCreateProductCommand() {

            var productRequest = makeProductRequest(PRODUCT_NAME_1, ACCOUNT_NAME_1);
            var expectedProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.New, ACCOUNT_NAME_1);
            var expectedCustomer = testCustomer(CustomerStatus.ModifyRequested, HashSet.of(expectedProduct), NEW_CHANGE_USER);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live),
                    CreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .productRequest(productRequest)
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:createProduct"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(CreateProductRequestedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    CreateProductRequestedEvent.builder()
                            .productId(PRODUCT_NAME_1)
                            .carriedState(expectedCustomer)
                            .user(NEW_CHANGE_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");
            checkProduct(((CreateProductRequestedEvent) result.getEvent()).getCarriedState().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a customer not with Live status when creating a product then an error is returned")
        public void testCreateProductCommandWhenCustomerStatusNotLive() {

            var productRequest = makeProductRequest(PRODUCT_NAME_1, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.New),
                    CreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .productRequest(productRequest)
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:createProduct"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"CreateProductCommand\" cannot be carried out as customer testCustomer has status New instead of Live";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.New, null, LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a product already exists when creating a product then an error is returned")
        public void testCreateProductCommandWhenProductExists() {

            var productRequest = makeProductRequest(PRODUCT_NAME_1, ACCOUNT_NAME_1);
            var product = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live, HashSet.of(product)),
                    CreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .productRequest(productRequest)
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:createProduct"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"CreateProductCommand\" cannot be carried out as product Product One already exists";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.Live, HashSet.of(product), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given an existing product in Rejected state when creating a product then product status is New")
        public void testCreateProductCommandWhenRejectedProductExists() {

            var productRequest = makeProductRequest(PRODUCT_NAME_1, ACCOUNT_NAME_1);
            var product = makeProduct(PRODUCT_NAME_1, ProductStatus.Rejected, ACCOUNT_NAME_1);
            var expectedProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.New, ACCOUNT_NAME_1);
            var expectedCustomer = testCustomer(CustomerStatus.ModifyRequested, HashSet.of(expectedProduct), NEW_CHANGE_USER);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live, HashSet.of(product)),
                    CreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .productRequest(productRequest)
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:createProduct"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(CreateProductRequestedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    CreateProductRequestedEvent.builder()
                            .productId(PRODUCT_NAME_1)
                            .carriedState(expectedCustomer)
                            .user(NEW_CHANGE_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");
            checkProduct(((CreateProductRequestedEvent) result.getEvent()).getCarriedState().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkExecutionCounts();
        }
    }

    @Nested
    @DisplayName("ApproveCreateProductCommand")
    class ApproveCreateProductCommandTests {

        @Test
        @DisplayName("Given a ModifyRequested customer and New product when approving create of a product then the statuses become ModifyRequested/Creating")
        public void testApproveCreateProductCommand() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.New, ACCOUNT_NAME_1);
            var expectedProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Creating, ACCOUNT_NAME_1).toBuilder()
                    .approval(ProductApproval.builder().approved(true).at(now()).user(APPROVAL_USER).build())
                    .build();
            var expectedCustomer = testCustomer(CustomerStatus.ModifyRequested, HashSet.of(expectedProduct), LAST_CHANGE_USER);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    ApproveCreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModifications"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(CreateProductApprovedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    CreateProductApprovedEvent.builder()
                            .productId(PRODUCT_NAME_1)
                            .carriedState(expectedCustomer)
                            .user(APPROVAL_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");
            checkProduct(((CreateProductApprovedEvent) result.getEvent()).getCarriedState().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a ModifyRequested customer and New product when same user approves create of a product then an error is returned")
        public void testApproveCreateProductCommandWithSameUserFails() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.New, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    ApproveCreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .user(LAST_CHANGE_USER)
                            .roles(getRoles("customers/customers:ApproveProductModifications"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            checkAccessDeniedEvent(result.getEvent());
            checkStateStore(result.getState(), testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, ACCESS_DENIED_EXCEPTION, ACCESS_DENIED_MSG);
            checkExecutionCounts(1, 0);
        }

        @Test
        @DisplayName("Given a not ModifyRequested customer and not New product when approving create of a product then an error is returned")
        public void testApproveCreateProductCommandWhenCustomerStatusNotModifyRequested() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live, HashSet.of(currentProduct)),
                    ApproveCreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"ApproveCreateProductCommand\" cannot be carried out as customer testCustomer has status Live instead of ModifyRequested";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.Live, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), currentProduct);

            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a ModifyRequested customer and not New product when approving create of a product then an error is returned")
        public void testApproveCreateProductCommandWhenProductStatusNotNew() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    ApproveCreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"ApproveCreateProductCommand\" cannot be carried out as product Product One has status Live instead of New";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), currentProduct);

            checkExecutionCounts();
        }
    }

    @Nested
    @DisplayName("RejectCreateProductCommand")
    class RejectCreateProductCommandTests {

        @Test
        @DisplayName("Given a ModifyRequested customer and New product when rejected create of a product then the statuses become Live/Live")
        public void testRejectCreateProductCommand() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.New, ACCOUNT_NAME_1);
            var expectedProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Rejected, ACCOUNT_NAME_1).toBuilder()
                    .approval(ProductApproval.builder().approved(false).at(now()).user(APPROVAL_USER).message(REASON).build())
                    .build();
            var expectedCustomer = testCustomer(CustomerStatus.Live, HashSet.of(expectedProduct), LAST_CHANGE_USER);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    RejectCreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reason(REASON)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(CreateProductRejectedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    CreateProductRejectedEvent.builder()
                            .productId(PRODUCT_NAME_1)
                            .reason(REASON)
                            .carriedState(expectedCustomer)
                            .user(APPROVAL_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");
            checkProduct(((CreateProductRejectedEvent) result.getEvent()).getCarriedState().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a ModifyRequested customer and New product when same user rejects create of a product then an error is returned")
        public void testRejectCreateProductCommandWithSameUserFails() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.New, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    RejectCreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reason(REASON)
                            .user(LAST_CHANGE_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            checkAccessDeniedEvent(result.getEvent());
            checkStateStore(result.getState(), testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, ACCESS_DENIED_EXCEPTION, ACCESS_DENIED_MSG);
            checkExecutionCounts(1, 0);
        }

        @Test
        @DisplayName("Given a not ModifyRequested customer and not New product when rejected create of a product then an error is returned")
        public void testRejectCreateProductCommandWhenCustomerStatusNotModifyRequested() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live, HashSet.of(currentProduct)),
                    RejectCreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reason(REASON)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"RejectCreateProductCommand\" cannot be carried out as customer testCustomer has status Live instead of ModifyRequested";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.Live, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), currentProduct);

            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a ModifyRequested customer and not New product when rejected create of a product then an error is returned")
        public void testRejectCreateProductCommandWhenProductStatusNotNew() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    RejectCreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reason(REASON)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"RejectCreateProductCommand\" cannot be carried out as product Product One has status Live instead of New";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), currentProduct);

            checkExecutionCounts();
        }
    }

    @Nested
    @DisplayName("FinalizeCreateProductCommand")
    class FinalizeCreateProductCommandTests {

        @Test
        @DisplayName("Given a ModifyRequested customer and Creating product when finalizing create of a product then the statuses become Live/Live")
        public void testFinalizeCreateProductCommand() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Creating, ACCOUNT_NAME_1).toBuilder()
                    .approval(ProductApproval.builder().approved(true).at(now()).user(APPROVAL_USER).build())
                    .build();
            var expectedProduct = currentProduct.toBuilder().status(ProductStatus.Live).build();
            var expectedCustomer = testCustomer(CustomerStatus.Live, HashSet.of(expectedProduct), LAST_CHANGE_USER);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    FinalizeCreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(ProductCreatedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    ProductCreatedEvent.builder()
                            .productId(PRODUCT_NAME_1)
                            .carriedState(expectedCustomer)
                            .user(LAST_CHANGE_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");
            checkProduct(((ProductCreatedEvent) result.getEvent()).getCarriedState().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a not ModifyRequested customer and not New product when finalizing create of a product then an error is returned")
        public void testFinalizeCreateProductCommandWhenCustomerStatusNotModifyRequested() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live, HashSet.of(currentProduct)),
                    FinalizeCreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"FinalizeCreateProductCommand\" cannot be carried out as customer testCustomer has status Live instead of ModifyRequested";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.Live, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), currentProduct);

            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a ModifyRequested customer and not New product when finalizing create of a product then an error is returned")
        public void testFinalizeCreateProductCommandWhenProductStatusNotNew() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    FinalizeCreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"FinalizeCreateProductCommand\" cannot be carried out as product Product One has status Live instead of Creating";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), currentProduct);

            checkExecutionCounts();
        }

    }

    @Nested
    @DisplayName("ModifyProductCommand")
    class ModifyProductCommandTests {

        @Test
        @DisplayName("Given a Live customer and Live product when requesting modify of a product then the statuses become ModifyRequested/ModifyRequested")
        public void testModifyProductCommand() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1);
            var productRequest = makeProductRequest(PRODUCT_NAME_1, ACCOUNT_NAME_2);
            var expectedProduct = currentProduct.toBuilder()
                    .status(ProductStatus.ModifyRequested)
                    .pendingProductConfiguration(makeProductConfiguration(productAdapter, ACCOUNT_NAME_2))
                    .build();
            var expectedCustomer = testCustomer(CustomerStatus.ModifyRequested, HashSet.of(expectedProduct), NEW_CHANGE_USER);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live, HashSet.of(currentProduct)),
                    ModifyProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .productRequest(productRequest)
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:ModifyProduct"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(ModifyProductRequestedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    ModifyProductRequestedEvent.builder()
                            .productId(PRODUCT_NAME_1)
                            .carriedState(expectedCustomer)
                            .user(NEW_CHANGE_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");
            checkProduct(((ModifyProductRequestedEvent) result.getEvent()).getCarriedState().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a not Live customer and Live product when requesting modify of a product then an error is returned")
        public void testModifyProductCommandWithNotLiveCustomer() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1);
            var productRequest = makeProductRequest(PRODUCT_NAME_1, ACCOUNT_NAME_2);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.DeleteRequested, HashSet.of(currentProduct)),
                    ModifyProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .productRequest(productRequest)
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:ModifyProduct"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"ModifyProductCommand\" cannot be carried out as customer testCustomer has status DeleteRequested instead of Live";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.DeleteRequested, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a Live customer and not Live product when requesting modify of a product then an error is returned")
        public void testModifyProductCommandWithNotLiveProduct() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.New, ACCOUNT_NAME_1);
            var productRequest = makeProductRequest(PRODUCT_NAME_1, ACCOUNT_NAME_2);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live, HashSet.of(currentProduct)),
                    ModifyProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .productRequest(productRequest)
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:ModifyProduct"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"ModifyProductCommand\" cannot be carried out as product Product One has status New instead of Live";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.Live, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkExecutionCounts();
        }
    }

    @Nested
    @DisplayName("ApproveModifyProductCommand")
    class ApproveModifyProductCommandTests {

        @Test
        @DisplayName("Given a ModifyRequested customer and ModifyRequested product when approving modify of a product then the statuses become Live/Live")
        public void testApproveModifyProductCommand() {

            var currentProduct = makeProductWithPendingConfiguration(PRODUCT_NAME_1, ProductStatus.ModifyRequested, ACCOUNT_NAME_1, ACCOUNT_NAME_2);
            var expectedProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_2);
            var expectedCustomer = testCustomer(CustomerStatus.Live, HashSet.of(expectedProduct), LAST_CHANGE_USER);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    ApproveModifyProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(ModifyProductApprovedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    ModifyProductApprovedEvent.builder()
                            .productId(PRODUCT_NAME_1)
                            .carriedState(expectedCustomer)
                            .user(APPROVAL_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");

            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a ModifyRequested customer and ModifyRequested product when same user approves modify of a product then an error is returned")
        public void testApproveModifyProductCommandWithSameUserFails() {

            var currentProduct = makeProductWithPendingConfiguration(PRODUCT_NAME_1, ProductStatus.ModifyRequested, ACCOUNT_NAME_1, ACCOUNT_NAME_2);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    ApproveModifyProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .user(LAST_CHANGE_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            checkAccessDeniedEvent(result.getEvent());
            checkStateStore(result.getState(), testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, ACCESS_DENIED_EXCEPTION, ACCESS_DENIED_MSG);
            checkExecutionCounts(1, 0);
        }

        @Test
        @DisplayName("Given a not ModifyRequested customer and not ModifyRequested product when approving modify of a product then an error is returned")
        public void testApproveModifyProductCommandWithCustomerAndProductInWrongState() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live, HashSet.of(currentProduct)),
                    ApproveModifyProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"ApproveModifyProductCommand\" cannot be carried out as customer testCustomer has status Live instead of ModifyRequested";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.Live, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a ModifyRequested customer and not ModifyRequested product when approving modify of a product then an error is returned")
        public void testApproveModifyProductCommandWithProductInWrongState() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    ApproveModifyProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"ApproveModifyProductCommand\" cannot be carried out as product Product One has status Live instead of ModifyRequested";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkExecutionCounts();
        }
    }

    @Nested
    @DisplayName("RejectModifyProductCommand")
    class RejectModifyProductCommandTests {

        @Test
        @DisplayName("Given a ModifyRequested customer and ModifyRequested product when rejecting modify of a product then the statuses become Live/Live")
        public void testRejectModifyProductCommand() {

            var currentProduct = makeProductWithPendingConfiguration(PRODUCT_NAME_1, ProductStatus.ModifyRequested, ACCOUNT_NAME_1, ACCOUNT_NAME_2);
            var expectedProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1);
            var expectedCustomer = testCustomer(CustomerStatus.Live, HashSet.of(expectedProduct), LAST_CHANGE_USER);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    RejectModifyProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reason(REASON)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(ModifyProductRejectedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    ModifyProductRejectedEvent.builder()
                            .productId(PRODUCT_NAME_1)
                            .reason(REASON)
                            .carriedState(expectedCustomer)
                            .user(APPROVAL_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");

            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a ModifyRequested customer and ModifyRequested product when same user rejects modify of a product then an error is returned")
        public void testRejectModifyProductCommandWithSameUserFails() {

            var currentProduct = makeProductWithPendingConfiguration(PRODUCT_NAME_1, ProductStatus.ModifyRequested, ACCOUNT_NAME_1, ACCOUNT_NAME_2);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    RejectModifyProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reason(REASON)
                            .user(LAST_CHANGE_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            checkAccessDeniedEvent(result.getEvent());
            checkStateStore(result.getState(), testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, ACCESS_DENIED_EXCEPTION, ACCESS_DENIED_MSG);
            checkExecutionCounts(1, 0);
        }

        @Test
        @DisplayName("Given a not ModifyRequested customer and not ModifyRequested product when rejecting modify of a product then an error is returned")
        public void testRejectModifyProductCommandWithCustomerAndProductInWrongState() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live, HashSet.of(currentProduct)),
                    RejectModifyProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reason(REASON)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"RejectModifyProductCommand\" cannot be carried out as customer testCustomer has status Live instead of ModifyRequested";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.Live, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a ModifyRequested customer and not ModifyRequested product when rejecting modify of a product then an error is returned")
        public void testRejectModifyProductCommandWithProductInWrongState() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    RejectModifyProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reason(REASON)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"RejectModifyProductCommand\" cannot be carried out as product Product One has status Live instead of ModifyRequested";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkExecutionCounts();
        }
    }

    @Nested
    @DisplayName("DeleteProductCommand")
    class DeleteProductCommandTests {

        @Test
        @DisplayName("Given a Live customer and Live product when deleting a product then the statuses become ModifyRequested/DeleteRequested")
        public void testDeleteProductCommand() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1);
            var expectedProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.DeleteRequested, ACCOUNT_NAME_1);
            var expectedCustomer = testCustomer(CustomerStatus.ModifyRequested, HashSet.of(expectedProduct), NEW_CHANGE_USER);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live, HashSet.of(currentProduct)),
                    DeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:ModifyProduct"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(DeleteProductRequestedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    DeleteProductRequestedEvent.builder()
                            .productId(PRODUCT_NAME_1)
                            .carriedState(expectedCustomer)
                            .user(NEW_CHANGE_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");

            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a not Live customer when deleting a product then an error is returned")
        public void testDeleteProductCommandWithCustomerInWrongState() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Deleting, HashSet.of(currentProduct)),
                    DeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:ModifyProduct"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"DeleteProductCommand\" cannot be carried out as customer testCustomer has status Deleting instead of Live";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.Deleting, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a Live customer and not Live product when deleting a product then an error is returned")
        public void testDeleteProductCommandWithProductInWrongState() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.ModifyRequested, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live, HashSet.of(currentProduct)),
                    DeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:ModifyProduct"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"DeleteProductCommand\" cannot be carried out as product Product One has status ModifyRequested instead of Live";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.Live, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkExecutionCounts();
        }
    }

    @Nested
    @DisplayName("ApproveDeleteProductCommand")
    class ApproveDeleteProductCommandTests {

        @Test
        @DisplayName("Given a ModifyRequested customer and DeleteRequested product when approving deleting a product then the statuses become Live/Deleting")
        public void testApproveDeleteProductCommand() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.DeleteRequested, ACCOUNT_NAME_1);
            var expectedProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Deleting, ACCOUNT_NAME_1).toBuilder()
                    .deleteApproval(ProductApproval.builder().approved(true).at(now()).user(APPROVAL_USER).build())
                    .build();
            var expectedCustomer = testCustomer(CustomerStatus.ModifyRequested, HashSet.of(expectedProduct), LAST_CHANGE_USER);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    ApproveDeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(DeleteProductApprovedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    DeleteProductApprovedEvent.builder()
                            .productId(PRODUCT_NAME_1)
                            .carriedState(expectedCustomer)
                            .user(APPROVAL_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");
            checkProduct(((DeleteProductApprovedEvent) result.getEvent()).getCarriedState().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a ModifyRequested customer and DeleteRequested product when same user approving deleting a product then an error is returned")
        public void testApproveDeleteProductCommandWithSameUserFails() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.DeleteRequested, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    ApproveDeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .user(LAST_CHANGE_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            checkAccessDeniedEvent(result.getEvent());
            checkStateStore(result.getState(), testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, ACCESS_DENIED_EXCEPTION, ACCESS_DENIED_MSG);
            checkExecutionCounts(1, 0);
        }

        @Test
        @DisplayName("Given a not ModifyRequested customer when approving deleting a product then an error is returned")
        public void testApproveDeleteProductCommandWithCustomerInWrongState() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.DeleteRequested, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Deleting, HashSet.of(currentProduct)),
                    ApproveDeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"ApproveDeleteProductCommand\" cannot be carried out as customer testCustomer has status Deleting instead of ModifyRequested";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.Deleting, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), currentProduct);

            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a ModifyRequested customer and not DeleteRequested product when approving deleting a product then an error is returned")
        public void testApproveDeleteProductCommandWithProductInWrongState() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Deleting, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    ApproveDeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"ApproveDeleteProductCommand\" cannot be carried out as product Product One has status Deleting instead of DeleteRequested";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), currentProduct);

            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a Deleting customer and DeleteRequested product when approving deleting a product then the statuses become Deleting/Deleting")
        public void testApproveDeleteProductCommandForDeletingCustomerAndDeleteRequestedProduct() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.DeleteRequested, ACCOUNT_NAME_1);
            var expectedProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Deleting, ACCOUNT_NAME_1).toBuilder()
                    .deleteApproval(ProductApproval.builder().approved(true).at(now()).user(APPROVAL_USER).build())
                    .build();
            var expectedCustomer = testCustomer(CustomerStatus.Deleting, HashSet.of(expectedProduct), LAST_CHANGE_USER);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Deleting, HashSet.of(currentProduct)),
                    ApproveDeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .forceDelete(true)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(DeleteProductApprovedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    DeleteProductApprovedEvent.builder()
                            .productId(PRODUCT_NAME_1)
                            .carriedState(expectedCustomer)
                            .user(APPROVAL_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");
            checkProduct(((DeleteProductApprovedEvent) result.getEvent()).getCarriedState().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a Deleting customer and Live product when approving deleting a product then the statuses become Deleting/Deleting")
        public void testApproveDeleteProductCommandForDeletingCustomerAndLiveProduct() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1);
            var expectedProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Deleting, ACCOUNT_NAME_1).toBuilder()
                    .deleteApproval(ProductApproval.builder().approved(true).at(now()).user(APPROVAL_USER).build())
                    .build();
            var expectedCustomer = testCustomer(CustomerStatus.Deleting, HashSet.of(expectedProduct), LAST_CHANGE_USER);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Deleting, HashSet.of(currentProduct)),
                    ApproveDeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .forceDelete(true)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(DeleteProductApprovedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    DeleteProductApprovedEvent.builder()
                            .productId(PRODUCT_NAME_1)
                            .carriedState(expectedCustomer)
                            .user(APPROVAL_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");
            checkProduct(((DeleteProductApprovedEvent) result.getEvent()).getCarriedState().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkExecutionCounts();
        }
    }

    @Nested
    @DisplayName("RejectDeleteProductCommand")
    class RejectDeleteProductCommandTests {

        @Test
        @DisplayName("Given a ModifyRequested customer and DeleteRequested product when rejecting deleting a product then the statuses become Live/Live")
        public void testRejectDeleteProductCommand() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.DeleteRequested, ACCOUNT_NAME_1);
            var expectedProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1).toBuilder()
                    .deleteApproval(ProductApproval.builder().approved(false).at(now()).user(APPROVAL_USER).build())
                    .build();
            var expectedCustomer = testCustomer(CustomerStatus.Live, HashSet.of(expectedProduct), LAST_CHANGE_USER);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    RejectDeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reason(REASON)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(DeleteProductRejectedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    DeleteProductRejectedEvent.builder()
                            .productId(PRODUCT_NAME_1)
                            .reason(REASON)
                            .carriedState(expectedCustomer)
                            .user(APPROVAL_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");
            checkProduct(((DeleteProductRejectedEvent) result.getEvent()).getCarriedState().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a ModifyRequested customer and DeleteRequested product when same user rejects deleting a product then an error is returned")
        public void testRejectDeleteProductCommandWithSameUserFails() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.DeleteRequested, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    RejectDeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reason(REASON)
                            .user(LAST_CHANGE_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            checkAccessDeniedEvent(result.getEvent());
            checkStateStore(result.getState(), testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, ACCESS_DENIED_EXCEPTION, ACCESS_DENIED_MSG);
            checkExecutionCounts(1, 0);
        }

        @Test
        @DisplayName("Given a not ModifyRequested customer when rejecting deleting a product then an error is returned")
        public void testRejectDeleteProductCommandWithCustomerInWrongState() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.DeleteRequested, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Deleting, HashSet.of(currentProduct)),
                    RejectDeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reason(REASON)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"RejectDeleteProductCommand\" cannot be carried out as customer testCustomer has status Deleting instead of ModifyRequested";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.Deleting, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), currentProduct);

            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a ModifyRequested customer and not DeleteRequested product when rejecting deleting a product then an error is returned")
        public void testRejectDeleteProductCommandWithProductInWrongState() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Deleting, ACCOUNT_NAME_1);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    RejectDeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reason(REASON)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(CURRENT_STATE_VERSION)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"RejectDeleteProductCommand\" cannot be carried out as product Product One has status Deleting instead of DeleteRequested";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), currentProduct);

            checkExecutionCounts();
        }
    }

    @Nested
    @DisplayName("FinalizeDeleteProductCommand")
    class FinalizeDeleteProductCommandTests {

        @Test
        @DisplayName("Given a ModifyRequested customer and Deleting product when finalizing deleting a product then the statuses become Live/Deleted")
        public void testFinalizeDeleteProductCommand() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Deleting, ACCOUNT_NAME_1).toBuilder()
                    .deleteApproval(ProductApproval.builder().approved(true).at(now()).user(APPROVAL_USER).build())
                    .build();
            var expectedProduct = currentProduct.toBuilder().status(ProductStatus.Deleted).build();
            var expectedCustomer = testCustomer(CustomerStatus.Live, HashSet.of(expectedProduct), LAST_CHANGE_USER);

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    FinalizeDeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(ProductDeletedEvent.class, result.getEvent().getClass());
            assertThat(result.getEvent()).isEqualToIgnoringGivenFields(
                    ProductDeletedEvent.builder()
                            .productId(PRODUCT_NAME_1)
                            .carriedState(expectedCustomer)
                            .user(LAST_CHANGE_USER)
                            .stateVersion(CURRENT_STATE_VERSION + 1)
                            .executionCount(1)
                            .reply(true)
                            .build(),
                    "messageId", "timestamp", "correlationId");
            checkProduct(((ProductDeletedEvent) result.getEvent()).getCarriedState().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkStateStore(result.getState(), expectedCustomer, CURRENT_STATE_VERSION + 1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct);

            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a not ModifyRequested customer when finalizing deleting a product then an error is returned")
        public void testFinalizeDeleteProductCommandWithCustomerInWrongState() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Deleting, ACCOUNT_NAME_1).toBuilder()
                    .deleteApproval(ProductApproval.builder().approved(true).at(now()).user(APPROVAL_USER).build())
                    .build();

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Deleting, HashSet.of(currentProduct)),
                    FinalizeDeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"FinalizeDeleteProductCommand\" cannot be carried out as customer testCustomer has status Deleting instead of ModifyRequested";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.Deleting, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), currentProduct);

            checkExecutionCounts();
        }

        @Test
        @DisplayName("Given a ModifyRequested customer and not Deleting product when finalizing deleting a product then an error is returned")
        public void testFinalizeDeleteProductCommandWithProductInWrongState() {

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1).toBuilder()
                    .deleteApproval(ProductApproval.builder().approved(true).at(now()).user(APPROVAL_USER).build())
                    .build();

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct)),
                    FinalizeDeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            String errorMsg = "Command \"FinalizeDeleteProductCommand\" cannot be carried out as product Product One has status Live instead of Deleting";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            checkStateStore(result.getState(), testCustomer(CustomerStatus.ModifyRequested, HashSet.of(currentProduct), LAST_CHANGE_USER), CURRENT_STATE_VERSION, errorMsg);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), currentProduct);

            checkExecutionCounts();
        }
    }


    @Nested
    @DisplayName("VerifyPaymentCommand")
    class VerifyPaymentCommandTests {

        @Test
        public void testVerifyPaymentProductNotLive() {

            var payment = Payment.builder()
                    .createdBy("user")
                    .allowedMethods(List.of(PaymentMethod.Bacs))
                    .creditor(PartyIdentification.builder().name("").build())
                    .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                    .debtor(PartyIdentification.builder().name("").build())
                    .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                    .amount(Money.parse("GBP 1.00"))
                    .latestExecutionDate(Instant.now())
                    .product(new CompositeReference(TEST_CUSTOMER,PRODUCT_NAME_1))
                    .build();

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Deleting, ACCOUNT_NAME_1).toBuilder()
                    .deleteApproval(ProductApproval.builder().approved(true).at(now()).user(APPROVAL_USER).build())
                    .build();

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.New, HashSet.of(currentProduct)),
                    VerifyPaymentCommand.builder()
                            .payment(payment)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(CustomerPaymentRejectedEvent.class, result.getEvent().getClass());
        }

        @Test
        public void testVerifyPaymentNoProduct() {

            var payment = Payment.builder()
                    .createdBy("user")
                    .allowedMethods(List.of(PaymentMethod.Bacs))
                    .creditor(PartyIdentification.builder().name("").build())
                    .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                    .debtor(PartyIdentification.builder().name("").build())
                    .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                    .amount(Money.parse("GBP 1.00"))
                    .latestExecutionDate(Instant.now())
                    .product(new CompositeReference(TEST_CUSTOMER,PRODUCT_NAME_1))
                    .build();

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.New, HashSet.of()),
                    VerifyPaymentCommand.builder()
                            .payment(payment)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(CustomerPaymentRejectedEvent.class, result.getEvent().getClass());
        }

        @Test
        public void testVerifyPaymentCustomerNotLive() {

            var payment = Payment.builder()
                    .createdBy("user")
                    .allowedMethods(List.of(PaymentMethod.Bacs))
                    .creditor(PartyIdentification.builder().name("").build())
                    .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                    .debtor(PartyIdentification.builder().name("").build())
                    .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                    .amount(Money.parse("GBP 1.00"))
                    .latestExecutionDate(Instant.now())
                    .product(new CompositeReference(TEST_CUSTOMER,PRODUCT_NAME_1))
                    .build();

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1).toBuilder()
                    .deleteApproval(ProductApproval.builder().approved(true).at(now()).user(APPROVAL_USER).build())
                    .build();

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.New, HashSet.of(currentProduct)),
                    VerifyPaymentCommand.builder()
                            .payment(payment)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(CustomerPaymentRejectedEvent.class, result.getEvent().getClass());
        }

        @Test
        public void testVerifyPaymentValid() {
            var payment = Payment.builder()
                    .createdBy("user")
                    .allowedMethods(List.of(PaymentMethod.Bacs))
                    .creditor(PartyIdentification.builder().name("").build())
                    .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                    .debtor(PartyIdentification.builder().name("").build())
                    .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                    .amount(Money.parse("GBP 1.00"))
                    .latestExecutionDate(Instant.now())
                    .product(new CompositeReference(TEST_CUSTOMER,PRODUCT_NAME_1))
                    .build();

            var currentProduct = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1).toBuilder()
                    .deleteApproval(ProductApproval.builder().approved(true).at(now()).user(APPROVAL_USER).build())
                    .build();

            var result = setStateAndSendCommand(
                    testCustomer(CustomerStatus.Live, HashSet.of(currentProduct)),
                    VerifyPaymentCommand.builder()
                            .payment(payment)
                            .reply(true)
                            .build(),
                    CURRENT_STATE_VERSION);

            assertEquals(CustomerPaymentVerifiedEvent.class, result.getEvent().getClass());
        }
    }

    @Test
    @DisplayName("End to end test with multiple products")
    public void endToEndTestWithMultipleProducts() {

        try (final TopologyTestDriver topology = harness.toTopology()) {

            var currentVersion = 0L;

            // Create customer
            var result = sendCommand(
                    topology,
                    CreateCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .user(LAST_CHANGE_USER)
                            .roles(getRoles("customers/customers:CreateCustomer"))
                            .stateVersion(null)
                            .reply(true)
                            .build());

            currentVersion++;
            assertEquals(currentVersion, result.getState().getCurrentVersion());
            var expectedCustomer = testCustomer(CustomerStatus.New, LAST_CHANGE_USER);
            checkStateStore(result.getState(), expectedCustomer, currentVersion);

            // Approve create customer
            result = sendCommand(
                    topology,
                    ApproveCreateCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveCustomerModification"))
                            .stateVersion(currentVersion)
                            .reply(true)
                            .build());

            currentVersion++;
            assertEquals(currentVersion, result.getState().getCurrentVersion());
            expectedCustomer = testCustomer(CustomerStatus.Live, LAST_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion);

            // Create product 1 and approve it

            var productRequest1 = makeProductRequest(PRODUCT_NAME_1, ACCOUNT_NAME_1);
            result = sendCommand(
                    topology,
                    CreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .productRequest(productRequest1)
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:createProduct"))
                            .stateVersion(currentVersion)
                            .reply(true)
                            .build());

            currentVersion++;
            var expectedProduct1 = makeProduct(PRODUCT_NAME_1, ProductStatus.New, ACCOUNT_NAME_1);
            expectedCustomer = testCustomer(CustomerStatus.ModifyRequested, HashSet.of(expectedProduct1), NEW_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct1);

            result = sendCommand(
                    topology,
                    ApproveCreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModifications"))
                            .stateVersion(currentVersion)
                            .reply(true)
                            .build());

            currentVersion++;
            expectedProduct1 = makeProduct(PRODUCT_NAME_1, ProductStatus.Creating, ACCOUNT_NAME_1).toBuilder()
                    .approval(ProductApproval.builder().approved(true).at(now()).user(APPROVAL_USER).build())
                    .build();
            expectedCustomer = testCustomer(CustomerStatus.ModifyRequested, HashSet.of(expectedProduct1), NEW_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct1);

            // Finalize creation of product 1

            result = sendCommand(
                    topology,
                    FinalizeCreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .reply(true)
                            .build());

            currentVersion++;
            expectedProduct1 = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_1).toBuilder()
                    .approval(ProductApproval.builder().approved(true).at(now()).user(APPROVAL_USER).build())
                    .build();
            expectedCustomer = testCustomer(CustomerStatus.Live, HashSet.of(expectedProduct1), NEW_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct1);

            // Create product 2 and reject it

            var productRequest2 = makeProductRequest(PRODUCT_NAME_2, ACCOUNT_NAME_2);
            result = sendCommand(
                    topology,
                    CreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_2))
                            .productRequest(productRequest2)
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:createProduct"))
                            .stateVersion(currentVersion)
                            .reply(true)
                            .build());

            currentVersion++;
            var expectedProduct2 = makeProduct(PRODUCT_NAME_2, ProductStatus.New, ACCOUNT_NAME_2);
            expectedCustomer = testCustomer(CustomerStatus.ModifyRequested, HashSet.of(expectedProduct1, expectedProduct2), NEW_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_2).get(), expectedProduct2);

            result = sendCommand(
                    topology,
                    RejectCreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_2))
                            .reason(REASON)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModifications"))
                            .stateVersion(currentVersion)
                            .reply(true)
                            .build());

            currentVersion++;
            expectedProduct2 = makeProduct(PRODUCT_NAME_2, ProductStatus.Rejected, ACCOUNT_NAME_2).toBuilder()
                    .approval(ProductApproval.builder().approved(false).at(now()).user(APPROVAL_USER).message(REASON).build())
                    .build();
            expectedCustomer = testCustomer(CustomerStatus.Live, HashSet.of(expectedProduct1, expectedProduct2), NEW_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_2).get(), expectedProduct2);

            // Create product 3 and approve it

            var productRequest3 = makeProductRequest(PRODUCT_NAME_3, ACCOUNT_NAME_3);
            result = sendCommand(
                    topology,
                    CreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_3))
                            .productRequest(productRequest3)
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:createProduct"))
                            .stateVersion(currentVersion)
                            .reply(true)
                            .build());

            currentVersion++;
            var expectedProduct3 = makeProduct(PRODUCT_NAME_3, ProductStatus.New, ACCOUNT_NAME_3);
            expectedCustomer = testCustomer(CustomerStatus.ModifyRequested, HashSet.of(expectedProduct1, expectedProduct2, expectedProduct3), NEW_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_2).get(), expectedProduct2);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_3).get(), expectedProduct3);

            result = sendCommand(
                    topology,
                    ApproveCreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_3))
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModifications"))
                            .stateVersion(currentVersion)
                            .reply(true)
                            .build());

            currentVersion++;
            expectedProduct3 = makeProduct(PRODUCT_NAME_3, ProductStatus.Creating, ACCOUNT_NAME_3).toBuilder()
                    .approval(ProductApproval.builder().approved(true).at(now()).user(APPROVAL_USER).build())
                    .build();
            expectedCustomer = testCustomer(CustomerStatus.ModifyRequested, HashSet.of(expectedProduct1, expectedProduct2, expectedProduct3), NEW_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_2).get(), expectedProduct2);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_3).get(), expectedProduct3);

            // Finalize creation of product 3

            result = sendCommand(
                    topology,
                    FinalizeCreateProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_3))
                            .reply(true)
                            .build());

            currentVersion++;
            expectedProduct3 = makeProduct(PRODUCT_NAME_3, ProductStatus.Live, ACCOUNT_NAME_3).toBuilder()
                    .approval(ProductApproval.builder().approved(true).at(now()).user(APPROVAL_USER).build())
                    .build();
            expectedCustomer = testCustomer(CustomerStatus.Live, HashSet.of(expectedProduct1, expectedProduct2, expectedProduct3), NEW_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_2).get(), expectedProduct2);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_3).get(), expectedProduct3);

            // Modify product 1

            var productRequest = makeProductRequest(PRODUCT_NAME_1, ACCOUNT_NAME_2);

            result = sendCommand(
                    topology,
                    ModifyProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .productRequest(productRequest)
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:ModifyProduct"))
                            .stateVersion(currentVersion)
                            .reply(true)
                            .build());

            currentVersion++;
            expectedProduct1 = expectedProduct1.toBuilder()
                    .status(ProductStatus.ModifyRequested)
                    .pendingProductConfiguration(updateAccountName(expectedProduct1.getProductConfiguration(), ACCOUNT_NAME_2))
                    .build();
            expectedCustomer = testCustomer(CustomerStatus.ModifyRequested, HashSet.of(expectedProduct1, expectedProduct2, expectedProduct3), NEW_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_2).get(), expectedProduct2);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_3).get(), expectedProduct3);

            // Try to modify product 3 (should fail)

            productRequest = makeProductRequest(PRODUCT_NAME_3, ACCOUNT_NAME_1);

            result = sendCommand(
                    topology,
                    ModifyProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_3))
                            .productRequest(productRequest)
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:ModifyProduct"))
                            .stateVersion(currentVersion)
                            .reply(true)
                            .build());


            String errorMsg = "Command \"ModifyProductCommand\" cannot be carried out as customer testCustomer has status ModifyRequested instead of Live";
            checkGenericErrorEvent(result.getEvent(), errorMsg);
            expectedCustomer = testCustomer(CustomerStatus.ModifyRequested, HashSet.of(expectedProduct1, expectedProduct2, expectedProduct3), NEW_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion, errorMsg);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_2).get(), expectedProduct2);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_3).get(), expectedProduct3);

            // Approve modify product 1

            result = sendCommand(
                    topology,
                    ApproveModifyProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(currentVersion)
                            .reply(true)
                            .build());

            currentVersion++;
            expectedProduct1 = makeProduct(PRODUCT_NAME_1, ProductStatus.Live, ACCOUNT_NAME_2).toBuilder()
                    .approval(ProductApproval.builder().approved(true).at(now()).user(APPROVAL_USER).build())
                    .build();
            expectedCustomer = testCustomer(CustomerStatus.Live, HashSet.of(expectedProduct1, expectedProduct2, expectedProduct3), NEW_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_2).get(), expectedProduct2);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_3).get(), expectedProduct3);

            // Delete product 3

            result = sendCommand(
                    topology,
                    DeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_3))
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:ModifyProduct"))
                            .stateVersion(currentVersion)
                            .reply(true)
                            .build());

            currentVersion++;
            expectedProduct3 = expectedProduct3.toBuilder().status(ProductStatus.DeleteRequested).build();
            expectedCustomer = testCustomer(CustomerStatus.ModifyRequested, HashSet.of(expectedProduct1, expectedProduct2, expectedProduct3), NEW_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_2).get(), expectedProduct2);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_3).get(), expectedProduct3);

            // Approve delete product 3

            result = sendCommand(
                    topology,
                    ApproveDeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_3))
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(currentVersion)
                            .reply(true)
                            .build());

            currentVersion++;
            expectedProduct3 = expectedProduct3.toBuilder().status(ProductStatus.Deleting)
                    .deleteApproval(new ProductApproval(true, APPROVAL_USER, now(), null))
                    .build();
            expectedCustomer = testCustomer(CustomerStatus.ModifyRequested, HashSet.of(expectedProduct1, expectedProduct2, expectedProduct3), NEW_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_2).get(), expectedProduct2);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_3).get(), expectedProduct3);

            // Finalize delete product 3

            result = sendCommand(
                    topology,
                    FinalizeDeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_3))
                            .reply(true)
                            .build());

            currentVersion++;
            expectedProduct3 = expectedProduct3.toBuilder().status(ProductStatus.Deleted)
                    .deleteApproval(new ProductApproval(true, APPROVAL_USER, now(), null))
                    .build();
            expectedCustomer = testCustomer(CustomerStatus.Live, HashSet.of(expectedProduct1, expectedProduct2, expectedProduct3), NEW_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_2).get(), expectedProduct2);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_3).get(), expectedProduct3);

            // Delete customer

            result = sendCommand(
                    topology,
                    DeleteCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .user(NEW_CHANGE_USER)
                            .roles(getRoles("customers/customers:ModifyCustomer"))
                            .stateVersion(currentVersion)
                            .reply(true)
                            .build());

            currentVersion++;
            assertEquals(currentVersion, result.getState().getCurrentVersion());

            // Approve delete customer

            result = sendCommand(
                    topology,
                    ApproveDeleteCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveCustomerModification"))
                            .stateVersion(currentVersion)
                            .reply(true)
                            .build());

            currentVersion++;
            assertEquals(currentVersion, result.getState().getCurrentVersion());
            expectedCustomer = testCustomer(CustomerStatus.Deleting, HashSet.of(expectedProduct1, expectedProduct2, expectedProduct3), NEW_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .deleteApproval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_2).get(), expectedProduct2);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_3).get(), expectedProduct3);

            // Force delete product 1 (as if from PM)
            // Product 2 was rejected (so never created) and product 3 is already deleted

            result = sendCommand(
                    topology,
                    ApproveDeleteProductCommand.builder()
                            .productId(new CompositeReference(TEST_CUSTOMER, PRODUCT_NAME_1))
                            .forceDelete(true)
                            .user(APPROVAL_USER)
                            .roles(getRoles("customers/customers:ApproveProductModification"))
                            .stateVersion(currentVersion)
                            .reply(true)
                            .build());

            currentVersion++;
            expectedProduct1 = expectedProduct1.toBuilder().status(ProductStatus.Deleting)
                    .deleteApproval(new ProductApproval(true, APPROVAL_USER, now(), null))
                    .build();
            expectedCustomer = testCustomer(CustomerStatus.Deleting, HashSet.of(expectedProduct1, expectedProduct2, expectedProduct3), NEW_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .deleteApproval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_2).get(), expectedProduct2);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_3).get(), expectedProduct3);

            // Finalize delete customer

            result = sendCommand(
                    topology,
                    FinalizeDeleteCustomerCommand.builder()
                            .customerId(TEST_CUSTOMER)
                            .reply(true)
                            .build());

            currentVersion++;
            assertEquals(currentVersion, result.getState().getCurrentVersion());
            expectedCustomer = testCustomer(CustomerStatus.Deleted, HashSet.of(expectedProduct1, expectedProduct2, expectedProduct3), NEW_CHANGE_USER).toBuilder()
                    .approval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .deleteApproval(new CustomerApproval(true, APPROVAL_USER, now(), null))
                    .build();
            checkStateStore(result.getState(), expectedCustomer, currentVersion);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_1).get(), expectedProduct1);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_2).get(), expectedProduct2);
            checkProduct(result.getState().getCurrent().getProduct(PRODUCT_NAME_3).get(), expectedProduct3);
        }
    }

    //
    // Utility methods for tests
    //

    private ProductConfiguration updateAccountName(ProductConfiguration productConfiguration, String accountName) {

        var newOutboundConfiguration = productConfiguration.getOutboundConfiguration().toBuilder()
                .source(makeUKBankAccount(accountName))
                .returns(makeUKBankAccount(accountName))
                .build();

        return productConfiguration.toBuilder()
                .outboundConfiguration(newOutboundConfiguration)
                .build();
    }

    private UKBankAccount makeUKBankAccount(String accountName) {
        return UKBankAccount.builder()
                .name(accountName)
                .accountNumber(new UKAccountNumber(ACCOUNT_NUMBER))
                .sortCode(SORT_CODE)
                .currencyUnit(CURRENCY)
                .build();
    }

    private OutboundConfiguration makeOutboundConfiguration(String accountName) {
        return OutboundConfiguration.builder()
                .source(makeUKBankAccount(accountName))
                .returns(makeUKBankAccount(accountName))
                .validationRules(HashSet.empty())
                .defaultPaymentMethods(List.of(PaymentMethod.Bacs))
                .payer(PartyIdentification.builder().name("cust").build())
                .build();
    }

    private ProductConfiguration makeProductConfiguration(ProductAdapter productAdapter, String accountName) {
        var outboundConfiguration = makeOutboundConfiguration(accountName);
        return new ProductConfiguration(productAdapter, outboundConfiguration);
    }

    private ProductRequest makeProductRequest(String productName, String accountName) {
        return ProductRequest.builder()
                .name(productName)
                .customerId(TEST_CUSTOMER)
                .adapter(productAdapter)
                .outboundConfiguration(makeOutboundConfiguration(accountName))
                .build();
    }

    private Product makeProduct(String productName, ProductStatus productStatus, String accountName) {
        return Product.builder()
                .name(productName)
                .createdAt(now())
                .status(productStatus)
                .productConfiguration(makeProductConfiguration(productAdapter, accountName))
                .build();
    }

    private Product makeProductWithPendingConfiguration(String productName, ProductStatus productStatus, String accountName, String pendingAccountName) {
        return makeProduct(productName, productStatus, accountName).toBuilder()
                .pendingProductConfiguration(makeProductConfiguration(productAdapter, pendingAccountName))
                .build();
    }

    private void checkExecutionCounts() {
        checkExecutionCounts(1, 1);
    }

    private void checkExecutionCounts(int stateUpdateHandlerCnt, int eventHandlerCnt) {
        verify(stateUpdateHandler, times(stateUpdateHandlerCnt)).apply(any(), any());
        verify(customerEventGenerator, times(eventHandlerCnt)).apply(any(), any());
    }

    private void checkProduct(Product actual, Product expected) {
        assertThat(actual).isEqualToIgnoringGivenFields(expected, "createdAt");
    }

    private void checkStateStore(AggregateState<Customer> state, Customer customer, long stateVersion, String errorMsg) {
        checkStateStore(state, customer, stateVersion, "IllegalStateException", errorMsg);
    }

    private void checkStateStore(AggregateState<Customer> state, Customer customer, long stateVersion, String errorType, String errorMsg) {
        assertThat(state.getCurrent()).isEqualToIgnoringGivenFields(
                customer,
                "messageId", "timestamp", "correlationId", "createdAt", "lastModifiedBy");

        assertEquals(stateVersion, state.getCurrentVersion());
        if (errorMsg == null) {
            assertNull(state.getError());
        } else {
            assertEquals(errorType, state.getError().getErrorType());
            assertEquals(errorMsg, state.getError().getErrorMessage());
        }
    }

    private void checkStateStore(AggregateState<Customer> state, Customer customer, long stateVersion) {
        checkStateStore(state, customer, stateVersion, null);
    }

    private void checkGenericErrorEvent(EventWithCauseImpl event, String errorMsg) {
        assertEquals(GenericErrorEvent.class, event.getClass());
        assertThat(event).isEqualToIgnoringGivenFields(GENERIC_ERROR_EVENT,
                "messageId", "timestamp", "correlationId", "error");
        assertEquals("IllegalStateException", ((GenericErrorEvent) event).getError().getErrorType());
        assertEquals(errorMsg, ((GenericErrorEvent) event).getError().getErrorMessage());
    }

    private void checkAccessDeniedEvent(EventWithCauseImpl event) {
        assertEquals(AccessDeniedEvent.class, event.getClass());
    }

    private Customer testCustomer(CustomerStatus customerStatus) {
        return Customer.builder()
                .name(TEST_CUSTOMER)
                .createdAt(now())
                .createdBy(LAST_CHANGE_USER)
                .lastModifiedBy(LAST_CHANGE_USER)
                .status(customerStatus)
                .build();
    }

    private Customer testCustomer(CustomerStatus customerStatus, String lastModifiedBy) {
        return Customer.builder()
                .name(TEST_CUSTOMER)
                .createdAt(now())
                .createdBy(LAST_CHANGE_USER)
                .lastModifiedBy(lastModifiedBy)
                .status(customerStatus)
                .build();
    }

    private Customer testCustomer(CustomerStatus customerStatus, Set<Product> products) {
        return testCustomer(customerStatus).toBuilder()
                .products(products)
                .build();
    }

    private Customer testCustomer(CustomerStatus customerStatus, Set<Product> products, String lastModifiedBy) {
        return testCustomer(customerStatus).toBuilder()
                .products(products)
                .lastModifiedBy(lastModifiedBy)
                .build();
    }

    private Set<Role> getRoles(String role) {
        return HashSet.of(Role.parse(role));
    }

    @Value
    private static class Result {
        private final EventWithCauseImpl event;
        private final AggregateState<Customer> state;
    }

    private <C extends Command> Result sendCommand(C command) {
        return setStateAndSendCommand(null, command, null);
    }

    private <C extends Command> Result setStateAndSendCommand(Customer currentCustomerState, C command, Long currentStateVersion) {
        try (final TopologyTestDriver topology = harness.toTopology()) {

            KeyValueStore<byte[], AggregateState<Customer>> stateStore = topology.getKeyValueStore(KafkaStreamsTestHarness.STATE_STORE);

            if (currentCustomerState != null) {
                stateStore.put(KEY.getBytes(), CustomerState.builder()
                        .current(currentCustomerState)
                        .currentVersion(currentStateVersion)
                        .message(new SerializedMessage(new byte[0], new MessageInfo()))
                        .build()
                );
            }

            KeyValueWithHeaders<String, C> commandMessage =
                    KeyValueWithHeaders.msg(KEY,
                            command,
                            new RecordHeader(MessageHeaders.CONTENT_TYPE, command.getClass().getName().getBytes()));

            harness.sendKeyValues(topology, commandMessage);

            var entities = harness.drainKeyValues(topology);
            assertEquals(1, entities.size());
            return new Result((EventWithCauseImpl) entities.get(0).value, stateStore.get(KEY.getBytes()));
        }
    }

    private <C extends Command> Result sendCommand(TopologyTestDriver topology, C command) {

        KeyValueStore<byte[], AggregateState<Customer>> stateStore = topology.getKeyValueStore(KafkaStreamsTestHarness.STATE_STORE);

        KeyValueWithHeaders<String, C> commandMessage =
                KeyValueWithHeaders.msg(KEY,
                        command,
                        new RecordHeader(MessageHeaders.CONTENT_TYPE, command.getClass().getName().getBytes()));

        harness.sendKeyValues(topology, commandMessage);

        var entities = harness.drainKeyValues(topology);
        assertEquals(1, entities.size());
        return new Result((EventWithCauseImpl) entities.get(0).value, stateStore.get(KEY.getBytes()));
    }

    private <S extends HasKey<String>, AS extends AggregateState<S>> KafkaStreamsTestHarness setupTestHarness(
            Class<S> stateClass
            , Class<AS> aggregateStateClass
            , List<Class<? extends Command>> commandClasses
            , List<Class<? extends EventWithCauseImpl>> eventClasses) {

        var mappings = List.of(GenericErrorEvent.class, CommandFailureInfo.class, AccessDeniedEvent.class, stateClass, aggregateStateClass)
                .appendAll(commandClasses)
                .appendAll(eventClasses);

        return KafkaStreamsTestHarness.builderWithMappings(
                Serdes.ByteArray()
                , new gov.scot.payments.testing.kafka.JsonSerde<>()
                , mappings.toJavaList().toArray(new Class[0]))
                .build();
    }

    private <S extends HasKey<String>, AS extends AggregateState<S>> void setupProcessor(
            Class<S> stateClass
            , Class<AS> aggregateStateClass
            , List<Class<? extends Command>> commandClasses
            , List<Class<? extends EventWithCauseImpl>> eventClasses
            , Function5<SerializedMessage, CommandFailureInfo, S, S, Long, AS> aggregateStateSupplier
            , StateUpdateFunction<AS, S> stateUpdateFunction
            , EventGeneratorFunction<AS, EventWithCauseImpl> eventGeneratorFunction
            , Function3<Command, S, Throwable, List<EventWithCauseImpl>> errorHandler
            , KafkaStreamsTestHarness testHarness) {

        @SuppressWarnings("unchecked")
        AggregateLookupService<S, AS> mockAggregateLookupService = mock(AggregateLookupService.class);

        final MicrometerMetrics metrics = new MicrometerMetrics(new SimpleMeterRegistry());
        ObjectMapper mapper = JacksonUtils.enhancedObjectMapper();
        mapper.activateDefaultTyping(
                BasicPolymorphicTypeValidator.builder()
                        .allowIfBaseType(Command.class)
                        .allowIfBaseType(Set.class)
                        .allowIfBaseType(List.class)
                        .allowIfBaseType(Map.class)
                        .allowIfBaseType(java.util.Map.class)
                        .allowIfSubType(stateClass)
                        .allowIfBaseType(CurrencyUnit.class)
                        .build()
        );
        mapper.registerModule(new VavrModule());
        JsonSerde<AS> stateSerde = new JsonSerde<>(aggregateStateClass, mapper);
        JsonSerde<Command> commandSerde = new JsonSerde<>(Command.class, mapper);
        DefaultJackson2JavaTypeMapper typeMapper = (DefaultJackson2JavaTypeMapper) ((JsonDeserializer<Command>) commandSerde.deserializer()).getTypeMapper();
        typeMapper.setClassIdFieldName(MessageHeaders.CONTENT_TYPE);
        final String mappingConfig = KafkaStreamsTestHarness.buildMappingsConfig(commandClasses.toJavaList().toArray(new Class[0]));
        commandSerde.configure(HashMap.of(JsonSerializer.TYPE_MAPPINGS, mappingConfig).toJavaMap(), false);

        Materialized<byte[], AS, KeyValueStore<Bytes, byte[]>> store = Materialized.<byte[], AS>as(Stores.inMemoryKeyValueStore(KafkaStreamsTestHarness.STATE_STORE))
                .withKeySerde(Serdes.ByteArray())
                .withValueSerde(stateSerde);

        PerCommandStatefulCommandHandlerDelegate<S, AS, EventWithCauseImpl> processor =
                PerCommandStatefulCommandHandlerDelegate
                        .<S, AS, EventWithCauseImpl>builder()
                        .aggregateQueryService(mockAggregateLookupService)
                        .metrics(metrics)
                        .aggregateStateSupplier(aggregateStateSupplier)
                        .eventGenerateFunction(eventGeneratorFunction)
                        .errorHandler(errorHandler)
                        .stateUpdateFunction(stateUpdateFunction)
                        .stateStore(store)
                        .stateHeaderEnricher(() -> new MessageHeaderEnricher("customers"))
                        .stateTopic("state")
                        .commandSerde(commandSerde)
                        .serializedMessageSerde(new JsonSerde<>(SerializedMessage.class, mapper))
                        .build();

        processor.apply(testHarness.stream()).to(KafkaStreamsTestHarness.OUTPUT_TOPIC);
    }

    private static class TestErrorHandler<E> implements Function3<Command, E, Throwable, List<EventWithCauseImpl>> {
        @Override
        public List<EventWithCauseImpl> apply(Command command, E entity, Throwable throwable) {
            return List.of(GenericErrorEvent.from(command, throwable));
        }
    }
}