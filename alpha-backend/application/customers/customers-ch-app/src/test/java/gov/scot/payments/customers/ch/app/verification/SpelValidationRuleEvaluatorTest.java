package gov.scot.payments.customers.ch.app.verification;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import java.math.BigDecimal;
import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

class SpelValidationRuleEvaluatorTest {

    private SpelValidationRuleEvaluator evaluator;

    @BeforeEach
    public void setUp(){
        evaluator = new SpelValidationRuleEvaluator(new SpelExpressionParser());
    }

    @Test
    void invalidEventGeneratedTestForSingleRule() {
        var money = Money.of(new BigDecimal("998.23"), "GBP");
        Payment instruction = makePayment(money);
        ValidationRuleEvaluationResult validation = evaluator.evaluate("amount.getNumber() < 1000.0",instruction);
        Assertions.assertFalse(validation.isSuccess());
    }

    @Test
    void whenInvalidRuleThenThrowsExceptionTest() {

        var money = Money.of(new BigDecimal("1048.23"), "GBP");
        Payment instruction = makePayment(money);
        ValidationRuleEvaluationResult validation = evaluator.evaluate("thisWillFailPArsing < 1000.0",instruction);
        Assertions.assertFalse(validation.isSuccess());
        assertThat(validation.getMessage()).startsWith("An unexpected error occurred validating rule 'thisWillFailPArsing < 1000.0'");
    }

    @Test
    void validEventGeneratedTestForSingleRule() {

        var money = Money.of(new BigDecimal("988.23"), "GBP");
        Payment instruction = makePayment(money);
        var lessThanValue = Money.of(new BigDecimal("1000.00"), "GBP");
        ValidationRuleEvaluationResult validation = evaluator.evaluate(greaterThanMoneyRule("amount", lessThanValue),instruction);
        Assertions.assertTrue(validation.isSuccess());
    }

    private String greaterThanMoneyRule(String fieldName, Money amount){
        return fieldName + ".isGreaterThan(T(org.javamoney.moneta.Money).of("+amount.getNumberStripped().toPlainString()+",'"+amount.getCurrency()+"'))";
    }

    private Payment makePayment(Money amount) {
        return Payment.builder()
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.Bacs))
                .creditor(PartyIdentification.builder().name("").build())
                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .debtor(PartyIdentification.builder().name("").build())
                .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .amount(amount)
                .latestExecutionDate(Instant.now())
                .product(new CompositeReference("",""))
                .build();
    }

}