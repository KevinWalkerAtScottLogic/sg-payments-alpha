package gov.scot.payments.customers.ch.app.verification;

import gov.scot.payments.payments.model.aggregate.Payment;

public interface ValidationRuleEvaluator {
    ValidationRuleEvaluationResult evaluate(String rule, Payment payment);

    String getName();

}
