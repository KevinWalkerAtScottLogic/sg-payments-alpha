package gov.scot.payments.customers.ch.app;

import gov.scot.payments.customers.model.api.CustomerPaymentBatch;
import gov.scot.payments.model.CompositeReference;
import io.vavr.collection.List;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

@AllArgsConstructor
public class CustomerWebClientAdapter {

    private final WebClient webClient;

    List<CustomerPaymentBatch> sendFileToAdapter(InputStream fileStream, String url, CompositeReference product, String fileName) throws IOException {

        var bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder
                .part("file", fileStream.readAllBytes())
                .header("Content-Disposition", "form-data; name=file; filename=" + fileName);

        var uriFromUrl = URI.create(url);
        UriComponents uri = UriComponentsBuilder.fromUri(uriFromUrl)
                .queryParam("product", product.toString())
                .queryParam("fileName", fileName)
                .build();

        return List.ofAll(webClient
                .post()
                .uri(uri.toUri())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .retrieve()
                .bodyToFlux(CustomerPaymentBatch.class)
                .collectList()
                .block());
    }
}
