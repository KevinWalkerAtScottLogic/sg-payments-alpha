package gov.scot.payments.customers.ch.app;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.application.component.commandhandler.stateful.AggregateState;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.customers.model.aggregate.Product;
import gov.scot.payments.model.CommandFailureInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;
import gov.scot.payments.customers.model.aggregate.Customer;

@SuperBuilder
@NoArgsConstructor
@ToString(callSuper = true)
public class CustomerState extends AggregateState<Customer> {

    @Getter @Nullable private Customer previous;
    @Getter @Nullable private Customer current;

    @JsonCreator
    public CustomerState(@JsonProperty("message") SerializedMessage message
            , @JsonProperty("error") CommandFailureInfo error
            , @JsonProperty("previous") Customer previous
            , @JsonProperty("current") Customer current
            , @JsonProperty("currentVersion") Long currentVersion) {
        super(message, error, currentVersion);
        this.previous = previous;
        this.current = current;
    }
}
