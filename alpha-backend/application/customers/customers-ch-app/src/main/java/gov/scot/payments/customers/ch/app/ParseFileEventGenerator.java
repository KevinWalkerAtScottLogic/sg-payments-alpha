package gov.scot.payments.customers.ch.app;

import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.customers.model.aggregate.OutboundConfiguration;
import gov.scot.payments.customers.model.aggregate.PaymentMethodConfiguration;
import gov.scot.payments.customers.model.aggregate.Product;
import gov.scot.payments.customers.model.api.CustomerPaymentBatch;
import gov.scot.payments.customers.model.command.ParseFileCommand;
import gov.scot.payments.customers.model.event.CustomerPaymentCreatedEvent;
import gov.scot.payments.customers.model.event.PaymentFileParsingEndEvent;
import gov.scot.payments.customers.model.event.PaymentFileParsingFailedEvent;
import gov.scot.payments.customers.model.event.PaymentFileParsingStartEvent;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.payments.model.aggregate.MetadataField;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.payments.model.api.CreatePaymentRequest;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.core.sync.ResponseTransformer;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@AllArgsConstructor
@Slf4j
public class ParseFileEventGenerator {

    private final CustomerWebClientAdapter customerWebClientAdapter;
    private final S3Client s3Client;


    public List<EventWithCauseImpl> createEventsFromFile(ParseFileCommand command, Customer customer) {
        Product product = customer.getProduct(command.getProduct().getComponent1())
                .getOrElseThrow(() -> new IllegalStateException(
                        String.format("Command \"ParseFileCommand\" cannot be carried out as Product: %s does not exist", command.getProduct().getComponent1())
                ));

        String[] pathElements = command.getFile().toString().split("/");
        String fileName = pathElements[pathElements.length - 1];

        if(product.getProductConfiguration().getOutboundConfiguration() == null
                || product.getProductConfiguration().getAdapter() == null){
            log.info("Skipping parsing {} for product {}:{}. No outbound or adapter configuration present",fileName,customer.getName(),product.getName());
            return List.empty();
        }
        OutboundConfiguration outboundConfiguration = product.getProductConfiguration().getOutboundConfiguration();
        String adapterUrl = product.getProductConfiguration().getAdapter().getLocation();

        try (InputStream is = getFileFromUri(command.getFile())) {
            List<CustomerPaymentBatch> batches = customerWebClientAdapter.sendFileToAdapter(is, adapterUrl, command.getProduct(), fileName);

            if (batches.map(CustomerPaymentBatch::getId).toSet().size() != batches.size()) {
                String errorMsg = String.format("Unable to parse %s as customer payment batch ids were not unique", fileName);
                return List.of(new PaymentFileParsingFailedEvent(
                        errorMsg, command.getFile(), command.getProduct().getComponent1(), customer, command.getUser(), command.getStateVersion())
                );
            }

            List<EventWithCauseImpl> paymentEvents = batches
                    .map(batch -> Tuple.of(batch.getId(), batch.getPayments().map(batch2 -> paymentRequestToPayment(batch2, command,outboundConfiguration))))
                    .flatMap(payments -> convertPaymentsToEventList(payments._1, payments._2, customer, command));

            log.info("Successfully parsed file: {} in to {} events", command.getFile(), paymentEvents.length());
            return paymentEvents;

        }
        catch (WebClientResponseException e){
            return List.of(new PaymentFileParsingFailedEvent(
                    e.getResponseBodyAsString(), command.getFile(), command.getProduct().getComponent1(), customer, command.getUser(), command.getStateVersion())
            );
        }
        catch (Throwable e) {
            log.warn("Error parsing file: {}", command.getFile(), e);
            final String message = e.getMessage() == null ? e.getClass().getSimpleName() : e.getMessage();
            return List.of(new PaymentFileParsingFailedEvent(
                    message, command.getFile(), command.getProduct().getComponent1(), customer, command.getUser(), command.getStateVersion())
            );
        }
    }

    List<EventWithCauseImpl> convertPaymentsToEventList(String batchId, List<Payment> payments, Customer customer, ParseFileCommand command) {

        List<EventWithCauseImpl> paymentEvents = payments
                .map(payment -> new CustomerPaymentCreatedEvent(
                        command.getFile(), batchId, payment, command.getProduct().getComponent1(), customer, command.getUser(), command.getStateVersion()
                ));

        EventWithCauseImpl parsingEventStart = PaymentFileParsingStartEvent.builder()
                .uri(command.getFile())
                .customerPaymentBatchId(batchId)
                .productId(command.getProduct().getComponent1())
                .carriedState(customer)
                .user(command.getUser())
                .stateVersion(command.getStateVersion())
                .paymentIds(paymentEvents.map(event -> (CustomerPaymentCreatedEvent) event).map(event -> event.getPayment().getId()))
                .build();

        EventWithCauseImpl parsingEventEnd = PaymentFileParsingEndEvent.builder()
                .uri(command.getFile())
                .customerPaymentBatchId(batchId)
                .productId(command.getProduct().getComponent1())
                .carriedState(customer)
                .user(command.getUser())
                .stateVersion(command.getStateVersion())
                .build();

        return paymentEvents.prepend(parsingEventStart).append(parsingEventEnd);
    }

    private Payment paymentRequestToPayment(CreatePaymentRequest request, ParseFileCommand command, OutboundConfiguration outboundConfiguration) {
        List<PaymentMethod> allowedMethods = request.getAllowedMethods() == null ? outboundConfiguration.getDefaultPaymentMethods(): request.getAllowedMethods();
        List<MetadataField> metadata = mergeMetadata(outboundConfiguration.getMetadata(),request.getMetadata());
        Map<String, Map<String, String>> methodProperties = outboundConfiguration.getPaymentMethodConfiguration()
                .entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, v -> v.getValue().getProperties()));

        return Payment.builder()
                .batchId(request.getBatchId())
                .createdBy(command.getUser())
                .createdAt(command.getCreatedAt())
                .allowedMethods(allowedMethods)
                .amount(request.getAmount())
                .earliestExecutionDate(request.getEarliestExecutionDate())
                .latestExecutionDate(request.getLatestExecutionDate())
                .product(request.getProduct())
                .creditorAccount(request.getCreditorAccount())
                .creditor(request.getCreditor())
                .debtorAccount(outboundConfiguration.getSource())
                .debtor(outboundConfiguration.getPayer())
                .creditorMetadata(request.getCreditorMetadata())
                .metadata(metadata)
                .methodProperties(methodProperties)
                .build();
    }

    private List<MetadataField> mergeMetadata(List<MetadataField> defaults, List<MetadataField> provided) {
        Map<String,MetadataField> defaultMap = defaults.toJavaMap(mf ->new Tuple2<>(mf.getName(),mf));
        Map<String,MetadataField> providedMap = provided.toJavaMap(mf ->new Tuple2<>(mf.getName(),mf));
        defaultMap.putAll(providedMap);
        return List.ofAll(defaultMap.values());
    }

    InputStream getFileFromUri(URI uri) {
        String bucket = uri.getHost();
        String key = URLDecoder.decode(uri.getPath().substring(1), StandardCharsets.UTF_8);
        String versionId = uri.getFragment();

        var getObjectRequestBuilder = GetObjectRequest.builder();
        getObjectRequestBuilder.bucket(bucket);
        getObjectRequestBuilder.key(key);

        if (versionId != null && !versionId.equals("null")) {
            getObjectRequestBuilder.versionId(versionId);
        }

        return s3Client.getObject(getObjectRequestBuilder.build(), ResponseTransformer.toInputStream());
    }

}
