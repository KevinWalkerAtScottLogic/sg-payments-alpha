package gov.scot.payments.customers.ch.app.verification;

import lombok.Value;

@Value
public class ValidationRuleEvaluationResult {

    private boolean success;
    private String message;

    public static ValidationRuleEvaluationResult success(){
        return new ValidationRuleEvaluationResult(true,null);
    }

    public static ValidationRuleEvaluationResult failure(String message){
        return new ValidationRuleEvaluationResult(false,message);
    }
}
