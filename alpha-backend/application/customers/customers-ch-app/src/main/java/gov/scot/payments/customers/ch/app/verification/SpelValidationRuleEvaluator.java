package gov.scot.payments.customers.ch.app.verification;

import gov.scot.payments.payments.model.aggregate.Payment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.expression.ExpressionParser;

@RequiredArgsConstructor
@Slf4j
public class SpelValidationRuleEvaluator implements ValidationRuleEvaluator {

    private final ExpressionParser expressionParser;

    @Override
    public ValidationRuleEvaluationResult evaluate(String rule, Payment payment) {
        ValidationRuleEvaluationResult validation;
        try {
            var expression = expressionParser.parseExpression(rule);
            if (!expression.getValue(payment, Boolean.class)) {
                validation = ValidationRuleEvaluationResult.success();
            } else{
                validation = ValidationRuleEvaluationResult.failure("");
            }
        } catch (Exception e){
            validation = ValidationRuleEvaluationResult.failure(String.format("An unexpected error occurred validating rule '%s', Details: %s",rule,e.getMessage()));
        }
        return validation;
    }

    @Override
    public String getName() {
        return "spel";
    }
}
