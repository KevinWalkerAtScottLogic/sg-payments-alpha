package gov.scot.payments.customers.ch.app;

import gov.scot.payments.application.component.commandhandler.stateful.StateUpdateFunction;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.customers.model.aggregate.*;
import gov.scot.payments.customers.model.command.*;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.HasUser;
import io.vavr.collection.HashSet;
import io.vavr.control.Option;
import lombok.extern.slf4j.Slf4j;

import java.util.EnumSet;
import java.util.function.Function;

import static gov.scot.payments.application.component.commandhandler.stateful.AclFilteringStatefulCommandHandlerFunction.aclAndLock;
import static gov.scot.payments.application.component.commandhandler.stateful.AclFilteringStatefulCommandHandlerFunction.lock;
import static java.time.Instant.now;

@Slf4j
public class CustomerStateUpdateFunc extends StateUpdateFunction.PatternMatching<CustomerState, Customer> {
    @Override
    protected void handlers(PatternMatcher.Builder2<Command, CustomerState, Customer> builder) {

        builder
                .match2(CreateCustomerCommand.class, (c, s) -> createCustomerRequested(c, s.getCurrent()))
                .match2(ApproveCreateCustomerCommand.class, aclAndLock(this::requireDifferentUserForApprovals, this::approveCreateCustomer))
                .match2(RejectCreateCustomerCommand.class, aclAndLock(this::requireDifferentUserForApprovals, this::rejectCreateCustomer))
                .match2(DeleteCustomerCommand.class, lock(this::deleteCustomerRequested))
                .match2(ApproveDeleteCustomerCommand.class, aclAndLock(this::requireDifferentUserForApprovals, this::approveDeleteCustomer))
                .match2(RejectDeleteCustomerCommand.class, aclAndLock(this::requireDifferentUserForApprovals, this::rejectDeleteCustomer))
                .match2(FinalizeDeleteCustomerCommand.class, lock(this::finalizeDeleteCustomer))
                .match2(CreateProductCommand.class, lock(this::createProductRequested))
                .match2(ApproveCreateProductCommand.class, aclAndLock(this::requireDifferentUserForApprovals, this::approveCreateProduct))
                .match2(RejectCreateProductCommand.class, aclAndLock(this::requireDifferentUserForApprovals, this::rejectCreateProduct))
                .match2(FinalizeCreateProductCommand.class, (c, s) -> finalizeCreateProduct(c, s.getCurrent()))
                .match2(ModifyProductCommand.class, lock(this::modifyProductRequested))
                .match2(ApproveModifyProductCommand.class, aclAndLock(this::requireDifferentUserForApprovals, this::approveModifyProduct))
                .match2(RejectModifyProductCommand.class, aclAndLock(this::requireDifferentUserForApprovals, this::rejectModifyProduct))
                .match2(DeleteProductCommand.class, lock(this::deleteProductRequested))
                .match2(ApproveDeleteProductCommand.class, aclAndLock(this::requireDifferentUserForApprovalsExceptForceDelete, this::approveDeleteProduct))
                .match2(RejectDeleteProductCommand.class, aclAndLock(this::requireDifferentUserForApprovals, this::rejectDeleteProduct))
                .match2(ParseFileCommand.class, (c, s) -> parseFile(c, s.getCurrent()))
                .match2(FinalizeDeleteProductCommand.class, lock(this::finalizeDeleteProduct))
                .match2(VerifyPaymentCommand.class, this::verifyPayment)
        ;
    }

    private Customer verifyPayment(VerifyPaymentCommand command, CustomerState state) {
        if(state.getCurrent() == null){
            throw new IllegalStateException(String.format("Customer %s does not exist", command.getKey()));
        }
        if(!EnumSet.of(CustomerStatus.Live,CustomerStatus.DeleteRequested,CustomerStatus.ModifyRequested).contains(state.getCurrent().getStatus()) ){
            throw new IllegalStateException(String.format("Customer %s is not in Live, Delete requested or Modify Requested state", command.getKey()));
        }
        Option<Product> product = state.getCurrent().getProduct(command.getPayment().getProduct().getComponent1());
        if(product.isEmpty()){
            throw new IllegalStateException(String.format("Product %s does not exist", command.getPayment().getProduct()));
        }
        if(!EnumSet.of(ProductStatus.Live,ProductStatus.DeleteRequested,ProductStatus.ModifyRequested).contains(product.get().getStatus())){
            throw new IllegalStateException(String.format("Product %s is not in Live, Delete requested or Modify Requested state", command.getPayment().getProduct()));
        }
        if(product.get().getProductConfiguration().getOutboundConfiguration() == null){
            throw new IllegalStateException(String.format("Product %s is not configured for outbound payments", command.getPayment().getProduct()));
        }
        return state.getCurrent();
    }

    private Customer parseFile(ParseFileCommand command, Customer customer) {
        if (customer == null) {
            throw new IllegalStateException(String.format("Command \"ParseFileCommand\" cannot be carried out as Customer: %s does not exist", command.getKey()));
        }

        if (customer.getStatus() != CustomerStatus.Live) {
            throw new IllegalStateException(String.format("Command \"ParseFileCommand\" cannot be carried out as Customer: %s is not live", customer.getKey()));

        }

        Product product = customer.getProduct(command.getProduct().getComponent1())
                .getOrElseThrow(() -> new IllegalStateException(
                        String.format("Command \"ParseFileCommand\" cannot be carried out as Product: %s does not exist", command.getProduct().getComponent1())
                ));

        if (product.getStatus() != ProductStatus.Live) {
            throw new IllegalStateException(String.format("Command \"ParseFileCommand\" cannot be carried out as Product: %s is not live", command.getProduct().getComponent1()));
        }

        return customer;
    }

    private boolean requireDifferentUserForApprovals(HasUser command, Customer customer) {
        // Approvals and rejections must be by a different user than the one who created the request
        return !command.getUser().equals(customer.getLastModifiedBy());
    }

    private boolean requireDifferentUserForApprovalsExceptForceDelete(ApproveDeleteProductCommand command, Customer customer) {
        // Approvals and rejections must be by a different user than the one who created the request
        // except for the case of the Process Manager forcing the deletion of the product on customer deletion.
        if (command.isForceDelete()) {
            return true;
        }
        return !command.getUser().equals(customer.getLastModifiedBy());
    }

    private Customer createCustomerRequested(CreateCustomerCommand command, Customer customer) {

        // Expecting state to exist, but current to be null, or status to be Rejected
        if (customer != null && customer.getStatus() != CustomerStatus.Rejected) {
            throw new IllegalStateException(String.format("Command \"CreateCustomerCommand\" cannot be carried out as customer %s already exists", command.getCustomerId()));
        }

        return Customer.builder()
                .name(command.getCustomerId())
                .createdAt(now())
                .createdBy(command.getUser())
                .lastModifiedBy(command.getUser())
                .status(CustomerStatus.New)
                .build();
    }

    private Customer approveCreateCustomer(ApproveCreateCustomerCommand command, Customer customer) {
        return processCustomerCommand(command, customer, CustomerStatus.New, CustomerStatus.Live)
                .toBuilder()
                .approval(CustomerApproval.builder()
                        .approved(true)
                        .at(now())
                        .user(command.getUser())
                        .build())
                .build();
    }

    private Customer rejectCreateCustomer(RejectCreateCustomerCommand command, Customer customer) {
        return processCustomerCommand(command, customer, CustomerStatus.New, CustomerStatus.Rejected)
                .toBuilder()
                .approval(CustomerApproval.builder()
                        .approved(false)
                        .at(now())
                        .user(command.getUser())
                        .build())
                .build();
    }

    private Customer deleteCustomerRequested(DeleteCustomerCommand command, Customer customer) {
        return processCustomerCommand(command, customer, CustomerStatus.Live, CustomerStatus.DeleteRequested);
    }

    private Customer approveDeleteCustomer(ApproveDeleteCustomerCommand command, Customer customer) {
        // The emitted DeleteCustomerApprovedEvent will initiate product deletion in the ProcessManager
        return processCustomerCommand(command, customer, CustomerStatus.DeleteRequested, CustomerStatus.Deleting)
                .toBuilder()
                .deleteApproval(CustomerApproval.builder()
                        .approved(true)
                        .at(now())
                        .user(command.getUser())
                        .build())
                .build();
    }

    private Customer rejectDeleteCustomer(RejectDeleteCustomerCommand command, Customer customer) {
        return processCustomerCommand(command, customer, CustomerStatus.DeleteRequested, CustomerStatus.Live)
                .toBuilder()
                .deleteApproval(CustomerApproval.builder()
                        .approved(false)
                        .at(now())
                        .user(command.getUser())
                        .build())
                .build();
    }

    private Customer finalizeDeleteCustomer(FinalizeDeleteCustomerCommand command, Customer customer) {
        return processCustomerCommand(command, customer, CustomerStatus.Deleting, CustomerStatus.Deleted);
    }

    private Customer createProductRequested(CreateProductCommand command, Customer customer) {

        checkCustomerState(customer, CustomerStatus.Live, command.getClass().getSimpleName(), command.getProductId().getComponent0());

        var productRequest = command.getProductRequest();

        // Try to get existing product with the given name
        var existingProduct = customer.getProduct(productRequest.getName());

        // Check that product does not already exist or is in Rejected state
        if (existingProduct.isDefined() && existingProduct.get().getStatus() != ProductStatus.Rejected) {
            throw new IllegalStateException(String.format("Command \"CreateProductCommand\" cannot be carried out as product %s already exists",
                    productRequest.getName()));
        }

        var product = Product.builder()
                .name(productRequest.getName())
                .createdAt(now())
                .status(ProductStatus.New)
                .productConfiguration(ProductConfiguration.builder()
                        .adapter(productRequest.getAdapter())
                        .outboundConfiguration(productRequest.getOutboundConfiguration())
                        .build())
                .build();

        var currentProducts = customer.getProducts();
        var updatedProducts = currentProducts == null ? HashSet.of(product) : currentProducts.remove(product).add(product);

        return customer.toBuilder()
                .products(updatedProducts)
                .status(CustomerStatus.ModifyRequested)
                .lastModifiedBy(command.getUser())
                .build();
    }

    private Customer approveCreateProduct(ApproveCreateProductCommand command, Customer customer) {
        return processProductCommand(command, customer, CustomerStatus.ModifyRequested, CustomerStatus.ModifyRequested, ProductStatus.New, ProductStatus.Creating,
                product -> product.toBuilder().approval(
                        ProductApproval.builder()
                                .approved(true)
                                .at(now())
                                .user(command.getUser())
                                .build()
                ).build()
        );
    }

    private Customer rejectCreateProduct(RejectCreateProductCommand command, Customer customer) {
        return processProductCommand(command, customer, CustomerStatus.ModifyRequested, CustomerStatus.Live, ProductStatus.New, ProductStatus.Rejected,
                product -> product.toBuilder().approval(
                        ProductApproval.builder()
                                .approved(false)
                                .at(now())
                                .user(command.getUser())
                                .message(command.getReason())
                                .build()
                ).build()
        );
    }

    private Customer finalizeCreateProduct(FinalizeCreateProductCommand command, Customer customer) {
        return processProductCommand(command, customer, CustomerStatus.ModifyRequested, CustomerStatus.Live, ProductStatus.Creating, ProductStatus.Live);
    }

    private Customer modifyProductRequested(ModifyProductCommand command, Customer customer) {
        return processProductCommand(command, customer, CustomerStatus.Live, CustomerStatus.ModifyRequested, ProductStatus.Live, ProductStatus.ModifyRequested,
                product -> product.toBuilder()
                        .pendingProductConfiguration(
                                ProductConfiguration.builder()
                                        .adapter(command.getProductRequest().getAdapter())
                                        .outboundConfiguration(command.getProductRequest().getOutboundConfiguration())
                                        .build()
                        )
                        .build()
        )
                .toBuilder()
                .lastModifiedBy(command.getUser())
                .build();
    }

    private Customer approveModifyProduct(ApproveModifyProductCommand command, Customer customer) {
        return processProductCommand(command, customer, CustomerStatus.ModifyRequested, CustomerStatus.Live, ProductStatus.ModifyRequested, ProductStatus.Live,
                product -> product.toBuilder()
                        .productConfiguration(product.getPendingProductConfiguration())
                        .pendingProductConfiguration(null)
                        .build()
        );
    }

    private Customer rejectModifyProduct(RejectModifyProductCommand command, Customer customer) {
        return processProductCommand(command, customer, CustomerStatus.ModifyRequested, CustomerStatus.Live, ProductStatus.ModifyRequested, ProductStatus.Live,
                product -> product.toBuilder()
                        .pendingProductConfiguration(null)
                        .approval(
                                ProductApproval.builder()
                                        .approved(false)
                                        .at(now())
                                        .user(command.getUser())
                                        .message(command.getReason())
                                        .build())
                        .build()
        );
    }

    private Customer deleteProductRequested(DeleteProductCommand command, Customer customer) {
        return processProductCommand(command, customer, CustomerStatus.Live, CustomerStatus.ModifyRequested, ProductStatus.Live, ProductStatus.DeleteRequested)
                .toBuilder()
                .lastModifiedBy(command.getUser())
                .build();
    }

    private Customer approveDeleteProduct(ApproveDeleteProductCommand command, Customer customer) {
        if (!command.isForceDelete()) {
            return processProductCommand(command, customer, CustomerStatus.ModifyRequested, CustomerStatus.ModifyRequested, ProductStatus.DeleteRequested, ProductStatus.Deleting,
                    product -> product.toBuilder().deleteApproval(
                            ProductApproval.builder()
                                    .approved(true)
                                    .at(now())
                                    .user(command.getUser())
                                    .build()
                    ).build()
            );
        }

        // Force delete is set, so allow a different combination of statuses.

        // Get product if it exists
        Product product = getProduct(command, customer);

        var customerStatus = customer.getStatus();
        var productStatus = product.getStatus();

        if (!((customerStatus == CustomerStatus.ModifyRequested || customerStatus == CustomerStatus.Deleting)
                && (productStatus == ProductStatus.Live || productStatus == ProductStatus.DeleteRequested))) {
            throw new IllegalStateException(
                    String.format("Command \"ApproveDeleteProduct\" cannot be carried out on customer %s and product %s due to invalid" +
                                    " customer and product statuses. Customer status = %s, product status = %s",
                            customer.getName(), product.getName(), customerStatus, productStatus));
        }

        var updatedProduct = product.toBuilder()
                .deleteApproval(
                        ProductApproval.builder()
                                .approved(true)
                                .at(now())
                                .user(command.getUser())
                                .build()
                )
                .status(ProductStatus.Deleting)
                .build();

        var updatedProducts = customer.getProducts().replace(product, updatedProduct);

        return customer.toBuilder()
                .products(updatedProducts)
                .build();
    }

    private Customer rejectDeleteProduct(RejectDeleteProductCommand command, Customer customer) {
        return processProductCommand(command, customer, CustomerStatus.ModifyRequested, CustomerStatus.Live, ProductStatus.DeleteRequested, ProductStatus.Live,
                product -> product.toBuilder().deleteApproval(
                        ProductApproval.builder()
                                .approved(false)
                                .at(now())
                                .user(command.getUser())
                                .build()
                ).build()
        );
    }

    private Customer finalizeDeleteProduct(FinalizeDeleteProductCommand command, Customer customer) {
        return processProductCommand(command, customer, CustomerStatus.ModifyRequested, CustomerStatus.Live, ProductStatus.Deleting, ProductStatus.Deleted);
    }

    private <C extends HasCustomer> Customer processCustomerCommand(C command, Customer customer, CustomerStatus requiredCurrentStatus, CustomerStatus newStatus) {

        // Check customer exists
        if (customer == null) {
            throw new IllegalStateException(
                    String.format("Command \"%s\" cannot be carried out as customer %s does not exist",
                            command.getClass().getSimpleName(), command.getCustomerId()));
        }

        // Check customer is in valid state for command
        checkCustomerState(customer, requiredCurrentStatus, command.getClass().getSimpleName(), command.getCustomerId());

        // Update status
        return customer.toBuilder().status(newStatus).build();
    }

    private <C extends HasProduct> Customer processProductCommand(C command, Customer customer,
                                                                  CustomerStatus requiredCustomerStatus, CustomerStatus newCustomerStatus,
                                                                  ProductStatus requiredProductStatus, ProductStatus newProductStatus) {
        return processProductCommand(command, customer, requiredCustomerStatus, newCustomerStatus, requiredProductStatus, newProductStatus, product -> product);
    }

    private <C extends HasProduct> Customer processProductCommand(C command, Customer customer,
                                                                  CustomerStatus requiredCustomerStatus, CustomerStatus newCustomerStatus,
                                                                  ProductStatus requiredProductStatus, ProductStatus newProductStatus,
                                                                  Function<Product, Product> updateProductFunction) {

        // Check customer is in valid state for command
        checkCustomerState(customer, requiredCustomerStatus, command.getClass().getSimpleName(), command.getProductId().getComponent0());
        Product product = getProduct(command, customer);

        // Check product is in valid state for command
        var currentProductStatus = product.getStatus();
        if (product.getStatus() != requiredProductStatus) {
            throw new IllegalStateException(
                    String.format("Command \"%s\" cannot be carried out as product %s has status %s instead of %s",
                            command.getClass().getSimpleName(), product.getName(), currentProductStatus, requiredProductStatus));
        }

        var updatedProduct = updateProductFunction.apply(product)
                .toBuilder()
                .status(newProductStatus)
                .build();

        var updatedProducts = customer.getProducts().replace(product, updatedProduct);

        return customer.toBuilder()
                .status(newCustomerStatus)
                .products(updatedProducts)
                .build();
    }

    private <C extends HasProduct> Product getProduct(C command, Customer customer) {
        // Get product if it exists
        return customer.getProduct(command.getProductId().getComponent1())
                .getOrElseThrow(() -> new IllegalStateException(
                        String.format("Command \"%s\" cannot be carried out as product %s does not exist",
                                command.getClass().getSimpleName(), command.getProductId().getComponent1()))
                );
    }

    private void checkCustomerState(Customer customer, CustomerStatus requiredStatus, String commandName, String customerId) {
        if (customer == null) {
            throw new IllegalStateException(
                    String.format("Command \"%s\" cannot be carried out as customer %s does not exist",
                            commandName, customerId));
        }
        var currentCustomerStatus = customer.getStatus();
        if (currentCustomerStatus != requiredStatus) {
            throw new IllegalStateException(
                    String.format("Command \"%s\" cannot be carried out as customer %s has status %s instead of %s",
                            commandName, customerId, currentCustomerStatus, requiredStatus));
        }
    }
}
