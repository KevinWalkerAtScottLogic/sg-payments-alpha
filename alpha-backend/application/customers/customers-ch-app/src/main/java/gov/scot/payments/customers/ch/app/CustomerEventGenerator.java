package gov.scot.payments.customers.ch.app;

import gov.scot.payments.application.component.commandhandler.stateful.EventGeneratorFunction;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.customers.ch.app.verification.StatelessPaymentVerifier;
import gov.scot.payments.customers.model.aggregate.Validation;
import gov.scot.payments.customers.model.command.*;
import gov.scot.payments.customers.model.event.*;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.GenericErrorEvent;
import io.vavr.collection.List;
import io.vavr.Predicates;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CustomerEventGenerator extends EventGeneratorFunction.PatternMatching<CustomerState, EventWithCauseImpl> {

    private final ParseFileEventGenerator parseFileEventGenerator;
    private final StatelessPaymentVerifier verifier;

    @Override
    protected void handlers(final PatternMatcher.Builder2<Command, CustomerState, List<EventWithCauseImpl>> builder) {

        builder
                //check for verification error first
                .match2(Predicates.instanceOf(VerifyPaymentCommand.class),
                        customerState -> customerState.getError() != null,
                        (command, customerState) -> List.of(CustomerPaymentRejectedEvent.builder()
                                .payment(command.getPayment().getId())
                                .reason(customerState.getError().getErrorMessage())
                                .product(command.getPayment().getProduct())
                                .build()))
                // Check for error before trying any other matches
                .match2(command -> true,
                        customerState -> customerState.getError() != null,
                        (command, customerState) -> List.of(GenericErrorEvent.from(command, customerState.getError())))
                .match2(CreateCustomerCommand.class,
                        (c, s) -> List.of(new CreateCustomerRequestedEvent(s.getCurrent(), c.getUser(), s.getCurrentVersion())))
                .match2(ApproveCreateCustomerCommand.class,
                        (c, s) -> List.of(new CreateCustomerApprovedEvent(s.getCurrent(), c.getUser(), s.getCurrentVersion())))
                .match2(RejectCreateCustomerCommand.class,
                        (c, s) -> List.of(new CreateCustomerRejectedEvent(s.getCurrent(), c.getReason(), c.getUser(), s.getCurrentVersion())))
                .match2(DeleteCustomerCommand.class,
                        (c, s) -> List.of(new DeleteCustomerRequestedEvent(s.getCurrent(), c.getUser(), s.getCurrentVersion())))
                .match2(ApproveDeleteCustomerCommand.class,
                        (c, s) -> List.of(new DeleteCustomerApprovedEvent(s.getCurrent(), c.getUser(), s.getCurrentVersion())))
                .match2(RejectDeleteCustomerCommand.class,
                        (c, s) -> List.of(new DeleteCustomerRejectedEvent(s.getCurrent(), c.getReason(), c.getUser(), s.getCurrentVersion())))
                .match2(FinalizeDeleteCustomerCommand.class,
                        (c, s) -> List.of(new CustomerDeletedEvent(s.getCurrent(), s.getCurrent().getLastModifiedBy(), s.getCurrentVersion())))
                .match2(CreateProductCommand.class,
                        (c, s) -> List.of(
                                new CreateProductRequestedEvent(
                                        c.getProductId().getComponent1(),
                                        s.getCurrent(),
                                        c.getUser(),
                                        s.getCurrentVersion()))
                )
                .match2(ApproveCreateProductCommand.class,
                        (c, s) -> List.of(
                                new CreateProductApprovedEvent(
                                        c.getProductId().getComponent1(),
                                        s.getCurrent(),
                                        c.getUser(),
                                        s.getCurrentVersion()))
                )
                .match2(RejectCreateProductCommand.class,
                        (c, s) -> List.of(
                                new CreateProductRejectedEvent(
                                        c.getProductId().getComponent1(),
                                        c.getReason(),
                                        s.getCurrent(),
                                        c.getUser(),
                                        s.getCurrentVersion()))
                )
                .match2(FinalizeCreateProductCommand.class,
                        (c, s) -> List.of(
                                new ProductCreatedEvent(
                                        c.getProductId().getComponent1(),
                                        s.getCurrent(),
                                        s.getCurrent().getCreatedBy(),
                                        s.getCurrentVersion()))
                )
                .match2(ModifyProductCommand.class,
                        (c, s) -> List.of(
                                new ModifyProductRequestedEvent(
                                        c.getProductId().getComponent1(),
                                        s.getCurrent(),
                                        c.getUser(),
                                        s.getCurrentVersion()))
                )
                .match2(ApproveModifyProductCommand.class,
                        (c, s) -> List.of(
                                new ModifyProductApprovedEvent(
                                        c.getProductId().getComponent1(),
                                        s.getCurrent(),
                                        c.getUser(),
                                        s.getCurrentVersion()))
                )
                .match2(RejectModifyProductCommand.class,
                        (c, s) -> List.of(
                                new ModifyProductRejectedEvent(
                                        c.getProductId().getComponent1(),
                                        c.getReason(),
                                        s.getCurrent(),
                                        c.getUser(),
                                        s.getCurrentVersion()))
                )
                .match2(DeleteProductCommand.class,
                        (c, s) -> List.of(
                                new DeleteProductRequestedEvent(
                                        c.getProductId().getComponent1(),
                                        s.getCurrent(),
                                        c.getUser(),
                                        s.getCurrentVersion()))
                )
                .match2(ApproveDeleteProductCommand.class,
                        (c, s) -> List.of(
                                new DeleteProductApprovedEvent(
                                        c.getProductId().getComponent1(),
                                        s.getCurrent(),
                                        c.getUser(),
                                        s.getCurrentVersion()))
                )
                .match2(RejectDeleteProductCommand.class,
                        (c, s) -> List.of(
                                new DeleteProductRejectedEvent(
                                        c.getProductId().getComponent1(),
                                        c.getReason(),
                                        s.getCurrent(),
                                        c.getUser(),
                                        s.getCurrentVersion()))
                )
                .match2(ParseFileCommand.class, (c, s) -> parseFileEventGenerator.createEventsFromFile(c, s.getCurrent()))
                .match2(FinalizeDeleteProductCommand.class,
                        (c, s) -> List.of(
                                new ProductDeletedEvent(
                                        c.getProductId().getComponent1(),
                                        s.getCurrent(),
                                        s.getCurrent().getCreatedBy(),
                                        s.getCurrentVersion()))
                )
                .match2(VerifyPaymentCommand.class, (c, s) -> verifyPayment(c, s))
                // AssignPermissionsToCustomerCommand - No event ?
        ;
    }

    private List<EventWithCauseImpl> verifyPayment(VerifyPaymentCommand c, CustomerState s) {
        List<Validation> validationRules = s.getCurrent()
                .getProduct(c.getPayment().getProduct().getComponent1())
                .get()
                .getProductConfiguration()
                .getOutboundConfiguration()
                .getValidationRules()
                .toList();
        return List.of(verifier.verifyPayment(c.getPayment(), validationRules)
                .toEvent(c.getPayment()));
    }
}
