package gov.scot.payments.customers.ch.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.component.commandhandler.stateful.EventGeneratorFunction;
import gov.scot.payments.application.component.commandhandler.stateful.StateUpdateFunction;
import gov.scot.payments.application.component.commandhandler.stateful.StatefulCommandHandlerApplication;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.customers.ch.app.verification.HttpValidationRuleEvaluator;
import gov.scot.payments.customers.ch.app.verification.SpelValidationRuleEvaluator;
import gov.scot.payments.customers.ch.app.verification.StatelessPaymentVerifier;
import gov.scot.payments.customers.ch.app.verification.ValidationRuleEvaluator;
import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.model.CommandFailureInfo;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.user.Scope;
import io.vavr.Tuple2;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextInstanceDataAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.context.config.annotation.ContextResourceLoaderConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import org.springframework.http.codec.ClientCodecConfigurer;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.web.reactive.function.client.WebClient;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3ClientBuilder;

import java.net.URI;
import java.util.function.Consumer;

@ApplicationComponent
@EnableAutoConfiguration(exclude = {ContextStackAutoConfiguration.class, ContextResourceLoaderAutoConfiguration.class,
        ContextInstanceDataAutoConfiguration.class})
public class CustomersCHApplication extends StatefulCommandHandlerApplication<Customer, CustomerState, EventWithCauseImpl> {

    @Override
    protected Set<String> registeredVerbs() {
        return HashSet.of("Read","CreateCustomer","ModifyCustomer","ApproveCustomerModification","ApproveProductModification","ModifyProduct");
    }

    @Override
    protected Class<Customer> stateEntityClass() {
        return Customer.class;
    }

    @Override
    protected Class<CustomerState> stateClass() {
        return CustomerState.class;
    }

    @Override
    protected CustomerState createNewAggregateInstance(SerializedMessage command, CommandFailureInfo error, Customer previous, Customer current, Long currentVersion) {
        return new CustomerState(command, error, previous, current, currentVersion);
    }

    @Override
    protected Scope extractScope(CustomerState state) {
        return new Scope(state.getCurrent().getName());
    }

    @Bean
    public S3Client s3Client(@Value("${S3_ENDPOINT_OVERRIDE:}") String s3Endpoint
            , @Value("${cloud.aws.region.static}") String region
            , @Value("${cloud.aws.credentials.accessKey}") String accessKey
            , @Value("${cloud.aws.credentials.secretKey}") String secretKey){
        AwsCredentials credentials = AwsBasicCredentials.create(accessKey,secretKey);
        AwsCredentialsProvider credentialsProvider = StaticCredentialsProvider.create(credentials);
        S3ClientBuilder builder = S3Client.builder()
                .credentialsProvider(credentialsProvider)
                .region(Region.of(region));
        if(!StringUtils.isEmpty(s3Endpoint)){
            builder = builder.endpointOverride(URI.create(s3Endpoint));
        }
        return builder.build();
    }

    @Bean
    public WebClient webClient(ObjectMapper mapper) {
        Consumer<ClientCodecConfigurer> consumer = configurer ->
                configurer.defaultCodecs().jackson2JsonDecoder(new Jackson2JsonDecoder(mapper));

        return WebClient.builder()
                .exchangeStrategies(strategies -> strategies.codecs(consumer))
                .build();
    }

    @Bean
    public StateUpdateFunction<CustomerState, Customer> stateUpdateFunction() {
        return new CustomerStateUpdateFunc();
    }

    @Bean
    public CustomerWebClientAdapter customerWebClientAdapter(WebClient webClient) {
        return new CustomerWebClientAdapter(webClient);
    }

    @Bean
    public ParseFileEventGenerator parseFileEventGenerator(CustomerWebClientAdapter customerWebClientAdapter, S3Client s3Client) {
        return new ParseFileEventGenerator(customerWebClientAdapter, s3Client);
    }

    @Bean
    public EventGeneratorFunction<CustomerState, EventWithCauseImpl> eventGenerationFunction(ParseFileEventGenerator parseFileEventGenerator,StatelessPaymentVerifier statelessPaymentVerifier) {
        return new CustomerEventGenerator(parseFileEventGenerator, statelessPaymentVerifier);
    }

    @Bean
    public StatelessPaymentVerifier statelessPaymentVerifier(List<ValidationRuleEvaluator> evaluators){
        return new StatelessPaymentVerifier(io.vavr.collection.List.ofAll(evaluators).toMap(e -> new Tuple2<>(e.getName(),e)));
    }

    @Bean
    public ValidationRuleEvaluator spelRuleEvaluator(){
        SpelExpressionParser parser = new SpelExpressionParser();
        return new SpelValidationRuleEvaluator(parser);
    }

    @Bean
    public ValidationRuleEvaluator httpRuleEvaluator(WebClient webClient){
        return new HttpValidationRuleEvaluator(webClient);
    }

    public static void main(String[] args) {
        BaseApplication.run(CustomersCHApplication.class, args);
    }
}