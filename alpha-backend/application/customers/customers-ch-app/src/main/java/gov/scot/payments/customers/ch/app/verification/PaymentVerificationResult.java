package gov.scot.payments.customers.ch.app.verification;

import gov.scot.payments.customers.model.aggregate.Product;
import gov.scot.payments.customers.model.event.CustomerPaymentApprovalRequiredEvent;
import gov.scot.payments.customers.model.event.CustomerPaymentRejectedEvent;
import gov.scot.payments.customers.model.event.CustomerPaymentVerifiedEvent;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.payments.model.aggregate.Payment;
import io.vavr.collection.List;
import lombok.Value;

import java.util.function.Supplier;

@Value
public class PaymentVerificationResult {

    public enum Status {Verified, ApprovalRequired, Rejected}

    private Status status;
    private String message;

    public static PaymentVerificationResult of(Status status, Supplier<String> message){
        switch (status) {
            case Verified:
                return verified();
            case Rejected:
                return rejected(message.get());
            case ApprovalRequired:
                return approve(message.get());
        }
        return null;
    }

    public static PaymentVerificationResult verified(){
        return new PaymentVerificationResult(Status.Verified,null);
    }

    public static PaymentVerificationResult rejected(final String message) {
        if(message == null){
            throw new IllegalArgumentException("Message must not be null");
        }
        return new PaymentVerificationResult(Status.Rejected,message);
    }

    public static PaymentVerificationResult approve(final String message) {
        if(message == null){
            throw new IllegalArgumentException("Message must not be null");
        }
        return new PaymentVerificationResult(Status.ApprovalRequired,message);
    }

    public PaymentVerificationResult merge(final PaymentVerificationResult other) {
        Status toUse = status.ordinal() > other.status.ordinal() ? status : other.status;
        String messageToUse;
        if(message == null){
           messageToUse = other.message;
        } else if(other.message == null){
            messageToUse = message;
        } else {
            messageToUse = String.format("%s, %s",message,other.message);
        }
        return new PaymentVerificationResult(toUse,messageToUse);
    }

    public EventWithCauseImpl toEvent(Payment payment) {
        EventWithCauseImpl event = null;
        switch (status){
            case Verified:
                event = CustomerPaymentVerifiedEvent.builder()
                        .payment(payment.getId())
                        .product(payment.getProduct())
                        .build();
                break;
            case Rejected:
                event = CustomerPaymentRejectedEvent.builder()
                        .payment(payment.getId())
                        .reason(message)
                        .product(payment.getProduct())
                        .build();
                break;
            case ApprovalRequired:
                event = CustomerPaymentApprovalRequiredEvent.builder()
                        .payment(payment.getId())
                        .reason(message)
                        .product(payment.getProduct())
                        .build();
                break;
        }
        return event;
    }

}
