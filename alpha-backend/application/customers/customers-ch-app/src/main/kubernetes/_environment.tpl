{{- define "customers-customers-ch-app.env" -}}
- name: CLIENT_SECRET
  valueFrom:
    configMapKeyRef:
      name: cognito-info
      key: client_secret
- name: CLIENT_ID
  valueFrom:
    configMapKeyRef:
      name: cognito-info
      key: client_id
- name: S3_ENDPOINT_OVERRIDE
  value: {{ .Values.${projectValuesName}.s3Override | quote }}
{{- end -}}