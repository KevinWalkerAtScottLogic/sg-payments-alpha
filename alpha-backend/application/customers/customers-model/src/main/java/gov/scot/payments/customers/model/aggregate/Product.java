package gov.scot.payments.customers.model.aggregate;

import lombok.*;
import org.apache.avro.reflect.Nullable;

import java.time.Instant;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Builder(toBuilder = true)
public class Product {

    @EqualsAndHashCode.Include @NonNull private String name;
    @NonNull @Builder.Default private Instant createdAt = Instant.now();
    @NonNull @Builder.Default private ProductStatus status = ProductStatus.New;
    @NonNull private ProductConfiguration productConfiguration;
    @Nullable private ProductConfiguration pendingProductConfiguration;
    @Nullable private ProductApproval deleteApproval;
    @Nullable private ProductApproval approval;
}
