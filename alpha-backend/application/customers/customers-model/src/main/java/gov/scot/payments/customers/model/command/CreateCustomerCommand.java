package gov.scot.payments.customers.model.command;

import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Subject;
import io.vavr.collection.Set;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "createCustomer")
@NoArgsConstructor
public class CreateCustomerCommand extends BaseCustomerCommand {

    public CreateCustomerCommand(String customerId, String user, Set<Role> roles, Long stateVersion, boolean reply) {
        super(customerId, user, roles, stateVersion, reply);
    }

    @MessageConstructor(role = "customers:CreateCustomer")
    public static CreateCustomerCommand fromRequest(String customerId, boolean reply, Long stateVersion, Subject principal){
        return new CreateCustomerCommand(customerId, principal.getName(), principal.getRoles(), stateVersion, reply);
    }
}
