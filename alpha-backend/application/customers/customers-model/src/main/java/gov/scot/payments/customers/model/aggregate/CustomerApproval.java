package gov.scot.payments.customers.model.aggregate;

import lombok.*;
import org.apache.avro.reflect.Nullable;

import java.time.Instant;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
public class CustomerApproval {

    private boolean approved;
    @NonNull private String user;
    @EqualsAndHashCode.Exclude @NonNull private Instant at;
    @Nullable private String message;
}
