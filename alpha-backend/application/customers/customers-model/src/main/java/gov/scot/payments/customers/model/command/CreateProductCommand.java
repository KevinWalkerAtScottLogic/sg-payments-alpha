package gov.scot.payments.customers.model.command;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.customers.model.api.ProductRequest;
import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Subject;
import io.vavr.collection.Set;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "createProduct")
@NoArgsConstructor
public class CreateProductCommand extends BaseProductCommand {

    @NonNull private ProductRequest productRequest;

    public CreateProductCommand(ProductRequest productRequest, String user, Set<Role> roles, Long stateVersion, boolean reply) {
        super(new CompositeReference(productRequest.getCustomerId(), productRequest.getName()), user, roles, stateVersion, reply);
        this.productRequest = productRequest;
    }

    @MessageConstructor(role = "customers:ModifyCustomer")
    public static CreateProductCommand fromRequest(ProductRequest product, boolean reply, Long stateVersion, Subject principal){
        return new CreateProductCommand(product, principal.getName(), principal.getRoles(), stateVersion, reply);
    }
}
