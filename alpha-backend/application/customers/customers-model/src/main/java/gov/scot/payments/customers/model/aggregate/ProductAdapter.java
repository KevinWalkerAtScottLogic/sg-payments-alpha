package gov.scot.payments.customers.model.aggregate;

import lombok.*;

import java.net.URL;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
public class ProductAdapter {

    @NonNull private ProductAdapterType type;
    @NonNull private String location;
}
