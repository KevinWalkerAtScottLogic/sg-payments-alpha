package gov.scot.payments.customers.model.event;

import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "productCreated")
@NoArgsConstructor
@ToString
public class ProductCreatedEvent extends BaseProductEventWithCause {

    public ProductCreatedEvent(String productId, Customer customer, String user, Long stateVersion) {
        super(productId, customer, user, stateVersion);
    }
}
