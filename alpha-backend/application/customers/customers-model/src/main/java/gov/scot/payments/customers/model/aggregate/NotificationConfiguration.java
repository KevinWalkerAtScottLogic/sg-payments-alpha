package gov.scot.payments.customers.model.aggregate;

import io.vavr.collection.Map;
import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
public class NotificationConfiguration {

    @NonNull private String channel;
    @NonNull private Map<String, String> config;
}
