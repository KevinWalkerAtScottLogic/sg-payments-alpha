package gov.scot.payments.customers.model.aggregate;

public enum CustomerStatus {
    New,
    Live,
    Rejected,
    ModifyRequested,
    DeleteRequested,
    Deleting,
    Deleted
}
