package gov.scot.payments.customers.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.*;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

import java.net.URI;
import java.net.URL;
import java.time.Instant;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "parseFile")
@NoArgsConstructor
@ToString
public class ParseFileCommand extends CommandImpl implements HasStateVersion, HasKey<String>, ReadOnlyState {

    @NonNull private CompositeReference product;
    @NonNull private URI file;
    @Nullable private Long stateVersion;
    @NonNull private String user;
    @NonNull private Instant createdAt;

    public ParseFileCommand(CompositeReference product, URI file, Long stateVersion, String user, Instant createdAt, boolean reply) {
        super(reply);
        this.product = product;
        this.file = file;
        this.stateVersion = stateVersion;
        this.user=user;
        this.createdAt=createdAt;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return product.getComponent0();
    }
}
