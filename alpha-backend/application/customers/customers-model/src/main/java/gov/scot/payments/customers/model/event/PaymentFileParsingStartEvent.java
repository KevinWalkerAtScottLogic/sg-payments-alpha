package gov.scot.payments.customers.model.event;

import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.model.MessageType;
import io.vavr.collection.List;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.net.URI;
import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "PaymentFileParsingStart")
@NoArgsConstructor
@RequiredArgsConstructor
public class PaymentFileParsingStartEvent extends BaseProductEventWithCause {

    @NonNull private URI uri;
    @NonNull private String customerPaymentBatchId;
    @NonNull private List<UUID> paymentIds;

    public PaymentFileParsingStartEvent(URI uri, String customerPaymentBatchId, List<UUID> paymentIds, String productId, Customer customer, String user, Long stateVersion){
        super(productId, customer, user, stateVersion);
        this.uri=uri;
        this.customerPaymentBatchId=customerPaymentBatchId;
        this.paymentIds=paymentIds;
    }

}
