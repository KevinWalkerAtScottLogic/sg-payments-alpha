package gov.scot.payments.customers.model.event;

import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "deleteCustomerRequested")
@NoArgsConstructor
public class DeleteCustomerRequestedEvent extends BaseCustomerEventWithCause {

    public DeleteCustomerRequestedEvent(Customer customer, String user, Long stateVersion) {
        super(customer, user, stateVersion);
    }
}
