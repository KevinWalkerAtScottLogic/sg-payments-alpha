package gov.scot.payments.customers.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "paymentApprovalRequired")
@NoArgsConstructor
@ToString
public class CustomerPaymentApprovalRequiredEvent extends EventWithCauseImpl implements HasKey<String> {

    @NonNull private UUID payment;
    @NonNull private CompositeReference product;
    @NonNull private String reason;

    @Override
    @JsonIgnore
    public String getKey() {
        return product.getComponent0();
    }
}
