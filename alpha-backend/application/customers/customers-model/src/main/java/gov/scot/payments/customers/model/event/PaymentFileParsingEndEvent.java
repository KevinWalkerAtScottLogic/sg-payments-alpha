package gov.scot.payments.customers.model.event;

import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.model.MessageType;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.net.URI;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "PaymentFileParsingEnd")
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
public class PaymentFileParsingEndEvent extends BaseProductEventWithCause {

    @NonNull private URI uri;
    @NonNull private String customerPaymentBatchId;

    public PaymentFileParsingEndEvent(URI uri, String customerPaymentBatchId, String productId, Customer customer, String user, Long stateVersion){
        super(productId, customer, user, stateVersion);
        this.uri=uri;
        this.customerPaymentBatchId=customerPaymentBatchId;
    }

}
