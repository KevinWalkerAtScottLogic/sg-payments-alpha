package gov.scot.payments.customers.model.aggregate;

public enum ValidationAction {
    RejectAction,
    RequiresApprovalAction;

}
