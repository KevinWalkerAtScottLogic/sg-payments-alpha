package gov.scot.payments.customers.model.event;

import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "createCustomerRejected")
@NoArgsConstructor
public class CreateCustomerRejectedEvent extends BaseCustomerEventWithCause {

    @NonNull private String reason;

    public CreateCustomerRejectedEvent(Customer customer, String reason, String user, Long stateVersion) {
        super(customer, user, stateVersion);
        this.reason = reason;
    }
}
