package gov.scot.payments.customers.model.aggregate;

public interface HasCustomer {

    String getCustomerId();
}
