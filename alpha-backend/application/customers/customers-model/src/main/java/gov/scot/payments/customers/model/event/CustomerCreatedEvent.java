package gov.scot.payments.customers.model.event;

import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "customerCreated")
@NoArgsConstructor
public class CustomerCreatedEvent extends BaseCustomerEvent {

    public CustomerCreatedEvent(Customer customer, String user, Long stateVersion) {
        super(customer, user, stateVersion);
    }
}
