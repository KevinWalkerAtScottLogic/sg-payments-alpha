package gov.scot.payments.customers.model.event;

import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.customers.model.aggregate.Product;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "createProductRejected")
@NoArgsConstructor
public class CreateProductRejectedEvent extends BaseProductEventWithCause {

    @NonNull private String reason;

    public CreateProductRejectedEvent(String productId, String reason, Customer customer, String user, Long stateVersion) {
        super(productId, customer, user, stateVersion);
        this.reason = reason;
    }
}
