package gov.scot.payments.customers.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.customers.model.aggregate.HasProduct;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.*;
import gov.scot.payments.model.user.Role;
import io.vavr.collection.Set;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@NoArgsConstructor
@MessageType(context = "customers", type = "baseProductCommand")
public class BaseProductCommand extends CommandImpl implements HasProduct, HasUser, HasStateVersion, HasKey<String> {

    /*
     * Defines @MessageType and is not abstract in order to pass the archunit tests.
     */

    @Getter @NonNull private CompositeReference productId;
    @Getter @NonNull private String user;
    @Getter @NonNull private Set<Role> roles;
    @Getter @Nullable private Long stateVersion;

    public BaseProductCommand(CompositeReference productId, String user, Set<Role> roles, Long stateVersion, boolean reply) {
        super(reply);
        this.productId = productId;
        this.user = user;
        this.roles = roles;
        this.stateVersion = stateVersion;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return productId.getComponent0();
    }
}
