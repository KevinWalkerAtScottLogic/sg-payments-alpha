package gov.scot.payments.customers.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.net.URI;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "paymentFileParsingFailed")
@NoArgsConstructor
@RequiredArgsConstructor
public class PaymentFileParsingFailedEvent extends BaseProductEventWithCause {

    @NonNull private String errorMessage;
    @NonNull private URI uri;

    public PaymentFileParsingFailedEvent(String errorMessage, URI uri, String productId, Customer customer, String user, Long stateVersion) {
        super(productId, customer, user, stateVersion);
        this.uri = uri;
        this.errorMessage = errorMessage;
    }
}
