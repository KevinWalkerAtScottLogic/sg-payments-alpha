package gov.scot.payments.customers.model.command;

import gov.scot.payments.customers.model.api.CustomerRejectRequest;
import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Subject;
import io.vavr.collection.Set;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "rejectDeleteCustomer")
@NoArgsConstructor
public class RejectDeleteCustomerCommand extends BaseCustomerCommand {

    @NonNull private String reason;

    public RejectDeleteCustomerCommand(String customerId, String reason, String user, Set<Role> roles, Long stateVersion, boolean reply) {
        super(customerId, user, roles, stateVersion, reply);
        this.reason = reason;
    }

    @MessageConstructor(role = "customers:ApproveCustomerModification")
    public static RejectDeleteCustomerCommand fromRequest(CustomerRejectRequest rejectRequest, boolean reply, Long stateVersion, Subject principal){
        return new RejectDeleteCustomerCommand(rejectRequest.getCustomerId(), rejectRequest.getReason(), principal.getName(), principal.getRoles(), stateVersion, reply);
    }
}
