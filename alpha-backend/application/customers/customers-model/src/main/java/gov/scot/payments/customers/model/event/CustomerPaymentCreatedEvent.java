package gov.scot.payments.customers.model.event;

import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.Payment;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.net.URI;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "customerPaymentCreated")
@NoArgsConstructor
@ToString
public class CustomerPaymentCreatedEvent extends BaseProductEventWithCause {

    @NonNull private URI uri;
    @NonNull private String customerPaymentBatchId;
    @NonNull private Payment payment;

    public CustomerPaymentCreatedEvent(URI uri, String customerPaymentBatchId, Payment payment, String productId, Customer customer, String user, Long stateVersion) {
        super(productId, customer, user, stateVersion);
        this.uri=uri;
        this.customerPaymentBatchId =customerPaymentBatchId;
        this.payment = payment;
    }


}
