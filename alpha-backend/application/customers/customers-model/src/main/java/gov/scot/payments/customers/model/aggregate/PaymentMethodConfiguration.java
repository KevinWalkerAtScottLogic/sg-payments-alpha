package gov.scot.payments.customers.model.aggregate;

import lombok.*;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder(toBuilder = true)
public class PaymentMethodConfiguration {

    public static final String BACS_SERVICE_USER_NUMBER = "BACS_SUN";
    public static final String BACS_SERVICE_ACCOUNT_NAME= "BACS_SAN";

    @NonNull @Builder.Default private Map<String,String> properties =  new HashMap<>();
}
