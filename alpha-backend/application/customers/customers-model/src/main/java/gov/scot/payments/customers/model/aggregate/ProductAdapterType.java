package gov.scot.payments.customers.model.aggregate;

public enum ProductAdapterType {
    internal,
    external
}
