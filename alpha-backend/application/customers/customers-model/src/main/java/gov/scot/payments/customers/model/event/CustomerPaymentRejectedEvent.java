package gov.scot.payments.customers.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "paymentRejected")
@NoArgsConstructor
public class CustomerPaymentRejectedEvent extends EventWithCauseImpl implements HasKey<String> {

    @NonNull private UUID payment;
    @NonNull private CompositeReference product;
    @NonNull private String reason;

    @Override
    @JsonIgnore
    public String getKey() {
        return product.getComponent0();
    }
}
