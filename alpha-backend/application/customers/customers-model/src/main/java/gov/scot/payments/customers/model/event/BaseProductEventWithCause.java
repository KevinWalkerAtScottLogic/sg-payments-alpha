package gov.scot.payments.customers.model.event;

import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.customers.model.aggregate.Product;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@NoArgsConstructor
@MessageType(context = "customers", type = "baseProductEventWithCause")
public class BaseProductEventWithCause extends BaseCustomerEventWithCause {

    /*
     * Defines @MessageType and is not abstract in order to pass the archunit tests.
     */

    @Getter @NonNull private String productId;

    BaseProductEventWithCause(String productId, Customer customer, String user, Long stateVersion) {
        super(customer, user, stateVersion);
        this.productId = productId;
    }
}
