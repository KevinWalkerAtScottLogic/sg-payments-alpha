package gov.scot.payments.customers.model.command;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Subject;
import io.vavr.collection.Set;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "approveCreateProduct")
@ToString
@NoArgsConstructor
public class ApproveCreateProductCommand extends BaseProductCommand {

    public ApproveCreateProductCommand(CompositeReference productId, String user, Set<Role> roles, Long stateVersion, boolean reply) {
        super(productId, user, roles, stateVersion, reply);
    }

    @MessageConstructor(role = "customers:ApproveProductModification")
    public static ApproveCreateProductCommand fromRequest(CompositeReference productId, boolean reply, Long stateVersion, Subject principal){
        return new ApproveCreateProductCommand(productId, principal.getName(), principal.getRoles(), stateVersion, reply);
    }

}
