package gov.scot.payments.customers.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.customers.model.aggregate.HasCustomer;
import gov.scot.payments.model.*;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Scope;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@NoArgsConstructor
@MessageType(context = "customers", type = "baseCustomerCommand")
public class BaseCustomerCommand extends CommandImpl implements HasCustomer, HasUser, HasStateVersion, HasKey<String> {

    /*
     * Defines @MessageType and is not abstract in order to pass the archunit tests.
     */

    @Getter @NonNull private String customerId;
    @Getter @NonNull private String user;
    @Getter @NonNull private Set<Role> roles;
    @Getter @Nullable private Long stateVersion;

    public BaseCustomerCommand(String customerId, String user, Set<Role> roles, Long stateVersion, boolean reply) {
        super(reply);
        this.customerId = customerId;
        this.user = user;
        this.roles = roles;
        this.stateVersion = stateVersion;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return customerId;
    }
}
