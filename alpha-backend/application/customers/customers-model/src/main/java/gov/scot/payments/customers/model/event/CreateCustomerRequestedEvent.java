package gov.scot.payments.customers.model.event;

import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "createCustomerRequested")
@NoArgsConstructor
public class CreateCustomerRequestedEvent extends BaseCustomerEventWithCause {

    public CreateCustomerRequestedEvent(final Customer customer, final String user, final Long stateVersion) {
        super(customer, user, stateVersion);
    }

    public CreateCustomerRequestedEvent(Customer customer, String user, Long stateVersion,
                                        final UUID correlationId, final int executionCount, final boolean reply) {
        super(customer, user, stateVersion, correlationId, executionCount, reply);
    }
}
