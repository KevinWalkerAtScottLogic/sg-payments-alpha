package gov.scot.payments.customers.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.customers.model.aggregate.HasCustomer;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "finalizeDeleteCustomer")
@NoArgsConstructor
public class FinalizeDeleteCustomerCommand extends CommandImpl implements HasCustomer, HasKey<String> {

    @NonNull private String customerId;

    public FinalizeDeleteCustomerCommand(String customerId, boolean reply) {
        super(reply);
        this.customerId = customerId;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return customerId;
    }
}
