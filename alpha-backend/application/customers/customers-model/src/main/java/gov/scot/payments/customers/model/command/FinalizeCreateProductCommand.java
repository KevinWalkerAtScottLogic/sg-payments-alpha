package gov.scot.payments.customers.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.customers.model.aggregate.HasProduct;
import gov.scot.payments.model.*;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Subject;
import io.vavr.collection.Set;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "finalizeCreateProduct")
@NoArgsConstructor
@ToString
public class FinalizeCreateProductCommand extends CommandImpl implements HasProduct, HasKey<String> {

    @NonNull private CompositeReference productId;

    public FinalizeCreateProductCommand(CompositeReference productId, boolean reply) {
        super(reply);
        this.productId = productId;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return productId.getComponent0();
    }
}
