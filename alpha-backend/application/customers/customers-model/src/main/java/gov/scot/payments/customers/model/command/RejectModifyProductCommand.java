package gov.scot.payments.customers.model.command;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.customers.model.api.ProductRejectRequest;
import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Subject;
import io.vavr.collection.Set;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "rejectModifyProduct")
@NoArgsConstructor
public class RejectModifyProductCommand extends BaseProductCommand {

    @NonNull private String reason;

    public RejectModifyProductCommand(CompositeReference productId, String reason, String user, Set<Role> roles, Long stateVersion, boolean reply) {
        super(productId, user, roles, stateVersion, reply);
        this.reason = reason;
    }

    @MessageConstructor(role = "customers:ApproveProductModification")
    public static RejectModifyProductCommand fromRequest(ProductRejectRequest rejectRequest, boolean reply, Long stateVersion, Subject principal){
        return new RejectModifyProductCommand(rejectRequest.getProductReference(), rejectRequest.getReason(), principal.getName(), principal.getRoles(), stateVersion, reply);
    }
}
