package gov.scot.payments.customers.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.*;
import gov.scot.payments.payments.model.aggregate.Payment;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

import java.net.URL;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "customers", type = "verifyPayment")
@NoArgsConstructor
@ToString
public class VerifyPaymentCommand extends CommandImpl implements HasKey<String>, ReadOnlyState {

    @NonNull private Payment payment;

    public VerifyPaymentCommand(Payment payment, boolean reply) {
        super(reply);
        this.payment = payment;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return payment.getProduct().getComponent0();
    }
}
