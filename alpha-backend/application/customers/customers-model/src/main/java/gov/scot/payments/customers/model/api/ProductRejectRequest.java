package gov.scot.payments.customers.model.api;

import gov.scot.payments.model.CompositeReference;
import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Builder
public class ProductRejectRequest {
    @EqualsAndHashCode.Include @NonNull private CompositeReference productReference;
    @NonNull private String reason;
}
