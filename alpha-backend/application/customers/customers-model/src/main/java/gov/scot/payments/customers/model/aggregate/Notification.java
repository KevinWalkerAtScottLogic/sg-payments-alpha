package gov.scot.payments.customers.model.aggregate;

import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
public class Notification {

    @NonNull private String event;
    @NonNull private NotificationConfiguration configuration;
}
