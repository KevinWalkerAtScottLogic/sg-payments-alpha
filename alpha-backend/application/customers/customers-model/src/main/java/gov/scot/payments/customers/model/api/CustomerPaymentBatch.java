package gov.scot.payments.customers.model.api;

import gov.scot.payments.payments.model.api.CreatePaymentRequest;
import io.vavr.collection.List;
import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Builder(toBuilder = true)
public class CustomerPaymentBatch {

    @EqualsAndHashCode.Include String id;

    List<CreatePaymentRequest> payments;

}
