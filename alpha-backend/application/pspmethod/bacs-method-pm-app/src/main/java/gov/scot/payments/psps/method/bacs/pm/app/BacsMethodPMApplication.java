package gov.scot.payments.psps.method.bacs.pm.app;

import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.psps.method.pm.app.BaseMethodPspProcessManager;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;

@ApplicationComponent
public class BacsMethodPMApplication extends BaseMethodPspProcessManager {

    @Override
    protected Set<PaymentMethod> supportedPaymentMethods() {
        return HashSet.of(PaymentMethod.Bacs);
    }

    public static void main(String[] args){
        BaseApplication.run(BacsMethodPMApplication.class,args);
    }
}