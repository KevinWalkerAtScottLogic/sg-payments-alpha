SCRIPT_PATH=$(dirname "$0")

echo Enter a payment method "(Bacs, FassterPayments, SEPA_DC, iMovo etc..)"
read PAYMENT_METHOD

CH_COMPONENT_NAME="$PAYMENT_METHOD-method-ch"
PM_COMPONENT_NAME="$PAYMENT_METHOD-method-pm"

CH_COMPONENT_PATH="$SCRIPT_PATH"/"$CH_COMPONENT_NAME"-app
if [ -d "$CH_COMPONENT_PATH" ]; then
    echo Component already exists, exiting
    exit
fi

PM_COMPONENT_PATH="$SCRIPT_PATH"/"$PM_COMPONENT_NAME"-app
if [ -d "$PM_COMPONENT_PATH" ]; then
    echo Component already exists, exiting
    exit
fi

../../gradlew :application:pspmethod:method-psp-base-ch:cleanArch :application:pspmethod:method-psp-base-ch::generate -Dgroup=gov.scot.payments.psps.method."$PAYMENT_METHOD" -Dname="$CH_COMPONENT_NAME" -Dversion=1.0-SNAPSHOT -Dcom.orctom.gradle.archetype.binding.contextName="$PAYMENT_METHOD"method -Dcom.orctom.gradle.archetype.binding.paymentMethod="$PAYMENT_METHOD"
../../gradlew :application:pspmethod:method-psp-base-pm:cleanArch :application:pspmethod:method-psp-base-pm::generate -Dgroup=gov.scot.payments.psps.method."$PAYMENT_METHOD" -Dname="$PM_COMPONENT_NAME" -Dversion=1.0-SNAPSHOT -Dcom.orctom.gradle.archetype.binding.contextName="$PAYMENT_METHOD"method -Dcom.orctom.gradle.archetype.binding.paymentMethod="$PAYMENT_METHOD"

CH_GENERATED_PATH="$SCRIPT_PATH"/method-psp-base-ch/generated
PM_GENERATED_PATH="$SCRIPT_PATH"/method-psp-base-pm/generated

cp -R "$CH_GENERATED_PATH"/* "$SCRIPT_PATH"
cp -R "$PM_GENERATED_PATH"/* "$SCRIPT_PATH"

exit