package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentBatch;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import io.vavr.collection.List;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.Stores;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;

@Disabled
public class TimeWindowingPaymentBatcherTest extends PaymentBatcherTest {

    private TimeWindowingPaymentBatcher batcher;

    @BeforeEach
    public void setUpBatcher() throws Exception {
        Materialized materialized = Materialized
                .as(Stores.inMemoryWindowStore(KafkaStreamsTestHarness.STATE_STORE, Duration.ofSeconds(4),Duration.ofSeconds(3),false))
                .with(Serdes.String(), batchSerde)
                .withCachingDisabled()
                .withRetention(Duration.ofSeconds(4));

        harness.withWindowStore(Duration.ofSeconds(3),Duration.ofSeconds(4));
        batcher = PaymentBatchers.time(Duration.ofSeconds(3), materialized )
                                 .groupingFunction(PaymentBatchers.groupedByProduct(paymentSerde))
                                 .build();
        batcher.batch(harness.stream()).to("batches");
    }

    @Test
    public void testBatching(){
        Instant now = Instant.now();
        try (final TopologyTestDriver topology = harness.toTopology(now)) {
            sendPayments(topology,new CompositeReference("customer","product1"),1,3,now);
            sendPayments(topology,new CompositeReference("customer","product2"),4,2,now);
            sendPayments(topology,new CompositeReference("customer","product3"),6,3,now);
            sleep(topology,Duration.ofSeconds(5));
            now = now.plusSeconds(5);
            sendPayments(topology,new CompositeReference("customer","product1"),1,1,now);
            sendPayments(topology,new CompositeReference("customer","product2"),1,1,now);
            sendPayments(topology,new CompositeReference("customer","product3"),1,1,now);
            sleep(topology,Duration.ofSeconds(5));
            now = now.plusSeconds(5);
            List<PspAdapterPaymentBatch> batches = getBatches(topology);
            verifyBatches(batches,3,8);
            verifyBatch(batches.get(0),3,6,pi -> pi.getProduct().getComponent1().equals("product1"));
            verifyBatch(batches.get(1),2,9,pi -> pi.getProduct().getComponent1().equals("product2"));
            verifyBatch(batches.get(2),3,21,pi -> pi.getProduct().getComponent1().equals("product3"));

            now = sendPayments(topology,new CompositeReference("customer","product"),1,10,Duration.ofSeconds(1),now);
            sleep(topology,Duration.ofSeconds(5));
            now = now.plusSeconds(5);
            batches = getBatches(topology).filter(b -> b.getEarliestPayment().get().getProduct().getComponent1().equals("product"));
            verifyBatches(batches,3,9);
            verifyBatch(batches.get(0),3,6,pi -> pi.getProduct().getComponent1().equals("product"));
            verifyBatch(batches.get(1),3,15,pi -> pi.getProduct().getComponent1().equals("product"));
            verifyBatch(batches.get(2),3,24,pi -> pi.getProduct().getComponent1().equals("product"));
        }


    }

    private Instant sendPayments(TopologyTestDriver topology,CompositeReference service, int startingValue, int count, Duration sleep, Instant now) {
        Instant current = now;
        List<Payment> instructions = List.empty();
        for(int i=startingValue;i<=startingValue+count;i++){
            Payment payment = createPayment(service, i, "GBP");
            harness.send(topology,payment);
            instructions = instructions.append(payment);
            sleep(topology,sleep);
            current = current.plus(sleep);
        }
        return current;
    }

}
