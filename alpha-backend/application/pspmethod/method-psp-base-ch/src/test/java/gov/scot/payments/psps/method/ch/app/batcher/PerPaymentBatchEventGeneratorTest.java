package gov.scot.payments.psps.method.ch.app.batcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import gov.scot.payments.application.kafka.MessageInfo;
import gov.scot.payments.application.kafka.MessageWithInfo;
import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.psps.method.ch.app.batcher.PaymentBatchEventGeneratorDelegate;
import gov.scot.payments.psps.method.ch.app.batcher.PerPaymentBatchEventGenerator;
import gov.scot.payments.psps.method.model.PspAdapterPayment;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentState;
import gov.scot.payments.psps.method.model.command.MethodAddPaymentToBatchCommand;
import gov.scot.payments.psps.method.model.command.MethodCompletePaymentCancelCommand;
import gov.scot.payments.psps.method.model.event.MethodPaymentBatchClosedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentCancelSuccessEvent;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.jackson.datatype.VavrModule;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.TopologyTestDriver;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.support.JacksonUtils;
import org.springframework.kafka.support.converter.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.messaging.MessageHeaders;

import javax.money.CurrencyUnit;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class PerPaymentBatchEventGeneratorTest {

    private KafkaStreamsTestHarness harness;
    private PerPaymentBatchEventGenerator generator;
    private Serde<Command> commandSerde;

    @BeforeEach
    public void setUp(){
        ObjectMapper mapper = JacksonUtils.enhancedObjectMapper();
        mapper.activateDefaultTyping(
                BasicPolymorphicTypeValidator.builder()
                                             .allowIfBaseType(Command.class)
                                             .allowIfBaseType(Set.class)
                                             .allowIfBaseType(java.util.Map.class)
                                             .allowIfBaseType(List.class)
                                             .allowIfBaseType(CurrencyUnit.class)
                                             .build()
        );
        mapper.registerModule(new VavrModule());
        JsonSerde<Command> commandSerde = new JsonSerde<>(Command.class, mapper);
        DefaultJackson2JavaTypeMapper typeMapper = (DefaultJackson2JavaTypeMapper) ((JsonDeserializer<Command>) commandSerde.deserializer()).getTypeMapper();
        typeMapper.setClassIdFieldName(MessageHeaders.CONTENT_TYPE);
        final String mappingConfig = KafkaStreamsTestHarness.buildMappingsConfig(MethodAddPaymentToBatchCommand.class, MethodCompletePaymentCancelCommand.class,PspAdapterPaymentState.class, MethodPaymentCancelSuccessEvent.class, MethodPaymentBatchClosedEvent.class);
        commandSerde.configure(HashMap.of(JsonSerializer.TYPE_MAPPINGS, mappingConfig).toJavaMap(), false);
        this.commandSerde = commandSerde;
        harness = KafkaStreamsTestHarness.builderWithMappings(Serdes.ByteArray(),new gov.scot.payments.testing.kafka.JsonSerde(),MethodAddPaymentToBatchCommand.class, MethodCompletePaymentCancelCommand.class, PspAdapterPaymentState.class, MethodPaymentCancelSuccessEvent.class, MethodPaymentBatchClosedEvent.class)
                                         .build();

        generator = PerPaymentBatchEventGenerator.builder()
                                                 .commandSerde(commandSerde)
                                                 .errorHandler( (c,s,t) -> List.empty())
                                                 .metrics(new MicrometerMetrics(new SimpleMeterRegistry()))
                                                 .paymentMethod(PaymentMethod.Bacs)
                                                 .eventGenerator(new PaymentBatchEventGeneratorDelegate(PaymentMethod.Bacs))
                                                 .build();
        generator.apply(harness.table()).to(KafkaStreamsTestHarness.OUTPUT_TOPIC);
    }

    @Test
    public void test(){
        var payment = makePayment();
        var cancelCommand = MethodCompletePaymentCancelCommand.builder()
                                                        .paymentId(payment.getId())
                                                        .method(PaymentMethod.Bacs)
                                                        .psp("psp")
                                                        .pspMetadata(new java.util.HashMap<>(java.util.Map.of("a","b")))
                                                        .build();

        var addToBatchCommand = MethodAddPaymentToBatchCommand.builder()
                                                              .payment(payment)
                                                              .build();
        var cancelState = PspAdapterPaymentState.builder()
                                                .message(new MessageWithInfo<>(cancelCommand, MessageInfo.builder().context("pspmethod").type("completePaymentCancel").contentType(MethodCompletePaymentCancelCommand.class.getName()).build()).serialize((Serde)commandSerde))
                                                .current(PspAdapterPayment.builder().payment(payment).build())
                                                .build();
        var addToBatchState = PspAdapterPaymentState.builder()
                                                .message(new MessageWithInfo<>(addToBatchCommand,MessageInfo.builder().context("pspmethod").type("addPaymentToBatch").contentType(MethodAddPaymentToBatchCommand.class.getName()).build()).serialize((Serde)commandSerde))
                                                .current(PspAdapterPayment.builder().payment(payment).build())
                                                .build();

        try(TopologyTestDriver topology = harness.toTopology()){

            final KeyValueWithHeaders<String, PspAdapterPaymentState> cancelMsg = KeyValueWithHeaders.msg("1", cancelState,"contentType",MethodAddPaymentToBatchCommand.class.getName());
            final KeyValueWithHeaders<String, PspAdapterPaymentState> addMsg = KeyValueWithHeaders.msg("2", addToBatchState,"contentType",MethodAddPaymentToBatchCommand.class.getName());
            harness.sendKeyValues(topology, cancelMsg, addMsg);
            var events = harness.drain(topology);
            assertEquals(2,events.size());
            assertEquals(MethodPaymentCancelSuccessEvent.class,events.get(0).getClass());
            assertEquals(MethodPaymentBatchClosedEvent.class,events.get(1).getClass());
        }
    }

    private Payment makePayment() {
        return Payment.builder()
                      .createdBy("user")
                      .allowedMethods(List.of(PaymentMethod.Bacs))
                      .debtor(PartyIdentification.builder().name("").build())
                      .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                      .creditor(PartyIdentification.builder().name("").build())
                        .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                        .amount(Money.parse("GBP 1.00"))
                      .latestExecutionDate(Instant.now())
                      .product(new CompositeReference("",""))
                      .build();
    }

}