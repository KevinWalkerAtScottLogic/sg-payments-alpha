package gov.scot.payments.psps.method.ch.app;

import gov.scot.payments.psps.method.ch.app.PspClient;
import gov.scot.payments.psps.method.ch.app.RetryingPspClient;
import gov.scot.payments.psps.method.model.PspCancelRequest;
import gov.scot.payments.psps.method.model.PspSubmissionRequest;
import io.vavr.collection.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.retry.RetryOperations;

import java.util.UUID;

import static org.mockito.Mockito.*;

class RetryingPspClientTest {

    private RetryingPspClient invoker;
    private RetryOperations retryOperations;
    private PspClient client;

    @BeforeEach
    public void setUp(){
        client = mock(PspClient.class);
        retryOperations = mock(RetryOperations.class);
        invoker = new RetryingPspClient(retryOperations,client);
    }

    @Test
    void submitPayments() {
        invoker.submitPayments(PspSubmissionRequest.builder().payments(List.of()).build());
        verify(retryOperations,times(1)).execute(any());
    }

    @Test
    void estimateTransactionCosts() {
        invoker.estimateTransactionCosts(PspSubmissionRequest.builder().payments(List.of()).build());
        verify(retryOperations,times(1)).execute(any());
    }

    @Test
    void cancelPayment() {
        invoker.cancelPayment(PspCancelRequest.builder().payment(UUID.randomUUID()).psp("psp").build());
        verify(retryOperations,times(1)).execute(any());
    }
}