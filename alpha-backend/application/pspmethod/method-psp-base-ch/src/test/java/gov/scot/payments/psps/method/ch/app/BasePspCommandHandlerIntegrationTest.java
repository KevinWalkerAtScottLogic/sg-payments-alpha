package gov.scot.payments.psps.method.ch.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.component.commandhandler.stateful.StatefulErrorHandler;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.Message;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.psps.method.ch.app.batcher.PaymentBatchEventGenerator;
import gov.scot.payments.psps.method.ch.app.batcher.PaymentBatchEventGeneratorDelegate;
import gov.scot.payments.psps.method.ch.app.batcher.PerPaymentBatchEventGenerator;
import gov.scot.payments.psps.method.model.PspAdapterPayment;
import gov.scot.payments.psps.method.model.PspSubmissionResponse;
import gov.scot.payments.psps.method.model.PspTransactionCostEstimateResponse;
import gov.scot.payments.psps.method.model.command.MethodAddPaymentToBatchCommand;
import gov.scot.payments.psps.method.model.command.MethodSubmitPaymentBatchCommand;
import gov.scot.payments.psps.method.model.event.MethodPaymentAcceptedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentBatchClosedEvent;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.vavr.collection.List;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.javamoney.moneta.Money;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.apache.kafka.streams.KafkaStreams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.reactive.server.WebTestClient;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.spring.TestConfiguration;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;

import static gov.scot.payments.testing.security.MockSubjectAuthentication.user;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockAuthentication;

@ApplicationIntegrationTest(classes = {BasePspCommandHandlerIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.COMMAND_HANDLER
        ,properties = {"spring.cloud.kubernetes.enabled=false"
        ,"context.name=bacs"
        ,"component.name=bacs-ch"
        ,"state.query.authority=bacs:Read"
        ,"spring.cloud.discovery.client.simple.instances.adapter1[0].uri=http://localhost:8080"
        ,"spring.cloud.discovery.client.simple.instances.adapter1[0].metadata.scot.gov.payments/payment-method=bacs"
        ,"spring.cloud.discovery.client.simple.instances.adapter1[0].metadata.scot.gov.payments/service-type=psp"})
public class BasePspCommandHandlerIntegrationTest {

    @MockBean
    PspClientFactory clientFactory;

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, EmbeddedBrokerClient brokerClient, @Value("${namespace}") String namespace){
        brokerClient.getBroker().addTopicsIfNotExists(namespace+"-psps-command-errors");
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
    }

    @Test
    public void test(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        PspClient pspClient = mock(PspClient.class);
        when(clientFactory.apply(any())).thenReturn(pspClient);
        when(pspClient.getName()).thenReturn("adapter");
        when(pspClient.estimateTransactionCosts(any())).thenReturn(PspTransactionCostEstimateResponse.builder()
                                                                                                     .psp("adapter")
                                                                                                     .amount(Money.parse("GBP 1.00"))
                                                                                                     .build());
        when(pspClient.submitPayments(any())).thenReturn(PspSubmissionResponse.builder()
                                                                              .psp("adapter")
                                                                              .success(true)
                                                                              .metadata(Map.of("a","b"))
                                                                              .build());
        var payment = Payment.builder()
                             .createdBy("user")
                             .allowedMethods(List.of(PaymentMethod.Bacs))
                             .debtor(PartyIdentification.builder().name("").build())
                             .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                             .creditor(PartyIdentification.builder().name("").build())
                            .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                            .amount(Money.parse("GBP 1.00"))
                             .latestExecutionDate(Instant.now())
                             .product(new CompositeReference("customer","product"))
                             .build();

        var addPayment = MethodAddPaymentToBatchCommand.builder()
                                                    .payment(payment)
                                                    .build();
        var submitPayment = MethodSubmitPaymentBatchCommand.builder()
                                                     .key("")
                                                     .payments(List.of(payment.getId()))
                                                     .build();
        final KeyValueWithHeaders<String, MethodAddPaymentToBatchCommand> addPaymentMessage = KeyValueWithHeaders.msg(payment.getKey(), addPayment,new RecordHeader(Message.CONTEXT_HEADER,"bacs".getBytes()));
        final KeyValueWithHeaders<String, MethodSubmitPaymentBatchCommand> submitPaymentMessage = KeyValueWithHeaders.msg("", submitPayment,new RecordHeader(Message.CONTEXT_HEADER,"bacs".getBytes()));

        brokerClient.sendKeyValuesToDestination("commands",List.of(addPaymentMessage,submitPaymentMessage));

        List<EventWithCauseImpl> events = brokerClient.readAllFromDestination("events",EventWithCauseImpl.class);
        assertEquals(2,events.size());
        assertEquals(MethodPaymentBatchClosedEvent.class,events.get(0).getClass());
        assertEquals(MethodPaymentAcceptedEvent.class,events.get(1).getClass());

        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("customer.product/bacs:Read"))))
                .get()
                .uri("/state/"+payment.getKey())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.status").isEqualTo("Accepted");
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends BasePspCommandHandler {

        @Override
        protected PaymentMethod paymentMethod() {
            return PaymentMethod.Bacs;
        }

        @Bean
        public PaymentBatchEventGenerator paymentBatchEventGenerator(Metrics metrics
                , StatefulErrorHandler<PspAdapterPayment, EventWithCauseImpl> errorHandler){
            return PerPaymentBatchEventGenerator.builder()
                                                .eventGenerator(new PaymentBatchEventGeneratorDelegate(paymentMethod()))
                                                .paymentMethod(paymentMethod())
                                                .metrics(metrics)
                                                .errorHandler(errorHandler)
                                                .commandSerde(valueSerde(Command.class))
                                                .build();
        }

    }
}