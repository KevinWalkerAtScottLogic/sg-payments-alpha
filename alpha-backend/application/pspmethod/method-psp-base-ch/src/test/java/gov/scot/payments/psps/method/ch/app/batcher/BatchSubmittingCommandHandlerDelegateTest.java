package gov.scot.payments.psps.method.ch.app.batcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import gov.scot.payments.application.kafka.MessageInfo;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.psps.method.ch.app.PaymentSubmitter;
import gov.scot.payments.psps.method.ch.app.batcher.BatchSubmittingCommandHandlerDelegate;
import gov.scot.payments.psps.method.ch.app.batcher.kafka.BatchSubmittingTransformer;
import gov.scot.payments.psps.method.ch.app.batcher.kafka.BatchSubmittingTransformerSupplier;
import gov.scot.payments.psps.method.model.PspAdapterPayment;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentState;
import gov.scot.payments.psps.method.model.PspSubmissionFailureDetails;
import gov.scot.payments.psps.method.model.command.MethodSubmitPaymentBatchCommand;
import gov.scot.payments.psps.method.model.event.MethodPaymentAcceptedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentCancelSuccessEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentRejectedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentSubmissionFailedEvent;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.vavr.Tuple2;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.jackson.datatype.VavrModule;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.ValueAndTimestamp;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;
import org.springframework.kafka.support.JacksonUtils;
import org.springframework.kafka.support.converter.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.messaging.MessageHeaders;

import javax.money.CurrencyUnit;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

class BatchSubmittingCommandHandlerDelegateTest {

    private KafkaStreamsTestHarness harness;
    private BatchSubmittingCommandHandlerDelegate delegate;
    private Serde<Command> commandSerde;
    private Serde<PspAdapterPaymentState> stateSerde;
    private PaymentSubmitter submitter;

    @BeforeEach
    public void setUp(){
        ObjectMapper mapper = JacksonUtils.enhancedObjectMapper();
        mapper.activateDefaultTyping(
                BasicPolymorphicTypeValidator.builder()
                                             .allowIfBaseType(Command.class)
                                             .allowIfBaseType(Set.class)
                                             .allowIfBaseType(java.util.Map.class)
                                             .allowIfBaseType(List.class)
                                             .allowIfBaseType(CurrencyUnit.class)
                                             .build()
        );
        mapper.registerModule(new VavrModule());
        JsonSerde<Command> commandSerde = new JsonSerde<>(Command.class, mapper);
        DefaultJackson2JavaTypeMapper typeMapper = (DefaultJackson2JavaTypeMapper) ((JsonDeserializer<Command>) commandSerde.deserializer()).getTypeMapper();
        typeMapper.setClassIdFieldName(MessageHeaders.CONTENT_TYPE);
        final String mappingConfig = KafkaStreamsTestHarness.buildMappingsConfig(MethodSubmitPaymentBatchCommand.class, PspAdapterPaymentState.class, MethodPaymentAcceptedEvent.class, MethodPaymentRejectedEvent.class,MethodPaymentSubmissionFailedEvent.class);
        commandSerde.configure(HashMap.of(JsonSerializer.TYPE_MAPPINGS, mappingConfig).toJavaMap(), false);
        this.commandSerde = commandSerde;
        this.stateSerde = new JsonSerde<>(PspAdapterPaymentState.class, mapper);
        harness = KafkaStreamsTestHarness.builderWithMappings(Serdes.ByteArray(),new gov.scot.payments.testing.kafka.JsonSerde(),MethodSubmitPaymentBatchCommand.class, PspAdapterPaymentState.class,MethodPaymentAcceptedEvent.class, MethodPaymentRejectedEvent.class, MethodPaymentSubmissionFailedEvent.class)
                                         .build();
        harness.withKvStore(stateSerde);
        submitter = mock(PaymentSubmitter.class);
        BatchSubmittingTransformerSupplier batchSubmittingTransformerSupplier = new BatchSubmittingTransformerSupplier() {
            @Override
            public String[] getStateStores() {
                return new String[]{KafkaStreamsTestHarness.STATE_STORE};
            }

            @Override
            public ValueTransformer<MethodSubmitPaymentBatchCommand, Iterable<EventWithCauseImpl>> get() {
                return new BatchSubmittingTransformer(submitter,KafkaStreamsTestHarness.STATE_STORE,commandSerde,PaymentMethod.Bacs);
            }
        };
        BeanFactory beanFactory = mock(BeanFactory.class);
        StreamsBuilderFactoryBean streamsBuilderFactoryBean = mock(StreamsBuilderFactoryBean.class);
        when(beanFactory.getBean(anyString(), ArgumentMatchers.eq(StreamsBuilderFactoryBean.class))).thenReturn(streamsBuilderFactoryBean);
        delegate = BatchSubmittingCommandHandlerDelegate.builder()
                                                        .delegate( s -> s.mapValues(c -> MethodPaymentCancelSuccessEvent.builder().build()))
                .transformer(batchSubmittingTransformerSupplier)
                .beanFactory(beanFactory)
                                                         .build();
        delegate.apply(harness.stream()).to(KafkaStreamsTestHarness.OUTPUT_TOPIC);
    }

    @Test
    public void test(){
        var notPresent = makePayment();
        var incorrectState = makePayment();
        var validSuccess = makePayment();
        var validFail = makePayment();
        var validError = makePayment();
        var command = MethodSubmitPaymentBatchCommand.builder()
                                                     .key("")
                                                     .payments(List.of(notPresent.getId(),incorrectState.getId(),validSuccess.getId(),validFail.getId(),validError.getId()))
                                                     .build();
        final KeyValueWithHeaders<String, MethodSubmitPaymentBatchCommand> message = KeyValueWithHeaders.msg("1", command,"contentType",MethodSubmitPaymentBatchCommand.class.getName());
        try(TopologyTestDriver topology = harness.toTopology()){
            KeyValueStore<byte[], ValueAndTimestamp<PspAdapterPaymentState>> store =  topology.getTimestampedKeyValueStore(KafkaStreamsTestHarness.STATE_STORE);
            store.put(incorrectState.getKey().getBytes(),ValueAndTimestamp.make(PspAdapterPaymentState.builder().current(PspAdapterPayment.builder().payment(incorrectState).status(PspAdapterPayment.Status.Accepted).build()).currentVersion(1L).message(new SerializedMessage(new byte[0], new MessageInfo())).build(),0L));
            store.put(validSuccess.getKey().getBytes(),ValueAndTimestamp.make(PspAdapterPaymentState.builder().current(PspAdapterPayment.builder().payment(validSuccess).status(PspAdapterPayment.Status.Registered).build()).currentVersion(1L).message(new SerializedMessage(new byte[0], new MessageInfo())).build(),0L));
            store.put(validFail.getKey().getBytes(),ValueAndTimestamp.make(PspAdapterPaymentState.builder().current(PspAdapterPayment.builder().payment(validFail).status(PspAdapterPayment.Status.Registered).build()).currentVersion(1L).message(new SerializedMessage(new byte[0], new MessageInfo())).build(),0L));
            store.put(validError.getKey().getBytes(),ValueAndTimestamp.make(PspAdapterPaymentState.builder().current(PspAdapterPayment.builder().payment(validError).status(PspAdapterPayment.Status.Registered).build()).currentVersion(1L).message(new SerializedMessage(new byte[0], new MessageInfo())).build(),0L));

            final Tuple2<String, EventWithCauseImpl> successResponse = new Tuple2<>(validSuccess.getKey(),MethodPaymentAcceptedEvent.builder()
                                                                                                                                    .method(PaymentMethod.Bacs)
                                                                                                                                    .paymentId(validSuccess.getId())
                                                                                                                                    .psp("psp")
                                                                                                                                    .pspMetadata(new java.util.HashMap<>())
                                                                                                                                    .previousFailures(List.empty())
                                                                                                                                    .build() );
            final Tuple2<String, EventWithCauseImpl> errorResponse = new Tuple2<>(validError.getKey(), MethodPaymentSubmissionFailedEvent.builder()
                                                                                                                                              .method(PaymentMethod.Bacs)
                                                                                                                                              .paymentId(validError.getId())
                                                                                                                                              .details(List.of(PspSubmissionFailureDetails.builder().psp("psp").message("m").build()))
                                                                                                                                              .build());
            final Tuple2<String, EventWithCauseImpl> failResponse = new Tuple2<>(validFail.getKey(),MethodPaymentRejectedEvent.builder()
                                                                                                                                .method(PaymentMethod.Bacs)
                                                                                                                                .paymentId(validFail.getId())
                                                                                                                                .details(List.of(PspSubmissionFailureDetails.builder().psp("psp").message("m").build()))
                                                                                                                                .build() );
            when(submitter.submitPayments(any(), any())).thenReturn(List.of(successResponse, failResponse, errorResponse));
            harness.send(topology,command);
            var events = harness.drain(topology);
            assertEquals(3,events.size());
            assertEquals(MethodPaymentAcceptedEvent.class,events.get(0).getClass());
            assertEquals(MethodPaymentRejectedEvent.class,events.get(1).getClass());
            assertEquals(MethodPaymentSubmissionFailedEvent.class,events.get(2).getClass());

            assertEquals(PspAdapterPayment.Status.Accepted,store.get(incorrectState.getKey().getBytes()).value().getCurrent().getStatus());
            assertEquals(PspAdapterPayment.Status.Accepted,store.get(validSuccess.getKey().getBytes()).value().getCurrent().getStatus());
            assertEquals(PspAdapterPayment.Status.Rejected,store.get(validFail.getKey().getBytes()).value().getCurrent().getStatus());
            assertEquals(PspAdapterPayment.Status.Rejected,store.get(validError.getKey().getBytes()).value().getCurrent().getStatus());

            ArgumentCaptor<String> keyCaptor = ArgumentCaptor.forClass(String.class);
            ArgumentCaptor<List> captor = ArgumentCaptor.forClass(List.class);
            verify(submitter,times(1)).submitPayments(keyCaptor.capture(), captor.capture());
            List<Payment> submitted = captor.getValue();
            assertEquals(3,submitted.size());
            assertEquals(validSuccess.getId(),submitted.get(0).getId());
            assertEquals(validFail.getId(),submitted.get(1).getId());
            assertEquals(validError.getId(),submitted.get(2).getId());
            verifyNoMoreInteractions(submitter);
        }

    }


    private Payment makePayment() {
        return Payment.builder()
                      .createdBy("user")
                      .allowedMethods(List.of(PaymentMethod.Bacs))
                      .debtor(PartyIdentification.builder().name("").build())
                      .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                      .creditor(PartyIdentification.builder().name("").build())
                        .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                        .amount(Money.parse("GBP 1.00"))
                      .latestExecutionDate(Instant.now())
                      .product(new CompositeReference("",""))
                      .build();
    }

}