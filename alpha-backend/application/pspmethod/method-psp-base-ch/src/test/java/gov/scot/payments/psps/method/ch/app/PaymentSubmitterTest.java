package gov.scot.payments.psps.method.ch.app;

import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.psps.method.ch.app.PaymentSubmitter;
import gov.scot.payments.psps.method.ch.app.PspClient;
import gov.scot.payments.psps.method.ch.app.PspClientRegistry;
import gov.scot.payments.psps.method.model.PspSubmissionRequest;
import gov.scot.payments.psps.method.model.PspSubmissionResponse;
import gov.scot.payments.psps.method.model.PspTransactionCostEstimateResponse;
import gov.scot.payments.psps.method.model.event.MethodPaymentAcceptedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentRejectedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentSubmissionFailedEvent;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class PaymentSubmitterTest {

    private PaymentSubmitter submitter;
    private PspClientRegistry registry;

    @BeforeEach
    public void setUp(){
        registry = mock(PspClientRegistry.class);
        final MicrometerMetrics metrics = new MicrometerMetrics(new SimpleMeterRegistry());
        submitter = new PaymentSubmitter(metrics,registry, PaymentMethod.Bacs);
    }

    @Test
    void clientListingError() {
        var payment1 = makePayment();
        var payment2 = makePayment();
        doThrow(new RuntimeException()).when(registry).listPspClients();
        List<Tuple2<String,EventWithCauseImpl>> events = submitter.submitPayments("key", List.of(payment1,payment2));
        assertEquals(2,events.size());
        assertEquals(payment1.getId().toString(),events.get(0)._1);
        assertEquals(payment2.getId().toString(),events.get(1)._1);
        var event = (MethodPaymentSubmissionFailedEvent)events.get(0)._2;
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals(payment1.getId(),event.getPaymentId());
        event = (MethodPaymentSubmissionFailedEvent)events.get(1)._2;
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals(payment2.getId(),event.getPaymentId());
        verify(registry,times(1)).listPspClients();
        verifyNoMoreInteractions(registry);
    }

    @Test
    void noAvailableClientsError() {
        var payment1 = makePayment();
        var payment2 = makePayment();
        var client1 = mock(PspClient.class);
        var client2 = mock(PspClient.class);
        when(client1.getName()).thenReturn("1");
        when(client2.getName()).thenReturn("2");
        doThrow(new RuntimeException()).when(client1).estimateTransactionCosts(any());
        doThrow(new RuntimeException()).when(client2).estimateTransactionCosts(any());
        when(registry.listPspClients()).thenReturn(List.of(client1,client2));
        List<Tuple2<String,EventWithCauseImpl>> events = submitter.submitPayments("key", List.of(payment1,payment2));
        assertEquals(2,events.size());
        assertEquals(payment1.getId().toString(),events.get(0)._1);
        assertEquals(payment2.getId().toString(),events.get(1)._1);
        var event = (MethodPaymentSubmissionFailedEvent)events.get(0)._2;
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals(payment1.getId(),event.getPaymentId());
        event = (MethodPaymentSubmissionFailedEvent)events.get(1)._2;
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals(payment2.getId(),event.getPaymentId());
        verify(registry,times(1)).listPspClients();
        verify(client1,times(1)).estimateTransactionCosts(any());
        verify(client2,times(1)).estimateTransactionCosts(any());
        verify(client1,times(1)).getName();
        verify(client2,times(1)).getName();
        verifyNoMoreInteractions(registry,client1,client2);
    }

    @Test
    void errorThenFailed() {
        var payment1 = makePayment();
        var payment2 = makePayment();
        var client1 = mock(PspClient.class);
        var client2 = mock(PspClient.class);
        when(client1.getName()).thenReturn("1");
        when(client2.getName()).thenReturn("2");
        when(client1.estimateTransactionCosts(any())).thenReturn(PspTransactionCostEstimateResponse.builder().psp("1").amount(Money.parse("GBP 2.00")).build());
        when(client2.estimateTransactionCosts(any())).thenReturn(PspTransactionCostEstimateResponse.builder().psp("2").amount(Money.parse("GBP 1.00")).build());
        when(client1.submitPayments(any())).thenReturn(PspSubmissionResponse.builder().psp("1").code("c").error(false).message("m").success(false).build());
        doThrow(new RuntimeException()).when(client2).submitPayments(any());
        when(registry.listPspClients()).thenReturn(List.of(client1,client2));
        List<Tuple2<String,EventWithCauseImpl>> events = submitter.submitPayments("key", List.of(payment1,payment2));
        assertEquals(2,events.size());
        assertEquals(payment1.getId().toString(),events.get(0)._1);
        assertEquals(payment2.getId().toString(),events.get(1)._1);
        var event = (MethodPaymentRejectedEvent)events.get(0)._2;
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals(payment1.getId(),event.getPaymentId());
        event = (MethodPaymentRejectedEvent)events.get(1)._2;
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals(payment2.getId(),event.getPaymentId());
        verify(registry,times(1)).listPspClients();
        verify(client1,times(1)).estimateTransactionCosts(any());
        verify(client2,times(1)).estimateTransactionCosts(any());
        verify(client1,times(1)).submitPayments(any());
        verify(client2,times(1)).submitPayments(any());
        verify(client1,atLeastOnce()).getName();
        verify(client2,atLeastOnce()).getName();
        verifyNoMoreInteractions(registry,client1,client2);
    }

    @Test
    void errorThenSuccess() {
        var payment1 = makePayment();
        var payment2 = makePayment();
        var client1 = mock(PspClient.class);
        var client2 = mock(PspClient.class);
        when(client1.getName()).thenReturn("1");
        when(client2.getName()).thenReturn("2");
        when(client1.estimateTransactionCosts(any())).thenReturn(PspTransactionCostEstimateResponse.builder().psp("1").amount(Money.parse("GBP 2.00")).build());
        when(client2.estimateTransactionCosts(any())).thenReturn(PspTransactionCostEstimateResponse.builder().psp("2").amount(Money.parse("GBP 1.00")).build());
        when(client1.submitPayments(any())).thenReturn(PspSubmissionResponse.builder().psp("1").error(false).success(true).build());
        doThrow(new RuntimeException()).when(client2).submitPayments(any());
        when(registry.listPspClients()).thenReturn(List.of(client1,client2));
        List<Tuple2<String,EventWithCauseImpl>> events = submitter.submitPayments("key", List.of(payment1,payment2));
        assertEquals(2,events.size());
        assertEquals(payment1.getId().toString(),events.get(0)._1);
        assertEquals(payment2.getId().toString(),events.get(1)._1);
        var event = (MethodPaymentAcceptedEvent)events.get(0)._2;
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals(payment1.getId(),event.getPaymentId());
        event = (MethodPaymentAcceptedEvent)events.get(1)._2;
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals(payment2.getId(),event.getPaymentId());
        verify(registry,times(1)).listPspClients();
        verify(client1,times(1)).estimateTransactionCosts(any());
        verify(client2,times(1)).estimateTransactionCosts(any());
        verify(client1,times(1)).submitPayments(any());
        verify(client2,times(1)).submitPayments(any());
        verify(client1,atLeastOnce()).getName();
        verify(client2,atLeastOnce()).getName();
        verifyNoMoreInteractions(registry,client1,client2);
    }

    @Test
    void notAllPaymensReady() {
        var payment1 = makePayment();
        var payment2 = makePayment().toBuilder().status(PaymentStatus.Rejected).submissions(List.of(Submission.builder().status(Submission.Status.Failure).message("hello").build())).build();
        var client1 = mock(PspClient.class);
        when(client1.getName()).thenReturn("1");
        when(client1.estimateTransactionCosts(any())).thenReturn(PspTransactionCostEstimateResponse.builder().psp("1").amount(Money.parse("GBP 2.00")).build());
        when(client1.submitPayments(any())).thenReturn(PspSubmissionResponse.builder().psp("1").error(false).success(true).build());
        when(registry.listPspClients()).thenReturn(List.of(client1));
        ArgumentCaptor<PspSubmissionRequest> captor = ArgumentCaptor.forClass(PspSubmissionRequest.class);
        List<Tuple2<String,EventWithCauseImpl>> events = submitter.submitPayments("key", List.of(payment1,payment2));
        assertEquals(2,events.size());
        assertEquals(payment1.getId().toString(),events.get(0)._1);
        assertEquals(payment2.getId().toString(),events.get(1)._1);
        var event = (MethodPaymentAcceptedEvent)events.get(0)._2;
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals(payment1.getId(),event.getPaymentId());
        var failedEvent = (MethodPaymentSubmissionFailedEvent)events.get(1)._2;
        assertEquals(PaymentMethod.Bacs,failedEvent.getMethod());
        assertEquals(payment2.getId(),failedEvent.getPaymentId());
        assertEquals("hello",failedEvent.getDetails().head().getMessage());
        verify(registry,times(1)).listPspClients();
        verify(client1,times(1)).estimateTransactionCosts(any());
        verify(client1,times(1)).submitPayments(captor.capture());
        assertEquals(1,captor.getValue().getPayments().size());
        verify(client1,atLeastOnce()).getName();
        verifyNoMoreInteractions(registry,client1);
    }

    private Payment makePayment() {
        return Payment.builder()
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.Bacs))
                      .debtor(PartyIdentification.builder().name("").build())
                      .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                      .creditor(PartyIdentification.builder().name("").build())
                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .amount(Money.parse("GBP 1.00"))
                .latestExecutionDate(Instant.now())
                .status(PaymentStatus.ReadyForSubmission)
                .product(new CompositeReference("",""))
                .build();
    }
}