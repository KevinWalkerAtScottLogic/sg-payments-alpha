package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentBatch;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import io.vavr.collection.List;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.state.Stores;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;

import java.util.Comparator;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class CountWindowingPaymentBatcherTest extends PaymentBatcherTest {

    private TransformerWindowingPaymentBatcher batcher;

    @BeforeEach
    public void setUpBatcher() throws Exception {
        batcher = PaymentBatchers.count(KafkaStreamsTestHarness.STATE_STORE, Stores.inMemoryKeyValueStore(KafkaStreamsTestHarness.STATE_STORE),batchSerde,3)
                                 .groupingFunction(PaymentBatchers.groupedByProduct(paymentSerde))
                                 .build();
        BeanFactory beanFactory = mock(BeanFactory.class);
        StreamsBuilderFactoryBean factory = mock(StreamsBuilderFactoryBean.class);
        when(beanFactory.getBean(anyString(),eq(StreamsBuilderFactoryBean.class))).thenReturn(factory);
        when(factory.getObject()).thenReturn(harness.getBuilder());
        batcher.setBeanFactory(beanFactory);
        batcher.batch(harness.stream()).to("batches");
    }

    @Test
    public void testBasicBatching(){
        try (final TopologyTestDriver topology = harness.toTopology()) {
            sendPayments(topology,new CompositeReference("customer","product"),1,5);
            sendPayments(topology,new CompositeReference("customer","product"),6,3);
            List<PspAdapterPaymentBatch> batches = getAndVerifyBatches(topology,2,6);
            verifyBatch(batches.get(0),3,15,pi -> pi.getProduct().getComponent1().equals("product"));
            verifyBatch(batches.get(1),3,6,pi -> pi.getProduct().getComponent1().equals("product"));
        }

    }

    @Test
    public void testGroupedBatching(){
        try (final TopologyTestDriver topology = harness.toTopology()) {
            sendPayments(topology,new CompositeReference("customer","product1"),1,3);
            sendPayments(topology,new CompositeReference("customer","product2"),6,3);
            sendPayments(topology,new CompositeReference("customer","product3"),9,2);
            List<PspAdapterPaymentBatch> batches = getAndVerifyBatches(topology,2,6)
                    .sorted(Comparator.comparing(pib -> pib.getEarliestPayment().get().getProduct().getComponent1()));
            verifyBatch(batches.get(0),3,6,pi -> pi.getProduct().getComponent1().equals("product1"));
            verifyBatch(batches.get(1),3,21,pi -> pi.getProduct().getComponent1().equals("product2"));
        }

    }

}
