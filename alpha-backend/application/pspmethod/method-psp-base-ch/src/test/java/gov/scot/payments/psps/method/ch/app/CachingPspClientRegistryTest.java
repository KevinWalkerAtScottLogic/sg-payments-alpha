package gov.scot.payments.psps.method.ch.app;

import gov.scot.payments.psps.method.ch.app.CachingPspClientRegistry;
import gov.scot.payments.psps.method.ch.app.PspClient;
import gov.scot.payments.psps.method.ch.app.PspClientRegistry;
import io.vavr.collection.List;
import io.vavr.control.Option;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CachingPspClientRegistryTest {

    private CachingPspClientRegistry registry;
    private PspClientRegistry delegate;

    @BeforeEach
    public void setUp(){
        delegate = mock(PspClientRegistry.class);
        registry = new CachingPspClientRegistry(delegate, Duration.ofSeconds(1));
    }

    @Test
    void listPspClients() {
        PspClient client = mock(PspClient.class);
        when(delegate.listPspClients()).thenReturn(List.of(client));
        List<PspClient> clients = registry.listPspClients();
        assertEquals(1,clients.size());
        assertEquals(client,clients.get(0));
        clients = registry.listPspClients();
        verify(delegate,times(1)).listPspClients();
    }

    @Test
    void getClient() throws InterruptedException {
        PspClient client = mock(PspClient.class);
        when(client.getName()).thenReturn("psp");
        when(delegate.listPspClients()).thenReturn(List.of(client));
        Option<PspClient> clients = registry.getClient("psp");
        assertTrue(clients.isDefined());
        assertEquals(client,clients.get());
        Thread.sleep(2000L);
        clients = registry.getClient("psp1");
        verify(delegate,times(2)).listPspClients();
    }
}