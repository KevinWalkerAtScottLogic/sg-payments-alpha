package @group@.ch.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.component.commandhandler.stateful.EventGeneratorFunction;
import gov.scot.payments.application.component.commandhandler.stateful.StateUpdateFunction;
import gov.scot.payments.application.component.commandhandler.stateful.StatefulCommandHandlerApplication;
import gov.scot.payments.model.CommandFailureInfo;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.user.Subject;
import org.springframework.context.annotation.Bean;
import @group@.@namePackage@.model.@projectName.capitalize()@;

@ApplicationComponent
public class @paymentMethod.capitalize()@MethodCHApplication extends BasePspCommandHandler{

    @Override
    protected PaymentMethod paymentMethod() {
        return PaymentMethod.@paymentMethod.capitalize()@;
    }

    @Bean
    public PaymentBatchEventGenerator paymentBatchEventGenerator(Metrics metrics
            , StatefulErrorHandler<PspAdapterPayment, EventWithCauseImpl> errorHandler){
        //switch for batchingeventgenerator if batching is desired
        return PerPaymentBatchEventGenerator.builder()
                .eventGenerator(new PaymentBatchEventGeneratorDelegate(paymentMethod()))
                .paymentMethod(paymentMethod())
                .metrics(metrics)
                .errorHandler(errorHandler)
                .commandSerde(valueSerde(Command.class))
                .build();
    }

    public static void main(String[] args){
        BaseApplication.run(@paymentMethod.capitalize()@MethodCHApplication.class,args);
    }

}