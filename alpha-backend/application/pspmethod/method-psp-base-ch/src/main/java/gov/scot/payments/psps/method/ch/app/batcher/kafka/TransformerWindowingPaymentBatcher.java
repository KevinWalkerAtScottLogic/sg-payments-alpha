package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentBatch;
import lombok.Setter;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KGroupedStream;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;

import java.util.function.Supplier;

public class TransformerWindowingPaymentBatcher extends PaymentBatcher implements BeanFactoryAware {

    @Setter private BeanFactory beanFactory;

    private final Supplier<BaseTransformer> transformer;
    private final String stateStoreName;
    private final KeyValueBytesStoreSupplier store;
    private final Serde<PspAdapterPaymentBatch> valueSerde;

    public TransformerWindowingPaymentBatcher(BatchGrouping groupingFunction
            , Supplier<BaseTransformer> transformer
            , String stateStore
            , KeyValueBytesStoreSupplier store
            , Serde<PspAdapterPaymentBatch> valueSerde) {
        super(groupingFunction);
        this.transformer = transformer;
        this.stateStoreName = stateStore;
        this.store = store;
        this.valueSerde = valueSerde;
    }

    @Override
    protected WindowedKStream applyWindowing(KGroupedStream<byte[], Payment> stream) {
        return null;
    }

    @Override
    public KStream<byte[], PspAdapterPaymentBatch> batch(KStream<byte[], Payment> stream){
        registerStateStore(stateStoreName);
        return stream
                .peek( (k,v) -> log.info("handling payment {}",v.getId()))
                .map( (k,v) -> {
                    byte[] key = groupingFunction.apply(v);
                    return new KeyValue<>(key == null ? "NULL_KEY".getBytes() : key,v);
                })
                .transform(transformer::get,stateStoreName)
                .map( (k,b) -> new KeyValue<>(b.getId().toString().getBytes(),b))
                .peek( (k,v) -> log.info("Emitting batch {} of {} payments",v,v.size()));
    }

    private void registerStateStore(final String stateStoreName){
        try {
            String factoryName = String.format("&stream-builder-%s","handle");
            StreamsBuilder streamsBuilder = beanFactory.getBean(factoryName, StreamsBuilderFactoryBean.class).getObject();
            log.info("Registering state store: {} for stream factory: {}",stateStoreName,factoryName);
            StoreBuilder builder = Stores.keyValueStoreBuilder(store, Serdes.ByteArray(), valueSerde);
            streamsBuilder.addStateStore(builder);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static TransformerWindowingPaymentInstructionBatcherBuilder builder(){
        return new TransformerWindowingPaymentInstructionBatcherBuilder();
    }

    public static class TransformerWindowingPaymentInstructionBatcherBuilder
            extends PaymentInstructionBatcherBuilder<TransformerWindowingPaymentBatcher, TransformerWindowingPaymentInstructionBatcherBuilder> {

        private Supplier<BaseTransformer> transformer;
        private String stateStore;
        private KeyValueBytesStoreSupplier store;
        private Serde<PspAdapterPaymentBatch> valueSerde;

        TransformerWindowingPaymentInstructionBatcherBuilder() {
        }

        @Override
        public TransformerWindowingPaymentBatcher build() {
            return new TransformerWindowingPaymentBatcher(groupingFunction,transformer,stateStore,store,valueSerde);
        }

        public TransformerWindowingPaymentInstructionBatcherBuilder transformer(String stateStore, KeyValueBytesStoreSupplier store, Serde<PspAdapterPaymentBatch> valueSerde,Supplier<BaseTransformer> transformer) {
            this.transformer = transformer;
            this.stateStore = stateStore;
            this.store = store;
            this.valueSerde = valueSerde;
            return this;
        }
    }
}
