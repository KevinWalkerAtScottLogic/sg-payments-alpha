package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentBatch;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.SessionWindows;
import org.apache.kafka.streams.state.SessionStore;

import java.time.Duration;

@Value
@EqualsAndHashCode(callSuper = true)
public class BatchSessionWindowing extends BatchOperation{

    private SessionWindows window;
    private Duration inactivity;
    private Materialized<byte[], PspAdapterPaymentBatch, SessionStore<Bytes, byte[]>> materialized;

    public BatchSessionWindowing(Materialized<byte[], PspAdapterPaymentBatch, SessionStore<Bytes, byte[]>> materialized, Duration inactivity) {
        this.inactivity = inactivity;
        this.materialized = materialized;
        this.window = SessionWindows.with(inactivity).grace(inactivity);
    }

    /*
    public Materialized getMaterialized(){
        return Materialized.with(keySerde, valueSerde)
                .withLoggingDisabled()
                .withCachingDisabled()
                .withRetention(inactivity.plus(inactivity));
    }

     */
}
