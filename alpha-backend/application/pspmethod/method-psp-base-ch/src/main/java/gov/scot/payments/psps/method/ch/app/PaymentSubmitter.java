package gov.scot.payments.psps.method.ch.app;

import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.payments.model.aggregate.PaymentStatus;
import gov.scot.payments.payments.model.aggregate.Submission;
import gov.scot.payments.psps.method.model.PspSubmissionFailureDetails;
import gov.scot.payments.psps.method.model.PspSubmissionRequest;
import gov.scot.payments.psps.method.model.PspSubmissionResponse;
import gov.scot.payments.psps.method.model.PspTransactionCostEstimateResponse;
import gov.scot.payments.psps.method.model.event.*;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Try;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
@Builder
public class PaymentSubmitter {

    @NonNull private final Metrics metrics;
    @NonNull private final PspClientRegistry clientRegistry;
    @NonNull private final PaymentMethod method;

    public List<Tuple2<String,EventWithCauseImpl>> submitPayments(final String key, final List<Payment> payments) {
        Tuple2<List<Payment>,List<Payment>> partitionedPayments = payments.partition(p -> p.getStatus() == PaymentStatus.ReadyForSubmission);
        //only submit payments in ready to submit
        final PspSubmissionRequest request = new PspSubmissionRequest(java.util.Map.of(PspAdapterPaymentBatch.BATCH_ID_KEY, key), partitionedPayments._1);
        List<PspSubmissionResponse> responses =  submitPaymentsInternal(request);
        List<Tuple2<String,EventWithCauseImpl>> events = request.getPayments()
                .map(p -> new Tuple2<>(p.getId().toString(),convertSubmissionToEvent(p,responses)));

        //take any payments not ready to submit and return errors based on the submissions
        events = events.appendAll(partitionedPayments._2.map(p -> new Tuple2<>(p.getId().toString(),MethodPaymentSubmissionFailedEvent.builder()
                .method(method)
                .paymentId(p.getId())
                .details(p.getSubmissions().map(this::submissionToFailureDetails))
                .build())));
        return events;
    }

    private PspSubmissionFailureDetails submissionToFailureDetails(Submission submission) {
        return PspSubmissionFailureDetails.builder()
                .message(submission.getMessage())
                .error(true)
                .build();
    }

    private EventWithCauseImpl convertSubmissionToEvent(Payment p, List<PspSubmissionResponse> responses){
        if(responses.isEmpty()){
            return MethodPaymentSubmissionFailedEvent.builder()
                                             .method(method)
                                             .paymentId(p.getId())
                                             .details(List.of(PspSubmissionFailureDetails.builder().error(true).message("No available PSP's").build()))
                                             .build();
        }
        if(responses.last().isSuccess()){
            PspSubmissionResponse success = responses.last();
            return MethodPaymentAcceptedEvent.builder()
                                             .correlationId(p.getId())
                                             .method(method)
                                             .paymentId(p.getId())
                                             .psp(success.getPsp())
                                             .pspMetadata(success.getMetadata())
                                             .previousFailures(responses.map(r -> r.toFailureDetails()).filter(Objects::nonNull))
                                             .build();
        } else {
            if(responses.forAll(r -> r.isError())){
                return MethodPaymentSubmissionFailedEvent.builder()
                        .method(method)
                        .paymentId(p.getId())
                        .details(responses.map(r -> r.toFailureDetails()).filter(Objects::nonNull))
                        .build();
            } else{
                return MethodPaymentRejectedEvent.builder()
                        .method(method)
                        .paymentId(p.getId())
                        .details(responses.map(r -> r.toFailureDetails()).filter(Objects::nonNull))
                        .build();
            }

        }
    }

    private List<PspSubmissionResponse> submitPaymentsInternal(PspSubmissionRequest request){
        log.debug("Handling payment request: {}",request);
        Map<String, PspClient> clients;
        try{
            clients = clientRegistry
                    .listPspClients()
                    .toMap(c -> new Tuple2<>(c.getName(),c));
        } catch (Throwable e){
            return List.of(PspSubmissionResponse.fromThrowable(e));
        }

        List<PspTransactionCostEstimateResponse> estimatedCosts = clients
                .values()
                .toList()
                .map(c -> Try.ofSupplier(() -> c.estimateTransactionCosts(request)).getOrNull())
                .filter(Objects::nonNull)
                .sorted();

        List<PspSubmissionResponse> responses = List.empty();
        for(int i=0;i<estimatedCosts.size();i++){
            PspClient client = clients.get(estimatedCosts.get(i).getPsp()).get();
            PspSubmissionResponse response = Try.ofSupplier(metrics.time("payment.submit",() -> client.submitPayments(request)))
                                                .recover(e -> {
                                                    log.warn("Handling payment submit error",e);
                                                    metrics.increment("payment.submit.error");
                                                    return PspSubmissionResponse.fromThrowable(client.getName(),e);
                                                }).get();
            responses = responses.append(response);
            if(response.isSuccess() || (!response.isSuccess() && !response.isError())){
                break;
            }
        }
        return responses;
    }
}
