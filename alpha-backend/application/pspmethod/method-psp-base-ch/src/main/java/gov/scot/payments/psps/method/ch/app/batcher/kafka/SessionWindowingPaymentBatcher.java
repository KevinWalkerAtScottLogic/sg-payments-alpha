package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import gov.scot.payments.payments.model.aggregate.Payment;
import org.apache.kafka.streams.kstream.KGroupedStream;
import org.apache.kafka.streams.kstream.SessionWindowedKStream;

public class SessionWindowingPaymentBatcher extends PaymentBatcher {

    private final BatchSessionWindowing window;

    public SessionWindowingPaymentBatcher(BatchGrouping groupingFunction
            , BatchSessionWindowing window) {
        super(groupingFunction);
        this.window = window;
    }

    @Override
    protected WindowedKStream applyWindowing(KGroupedStream<byte[], Payment> stream) {
        SessionWindowedKStream<byte[], Payment> windowed = stream.windowedBy(window.getWindow());
        return (i,a,m) -> windowed.aggregate(i,a,m,window.getMaterialized());
    }

    public static SessionWindowingPaymentInstructionBatcherBuilder builder(){
        return new SessionWindowingPaymentInstructionBatcherBuilder();
    }

    public static class SessionWindowingPaymentInstructionBatcherBuilder
            extends PaymentInstructionBatcherBuilder<SessionWindowingPaymentBatcher,SessionWindowingPaymentInstructionBatcherBuilder> {

        private BatchSessionWindowing window;

        SessionWindowingPaymentInstructionBatcherBuilder() {
        }

        @Override
        public SessionWindowingPaymentBatcher build() {
            return new SessionWindowingPaymentBatcher(groupingFunction,window);
        }

        public SessionWindowingPaymentInstructionBatcherBuilder window(BatchSessionWindowing window) {
            this.window = window;
            return this;
        }
    }

}
