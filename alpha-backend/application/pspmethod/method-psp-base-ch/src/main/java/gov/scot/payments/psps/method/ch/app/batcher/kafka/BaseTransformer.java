package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentBatch;
import io.vavr.control.Option;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseTransformer implements Transformer<byte[], Payment, KeyValue<byte[], PspAdapterPaymentBatch>> {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    protected ProcessorContext processorContext;
    protected KeyValueStore<byte[],PspAdapterPaymentBatch> currentBatches;

    private final String stateStoreName;

    protected BaseTransformer(String stateStoreName){
        this.stateStoreName = stateStoreName;
    }

    @Override
    public void init(ProcessorContext processorContext) {
        this.processorContext = processorContext;
        this.currentBatches = (KeyValueStore<byte[],PspAdapterPaymentBatch>) processorContext.getStateStore(stateStoreName);
    }

    @Override
    public final synchronized KeyValue<byte[], PspAdapterPaymentBatch> transform(byte[] key, Payment value) {
        PspAdapterPaymentBatch batch = Option.of(currentBatches.get(key))
                .orElse(Option.of(new PspAdapterPaymentBatch()))
                .map(b -> b.addPayment(value))
                .peek(b -> log.info("key: {}, Adding payment {} to batch {}",new String(key),value.getId(),b))
                .peek(b -> currentBatches.put(key,b))
                .get();
        handleBatch(key,batch,processorContext,currentBatches);
        return null;
    }

    protected void handleBatch(byte[] key, PspAdapterPaymentBatch batch, ProcessorContext context, KeyValueStore<byte[],PspAdapterPaymentBatch> currentBatches){

    }

    @Override
    public final void close() {

    }
}
