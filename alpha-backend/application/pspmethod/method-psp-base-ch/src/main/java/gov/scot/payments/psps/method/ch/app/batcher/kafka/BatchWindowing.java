package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentBatch;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.Windows;
import org.apache.kafka.streams.kstream.internals.TimeWindow;
import org.apache.kafka.streams.state.WindowStore;

import java.time.Duration;

@Value
@EqualsAndHashCode(callSuper = true)
public class BatchWindowing extends BatchOperation{

    private Windows<TimeWindow> window;
    private Duration windowPeriod;
    private Materialized<byte[], PspAdapterPaymentBatch, WindowStore<Bytes, byte[]>> materialized;

    public BatchWindowing(Materialized<byte[], PspAdapterPaymentBatch, WindowStore<Bytes, byte[]>> materialized, Duration windowPeriod) {
        this.windowPeriod = windowPeriod;
        this.materialized = materialized;
        this.window = TimeWindows.of(windowPeriod).grace(Duration.ofSeconds(0));
    }

    /*
    public Materialized getMaterialized(){
        return Materialized.with(keySerde, valueSerde)
                .withLoggingDisabled()
                .withCachingDisabled()
                .withRetention(windowPeriod.plusSeconds(1));
    }
     */
}
