package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import gov.scot.payments.payments.model.aggregate.Payment;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.kstream.Grouped;

import java.util.function.Function;

@Value
@EqualsAndHashCode(callSuper = true)
public class BatchGrouping extends BatchOperation implements Function<Payment,byte[]>{

    private final Serde<byte[]> keySerde;
    private final Serde<Payment> valueSerde;
    private Function<Payment,String> function;

    @Override
    public byte[] apply(Payment paymentInstruction) {
        final String key = function.apply(paymentInstruction);
        return key == null ? null : key.getBytes();
    }

    public Grouped<byte[], Payment> getGrouped() {
        return Grouped.with(keySerde, valueSerde);
    }
}
