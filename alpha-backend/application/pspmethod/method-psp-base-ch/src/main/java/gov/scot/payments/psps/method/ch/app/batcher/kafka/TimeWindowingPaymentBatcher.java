package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import gov.scot.payments.payments.model.aggregate.Payment;
import org.apache.kafka.streams.kstream.KGroupedStream;
import org.apache.kafka.streams.kstream.TimeWindowedKStream;

public class TimeWindowingPaymentBatcher extends PaymentBatcher {

    private final BatchWindowing window;

    public TimeWindowingPaymentBatcher(BatchGrouping groupingFunction
            , BatchWindowing window) {
        super(groupingFunction);
        this.window = window;
    }

    @Override
    protected WindowedKStream applyWindowing(KGroupedStream<byte[], Payment> stream) {
        TimeWindowedKStream<byte[], Payment> windowed = stream.windowedBy(window.getWindow());
        return (i,a,m) -> windowed.aggregate(i, a,window.getMaterialized());
    }

    public static TimeWindowingPaymentInstructionBatcherBuilder builder(){
        return new TimeWindowingPaymentInstructionBatcherBuilder();
    }

    public static class TimeWindowingPaymentInstructionBatcherBuilder
            extends PaymentInstructionBatcherBuilder<TimeWindowingPaymentBatcher,TimeWindowingPaymentInstructionBatcherBuilder> {

        private BatchWindowing window;

        TimeWindowingPaymentInstructionBatcherBuilder() {
        }

        @Override
        public TimeWindowingPaymentBatcher build() {
            return new TimeWindowingPaymentBatcher(groupingFunction,window);
        }

        public TimeWindowingPaymentInstructionBatcherBuilder window(BatchWindowing window) {
            this.window = window;
            return this;
        }
    }
}
