package gov.scot.payments.psps.method.ch.app.batcher;

import gov.scot.payments.application.component.commandhandler.stateful.EventGeneratorFunction;
import gov.scot.payments.application.component.commandhandler.stateful.StateTableToEventStreamFunction;
import gov.scot.payments.application.component.commandhandler.stateful.StatefulErrorHandler;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.AccessDeniedEvent;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasState;
import gov.scot.payments.model.OptimisticLockFailureEvent;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentState;
import gov.scot.payments.psps.method.model.PspAdapterPayment;
import gov.scot.payments.psps.method.model.command.MethodAddPaymentToBatchCommand;
import gov.scot.payments.psps.method.model.event.MethodPaymentBatchClosedEvent;
import io.vavr.collection.List;
import io.vavr.control.Try;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Predicate;

import static gov.scot.payments.application.component.commandhandler.stateful.AggregateState.failure;
import static io.vavr.Predicates.instanceOf;

@Slf4j
@SuperBuilder
public abstract class PaymentBatchEventGenerator implements StateTableToEventStreamFunction<PspAdapterPaymentState, EventWithCauseImpl> {

    @NonNull private PaymentMethod paymentMethod;
    @NonNull protected final Serde<Command> commandSerde;
    @NonNull private final Metrics metrics;
    @NonNull private final StatefulErrorHandler<PspAdapterPayment,EventWithCauseImpl> errorHandler;
    @NonNull private final EventGeneratorFunction<PspAdapterPaymentState, EventWithCauseImpl> eventGenerator;

    protected abstract KStream<byte[], MethodPaymentBatchClosedEvent> applyBatching(KStream<byte[], PspAdapterPaymentState> stateEvents);

    @Override
    public KStream<byte[], EventWithCauseImpl> apply(KTable<byte[], PspAdapterPaymentState> state) {
        Predicate<byte[],PspAdapterPaymentState> submitPredicate = (k,v) -> {
            Command command = v.getMessage().deseralize(commandSerde);
            return MethodAddPaymentToBatchCommand.class.isAssignableFrom(command.getClass()) && v.getPrevious() == null;
        };
        KStream<byte[], PspAdapterPaymentState>[] branches =  state.toStream().branch(submitPredicate,(k,v) -> true);
        KStream<byte[], MethodPaymentBatchClosedEvent> batches = applyBatching(branches[0]);
        KStream<byte[], EventWithCauseImpl> delegated = branches[1]
                .flatMapValues(this::generateEvents);
        return delegated.merge((KStream)batches);
    }

    private Iterable<EventWithCauseImpl> generateEvents(PspAdapterPaymentState state) {
        Command command = state.getMessage().deseralize(commandSerde);
        log.debug("Generating events from state {}",state);
        if(state.isAccessDenied()){
            return List.of( AccessDeniedEvent.from(command,state.getError().getErrorMessage()));
        } if(state.isLockFail()){
            return List.of( OptimisticLockFailureEvent.from(command,state.getError().getErrorMessage()));
        } else{
            return Try.ofSupplier(metrics.time("events.generate",() -> eventGenerator.apply(command, state)))
                      .recover(e -> {
                          log.warn("Handling error generating events",e);
                          metrics.increment("events.generate.error");
                          return errorHandler.apply(command,state.getPrevious(),e);
                      })
                      .peek(l -> l.forEach(e -> e.setCauseDetails(command)))
                      .peek(l -> l.filter(instanceOf(HasState.class)).map(e -> (HasState)e).forEach(e -> e.setCarriedState(state.getCurrent(),state.getCurrentVersion())))
                      .get();
        }

    }


}
