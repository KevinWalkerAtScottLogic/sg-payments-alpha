package gov.scot.payments.psps.method.ch.app.batcher;

import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentState;
import gov.scot.payments.psps.method.ch.app.batcher.kafka.PaymentBatcher;
import gov.scot.payments.psps.method.model.event.MethodPaymentBatchClosedEvent;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;

import java.util.UUID;

@SuperBuilder
public class BatchingPaymentBatchEventGenerator extends PaymentBatchEventGenerator {

    @NonNull private final PaymentBatcher batcher;

    @Override
    protected KStream<byte[], MethodPaymentBatchClosedEvent> applyBatching(KStream<byte[], PspAdapterPaymentState> stateEvents) {
        return batcher.batch(stateEvents.mapValues(s -> s.getCurrent().getPayment()))
                      .map( (k,v) -> {
                          String key = v.getId().toString();
                          MethodPaymentBatchClosedEvent batch = MethodPaymentBatchClosedEvent.builder()
                                                                                             .key(key)
                                                                                             .correlationId(UUID.randomUUID())
                                                                                             .payments(v.getPayments().map(Payment::getId))
                                                       .build();
                          return new KeyValue<>(key.getBytes(),batch);
                      });
    }
}
