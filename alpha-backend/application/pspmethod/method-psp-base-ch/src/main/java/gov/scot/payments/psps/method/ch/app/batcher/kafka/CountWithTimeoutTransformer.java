package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentBatch;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.state.KeyValueIterator;

import java.time.Duration;
import java.time.Instant;

public class CountWithTimeoutTransformer extends CountTransformer{

    private final Duration inactivityPeriod;
    private final Duration resolution;
    private final boolean useEarliestPayment;

    public CountWithTimeoutTransformer(String stateStoreName
            ,Duration inactivityPeriod
            , int maxCount
            , Duration resolution
            , boolean useEarliestPayment) {
        super(stateStoreName,maxCount);
        this.inactivityPeriod = inactivityPeriod;
        this.resolution = resolution;
        this.useEarliestPayment = useEarliestPayment;
    }

    @Override
    public void init(ProcessorContext processorContext) {
        super.init(processorContext);
        if(inactivityPeriod != null){
            processorContext.schedule(resolution, PunctuationType.WALL_CLOCK_TIME, this::doPunctuate);
        }
    }

    private synchronized void doPunctuate(long timestamp) {
        Instant currentTime = Instant.ofEpochMilli(timestamp);
        KeyValueIterator<byte[], PspAdapterPaymentBatch> iterator = currentBatches.all();
        Map<byte[], PspAdapterPaymentBatch> toEmit = HashMap.empty();
        while (iterator.hasNext()) {
            KeyValue<byte[], PspAdapterPaymentBatch> keyValue = iterator.next();
            Option<Payment> paymentToUse = useEarliestPayment ? keyValue.value.getEarliestPayment() : keyValue.value.getLatestPayment();
            Instant timeToUse = paymentToUse
                    .map(Payment::getCreatedAt)
                    .getOrElse(Instant.MAX);
            if(Duration.between(timeToUse,currentTime).compareTo(inactivityPeriod) >= 0){
                toEmit = toEmit.put(keyValue.key, keyValue.value);
            }
        }
        toEmit.forEach((k,v) -> {
            processorContext.forward(k, v);
            currentBatches.delete(k);
        });
    }

}
