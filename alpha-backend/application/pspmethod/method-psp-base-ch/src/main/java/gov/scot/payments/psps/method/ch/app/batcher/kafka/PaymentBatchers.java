package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentBatch;
import io.vavr.control.Option;
import lombok.experimental.UtilityClass;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.SessionStore;
import org.apache.kafka.streams.state.WindowStore;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.support.CronTrigger;

import java.time.Duration;

@UtilityClass
public class PaymentBatchers {

    public BatchGrouping groupedByProduct(Serde<Payment> valueSerde){
        return new BatchGrouping(Serdes.ByteArray(), valueSerde,p -> p.getProduct().toString());
    }

    public BatchGrouping noGrouping(Serde<Payment> valueSerde){
        return new BatchGrouping(Serdes.ByteArray(),valueSerde,pi -> "DEFAULT_GROUP");
    }

    public BatchGrouping groupedByBatchId(Serde<Payment> valueSerde){
        return new BatchGrouping(Serdes.ByteArray(),valueSerde,pi -> Option.of(pi.getBatchId()).getOrElse("NO_BATCH"));
    }

    public SessionWindowingPaymentBatcher.SessionWindowingPaymentInstructionBatcherBuilder session(Duration inactivity, Materialized<byte[], PspAdapterPaymentBatch, SessionStore<Bytes, byte[]>> materialized){
        BatchSessionWindowing window = new BatchSessionWindowing(materialized,inactivity);
        return SessionWindowingPaymentBatcher.builder()
                .window(window);
    }

    public TimeWindowingPaymentBatcher.TimeWindowingPaymentInstructionBatcherBuilder time(Duration windowPeriod, Materialized<byte[], PspAdapterPaymentBatch, WindowStore<Bytes, byte[]>> materialized){
        BatchWindowing window = new BatchWindowing(materialized,windowPeriod);
        return TimeWindowingPaymentBatcher.builder()
                .window(window);
    }

    public TransformerWindowingPaymentBatcher.TransformerWindowingPaymentInstructionBatcherBuilder countWithTimeout(String stateStore, KeyValueBytesStoreSupplier storeSupplier, Serde<PspAdapterPaymentBatch> valueSerde, int size, Duration maxPaymentAge, Duration resolution, boolean useEarliestPaymentInBatch){
        return TransformerWindowingPaymentBatcher.builder()
                .transformer(stateStore,storeSupplier,valueSerde,() -> new CountWithTimeoutTransformer(stateStore,maxPaymentAge,size,resolution,useEarliestPaymentInBatch));
    }

    public TransformerWindowingPaymentBatcher.TransformerWindowingPaymentInstructionBatcherBuilder countWithTimeout(String stateStore, KeyValueBytesStoreSupplier storeSupplier,Serde<PspAdapterPaymentBatch> valueSerde, int size, Duration maxPaymentAge){
        return countWithTimeout(stateStore,storeSupplier,valueSerde,size, maxPaymentAge, Duration.ofMinutes(1),false);
    }


    public TransformerWindowingPaymentBatcher.TransformerWindowingPaymentInstructionBatcherBuilder count(String stateStore, KeyValueBytesStoreSupplier storeSupplier,Serde<PspAdapterPaymentBatch> valueSerde, int size){
        return TransformerWindowingPaymentBatcher.builder()
                .transformer(stateStore,storeSupplier,valueSerde,() -> new CountTransformer(stateStore,size));
    }

    public TransformerWindowingPaymentBatcher.TransformerWindowingPaymentInstructionBatcherBuilder periodic(String stateStore, KeyValueBytesStoreSupplier storeSupplier,Serde<PspAdapterPaymentBatch> valueSerde, Duration frequency){
        return TransformerWindowingPaymentBatcher.builder()
                .transformer(stateStore,storeSupplier,valueSerde,() -> new PeriodicTransformer(stateStore,frequency));
    }

    public TransformerWindowingPaymentBatcher.TransformerWindowingPaymentInstructionBatcherBuilder periodicWithLimit(String stateStore, KeyValueBytesStoreSupplier storeSupplier,Serde<PspAdapterPaymentBatch> valueSerde, int limit, Duration frequency){
        return TransformerWindowingPaymentBatcher.builder()
                .transformer(stateStore,storeSupplier,valueSerde,() -> new PeriodicWithCountTransformer(stateStore,frequency,limit));
    }

    public TransformerWindowingPaymentBatcher.TransformerWindowingPaymentInstructionBatcherBuilder scheduled(String stateStore, KeyValueBytesStoreSupplier storeSupplier,Serde<PspAdapterPaymentBatch> valueSerde, String cron){
        Trigger trigger = new CronTrigger(cron);
        return TransformerWindowingPaymentBatcher.builder()
                .transformer(stateStore,storeSupplier,valueSerde,() -> new ScheduledTransformer(stateStore,trigger));
    }

}
