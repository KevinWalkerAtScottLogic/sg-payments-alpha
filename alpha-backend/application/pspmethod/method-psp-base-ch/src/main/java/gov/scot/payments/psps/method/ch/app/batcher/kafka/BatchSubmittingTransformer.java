package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import gov.scot.payments.application.kafka.MessageInfo;
import gov.scot.payments.application.kafka.MessageWithInfo;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.psps.method.ch.app.PaymentSubmitter;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentState;
import gov.scot.payments.psps.method.model.PspAdapterPayment;
import gov.scot.payments.psps.method.model.PspSubmissionFailureDetails;
import gov.scot.payments.psps.method.model.command.MethodSubmitPaymentBatchCommand;
import gov.scot.payments.psps.method.model.event.MethodPaymentAcceptedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentRejectedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentSubmissionFailedEvent;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.control.Option;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.TimestampedKeyValueStore;
import org.apache.kafka.streams.state.ValueAndTimestamp;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@SuperBuilder
@RequiredArgsConstructor
public class BatchSubmittingTransformer implements ValueTransformer<MethodSubmitPaymentBatchCommand, Iterable<EventWithCauseImpl>> {

    private ProcessorContext processorContext;
    private TimestampedKeyValueStore<byte[], PspAdapterPaymentState> paymentStore;

    private final PaymentSubmitter submitter;
    private final String storeName;
    private final Serde<Command> commandSerde;
    private final PaymentMethod paymentMethod;

    @Override
    public void init(final ProcessorContext context) {
        this.processorContext = context;
        paymentStore = (TimestampedKeyValueStore<byte[], PspAdapterPaymentState>)processorContext.getStateStore(storeName);

    }

    @Override
    public List<EventWithCauseImpl> transform(final MethodSubmitPaymentBatchCommand value) {
        MessageWithInfo message = new MessageWithInfo<>(value, MessageInfo.fromHeaders(processorContext.headers()));
        SerializedMessage serializedCommand = message.serialize(commandSerde);

        List<Payment> payments = getPayments(value);
        return Try.ofCallable(() -> submitter.submitPayments(value.getKey(), payments))
                .getOrElseGet(e -> payments.map(p -> new Tuple2<>(p.getKey(), MethodPaymentSubmissionFailedEvent.builder()
                        .method(paymentMethod)
                        .paymentId(p.getId())
                        .details(List.of(PspSubmissionFailureDetails.builder().error(true).message(e.getMessage()).build()))
                        .build()))
                )
                .peek(t -> t._2.setCauseDetails(value))
                .map(e -> {
                    byte[] key = e._1.getBytes();
                    PspAdapterPaymentState currentState = paymentStore.get(key).value();
                    final PspAdapterPayment currentPayment = currentState.getCurrent();
                    final Long currentVersion = currentState.getCurrentVersion();

                    PspAdapterPayment updated = updateState(currentPayment,e._2);
                    PspAdapterPaymentState state = new PspAdapterPaymentState(serializedCommand,null, currentPayment, updated, currentVersion == null ? 1 : currentVersion +1);
                    paymentStore.put(key,ValueAndTimestamp.make(state,value.getTimestamp().toEpochMilli()));
                    return e._2;
                });
    }

    protected List<Payment> getPayments(MethodSubmitPaymentBatchCommand value) {
        return value.getPayments()
                    .map(u -> u.toString().getBytes())
                    .map(k -> ValueAndTimestamp.getValueOrNull(paymentStore.get(k)))
                    .filter(Objects::nonNull)
                    .map(s -> s.getCurrent())
                    .filter(p -> p.getStatus() == PspAdapterPayment.Status.Registered && p.getCancelStatus() == PspAdapterPayment.CancelStatus.Live)
                    .map(p -> p.getPayment());
    }

    private PspAdapterPayment updateState(final PspAdapterPayment currentState, final EventWithCauseImpl event) {

        PspAdapterPayment.Status status = PspAdapterPayment.Status.Registered;
        String psp = null;
        Map<String,String> pspMetadata = new HashMap<>();
        if(event instanceof MethodPaymentAcceptedEvent){
            final MethodPaymentAcceptedEvent accepted = (MethodPaymentAcceptedEvent) event;
            status = PspAdapterPayment.Status.Accepted;
            psp = accepted.getPsp();
            pspMetadata = accepted.getPspMetadata();
        } else if(event instanceof MethodPaymentRejectedEvent){
            final MethodPaymentRejectedEvent rejected = (MethodPaymentRejectedEvent) event;
            status = PspAdapterPayment.Status.Rejected;
            psp = rejected.getDetails().lastOption().map(f -> f.getPsp()).getOrNull();
            pspMetadata = rejected.getDetails().lastOption().flatMap(f -> Option.of(f.getPspMetadata())).getOrElse(new HashMap<>());
        } else if(event instanceof MethodPaymentSubmissionFailedEvent){
            final MethodPaymentSubmissionFailedEvent failed = (MethodPaymentSubmissionFailedEvent) event;
            status = PspAdapterPayment.Status.Rejected;
            psp = failed.getDetails().lastOption().map(f -> f.getPsp()).getOrNull();
            pspMetadata = failed.getDetails().lastOption().flatMap(f -> Option.of(f.getPspMetadata())).getOrElse(new HashMap<>());
        }
        Map<String,String> metadata = currentState.getPspMetadata() == null ? new HashMap<>() : new HashMap<>(currentState.getPspMetadata());
        metadata.putAll(pspMetadata);
        return currentState
                .toBuilder()
                .status(status)
                .routedTo(psp)
                .pspMetadata(metadata)
                .build();
    }

    @Override
    public void close() {

    }
}
