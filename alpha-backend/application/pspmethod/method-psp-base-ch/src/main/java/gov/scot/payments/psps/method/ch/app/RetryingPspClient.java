package gov.scot.payments.psps.method.ch.app;

import gov.scot.payments.psps.method.model.PspCancelRequest;
import gov.scot.payments.psps.method.model.PspCancelResponse;
import gov.scot.payments.psps.method.model.PspSubmissionRequest;
import gov.scot.payments.psps.method.model.PspSubmissionResponse;
import gov.scot.payments.psps.method.model.PspTransactionCostEstimateResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.retry.RetryOperations;

@RequiredArgsConstructor
public class RetryingPspClient implements PspClient{

    private final RetryOperations retryTemplate;
    private final PspClient delegate;

    public PspSubmissionResponse submitPayments(final PspSubmissionRequest request) {
        return retryTemplate.execute(c -> delegate.submitPayments(request));
    }

    public PspTransactionCostEstimateResponse estimateTransactionCosts(final PspSubmissionRequest request) {
        return retryTemplate.execute(c -> delegate.estimateTransactionCosts(request));
    }

    @Override
    public PspCancelResponse cancelPayment(final PspCancelRequest request) {
        return retryTemplate.execute(c -> delegate.cancelPayment(request));
    }

    @Override
    public String getName() {
        return delegate.getName();
    }
}
