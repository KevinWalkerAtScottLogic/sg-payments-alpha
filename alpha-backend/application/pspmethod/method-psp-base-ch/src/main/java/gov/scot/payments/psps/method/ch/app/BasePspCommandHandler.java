package gov.scot.payments.psps.method.ch.app;

import gov.scot.payments.application.component.commandhandler.CommandHandlerDelegate;
import gov.scot.payments.application.component.commandhandler.stateful.*;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.CommandFailureInfo;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.user.Scope;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.psps.method.ch.app.batcher.BatchSubmittingCommandHandlerDelegate;
import gov.scot.payments.psps.method.ch.app.batcher.kafka.BatchSubmittingTransformer;
import gov.scot.payments.psps.method.ch.app.batcher.kafka.BatchSubmittingTransformerSupplier;
import gov.scot.payments.psps.method.model.PspAdapterPayment;
import gov.scot.payments.psps.method.model.command.MethodSubmitPaymentBatchCommand;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.kstream.ValueTransformerSupplier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.function.context.config.ContextFunctionCatalogAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.util.ResourceUtils;

import java.time.Duration;

@EnableDiscoveryClient
@EnableAutoConfiguration(exclude = ContextFunctionCatalogAutoConfiguration.class)
@PropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "application-method-psp-ch.properties")
public abstract class BasePspCommandHandler extends StatefulCommandHandlerApplication<PspAdapterPayment, PspAdapterPaymentState, EventWithCauseImpl> {

    //PspClientFactory
    //paymentbatcheventgenerator

    protected abstract PaymentMethod paymentMethod();

    @Override
    protected Class<PspAdapterPaymentState> stateClass() {
        return PspAdapterPaymentState.class;
    }

    @Override
    protected Class<PspAdapterPayment> stateEntityClass() {
        return PspAdapterPayment.class;
    }

    @Override
    protected PspAdapterPaymentState createNewAggregateInstance(SerializedMessage command, CommandFailureInfo error, PspAdapterPayment previous, PspAdapterPayment current, Long currentVersion) {
        return new PspAdapterPaymentState(command, error, previous, current, currentVersion);
    }

    @Override
    protected Scope extractScope(PspAdapterPaymentState state) {
        return state.getCurrent().getPayment().getProduct().toScope();
    }

    @Bean
    public PaymentSubmitter paymentSubmitter(Metrics metrics, PspClientRegistry clientRegistry){
        return PaymentSubmitter.builder()
                               .clientRegistry(clientRegistry)
                               .method(paymentMethod())
                               .metrics(metrics)
                               .build();
    }

    @Bean
    public BatchSubmittingTransformerSupplier batchingTransformer(PaymentSubmitter submitter
            , PerCommandStatefulCommandHandlerDelegate<PspAdapterPayment,PspAdapterPaymentState,EventWithCauseImpl> delegate){
        return new BatchSubmittingTransformerSupplier() {
            @Override
            public String[] getStateStores() {
                return new String[]{delegate.getStoreName()};
            }

            @Override
            public ValueTransformer<MethodSubmitPaymentBatchCommand, Iterable<EventWithCauseImpl>> get() {
                return new BatchSubmittingTransformer(submitter,delegate.getStoreName(),valueSerde(Command.class),paymentMethod());
            }
        };
    }

    @Bean
    @Primary
    public CommandHandlerDelegate<EventWithCauseImpl> batchFilteringCommandHandlerDelegate(PerCommandStatefulCommandHandlerDelegate<PspAdapterPayment,PspAdapterPaymentState,EventWithCauseImpl> delegate
                    , BatchSubmittingTransformerSupplier transformer){
        return BatchSubmittingCommandHandlerDelegate.builder()
                                                     .delegate(delegate)
                                                     .transformer(transformer)
                                                     .build();
    }

    @Bean
    public StateUpdateFunction<PspAdapterPaymentState, PspAdapterPayment> stateUpdateFunction(PspClientRegistry pspClientRegistry, Metrics metrics) {
        return new PspAdapterStateUpdater(metrics,pspClientRegistry);
    }

    @Bean
    public PspClientRegistry pspClientRegistry(PspClientFactory clientFactory
            , DiscoveryClient discoveryClient
            , @Value("${psp.cache.timeout:PT1M}") Duration cacheTimeout){
        return new CachingPspClientRegistry(new DiscoveryClientPspClientRegistry(discoveryClient,clientFactory),cacheTimeout);
    }


}