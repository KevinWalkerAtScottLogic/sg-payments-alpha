package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentBatch;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueStore;


public class CountTransformer extends BaseTransformer{

    private final int maxCount;

    public CountTransformer(String stateStoreName,int maxCount) {
        super(stateStoreName);
        this.maxCount = maxCount;
    }

    @Override
    protected void handleBatch(byte[] key, PspAdapterPaymentBatch batch, ProcessorContext processorContext, KeyValueStore<byte[],PspAdapterPaymentBatch> currentBatches) {
        if (batch.getPayments().size() >= maxCount) {
            processorContext.forward(key,batch);
            currentBatches.delete(key);
        }
    }
}
