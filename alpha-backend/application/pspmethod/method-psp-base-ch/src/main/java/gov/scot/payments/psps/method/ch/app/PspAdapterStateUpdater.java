package gov.scot.payments.psps.method.ch.app;

import gov.scot.payments.application.component.commandhandler.stateful.StateUpdateFunction;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Command;
import gov.scot.payments.psps.method.model.PspAdapterPayment;
import gov.scot.payments.psps.method.model.PspCancelRequest;
import gov.scot.payments.psps.method.model.PspCancelResponse;
import gov.scot.payments.psps.method.model.command.MethodAddPaymentToBatchCommand;
import gov.scot.payments.psps.method.model.command.MethodCancelPaymentCommand;
import gov.scot.payments.psps.method.model.command.MethodCompletePaymentCancelCommand;
import gov.scot.payments.psps.method.model.command.MethodCompletePaymentCommand;
import gov.scot.payments.psps.method.model.command.MethodFailPaymentCancelCommand;
import gov.scot.payments.psps.method.model.command.MethodReturnPaymentCommand;
import io.vavr.control.Option;
import io.vavr.control.Try;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
@Builder
@Slf4j
public class PspAdapterStateUpdater extends StateUpdateFunction.PatternMatching<PspAdapterPaymentState, PspAdapterPayment> {

    @NonNull private final Metrics metrics;
    @NonNull private final PspClientRegistry clientRegistry;

    @Override
    protected void handlers(PatternMatcher.Builder2<Command, PspAdapterPaymentState, PspAdapterPayment> builder) {
        builder.match2(MethodAddPaymentToBatchCommand.class,(c, s) -> handleAddToBatch(c,s.getCurrent()))
               .match2(MethodReturnPaymentCommand.class,(c, s) -> handleReturn(c,s.getCurrent()))
               .match2(MethodCompletePaymentCommand.class,(c, s) -> handleComplete(c,s.getCurrent()))
               .match2(MethodCompletePaymentCancelCommand.class,(c, s) -> handleCompleteCancel(c,s.getCurrent()))
               .match2(MethodFailPaymentCancelCommand.class,(c, s) -> handleFailCancel(c,s.getCurrent()))
               .match2(MethodCancelPaymentCommand.class,(c, s) -> handleCancel(c,s.getCurrent()));
    }

    private PspAdapterPayment handleCancel(final MethodCancelPaymentCommand c, final PspAdapterPayment current) {
        if(current == null){
            throw new IllegalStateException(String.format("payment: %s not registered",c.getPaymentId()));
        }
        if(current.getStatus() == PspAdapterPayment.Status.Registered){
            return current.toBuilder()
                          .cancelStatus(PspAdapterPayment.CancelStatus.Complete)
                          .build();
        } else if(current.getStatus() == PspAdapterPayment.Status.Accepted){
            PspCancelResponse response = cancelPayment(new PspCancelRequest(current.getPayment().getId(),current.getPspMetadata() == null ? new HashMap<>() : current.getPspMetadata(),current.getRoutedTo()));
            Map<String,String> currentMetadata = current.getPspMetadata();
            currentMetadata = currentMetadata != null ? new HashMap<>(currentMetadata) : new HashMap<>();
            if(response.getMetadata() != null){
                currentMetadata.putAll(response.getMetadata());
            }
            return current.toBuilder()
                          .cancelStatus(response.isSuccess() ? PspAdapterPayment.CancelStatus.Complete : PspAdapterPayment.CancelStatus.Failed)
                          .cancelErrorCode(response.getCode())
                          .cancelErrorMessage(response.getMessage())
                          .pspMetadata(currentMetadata)
                          .build();
        } else{
            return current;
        }
    }

    private PspAdapterPayment handleFailCancel(final MethodFailPaymentCancelCommand c, final PspAdapterPayment current) {
        if(current == null){
            throw new IllegalStateException(String.format("payment: %s not registered",c.getPaymentId()));
        }
        Map<String,String> currentMetadata = current.getPspMetadata();
        currentMetadata = currentMetadata != null ? new HashMap<>(currentMetadata) : new HashMap<>();
        currentMetadata.putAll(c.getPspMetadata());
        return current.toBuilder()
                      .cancelStatus(PspAdapterPayment.CancelStatus.Failed)
                      .pspMetadata(currentMetadata)
                      .build();
    }

    private PspAdapterPayment handleCompleteCancel(final MethodCompletePaymentCancelCommand c, final PspAdapterPayment current) {
        if(current == null){
            throw new IllegalStateException(String.format("payment: %s not registered",c.getPaymentId()));
        }
        Map<String,String> currentMetadata = current.getPspMetadata();
        currentMetadata = currentMetadata != null ? new HashMap<>(currentMetadata) : new HashMap<>();
        currentMetadata.putAll(c.getPspMetadata());
        return current.toBuilder()
                      .pspMetadata(currentMetadata)
                      .cancelStatus(PspAdapterPayment.CancelStatus.Complete)
                      .build();
    }

    private PspAdapterPayment handleComplete(final MethodCompletePaymentCommand c, final PspAdapterPayment current) {
        if(current == null){
            throw new IllegalStateException(String.format("payment: %s not registered",c.getPaymentId()));
        }
        Map<String,String> currentMetadata = current.getPspMetadata();
        currentMetadata = currentMetadata != null ? new HashMap<>(currentMetadata) : new HashMap<>();
        currentMetadata.putAll(c.getPspMetadata());
        return current.toBuilder()
                      .status(PspAdapterPayment.Status.Complete)
                      .routedTo(c.getPsp())
                      .pspMetadata(currentMetadata)
                      .build();
    }

    private PspAdapterPayment handleReturn(final MethodReturnPaymentCommand c, final PspAdapterPayment current) {
        if(current == null){
            throw new IllegalStateException(String.format("payment: %s not registered",c.getPaymentId()));
        }
        Map<String,String> currentMetadata = current.getPspMetadata();
        currentMetadata = currentMetadata != null ? new HashMap<>(currentMetadata) : new HashMap<>();
        currentMetadata.putAll(c.getPspMetadata());
        return current.toBuilder()
                                .status(PspAdapterPayment.Status.Returned)
                                .routedTo(c.getPsp())
                                .pspMetadata(currentMetadata)
                                .build();
    }

    private PspAdapterPayment handleAddToBatch(final MethodAddPaymentToBatchCommand c, final PspAdapterPayment current) {
        if(current != null){
            throw new IllegalStateException(String.format("payment: %s already registered",c.getPayment().getId()));
        }
        return PspAdapterPayment.builder()
                                .payment(c.getPayment())
                                .build();
    }

    private PspCancelResponse cancelPayment(PspCancelRequest request){
        Option<PspClient> client = clientRegistry.getClient(request.getPsp());
        if(client.isDefined()){
            PspClient pspClient = client.get();
            return Try.ofSupplier(metrics.time("payment.cancel",() -> pspClient.cancelPayment(request)))
                                            .recover(e -> {
                                                log.warn("Handling payment cancel error",e);
                                                metrics.increment("payment.cancel.error");
                                                return PspCancelResponse.fromThrowable(pspClient.getName(),request.getPayment(),e);
                                            }).get();
        } else{
            return PspCancelResponse.builder()
                                    .paymentId(request.getPayment())
                                    .success(false)
                                    .psp(request.getPsp())
                                    .message(String.format("PSP: %s not found, can not cancel payment",request.getPsp()))
                                    .build();
        }
    }

}
