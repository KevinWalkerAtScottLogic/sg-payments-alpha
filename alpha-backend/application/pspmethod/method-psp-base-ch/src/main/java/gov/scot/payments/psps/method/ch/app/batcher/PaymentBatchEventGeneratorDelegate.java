package gov.scot.payments.psps.method.ch.app.batcher;

import gov.scot.payments.application.component.commandhandler.stateful.EventGeneratorFunction;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentState;
import gov.scot.payments.psps.method.model.PspAdapterPayment;
import gov.scot.payments.psps.method.model.command.*;
import gov.scot.payments.psps.method.model.event.*;
import io.vavr.collection.List;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static gov.scot.payments.application.component.commandhandler.stateful.AggregateState.failure;

@RequiredArgsConstructor
public class PaymentBatchEventGeneratorDelegate extends EventGeneratorFunction.PatternMatching<PspAdapterPaymentState, EventWithCauseImpl> {

    @NonNull private final PaymentMethod paymentMethod;

    @Override
    protected void handlers(final PatternMatcher.Builder2<Command, PspAdapterPaymentState, List<EventWithCauseImpl>> builder) {
        builder.match2(c -> true,failure(),(c,s) -> List.of())
                .match2(MethodReturnPaymentCommand.class,(c, s) -> List.of(handleReturn(c,s)))
                .match2(MethodCompletePaymentCommand.class,(c, s) -> List.of(handleComplete(c,s)))
                .match2(MethodCompletePaymentCancelCommand.class,(c, s) -> List.of(handleCancelSuccess(c,s)))
                .match2(MethodFailPaymentCancelCommand.class,(c, s) -> List.of(handleCancelFail(c,s)))
                .match2(MethodCancelPaymentCommand.class,(c, s) -> handleCancel(c,s))
                .orElse(List.empty());
    }

    private MethodPaymentCancelFailedEvent handleCancelFail(MethodFailPaymentCancelCommand c, final PspAdapterPaymentState s) {
        return MethodPaymentCancelFailedEvent.builder()
                                             .code(c.getCode())
                                             .message(c.getMessage())
                                             .psp(c.getPsp())
                                             .pspMetadata(c.getPspMetadata())
                                             .method(c.getMethod())
                                             .paymentId(c.getPaymentId())
                                             .build();
    }

    private MethodPaymentCancelSuccessEvent handleCancelSuccess(MethodCompletePaymentCancelCommand c, final PspAdapterPaymentState s) {
        return MethodPaymentCancelSuccessEvent.builder()
                                              .psp(c.getPsp())
                                              .pspMetadata(c.getPspMetadata())
                                              .method(c.getMethod())
                                              .paymentId(c.getPaymentId())
                                              .build();
    }

    private MethodPaymentCompleteEvent handleComplete(MethodCompletePaymentCommand c, final PspAdapterPaymentState s) {
        return MethodPaymentCompleteEvent.builder()
                                         .psp(c.getPsp())
                                         .pspMetadata(c.getPspMetadata())
                                         .method(c.getMethod())
                                         .paymentId(c.getPaymentId())
                                         .build();
    }

    private MethodPaymentReturnedEvent handleReturn(MethodReturnPaymentCommand c, final PspAdapterPaymentState s) {
        return MethodPaymentReturnedEvent.builder()
                                         .code(c.getCode())
                                         .message(c.getMessage())
                                         .psp(c.getPsp())
                                         .pspMetadata(c.getPspMetadata())
                                         .method(c.getMethod())
                                         .paymentId(c.getPaymentId())
                                         .amount(c.getAmount())
                                         .build();
    }

    private List<EventWithCauseImpl> handleCancel(final MethodCancelPaymentCommand c, final PspAdapterPaymentState s) {
        if(s.getCurrent().getStatus() == PspAdapterPayment.Status.Registered
                || s.getCurrent().getCancelStatus() == PspAdapterPayment.CancelStatus.Complete){
            return List.of(MethodPaymentCancelSuccessEvent.builder()
                                                  .psp(s.getCurrent().getRoutedTo())
                                                  .pspMetadata(s.getCurrent().getPspMetadata())
                                                  .method(paymentMethod)
                                                  .paymentId(c.getPaymentId())
                                                  .build());
        } else if(s.getCurrent().getCancelStatus() == PspAdapterPayment.CancelStatus.Failed){
            return List.of(MethodPaymentCancelFailedEvent.builder()
                                                 .code(s.getCurrent().getCancelErrorCode())
                                                 .message(s.getCurrent().getCancelErrorMessage())
                                                 .psp(s.getCurrent().getRoutedTo())
                                                 .pspMetadata(s.getCurrent().getPspMetadata())
                                                 .method(paymentMethod)
                                                 .paymentId(c.getPaymentId())
                                                 .build());
        }
        return List.empty();
    }


}