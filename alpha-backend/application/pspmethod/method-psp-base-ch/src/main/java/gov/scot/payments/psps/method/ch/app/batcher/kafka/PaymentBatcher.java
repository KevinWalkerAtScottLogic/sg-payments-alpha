package gov.scot.payments.psps.method.ch.app.batcher.kafka;

import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentBatch;
import org.apache.kafka.streams.kstream.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class PaymentBatcher {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    protected abstract WindowedKStream applyWindowing(KGroupedStream<byte[],Payment> stream);

    protected final BatchGrouping groupingFunction;

    protected PaymentBatcher(BatchGrouping groupingFunction) {
        this.groupingFunction = groupingFunction;
    }

    public KStream<byte[], PspAdapterPaymentBatch> batch(KStream<byte[], Payment> stream){
        KGroupedStream<byte[],Payment> grouped = applyGrouping(stream);
        WindowedKStream windowed = applyWindowing(grouped);
        Suppressed<Windowed> suppressed = Suppressed.untilWindowCloses(Suppressed.BufferConfig.unbounded())/*.withName(environment+"-"+gatewayName+"-batch-topic")*/;
        return windowed.aggregate(PspAdapterPaymentBatch::new, this::aggregate, this::merge)
                .suppress(suppressed)
                .toStream( (k,b) -> b == null ? null : b.getId().toString().getBytes())
                .filterNot( (k,v) -> v == null)
                .peek( (k,v) -> log.info("Emitting batch {} of {} payments",v,v.size()));
    }

    protected KGroupedStream<byte[], Payment> applyGrouping(KStream<?, Payment> paymentInstructionStream) {
        return paymentInstructionStream
                .peek( (k,v) -> log.debug("handling payment {}",v))
                .groupBy( (k,v) -> groupingFunction.apply(v),groupingFunction.getGrouped());
    }

    private PspAdapterPaymentBatch aggregate(byte[] key, Payment payment, PspAdapterPaymentBatch batch) {
        log.info("Adding payment {} to existing batch {}",payment.getId(),batch);
        return batch.addPayment(payment);
    }

    private PspAdapterPaymentBatch merge(byte[] k, PspAdapterPaymentBatch v1, PspAdapterPaymentBatch v2) {
        log.info("Merging batches {} and {}",v1,v2);
        return v1.merge(v2);
    }

    interface WindowedKStream {
        KTable<Windowed<byte[]>, PspAdapterPaymentBatch> aggregate(final Initializer<PspAdapterPaymentBatch> initializer,
                                                               final Aggregator<byte[], Payment, PspAdapterPaymentBatch> aggregator,
                                                               final Merger<byte[], PspAdapterPaymentBatch> sessionMerger);
    }

    public static abstract class PaymentInstructionBatcherBuilder<T extends PaymentBatcher,B extends PaymentInstructionBatcherBuilder> {
        protected BatchGrouping groupingFunction;

        PaymentInstructionBatcherBuilder() {
        }

        public B groupingFunction(BatchGrouping groupingFunction) {
            this.groupingFunction = groupingFunction;
            return (B)this;
        }

        public abstract T build();

    }


}
