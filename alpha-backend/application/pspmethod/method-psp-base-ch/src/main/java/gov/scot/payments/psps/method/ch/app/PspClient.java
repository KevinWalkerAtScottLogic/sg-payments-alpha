package gov.scot.payments.psps.method.ch.app;

import gov.scot.payments.psps.method.model.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.reactive.function.client.WebClient;

import java.net.URL;

public interface PspClient {

    PspSubmissionResponse submitPayments(PspSubmissionRequest request);
    PspTransactionCostEstimateResponse estimateTransactionCosts(PspSubmissionRequest request);
    PspCancelResponse cancelPayment(PspCancelRequest request);
    String getName();

}
