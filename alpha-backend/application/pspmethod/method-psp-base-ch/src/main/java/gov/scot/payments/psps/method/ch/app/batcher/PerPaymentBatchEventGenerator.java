package gov.scot.payments.psps.method.ch.app.batcher;

import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentState;
import gov.scot.payments.psps.method.model.event.MethodPaymentBatchClosedEvent;
import io.vavr.collection.List;
import lombok.experimental.SuperBuilder;
import org.apache.kafka.streams.kstream.KStream;

import java.util.UUID;

@SuperBuilder
public class PerPaymentBatchEventGenerator extends PaymentBatchEventGenerator {

    @Override
    protected KStream<byte[], MethodPaymentBatchClosedEvent> applyBatching(KStream<byte[], PspAdapterPaymentState> stateEvents) {
        return stateEvents
                .mapValues( v -> {
                    final UUID id = v.getCurrent().getPayment().getId();
                    return MethodPaymentBatchClosedEvent.builder()
                                                        .key(id.toString())
                                                        .payments(List.of(id))
                                                        .correlationId(UUID.randomUUID())
                                                        .build();
                });
    }
}
