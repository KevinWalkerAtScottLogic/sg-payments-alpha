package gov.scot.payments.psps.method.model;

import gov.scot.payments.model.Event;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasCause;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.psps.method.model.event.MethodPaymentAcceptedEvent;
import io.vavr.collection.List;
import lombok.*;

import java.util.Map;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder(toBuilder = true)
public class PspSubmissionResponse {

    private String psp;
    private boolean success;
    private boolean error;
    private String code;
    private String message;
    @Builder.Default private Map<String,String> metadata = Map.of();

    public static PspSubmissionResponse fromThrowable(final Throwable e) {
        return PspSubmissionResponse.builder()
                .message(e.getMessage() == null ? "" : null)
                .success(false)
                .error(true)
                .build();
    }

    public static PspSubmissionResponse fromThrowable(String psp, final Throwable e) {
        return PspSubmissionResponse.builder()
                .message(e.getMessage() == null ? "" : null)
                .success(false)
                .error(true)
                .psp(psp)
                .build();
    }

    public PspSubmissionFailureDetails toFailureDetails() {
        if(success){
            return null;
        }
        return PspSubmissionFailureDetails.builder()
                .psp(psp)
                .code(code)
                .message(message)
                .pspMetadata(metadata)
                .error(error)
                .build();
    }
}
