package gov.scot.payments.psps.method.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.psps.method.model.PspSubmissionFailureDetails;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.javamoney.moneta.Money;

import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "pspmethod", type = "paymentBatchClosed")
@NoArgsConstructor
@RequiredArgsConstructor
public class MethodPaymentBatchClosedEvent extends EventWithCauseImpl implements HasKey<String> {

    @NonNull private String key;
    @NonNull private List<UUID> payments;

    @Override
    @JsonIgnore
    public String getKey() {
        return key;
    }
}
