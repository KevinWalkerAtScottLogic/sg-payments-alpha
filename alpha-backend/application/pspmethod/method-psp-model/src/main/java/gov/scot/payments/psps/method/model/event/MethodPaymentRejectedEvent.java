package gov.scot.payments.psps.method.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.psps.method.model.PspSubmissionFailureDetails;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "pspmethod", type = "paymentRejected")
@NoArgsConstructor
@RequiredArgsConstructor
public class MethodPaymentRejectedEvent extends EventWithCauseImpl implements HasKey<String> {

    @NonNull private UUID paymentId;
    @NonNull private PaymentMethod method;
    @NonNull private List<PspSubmissionFailureDetails> details;
    
    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }
}
