package gov.scot.payments.psps.method.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

import java.util.Map;
import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "pspmethod", type = "completePayment")
@NoArgsConstructor
public class MethodCompletePaymentCommand extends CommandImpl implements HasKey<String> {

    @NonNull private UUID paymentId;
    @NonNull private PaymentMethod method;
    @NonNull private String psp;
    @NonNull private Map<String,String> pspMetadata;

    public MethodCompletePaymentCommand(UUID paymentId, boolean reply) {
        super(reply);
        this.paymentId = paymentId;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }
}