package gov.scot.payments.psps.method.model;

import lombok.*;
import org.apache.avro.reflect.Nullable;

import java.time.Instant;
import java.util.Map;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder(toBuilder = true)
public class PspSubmissionFailureDetails {

    @Builder.Default @NonNull private Instant timestamp = Instant.now();
    @Nullable private String psp;
    @NonNull private String message;
    @Nullable private String code;
    @Nullable private Map<String,String> pspMetadata;
    private boolean error;
}
