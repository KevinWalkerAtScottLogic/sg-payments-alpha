package gov.scot.payments.psps.method.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;
import org.javamoney.moneta.Money;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder(toBuilder = true)
public class PspCompleteResponse {

    @NonNull private UUID paymentId;
    @NonNull @Builder.Default private Map<String,String> metadata = new HashMap<>();
}
