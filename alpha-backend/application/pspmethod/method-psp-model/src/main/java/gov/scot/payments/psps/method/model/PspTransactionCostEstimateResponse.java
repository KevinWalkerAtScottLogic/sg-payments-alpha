package gov.scot.payments.psps.method.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gov.scot.payments.payments.model.aggregate.MoneyDeserializer;
import gov.scot.payments.payments.model.aggregate.MoneySerializer;
import lombok.*;
import org.javamoney.moneta.Money;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder(toBuilder = true)
public class PspTransactionCostEstimateResponse implements Comparable<PspTransactionCostEstimateResponse>{

    @NonNull private String psp;

    @JsonDeserialize(using = MoneyDeserializer.class)
    @JsonSerialize(using = MoneySerializer.class)
    @NonNull private Money amount;

    @Override
    public int compareTo(PspTransactionCostEstimateResponse other) {
        return amount.compareTo(other.amount);
    }
}
