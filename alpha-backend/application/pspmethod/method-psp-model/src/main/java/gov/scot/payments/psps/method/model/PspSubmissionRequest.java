package gov.scot.payments.psps.method.model;


import gov.scot.payments.payments.model.aggregate.MetadataField;
import gov.scot.payments.payments.model.aggregate.Payment;
import io.vavr.collection.List;
import lombok.*;

import java.util.Map;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Builder(toBuilder = true)
public class PspSubmissionRequest {

    @NonNull @Builder.Default
    Map<String, String> metadata = Map.of();

    @NonNull private List<Payment> payments;
}
