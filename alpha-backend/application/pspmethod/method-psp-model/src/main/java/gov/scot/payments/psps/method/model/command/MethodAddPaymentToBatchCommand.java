package gov.scot.payments.psps.method.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.Payment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "pspmethod", type = "addPaymentToBatch")
@NoArgsConstructor
public class MethodAddPaymentToBatchCommand extends CommandImpl implements HasKey<String> {

    @NonNull private Payment payment;

    public MethodAddPaymentToBatchCommand(Payment payment, boolean reply) {
        super(reply);
        this.payment = payment;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return payment.getKey();
    }
}