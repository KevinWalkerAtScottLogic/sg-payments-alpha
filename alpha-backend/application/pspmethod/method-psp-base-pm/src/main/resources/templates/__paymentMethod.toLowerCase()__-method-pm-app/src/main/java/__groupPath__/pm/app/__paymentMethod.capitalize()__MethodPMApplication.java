package @group@.pm.app;

import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.component.processmanager.EventTransformer;
import gov.scot.payments.application.component.processmanager.ProcessManagerApplication;
import gov.scot.payments.application.component.processmanager.EventFilter;
import gov.scot.payments.application.kafka.StreamJoiner;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.kafka.HeaderPreservingCompositeNonNativeSerde;
import gov.scot.payments.model.Event;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import org.springframework.context.annotation.Bean;

@ApplicationComponent
public class @paymentMethod.capitalize()@MethodPMApplication extends BaseMethodPspProcessManager {

    @Override
    protected Set<PaymentMethod> supportedPaymentMethods() {
        return HashSet.of(PaymentMethod.@paymentMethod.capitalize()@);
    }

    public static void main(String[] args){
        BaseApplication.run(@paymentMethod.capitalize()@MethodPMApplication.class,args);
    }
}