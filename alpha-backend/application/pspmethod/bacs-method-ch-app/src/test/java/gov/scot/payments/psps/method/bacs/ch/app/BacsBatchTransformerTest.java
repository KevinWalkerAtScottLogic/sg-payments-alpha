package gov.scot.payments.psps.method.bacs.ch.app;

import gov.scot.payments.application.component.commandhandler.stateful.AggregateState;
import gov.scot.payments.model.*;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.psps.method.ch.app.PaymentSubmitter;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentState;
import gov.scot.payments.psps.method.model.PspAdapterPayment;
import gov.scot.payments.psps.method.model.command.MethodSubmitPaymentBatchCommand;
import gov.scot.payments.psps.method.model.event.MethodPaymentAcceptedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentRejectedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentSubmissionFailedEvent;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Delegate;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.Stores;
import org.apache.kafka.streams.state.TimestampedKeyValueStore;
import org.apache.kafka.streams.state.ValueAndTimestamp;
import org.apache.kafka.streams.state.internals.InMemoryKeyValueStore;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class BacsBatchTransformerTest {

    private BacsBatchTransformer bacsBatchTransformer;
    private TimestampedKeyValueStore<byte[], PspAdapterPaymentState> paymentStore;
    private KeyValueStore<String, UUID> keyStore;
    private Map<String,UUID> keys;
    private PaymentSubmitter submitter;

    @BeforeEach
    public void setUp(){
        keys = new HashMap<>();
        keyStore = mock(KeyValueStore.class);
        doAnswer(i -> {
            String key = i.getArgument(0,String.class);
            UUID value = keys.get(key);
            return value;
        }).when(keyStore).get(anyString());
        doAnswer(i -> {
            String key = i.getArgument(0,String.class);
            UUID value = i.getArgument(1,UUID.class);
            keys.put(key,value);
            return null;
        }).when(keyStore).put(anyString(),any());
        paymentStore = mock(TimestampedKeyValueStore.class);
        ProcessorContext context = mock(ProcessorContext.class);
        when(context.headers()).thenReturn(new RecordHeaders());
        when(context.getStateStore("store")).thenReturn(paymentStore);
        when(context.getStateStore(BacsBatchTransformer.STATE_STORE_NAME)).thenReturn(keyStore);
        Serde serde = mock(Serde.class);
        Serializer serializer = mock(Serializer.class);
        when(serde.serializer()).thenReturn(serializer);
        when(serializer.serialize(any(),any(),any())).thenReturn(new byte[0]);
        submitter = mock(PaymentSubmitter.class);
        when(submitter.submitPayments(anyString(),any())).thenReturn(List.empty());
       bacsBatchTransformer = new BacsBatchTransformer(submitter,"store",serde, PaymentMethod.Bacs);
       bacsBatchTransformer.init(context);
    }

    @Test
    public void test(){
        //payment 1 not existing - should leave ref as is
        PspAdapterPaymentState payment1 = makePayment("1");

        //payment 2 existing - should create new ref and store that too
        PspAdapterPaymentState payment2 = makePayment("2");
        storeRef(payment2.getCurrent().getPayment());

        //payment 3 - building society and ref exists - shoudl error
        PspAdapterPaymentState payment3 = makePayment("3",UKBuildingSocietyAccount.builder()
                .name("act")
                .sortCode(SortCode.fromString("123456"))
                .accountNumber(UKAccountNumber.fromString("12345678"))
                .currency("GBP")
                .rollNumber("12345")
                .build());
        storeRef(payment3.getCurrent().getPayment());

        //payment 4 - too long ref truncated
        PspAdapterPaymentState payment4 = makePayment("4444444444444444444444444444444444444444");

        //payment 5 - too long ref truncated should create new ref and store that too
        PspAdapterPaymentState payment5 = makePayment("4444444444444444444444444444444444444444");

        MethodSubmitPaymentBatchCommand command = MethodSubmitPaymentBatchCommand.builder()
                .key("")
                .payments(List.of(payment1.getCurrent().getPayment().getId()
                    , payment2.getCurrent().getPayment().getId()
                    , payment3.getCurrent().getPayment().getId()
                    , payment4.getCurrent().getPayment().getId()
                    , payment5.getCurrent().getPayment().getId()))
                .build();
        bacsBatchTransformer.transform(command);
        ArgumentCaptor<List<Payment>> captor = ArgumentCaptor.forClass(List.class);
        verify(submitter,times(1)).submitPayments(anyString(),captor.capture());

        List<Payment> submitted = captor.getValue();
        assertEquals(5,submitted.size());
        assertEquals(PaymentStatus.ReadyForSubmission,submitted.get(0).getStatus());
        assertEquals("1",submitted.get(0).getCreditorMetadata(MetadataField.CLIENT_REF).get().getValue());
        assertEquals(payment1.getCurrent().getPayment().getId(),keys.get("2020-02-27;123456-12345678;123456-12345678;GBP 100;1"));

        assertEquals(PaymentStatus.ReadyForSubmission,submitted.get(1).getStatus());
        assertEquals("1-2",submitted.get(1).getCreditorMetadata(MetadataField.CLIENT_REF).get().getValue());
        assertEquals(payment2.getCurrent().getPayment().getId(),keys.get("2020-02-27;123456-12345678;123456-12345678;GBP 100;1-2"));

        assertEquals(PaymentStatus.Rejected,submitted.get(2).getStatus());
        assertEquals(payment3.getCurrent().getPayment().getId(),keys.get("2020-02-27;123456-12345678;123456-12345678;GBP 100;12345"));

        assertEquals(PaymentStatus.ReadyForSubmission,submitted.get(3).getStatus());
        assertEquals("4444444444444444444444444444444444444444",submitted.get(3).getCreditorMetadata(MetadataField.CLIENT_REF).get().getValue());
        assertEquals(payment4.getCurrent().getPayment().getId(),keys.get("2020-02-27;123456-12345678;123456-12345678;GBP 100;444444444444444444"));

        assertEquals(PaymentStatus.ReadyForSubmission,submitted.get(4).getStatus());
        assertEquals("1-4444444444444444444444444444444444444444",submitted.get(4).getCreditorMetadata(MetadataField.CLIENT_REF).get().getValue());
        assertEquals(payment5.getCurrent().getPayment().getId(),keys.get("2020-02-27;123456-12345678;123456-12345678;GBP 100;1-4444444444444444"));

        assertEquals(6,keys.size());
    }

    private void storeRef(Payment payment) {
        String key = bacsBatchTransformer.generateBacsKey(payment,bacsBatchTransformer.generateBacsRef(payment));
        keys.put(key,payment.getId());
    }

    private PspAdapterPaymentState makePayment(String ref, CashAccount account) {
        Payment payment = Payment.builder()
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.Bacs))
                .debtor(PartyIdentification.builder().name("").build())
                .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .creditor(PartyIdentification.builder().name("").build())
                .creditorAccount(account)
                .amount(Money.parse("GBP 1.00"))
                .latestExecutionDate(LocalDate.parse("2020-02-27").atStartOfDay().toInstant(ZoneOffset.UTC))
                .creditorMetadata(List.of(MetadataField.clientRef(ref)))
                .status(PaymentStatus.ReadyForSubmission)
                .product(new CompositeReference("",""))
                .build();

        PspAdapterPaymentState state =  PspAdapterPaymentState.builder()
                .current(PspAdapterPayment.builder()
                        .payment(payment)
                        .build())
                .build();
        when(paymentStore.get(state.getCurrent().getKey().getBytes())).thenReturn(ValueAndTimestamp.make(state,0L));
        return state;
    }

    private PspAdapterPaymentState makePayment(String ref) {
        return makePayment(ref,UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build());
    }

}