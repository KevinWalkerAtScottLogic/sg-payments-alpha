{{- define "bacsmethod-bacs-method-ch-app.env" -}}
- name: CLIENT_SECRET
  valueFrom:
    configMapKeyRef:
      name: cognito-info
      key: client_secret
- name: CLIENT_ID
  valueFrom:
    configMapKeyRef:
      name: cognito-info
      key: client_id
- name: SUPPORTED_PAYMENT_METHODS
  value: bacs
- name: KUBERNETES
  value: "true"
- name: BATCH_SIZE
  value: {{ .Values.${projectValuesName}.batchSize | quote }}
- name: BATCH_MAX_PAYMENT_AGE
  value: {{ .Values.${projectValuesName}.batchMaxPaymentAge | quote }}
- name: BATCH_RESOLUTION
  value: {{ .Values.${projectValuesName}.batchResolution | quote }}
{{- end -}}