package gov.scot.payments.psps.method.bacs.ch.app;

import gov.scot.payments.psps.method.ch.app.PspClient;
import gov.scot.payments.psps.method.model.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

@AllArgsConstructor
@NoArgsConstructor
public class BacsMethodClient implements PspClient {

    private String psp;

    private WebClient webClient;

    public BacsMethodClient(ServiceInstance serviceInstance) {
        this.psp = serviceInstance.getMetadata().get("app.kubernetes.io/name");
        this.webClient = WebClient.create(serviceInstance.getUri().toString());
    }

    @Override
    public PspSubmissionResponse submitPayments(PspSubmissionRequest request) {
        return webClient
            .post()
            .uri("/submit/")
            .contentType(MediaType.TEXT_PLAIN)
            .bodyValue(request)
            .retrieve()
            .bodyToMono(PspSubmissionResponse.class)
            .block();
    }

    @Override
    public PspTransactionCostEstimateResponse estimateTransactionCosts(PspSubmissionRequest request) {
        return webClient
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path("/estimateTransactionCosts/")
                        .build()
                )
                .bodyValue(request)
                .retrieve()
                .bodyToMono(PspTransactionCostEstimateResponse.class)
                .block();
    }

    @Override
    public PspCancelResponse cancelPayment(PspCancelRequest request) {
        return PspCancelResponse.builder().success(false).build();
    }

    @Override
    public String getName() {
        return psp;
    }
}
