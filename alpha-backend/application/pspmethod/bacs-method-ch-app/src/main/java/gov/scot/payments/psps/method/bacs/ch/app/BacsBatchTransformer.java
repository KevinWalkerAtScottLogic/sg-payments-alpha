package gov.scot.payments.psps.method.bacs.ch.app;

import gov.scot.payments.model.Command;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.psps.method.ch.app.PaymentSubmitter;
import gov.scot.payments.psps.method.ch.app.PspAdapterPaymentState;
import gov.scot.payments.psps.method.ch.app.batcher.kafka.BatchSubmittingTransformer;
import gov.scot.payments.psps.method.model.command.MethodSubmitPaymentBatchCommand;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.TimestampedKeyValueStore;
import org.apache.kafka.streams.state.ValueAndTimestamp;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Iterator;
import java.util.UUID;

@SuperBuilder
public class BacsBatchTransformer extends BatchSubmittingTransformer {

    public static final String STATE_STORE_NAME = "bacsKeyToPaymentId";

    private KeyValueStore<String, UUID> paymentKeyStore;

    public BacsBatchTransformer(PaymentSubmitter submitter, String storeName, Serde<Command> commandSerde, PaymentMethod paymentMethod) {
        super(submitter, storeName, commandSerde, paymentMethod);
    }

    @Override
    public void init(ProcessorContext context) {
        super.init(context);
        paymentKeyStore = (KeyValueStore<String, UUID>)context.getStateStore(STATE_STORE_NAME);
    }

    @Override
    protected List<Payment> getPayments(MethodSubmitPaymentBatchCommand value) {
        return super.getPayments(value)
                .map(this::updateReferenceIfNecessary);
    }

    private Payment updateReferenceIfNecessary(Payment payment) {
        String ref = generateBacsRef(payment);
        String bacsKey = generateBacsKey(payment,ref);
        UUID existingWithKey = paymentKeyStore.get(bacsKey);
        if(existingWithKey == null){
            paymentKeyStore.put(bacsKey,payment.getId());
            return payment;
        } else if(payment.getCreditorAccountAs(UKBuildingSocietyAccount.class).isDefined()){
            return payment.toBuilder()
                    .status(PaymentStatus.Rejected)
                    .submissions(payment.getSubmissions().append(Submission.builder()
                            .paymentMethod(PaymentMethod.Bacs)
                            .message("Can not pay the same building society account the same amount multiple times in a single day")
                            .status(Submission.Status.Failure)
                            .build()))
                    .build();
        } else {
            int i = 1;
            String newRef;
            do {
                newRef = String.format("%s-%s",i,ref);
                bacsKey = generateBacsKey(payment,newRef);
                i++;
            } while(paymentKeyStore.get(bacsKey) != null);
            paymentKeyStore.put(bacsKey,payment.getId());

            io.vavr.collection.Map<String,MetadataField> metadataMap = payment.getCreditorMetadataAsMap();
            MetadataField clientRef = MetadataField.clientRef(newRef);
            metadataMap = metadataMap.put(clientRef.getName(),clientRef);
            return payment.toBuilder()
                    .creditorMetadata(metadataMap.toList().map(t -> t._2))
                    .build();
        }
    }

    String generateBacsKey(Payment payment, String ref) {
        UKBankAccount creditorAccount = payment.getCreditorAccountAs(UKBankAccount.class).get();
        String truncatedRef = String.format("%.18s",ref);
        String processingDate = LocalDate.ofInstant(payment.getLatestExecutionDate(), ZoneId.of("UTC")).toString();
        String payerAccount = String.format("%s-%s",payment.getDebtorAccount().getSortCode(),payment.getDebtorAccount().getAccountNumber());
        String receiverAccount = String.format("%s-%s",creditorAccount.getSortCode(),creditorAccount.getAccountNumber());
        String amount = payment.getAmount().scaleByPowerOfTen(2).stripTrailingZeros().toString();
       return String.format("%s;%s;%s;%s;%s",processingDate,payerAccount,receiverAccount,amount,truncatedRef).toUpperCase();
    }

    String generateBacsRef(Payment payment) {
        Option<UKBuildingSocietyAccount> buildingSociety = payment.getCreditorAccountAs(UKBuildingSocietyAccount.class);
        if(buildingSociety.isDefined()){
            return buildingSociety.get().getRollNumber();
        }
        return payment.getCreditorMetadata(MetadataField.CLIENT_REF).map(MetadataField::getValue).getOrElse("");
    }
}
