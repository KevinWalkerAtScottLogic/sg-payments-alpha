package gov.scot.payments.psps.method.sepadc.ch.app;

import gov.scot.payments.psps.method.ch.app.PspClient;
import gov.scot.payments.psps.method.model.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

@AllArgsConstructor
@NoArgsConstructor
public class SepaDCClient implements PspClient {

    private String psp;

    private WebClient webClient;

    public SepaDCClient(ServiceInstance serviceInstance) {
        this.psp = serviceInstance.getMetadata().get("app.kubernetes.io/name");
        this.webClient = WebClient.create(serviceInstance.getUri().toString());
    }

    @Override
    public PspSubmissionResponse submitPayments(PspSubmissionRequest request) {
        return webClient
                .post()
                .uri("/submit/")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(request.getPayments().head())
                .exchange()
                .flatMap(clientResponse -> clientResponse.bodyToMono(PspSubmissionResponse.class)) // TODO probably not correct, need the adapter impl first.
                .block();
    }

    @Override
    public PspTransactionCostEstimateResponse estimateTransactionCosts(PspSubmissionRequest request) {
        var amount = request.getPayments().head().getAmount();

        return webClient
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path("/estimateTransactionCosts/")
                        .queryParam("amount", amount.getNumberStripped().toString())
                        .queryParam("currency", amount.getCurrency().toString())
                        .build()
                )
                .exchange()
                .flatMap(clientResponse -> clientResponse.bodyToMono(PspTransactionCostEstimateResponse.class))
                .block();

    }

    @Override
    public PspCancelResponse cancelPayment(PspCancelRequest request) {
        // Unsupported for now
        return PspCancelResponse.builder().success(false).build();
    }

    @Override
    public String getName() {
        return psp;
    }
}
