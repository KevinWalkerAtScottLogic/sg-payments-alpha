package gov.scot.payments.psps.method.sepadc.ch.app;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchRules;
import com.tngtech.archunit.junit.ArchTest;
import gov.scot.payments.testing.architecture.ApplicationRules;
import gov.scot.payments.testing.architecture.CodingStandardsRules;

@AnalyzeClasses(importOptions = {ImportOption.DoNotIncludeTests.class, ImportOption.DoNotIncludeArchives.class})
public class ArchitectureTest{

    @ArchTest
    public static final ArchRules generalRules = ArchRules.in(CodingStandardsRules.class);

    @ArchTest
    public static final ArchRules appRules = ArchRules.in(ApplicationRules.class);
}