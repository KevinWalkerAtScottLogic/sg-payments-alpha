package gov.scot.payments.psps.method.sepadc.pm.app;

import gov.scot.payments.application.ApplicationComponent;
import org.springframework.context.annotation.Import;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.apache.kafka.streams.KafkaStreams;
import org.springframework.beans.factory.annotation.Autowired;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.spring.TestConfiguration;

import java.time.Duration;

@ApplicationIntegrationTest(classes = {SepaDCMethodPMAppTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.PROCESS_MANAGER)
public class SepaDCMethodPMAppTest {

    @BeforeEach
    public void setUp(EmbeddedBrokerClient brokerClient
            , @Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter
            , @Value("${commands.destination}") String commandTopic){
            String retryTopic = commandTopic + "-retry-1";
            kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING,Duration.ofSeconds(10));
            brokerClient.getBroker().addTopicsIfNotExists(commandTopic,retryTopic);
     }

    @Test
    public void test(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){

    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends SepaDCMethodPMApplication {

    }
}