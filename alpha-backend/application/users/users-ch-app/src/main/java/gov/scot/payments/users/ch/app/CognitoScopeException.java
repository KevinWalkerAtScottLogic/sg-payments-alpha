package gov.scot.payments.users.ch.app;

import software.amazon.awssdk.services.cognitoidentityprovider.model.ResourceNotFoundException;

public class CognitoScopeException extends RuntimeException {

    public CognitoScopeException(String string) {
        super(string);
    }

    public CognitoScopeException(String appErrMsg, Throwable e) {
        super(appErrMsg, e);
    }
}
