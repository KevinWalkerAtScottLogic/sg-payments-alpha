package gov.scot.payments.users.ch.app;

public class CognitoApplicationException extends RuntimeException {

    public CognitoApplicationException(String errMessage) {
        super(errMessage);
    }

    public CognitoApplicationException(String appErrMsg, Exception e) {
        super(appErrMsg, e);
    }

}
