package gov.scot.payments.users.ch.app;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.user.*;
import io.vavr.collection.Set;

import java.net.URL;

public class MockCognitoService implements CognitoService {
    @Override
    public User addUser(User user) {
        return user;
    }

    @Override
    public String removeUser(String user) {
        return user;
    }

    @Override
    public Application addApplication(String appName, String sshPublicKey, Set<Role> roles) {
        return Application.builder()
                .name(appName)
                .clientId("client id")
                .clientSecret("clientSecret")
                .roles(roles)
                .build();
    }

    @Override
    public String removeApplication(String clientName) {
        return clientName;
    }

    @Override
    public Scope addScope(Scope scope) {
        return scope;
    }

    @Override
    public Scope removeScope(Scope scope) {
        return scope;
    }

    @Override
    public Application assignRolesToApplication(String appName, String appId, Set<Role> roles) {
        return Application.builder()
                .name(appName)
                .clientId(appId)
                .clientSecret("clientSecret")
                .roles(roles)
                .build();
    }

    @Override
    public ResourceWithVerbs addActionsToOauthServers(ResourceWithVerbs resource) {
        return resource;
    }

    @Override
    public String removeActionsFromOauthServers(String resource) {
        return resource;
    }

    @Override
    public User updateUserRoles(User user) {
        return user;
    }

    @Override
    public Scope updatePermissionsForFileChange(CompositeReference ref, URL resource) {
        return ref.toScope();
    }

    @Override
    public Scope deletePermissionsForFileChange(CompositeReference ref, URL resource) {
        return ref.toScope();
    }
}
