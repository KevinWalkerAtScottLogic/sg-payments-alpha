package gov.scot.payments.users.ch.app;

import gov.scot.payments.model.user.Scope;
import lombok.Builder;
import lombok.NonNull;


@Builder
public class SftpFilePathGenerator {

    @NonNull private final String fileBucket;
    @NonNull private final String environment;

    public String getFilePathFromScope(Scope scope){

        Scope parent = scope.getParent();
        String child = scope.getName();
        if(parent == null && child == null){
            throw new InvalidSftpPathException("Invalid scope provided for SFTP path");
        }

        StringBuilder sb = new StringBuilder();
        var dirBase = String.format("/%s/%s", fileBucket, environment);
        sb.append(dirBase);
        if(parent != null) {
            sb.append(String.format("/%s", parent.getName()));
        }
        sb.append(String.format("/%s", child));
        return sb.toString();
    }


}
