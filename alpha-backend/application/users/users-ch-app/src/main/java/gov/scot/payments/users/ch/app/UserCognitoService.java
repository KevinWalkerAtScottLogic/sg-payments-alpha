package gov.scot.payments.users.ch.app;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.user.*;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import lombok.extern.slf4j.Slf4j;
import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient;
import software.amazon.awssdk.services.cognitoidentityprovider.model.*;
import software.amazon.awssdk.services.transfer.TransferClient;

import java.net.URL;


/*
* A class for mapping between actions in the User context
* and to the Cognito resource
 */
@Slf4j
public class UserCognitoService implements CognitoService {

    // TODO - separte out concerns more so we are not explicitly tied to
    // cognito and sftp transfer
    private final CognitoAdapter cognitoAdapter;
    private final UserSftpService sftpTransferService;

    public UserCognitoService(CognitoAdapter cognitoAdapter, UserSftpService sftpService){
        this.cognitoAdapter = cognitoAdapter;
        this.sftpTransferService = sftpService;
    }

    @Override
    public User addUser(User user) {
        try {
            validateUser(user);
            var actionString = CognitoModelTranslator.mapRolesToPermittedActionsString(user.getRoles());
            var cognitoUser = cognitoAdapter.addCognitoUser(user.getName(), user.getEmail(), actionString);
            assignUserScopes(user);
            return CognitoModelTranslator.mapUserPoolTypeToUser(cognitoUser,HashSet.ofAll(cognitoAdapter.getUserGroups(cognitoUser.username())));
        } catch (UsernameExistsException e){
            String errMsg = String.format("Cannot add user %s as it already exists.", user.getName());
            throw new CognitoUserException(errMsg, e);
        }
    }

    private void validateUser(User user) {
        verifyGroupsMatchRolesForUser(user);
        verifyGroupsExist(user.getScopes());
        verifyAppValidForSftp(user);
    }

    private void verifyAppValidForSftp(User user) {
        if(!sftpTransferService.doesSubjectHaveSftpPermission(user)) {
            return;
        }
        if(user.getSshPublicKey() == null){
            throw new MissingKeyException(String.format("User %s does not contain ssh key.", user.getName()));
        }
    }

    private void verifyGroupsMatchRolesForUser(User user) {

        var scopeNames = user.getScopes().map(Scope::getQualifiedName);
        var groupNames = user.getGroups().map(Group::getName);

        if(!scopeNames.equals(groupNames)){
            throw new CognitoScopeException(String.format("User %s must have matching groups and roles.", user.getName()));
        }
    }

    private void verifyGroupsExist(Set<Scope> groups) {

        var existingGroupNames = List.ofAll(cognitoAdapter.listGroups()).map(GroupType::groupName);
        groups.forEach( group -> {
            if(!existingGroupNames.contains(group.toString())){
                throw new CognitoScopeException(String.format("Cognito group %s does not exist.", group.toString()));
            }
        });
    }

    private void assignUserScopes(User user) {
        if(sftpTransferService.doesSubjectHaveSftpPermission(user)){
            sftpTransferService.addUserToSftp(user);
        }
        var scopes = user.getScopes();
        scopes.forEach( scope -> cognitoAdapter.assignUserToGroup(user.getName(), scope.toString()));
    }

    @Override
    public String removeUser(String user) {
        try{
            cognitoAdapter.deleteCognitoUser(user);
            sftpTransferService.removeFromSftp(user);
            return user;
        } catch (UserNotFoundException e){
            String errMsg = String.format("An error occurred deleting user %s", user);
            throw new CognitoUserException(errMsg, e);
        }
    }

    @Override
    public Application addApplication(String appName, String sshPublicKey, Set<Role> roles) {
        var currentClient = cognitoAdapter.getUserPoolClientIfExists(appName);
        if(currentClient.isDefined()){
            throw new CognitoApplicationException(String.format("A client id with the name %s already exists", appName));
        }
        validateApplication(appName, sshPublicKey, roles);
        var cognitoAppClient = cognitoAdapter.addAppClient(appName);
        if(sftpTransferService.doRolesHaveSftpPermission(roles)){
            sftpTransferService.addApplicationToSftp(appName, sshPublicKey);
        }
        return assignRolesToApplication(appName, cognitoAppClient.clientId(), roles);
    }

    private void validateApplication(String appName, String sshPublicKey, Set<Role> roles) {
        verifyApplicationRolesExist(roles);
        verifyAppValidForSftp(appName, sshPublicKey, roles);
    }

    private void verifyAppValidForSftp(String appName, String sshPublicKey, Set<Role> roles) {
        if(!sftpTransferService.doRolesHaveSftpPermission(roles)) {
            return;
        }
        if(sshPublicKey == null){
            throw new MissingKeyException(String.format("Application %s does not contain ssh key.", appName));
        }
    }

    private void verifyApplicationRolesExist(Set<Role> roles) {

        var servers = cognitoAdapter.listResourceServers();
        roles.forEach( role -> {
            if(!servers.contains(role.toString())){
                throw new CognitoScopeException(String.format("OAuth Role %s not defined in resource server.", role.toString()));
            }
        });
    }

    @Override
    public String removeApplication(String clientName) {
        try {
            var currentClient = cognitoAdapter.getUserPoolClientIfExists(clientName);
            if(!currentClient.isDefined()){
                throw new CognitoApplicationException(String.format("Client name %s does not exist", clientName));
            }
            cognitoAdapter.removeAppClient(currentClient.get().clientId());
            sftpTransferService.removeFromSftp(clientName);
            return clientName;
        } catch(InvalidParameterException | ResourceNotFoundException e){
            String appErrMsg = String.format("Invalid client name %s", clientName);
            throw new CognitoApplicationException(appErrMsg, e);
        }
    }

    @Override
    public Scope addScope(Scope scope) {

        var scopeString = scope.toString();

        var existingScope = cognitoAdapter.describeResourceService(scopeString);
        if(existingScope.isEmpty()){
            cognitoAdapter.addResourceServer(scopeString);
        }
        var existingGroup = cognitoAdapter.describeCognitoGroup(scopeString);
        if(existingGroup.isEmpty()){
            cognitoAdapter.addCognitoGroup(scopeString, "A group representing permissions for "+scopeString);
        }
        return scope;
    }

    @Override
    public Scope removeScope(Scope scope) {

        var scopeString = scope.toString();
        sftpTransferService.updateSftpPermissionsForScopeRemoval(scope, getAllSubjects());

        Throwable ex = null;
        try {
            cognitoAdapter.deleteResourceServer(scopeString);
        } catch (Exception e){
            ex = e;
        }
        try {
            cognitoAdapter.deleteCognitoGroup(scopeString);
        } catch (Exception e){
            ex = e;
        }
        // TODO - check how remove scope works with SFTP permissions
        if(ex != null){
            String appErrMsg = String.format("Scope %s not found in user pool", scopeString);
            throw new CognitoScopeException(appErrMsg, ex);
        }
        return scope;

    }

    @Override
    public Application assignRolesToApplication(String appName, String appId, Set<Role> roles) {

        try {
            var oauthStr = roles.map(Role::toString);
            var cognitoAppClient = cognitoAdapter.updateClientIdRoles(appId, List.ofAll(oauthStr));
            if(sftpTransferService.doRolesHaveSftpPermission(roles)){
                sftpTransferService.setApplicationPermissions(appName, roles);
            }
            return CognitoModelTranslator.mapUserPoolClientToApplication(cognitoAppClient);
        } catch (ResourceNotFoundException e){
            String appErrMsg = String.format("Invalid client ID %s", appId);
            throw new CognitoApplicationException(appErrMsg, e);
        }
    }

    @Override
    public ResourceWithVerbs addActionsToOauthServers(ResourceWithVerbs resource) {
        var actionStrings = resource.toActions().map(Action::toString);
        cognitoAdapter.addOathScopesToResourceServers(actionStrings);
        return resource;
    }

    @Override
    public String removeActionsFromOauthServers(String resource) {
        cognitoAdapter.removeAllPrefixedOathScopesFromResourceServers(resource);
        return resource;
    }

    @Override
    public User updateUserRoles(User user){
        assignUserScopes(user);
        var actionString = CognitoModelTranslator.mapRolesToPermittedActionsString(user.getRoles());
        cognitoAdapter.updateUserPermissions(user.getName(), actionString);
        if(sftpTransferService.doesSubjectHaveSftpPermission(user)){
            sftpTransferService.setUserPermissions(user);
        }

        var groups = cognitoAdapter.getUserGroups(user.getName());
        var attributes = cognitoAdapter.getAttributes(user.getName());
        return CognitoModelTranslator.mapUserPoolTypeToUser(user.getName(),attributes, HashSet.ofAll(groups));
    }

    @Override
    public Scope updatePermissionsForFileChange(CompositeReference ref, URL resource){
        var scope = ref.toScope();
        List<Subject> allSubjects = getAllSubjects();
        var filePath = resource.getPath();
        sftpTransferService.updateRelevantSubjectsForFileResources(allSubjects, scope, filePath);
        return scope;
    }

    @Override
    public Scope deletePermissionsForFileChange(CompositeReference ref, URL resource){
        var scope = ref.toScope();
        List<Subject> allSubjects = getAllSubjects();
        sftpTransferService.updateSftpPermissionsForScopeRemoval(scope, allSubjects);
        return scope;
    }

    private List<Subject> getAllSubjects() {
        List<Subject> users = List.ofAll(getAllUsers());
        List<Subject> applications = List.ofAll(getAllApplications());
        return users.appendAll(applications);
    }

    private List<User> getAllUsers() {
        // filter all users with 'paymentFiles' resource
        var cognitoUsers = cognitoAdapter.listUsers();
        return cognitoUsers.map(x -> {
            var groups = HashSet.ofAll(cognitoAdapter.getUserGroups(x.username()));
            return CognitoModelTranslator.mapUserPoolTypeToUser(x, groups);
        });
    }

    private List<Application> getAllApplications(){
        var apps = cognitoAdapter.listUserPoolClients();
        return apps.map(CognitoModelTranslator::mapUserPoolClientToApplication);
    }

    public static void main(String[] args) {

        var id = System.getenv("COGNITO_USER_POOL_ID");
        var provider = EnvironmentVariableCredentialsProvider.create();
        var cognitoClient = CognitoIdentityProviderClient.builder()
                                     .credentialsProvider(provider)
                                     .region(Region.EU_WEST_2)
                                     .build();
        var wrapper = new CognitoAdapter(id, cognitoClient);

        var masterUserRole = System.getenv("MASTER_USER_ROLE");
        var transferId = System.getenv("TRANSFER_ID");
        var bucketName = System.getenv("BUCKET_NAME");
        var environment = System.getenv("ENVIRONMENT");
        var sshKey = System.getenv("SSHKEY");

        var transferClient = TransferClient.builder()
                .credentialsProvider(provider)
                .region(Region.EU_WEST_2)
                .build();

        var sftpTransfer = SftpTransferAdapter.builder()
                .serverId(transferId)
                .userRoleArn(masterUserRole)
                .transferClient(transferClient)
                .bucketName(bucketName)
                .environment(environment)
                .build();

        var pathGenerator = SftpFilePathGenerator.builder()
                .environment(environment)
                .fileBucket(bucketName)
                .build();
        var generator = new SftpNameGenerator(environment);
        var sftpService = UserSftpService.builder()
                .filePathGenerator(pathGenerator)
                .transferAdapter(sftpTransfer)
                .sftpNameGenerator(generator::getSftpUserNameForEnvironment)
                .build();

        var userMapper = new UserCognitoService(wrapper, sftpService);
    }

}
