package gov.scot.payments.users.ch.app;

import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Scope;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.model.user.User;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import software.amazon.awssdk.services.transfer.model.HomeDirectoryMapEntry;

import java.util.function.UnaryOperator;

@Builder
@Slf4j
public class UserSftpService {

    private static final String PAYMENT_FILES_RESOURCE = "paymentfiles";

    private final SftpTransferAdapter transferAdapter;
    private final SftpFilePathGenerator filePathGenerator;
    @Builder.Default private final UnaryOperator<String> sftpNameGenerator = name -> name;


    public boolean doesSubjectHaveSftpPermission(Subject subject) {
        return doRolesHaveSftpPermission(subject.getRoles());
    }

    public boolean doRolesHaveSftpPermission(Set<Role> roles){
        return roles.filter(r -> r.hasPermissionForResource(PAYMENT_FILES_RESOURCE)).size() > 0;
    }

    public boolean doRolesHaveSftpPermissionForScope(Set<Role> roles, Scope scope){
        return roles.filter(r -> r.getScope().equals(scope)
                                    && r.hasPermissionForResource(PAYMENT_FILES_RESOURCE)).size() > 0;
    }

    public void addUserToSftp(User user){
        if(user.getSshPublicKey() == null){
            throw new MissingKeyException(String.format("No ssh key present for %s", user.getName()));
        }
        var sftpName = sftpNameGenerator.apply(user.getName());
        transferAdapter.createUser(sftpName, user.getSshPublicKey());
        setUserPermissions(user);
    }

    public void addApplicationToSftp(String appName, String sshKey) {
        if (sshKey == null) {
            throw new MissingKeyException(String.format("No ssh key present for %s", appName));
        }
        var sftpName = sftpNameGenerator.apply(appName);
        transferAdapter.createUser(sftpName, sshKey);
    }

    public void setUserPermissions(User user){
        var dirMapping = getDirectoryMappingsFromRoles(user.getRoles());
        var sftpName = sftpNameGenerator.apply(user.getName());
        transferAdapter.updateUserPolicy(sftpName, dirMapping);
    }

    public void setApplicationPermissions(String appName, Set<Role> roles){
        var dirMapping = getDirectoryMappingsFromRoles(roles);
        var sftpName = sftpNameGenerator.apply(appName);
        transferAdapter.updateUserPolicy(sftpName, dirMapping);
    }

    public void removeFromSftp(String username){
        var sftpName = sftpNameGenerator.apply(username);
        transferAdapter.deleteUser(sftpName);
    }

    private List<HomeDirectoryMapEntry> getDirectoryMappingsFromRoles(Set<Role> roles){

        return roles
                .filter(r -> r.hasPermissionForResource(PAYMENT_FILES_RESOURCE))
                .map(this::getHomeDirectoryMappingFromRole)
                .toList();
    }

    private HomeDirectoryMapEntry getHomeDirectoryMappingFromRole(Role role){

        var path = filePathGenerator.getFilePathFromScope(role.getScope());

        var scope = role.getScope();
        var product = scope.getName();
        return HomeDirectoryMapEntry.builder()
                .entry(String.format("/%s", product))
                .target(path)
                .build();
    }

    public void updateRelevantSubjectsForFileResources(List<Subject> users, Scope scope, String fileResource) {

        var homeDirEntry = HomeDirectoryMapEntry.builder()
                .entry(scope.getName())
                .target(fileResource)
                .build();

        users.filter(this::doesSubjectHaveSftpPermission)
                .filter(u -> u.getScopes().contains(scope))
                .forEach(u -> transferAdapter.updateUserPolicy(u.getName(), List.of(homeDirEntry)));
    }

    public void updateSftpPermissionsForScopeRemoval(Scope scope, List<Subject> allSubjects) {
        allSubjects.forEach(s -> updateSftpUserForScopeRemoval(sftpNameGenerator.apply(s.getName()), s.getRoles(), scope));
    }

    private void updateSftpUserForScopeRemoval(String subjectName, Set<Role> roles, Scope scope) {

        if(!doRolesHaveSftpPermissionForScope(roles, scope)){
            log.info(String.format("Subject %s does not have sftp permissions for %s", subjectName, scope.getName()));
            return;
        }
        log.info("Updating home directory mappings for " +subjectName + " and "+ scope.getName());
        transferAdapter.removeHomeDirMappingFromUser(subjectName, scope.getName());
    }
}
