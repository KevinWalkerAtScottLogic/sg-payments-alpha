package gov.scot.payments.users.ch.app;

import gov.scot.payments.application.component.commandhandler.CommandHandlerFunction;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.func.PatternMatchingFunctionDelegate;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.users.model.*;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CognitoUserCommandHandler extends PatternMatchingFunctionDelegate<Command, List<EventWithCauseImpl>> implements CommandHandlerFunction<EventWithCauseImpl> {

    private final CognitoService service;

    @Override
    protected void handlers(final PatternMatcher.Builder<Command, List<EventWithCauseImpl>> builder) {
        builder.match(AddScopeCommand.class,c -> List.of(new ScopeAddedEvent(service.addScope(c.getScope()))))
               .match(RemoveScopeCommand.class, c -> List.of(new ScopeRemovedEvent(service.removeScope(c.getScope()))))
               .match(AddUserCommand.class, c -> List.of(new UserAddedEvent(service.addUser(c.getUser()))))
               .match(RemoveUserCommand.class, c -> List.of(new UserRemovedEvent(service.removeUser(c.getUser()), c.getEmail())))
               .match(AddApplicationCommand.class, c -> List.of(new ApplicationAddedEvent(service.addApplication(c.getApplication(), c.getSshPublicKey(), HashSet.empty()))))
               .match(RemoveApplicationCommand.class, c -> List.of(new ApplicationRemovedEvent(service.removeApplication(c.getClientName()))))
               .match(AddResourceCommand.class, c -> List.of(new ResourceAddedEvent(service.addActionsToOauthServers(c.getResource()))))
               .match(RemoveResourceCommand.class, c -> List.of(new ResourceRemovedEvent(service.removeActionsFromOauthServers(c.getResource()))))
               .match(UpdateUserFilePermissionsCommand.class, c -> List.of(new UserFileResourcesUpdatedEvent(service.updatePermissionsForFileChange(c.getProduct(), c.getPath()))))
               .match(DeleteUserFilePermissionsCommand.class, c -> List.of(new UserFileResourcesDeletedEvent(service.deletePermissionsForFileChange(c.getProduct(), c.getPath()))))
               .match(AssignUserRolesCommand.class, c -> List.of(new UserRolesAssignedEvent(service.updateUserRoles(c.getUser()))))
               .match(AssignApplicationRolesCommand.class, c -> List.of(new ApplicationRolesAssignedEvent(service.assignRolesToApplication(c.getApplication().getName(), c.getApplication().getClientId(), c.getApplication().getRoles()))));

    }
}
