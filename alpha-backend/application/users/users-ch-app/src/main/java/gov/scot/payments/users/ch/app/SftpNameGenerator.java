package gov.scot.payments.users.ch.app;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SftpNameGenerator {

    private String environment;

    public String getSftpUserNameForEnvironment(String username){
        // concat the environment?
        int code = (Math.abs(environment.hashCode())) % 100000;
        var codeStr = String.valueOf(code);
        return codeStr+"_"+username;
    }

}
