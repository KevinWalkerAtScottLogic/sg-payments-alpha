package gov.scot.payments.users.ch.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.component.commandhandler.CommandHandlerApplication;
import gov.scot.payments.application.component.commandhandler.CommandHandlerFunction;
import gov.scot.payments.model.EventWithCauseImpl;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.util.StringUtils;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClientBuilder;
import software.amazon.awssdk.services.transfer.TransferClient;
import software.amazon.awssdk.services.transfer.TransferClientBuilder;

import java.net.URI;

@ApplicationComponent
@EnableAutoConfiguration(exclude = {ContextStackAutoConfiguration.class})
public class UsersCHApplication extends CommandHandlerApplication<EventWithCauseImpl> {

    @Bean
    public CommandHandlerFunction<EventWithCauseImpl> chFunction(CognitoService userCognitoService) {
        return new CognitoUserCommandHandler(userCognitoService);
    }

    @Bean
    public UserQueryService userQueryService(CognitoService userCognitoService){
        return new UserQueryService(userCognitoService);
    }

    @Bean
    public CognitoIdentityProviderClient cognitoClient(@Value("${COGNITO_ENDPOINT_OVERRIDE:}") String cognitoEndpoint
            , @Value("${cloud.aws.region.static}") String region
            , @Value("${cloud.aws.credentials.accessKey}") String accessKey
            , @Value("${cloud.aws.credentials.secretKey}") String secretKey){
        AwsCredentials credentials = AwsBasicCredentials.create(accessKey,secretKey);
        AwsCredentialsProvider credentialsProvider = StaticCredentialsProvider.create(credentials);
        CognitoIdentityProviderClientBuilder builder = CognitoIdentityProviderClient.builder()
                                                                                         .credentialsProvider(credentialsProvider)
                                                                                         .region(Region.of(region));
        if(!StringUtils.isEmpty(cognitoEndpoint)){
            builder = builder.endpointOverride(URI.create(cognitoEndpoint));
        }
        return builder.build();
    }

    @Bean
    public TransferClient transferClient(@Value("${TRANSFER_ENDPOINT_OVERRIDE:}") String transferEndpoint
            , @Value("${cloud.aws.region.static}") String region
            , @Value("${cloud.aws.credentials.accessKey}") String accessKey
            , @Value("${cloud.aws.credentials.secretKey}") String secretKey){
        AwsCredentials credentials = AwsBasicCredentials.create(accessKey,secretKey);
        AwsCredentialsProvider credentialsProvider = StaticCredentialsProvider.create(credentials);
        TransferClientBuilder builder = TransferClient.builder()
                .credentialsProvider(credentialsProvider)
                .region(Region.of(region));
        if(!StringUtils.isEmpty(transferEndpoint)){
            builder = builder.endpointOverride(URI.create(transferEndpoint));
        }
        return builder.build();
    }

    @Bean
    public CognitoService userCognitoService(@Value("${COGNITO_USER_POOL_ID}") String userPool,
                                                 @Value("${FILE_BUCKET_NAME}") String fileBucket,
                                                 @Value("${SFTP_SERVER_ID}") String sftpServerId,
                                                 @Value("${SFTP_USER_ROLE}") String userRoleArn,
                                                 @Value("${ENVIRONMENT}") String environment,
                                                 @Value("${environment.local}") String runLocal
            , CognitoIdentityProviderClient client
            , TransferClient transferClient){

        if(runLocal.equals("true")){
            return new MockCognitoService();
        }

        var wrapper = new CognitoAdapter(userPool, client);
        var transferAdapter = SftpTransferAdapter.builder()
                .environment(environment)
                .serverId(sftpServerId)
                .bucketName(fileBucket)
                .userRoleArn(userRoleArn)
                .transferClient(transferClient)
                .build();

        var pathGenerator = SftpFilePathGenerator.builder()
                .environment(environment)
                .fileBucket(fileBucket)
                .build();
        var generator = new SftpNameGenerator(environment);
        var transferService = UserSftpService.builder()
                .transferAdapter(transferAdapter)
                .filePathGenerator(pathGenerator)
                .sftpNameGenerator(name -> generator.getSftpUserNameForEnvironment(name))
                .build();
        return new UserCognitoService(wrapper, transferService);
    }

    @Override
    protected Set<String> registeredVerbs() {
        return HashSet.of("Read","Write");
    }

    public static void main(String[] args){
        BaseApplication.run(UsersCHApplication.class,args);
    }


}