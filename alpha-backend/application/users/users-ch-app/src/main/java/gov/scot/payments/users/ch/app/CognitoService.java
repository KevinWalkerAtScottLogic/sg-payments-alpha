package gov.scot.payments.users.ch.app;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.user.*;
import io.vavr.collection.Set;

import java.net.URL;

public interface CognitoService {
    User addUser(User user);

    String removeUser(String user);

    Application addApplication(String appName, String sshPublicKey, Set<Role> roles);

    String removeApplication(String clientName);

    Scope addScope(Scope scope);

    Scope removeScope(Scope scope);

    Application assignRolesToApplication(String appName, String appId, Set<Role> roles);

    ResourceWithVerbs addActionsToOauthServers(ResourceWithVerbs resource);

    String removeActionsFromOauthServers(String resource);

    User updateUserRoles(User user);

    Scope updatePermissionsForFileChange(CompositeReference ref, URL resource);

    Scope deletePermissionsForFileChange(CompositeReference ref, URL resource);
}
