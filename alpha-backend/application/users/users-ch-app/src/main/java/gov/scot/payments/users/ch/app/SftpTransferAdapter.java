package gov.scot.payments.users.ch.app;

import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import lombok.Builder;
import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.transfer.TransferClient;
import software.amazon.awssdk.services.transfer.model.*;

@Builder
public class SftpTransferAdapter {

    private final TransferClient transferClient;
    private final String serverId;
    private final String userRoleArn;
    private final String environment;
    private final String bucketName;

    public boolean doesUserExist(String username) {

        var response = transferClient.listUsers(ListUsersRequest.builder()
                .serverId(serverId)
                .build());
        return response.users().stream().anyMatch(u -> u.userName().equals(username));

    }

    public void createUser(String username, String sshPublicKey){

        if(doesUserExist(username)){
            return;
        }
        try {
            transferClient.createUser(CreateUserRequest.builder()
                    .userName(username)
                    .serverId(serverId)
                    .role(userRoleArn)
                    .sshPublicKeyBody(sshPublicKey)
                    .build());

        }catch(ResourceExistsException e){
            throw new SftpUserException("An SFTP User already exists for " + username);
        }
    }

    public Set<HomeDirectoryMapEntry> getCurrentUserDirectories(String username){
        var user = transferClient.describeUser(DescribeUserRequest.builder()
                .serverId(serverId)
                .userName(username)
                .build());

        return HashSet.ofAll(user.user().homeDirectoryMappings());
    }

    public void deleteUser(String username){
        transferClient.deleteUser(DeleteUserRequest.builder()
                    .userName(username)
                    .serverId(serverId)
                    .build());
    }

    public void updateUserPolicy(String username, List<HomeDirectoryMapEntry> homeDirectoryMapEntries){

        var currentDir = getCurrentUserDirectories(username);
        var newDirectories = currentDir.addAll(homeDirectoryMapEntries);

        transferClient.updateUser(UpdateUserRequest.builder()
                .userName(username)
                .serverId(serverId)
                .homeDirectoryType(HomeDirectoryType.LOGICAL)
                .homeDirectoryMappings(newDirectories.toJavaList())
                .build());
    }

    public void removeHomeDirMappingFromUser(String username, String homeDirName){

        var currentDir = getCurrentUserDirectories(username);
        var newDirectories = currentDir.filter(homeDir -> !homeDir.entry().equals(homeDirName));

        var updateBuilder = UpdateUserRequest.builder().userName(username).serverId(serverId);

        // TODO update test to add case for empty listing
        if(!newDirectories.isEmpty()){
            updateBuilder = updateBuilder
                    .homeDirectoryType(HomeDirectoryType.LOGICAL)
                    .homeDirectoryMappings(newDirectories.toJavaList());
        } else {
            deleteUser(username);
        }

        transferClient.updateUser(updateBuilder.build());
    }

    public static void main(String[] args) {
        var provider = EnvironmentVariableCredentialsProvider.create();
        var masterUserRole = System.getenv("MASTER_USER_ROLE");
        var transferId = System.getenv("TRANSFER_ID");
        var environment = System.getenv("ENVIRONMENT");

        var transferClient = TransferClient.builder()
                .credentialsProvider(provider)
                .region(Region.EU_WEST_2)
                .build();

        var adaptor = SftpTransferAdapter.builder()
                .serverId(transferId)
                .userRoleArn(masterUserRole)
                .transferClient(transferClient)
                .environment(environment)
                .build();

    }

}
