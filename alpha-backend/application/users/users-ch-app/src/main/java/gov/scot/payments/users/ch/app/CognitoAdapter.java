package gov.scot.payments.users.ch.app;

import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import lombok.extern.slf4j.Slf4j;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient;
import software.amazon.awssdk.services.cognitoidentityprovider.model.*;

import java.util.ArrayList;
import java.util.Collections;

@Slf4j
public class CognitoAdapter {

    private final CognitoIdentityProviderClient cognitoclient;
    private final String userPoolId;
    private final int MAX_RESOURCE_RESULTS = 50;

    public CognitoAdapter(String userPoolId, CognitoIdentityProviderClient cognitoclient){
        this.cognitoclient = cognitoclient;
        this.userPoolId = userPoolId;
    }

    public UserType addCognitoUser(String userName, String email, String permittedActions){

        var emailAttr = AttributeType.builder()
                                     .name("email")
                                     .value(email)
                                     .build();
        var customAttr = AttributeType.builder()
                .name("custom:permitted_actions")
                .value(permittedActions)
                .build();
        var list = List.of(emailAttr, customAttr);

        var response = cognitoclient.adminCreateUser(
                AdminCreateUserRequest.builder()
                        .userPoolId(userPoolId)
                        .username(userName)
                        .userAttributes(list.toJavaList())
                        .desiredDeliveryMediumsWithStrings("EMAIL")
                        .build());
        log.info("Added user {} to user pool ", userName);
        return response.user();
    }

    public void deleteCognitoUser(String userName){
        var response = cognitoclient.adminDeleteUser(
                AdminDeleteUserRequest.builder()
                        .userPoolId(userPoolId)
                        .username(userName)
                        .build());
        log.info("Deleted user {} from user pool ", userName);
    }

    public List<AttributeType> getAttributes(String username){
        var response = cognitoclient.adminGetUser(AdminGetUserRequest.builder()
                                                      .username(username)
                                                      .userPoolId(userPoolId)
                                                      .build());
        return List.ofAll(response.userAttributes());
    }

    public List<UserType> listUsers(){

        var response = cognitoclient.listUsers(ListUsersRequest.builder()
                                                               .userPoolId(userPoolId)
                                                               .build());
        return List.ofAll(response.users());
    }

    public List<GroupType> getUserGroups(String username){

        var response = cognitoclient.adminListGroupsForUser(AdminListGroupsForUserRequest.builder()
                                                                                         .username(username)
                                                                                         .userPoolId(userPoolId)
                                                                                         .build());

        return List.ofAll(response.groups());
    }

    public List<String> listResourceServers(){

        var arrayList = new ArrayList<String>();
        var itr = cognitoclient.listResourceServersPaginator(ListResourceServersRequest.builder()
               .userPoolId(userPoolId)
               .maxResults(MAX_RESOURCE_RESULTS) // this is the max results per page
               .build()).iterator();
        itr.forEachRemaining(
               page -> page.resourceServers().forEach(
                       server -> server.scopes().forEach(
                               scope -> arrayList.add(server.name()+"/"+scope.scopeName())))
        );

        return List.ofAll(arrayList);
    }

    public List<GroupType> listGroups(){

        var response = cognitoclient.listGroups(
                ListGroupsRequest.builder()
                        .userPoolId(userPoolId)
                        .build()
        );
        return List.ofAll(response.groups());
    }


    public Option<ResourceServerType> describeResourceService(String scope){

        try {
            var response = cognitoclient.describeResourceServer(DescribeResourceServerRequest.builder()
                                                                                             .identifier(scope)
                                                                                             .userPoolId(userPoolId)
                                                                                             .build());
            return Option.of(response.resourceServer());

        } catch(ResourceNotFoundException e){
            return Option.none();
        }
    }

    public ResourceServerType addResourceServer(String resourceName){
        var response = cognitoclient.createResourceServer(CreateResourceServerRequest.builder()
                .identifier(resourceName)
                .name(resourceName)
                .userPoolId(userPoolId)
                .build());
        log.info("Created resource server {}", resourceName);
        return response.resourceServer();
    }

    private List<ResourceServerScopeType> getExistingScopesForResourceServer(String resourceName){

        var response = cognitoclient.describeResourceServer(DescribeResourceServerRequest.builder()
                .userPoolId(userPoolId)
                .identifier(resourceName)
                .build());
        return List.ofAll(response.resourceServer().scopes());
    }

    public void removeAllPrefixedOathScopesFromResourceServers(String resourceScopePrefix){
        listResourceServers()
                .map(s -> new Tuple2<>(s,getExistingScopesForResourceServer(s)))
                .map(p -> p.map2(l -> l.map(ResourceServerScopeType::scopeName)))
                .map(p -> p.map2(l -> l.filter(r -> r.startsWith(resourceScopePrefix))))
                .forEach(p -> removeOathScopesFromResourceServer(p._1,p._2));
    }


    public ResourceServerType removeOathScopesFromResourceServer(String resourceName, List<String> resourceScopeStr){

        var existingScopes = getExistingScopesForResourceServer(resourceName);
        var scopes = resourceScopeStr.map(scope -> ResourceServerScopeType.builder()
                                                                          .scopeName(scope)
                                                                          .scopeDescription(scope)
                                                                          .build());

        existingScopes = existingScopes.removeAll(scopes);

        var response = cognitoclient.updateResourceServer(UpdateResourceServerRequest.builder()
                                                                                     .userPoolId(userPoolId)
                                                                                     .name(resourceName)
                                                                                     .identifier(resourceName)
                                                                                     .scopes(existingScopes.toJavaList())
                                                                                     .build());
        log.info("Removed scopes {} from resource server {}", resourceScopeStr.toString(), resourceName);
        return response.resourceServer();
    }

    public void addOathScopesToResourceServers(Set<String> resourceScopeStr){
        listResourceServers()
                .forEach(s -> addOathScopesToResourceServer(s,resourceScopeStr));
    }

    public ResourceServerType addOathScopesToResourceServer(String resourceName, Set<String> resourceScopeStr){

        var existingScopes = getExistingScopesForResourceServer(resourceName);
        var scopes = resourceScopeStr
                .map(scope -> ResourceServerScopeType.builder()
                        .scopeName(scope)
                        .scopeDescription(scope)
                        .build());
        scopes = scopes.addAll(existingScopes);

        var response = cognitoclient.updateResourceServer(UpdateResourceServerRequest.builder()
                .userPoolId(userPoolId)
                .name(resourceName)
                .identifier(resourceName)
                .scopes(scopes.toJavaList())
                .build());
        log.info("Added scopes {} from resource server {}", resourceScopeStr.toString(), resourceName);
        return response.resourceServer();
    }

    public void deleteResourceServer(String resourceName){
        var response = cognitoclient.deleteResourceServer(DeleteResourceServerRequest.builder()
                .identifier(resourceName)
                .userPoolId(userPoolId)
                .build());
        log.info(response.toString());
    }

    public GroupType addCognitoGroup(String groupName, String description){
        var response = cognitoclient.createGroup(CreateGroupRequest.builder()
                .groupName(groupName)
                .description(description)
                .userPoolId(userPoolId)
                .build());
        log.info("Added cognito group {} ", groupName);
        return response.group();
    }

    public void deleteCognitoGroup(String groupName){
        var response = cognitoclient.deleteGroup(DeleteGroupRequest.builder()
                .groupName(groupName)
                .userPoolId(userPoolId)
                .build());
        log.info("Deleted cognito group {} ", groupName);
    }

    public Option<GroupType> describeCognitoGroup(String groupName){
        try {
            var response = cognitoclient.getGroup(GetGroupRequest.builder()
                    .groupName(groupName)
                    .userPoolId(userPoolId)
                    .build());
            return Option.of(response.group());
        } catch (ResourceNotFoundException e){
            return Option.none();
        }
    }

    public Option<UserPoolClientDescription> getUserPoolClientIfExists(String name){

        var response = cognitoclient.listUserPoolClients(ListUserPoolClientsRequest.builder()
                .userPoolId(userPoolId)
                .build());

        return Option.ofOptional(response.userPoolClients().stream()
                .filter(client -> client.clientName().equals(name))
                .findFirst());
    }

    public List<UserPoolClientType> listUserPoolClients(){

        var response = cognitoclient.listUserPoolClients(ListUserPoolClientsRequest.builder()
                .userPoolId(userPoolId)
                .build());

        var descriptions = List.ofAll(response.userPoolClients());
        return descriptions.map(app -> describeUserPoolClient(app));
    }

    private UserPoolClientType describeUserPoolClient(UserPoolClientDescription description){
        return cognitoclient.describeUserPoolClient(DescribeUserPoolClientRequest.builder()
                .clientId(description.clientId())
                .userPoolId(userPoolId)
                .build()).userPoolClient();
    }


    public UserPoolClientType addAppClient(String applicationName){

        var response = cognitoclient.createUserPoolClient(CreateUserPoolClientRequest.builder()
                .clientName(applicationName)
                .userPoolId(userPoolId)
                .generateSecret(true)
                .allowedOAuthFlows(OAuthFlowType.CLIENT_CREDENTIALS)
                .allowedOAuthScopes(Collections.emptyList())
                .explicitAuthFlows(ExplicitAuthFlowsType.ADMIN_NO_SRP_AUTH)
                .build());
        log.info("Added app client {} with id {} ", applicationName, response.userPoolClient().clientId());
        return response.userPoolClient();
    }

    public void removeAppClient(String applicationId){
        var response = cognitoclient.deleteUserPoolClient(DeleteUserPoolClientRequest.builder()
                .clientId(applicationId)
                .userPoolId(userPoolId)
                .build());
        log.info("Delete app client {} with id {} ", applicationId);
    }

    public UserPoolClientType updateClientIdRoles(String clientId, List<String> scopes){
        var response = cognitoclient.updateUserPoolClient(UpdateUserPoolClientRequest.builder()
                .allowedOAuthScopes(scopes.toJavaList())
                .allowedOAuthFlows(OAuthFlowType.CLIENT_CREDENTIALS)
                .clientId(clientId)
                .userPoolId(userPoolId)
                .build());

        return response.userPoolClient();
    }

    public void assignUserToGroup(String userName, String groupName){
        var response = cognitoclient.adminAddUserToGroup(AdminAddUserToGroupRequest.builder()
                .groupName(groupName)
                .username(userName)
                .userPoolId(userPoolId)
                .build());
        log.info("Assigned user {} to group {} ", userName, groupName);
    }

    public void removeUserFromGroup(String userName, String groupName){
        var response = cognitoclient.adminRemoveUserFromGroup(AdminRemoveUserFromGroupRequest.builder()
                .groupName(groupName)
                .username(userName)
                .userPoolId(userPoolId)
                .build());
        log.info(response.toString());
    }

    public void updateUserPermissions(String userName, String permittedActions){

        var customAttr = AttributeType.builder()
                                      .name("custom:permitted_actions")
                                      .value(permittedActions)
                                      .build();

        var response = cognitoclient.adminUpdateUserAttributes(AdminUpdateUserAttributesRequest.builder()
                .username(userName)
                .userPoolId(userPoolId)
                .userAttributes(customAttr)
                .build());

        log.info("Added permissions {} to user ", permittedActions, userName);

    }
}
