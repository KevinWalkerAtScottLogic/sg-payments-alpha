package gov.scot.payments.users.ch.app;

public class InvalidSftpPathException extends RuntimeException {
    public InvalidSftpPathException(String s) {
        super(s);
    }
}
