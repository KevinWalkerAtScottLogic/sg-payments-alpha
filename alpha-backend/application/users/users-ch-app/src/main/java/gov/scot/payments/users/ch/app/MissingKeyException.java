package gov.scot.payments.users.ch.app;

public class MissingKeyException extends RuntimeException {
    public MissingKeyException(String message){
        super(message);
    }
}
