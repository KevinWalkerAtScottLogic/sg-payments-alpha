{{- define "users-users-ch-app.env" -}}
- name: COGNITO_USER_POOL_ID
  valueFrom:
    configMapKeyRef:
      name: cognito-info
      key: user_pool_id
- name: FILE_BUCKET_NAME
  valueFrom:
    configMapKeyRef:
      name: sftp-info
      key: bucket_name
- name: SFTP_SERVER_ID
  valueFrom:
    configMapKeyRef:
      name: sftp-info
      key: server_id
- name: SFTP_USER_ROLE
  valueFrom:
    configMapKeyRef:
      name: sftp-info
      key: user_role_arn
- name: LOCAL
  value: {{ .Values.${projectValuesName}.local | quote }}
{{- end -}}