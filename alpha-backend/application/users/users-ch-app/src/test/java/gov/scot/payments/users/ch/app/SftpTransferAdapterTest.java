package gov.scot.payments.users.ch.app;

import io.vavr.collection.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import software.amazon.awssdk.services.transfer.TransferClient;
import software.amazon.awssdk.services.transfer.model.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class SftpTransferAdapterTest {

    private SftpTransferAdapter transferAdapter;
    private TransferClient mockTransfer;
    private final static String TEST_SERVER_ID = "testserver";
    private final String TEST_USER_ARN = "roleArn";
    private final String TEST_ENVIRONMENT = "test_environment";

    @BeforeEach
    void setUp() {
        mockTransfer = Mockito.mock(TransferClient.class);
        transferAdapter = SftpTransferAdapter.builder()
                .transferClient(mockTransfer)
                .environment(TEST_ENVIRONMENT)
                .serverId(TEST_SERVER_ID)
                .userRoleArn(TEST_USER_ARN)
                .build();
    }

    @Test
    void createUser() {

        when(mockTransfer.listUsers((ListUsersRequest) any())).thenReturn(ListUsersResponse.builder().build());

        transferAdapter.createUser("test-user", "testkey");
        ArgumentCaptor<CreateUserRequest> argumentCaptor = ArgumentCaptor.forClass(CreateUserRequest.class);
        verify(mockTransfer, times(1)).createUser(argumentCaptor.capture());
        var request = argumentCaptor.getValue();

        assertEquals( request.sshPublicKeyBody(), "testkey");
        assertEquals( request.userName(), "test-user");
        assertEquals( request.serverId(), TEST_SERVER_ID);
        assertEquals( request.role(), TEST_USER_ARN);
    }

    @Test
    void createUserDoesNotCallWhenUserExists() {

        when(mockTransfer.listUsers((ListUsersRequest) any()))
                .thenReturn(ListUsersResponse.builder()
                .users(ListedUser.builder().userName("test-user").build())
                .build());

        transferAdapter.createUser("test-user", "testkey");
        verify(mockTransfer, times(0)).createUser((CreateUserRequest) any());
    }

    @Test
    void testCreateUserThrowsExceptionWithUserExists() {
        when(mockTransfer.listUsers((ListUsersRequest) any())).thenReturn(ListUsersResponse.builder().build());
        when(mockTransfer.createUser((CreateUserRequest) any())).thenThrow(ResourceExistsException.class);
        assertThrows(SftpUserException.class, () -> transferAdapter.createUser("test-user", "testkey"));
    }

    @Test
    void testUpdateUserPolicyAddsToExistingMappings() {

        var existingMappings = HomeDirectoryMapEntry.builder().entry("/existing/").target("/mymappedfolder2/").build();
        var mapping = HomeDirectoryMapEntry.builder().entry("/example/").target("/mymappedfolder/").build();
        var describeUserResponse = DescribeUserResponse.builder()
                .user(DescribedUser.builder().homeDirectoryMappings(existingMappings).build())
                .build();

        when(mockTransfer.describeUser((DescribeUserRequest) any())).thenReturn(describeUserResponse);

        transferAdapter.updateUserPolicy("test-user", List.of(mapping));

        ArgumentCaptor<UpdateUserRequest> argumentCaptor = ArgumentCaptor.forClass(UpdateUserRequest.class);
        verify(mockTransfer, times(1)).updateUser(argumentCaptor.capture());
        var request = argumentCaptor.getValue();
        assertEquals( request.serverId(), TEST_SERVER_ID);
        assertEquals(request.homeDirectoryType(), HomeDirectoryType.LOGICAL);
        var mappings = request.homeDirectoryMappings();
        assertEquals(2, mappings.size());
    }

    @Test
    void testRemoveHomeDirMapping() {

        var existing = HomeDirectoryMapEntry.builder().entry("/existing/").target("/mymappedfolder2/").build();
        var example = HomeDirectoryMapEntry.builder().entry("/example/").target("/mymappedfolder/").build();
        var describeUserResponse = DescribeUserResponse.builder()
                .user(DescribedUser.builder().homeDirectoryMappings(List.of(existing, example).toJavaList()).build())
                .build();
        when(mockTransfer.describeUser((DescribeUserRequest) any())).thenReturn(describeUserResponse);
        transferAdapter.removeHomeDirMappingFromUser("test-user", "/example/");
        ArgumentCaptor<UpdateUserRequest> argumentCaptor = ArgumentCaptor.forClass(UpdateUserRequest.class);
        verify(mockTransfer, times(1)).updateUser(argumentCaptor.capture());
        var request = argumentCaptor.getValue();
        assertEquals( request.serverId(), TEST_SERVER_ID);
        assertEquals(request.homeDirectoryType(), HomeDirectoryType.LOGICAL);
        var mappings = request.homeDirectoryMappings();
        assertEquals(1, mappings.size());
    }

    @Test
    void testDeleteUserWhenAllDirMappingsRemoved() {

        var existingMappings = HomeDirectoryMapEntry.builder().entry("/existing/").target("/mymappedfolder2/").build();
        var describeUserResponse = DescribeUserResponse.builder()
                .user(DescribedUser.builder().homeDirectoryMappings(existingMappings).build())
                .build();
        when(mockTransfer.describeUser((DescribeUserRequest) any())).thenReturn(describeUserResponse);
        transferAdapter.removeHomeDirMappingFromUser("test-user", "/existing/");
        verify(mockTransfer).deleteUser((DeleteUserRequest) any());

    }
}