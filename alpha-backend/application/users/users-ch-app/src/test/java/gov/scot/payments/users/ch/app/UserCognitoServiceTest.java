package gov.scot.payments.users.ch.app;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.user.*;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import software.amazon.awssdk.services.cognitoidentityprovider.model.*;

import java.net.MalformedURLException;
import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class UserCognitoServiceTest {

    private static final String TEST_CLIENT_NAME = "test-client-name";
    private static final String TEST_CLIENT_ID = "test-client-id";
    private static final String TEST_USER_NAME="test-username";
    private static final String TEST_EMAIL="test-email";
    private static final String TEST_SCOPE= "product.test-scope";
    private static final String TEST_RESOURCE="resource";
    private static final String TEST_RESOURCE_CREATE="resource:create";
    private static final String TEST_RESOURCE_READ="resource:read";
    private static final String TEST_ROLE="product.test-scope/resource:create";
    private static final String PAYMENTSFILE_ROLE="product.test-scope/paymentfiles:write";
    private static final String PAYMENTSFILE_WRITE="paymentfiles:write";
    private static final String TEST_SSH_PUB_KEY= "testkey";
    private static final List<String> TEST_RESOURCES = List.of("product.test-scope/resource:read", "product.test-scope/resource:create");

    private UserCognitoService userService;
    private CognitoAdapter mockCognitoAdapter;
    private UserSftpService sftpService;
    private SftpTransferAdapter mockTransferAdapter;

    @BeforeEach
    private void setUp(){
        mockCognitoAdapter = Mockito.mock(CognitoAdapter.class);
        mockTransferAdapter = Mockito.mock(SftpTransferAdapter.class);
        sftpService = Mockito.spy(UserSftpService.builder()
                .filePathGenerator(SftpFilePathGenerator.builder()
                        .fileBucket("bucket")
                        .environment("environment")
                        .build())
                .transferAdapter(mockTransferAdapter).build());
        userService = new UserCognitoService(mockCognitoAdapter, sftpService);
    }


    private User createExampleUser() {
        return User.builder()
                .name(TEST_USER_NAME)
                .email(TEST_EMAIL)
                .roles(HashSet.of(Role.parse(TEST_ROLE)))
                .groups(HashSet.of(new Group(TEST_SCOPE)))
                .build();
    }
    private User createExampleUserWithFilePermissionsRole() {
        return User.builder()
                .name(TEST_USER_NAME)
                .email(TEST_EMAIL)
                .sshPublicKey(TEST_SSH_PUB_KEY)
                .roles(HashSet.of(Role.parse(PAYMENTSFILE_ROLE)))
                .groups(HashSet.of(new Group(TEST_SCOPE)))
                .build();
    }

    private UserType createExampleCognitoUser() {
        return createExampleCognitoUserWithActionString("resource:create");
    }

    private UserType createExampleCognitoUserWithPaymentFileResource() {
        return createExampleCognitoUserWithActionString("paymentfiles:write");
    }

    private UserType createExampleCognitoUserWithActionString(String permittedActions) {
        return UserType.builder()
                .username(TEST_USER_NAME)
                .attributes(List.of(
                        AttributeType.builder().name("email").value(TEST_EMAIL).build(),
                        AttributeType.builder().name("custom:permitted_actions").value(permittedActions).build()
                ).toJavaList())
                .build();
    }

    private UserPoolClientType createExampleUserPoolClient() {
        return UserPoolClientType.builder()
                .clientName(TEST_CLIENT_NAME)
                .clientId(TEST_CLIENT_ID)
                .allowedOAuthFlows(OAuthFlowType.CLIENT_CREDENTIALS)
                .clientSecret("")
                .allowedOAuthScopes(TEST_RESOURCES.toJavaList())
                .build();
    }

    private ResourceServerType createExampleResourceResponse(){

        return ResourceServerType.builder()
                .name(TEST_SCOPE)
                .scopes(List.of(
                        ResourceServerScopeType.builder()
                                .scopeName(TEST_RESOURCE_CREATE)
                                .scopeDescription(TEST_RESOURCE_CREATE)
                                .build(),
                        ResourceServerScopeType.builder()
                                .scopeName(TEST_RESOURCE_READ)
                                .scopeDescription(TEST_RESOURCE_READ)
                                .build()
                        ).toJavaList())
                .build();
    }

    @Test
    void whenUserExistsAddUserThrowsException()  {
        var groupList = List.of(GroupType.builder().groupName(TEST_SCOPE).build());
        when(mockCognitoAdapter.listGroups()).thenReturn(groupList);
        when(mockCognitoAdapter.addCognitoUser(any(), any(), any())).thenThrow(UsernameExistsException.class);
        var user = createExampleUser();
        assertThrows(CognitoUserException.class, () -> userService.addUser(user));
    }

    @Test
    void testAddApplicationSucceeds() throws CognitoApplicationException {
        var exampleClient = createExampleUserPoolClient();
        when(mockCognitoAdapter.getUserPoolClientIfExists(anyString())).thenReturn(Option.none());
        when(mockCognitoAdapter.listResourceServers()).thenReturn(TEST_RESOURCES);
        when(mockCognitoAdapter.addAppClient(TEST_CLIENT_NAME)).thenReturn(exampleClient);
        when(mockCognitoAdapter.updateClientIdRoles(anyString(),any())).thenReturn(exampleClient);
        var exampleRoles = createExampleRoles();
        var application = userService.addApplication(TEST_CLIENT_NAME, TEST_SSH_PUB_KEY, exampleRoles);
        verify(mockCognitoAdapter).addAppClient(TEST_CLIENT_NAME);
        assertEquals(application.getClientId(), TEST_CLIENT_ID);
        assertEquals(application.getName(), TEST_CLIENT_NAME);
    }

    @Test
    void testAddApplicationThrowsExceptionWhenScopesNotPresent() throws CognitoApplicationException {
        when(mockCognitoAdapter.getUserPoolClientIfExists(anyString())).thenReturn(Option.none());
        var exampleClient = createExampleUserPoolClient();
        when(mockCognitoAdapter.listResourceServers()).thenReturn(List.of("invalid/myscope"));
        when(mockCognitoAdapter.addAppClient(TEST_CLIENT_NAME)).thenReturn(exampleClient);
        when(mockCognitoAdapter.updateClientIdRoles(anyString(),any())).thenReturn(exampleClient);
        var exampleRoles = createExampleRoles();
        assertThrows(CognitoScopeException.class, () -> userService.addApplication(TEST_CLIENT_NAME, TEST_SSH_PUB_KEY, exampleRoles));
    }

    @Test
    void testAddApplicationThrowsExceptionWhenApplicationExists() {
        var exampleClient = UserPoolClientDescription.builder()
                                                     .clientName(TEST_CLIENT_NAME)
                                                     .clientId(TEST_CLIENT_ID)
                                                     .build();
        when(mockCognitoAdapter.getUserPoolClientIfExists(any())).thenReturn(Option.of(exampleClient));
        assertThrows(CognitoApplicationException.class, ()-> userService.addApplication(TEST_CLIENT_NAME, TEST_SSH_PUB_KEY, HashSet.of()));
    }

    private Application createExampleApplication() {
        return Application.builder().name(TEST_CLIENT_NAME).clientId(TEST_CLIENT_ID).roles(HashSet.of()).build();
    }

    @Test
    void testRemoveApplicationSucceeds() throws CognitoApplicationException {
        var app = createExampleApplication();
        var clientDescription = UserPoolClientDescription.builder()
                .clientId(TEST_CLIENT_ID)
                .clientName(TEST_CLIENT_NAME)
                .build();
        when(mockCognitoAdapter.getUserPoolClientIfExists(app.getName())).thenReturn(Option.of(clientDescription));
        userService.removeApplication(app.getName());
        verify(mockCognitoAdapter).removeAppClient(TEST_CLIENT_ID);
    }

    @Test
    void testRemoveApplicationThrowsExceptionWhenGetApplicationReturnsEmpty() {
        var app = createExampleApplication();
        when(mockCognitoAdapter.getUserPoolClientIfExists(app.getName())).thenReturn(Option.none());
        assertThrows(CognitoApplicationException.class, ()-> userService.removeApplication(app.getName()));
    }

    private Set<Role> createExampleRoles() {
        var createAction = new Action(TEST_RESOURCE, "create");
        var readAction = new Action(TEST_RESOURCE, "read");
        var scope = Scope.parse(TEST_SCOPE);
        return HashSet.of(new Role(readAction, scope), new Role(createAction, scope));
    }

    @Test
    void testAssignRolesSucceeds() throws CognitoApplicationException {
        var exampleClient = createExampleUserPoolClient();
        when(mockCognitoAdapter.updateClientIdRoles(anyString(),any())).thenReturn(exampleClient);
        var clientDescription = UserPoolClientDescription.builder()
                .clientId(TEST_CLIENT_ID)
                .clientName(TEST_CLIENT_NAME)
                .build();
        when(mockCognitoAdapter.getUserPoolClientIfExists(TEST_CLIENT_NAME)).thenReturn(Option.of(clientDescription));
        userService.assignRolesToApplication(TEST_CLIENT_NAME, TEST_CLIENT_ID, createExampleRoles());
        verify(mockCognitoAdapter).updateClientIdRoles(TEST_CLIENT_ID,TEST_RESOURCES);
    }

    @Test
    void testAssignRolesThrowsExceptionWhenApplicationDoesNotExist() {
        doThrow(ResourceNotFoundException.class).when(mockCognitoAdapter).updateClientIdRoles(any(), any());
        assertThrows(CognitoApplicationException.class, ()-> userService.assignRolesToApplication(TEST_CLIENT_NAME, TEST_SSH_PUB_KEY, createExampleRoles()));
    }

    @Test
    void testAddScopeSucceeds() {
        when(mockCognitoAdapter.describeResourceService(anyString())).thenReturn(Option.none());
        when(mockCognitoAdapter.describeCognitoGroup(anyString())).thenReturn(Option.none());
        var scope = Scope.parse(TEST_SCOPE);
        var app = userService.addScope(scope);
        assertEquals(app.getName(), scope.getName());
        assertEquals(app.getParent(), scope.getParent());
    }

    @Test
    void testAddScopeIdempotent() {
        var scope = Scope.parse(TEST_SCOPE);
        when(mockCognitoAdapter.describeCognitoGroup(TEST_SCOPE)).thenReturn(Option.none());
        when(mockCognitoAdapter.describeResourceService(TEST_SCOPE)).thenReturn(Option.none());
        var app = userService.addScope(scope);

        when(mockCognitoAdapter.describeCognitoGroup(TEST_SCOPE)).thenReturn(Option.of(GroupType.builder().groupName(TEST_SCOPE).build()));
        when(mockCognitoAdapter.describeResourceService(TEST_SCOPE)).thenReturn(Option.of(ResourceServerType.builder().name(TEST_SCOPE).build()));

        // make sure if we call add scope twice we don't call add if it already exists
        app = userService.addScope(scope);
        assertEquals(app.getName(), scope.getName());
        assertEquals(app.getParent(), scope.getParent());

        verify(mockCognitoAdapter, times(1)).addCognitoGroup(eq(TEST_SCOPE), any());
        verify(mockCognitoAdapter, times(1)).addResourceServer(TEST_SCOPE);
    }

    @Test
    void testRemoveScopeSucceeds() throws CognitoScopeException {

        var cognitoUsersList = List.of(createExampleCognitoUser(), createExampleCognitoUser());
        when(mockCognitoAdapter.listUsers()).thenReturn(cognitoUsersList);
        when(mockCognitoAdapter.listUserPoolClients()).thenReturn(List.empty());
        setupMockCongitoToReturnGroups();

        var scope = Scope.parse(TEST_SCOPE);
        var app = userService.removeScope(scope);

        assertEquals(app.getName(), scope.getName());
        assertEquals(app.getParent(), scope.getParent());
        verify(mockCognitoAdapter).deleteResourceServer(scope.toString());
        verify(mockCognitoAdapter).deleteCognitoGroup(scope.toString());
        verifyNoInteractions(mockTransferAdapter);
    }

    @Test
    void testRemoveScopeRemovesSftpWhenPaymentFileUserExists() throws CognitoScopeException {

        var cognitoUsersList = List.of(createExampleCognitoUser(), createExampleCognitoUserWithPaymentFileResource());
        when(mockCognitoAdapter.listUsers()).thenReturn(cognitoUsersList);
        when(mockCognitoAdapter.listUserPoolClients()).thenReturn(List.empty());
        setupMockCongitoToReturnGroups();

        var scope = Scope.parse(TEST_SCOPE);
        var app = userService.removeScope(scope);

        assertEquals(app.getName(), scope.getName());
        assertEquals(app.getParent(), scope.getParent());
        verify(mockCognitoAdapter).deleteCognitoGroup(scope.toString());
        verify(mockCognitoAdapter).deleteResourceServer(scope.toString());
        // TODO verify the list is actually called?
        verify(sftpService).updateSftpPermissionsForScopeRemoval(eq(scope), any());
    }

    @Test
    void testRemoveScopeThrowsExceptionWhenResourceServerNotExists() {
        when(mockCognitoAdapter.listUsers()).thenReturn(List.empty());
        when(mockCognitoAdapter.listUserPoolClients()).thenReturn(List.empty());
        doThrow(ResourceNotFoundException.class).when(mockCognitoAdapter).deleteResourceServer(any());
        var scope = Scope.parse(TEST_SCOPE);
        assertThrows(CognitoScopeException.class, ()-> userService.removeScope(scope));
    }

    @Test
    void testRemoveScopeThrowsExceptionWhenCognitoGroupNotExists() {
        when(mockCognitoAdapter.listUsers()).thenReturn(List.empty());
        when(mockCognitoAdapter.listUserPoolClients()).thenReturn(List.empty());
        doThrow(ResourceNotFoundException.class).when(mockCognitoAdapter).deleteCognitoGroup(any());
        var scope = Scope.parse(TEST_SCOPE);
        assertThrows(CognitoScopeException.class, ()-> userService.removeScope(scope));
    }

    @Test
    void testRemoveScopeThrowsExceptionWhenGroupNotExists() {
        when(mockCognitoAdapter.listUsers()).thenReturn(List.empty());
        when(mockCognitoAdapter.listUserPoolClients()).thenReturn(List.empty());
        doThrow(ResourceNotFoundException.class).when(mockCognitoAdapter).deleteResourceServer(any());
        var scope = Scope.parse(TEST_SCOPE);
        assertThrows(CognitoScopeException.class, ()-> userService.removeScope(scope));
    }

    @Test
    void testAddUserWithGroupsAndRolesSucceeds() throws CognitoUserException {


        when(mockCognitoAdapter.addCognitoUser(TEST_USER_NAME, TEST_EMAIL, TEST_RESOURCE_CREATE)).thenReturn(createExampleCognitoUser());
        setupMockCongitoToReturnGroups();
        var user = createExampleUser();
        var createdUser = userService.addUser(user);
        assertThat(createdUser).isEqualToComparingFieldByField(user);
        verify(mockCognitoAdapter).addCognitoUser(TEST_USER_NAME, TEST_EMAIL, TEST_RESOURCE_CREATE);
        verify(mockCognitoAdapter).assignUserToGroup(TEST_USER_NAME, TEST_SCOPE);
        verifyNoInteractions(mockTransferAdapter);
    }

    @Test
    void testAddUserWithSftpGroupsCallsSftpClient() throws CognitoUserException {
        when(mockCognitoAdapter.addCognitoUser(TEST_USER_NAME, TEST_EMAIL, PAYMENTSFILE_WRITE)).thenReturn(createExampleCognitoUserWithPaymentFileResource());
        setupMockCongitoToReturnGroups();
        var user = createExampleUserWithFilePermissionsRole();
        var createdUser = userService.addUser(user);
        assertThat(createdUser).isEqualToIgnoringGivenFields(user, "sshPublicKey");
        verify(mockCognitoAdapter).addCognitoUser(TEST_USER_NAME, TEST_EMAIL, PAYMENTSFILE_WRITE);
        verify(mockCognitoAdapter).assignUserToGroup(TEST_USER_NAME, TEST_SCOPE);
        verify(sftpService).addUserToSftp(user);
    }



    private void setupMockCongitoToReturnGroups() {
        var groupList = List.of(GroupType.builder().groupName(TEST_SCOPE).build());
        when(mockCognitoAdapter.listGroups()).thenReturn(groupList);
        when(mockCognitoAdapter.getUserGroups(anyString())).thenReturn(groupList);
    }

    @Test
    void testAddUserThrowsExceptionWhenGroupDoesntExist() throws CognitoUserException {
        when(mockCognitoAdapter.addCognitoUser(TEST_USER_NAME, TEST_EMAIL, TEST_RESOURCE_CREATE)).thenReturn(createExampleCognitoUser());
        when(mockCognitoAdapter.listGroups()).thenReturn(List.of());
        var user = createExampleUser().toBuilder()
                .groups(HashSet.of(new Group(TEST_SCOPE)))
                .build();
        var exception = assertThrows(CognitoScopeException.class, ()-> userService.addUser(user));
        assertEquals("Cognito group "+TEST_SCOPE+" does not exist.", exception.getMessage());
    }

    @Test
    void testAddUserThrowsExceptionIfUserAlreadyExists() throws CognitoUserException {
        var user = createExampleUser();
        var groupList = List.of(GroupType.builder().groupName(TEST_SCOPE).build());
        when(mockCognitoAdapter.listGroups()).thenReturn(groupList);
        when(mockCognitoAdapter.addCognitoUser(any(), any(), any())).thenThrow(UsernameExistsException.class);
        assertThrows(CognitoUserException.class, ()-> userService.addUser(user));
    }

    @Test
    void testAddUserThrowsExceptionIfRolesAndGroupsDontMatch() throws CognitoUserException {
        var user = User.builder()
                .name(TEST_USER_NAME)
                .email(TEST_EMAIL)
                .roles(HashSet.of(Role.parse(TEST_ROLE)))
                .groups(HashSet.of(new Group("test2")))
                .build();
        var exception = assertThrows(CognitoScopeException.class, ()-> userService.addUser(user));
        assertEquals("User "+TEST_USER_NAME+" must have matching groups and roles.", exception.getMessage());
    }

    @Test
    void testRemoveUserSuceeds() throws CognitoUserException {
        var user = createExampleUser();
        userService.removeUser(user.getName());
        verify(mockCognitoAdapter).deleteCognitoUser(TEST_USER_NAME);
    }

    @Test
    void testRemoveUserThrowsExceptionIfUserAlreadyExists() {
        var user = createExampleUser();
        doThrow(UserNotFoundException.class).when(mockCognitoAdapter).deleteCognitoUser(TEST_USER_NAME);
        assertThrows(CognitoUserException.class, ()-> userService.removeUser(user.getName()));
    }

    @Test
    void testAddActionsToOauthServer() {

        var actionStrList = HashSet.of(TEST_RESOURCE_CREATE, TEST_RESOURCE_READ);
        when(mockCognitoAdapter.addOathScopesToResourceServer(TEST_SCOPE, actionStrList)).thenReturn(createExampleResourceResponse());
        var actionList = List.of(new Action(TEST_RESOURCE, "create"), new Action(TEST_RESOURCE, "read"));
        var resourceWithVerbs = ResourceWithVerbs.builder().resource(TEST_RESOURCE).verbs(HashSet.of("read")).build();
        var response = userService.addActionsToOauthServers(resourceWithVerbs);
        assertEquals(resourceWithVerbs.getResource(), response.getResource());
        assertEquals(resourceWithVerbs.getVerbs(), response.getVerbs());
    }

    @Test
    void testRemoveActionsFromOauthServer() {

        var actionStrList = List.of(TEST_RESOURCE_CREATE, TEST_RESOURCE_READ);
        when(mockCognitoAdapter.removeOathScopesFromResourceServer(TEST_SCOPE, actionStrList)).thenReturn(createExampleResourceResponse());
        var actionList = List.of(new Action(TEST_RESOURCE, "create"), new Action(TEST_RESOURCE, "read"));
        var resourceWithVerbs = ResourceWithVerbs.builder().resource(TEST_RESOURCE).verbs(HashSet.of("read")).build();
        var response = userService.removeActionsFromOauthServers(TEST_RESOURCE);
        assertEquals(TEST_RESOURCE, response);
    }

    @Test
    void testupdatePermissionsForFileChange() throws MalformedURLException {
        var cognitoUsersList = List.of(createExampleCognitoUser(), createExampleCognitoUserWithPaymentFileResource());
        when(mockCognitoAdapter.listUsers()).thenReturn(cognitoUsersList);
        when(mockCognitoAdapter.listUserPoolClients()).thenReturn(List.empty());
        setupMockCongitoToReturnGroups();
        var s3Url = new URL("https://s3.amazonaws.com/folder1/");
        userService.updatePermissionsForFileChange(new CompositeReference("test-scope", "product"), s3Url);
        List<Subject> usersList = List.of(createExampleUser(), createExampleUserWithFilePermissionsRole());
        var scope = Scope.parse("test-scope.product");
        verify(sftpService, times(1)).updateRelevantSubjectsForFileResources(usersList, scope, "/folder1/");
    }
}