package gov.scot.payments.users.ch.app;

import gov.scot.payments.model.user.*;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import software.amazon.awssdk.services.transfer.model.HomeDirectoryMapEntry;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class UserSftpServiceTest {

    private SftpTransferAdapter adapter;
    private UserSftpService service;

    private final String TEST_FILE_BUCKET = "mybucket";
    private final String TEST_ENVIRONMENT = "test_environment";

    @BeforeEach
    void setUp() {
        adapter = Mockito.mock(SftpTransferAdapter.class);
        var pathGenerator = SftpFilePathGenerator.builder()
                            .fileBucket(TEST_FILE_BUCKET)
                            .environment(TEST_ENVIRONMENT)
                            .build();
        service = UserSftpService.builder()
                .filePathGenerator(pathGenerator)
                .transferAdapter(adapter)
                .sftpNameGenerator(name -> "sftp_"+name)
                .build();
    }

    @Test
    void testdoesUserHaveSftpPermission() {
        var user = User.builder()
                .name("test")
                .email("testemail")
                .roles(HashSet.of(Role.parse("customer.scope/paymentfiles:write")))
                .groups(HashSet.of(new Group("customer.scope")))
                .build();
        assertTrue(service.doesSubjectHaveSftpPermission(user));
        var nonpaymentUser = user.toBuilder().roles(HashSet.of(Role.parse("customer.scope/payments:write"))).build();
        assertFalse(service.doesSubjectHaveSftpPermission(nonpaymentUser));
    }

    @Test
    void testaddUserToSftp() {

        var user = User.builder()
                .name("test")
                .email("testmail")
                .roles(HashSet.of(Role.parse("customer.scope/paymentfiles:write"), Role.parse("customer.scope2/paymentfiles:write")))
                .groups(HashSet.of(new Group("scope"), new Group(("scope2"))))
                .sshPublicKey("testkey")
                .build();

        String targetBase = "/"+TEST_FILE_BUCKET+"/"+TEST_ENVIRONMENT;
        HomeDirectoryMapEntry homeDir1 = getHomeDirectoryMapEntry("/scope",  targetBase+ "/customer/scope");
        HomeDirectoryMapEntry homeDir2 = getHomeDirectoryMapEntry("/scope2", targetBase+ "/customer/scope2");
        service.addUserToSftp(user);
        verify(adapter, times(1)).createUser("sftp_test", "testkey");
        verify(adapter, times(1)).updateUserPolicy("sftp_test", List.of(homeDir2, homeDir1));

    }

    @Test
    void testaddApplicationToSftp() {
        service.addApplicationToSftp("test", "testkey");
        verify(adapter, times(1)).createUser("sftp_test", "testkey");
    }

    @Test
    void testSetApplicationPermissions() {

        String targetBase = "/"+TEST_FILE_BUCKET+"/"+TEST_ENVIRONMENT;
        HomeDirectoryMapEntry homeDir1 = getHomeDirectoryMapEntry("/scope",  targetBase+ "/customer1/scope");
        HomeDirectoryMapEntry homeDir2 = getHomeDirectoryMapEntry("/scope2", targetBase+ "/customer2/scope2");
        service.setApplicationPermissions("test", HashSet.of(Role.parse("customer1.scope/paymentfiles:write"), Role.parse("customer2.scope2/paymentfiles:write")));
        verify(adapter, times(1)).updateUserPolicy("sftp_test", List.of(homeDir2, homeDir1));

    }

    private HomeDirectoryMapEntry getHomeDirectoryMapEntry(String entry, String target) {
        return HomeDirectoryMapEntry.builder()
                .entry(entry)
                .target(target)
                .build();
    }

    @Test
    void testaddUserToSftpThrowsExceptionWhenMissingKey() {
        var user = User.builder()
                .name("test")
                .email("testmail")
                .roles(HashSet.of(Role.parse("scope/paymentfiles:write"), Role.parse("scope2/paymentfiles:write")))
                .groups(HashSet.of(new Group("scope"), new Group(("scope2"))))
                .build();
        assertThrows(MissingKeyException.class, () -> service.addUserToSftp(user));
    }

    @Test
    void testupdateSftpPermissionsForScopeRemoval() {

        var user = User.builder()
                .name("test")
                .email("testmail")
                .roles(HashSet.of(Role.parse("scope/paymentfiles:write"), Role.parse("scope2/paymentfiles:write")))
                .groups(HashSet.of(new Group("scope"), new Group(("scope2"))))
                .build();

        var app = Application.builder()
                .name("testapp")
                .roles(HashSet.of(Role.parse("scope/paymentfiles:write"), Role.parse("scope2/paymentfiles:write")))
                .build();

        service.updateSftpPermissionsForScopeRemoval(new Scope("scope2"), List.of(user, app));

        verify(adapter).removeHomeDirMappingFromUser("sftp_test", "scope2");
        verify(adapter).removeHomeDirMappingFromUser("sftp_testapp", "scope2");

    }
}