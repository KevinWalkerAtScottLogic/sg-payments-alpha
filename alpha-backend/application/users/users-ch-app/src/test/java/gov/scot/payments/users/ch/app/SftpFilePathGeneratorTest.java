package gov.scot.payments.users.ch.app;

import gov.scot.payments.model.user.Scope;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SftpFilePathGeneratorTest {

    @Test
    void test_buildFilePath() {

        var generator = SftpFilePathGenerator.builder()
                .fileBucket("mybucket")
                .environment("testbranch")
                .build();

        var path = generator.getFilePathFromScope(Scope.parse("customer.product"));
        assertEquals("/mybucket/testbranch/customer/product", path);

    }

    @Test
    void test_buildFilePathWithNoParent() {

        var generator = SftpFilePathGenerator.builder()
                .fileBucket("mybucket")
                .environment("testbranch")
                .build();

        var scope = Scope.parse("customer");
        var path = generator.getFilePathFromScope(scope);
        assertEquals("/mybucket/testbranch/customer", path);

    }

    @Test
    void test_buildFilePathWithNoParentOrChildThrowsException() {

        var generator = SftpFilePathGenerator.builder()
                .fileBucket("mybucket")
                .environment("testbranch")
                .build();

        assertThrows(InvalidSftpPathException.class, () ->  generator.getFilePathFromScope(new Scope()));

    }
}