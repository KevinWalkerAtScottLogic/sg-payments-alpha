package gov.scot.payments.users.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Scope;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "users", type = "userFileResourcesUpdated")
@NoArgsConstructor
@ToString
public class UserFileResourcesUpdatedEvent extends EventWithCauseImpl implements HasKey<String> {

    @NonNull Scope scope;

    public UserFileResourcesUpdatedEvent(Scope scope){
        this.scope = scope;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return scope.getName();
    }
}
