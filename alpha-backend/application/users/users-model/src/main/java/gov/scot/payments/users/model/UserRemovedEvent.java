package gov.scot.payments.users.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "users", type = "userRemoved")
@NoArgsConstructor
public class UserRemovedEvent extends EventWithCauseImpl implements HasKey<String> {

    @NonNull private String user;
    @NonNull private String email;

    public UserRemovedEvent(String user, String email){
        this.user = user;
        this.email = email;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return user;
    }
}
