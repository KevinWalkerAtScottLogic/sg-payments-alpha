package gov.scot.payments.users.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.model.user.User;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "users", type = "assignUserRoles")
@NoArgsConstructor
public class AssignUserRolesCommand extends CommandImpl implements HasKey<String> {

    @NonNull private User user;

    public AssignUserRolesCommand(User user, boolean reply){
        super(reply);
        this.user = user;
    }

    @MessageConstructor(role = "users:Write")
    public static AssignUserRolesCommand fromRequest(User user, boolean reply, Long stateVersion, Subject principal){
        return new AssignUserRolesCommand(user,reply);
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return user.getName();
    }
}
