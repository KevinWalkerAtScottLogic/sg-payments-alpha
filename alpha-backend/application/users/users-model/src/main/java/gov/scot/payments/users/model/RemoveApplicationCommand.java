package gov.scot.payments.users.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Subject;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "users", type = "removeApplication")
@NoArgsConstructor
public class RemoveApplicationCommand extends CommandImpl implements HasKey<String> {

    @NonNull private String clientName;

    public RemoveApplicationCommand(String clientName, boolean reply){
        super(reply);
        this.clientName = clientName;
    }

    @MessageConstructor(role = "users:Write")
    public static RemoveApplicationCommand fromRequest(String clientId, boolean reply, Long stateVersion, Subject principal){
        return new RemoveApplicationCommand(clientId,reply);
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return clientName;
    }
}
