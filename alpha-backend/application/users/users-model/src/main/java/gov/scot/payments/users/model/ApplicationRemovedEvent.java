package gov.scot.payments.users.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "users", type = "applicationRemoved")
@NoArgsConstructor
public class ApplicationRemovedEvent extends EventWithCauseImpl implements HasKey<String> {

    @NonNull private String application;

    public ApplicationRemovedEvent(String application){
        this.application = application;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return application;
    }
}
