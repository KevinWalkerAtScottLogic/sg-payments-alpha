package gov.scot.payments.users.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Subject;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "users", type = "removeUser")
@NoArgsConstructor
public class RemoveUserCommand extends CommandImpl implements HasKey<String> {

    @NonNull private String user;
    @NonNull private String email;

    public RemoveUserCommand(String user, String email, boolean reply){
        super(reply);
        this.user = user;
        this.email = email;
    }

    @MessageConstructor(role = "users:Write")
    public static RemoveUserCommand fromRequest(RemoveUserRequest request, boolean reply, Long stateVersion, Subject principal){
        return new RemoveUserCommand(request.getUserName(), request.getEmail(), reply);
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return user;
    }
}
