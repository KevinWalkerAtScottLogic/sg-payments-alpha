package gov.scot.payments.users.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Application;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.model.user.User;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "users", type = "addApplication")
@NoArgsConstructor
public class AddApplicationCommand extends CommandImpl implements HasKey<String> {

    @NonNull private String application;
    @Nullable private String sshPublicKey;

    public AddApplicationCommand(String application, boolean reply){
        super(reply);
        this.application = application;
    }

    @MessageConstructor(role = "users:Write")
    public static AddApplicationCommand fromRequest(String application, boolean reply, Long stateVersion, Subject principal){
        return new AddApplicationCommand(application,reply);
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return application;
    }
}
