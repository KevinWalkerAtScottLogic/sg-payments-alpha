package gov.scot.payments.users.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Subject;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "users", type = "removeResource")
@NoArgsConstructor
public class RemoveResourceCommand extends CommandImpl implements HasKey<String> {

    @NonNull private String resource;

    public RemoveResourceCommand(String resource, boolean reply){
        super(reply);
        this.resource = resource;
    }

    @MessageConstructor(role = "users:Write")
    public static RemoveResourceCommand fromRequest(String resource, boolean reply, Long stateVersion, Subject principal){
        return new RemoveResourceCommand(resource,reply);
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return resource;
    }
}
