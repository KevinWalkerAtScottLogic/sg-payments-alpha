package gov.scot.payments.users.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Scope;
import gov.scot.payments.model.user.User;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "users", type = "userAdded")
@NoArgsConstructor
public class UserAddedEvent extends EventWithCauseImpl implements HasKey<String> {

    @NonNull private User user;

    public UserAddedEvent(User user){
        this.user = user;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return user.getName();
    }
}
