package gov.scot.payments.users.model;

import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Builder
public class RemoveUserRequest {

    @NonNull private String userName;
    @NonNull private String email;

}
