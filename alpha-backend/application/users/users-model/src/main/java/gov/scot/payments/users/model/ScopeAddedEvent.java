package gov.scot.payments.users.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Scope;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "users", type = "scopeAdded")
@NoArgsConstructor
public class ScopeAddedEvent extends EventWithCauseImpl implements HasKey<String> {

    @NonNull private Scope scope;

    public ScopeAddedEvent(Scope scope){
        this.scope = scope;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return scope.getName();
    }
}
