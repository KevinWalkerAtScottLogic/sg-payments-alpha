package gov.scot.payments.users.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Scope;
import gov.scot.payments.model.user.Subject;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "users", type = "addScope")
@NoArgsConstructor
@ToString
public class AddScopeCommand extends CommandImpl implements HasKey<String> {

    @NonNull private Scope scope;

    public AddScopeCommand(Scope scope, boolean reply){
        super(reply);
        this.scope = scope;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return scope.getName();
    }
}
