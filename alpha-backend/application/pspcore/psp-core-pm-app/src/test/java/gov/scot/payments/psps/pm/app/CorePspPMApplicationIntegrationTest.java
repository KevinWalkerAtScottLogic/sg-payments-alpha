package gov.scot.payments.psps.pm.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.Event;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.event.PaymentCancelApprovedEvent;
import gov.scot.payments.payments.model.event.PaymentReadyForSubmissionEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentAcceptedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentCancelFailedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentCancelSuccessEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentCompleteEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentRejectedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentReturnedEvent;
import gov.scot.payments.psps.method.model.event.MethodPaymentSubmissionFailedEvent;
import gov.scot.payments.psps.model.command.CancelPaymentCommand;
import gov.scot.payments.psps.model.command.CompletePaymentCancelCommand;
import gov.scot.payments.psps.model.command.CompletePaymentCommand;
import gov.scot.payments.psps.model.command.CompletePaymentSubmissionCommand;
import gov.scot.payments.psps.model.command.FailPaymentCancelCommand;
import gov.scot.payments.psps.model.command.FailPaymentSubmissionCommand;
import gov.scot.payments.psps.model.command.ReRoutePaymentCommand;
import gov.scot.payments.psps.model.command.ReturnPaymentCommand;
import gov.scot.payments.psps.model.command.RoutePaymentCommand;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.springframework.context.annotation.Import;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.apache.kafka.streams.KafkaStreams;
import org.springframework.beans.factory.annotation.Autowired;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.spring.TestConfiguration;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ApplicationIntegrationTest(classes = {CorePspPMApplicationIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.PROCESS_MANAGER
        , properties = {"events.destinations=test"})
public class CorePspPMApplicationIntegrationTest {

    @BeforeEach
    public void setUp(EmbeddedBrokerClient brokerClient
            , @Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter
            , @Value("${commands.destination}") String commandTopic){
        brokerClient.getBroker().addTopicsIfNotExists(commandTopic);
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(20));
     }

    @Test
    public void testPaymentReadyForSubmission(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){
        Payment payment = makePayment();

        var event = PaymentReadyForSubmissionEvent.builder()
                .carriedState(payment)
                .correlationId(UUID.randomUUID())
                .stateVersion(1L)
                .build();
        var command = RoutePaymentCommand.builder()
                .payment(payment)
                .build();

        sendEventExternal(brokerClient, event);
        readAndVerifyCommand(brokerClient,commandTopic, command,"payment");
    }

    @Test
    public void testCancelApproved(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){
        Payment payment = makePayment();

        var event = PaymentCancelApprovedEvent.builder()
                                              .carriedState(payment)
                                              .correlationId(UUID.randomUUID())
                                              .stateVersion(1L)
                                              .build();
        var command = CancelPaymentCommand.builder()
                                          .payment(payment)
                                          .build();

        sendEventExternal(brokerClient, event);
        readAndVerifyCommand(brokerClient,commandTopic, command,"payment");
    }

    @Test
    public void testMethodPaymentAccepted(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){
        UUID paymentId = UUID.randomUUID();

        var event = MethodPaymentAcceptedEvent.builder()
                                              .paymentId(paymentId)
                                              .method(PaymentMethod.Bacs)
                                              .psp("psp")
                                              .pspMetadata(Map.of("a","b"))
                                              .previousFailures(List.empty())
                                              .correlationId(UUID.randomUUID())
                                              .build();
        var command = CompletePaymentSubmissionCommand.builder()
                                                      .paymentId(paymentId)
                                                      .method(PaymentMethod.Bacs)
                                                      .psp("psp")
                                                      .pspMetadata(Map.of("a","b"))
                                                      .failureDetails(List.empty())
                                                      .build();

        sendEventExternal(brokerClient, event);
        readAndVerifyCommand(brokerClient,commandTopic, command,"paymentId","method","psp","pspMetadata","failureDetails");
    }

    @Test
    public void testMethodPaymentRejected(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){
        UUID paymentId = UUID.randomUUID();

        var event = MethodPaymentRejectedEvent.builder()
                                              .paymentId(paymentId)
                                              .method(PaymentMethod.Bacs)
                                              .details(List.empty())
                                              .correlationId(UUID.randomUUID())
                                              .build();
        var command = FailPaymentSubmissionCommand.builder()
                                                  .paymentId(paymentId)
                                                  .method(PaymentMethod.Bacs)
                                                  .failureDetails(List.empty())
                                                  .build();

        sendEventExternal(brokerClient, event);
        readAndVerifyCommand(brokerClient,commandTopic, command,"paymentId","method","failureDetails");
    }

    @Test
    public void testMethodPaymentSubmissionFailed(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){
        UUID paymentId = UUID.randomUUID();

        var event = MethodPaymentSubmissionFailedEvent.builder()
                                                      .paymentId(paymentId)
                                                      .method(PaymentMethod.Bacs)
                                                      .details(List.empty())
                                                      .correlationId(UUID.randomUUID())
                                                      .build();
        var command = ReRoutePaymentCommand.builder()
                                           .paymentId(paymentId)
                                           .build();

        sendEventExternal(brokerClient, event);
        readAndVerifyCommand(brokerClient,commandTopic, command,"paymentId");
    }

    @Test
    public void testMethodPaymentComplete(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){
        UUID paymentId = UUID.randomUUID();

        var event = MethodPaymentCompleteEvent.builder()
                                              .paymentId(paymentId)
                                              .method(PaymentMethod.Bacs)
                                              .psp("psp")
                                              .pspMetadata(Map.of("a","b"))
                                              .correlationId(UUID.randomUUID())
                                              .build();
        var command = CompletePaymentCommand.builder()
                                            .paymentId(paymentId)
                                            .method(PaymentMethod.Bacs)
                                            .psp("psp")
                                            .pspMetadata(Map.of("a","b"))
                                            .build();

        sendEventExternal(brokerClient, event);
        readAndVerifyCommand(brokerClient,commandTopic, command,"paymentId","method","psp","pspMetadata");
    }

    @Test
    public void testMethodPaymentReturned(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){
        UUID paymentId = UUID.randomUUID();

        var event = MethodPaymentReturnedEvent.builder()
                                              .paymentId(paymentId)
                                              .method(PaymentMethod.Bacs)
                                              .psp("psp")
                                              .pspMetadata(Map.of("a","b"))
                                              .message("1")
                                              .code("2")
                                              .amount(Money.of(new BigDecimal("1"),"GBP"))
                                              .correlationId(UUID.randomUUID())
                                              .build();
        var command = ReturnPaymentCommand.builder()
                                          .paymentId(paymentId)
                                          .method(PaymentMethod.Bacs)
                                          .psp("psp")
                                          .pspMetadata(Map.of("a","b"))
                                          .message("1")
                                          .code("2")
                                          .amount(Money.of(new BigDecimal("1"),"GBP"))
                                          .build();

        sendEventExternal(brokerClient, event);
        readAndVerifyCommand(brokerClient,commandTopic, command,"paymentId","method","psp","pspMetadata","message","code","amount");
    }

    @Test
    public void testMethodPaymentCancelFailed(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){
        UUID paymentId = UUID.randomUUID();

        var event = MethodPaymentCancelFailedEvent.builder()
                                                  .paymentId(paymentId)
                                                  .method(PaymentMethod.Bacs)
                                                  .psp("psp")
                                                  .pspMetadata(Map.of("a","b"))
                                                  .message("1")
                                                  .code("2")
                                                  .correlationId(UUID.randomUUID())
                                                  .build();
        var command = FailPaymentCancelCommand.builder()
                                              .paymentId(paymentId)
                                              .method(PaymentMethod.Bacs)
                                              .psp("psp")
                                              .pspMetadata(Map.of("a","b"))
                                              .message("1")
                                              .code("2")
                                              .build();

        sendEventExternal(brokerClient, event);
        readAndVerifyCommand(brokerClient,commandTopic, command,"paymentId","method","psp","pspMetadata","message","code");
    }

    @Test
    public void testMethodPaymentCancelSuccess(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic){
        UUID paymentId = UUID.randomUUID();

        var event = MethodPaymentCancelSuccessEvent.builder()
                                                   .paymentId(paymentId)
                                                   .method(PaymentMethod.Bacs)
                                                   .psp("psp")
                                                   .pspMetadata(Map.of("a","b"))
                                                   .correlationId(UUID.randomUUID())
                                                   .build();
        var command = CompletePaymentCancelCommand.builder()
                                                  .paymentId(paymentId)
                                                  .method(PaymentMethod.Bacs)
                                                  .psp("psp")
                                                  .pspMetadata(Map.of("a","b"))
                                                  .build();

        sendEventExternal(brokerClient, event);
        readAndVerifyCommand(brokerClient,commandTopic, command,"paymentId","method","psp","pspMetadata");
    }

    private Payment makePayment() {
        return Payment.builder()
                    .createdBy("user")
                    .allowedMethods(List.of(PaymentMethod.Bacs))
                    .creditor(PartyIdentification.builder().name("").build())
                      .debtor(PartyIdentification.builder().name("").build())
                      .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                      .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                    .amount(Money.parse("GBP 1.00"))
                    .latestExecutionDate(Instant.now())
                    .product(new CompositeReference("",""))
                    .build();
    }

    private void readAndVerifyCommand(EmbeddedBrokerClient brokerClient, String commandTopic, Command command,String... fields) {
        var commands = brokerClient.readAllFromTopic(commandTopic, command.getClass());
        assertEquals(1, commands.size());
        var actual = commands.head();
        assertEquals(command.getClass(), actual.getClass());
        assertThat(actual).isEqualToComparingOnlyGivenFields(command,fields);
    }

    private void sendEventExternal(EmbeddedBrokerClient brokerClient, Event event) {
        brokerClient.sendToDestination("events-internal", List.of(event));
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends CorePspPMApplication{

    }
}