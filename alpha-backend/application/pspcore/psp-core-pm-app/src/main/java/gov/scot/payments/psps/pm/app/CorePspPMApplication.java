package gov.scot.payments.psps.pm.app;

import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.application.component.processmanager.EventTransformer;
import gov.scot.payments.application.component.processmanager.ProcessManagerApplication;
import gov.scot.payments.payments.model.event.PaymentCancelApprovedEvent;
import gov.scot.payments.payments.model.event.PaymentReadyForSubmissionEvent;
import gov.scot.payments.psps.method.model.PspSubmissionFailureDetails;
import gov.scot.payments.psps.method.model.event.*;
import gov.scot.payments.payments.model.PaymentSubmissionFailureDetails;
import gov.scot.payments.psps.model.command.FailPaymentSubmissionCommand;
import gov.scot.payments.psps.model.command.*;
import io.vavr.collection.List;
import org.apache.kafka.common.header.Headers;
import org.springframework.context.annotation.Bean;

import java.util.function.Predicate;

@ApplicationComponent
public class CorePspPMApplication extends ProcessManagerApplication {

    /*
    @Bean
    public StreamJoiner<Event,Event,Event> joiner(SchemaRegistryClient confluentSchemaRegistryClient){
        HeaderPreservingCompositeNonNativeSerde<Event> eventSerde = valueSerde(Event.class);
        return StreamJoiner.<Event, Event, Event>builder()
            .repartitionTopicName()
            .joinWindow()
            .joiner()
            .rekeyFunction()
            .leftValueSerde(eventSerde)
            .rightValueSerde(eventSerde)
            .stateSerde(stateSerde(confluentSchemaRegistryClient,SerializedMessage.class))
            .build();
    }
     */

    @Bean
    public EventTransformer eventTransformFunction() {
        return EventTransformer.patternMatching(b ->
                //payments
                b.match(PaymentReadyForSubmissionEvent.class, e -> List.of(new RoutePaymentCommand(e.getCarriedState(),false)))
                 .match(PaymentCancelApprovedEvent.class, e -> List.of(new CancelPaymentCommand(e.getCarriedState(),false)))

                 //method psps
                .match(MethodPaymentAcceptedEvent.class, e -> List.of(handleCompletePaymentSubmission(e)))
                .match(MethodPaymentRejectedEvent.class, e -> List.of(handleFailPaymentSubmission(e)))
                .match(MethodPaymentSubmissionFailedEvent.class, e -> List.of(handleReRoutePayment(e)))
                .match(MethodPaymentCompleteEvent.class, e -> List.of(handleCompletePayment(e)))
                .match(MethodPaymentReturnedEvent.class, e -> List.of(handleReturnPayment(e)))
                .match(MethodPaymentCancelFailedEvent.class, e -> List.of(handleCancelFail(e)))
                .match(MethodPaymentCancelSuccessEvent.class, e -> List.of(handleCancelSuccess(e)))
                .orElse(List.empty())
        );
    }

    @Bean
    public Predicate<Headers> externalEventHeaderFilter(MessageTypeRegistry messageTypeRegistry){
        return h -> messageTypeRegistry.matchesType(h
                , PaymentReadyForSubmissionEvent.class
                ,PaymentCancelApprovedEvent.class
                ,MethodPaymentAcceptedEvent.class
                ,MethodPaymentRejectedEvent.class
                ,MethodPaymentSubmissionFailedEvent.class
                , MethodPaymentCompleteEvent.class
                , MethodPaymentReturnedEvent.class
                , MethodPaymentCancelFailedEvent.class
                , MethodPaymentCancelSuccessEvent.class);
    }

    private ReRoutePaymentCommand handleReRoutePayment(final MethodPaymentSubmissionFailedEvent e) {
        return ReRoutePaymentCommand.builder()
                                    .paymentId(e.getPaymentId())
                                    .build();
    }

    private CompletePaymentCancelCommand handleCancelSuccess(final MethodPaymentCancelSuccessEvent e) {
        return CompletePaymentCancelCommand.builder()
                                           .reply(false)
                                           .paymentId(e.getPaymentId())
                                            .method(e.getMethod())
                                           .psp(e.getPsp())
                                           .pspMetadata(e.getPspMetadata())
                                           .build();
    }

    private FailPaymentCancelCommand handleCancelFail(final MethodPaymentCancelFailedEvent e) {
        return FailPaymentCancelCommand.builder()
                                       .reply(false)
                                       .paymentId(e.getPaymentId())
                                    .method(e.getMethod())
                                       .psp(e.getPsp())
                                       .pspMetadata(e.getPspMetadata())
                                       .message(e.getMessage())
                                       .code(e.getCode())
                                       .build();
    }

    private ReturnPaymentCommand handleReturnPayment(final MethodPaymentReturnedEvent e) {
        return ReturnPaymentCommand.builder()
                                   .reply(false)
                                   .paymentId(e.getPaymentId())
                                   .method(e.getMethod())
                                   .psp(e.getPsp())
                                   .message(e.getMessage())
                                   .code(e.getCode())
                                   .pspMetadata(e.getPspMetadata())
                                   .amount(e.getAmount())
                                   .build();
    }

    private CompletePaymentCommand handleCompletePayment(final MethodPaymentCompleteEvent e) {
        return CompletePaymentCommand.builder()
                                     .paymentId(e.getPaymentId())
                                     .method(e.getMethod())
                                     .psp(e.getPsp())
                                     .pspMetadata(e.getPspMetadata())
                                     .build();
    }

    private FailPaymentSubmissionCommand handleFailPaymentSubmission(final MethodPaymentRejectedEvent e) {
        return FailPaymentSubmissionCommand.builder()
                                           .reply(false)
                                           .paymentId(e.getPaymentId())
                                           .method(e.getMethod())
                                           .failureDetails(e.getDetails().map(this::convertFailureDetails))
                                           .build();
    }

    private CompletePaymentSubmissionCommand handleCompletePaymentSubmission(final MethodPaymentAcceptedEvent e) {
        return CompletePaymentSubmissionCommand.builder()
                                               .reply(false)
                                               .paymentId(e.getPaymentId())
                                               .method(e.getMethod())
                                               .psp(e.getPsp())
                                               .pspMetadata(e.getPspMetadata())
                                               .failureDetails(e.getPreviousFailures().map(this::convertFailureDetails))
                                               .build();
    }

    private PaymentSubmissionFailureDetails convertFailureDetails(final PspSubmissionFailureDetails p) {
        return PaymentSubmissionFailureDetails.builder()
                                              .message(p.getMessage())
                                              .psp(p.getPsp())
                                              .code(p.getCode())
                                              .pspMetadata(p.getPspMetadata())
                                              .build();
    }

    public static void main(String[] args){
        BaseApplication.run(CorePspPMApplication.class,args);
    }
}