package gov.scot.payments.psps.model;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchRules;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.junit.ArchTest;
import gov.scot.payments.testing.architecture.CodingStandardsRules;
import gov.scot.payments.testing.architecture.ModelRules;
import gov.scot.payments.testing.architecture.ComponentRules;

@AnalyzeClasses(importOptions = {ImportOption.DoNotIncludeTests.class, ImportOption.DoNotIncludeArchives.class})
public class ArchitectureTest{

    @ArchTest
    public static final ArchRules generalRules = ArchRules.in(CodingStandardsRules.class);

    @ArchTest
    public static final ArchRules modelRules = ArchRules.in(ModelRules.class);

    @ArchTest
    public static final ArchRule anyAnnotatedMessagesMustHaveSameContext
            = ComponentRules.anyAnnotatedMessagesMustHaveSameContext("pspcore");


}