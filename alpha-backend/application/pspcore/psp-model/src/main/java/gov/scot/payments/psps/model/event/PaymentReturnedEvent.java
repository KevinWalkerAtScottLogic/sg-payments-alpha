package gov.scot.payments.psps.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.javamoney.moneta.Money;

import java.util.Map;
import java.time.Instant;
import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "pspcore", type = "paymentReturned")
@NoArgsConstructor
@RequiredArgsConstructor
public class PaymentReturnedEvent extends EventWithCauseImpl implements HasKey<String> {

    @NonNull private UUID paymentId;
    @NonNull private PaymentMethod method;
    @NonNull private String psp;
    @NonNull private String message;
    @NonNull private String code;
    @NonNull private Money amount;
    @NonNull private Map<String,String> pspMetadata;

    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }
}
