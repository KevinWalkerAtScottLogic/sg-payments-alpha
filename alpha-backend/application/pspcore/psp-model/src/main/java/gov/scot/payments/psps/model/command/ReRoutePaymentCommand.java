package gov.scot.payments.psps.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "pspcore", type = "reRoutePayment")
@NoArgsConstructor
public class ReRoutePaymentCommand extends CommandImpl implements HasKey<String> {

    @NonNull private UUID paymentId;

    public ReRoutePaymentCommand(UUID paymentId, boolean reply) {
        super(reply);
        this.paymentId = paymentId;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }
}