package gov.scot.payments.psps.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import io.vavr.collection.Map;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "pspcore", type = "cancelPayment")
@NoArgsConstructor
public class CancelPaymentCommand extends CommandImpl implements HasKey<String> {

    @NonNull private Payment payment;

    public CancelPaymentCommand(Payment payment, boolean reply) {
        super(reply);
        this.payment = payment;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return payment.getKey();
    }
}