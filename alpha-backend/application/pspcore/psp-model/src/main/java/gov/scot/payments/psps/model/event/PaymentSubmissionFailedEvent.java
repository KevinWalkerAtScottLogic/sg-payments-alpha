package gov.scot.payments.psps.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.payments.model.PaymentSubmissionFailureDetails;
import io.vavr.collection.List;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "pspcore", type = "paymentSubmissionFailed")
@NoArgsConstructor
@RequiredArgsConstructor
public class PaymentSubmissionFailedEvent extends EventWithCauseImpl implements HasKey<String> {

    @NonNull private UUID paymentId;
    @Nullable private PaymentMethod method;
    @NonNull private List<PaymentSubmissionFailureDetails> failureDetails;
    
    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }
}
