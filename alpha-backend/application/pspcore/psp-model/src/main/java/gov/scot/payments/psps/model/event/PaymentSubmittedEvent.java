package gov.scot.payments.psps.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.payments.model.PaymentSubmissionFailureDetails;
import io.vavr.collection.List;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Map;
import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "pspcore", type = "paymentSubmitted")
@NoArgsConstructor
@RequiredArgsConstructor
public class PaymentSubmittedEvent extends EventWithCauseImpl implements HasKey<String> {

    @NonNull private UUID paymentId;
    @NonNull private PaymentMethod method;
    @NonNull private String psp;
    @NonNull private Map<String,String> pspMetadata;
    @NonNull private List<PaymentSubmissionFailureDetails> failureDetails;

    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }
}
