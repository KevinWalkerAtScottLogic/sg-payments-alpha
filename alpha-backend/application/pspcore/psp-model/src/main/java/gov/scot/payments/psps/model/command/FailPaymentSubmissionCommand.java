package gov.scot.payments.psps.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.payments.model.PaymentSubmissionFailureDetails;
import io.vavr.collection.List;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "pspcore", type = "failPaymentSubmission")
@NoArgsConstructor
public class FailPaymentSubmissionCommand extends CommandImpl implements HasKey<String> {

    @NonNull private UUID paymentId;
    @Nullable private PaymentMethod method;
    @NonNull private List<PaymentSubmissionFailureDetails> failureDetails;

    public FailPaymentSubmissionCommand(UUID paymentId, boolean reply) {
        super(reply);
        this.paymentId = paymentId;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }
}