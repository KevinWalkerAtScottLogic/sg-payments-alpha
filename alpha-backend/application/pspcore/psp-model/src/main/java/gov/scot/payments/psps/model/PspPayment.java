package gov.scot.payments.psps.model;

import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import io.vavr.collection.List;
import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Builder(toBuilder = true)
@MessageType(context = "pspcore", type = "payment")
public class PspPayment implements HasKey<String> {

    public enum Status {Routed, Submitted, Failed, Complete, Returned};
    public enum CancelStatus {Live, Requested, Complete, Failed};

    @NonNull @EqualsAndHashCode.Include private Payment payment;
    private PaymentMethod routedTo;
    @NonNull @Builder.Default private List<PaymentMethod> remainingMethods = List.empty();
    @NonNull @Builder.Default private Status status = Status.Routed;
    @NonNull @Builder.Default private CancelStatus cancelStatus = CancelStatus.Live;

    @Override
    public String getKey() {
        return payment.getKey();
    }

}
