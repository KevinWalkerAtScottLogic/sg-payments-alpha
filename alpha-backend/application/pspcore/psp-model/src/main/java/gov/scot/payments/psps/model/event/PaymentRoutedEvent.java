package gov.scot.payments.psps.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasAdditionalHeaders;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "pspcore", type = "paymentRouted")
@NoArgsConstructor
@RequiredArgsConstructor
public class PaymentRoutedEvent extends EventWithCauseImpl implements HasKey<String>, HasAdditionalHeaders {

    @NonNull private Payment payment;
    @NonNull private PaymentMethod method;

    @Override
    @JsonIgnore
    public String getKey() {
        return payment.getKey();
    }

    @Override
    @JsonIgnore
    public Map<String, String> additionalHeaders() {
        return Map.of(Payment.METHOD_HEADER,method.name());
    }
}
