package gov.scot.payments.psps.ch.app;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.psps.model.command.*;
import gov.scot.payments.psps.model.event.PaymentCompleteEvent;
import gov.scot.payments.psps.model.event.PaymentReturnedEvent;
import gov.scot.payments.psps.model.event.PaymentRoutedEvent;
import gov.scot.payments.psps.model.event.PaymentSubmittedEvent;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import gov.scot.payments.application.ApplicationComponent;
import org.springframework.context.annotation.Import;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.apache.kafka.streams.KafkaStreams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.reactive.server.WebTestClient;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.confluent.kafka.schemaregistry.client.MockSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;

import static gov.scot.payments.testing.security.MockSubjectAuthentication.user;
import static org.junit.Assert.assertEquals;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockAuthentication;

@ApplicationIntegrationTest(classes = {CorePspCHApplicationIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.COMMAND_HANDLER)
public class CorePspCHApplicationIntegrationTest {

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, EmbeddedBrokerClient brokerClient, @Value("${namespace}") String namespace){
        brokerClient.getBroker().addTopicsIfNotExists(namespace+"-psps-command-errors");
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
    }

    @Test
    public void testHappyPath(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        var payment = makePayment();
        var route = RoutePaymentCommand.builder()
                .payment(payment)
                .build();
        var reRoute = ReRoutePaymentCommand.builder()
                .paymentId(payment.getId())
                .build();
        var completeSubmission = CompletePaymentSubmissionCommand.builder()
                .method(PaymentMethod.Bacs)
                .paymentId(payment.getId())
                .psp("psp")
                .pspMetadata(Map.of("a","b"))
                .failureDetails(List.empty())
                .build();
        var complete = CompletePaymentCommand.builder()
                .paymentId(payment.getId())
                .method(PaymentMethod.Bacs)
                .pspMetadata(Map.of("",""))
                .psp("psp")
                .build();
        var ret = ReturnPaymentCommand.builder()
                .paymentId(payment.getId())
                .method(PaymentMethod.Bacs)
                .psp("psp")
                .pspMetadata(Map.of())
                .message("")
                .code("")
                .amount(Money.parse("GBP 1.00"))
                .build();

        brokerClient.sendToDestination("commands",List.of(route,reRoute,completeSubmission,complete,ret));
        var events = brokerClient.readAllFromDestination("events", Event.class);
        assertEquals(5,events.size());
        assertEquals(PaymentRoutedEvent.class,events.get(0).getClass());
        assertEquals(PaymentRoutedEvent.class,events.get(1).getClass());
        assertEquals(PaymentSubmittedEvent.class,events.get(2).getClass());
        assertEquals(PaymentCompleteEvent.class,events.get(3).getClass());
        assertEquals(PaymentReturnedEvent.class,events.get(4).getClass());
        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("customer.product/payments:Read"))))
                .get()
                .uri("/state/"+payment.getId().toString())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.status").isEqualTo("Returned");

    }


    //route
    //fail sub

    //route
    //cancel
    //cancel success

    //route
    //cancel
    //cancel fail

    private Payment makePayment() {
        return Payment.builder()
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.Bacs,PaymentMethod.FasterPayments))
                .creditor(PartyIdentification.builder().name("").build())
                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                      .debtor(PartyIdentification.builder().name("").build())
                      .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                      .amount(Money.parse("GBP 1.00"))
                .latestExecutionDate(Instant.now())
                .product(new CompositeReference("customer","product"))
                .build();
    }
    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends CorePspCHApplication{

    }
}