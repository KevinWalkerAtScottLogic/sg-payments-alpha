package gov.scot.payments.psps.ch.app;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.psps.model.PspPayment;
import gov.scot.payments.psps.model.command.*;
import gov.scot.payments.psps.model.event.*;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class PspEventGeneratorTest {

    private PspEventGenerator generator;
    private Payment payment;
    private PspPaymentState state;

    @BeforeEach
    public void setUp(){
        generator = new PspEventGenerator();
        payment = makePayment();
        state = PspPaymentState.builder()
                               .current(PspPayment.builder()
                                                  .payment(payment)
                                                  .routedTo(PaymentMethod.Bacs)
                                                  .build())
                               .build();
    }

    @Test
    public void testRoute(){
        var command = RoutePaymentCommand.builder()
                                         .payment(payment)
                                         .build();
        var events = generator.apply(command,state);
        assertEquals(1,events.size());
        var event = (PaymentRoutedEvent)events.head();
        assertEquals(payment,event.getPayment());
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals(PaymentMethod.Bacs.name(),event.additionalHeaders().get(Payment.METHOD_HEADER));
    }

    @Test
    public void testReRouteSuccess(){
        var command = ReRoutePaymentCommand.builder()
                                           .paymentId(payment.getId())
                                           .build();
        var events = generator.apply(command,state);
        assertEquals(1,events.size());
        var event = (PaymentRoutedEvent)events.head();
        assertEquals(payment,event.getPayment());
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals(PaymentMethod.Bacs.name(),event.additionalHeaders().get(Payment.METHOD_HEADER));
    }

    @Test
    public void testReRouteFail(){
        var state = PspPaymentState.builder()
                                           .current(PspPayment.builder()
                                                              .payment(payment)
                                                              .build())
                                           .build();
        var command = ReRoutePaymentCommand.builder()
                                           .paymentId(payment.getId())
                                           .build();
        var events = generator.apply(command,state);
        assertEquals(1,events.size());
        var event = (PaymentSubmissionFailedEvent)events.head();
        assertEquals(payment.getId(),event.getPaymentId());
        assertNotNull(event.getFailureDetails().head());
    }

    @Test
    public void testCompleteSubmission(){
        var command = CompletePaymentSubmissionCommand.builder()
                .paymentId(payment.getId())
                .method(PaymentMethod.Bacs)
                .psp("psp")
                .pspMetadata(Map.of("a","b"))
                .failureDetails(List.empty())
                .build();
        var events = generator.apply(command,state);
        assertEquals(1,events.size());
        var event = (PaymentSubmittedEvent)events.head();
        assertEquals(payment.getId(),event.getPaymentId());
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals("psp",event.getPsp());
        assertEquals(Map.of("a","b"),event.getPspMetadata());
        assertEquals(List.of(),event.getFailureDetails());
    }

    @Test
    public void testFailSubmission(){
        var command = FailPaymentSubmissionCommand.builder()
                .paymentId(payment.getId())
                .method(PaymentMethod.Bacs)
                .failureDetails(List.empty())
                .build();
        var events = generator.apply(command,state);
        assertEquals(1,events.size());
        var event = (PaymentSubmissionFailedEvent)events.head();
        assertEquals(payment.getId(),event.getPaymentId());
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals(List.of(),event.getFailureDetails());
    }

    @Test
    public void testComplete(){
        var command = CompletePaymentCommand.builder()
                .paymentId(payment.getId())
                .method(PaymentMethod.Bacs)
                .psp("psp")
                .pspMetadata(Map.of("a","b"))
                .build();
        var events = generator.apply(command,state);
        assertEquals(1,events.size());
        var event = (PaymentCompleteEvent)events.head();
        assertEquals(payment.getId(),event.getPaymentId());
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals("psp",event.getPsp());
        assertEquals(Map.of("a","b"),event.getPspMetadata());
    }

    @Test
    public void testReturn(){
        var command = ReturnPaymentCommand.builder()
                .paymentId(payment.getId())
                .method(PaymentMethod.Bacs)
                .psp("psp")
                .message("1")
                .code("2")
                .amount(Money.parse("GBP 1.00"))
                .pspMetadata(Map.of("a","b"))
                .build();
        var events = generator.apply(command,state);
        assertEquals(1,events.size());
        var event = (PaymentReturnedEvent)events.head();
        assertEquals(payment.getId(),event.getPaymentId());
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals("psp",event.getPsp());
        assertEquals(Map.of("a","b"),event.getPspMetadata());
        assertEquals("1",event.getMessage());
        assertEquals("2",event.getCode());
        assertEquals(Money.parse("GBP 1.00"),event.getAmount());
    }

    @Test
    public void testCancel(){
        var command = CancelPaymentCommand.builder()
                .payment(payment)
                .build();
        var events = generator.apply(command,state);
        assertEquals(1,events.size());
        var event = (PaymentCancelledEvent)events.head();
        assertEquals(payment.getId(),event.getPaymentId());
        assertEquals(PaymentMethod.Bacs.name(),event.additionalHeaders().get(Payment.METHOD_HEADER));
    }

    @Test
    public void testCancelFail(){
        var command = FailPaymentCancelCommand.builder()
                .paymentId(payment.getId())
                .method(PaymentMethod.Bacs)
                .psp("psp")
                .message("1")
                .code("2")
                .pspMetadata(Map.of("a","b"))
                .build();
        var events = generator.apply(command,state);
        assertEquals(1,events.size());
        var event = (PaymentCancelFailedEvent)events.head();
        assertEquals(payment.getId(),event.getPaymentId());
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals("psp",event.getPsp());
        assertEquals(Map.of("a","b"),event.getPspMetadata());
        assertEquals("1",event.getMessage());
        assertEquals("2",event.getCode());
    }

    @Test
    public void testCancelSuccess(){
        var command = CompletePaymentCancelCommand.builder()
                .paymentId(payment.getId())
                .method(PaymentMethod.Bacs)
                .psp("psp")
                .pspMetadata(Map.of("a","b"))
                .build();
        var events = generator.apply(command,state);
        assertEquals(1,events.size());
        var event = (PaymentCancelCompleteEvent)events.head();
        assertEquals(payment.getId(),event.getPaymentId());
        assertEquals(PaymentMethod.Bacs,event.getMethod());
        assertEquals("psp",event.getPsp());
        assertEquals(Map.of("a","b"),event.getPspMetadata());
    }

    private Payment makePayment() {
        return Payment.builder()
                      .createdBy("user")
                      .allowedMethods(List.of(PaymentMethod.Bacs))
                      .creditor(PartyIdentification.builder().name("").build())
                      .debtor(PartyIdentification.builder().name("").build())
                      .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                      .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                      .amount(Money.parse("GBP 1.00"))
                      .latestExecutionDate(Instant.now())
                      .product(new CompositeReference("",""))
                      .build();
    }
}
