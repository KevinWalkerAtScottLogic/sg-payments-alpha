package gov.scot.payments.psps.ch.app;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.psps.model.PspPayment;
import gov.scot.payments.psps.model.command.CancelPaymentCommand;
import gov.scot.payments.psps.model.command.CompletePaymentCancelCommand;
import gov.scot.payments.psps.model.command.CompletePaymentCommand;
import gov.scot.payments.psps.model.command.CompletePaymentSubmissionCommand;
import gov.scot.payments.psps.model.command.FailPaymentCancelCommand;
import gov.scot.payments.psps.model.command.FailPaymentSubmissionCommand;
import gov.scot.payments.psps.model.command.ReRoutePaymentCommand;
import gov.scot.payments.psps.model.command.ReturnPaymentCommand;
import gov.scot.payments.psps.model.command.RoutePaymentCommand;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PspStateUpdaterTest {

    private PspStateUpdater updater;

    @BeforeEach
    public void setUp(){
        updater = new PspStateUpdater();
    }

    @Test
    public void testRoute(){
        var payment = makePayment();

        var command = RoutePaymentCommand.builder()
                                         .payment(payment)
                                         .build();

        var currentState = PspPayment.builder()
                                     .payment(payment)
                                     .build();
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspPaymentState.builder().current(currentState).build()));

        var updatedState = updater.apply(command,PspPaymentState.builder().build());
        assertEquals(payment.getId(),updatedState.getPayment().getId());
        assertEquals(PaymentMethod.Bacs,updatedState.getRoutedTo());
        assertEquals(PspPayment.CancelStatus.Live,updatedState.getCancelStatus());
        assertEquals(PspPayment.Status.Routed,updatedState.getStatus());
    }

    @Test
    public void testReRoute(){
        var payment = makePayment();

        var command = ReRoutePaymentCommand.builder()
                                           .paymentId(payment.getId())
                                           .build();

        var currentState = PspPayment.builder()
                                     .payment(payment)
                                     .status(PspPayment.Status.Complete)
                                     .build();
        //not yet routed
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspPaymentState.builder().build()));

        //incorrect state
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspPaymentState.builder().current(currentState).build()));

        //no methods remaining
        var updatedState = updater.apply(command,PspPaymentState.builder().current(currentState.toBuilder().status(PspPayment.Status.Routed).build()).build());
        assertNull(updatedState.getRoutedTo());
        assertEquals(PspPayment.CancelStatus.Live,updatedState.getCancelStatus());
        assertEquals(PspPayment.Status.Failed,updatedState.getStatus());

        //methods remaining
        updatedState = updater.apply(command,PspPaymentState.builder().current(currentState.toBuilder()
                                                                                           .status(PspPayment.Status.Routed)
                                                                                           .remainingMethods(List.of(PaymentMethod.FasterPayments,PaymentMethod.Chaps))
                                                                                           .build())
                                                            .build());
        assertEquals(PaymentMethod.FasterPayments,updatedState.getRoutedTo());
        assertEquals(PspPayment.CancelStatus.Live,updatedState.getCancelStatus());
        assertEquals(PspPayment.Status.Routed,updatedState.getStatus());

    }

    @Test
    public void testCompleteSubmission(){
        var payment = makePayment();

        var command = CompletePaymentSubmissionCommand.builder()
                                                      .method(PaymentMethod.Bacs)
                                                      .paymentId(payment.getId())
                                                      .psp("psp")
                                                      .pspMetadata(Map.of("a","b"))
                                                      .failureDetails(List.empty())
                                                      .build();

        var currentState = PspPayment.builder()
                                     .payment(payment)
                                     .status(PspPayment.Status.Complete)
                                     .build();
        //not yet routed
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspPaymentState.builder().build()));

        //incorrect state
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspPaymentState.builder().current(currentState).build()));

        //correct state
        var updatedState = updater.apply(command,PspPaymentState.builder().current(currentState.toBuilder().status(PspPayment.Status.Routed).build()).build());
        assertEquals(PspPayment.CancelStatus.Live,updatedState.getCancelStatus());
        assertEquals(PspPayment.Status.Submitted,updatedState.getStatus());
    }

    @Test
    public void testFailSubmission(){
        var payment = makePayment();

        var command = FailPaymentSubmissionCommand.builder()
                                                  .paymentId(payment.getId())
                                                  .failureDetails(List.empty())
                                                  .build();

        var currentState = PspPayment.builder()
                                     .payment(payment)
                                     .status(PspPayment.Status.Complete)
                                     .build();
        //not yet routed
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspPaymentState.builder().build()));

        //incorrect state
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspPaymentState.builder().current(currentState).build()));

        //correct state
        var updatedState = updater.apply(command,PspPaymentState.builder().current(currentState.toBuilder().status(PspPayment.Status.Routed).build()).build());
        assertEquals(PspPayment.CancelStatus.Live,updatedState.getCancelStatus());
        assertEquals(PspPayment.Status.Failed,updatedState.getStatus());
    }

    @Test
    public void testComplete(){
        var payment = makePayment();

        var command = CompletePaymentCommand.builder()
                                            .paymentId(payment.getId())
                                            .method(PaymentMethod.Bacs)
                                            .pspMetadata(Map.of("",""))
                                            .psp("psp")
                                            .build();

        var currentState = PspPayment.builder()
                                     .payment(payment)
                                     .status(PspPayment.Status.Routed)
                                     .build();
        //not yet routed
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspPaymentState.builder().build()));

        //incorrect state
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspPaymentState.builder().current(currentState).build()));

        //correct state
        var updatedState = updater.apply(command,PspPaymentState.builder().current(currentState.toBuilder().status(PspPayment.Status.Submitted).build()).build());
        assertEquals(PspPayment.CancelStatus.Live,updatedState.getCancelStatus());
        assertEquals(PspPayment.Status.Complete,updatedState.getStatus());
    }

    @Test
    public void testReturn(){
        var payment = makePayment();

        var command = ReturnPaymentCommand.builder()
                                          .paymentId(payment.getId())
                                          .method(PaymentMethod.Bacs)
                                          .psp("psp")
                                          .pspMetadata(Map.of())
                                          .message("")
                                          .code("")
                                          .amount(Money.parse("GBP 1.00"))
                                          .build();

        var currentState = PspPayment.builder()
                                     .payment(payment)
                                     .status(PspPayment.Status.Routed)
                                     .build();
        //not yet routed
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspPaymentState.builder().build()));

        //incorrect state
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspPaymentState.builder().current(currentState).build()));

        //correct state
        var updatedState = updater.apply(command,PspPaymentState.builder().current(currentState.toBuilder().status(PspPayment.Status.Complete).build()).build());
        assertEquals(PspPayment.CancelStatus.Live,updatedState.getCancelStatus());
        assertEquals(PspPayment.Status.Returned,updatedState.getStatus());
    }

    @Test
    public void testCancel(){
        var payment = makePayment();

        var command = CancelPaymentCommand.builder()
                                          .payment(payment)
                                          .build();

        var currentState = PspPayment.builder()
                                     .payment(payment)
                                     .cancelStatus(PspPayment.CancelStatus.Complete)
                                     .build();
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspPaymentState.builder().build()));
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspPaymentState.builder().current(currentState).build()));

        var updatedState = updater.apply(command,PspPaymentState.builder().current(PspPayment.builder()
                                                                                             .payment(payment)
                                                                                             .cancelStatus(PspPayment.CancelStatus.Live)
                                                                                             .build()).build());
        assertEquals(PspPayment.CancelStatus.Requested,updatedState.getCancelStatus());
    }

    @Test
    public void testCancelFail(){
        var payment = makePayment();

        var command = FailPaymentCancelCommand.builder()
                                              .paymentId(payment.getId())
                                              .method(PaymentMethod.Bacs)
                                              .pspMetadata(Map.of("",""))
                                              .psp("psp")
                                              .message("")
                                              .code("")
                                              .build();

        var currentState = PspPayment.builder()
                                     .payment(payment)
                                     .cancelStatus(PspPayment.CancelStatus.Complete)
                                     .build();
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspPaymentState.builder().build()));
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspPaymentState.builder().current(currentState).build()));

        var updatedState = updater.apply(command,PspPaymentState.builder().current(PspPayment.builder()
                                                                                             .payment(payment)
                                                                                             .cancelStatus(PspPayment.CancelStatus.Requested)
                                                                                             .build()).build());
        assertEquals(PspPayment.CancelStatus.Failed,updatedState.getCancelStatus());
    }

    @Test
    public void testCancelComplete(){
        var payment = makePayment();

        var command = CompletePaymentCancelCommand.builder()
                                                  .method(PaymentMethod.Bacs)
                                                  .pspMetadata(Map.of("",""))
                                                  .psp("psp")
                                                  .paymentId(payment.getId())
                                                  .build();

        var currentState = PspPayment.builder()
                                     .payment(payment)
                                     .cancelStatus(PspPayment.CancelStatus.Complete)
                                     .build();
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspPaymentState.builder().build()));
        assertThrows(IllegalStateException.class,() -> updater.apply(command,PspPaymentState.builder().current(currentState).build()));

        var updatedState = updater.apply(command,PspPaymentState.builder().current(PspPayment.builder()
                                                                                             .cancelStatus(PspPayment.CancelStatus.Requested)
                                                                                             .payment(payment)
                                                                                             .build()).build());
        assertEquals(PspPayment.CancelStatus.Complete,updatedState.getCancelStatus());
    }

    private Payment makePayment() {
        return Payment.builder()
                      .createdBy("user")
                      .allowedMethods(List.of(PaymentMethod.Bacs))
                      .debtor(PartyIdentification.builder().name("").build())
                      .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())

                      .creditor(PartyIdentification.builder().name("name").build())
                        .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                      .amount(Money.parse("GBP 1.00"))
                      .latestExecutionDate(Instant.now())
                      .product(new CompositeReference("",""))
                      .build();
    }
}
