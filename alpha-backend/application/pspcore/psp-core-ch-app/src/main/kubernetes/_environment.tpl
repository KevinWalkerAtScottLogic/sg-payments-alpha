{{- define "pspcore-psp-core-ch-app.env" -}}
- name: CLIENT_SECRET
  valueFrom:
    configMapKeyRef:
      name: cognito-info
      key: client_secret
- name: CLIENT_ID
  valueFrom:
    configMapKeyRef:
      name: cognito-info
      key: client_id
{{- end -}}