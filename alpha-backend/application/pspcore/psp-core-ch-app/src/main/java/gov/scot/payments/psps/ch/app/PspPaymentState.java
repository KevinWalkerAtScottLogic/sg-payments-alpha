package gov.scot.payments.psps.ch.app;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.application.component.commandhandler.stateful.AggregateState;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.model.CommandFailureInfo;
import gov.scot.payments.psps.model.PspPayment;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

@SuperBuilder
@NoArgsConstructor
@ToString(callSuper = true)
public class PspPaymentState extends AggregateState<PspPayment> {

    @Getter @Nullable private PspPayment previous;
    @Getter @Nullable private PspPayment current;

    @JsonCreator
    public PspPaymentState(@JsonProperty("message") SerializedMessage message
            , @JsonProperty("error") CommandFailureInfo error
            , @JsonProperty("previous") PspPayment previous
            , @JsonProperty("current") PspPayment current
            , @JsonProperty("currentVersion") Long currentVersion) {
        super(message, error, currentVersion);
        this.previous = previous;
        this.current = current;
    }

}
