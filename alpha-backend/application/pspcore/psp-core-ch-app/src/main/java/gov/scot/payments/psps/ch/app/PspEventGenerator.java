package gov.scot.payments.psps.ch.app;

import gov.scot.payments.application.component.commandhandler.stateful.EventGeneratorFunction;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.GenericErrorEvent;
import gov.scot.payments.payments.model.PaymentSubmissionFailureDetails;
import gov.scot.payments.psps.model.PspPayment;
import gov.scot.payments.psps.model.command.FailPaymentSubmissionCommand;
import gov.scot.payments.psps.model.command.*;
import gov.scot.payments.psps.model.event.*;
import io.vavr.collection.List;
import lombok.extern.slf4j.Slf4j;

import static gov.scot.payments.application.component.commandhandler.stateful.AggregateState.failure;

@Slf4j
public class PspEventGenerator extends EventGeneratorFunction.PatternMatching<PspPaymentState, EventWithCauseImpl> {

    @Override
    protected void handlers(PatternMatcher.Builder2<Command, PspPaymentState, List<EventWithCauseImpl>> builder) {
        builder.match2(c -> true, failure(), (c,s) -> List.of(GenericErrorEvent.from(c,s.getError())))
                .match2(RoutePaymentCommand.class, (c, s) -> List.of(handleRoutePayment(c,s.getCurrent())))
                .match2(ReRoutePaymentCommand.class, (c, s) -> List.of(handleReRoutePayment(c,s.getCurrent())))
                .match2(CompletePaymentSubmissionCommand.class, (c, s) -> List.of(handleSubmittedPayment(c,s.getCurrent())))
                .match2(FailPaymentSubmissionCommand.class, (c, s) -> List.of(handleFailedPaymentSubmission(c,s.getCurrent())))
                .match2(CompletePaymentCommand.class, (c, s) -> List.of(handleCompletePayment(c,s.getCurrent())))
                .match2(ReturnPaymentCommand.class, (c,s) -> List.of(handleReturnPayment(c,s.getCurrent())))
                .match2(CancelPaymentCommand.class, (c,s) -> List.of(handleCancelPayment(c,s.getCurrent())))
                .match2(FailPaymentCancelCommand.class, (c,s) -> List.of(handleFailCancelPayment(c,s.getCurrent())))
                .match2(CompletePaymentCancelCommand.class, (c,s) -> List.of(handleCompleteCancelPayment(c,s.getCurrent())));
    }

    private PaymentRoutedEvent handleRoutePayment(RoutePaymentCommand c, PspPayment current) {
        log.info("Handling route payment");
        return new PaymentRoutedEvent(current.getPayment(),current.getRoutedTo());
    }

    private PaymentCancelCompleteEvent handleCompleteCancelPayment(CompletePaymentCancelCommand c, PspPayment current) {
        log.info("Handling complete payment cancel");
        return PaymentCancelCompleteEvent.builder()
                .paymentId(current.getPayment().getId())
                .method(current.getRoutedTo())
                .psp(c.getPsp())
                .pspMetadata(c.getPspMetadata())
                .build();
    }

    private PaymentCancelFailedEvent handleFailCancelPayment(FailPaymentCancelCommand c, PspPayment current) {
        log.info("Handling fail payment cancel");
        return PaymentCancelFailedEvent.builder()
                .paymentId(current.getPayment().getId())
                .method(current.getRoutedTo())
                .psp(c.getPsp())
                .message(c.getMessage())
                .code(c.getCode())
                .pspMetadata(c.getPspMetadata())
                .build();
    }

    private PaymentCancelledEvent handleCancelPayment(CancelPaymentCommand command, PspPayment current) {
        log.info("Handling cancel payment");
        return PaymentCancelledEvent.builder()
                .paymentId(current.getPayment().getId())
                .method(current.getRoutedTo())
                .build();
    }

    private PaymentReturnedEvent handleReturnPayment(ReturnPaymentCommand command, PspPayment current) {
        log.info("Handling return payment");
        return PaymentReturnedEvent.builder()
                                   .paymentId(current.getPayment().getId())
                                   .method(command.getMethod())
                                   .psp(command.getPsp())
                                   .message(command.getMessage())
                                   .code(command.getCode())
                                   .pspMetadata(command.getPspMetadata())
                                   .amount(command.getAmount())
                .build();
    }

    private PaymentCompleteEvent handleCompletePayment(CompletePaymentCommand command, PspPayment current) {
        log.info("Handling complete payment");
        return PaymentCompleteEvent.builder()
                                   .paymentId(current.getPayment().getId())
                                   .method(command.getMethod())
                                   .psp(command.getPsp())
                                   .pspMetadata(command.getPspMetadata())
                .build();
    }

    private EventWithCauseImpl handleReRoutePayment(ReRoutePaymentCommand command, PspPayment current) {
        log.info("Handling re-route payment");
        if(current.getRoutedTo() != null){
            return new PaymentRoutedEvent(current.getPayment(),current.getRoutedTo());
        } else{
            return PaymentSubmissionFailedEvent.builder()
                    .paymentId(current.getPayment().getId())
                    .failureDetails(List.of(PaymentSubmissionFailureDetails.builder()
                                                                           .message("Payment methods exhausted")
                                                                           .build()))
                    .build();
        }
    }

    private PaymentSubmittedEvent handleSubmittedPayment(CompletePaymentSubmissionCommand command, PspPayment current) {
        log.info("Handling submitted payment");
        return PaymentSubmittedEvent.builder()
                .paymentId(current.getPayment().getId())
                                    .method(command.getMethod())
                                    .psp(command.getPsp())
                                    .pspMetadata(command.getPspMetadata())
                                    .failureDetails(command.getFailureDetails())
                .build();
    }

    private PaymentSubmissionFailedEvent handleFailedPaymentSubmission(FailPaymentSubmissionCommand command, PspPayment current) {
        log.info("Handling payment submission failed");
        return PaymentSubmissionFailedEvent.builder()
                                           .paymentId(current.getPayment().getId())
                                           .method(command.getMethod())
                                           .failureDetails(command.getFailureDetails())
                .build();
    }

}
