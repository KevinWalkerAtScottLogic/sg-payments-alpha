package gov.scot.payments.psps.ch.app;

import gov.scot.payments.application.component.commandhandler.stateful.StateUpdateFunction;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.model.Command;
import gov.scot.payments.psps.model.PspPayment;
import gov.scot.payments.psps.model.command.FailPaymentSubmissionCommand;
import gov.scot.payments.psps.model.command.*;
import io.vavr.collection.List;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PspStateUpdater extends StateUpdateFunction.PatternMatching<PspPaymentState, PspPayment> {

    @Override
    protected void handlers(PatternMatcher.Builder2<Command, PspPaymentState, PspPayment> builder) {
        builder.match2(RoutePaymentCommand.class, (c,s) -> handleRoutePayment(c,s.getCurrent()))
                .match2(ReRoutePaymentCommand.class, (c,s) -> handleReRoutePayment(c,s.getCurrent()))
                .match2(CompletePaymentSubmissionCommand.class, (c,s) -> handleSubmittedPayment(c,s.getCurrent()))
                .match2(FailPaymentSubmissionCommand.class, (c, s) -> handleFailedPaymentSubmission(c,s.getCurrent()))
                .match2(CompletePaymentCommand.class, (c,s) -> handleCompletePayment(c,s.getCurrent()))
                .match2(ReturnPaymentCommand.class, (c,s) -> handleReturnPayment(c,s.getCurrent()))
                .match2(CancelPaymentCommand.class, (c,s) -> handleCancelPayment(c,s.getCurrent()))
                .match2(FailPaymentCancelCommand.class, (c,s) -> handleFailCancelPayment(c,s.getCurrent()))
                .match2(CompletePaymentCancelCommand.class, (c,s) -> handleCompleteCancelPayment(c,s.getCurrent()));
    }

    private PspPayment handleRoutePayment(RoutePaymentCommand command, PspPayment current) {
        log.info("Handling route payment");
        if(current != null){
            throw new IllegalStateException(String.format("Payment: %s has already been routed",current.getKey()));
        }
        return PspPayment.builder()
                .payment(command.getPayment())
                .routedTo(command.getPayment().getAllowedMethods().get(0))
                .remainingMethods(command.getPayment().getAllowedMethods().subSequence(1))
                .build();
    }

    private PspPayment handleReRoutePayment(ReRoutePaymentCommand command, PspPayment current) {
        log.info("Handling re-route payment");
        if(current == null){
            throw new IllegalStateException(String.format("Payment: %s has not yet been routed",command.getPaymentId()));
        }
        if(current.getStatus() != PspPayment.Status.Routed){
            throw new IllegalStateException(String.format("Payment: %s is not in routed state: %s",command.getPaymentId(),current.getStatus()));
        }
        return current.getRemainingMethods().pop2Option()
                .map(t -> current.toBuilder()
                        .routedTo(t._1)
                        .remainingMethods(t._2)
                        .build())
                .getOrElse(() -> current.toBuilder()
                        .routedTo(null)
                        .remainingMethods(List.empty())
                        .status(PspPayment.Status.Failed)
                        .build());
    }

    private PspPayment handleSubmittedPayment(CompletePaymentSubmissionCommand command, PspPayment current) {
        log.info("Handling payment submitted");
        if(current == null){
            throw new IllegalStateException(String.format("Payment: %s has not yet been routed",command.getPaymentId()));
        }
        if(current.getStatus() != PspPayment.Status.Routed){
            throw new IllegalStateException(String.format("Payment: %s is not in routed state: %s",command.getPaymentId(),current.getStatus()));
        }
        return current.toBuilder()
                .status(PspPayment.Status.Submitted)
                .build();
    }

    private PspPayment handleFailedPaymentSubmission(FailPaymentSubmissionCommand command, PspPayment current) {
        log.info("Handling payment failed");
        if(current == null){
            throw new IllegalStateException(String.format("Payment: %s has not yet been routed",command.getPaymentId()));
        }
        if(current.getStatus() != PspPayment.Status.Routed){
            throw new IllegalStateException(String.format("Payment: %s is not in routed state: %s",command.getPaymentId(),current.getStatus()));
        }
        return current.toBuilder()
                .status(PspPayment.Status.Failed)
                .build();
    }

    private PspPayment handleCompletePayment(CompletePaymentCommand command, PspPayment current) {
        log.info("Handling complete payment");
        if(current == null){
            throw new IllegalStateException(String.format("Payment: %s has not yet been routed",command.getPaymentId()));
        }
        if(current.getStatus() != PspPayment.Status.Submitted){
            throw new IllegalStateException(String.format("Payment: %s is not in submitted state: %s",command.getPaymentId(),current.getStatus()));
        }
        return current.toBuilder()
                .status(PspPayment.Status.Complete)
                .build();
    }

    private PspPayment handleReturnPayment(ReturnPaymentCommand command, PspPayment current) {
        log.info("Handling return payment");
        if(current == null){
            throw new IllegalStateException(String.format("Payment: %s has not yet been routed",command.getPaymentId()));
        }
        if(current.getStatus() != PspPayment.Status.Submitted && current.getStatus() != PspPayment.Status.Complete){
            throw new IllegalStateException(String.format("Payment: %s is not in submitted state: %s",command.getPaymentId(),current.getStatus()));
        }
        return current.toBuilder()
                .status(PspPayment.Status.Returned)
                .build();
    }

    private PspPayment handleCancelPayment(CancelPaymentCommand command, PspPayment current) {
        log.info("Handling cancel payment");
        if(current == null){
            throw new IllegalStateException(String.format("Payment: %s has not yet been routed",command.getPayment().getId()));
        }
        if(current.getCancelStatus() == PspPayment.CancelStatus.Complete){
            throw new IllegalStateException(String.format("Payment: %s is not in cancel requested state: %s",command.getPayment().getId(),current.getCancelStatus()));
        }
        return current.toBuilder()
                .cancelStatus(PspPayment.CancelStatus.Requested)
                .build();
    }

    private PspPayment handleCompleteCancelPayment(CompletePaymentCancelCommand command, PspPayment current) {
        log.info("Handling complete payment cancel");
        if(current == null){
            throw new IllegalStateException(String.format("Payment: %s has not yet been routed",command.getPaymentId()));
        }
        if(current.getCancelStatus() != PspPayment.CancelStatus.Requested){
            throw new IllegalStateException(String.format("Payment: %s is not in cancel requested state: %s",command.getPaymentId(),current.getCancelStatus()));
        }
        return current.toBuilder()
                .cancelStatus(PspPayment.CancelStatus.Complete)
                .build();
    }

    private PspPayment handleFailCancelPayment(FailPaymentCancelCommand command, PspPayment current) {
        log.info("Handling failed payment cancel");
        if(current == null){
            throw new IllegalStateException(String.format("Payment: %s has not yet been routed",command.getPaymentId()));
        }
        if(current.getCancelStatus() != PspPayment.CancelStatus.Requested){
            throw new IllegalStateException(String.format("Payment: %s is not in cancel requested state: %s",command.getPaymentId(),current.getCancelStatus()));
        }
        return current.toBuilder()
                .cancelStatus(PspPayment.CancelStatus.Failed)
                .build();
    }

}
