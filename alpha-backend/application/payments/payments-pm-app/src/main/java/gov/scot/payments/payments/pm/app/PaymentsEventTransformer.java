package gov.scot.payments.payments.pm.app;

import gov.scot.payments.application.component.processmanager.EventTransformer;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.customers.model.event.CustomerPaymentApprovalRequiredEvent;
import gov.scot.payments.customers.model.event.CustomerPaymentRejectedEvent;
import gov.scot.payments.customers.model.event.CustomerPaymentVerifiedEvent;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchClosedEvent;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchUpdatedEvent;
import gov.scot.payments.payments.model.command.*;
import gov.scot.payments.payments.model.command.CompleteCancelPaymentCommand;
import gov.scot.payments.payments.model.command.FailCancelPaymentCommand;
import gov.scot.payments.psps.model.event.PaymentCancelCompleteEvent;
import gov.scot.payments.psps.model.event.PaymentCancelFailedEvent;
import gov.scot.payments.psps.model.event.PaymentCompleteEvent;
import gov.scot.payments.psps.model.event.PaymentReturnedEvent;
import gov.scot.payments.psps.model.event.PaymentSubmissionFailedEvent;
import gov.scot.payments.psps.model.event.PaymentSubmittedEvent;
import io.vavr.collection.List;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PaymentsEventTransformer extends EventTransformer.PatternMatching {

    @Override
    protected void handlers(PatternMatcher.Builder<Event, List<Command>> builder) {
        builder
                //payment batches
                .match(PaymentBatchUpdatedEvent.class, e -> List.of(new RegisterPaymentCommand(e.getPayment(), false)))
                .match(PaymentBatchClosedEvent.class, this::handleBatchClosed)

                //customers
                .match(CustomerPaymentVerifiedEvent.class, e -> List.of(new SubmitPaymentCommand(e.getPayment(), false)))
                .match(CustomerPaymentRejectedEvent.class, e -> List.of(new RejectPaymentCommand(e.getPayment(),e.getReason(), false)))
                .match(CustomerPaymentApprovalRequiredEvent.class, e -> List.of(new RequestPaymentApprovalCommand(e.getPayment(),e.getReason(), false)))

                //psp - post submission
                .match(PaymentSubmissionFailedEvent.class, this::handleFailPaymentSubmission)
                .match(PaymentSubmittedEvent.class, this::handleCompletePaymentSubmission)
                .match(PaymentReturnedEvent.class, this::handleReturnPayment)
                .match(PaymentCompleteEvent.class, this::handlePaymentComplete)

                //psp - cancellation
                .match(PaymentCancelFailedEvent.class, this::handlePaymentCancelComplete)
                .match(PaymentCancelCompleteEvent.class, this::handlePaymentCancelFail)

                .orElse(List.empty());
    }

    private List<Command> handlePaymentCancelFail(final PaymentCancelCompleteEvent e) {
        return List.of(CompleteCancelPaymentCommand.builder()
                                               .method(e.getMethod())
                                               .paymentId(e.getPaymentId())
                                               .pspMetadata(e.getPspMetadata())
                                               .psp(e.getPsp())
                                               .build());
    }

    private List<Command> handlePaymentCancelComplete(final PaymentCancelFailedEvent e) {
        return List.of(FailCancelPaymentCommand.builder()
                                               .method(e.getMethod())
                                               .paymentId(e.getPaymentId())
                                               .pspMetadata(e.getPspMetadata())
                                               .psp(e.getPsp())
                                               .code(e.getCode())
                                               .message(e.getMessage())
                                                   .build());
    }

    private List<Command> handleFailPaymentSubmission(final PaymentSubmissionFailedEvent e) {
        return List.of(FailPaymentSubmissionCommand.builder()
                                                       .method(e.getMethod())
                                                       .paymentId(e.getPaymentId())
                                                       .failureDetails(e.getFailureDetails())
                                                       .build());
    }

    private List<Command> handleCompletePaymentSubmission(final PaymentSubmittedEvent e) {
        return List.of(CompletePaymentSubmissionCommand.builder()
                                           .method(e.getMethod())
                                           .paymentId(e.getPaymentId())
                                           .psp(e.getPsp())
                                           .pspMetadata(e.getPspMetadata())
                                           .failureDetails(e.getFailureDetails())
                                           .build());
    }

    private List<Command> handleReturnPayment(final PaymentReturnedEvent e) {
        return List.of(ReturnPaymentCommand.builder()
                                           .amount(e.getAmount())
                                           .code(e.getCode())
                                           .message(e.getMessage())
                                           .method(e.getMethod())
                                           .paymentId(e.getPaymentId())
                                           .psp(e.getPsp())
                                           .pspMetadata(e.getPspMetadata())
                                           .build());
    }

    private List<Command> handlePaymentComplete(final PaymentCompleteEvent e) {
        return List.of(CompletePaymentCommand.builder()
                                           .method(e.getMethod())
                                           .paymentId(e.getPaymentId())
                                           .psp(e.getPsp())
                                           .pspMetadata(e.getPspMetadata())
                                           .build());
    }

    private List<Command> handleBatchClosed(PaymentBatchClosedEvent e) {
        return e.getCarriedState().getPayments().map(id -> new ReleasePaymentCommand(id,false));
    }

}
