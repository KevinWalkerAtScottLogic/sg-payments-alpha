package gov.scot.payments.payments.pm.app;

import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.MessageDeDuplicator;
import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.application.component.processmanager.EventFilter;
import gov.scot.payments.application.component.processmanager.ProcessManager;
import gov.scot.payments.application.component.processmanager.ProcessManagerApplication;
import gov.scot.payments.application.component.processmanager.ProcessManagerDelegate;
import gov.scot.payments.application.component.processmanager.RetryHandler;
import gov.scot.payments.customers.model.event.CustomerPaymentApprovalRequiredEvent;
import gov.scot.payments.customers.model.event.CustomerPaymentRejectedEvent;
import gov.scot.payments.customers.model.event.CustomerPaymentVerifiedEvent;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchClosedEvent;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchUpdatedEvent;
import gov.scot.payments.psps.model.event.PaymentCancelCompleteEvent;
import gov.scot.payments.psps.model.event.PaymentCancelFailedEvent;
import gov.scot.payments.psps.model.event.PaymentCompleteEvent;
import gov.scot.payments.psps.model.event.PaymentReturnedEvent;
import gov.scot.payments.psps.model.event.PaymentSubmissionFailedEvent;
import gov.scot.payments.psps.model.event.PaymentSubmittedEvent;
import org.apache.kafka.common.header.Headers;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.util.function.Predicate;

@ApplicationComponent
public class PaymentsPMApplication extends ProcessManagerApplication {

    /*
    @Bean
    public StreamJoiner<Event,Event,Event> joiner(SchemaRegistryClient confluentSchemaRegistryClient){
        HeaderPreservingCompositeNonNativeSerde<Event> eventSerde = valueSerde(Event.class);
        return StreamJoiner.<Event, Event, Event>builder()
            .repartitionTopicName()
            .joinWindow()
            .joiner()
            .rekeyFunction()
            .leftValueSerde(eventSerde)
            .rightValueSerde(eventSerde)
            .stateSerde(stateSerde(confluentSchemaRegistryClient, SerializedMessage.class))
            .build();
    }
     */

    @Bean
    public PaymentsEventTransformer eventTransformFunction() {
        return new PaymentsEventTransformer();
    }

    @Bean
    public Predicate<Headers> externalEventHeaderFilter(MessageTypeRegistry messageTypeRegistry){
        return h -> messageTypeRegistry.matchesType(h
                , PaymentBatchUpdatedEvent.class
                ,PaymentBatchClosedEvent.class
                ,CustomerPaymentVerifiedEvent.class
                ,CustomerPaymentRejectedEvent.class
                ,CustomerPaymentApprovalRequiredEvent.class
                , PaymentSubmissionFailedEvent.class
                , PaymentSubmittedEvent.class
                , PaymentReturnedEvent.class
                , PaymentCompleteEvent.class
                , PaymentCancelFailedEvent.class
                , PaymentCancelCompleteEvent.class);
    }

    public static void main(String[] args){
        BaseApplication.run(PaymentsPMApplication.class,args);
    }
}