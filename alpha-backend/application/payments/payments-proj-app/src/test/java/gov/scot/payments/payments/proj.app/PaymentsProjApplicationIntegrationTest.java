package gov.scot.payments.payments.proj.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.component.projector.springdata.RepositoryMutatingStorageService;
import gov.scot.payments.application.component.projector.springdata.jpa.JpaProjectionRepository;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.event.PaymentInvalidEvent;
import gov.scot.payments.payments.model.event.PaymentReadyForSubmissionEvent;
import gov.scot.payments.payments.model.event.PaymentRegisteredEvent;
import gov.scot.payments.payments.proj.model.PaymentDetails;
import gov.scot.payments.payments.proj.model.PaymentSummary;
import gov.scot.payments.payments.proj.model.PaymentSummaryStatus;
import gov.scot.payments.payments.proj.model.RecipientInformation;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.vavr.collection.List;
import org.apache.kafka.streams.KafkaStreams;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.util.UriBuilder;

import java.math.BigDecimal;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Collections;
import java.util.UUID;
import java.util.function.Function;

import static gov.scot.payments.testing.security.MockSubjectAuthentication.user;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.HOURS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockAuthentication;

@ApplicationIntegrationTest(classes = {PaymentsProjApplicationIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.PROJECTOR)
public class PaymentsProjApplicationIntegrationTest {

    private static final UUID TEST_ID = UUID.randomUUID();
    private static final String BATCH_ID = "batch_id";
    public static final String CURRENCY_CODE = "GBP";
    public static final String AMOUNT = "123.45";
    private static final String ACCOUNT_NUMBER = "12345678";
    private static final String SORT_CODE = "123456";
    private static final String CREDITOR_NAME = "creditorName";
    private static final String CREATED_BY = "Test User";
    private static final Instant CREATED_AT = Instant.parse("2020-01-03T12:00:00.00Z");
    private static final Instant TIMESTAMP_1 = Instant.parse("2020-01-03T12:01:00.00Z");
    private static final Instant TIMESTAMP_2 = Instant.parse("2020-01-03T12:02:00.00Z");
    private static final Instant TIMESTAMP_3 = Instant.parse("2020-01-03T12:03:00.00Z");
    private static final CompositeReference PRODUCT = CompositeReference.parse("Customer.Product");

    // These two dates should be the same
    private static final Instant LATEST_EXECUTION_DATE = Instant.parse("2020-01-06T00:00:00.00Z");
    private static final LocalDate PAYMENT_DATE = LocalDate.of(2020, 1, 6);

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter) {
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
    }

    @AfterEach
    public void tearDown(@Autowired PaymentsSummaryRepository repository) {
        repository.deleteAll();
    }


    @ParameterizedTest
    @ValueSource(strings = {"/", "/details/"})
    @DisplayName("Check projection ACL when querying by Id")
    public void testRepositoryFindByIdAcl(String uri, @Autowired WebTestClient client, @Autowired PaymentsSummaryRepository repository) throws InterruptedException {

        var paymentSummary = PaymentSummary.builder()
                .id(TEST_ID)
                .batchId(BATCH_ID)
                .createdAt(CREATED_AT)
                .status(PaymentSummaryStatus.AwaitingApproval)
                .amount(new BigDecimal(AMOUNT))
                .currency(CURRENCY_CODE)
                .paymentDate(PAYMENT_DATE)
                .processingTime(now().minus(5, HOURS))
                .customer("customer")
                .product("customer.product")
                .build();

        JpaProjectionRepository saveRepo = repository;
        saveRepo.save(paymentSummary);

        var idPath = uri + TEST_ID.toString();
        webQueryRepository(client, idPath, "./payments:Read")
                .jsonPath("$.id").isEqualTo(TEST_ID.toString());

        webQueryRepository(client, idPath, "customer/payments:Read")
                .jsonPath("$.id").isEqualTo(TEST_ID.toString());

        webQueryRepository(client, idPath, "customer.product/payments:Read")
                .jsonPath("$.id").isEqualTo(TEST_ID.toString());

        webQueryRepositoryExpectNotFound(client, idPath, "customer.product2/payments:Read");
        webQueryRepositoryExpectNotFound(client, idPath, "customer2.product/payments:Read");
        webQueryRepositoryExpectNotFound(client, idPath, "customer2/payments:Read");
    }

    @Test
    @DisplayName("Check projection ACL when querying for all")
    public void testRepositoryFindAllAcl( @Autowired WebTestClient client, @Autowired PaymentsSummaryRepository repository) throws InterruptedException {

        CompositeReference scope1 = CompositeReference.parse("customer.product");
        CompositeReference scope2 = CompositeReference.parse("customer.product2");
        CompositeReference scope3 = CompositeReference.parse("customer2.product");

        JpaProjectionRepository saveRepo = repository;
        saveRepo.deleteAll();

        var paymentSummary = PaymentSummary.builder()
                .id(TEST_ID)
                .batchId(BATCH_ID)
                .createdAt(CREATED_AT)
                .status(PaymentSummaryStatus.AwaitingApproval)
                .amount(new BigDecimal(AMOUNT))
                .currency(CURRENCY_CODE)
                .paymentDate(PAYMENT_DATE)
                .processingTime(now().minus(5, HOURS))
                .customer(scope1.getComponent0())
                .product(scope1.toString())
                .build();

        saveRepo.save(paymentSummary);

        var paymentSummary2 = paymentSummary.toBuilder().id(UUID.randomUUID()).customer(scope2.getComponent0()).product(scope2.toString()).build();
        saveRepo.save(paymentSummary2);
        var paymentSummary3 = paymentSummary.toBuilder().id(UUID.randomUUID()).customer(scope3.getComponent0()).product(scope3.toString()).build();
        saveRepo.save(paymentSummary3);

        webQueryRepository(client, "/", "./payments:Read")
                .jsonPath("$._embedded.paymentSummaryList.size()").isEqualTo(3);

        webQueryRepository(client, "/", "customer.product/payments:Read")
                .jsonPath("$._embedded.paymentSummaryList.size()").isEqualTo(1);

        webQueryRepository(client, "/", "customer2.product/payments:Read")
                .jsonPath("$._embedded.paymentSummaryList.size()").isEqualTo(1);

        webQueryRepository(client, "/", "customer/payments:Read")
                .jsonPath("$._embedded.paymentSummaryList.size()").isEqualTo(2);

        webQueryRepository(client, "/", "customer2/payments:Read")
                .jsonPath("$._embedded.paymentSummaryList.size()").isEqualTo(1);

    }

    @Test
    @DisplayName("Check details projection ACL when querying for all")
    public void testDetailsRepositoryFindAllAcl( @Autowired WebTestClient client, @Autowired PaymentDetailsRepository repository) throws InterruptedException {

        CompositeReference scope1 = CompositeReference.parse("customer.product");
        CompositeReference scope2 = CompositeReference.parse("customer.product2");
        CompositeReference scope3 = CompositeReference.parse("customer2.product");

        var paymentSummary = PaymentDetails.builder()
                .id(TEST_ID)
                .batchId(BATCH_ID)
                .createdAt(CREATED_AT)
                .status(PaymentStatus.Registered)
                .amount(new BigDecimal(AMOUNT))
                .currency(CURRENCY_CODE)
                .paymentDate(PAYMENT_DATE)
                .processingTime(now().minus(5, HOURS))
                .customer(scope1.getComponent0())
                .product(scope1.toString())
                .events(Collections.emptyList())
                .type(PaymentMethod.Bacs)
                .message("test-message")
                .recipientInfo(RecipientInformation.fromAccount(getCreditorAccount(), getCreditor()))
                .build();

        JpaProjectionRepository saveRepo = repository;
        saveRepo.deleteAll();
        saveRepo.save(paymentSummary);

        var paymentSummary2 = paymentSummary.toBuilder().id(UUID.randomUUID()).customer(scope2.getComponent0()).product(scope2.toString()).build();
        saveRepo.save(paymentSummary2);
        var paymentSummary3 = paymentSummary.toBuilder().id(UUID.randomUUID()).customer(scope3.getComponent0()).product(scope3.toString()).build();
        saveRepo.save(paymentSummary3);

        webQueryRepository(client, "/details", "./payments:Read")
                .jsonPath("$._embedded.paymentDetailsList.size()").isEqualTo(3);

        webQueryRepository(client, "/details", "customer.product/payments:Read")
                .jsonPath("$._embedded.paymentDetailsList.size()").isEqualTo(1);

        webQueryRepository(client, "/details", "customer2.product/payments:Read")
                .jsonPath("$._embedded.paymentDetailsList.size()").isEqualTo(1);

        webQueryRepository(client, "/details", "customer/payments:Read")
                .jsonPath("$._embedded.paymentDetailsList.size()").isEqualTo(2);

        webQueryRepository(client, "/details", "customer2/payments:Read")
                .jsonPath("$._embedded.paymentDetailsList.size()").isEqualTo(1);

    }


    @Test
    @DisplayName("Check projection ACL when using search query")
    public void testRepositorySearchAcl( @Autowired WebTestClient client, @Autowired PaymentsSummaryRepository repository) throws InterruptedException {

        CompositeReference scope1 = CompositeReference.parse("customer.product");
        CompositeReference scope2 = CompositeReference.parse("customer.product2");
        CompositeReference scope3 = CompositeReference.parse("customer2.product");

        var paymentSummary = PaymentSummary.builder()
                .id(TEST_ID)
                .batchId(BATCH_ID)
                .createdAt(CREATED_AT)
                .status(PaymentSummaryStatus.AwaitingApproval)
                .amount(new BigDecimal(AMOUNT))
                .currency(CURRENCY_CODE)
                .paymentDate(PAYMENT_DATE)
                .processingTime(now().minus(5, HOURS))
                .customer(scope1.getComponent0())
                .product(scope1.toString())
                .build();

        JpaProjectionRepository saveRepo = repository;
        saveRepo.save(paymentSummary);

        var paymentSummary2 = paymentSummary.toBuilder().id(UUID.randomUUID()).customer(scope2.getComponent0()).product(scope2.toString()).build();
        saveRepo.save(paymentSummary2);
        var paymentSummary3 = paymentSummary.toBuilder().id(UUID.randomUUID()).customer(scope3.getComponent0()).product(scope3.toString()).build();
        saveRepo.save(paymentSummary3);

        var queryString = String.format("id=in=(%s,%s,%s)", paymentSummary.getId(), paymentSummary2.getId(), paymentSummary3.getId());

        Function<UriBuilder, URI> searchQuery = uri -> uri.path("/search")
                .queryParam("query", queryString)
                .build();

        webQueryRepository(client, searchQuery, "./payments:Read")
                .jsonPath("$._embedded.paymentSummaryList.size()").isEqualTo(3);

        webQueryRepository(client, searchQuery, "customer.product/payments:Read")
                .jsonPath("$._embedded.paymentSummaryList.size()").isEqualTo(1);

        webQueryRepository(client, searchQuery, "customer2.product/payments:Read")
                .jsonPath("$._embedded.paymentSummaryList.size()").isEqualTo(1);

        webQueryRepository(client, searchQuery, "customer2/payments:Read")
                .jsonPath("$._embedded.paymentSummaryList.size()").isEqualTo(1);

    }

    @Test
    @DisplayName("Check details record following PaymentCreatedEvent")
    public void testDetailsProjectionFollowingPaymentCreatedEvent(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient, @Autowired PaymentDetailsRepository repository) throws InterruptedException {

        var paymentCreatedEvent = PaymentRegisteredEvent.builder()
                .carriedState(Payment.builder()
                        .id(TEST_ID)
                        .batchId(BATCH_ID)
                        .createdBy(CREATED_BY)
                        .createdAt(CREATED_AT)
                        .status(PaymentStatus.ApprovalRequired)
                        .amount(Money.of(new BigDecimal(AMOUNT), CURRENCY_CODE))
                        .earliestExecutionDate(CREATED_AT)
                        .latestExecutionDate(LATEST_EXECUTION_DATE)
                        .product(PRODUCT)
                        .creditorAccount(getCreditorAccount())
                        .creditor(getCreditor())
                        .debtorAccount(getCreditorAccount())
                        .debtor(getCreditor())
                        .allowedMethods(List.empty())
                        .build())
                .stateVersion(99L)
                .correlationId(UUID.randomUUID())
                .timestamp(TIMESTAMP_1)
                .build();

        brokerClient.sendKeyValuesToDestination("events-internal", List.of(
                KeyValueWithHeaders.msg(paymentCreatedEvent.getKey(), paymentCreatedEvent)
        ));

        Thread.sleep(1000);

        var findById = repository.findById(TEST_ID);
        if (findById.isEmpty()) {
            fail("Failed to get PaymentDetails from repository by id");
        }
        var ps = findById.get();
        assertEquals(1, ps.getEvents().size());


        var res = client
                .mutateWith(mockAuthentication(user("pass", Role.parse("./payments:Read"))))
                .get()
                .uri(builder -> builder.path("/details").build())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .returnResult();

        webQueryRepository(client, "/" + TEST_ID.toString())
                .jsonPath("$.id").isEqualTo(TEST_ID.toString())
                .jsonPath("$.batchId").isEqualTo(BATCH_ID)
                .jsonPath("$.status").isEqualTo(PaymentSummaryStatus.AwaitingApproval.toString())
                .jsonPath("$.message").doesNotExist()
                .jsonPath("$.amount").isEqualTo(AMOUNT)
                .jsonPath("$.currency").isEqualTo(CURRENCY_CODE)
                .jsonPath("$.paymentDate").isEqualTo(PAYMENT_DATE.toString())
                .jsonPath("$.processingTime").isEqualTo(TIMESTAMP_1.toString());


    }




    @Test
    @DisplayName("Check projection record following PaymentCreatedEvent")
    public void testProjectionFollowingPaymentCreatedEvent(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient, @Autowired PaymentsSummaryRepository repository) throws InterruptedException {

        var paymentCreatedEvent = PaymentRegisteredEvent.builder()
                .carriedState(Payment.builder()
                        .id(TEST_ID)
                        .batchId(BATCH_ID)
                        .createdBy(CREATED_BY)
                        .createdAt(CREATED_AT)
                        .status(PaymentStatus.ApprovalRequired)
                        .amount(Money.of(new BigDecimal(AMOUNT), CURRENCY_CODE))
                        .earliestExecutionDate(CREATED_AT)
                        .latestExecutionDate(LATEST_EXECUTION_DATE)
                        .product(PRODUCT)
                        .creditorAccount(getCreditorAccount())
                        .creditor(getCreditor())
                        .debtorAccount(getCreditorAccount())
                        .debtor(getCreditor())
                        .allowedMethods(List.empty())
                        .build())
                .stateVersion(99L)
                .correlationId(UUID.randomUUID())
                .timestamp(TIMESTAMP_1)
                .build();

        brokerClient.sendKeyValuesToDestination("events-internal", List.of(
                KeyValueWithHeaders.msg(paymentCreatedEvent.getKey(), paymentCreatedEvent)
        ));

        Thread.sleep(1000);

        var findById = repository.findById(TEST_ID);
        if (findById.isEmpty()) {
            fail("Failed to get PaymentSummary from repository by id");
        }
        var ps = findById.get();
        assertThat(ps.getId(), is(TEST_ID));
        assertThat(ps.getBatchId(), is(BATCH_ID));
        assertThat(ps.getStatus(), is(PaymentSummaryStatus.AwaitingApproval));
        assertThat(ps.getMessage(), is(nullValue()));
        assertThat(ps.getAmount(), is(new BigDecimal(AMOUNT)));
        assertThat(ps.getCurrency(), is(CURRENCY_CODE));
        assertThat(ps.getPaymentDate(), is(PAYMENT_DATE));
        assertThat(ps.getProcessingTime(), is(TIMESTAMP_1));

        webQueryRepository(client, "/" + TEST_ID.toString())
                .jsonPath("$.id").isEqualTo(TEST_ID.toString())
                .jsonPath("$.batchId").isEqualTo(BATCH_ID)
                .jsonPath("$.status").isEqualTo(PaymentSummaryStatus.AwaitingApproval.toString())
                .jsonPath("$.message").doesNotExist()
                .jsonPath("$.amount").isEqualTo(AMOUNT)
                .jsonPath("$.currency").isEqualTo(CURRENCY_CODE)
                .jsonPath("$.paymentDate").isEqualTo(PAYMENT_DATE.toString())
                .jsonPath("$.processingTime").isEqualTo(TIMESTAMP_1.toString())
        ;
    }

    @Disabled
    @Test
    @DisplayName("Check projection record following PaymentInvalidEvent")
    public void testProjectionFollowingPaymentInvalidEvent(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient, @Autowired PaymentsSummaryRepository repository) throws InterruptedException {

        var paymentCreatedEvent = PaymentRegisteredEvent.builder()
                .carriedState(Payment.builder()
                        .id(TEST_ID)
                        .batchId(BATCH_ID)
                        .createdBy(CREATED_BY)
                        .createdAt(CREATED_AT)
                        .status(PaymentStatus.ApprovalRequired)
                        .amount(Money.of(new BigDecimal(AMOUNT), CURRENCY_CODE))
                        .earliestExecutionDate(CREATED_AT)
                        .latestExecutionDate(LATEST_EXECUTION_DATE)
                        .product(PRODUCT)
                        .creditorAccount(getCreditorAccount())
                        .creditor(getCreditor())
                        .debtorAccount(getCreditorAccount())
                        .debtor(getCreditor())
                        .allowedMethods(List.empty())
                        .build())
                .stateVersion(99L)
                .correlationId(UUID.randomUUID())
                .timestamp(TIMESTAMP_1)
                .build();

        var paymentInvalidEvent = PaymentInvalidEvent.builder()
                .carriedState(Payment.builder()
                        .id(TEST_ID)
                        .batchId(BATCH_ID)
                        .createdBy(CREATED_BY)
                        .createdAt(CREATED_AT)
                        .status(PaymentStatus.Invalid)
                        .amount(Money.of(new BigDecimal(AMOUNT), CURRENCY_CODE))
                        .earliestExecutionDate(CREATED_AT)
                        .latestExecutionDate(LATEST_EXECUTION_DATE)
                        .product(PRODUCT)
                        .creditorAccount(getCreditorAccount())
                        .creditor(getCreditor())
                        .debtorAccount(getCreditorAccount())
                        .debtor(getCreditor())
                        .allowedMethods(List.empty())
                        .validation(Validation.builder().status(Validation.Status.Failure).message("a").build())
                        .build())
                .stateVersion(99L)
                .correlationId(UUID.randomUUID())
                .timestamp(TIMESTAMP_2)
                .build();

        brokerClient.sendKeyValuesToDestination("events-internal", List.of(
                KeyValueWithHeaders.msg(paymentCreatedEvent.getKey(), paymentCreatedEvent),
                KeyValueWithHeaders.msg(paymentInvalidEvent.getKey(), paymentInvalidEvent)
        ));

        Thread.sleep(1000);

        var findById = repository.findById(TEST_ID);
        if (findById.isEmpty()) {
            fail("Failed to get PaymentSummary from repository by id");
        }
        var ps = findById.get();
        assertThat(ps.getId(), is(TEST_ID.toString()));
        assertThat(ps.getBatchId(), is(BATCH_ID));
        assertThat(ps.getStatus(), is(PaymentSummaryStatus.Error));
        assertThat(ps.getMessage(), is("a"));
        assertThat(ps.getAmount(), is(new BigDecimal(AMOUNT)));
        assertThat(ps.getCurrency(), is(CURRENCY_CODE));
        assertThat(ps.getPaymentDate(), is(PAYMENT_DATE));
        assertThat(ps.getProcessingTime(), is(TIMESTAMP_2));

        webQueryRepository(client, "/" + TEST_ID.toString())
                .jsonPath("$.id").isEqualTo(TEST_ID.toString())
                .jsonPath("$.batchId").isEqualTo(BATCH_ID)
                .jsonPath("$.status").isEqualTo(PaymentSummaryStatus.Error.toString())
                .jsonPath("$.message").isEqualTo("a")
                .jsonPath("$.amount").isEqualTo(AMOUNT)
                .jsonPath("$.currency").isEqualTo(CURRENCY_CODE)
                .jsonPath("$.paymentDate").isEqualTo(PAYMENT_DATE.toString())
                .jsonPath("$.processingTime").isEqualTo(TIMESTAMP_2.toString())
        ;
    }

    @Test
    @DisplayName("Check projection record following PaymentReadyForSubmissionEvent")
    public void testProjectionFollowingPaymentReadyForSubmissionEvent(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient, @Autowired PaymentsSummaryRepository repository) throws InterruptedException {

        var paymentCreatedEvent = PaymentRegisteredEvent.builder()
                .carriedState(Payment.builder()
                        .id(TEST_ID)
                        .batchId(BATCH_ID)
                        .createdBy(CREATED_BY)
                        .createdAt(CREATED_AT)
                        .status(PaymentStatus.ApprovalRequired)
                        .amount(Money.of(new BigDecimal(AMOUNT), CURRENCY_CODE))
                        .earliestExecutionDate(CREATED_AT)
                        .latestExecutionDate(LATEST_EXECUTION_DATE)
                        .product(PRODUCT)
                        .debtorAccount(getCreditorAccount())
                        .debtor(getCreditor())
                        .creditorAccount(getCreditorAccount())
                        .creditor(getCreditor())
                        .allowedMethods(List.empty())
                        .build())
                .stateVersion(99L)
                .correlationId(UUID.randomUUID())
                .timestamp(TIMESTAMP_1)
                .build();

        var paymentReadyForSubmissionEvent = PaymentReadyForSubmissionEvent.builder()
                .carriedState(Payment.builder()
                        .id(TEST_ID)
                        .batchId(BATCH_ID)
                        .createdBy(CREATED_BY)
                        .createdAt(CREATED_AT)
                        .status(PaymentStatus.ReadyForSubmission)
                        .amount(Money.of(new BigDecimal(AMOUNT), CURRENCY_CODE))
                        .earliestExecutionDate(CREATED_AT)
                        .latestExecutionDate(LATEST_EXECUTION_DATE)
                        .product(PRODUCT)
                        .debtorAccount(getCreditorAccount())
                        .debtor(getCreditor())
                        .creditorAccount(getCreditorAccount())
                        .creditor(getCreditor())
                        .allowedMethods(List.empty())
                        .build())
                .stateVersion(99L)
                .correlationId(UUID.randomUUID())
                .timestamp(TIMESTAMP_2)
                .build();

        brokerClient.sendKeyValuesToDestination("events-internal", List.of(
                KeyValueWithHeaders.msg(paymentCreatedEvent.getKey(), paymentCreatedEvent),
                KeyValueWithHeaders.msg(paymentReadyForSubmissionEvent.getKey(), paymentReadyForSubmissionEvent)
        ));

        Thread.sleep(1000);

        var findById = repository.findById(TEST_ID);
        if (findById.isEmpty()) {
            fail("Failed to get PaymentSummary from repository by id");
        }
        var ps = findById.get();
        assertThat(ps.getId(), is(TEST_ID));
        assertThat(ps.getBatchId(), is(BATCH_ID));
        assertThat(ps.getStatus(), is(PaymentSummaryStatus.Returned));
        assertThat(ps.getMessage(), is(nullValue()));
        assertThat(ps.getAmount(), is(new BigDecimal(AMOUNT)));
        assertThat(ps.getCurrency(), is(CURRENCY_CODE));
        assertThat(ps.getPaymentDate(), is(PAYMENT_DATE));
        assertThat(ps.getProcessingTime(), is(TIMESTAMP_2));

        webQueryRepository(client, "/" + TEST_ID.toString())
                .jsonPath("$.id").isEqualTo(TEST_ID.toString())
                .jsonPath("$.batchId").isEqualTo(BATCH_ID)
                .jsonPath("$.status").isEqualTo(PaymentSummaryStatus.Returned.toString())
                .jsonPath("$.message").doesNotExist()
                .jsonPath("$.amount").isEqualTo(AMOUNT)
                .jsonPath("$.currency").isEqualTo(CURRENCY_CODE)
                .jsonPath("$.paymentDate").isEqualTo(PAYMENT_DATE.toString())
                .jsonPath("$.processingTime").isEqualTo(TIMESTAMP_2.toString())
        ;
    }

    @Test
    @DisplayName("Test queries with multiple PaymentSummary records")
    public void testProjectionWithMultiplePaymentSummaryRecords(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient) throws InterruptedException {

        var testId1 = UUID.randomUUID();
        var testId2 = UUID.randomUUID();
        var testId3 = UUID.randomUUID();

        var batchIdA = "batchIdA";
        var batchIdB = "batchIdB";

        var payment1 = Payment.builder()
                .id(testId1)
                .batchId(batchIdA)
                .createdBy(CREATED_BY)
                .createdAt(CREATED_AT)
                .status(PaymentStatus.ApprovalRequired)
                .amount(Money.of(new BigDecimal(AMOUNT), CURRENCY_CODE))
                .earliestExecutionDate(CREATED_AT)
                .latestExecutionDate(LATEST_EXECUTION_DATE)
                .product(PRODUCT)
                .debtorAccount(getCreditorAccount())
                .debtor(getCreditor())
                .creditorAccount(getCreditorAccount())
                .creditor(getCreditor())
                .allowedMethods(List.empty())
                .build();

        var payment2 = payment1.toBuilder()
                .id(testId2)
                .batchId(batchIdB)
                .build();

        var payment3 = payment1.toBuilder()
                .id(testId3)
                .batchId(batchIdA)
                .build();

        var paymentCreatedEvent1 = PaymentRegisteredEvent.builder()
                .carriedState(payment1)
                .stateVersion(99L)
                .correlationId(UUID.randomUUID())
                .timestamp(TIMESTAMP_1)
                .build();

        var paymentCreatedEvent2 = PaymentRegisteredEvent.builder()
                .carriedState(payment2)
                .stateVersion(99L)
                .correlationId(UUID.randomUUID())
                .timestamp(TIMESTAMP_2)
                .build();

        var paymentCreatedEvent3 = PaymentRegisteredEvent.builder()
                .carriedState(payment3)
                .stateVersion(99L)
                .correlationId(UUID.randomUUID())
                .timestamp(TIMESTAMP_3)
                .build();


        brokerClient.sendKeyValuesToDestination("events-internal", List.of(
                KeyValueWithHeaders.msg(paymentCreatedEvent1.getKey(), paymentCreatedEvent1),
                KeyValueWithHeaders.msg(paymentCreatedEvent2.getKey(), paymentCreatedEvent2),
                KeyValueWithHeaders.msg(paymentCreatedEvent3.getKey(), paymentCreatedEvent3)
        ));

        Thread.sleep(1000);

        // Test getting second PaymentSummary records by id
        webQueryRepository(client, "/" + testId2.toString())
                .jsonPath("$.id").isEqualTo(testId2.toString())
                .jsonPath("$.batchId").isEqualTo(batchIdB)
                .jsonPath("$.status").isEqualTo(PaymentSummaryStatus.AwaitingApproval.toString())
                .jsonPath("$.message").doesNotExist()
                .jsonPath("$.amount").isEqualTo(AMOUNT)
                .jsonPath("$.currency").isEqualTo(CURRENCY_CODE)
                .jsonPath("$.paymentDate").isEqualTo(PAYMENT_DATE.toString())
                .jsonPath("$.processingTime").isEqualTo(TIMESTAMP_2.toString())
        ;

        // Test getting all PaymentSummary records
        webQueryRepository(client,
                (uri -> uri.path("/search")
                        .queryParam("query", String.format("id=in=(%s,%s)", testId1.toString(),testId2.toString()))
                        .build()))
                .jsonPath("$._embedded.paymentSummaryList.size()").isEqualTo(2)
                // Ordered by processing time, latest first
                .jsonPath("$._embedded.paymentSummaryList[0].id").isEqualTo(testId2.toString())
                .jsonPath("$._embedded.paymentSummaryList[0].batchId").isEqualTo(batchIdB)
                .jsonPath("$._embedded.paymentSummaryList[1].id").isEqualTo(testId1.toString())
                .jsonPath("$._embedded.paymentSummaryList[1].batchId").isEqualTo(batchIdA)
        ;

        // Test getting first and second PaymentSummary records
        webQueryRepository(client,
                (uri -> uri.path("/search")
                        .queryParam("query", "id=in=("+testId1+","+testId2+")")
                        .build()))
                .jsonPath("$._embedded.paymentSummaryList.size()").isEqualTo(2)
                // Ordered by processing time, latest first
                .jsonPath("$._embedded.paymentSummaryList[0].id").isEqualTo(testId2.toString())
                .jsonPath("$._embedded.paymentSummaryList[0].batchId").isEqualTo(batchIdB)
                .jsonPath("$._embedded.paymentSummaryList[1].id").isEqualTo(testId1.toString())
                .jsonPath("$._embedded.paymentSummaryList[1].batchId").isEqualTo(batchIdA)
        ;

        // Test getting all PaymentSummary records with batch id = batchIdA
        webQueryRepository(client,
                (uri -> uri.path("/search")
                        .queryParam("query", "batchId=="+ batchIdA)
                        .build()))
                .jsonPath("$._embedded.paymentSummaryList.size()").isEqualTo(2)
                // Ordered by processing time, latest first
                .jsonPath("$._embedded.paymentSummaryList[0].id").isEqualTo(testId3.toString())
                .jsonPath("$._embedded.paymentSummaryList[0].batchId").isEqualTo(batchIdA)
                .jsonPath("$._embedded.paymentSummaryList[1].id").isEqualTo(testId1.toString())
                .jsonPath("$._embedded.paymentSummaryList[1].batchId").isEqualTo(batchIdA)
        ;
    }

    private WebTestClient.BodyContentSpec webQueryRepository(WebTestClient client, String uri, String role) {
        return webQueryRepository(client, uriBuilder -> uriBuilder.path(uri).build(), role);
    }

    private WebTestClient.BodyContentSpec webQueryRepository(WebTestClient client, String uri) {
        return webQueryRepository(client, uriBuilder -> uriBuilder.path(uri).build(), "./payments:Read");
    }

    private WebTestClient.BodyContentSpec webQueryRepository(WebTestClient client, Function<UriBuilder, URI> uriFunction) {
        return webQueryRepository(client, uriFunction, "./payments:Read");
    }

    private WebTestClient.BodyContentSpec webQueryRepository(WebTestClient client, Function<UriBuilder, URI> uriFunction, String role) {
        return client
                .mutateWith(mockAuthentication(user("pass", Role.parse(role))))
                .get()
                .uri(uriFunction)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody();
    }

    private void webQueryRepositoryExpectNotFound(WebTestClient client, String uri, String role) {
        client
                .mutateWith(mockAuthentication(user("pass", Role.parse(role))))
                .get()
                .uri(uriBuilder -> uriBuilder.path(uri).build())
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    private UKBankAccount getCreditorAccount() {
        return UKBankAccount.builder()
                .name("acct")
                .currency(CURRENCY_CODE)
                .sortCode(new SortCode(SORT_CODE))
                .accountNumber(new UKAccountNumber(ACCOUNT_NUMBER))
                .build();
    }

    private PartyIdentification getCreditor() {
        return PartyIdentification.builder()
                .name(CREDITOR_NAME)
                .build();
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    @EnableAutoConfiguration(exclude = {HypermediaAutoConfiguration.class})
    public static class TestApplication extends PaymentsProjApplication {

        @Bean
        public RepositoryMutatingStorageService<UUID, PaymentSummary> handler(Metrics metrics, PaymentsSummaryRepository repository) {
            return new PaymentsSummaryStorageService(metrics, repository);
        }
    }
}
