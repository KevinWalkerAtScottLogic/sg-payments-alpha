package gov.scot.payments.payments.proj.app;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.event.*;
import gov.scot.payments.payments.proj.model.PaymentSummaryStatus;
import io.vavr.collection.List;
import org.junit.jupiter.params.provider.Arguments;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.jupiter.params.provider.Arguments.arguments;

public class PaymentEventTestDataGenerator {

    public static final UUID TEST_ID = UUID.randomUUID();
    private static final String BATCH_ID = "batch_id";
    public static final String CURRENCY_CODE = "GBP";
    public static final long AMOUNT = 123L;
    private static final String ACCOUNT_NUMBER = "12345678";
    private static final String SORT_CODE = "123456";
    private static final String CREDITOR_NAME = "creditorName";
    private static final String CREATED_BY = "Test User";
    private static final Instant CREATED_AT = Instant.parse("2020-01-03T12:00:00.00Z");
    private static final CompositeReference PRODUCT = CompositeReference.parse("Customer.Product");

    // These two dates should be the same
    private static final Instant LATEST_EXECUTION_DATE = Instant.parse("2020-01-06T00:00:00.00Z");
    private static final LocalDate PAYMENT_DATE = LocalDate.of(2020, 1, 6);

    private final Payment examplePayment;

    public PaymentEventTestDataGenerator(Payment payment){
        this.examplePayment = payment;
    }

    private static UKBankAccount getCreditorAccount() {
        return UKBankAccount.builder()
                .name("acct")
                .currency(CURRENCY_CODE)
                .sortCode(new SortCode(SORT_CODE))
                .accountNumber(new UKAccountNumber(ACCOUNT_NUMBER))
                .build();
    }

    private static PartyIdentification getCreditor() {
        return PartyIdentification.builder()
                .name(CREDITOR_NAME)
                .build();
    }

    private Arguments getPaymentReadyForSubmissionEventTestArguments(Payment payment) {

        var registerPayment = payment.toBuilder()
                .status(PaymentStatus.ReadyForSubmission)
                .build();

        var invalidEvent = PaymentReadyForSubmissionEvent.builder()
                .carriedState(registerPayment)
                .build();

        return createEventTestArguments(invalidEvent, null);

    }

    private Arguments getPaymentApprovalRequestedTestArguments(Payment payment) {

        var registerPayment = payment.toBuilder()
                .status(PaymentStatus.ApprovalRequired)
                .build();

        var invalidEvent = PaymentApprovalRequestedEvent.builder()
                .carriedState(registerPayment)
                .build();

        return createEventTestArguments(invalidEvent, null);

    }

    private Arguments getPaymentCancellationRequestedEventArguments(Payment payment) {

        var errorMessage = "cancel_payment";

        var cancelPayment = payment.toBuilder()
                .status(PaymentStatus.Submitted)
                .cancellation(Cancellation.builder()
                                .reason(errorMessage)
                                .user("user")
                                .method(PaymentMethod.Bacs)
                                .build())
                .build();

        var invalidEvent = PaymentCancellationRequestedEvent.builder()
                .carriedState(cancelPayment)
                .build();

        return createEventTestArguments(invalidEvent, errorMessage);

    }

    private Arguments getPaymentCancellationFailedEventArguments(Payment payment) {

        var errorMessage = "cancel_failed";

        var cancelPayment = payment.toBuilder()
                .status(PaymentStatus.Submitted)
                .cancellation(Cancellation.builder()
                        .message(errorMessage)
                        .user("user")
                        .reason("reason")
                        .method(PaymentMethod.Bacs)
                        .build())
                .build();

        var invalidEvent = PaymentCancellationFailedEvent.builder()
                .carriedState(cancelPayment)
                .build();

        return createEventTestArguments(invalidEvent, errorMessage);

    }

    private Arguments getPaymentCancellationCompleteEventArguments(Payment payment) {

        var cancelPayment = payment.toBuilder()
                .status(PaymentStatus.Invalid)
                .cancellation(Cancellation.builder()
                        .reason("error")
                        .user("user")
                        .method(PaymentMethod.Bacs)
                        .build())
                .build();

        var invalidEvent = PaymentCancellationCompleteEvent.builder()
                .carriedState(cancelPayment)
                .build();

        return createEventTestArguments(invalidEvent, null);

    }

    private Arguments getPaymentCancelApprovedArguments(Payment payment) {

        var approvedCancelPayment = payment.toBuilder()
                .status(PaymentStatus.Rejected)
                .approval(Approval.builder()
                        .requiredBy("user")
                        .status(Approval.Status.Approved)
                        .build())
                .build();

        var invalidEvent = PaymentCancelApprovedEvent.builder()
                .carriedState(approvedCancelPayment)
                .build();

        return createEventTestArguments(invalidEvent,  null);

    }

    private Arguments getPaymentCancelRejectedArguments(Payment payment) {

        var errorMessage = "cancelled_rejected";

        var approvedCancelPayment = payment.toBuilder()
                .status(PaymentStatus.Rejected)
                .approval(Approval.builder()
                        .requiredBy("user")
                        .reason(errorMessage)
                        .status(Approval.Status.Rejected)
                        .build())
                .cancellation(null)
                .build();

        var cancelEvent = PaymentCancelRejectedEvent.builder()
                .carriedState(approvedCancelPayment)
                .build();

        return createEventTestArguments(cancelEvent, errorMessage);

    }

    private Arguments getPaymentCompleteEventArguments(Payment payment) {

        var invalidPayment = payment.toBuilder()
                .status(PaymentStatus.Paid)
                .build();

        var invalidEvent = PaymentCompleteEvent.builder()
                .carriedState(invalidPayment)
                .build();

        return createEventTestArguments(invalidEvent, null);

    }

    private Arguments getPaymentSubmissionSuccessEventArguments(Payment payment) {

        var submitted = payment.toBuilder()
                .status(PaymentStatus.Submitted)
                .build();

        var invalidEvent = PaymentSubmissionSuccessEvent.builder()
                .carriedState(submitted)
                .build();

        return createEventTestArguments(invalidEvent, null);

    }

    private Arguments getPaymentValidEventArguments(Payment payment) {

        var submitted = payment.toBuilder()
                .status(PaymentStatus.Valid)
                .build();

        var invalidEvent = PaymentValidEvent.builder()
                .carriedState(submitted)
                .build();

        return createEventTestArguments(invalidEvent, null);

    }

    private Arguments getPaymentInvalidEventArguments(Payment payment) {

        var errorMessage = "invalid";

        var invalidPayment = payment.toBuilder()
                .status(PaymentStatus.Invalid)
                .validation(
                        Validation.builder()
                                .status(Validation.Status.Failure)
                                .message(errorMessage)
                                .build())
                .build();

        var invalidEvent = PaymentInvalidEvent.builder()
                .carriedState(invalidPayment)
                .build();

        return createEventTestArguments(invalidEvent, errorMessage);

    }

    private Arguments getPaymentReturnedEventArguments(Payment payment) {

        var errorMessage = "returned";

        var returnedPayment = payment.toBuilder()
                .status(PaymentStatus.Returned)
                .returns(Return.builder()
                        .paymentMethod(PaymentMethod.Bacs)
                        .psp("psp")
                        .code("c")
                        .reason(errorMessage)
                        .build())
                .build();

        var returnedEvent = PaymentReturnedEvent.builder()
                .carriedState(returnedPayment)
                .build();

        return createEventTestArguments(returnedEvent, errorMessage);

    }

    private Arguments getPaymentSubmissionFailedEventArguments(Payment payment) {

        var submission = Submission.builder().paymentMethod(PaymentMethod.Bacs)
                .status(Submission.Status.Failure)
                .message("bacs_failed")
                .build();

        var submissionFailedPayment = payment.toBuilder()
                .status(PaymentStatus.SubmissionFailed)
                .submissions(List.of(submission))
                .build();

        var submissionFailedEvent = PaymentSubmissionFailedEvent.builder()
                .carriedState(submissionFailedPayment)
                .build();

        return createEventTestArguments(submissionFailedEvent, "bacs_failed");

    }

    private Arguments getMultiplePaymentSubmissionFailedEventArguments(Payment payment) {

        var submission = Submission.builder().paymentMethod(PaymentMethod.Bacs)
                .status(Submission.Status.Failure)
                .message("bacs_failed")
                .build();

        var submission2 = Submission.builder().paymentMethod(PaymentMethod.FasterPayments)
                .status(Submission.Status.Failure)
                .message("fp_failed")
                .build();

        var submissionFailedPayment = payment.toBuilder()
                .status(PaymentStatus.SubmissionFailed)
                .submissions(List.of(submission, submission2))
                .build();

        var submissionFailedEvent = PaymentSubmissionFailedEvent.builder()
                .carriedState(submissionFailedPayment)
                .build();

        return createEventTestArguments(submissionFailedEvent, "bacs_failed, fp_failed");

    }

    private Arguments getPaymentRejectedEventArguments(Payment payment) {

        var errorMessage = "rejected";

        var invalidPayment = payment.toBuilder()
                .status(PaymentStatus.Rejected)
                .approval(Approval.builder()
                            .requiredBy("user")
                            .reason(errorMessage)
                            .status(Approval.Status.Rejected)
                            .build())
                .build();

        var invalidEvent = PaymentRejectedEvent.builder()
                .carriedState(invalidPayment)
                .build();

        return createEventTestArguments(invalidEvent, errorMessage);

    }

    public Arguments createEventTestArguments(BasePaymentEventWithCause event, String expectedMessage){
        return arguments(
                event.getClass().getSimpleName(),
                examplePayment,
                event,
                PaymentSummaryStatus.fromPaymentStatus(event.getCarriedState().getStatus()),
                expectedMessage
        );
    }

    Stream<Arguments> getEventStateUpdateTestParameters() {

        return Stream.of(
                getPaymentReadyForSubmissionEventTestArguments(examplePayment),
                getPaymentApprovalRequestedTestArguments(examplePayment),
                getPaymentCancellationRequestedEventArguments(examplePayment),
                getPaymentCancellationFailedEventArguments(examplePayment),
                getPaymentCancellationCompleteEventArguments(examplePayment),
                getPaymentReadyForSubmissionEventTestArguments(examplePayment),
                getPaymentCancelApprovedArguments(examplePayment),
                getPaymentCancelRejectedArguments(examplePayment),
                getPaymentCompleteEventArguments(examplePayment),
                getPaymentReadyForSubmissionEventTestArguments(examplePayment),
                getPaymentSubmissionSuccessEventArguments(examplePayment),
                getPaymentValidEventArguments(examplePayment),
                getPaymentInvalidEventArguments(examplePayment),
                getPaymentReturnedEventArguments(examplePayment),
                getPaymentSubmissionFailedEventArguments(examplePayment),
                getMultiplePaymentSubmissionFailedEventArguments(examplePayment),
                getPaymentRejectedEventArguments(examplePayment)
        );
    }



}
