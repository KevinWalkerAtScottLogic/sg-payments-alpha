package gov.scot.payments.payments.proj.app;

import gov.scot.payments.application.component.projector.springdata.jpa.JpaProjectionRepository;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.payments.proj.model.PaymentDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface PaymentDetailsRepository extends JpaProjectionRepository<UUID, PaymentDetails>{

    @Query("from PaymentDetails p where " +
            "( p.product in (?#{#principal.getScopesAsString()}) " +
            "or  p.customer in (?#{#principal.getRootScopesAsString()}) " +
            "or ?#{#principal.hasGlobalAccess()} = true )")
    Page<PaymentDetails> findAll(Pageable pageable, @Param("principal") Subject subject);

    @Query("from PaymentDetails p where p.id = ?#{#id} and " +
            "( p.product in (?#{#principal.getScopesAsString()}) " +
            "or p.customer in (?#{#principal.getRootScopesAsString()}) " +
            "or ?#{#principal.hasGlobalAccess()} = true )")
    Optional<PaymentDetails> findById(@Param("id") UUID id, @Param("principal") Subject subject);
}
