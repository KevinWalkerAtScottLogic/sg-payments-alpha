package gov.scot.payments.payments.proj.app;

import gov.scot.payments.application.component.projector.springdata.ProjectionRepository;
import gov.scot.payments.application.component.projector.springdata.RepositoryMutatingStorageService;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Event;
import gov.scot.payments.payments.model.event.*;
import gov.scot.payments.payments.proj.model.PaymentDetails;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

@Slf4j
public class PaymentDetailsStorageService extends RepositoryMutatingStorageService<UUID, PaymentDetails>{

    public PaymentDetailsStorageService(final Metrics metrics, final ProjectionRepository<UUID, PaymentDetails> repository) {
        super(metrics, repository);
    }

    @Override
    protected boolean shouldDelete(Event event) {
        return false;
    }

    @Override
    protected void idExtractors(PatternMatcher.Builder2<Event,String,UUID> builder) {
        builder.match2(BasePaymentEventWithCause.class, (e, s) -> UUID.fromString(s));
    }

    @Override
    protected void createHandlers(PatternMatcher.Builder<Event, PaymentDetails> builder) {
        builder.match(PaymentRegisteredEvent.class, PaymentDetails::fromRegisterEvent);
    }

    @Override
    protected void updateHandlers(PatternMatcher.Builder2<Event, PaymentDetails, PaymentDetails> builder) {
        builder
                .match2(PaymentReadyForSubmissionEvent.class, (e, s) -> s.updateFromEvent(e))
                .match2(PaymentApprovalRequestedEvent.class, (e, s) -> s.updateFromEvent(e))
                .match2(PaymentCancellationRequestedEvent.class, (e, s) -> s.updateFromEvent(e))
                .match2(PaymentCancellationCompleteEvent.class, (e, s) -> s.updateFromEvent(e))
                .match2(PaymentCancellationFailedEvent.class, (e, s) ->s.updateFromEvent(e))
                .match2(PaymentCancelApprovedEvent.class, (e, s) -> s.updateFromEvent(e))
                .match2(PaymentCancelRejectedEvent.class, (e, s) -> s.updateFromEvent(e))
                .match2(PaymentCompleteEvent.class, (e, s) -> s.updateFromEvent(e))
                .match2(PaymentSubmissionSuccessEvent.class, (e, s) -> s.updateFromEvent(e))
                .match2(PaymentValidEvent.class, (e, s) -> s.updateFromEvent(e))
                .match2(PaymentInvalidEvent.class, (e, s) -> s.updateFromEvent(e))
                .match2(PaymentReturnedEvent.class, (e, s) -> s.updateFromEvent(e))
                .match2(PaymentSubmissionFailedEvent.class, (e, s) -> s.updateFromEvent(e))
                .match2(PaymentRejectedEvent.class, (e, s) -> s.updateFromEvent(e));

    }
}
