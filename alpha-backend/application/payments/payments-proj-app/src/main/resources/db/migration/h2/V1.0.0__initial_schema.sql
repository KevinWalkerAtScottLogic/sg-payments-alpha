CREATE domain IF NOT EXISTS jsonb AS other;

CREATE TABLE PAYMENT_SUMMARY (
    id VARCHAR(255) NOT NULL PRIMARY KEY,
    processing_time TIMESTAMP NOT NULL,
    batch_id VARCHAR(255),
    created_at TIMESTAMP NOT NULL,
    status INTEGER NOT NULL,
    message VARCHAR(255),
    amount DECIMAL NOT NULL,
    currency VARCHAR(3),
    payment_date DATE NOT NULL
);
