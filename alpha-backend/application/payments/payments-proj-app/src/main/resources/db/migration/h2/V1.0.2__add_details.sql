CREATE TABLE PAYMENT_DETAILS (
    id UUID NOT NULL PRIMARY KEY,
    processing_time TIMESTAMP NOT NULL,
    batch_id VARCHAR(255),
    created_at TIMESTAMP NOT NULL,
    product VARCHAR(255) NOT NULL,
    customer VARCHAR(255) NOT NULL,
    amount DECIMAL NOT NULL,
    currency VARCHAR(3),
    payment_date DATE NOT NULL,
    type VARCHAR(50),
    message VARCHAR(255),
    name VARCHAR(255) NOT NULL,
    client_reference VARCHAR(255),
    sort_code VARCHAR(8),
    account_number VARCHAR(10),
    roll_number VARCHAR(10),
    status VARCHAR(50) NOT NULL,
    iban VARCHAR(30),
    state_version BIGINT
);

CREATE TABLE PAYMENT_DETAILS_EVENTS(
    date TIMESTAMP NOT NULL,
    status VARCHAR(50) NOT NULL,
    details VARCHAR(255),
    user_val VARCHAR(100),
    payment_details_id UUID NOT NULL
);

