CREATE TABLE PAYMENT_DETAILS (
    id UUID NOT NULL PRIMARY KEY,
    processing_time TIMESTAMP NOT NULL,
    batch_id TEXT,
    created_at TIMESTAMP NOT NULL,
    product TEXT NOT NULL,
    customer TEXT NOT NULL,
    amount DECIMAL NOT NULL,
    currency CHAR(3) NOT NULL,
    payment_date DATE NOT NULL,
    type TEXT,
    message TEXT,
    name TEXT NOT NULL,
    client_reference TEXT,
    sort_code CHAR(6),
    account_number CHAR(8),
    roll_number TEXT,
    status TEXT NOT NULL,
    iban VARCHAR(30),
    state_version INTEGER
);

CREATE TABLE PAYMENT_DETAILS_EVENTS(
    date TIMESTAMP NOT NULL,
    status TEXT NOT NULL,
    details TEXT,
    user_val TEXT,
    payment_details_id UUID NOT NULL REFERENCES PAYMENT_DETAILS (id)
);

CREATE INDEX PAYMENT_DETAILS_processing_time ON PAYMENT_DETAILS(processing_time DESC);

