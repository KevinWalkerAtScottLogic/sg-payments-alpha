package gov.scot.payments.payments.ch.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.customers.model.aggregate.PaymentMethodConfiguration;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.command.*;
import gov.scot.payments.payments.model.event.PaymentReadyForSubmissionEvent;
import gov.scot.payments.payments.model.event.PaymentRegisteredEvent;
import gov.scot.payments.payments.model.api.RejectPaymentRequest;
import gov.scot.payments.payments.model.event.*;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.vavr.collection.List;
import org.apache.kafka.streams.KafkaStreams;
import org.assertj.core.api.Assertions;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Map;
import java.util.UUID;

import static gov.scot.payments.payments.ch.app.PaymentTestUtils.*;
import static gov.scot.payments.testing.security.MockSubjectAuthentication.user;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockAuthentication;

@ApplicationIntegrationTest(classes = {PaymentsCHApplicationIntegrationTest.TestApplication.class}
        , componentType = ApplicationIntegrationTest.ComponentType.COMMAND_HANDLER)
public class PaymentsCHApplicationIntegrationTest {

    private static final String COMMANDS_TOPIC = "commands";
    private static final String EVENTS_TOPIC = "events";

    private static final String TEST_USER = "testUser";
    private static final String AMOUNT_VALUE = "100";
    private static final String BATCH_ID = "111";
    private static final String CREDITOR_NAME = "someName";
    private static final String CURRENCY_CODE = "GBP";
    private static final String CLIENT_REFERENCE = "ref1";
    private static final String PRODUCT_REFERENCE = "ref2";
    private static final String ACCOUNT_NUMBER = "12345678";
    private static final String SORT_CODE = "123456";
    private static final String USER = "testUser";

    private static final List<PaymentMethod> ALLOWED_METHODS = List.of(PaymentMethod.Bacs);
    private static final Money AMOUNT = Money.of(new BigDecimal(AMOUNT_VALUE), CURRENCY_CODE);
    private static final CompositeReference PRODUCT = new CompositeReference(CLIENT_REFERENCE, PRODUCT_REFERENCE);

    private static final Submission FAILURE_SUBMISSION_EVENT = Submission.builder()
            .status(Submission.Status.Failure)
            .paymentMethod(PaymentMethod.Bacs)
            .build();
    private static final Submission SUCCESS_SUBMISSION_EVENT = Submission.builder()
            .status(Submission.Status.Success)
            .paymentMethod(PaymentMethod.Bacs)
            .psp("psp")
            .pspMetadata(Map.of())
            .build();
    public static final Approval APPROVED_APPROVAL_EVENT = Approval.builder()
            .status(Approval.Status.Approved)
            .user(USER+"1")
            .requiredBy("r")
            .build();
    public static final Approval REJECTED_APPROVAL_EVENT = Approval.builder()
            .status(Approval.Status.Rejected)
            .requiredBy("r")
            .reason(REASON)
            .user(USER+"1")
            .build();
    public static final Settlement SETTLEMENT_EVENT = Settlement.builder()
            .paymentMethod(PaymentMethod.Bacs)
            .psp("psp")
            .build();
    public static final Return RETURN_EVENT = Return.builder()
            .paymentMethod(PaymentMethod.Bacs)
            .psp("psp")
            .code("c")
            .reason(REASON)
            .build();

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, EmbeddedBrokerClient brokerClient, @Value("${namespace}") String namespace) {
        brokerClient.getBroker().addTopicsIfNotExists(namespace + "-payments-command-errors");
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
    }

    @Test
    public void testRegisterPaymentCommand(EmbeddedBrokerClient brokerClient){

        Payment originalPayment = getValidPayment(UUID.randomUUID());
        var finalizeCreatePaymentMsg = KeyValueWithHeaders.msg("1",
                RegisterPaymentCommand.builder()
                        .payment(originalPayment)
                        .build());

        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(finalizeCreatePaymentMsg));

        List<PaymentRegisteredEvent> createdEvents = brokerClient.readAllFromDestination(EVENTS_TOPIC, PaymentRegisteredEvent.class);
        assertEquals(1, createdEvents.size());

        Payment createdPayment = createdEvents.get(0).getCarriedState();
        assertEquals(originalPayment.getKey(), createdEvents.get(0).getKey());
        assertEquals(originalPayment, createdPayment);
    }

    @Test
    public void testRegisterPaymentCommandWithNoPayments(EmbeddedBrokerClient brokerClient){

        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.empty());

        List<PaymentRegisteredEvent> createdEvents = brokerClient.readAllFromDestination(EVENTS_TOPIC, PaymentRegisteredEvent.class);
        assertEquals(0, createdEvents.size());

    }

    @Test
    public void testRegisterPaymentCommandWithMultiplePayments(EmbeddedBrokerClient brokerClient){

        Payment payment1 = getValidPayment(UUID.randomUUID());
        var finalizeCreatePaymentMsg = KeyValueWithHeaders.msg(payment1.getKey(),
                RegisterPaymentCommand.builder()
                        .payment(payment1)
                        .build());

        Payment payment2 = getValidPayment(UUID.randomUUID());
        var finalizeCreatePaymentMsg2 = KeyValueWithHeaders.msg(payment2.getKey(),
                RegisterPaymentCommand.builder()
                        .payment(payment2)
                        .build());

        Payment payment3 = getValidPayment(UUID.randomUUID());
        var finalizeCreatePaymentMsg3 = KeyValueWithHeaders.msg(payment3.getKey(),
                RegisterPaymentCommand.builder()
                        .payment(payment3)
                        .build());

        Payment payment4 = getValidPayment(UUID.randomUUID());
        var finalizeCreatePaymentMsg4 = KeyValueWithHeaders.msg(payment4.getKey(),
                RegisterPaymentCommand.builder()
                        .payment(payment4)
                        .build());

        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(finalizeCreatePaymentMsg, finalizeCreatePaymentMsg2, finalizeCreatePaymentMsg3, finalizeCreatePaymentMsg4));

        List<PaymentRegisteredEvent> createdEvents = brokerClient.readAllFromDestination(EVENTS_TOPIC, PaymentRegisteredEvent.class);
        assertEquals(4, createdEvents.size());

        PaymentRegisteredEvent event1 = createdEvents.get(0);
        assertEquals(payment1.getKey(), event1.getKey());
        assertEquals(payment1, event1.getCarriedState());

        PaymentRegisteredEvent event2 = createdEvents.get(1);
        assertEquals(payment2.getKey(), event2.getKey());
        assertEquals(payment2, event2.getCarriedState());

        PaymentRegisteredEvent event3 = createdEvents.get(2);
        assertEquals(payment3.getKey(), event3.getKey());
        assertEquals(payment3, event3.getCarriedState());

        PaymentRegisteredEvent event4 = createdEvents.get(3);
        assertEquals(payment4.getKey(), event4.getKey());
        assertEquals(payment4, event4.getCarriedState());
    }

    @Test
    @DisplayName("Test RejectPayment")
    void testCreatePaymentCommandThenRejectPaymentPathway(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient) {

        var expectedNumEvents = 0;
        var stateVersion = 0L;

        // Send a CreatePaymentCommand via Kafka

        var paymentId = UUID.randomUUID();
        var payment = getValidPayment(paymentId);

        var createPaymentMsg = KeyValueWithHeaders.msg(paymentId.toString(),
                RegisterPaymentCommand.builder()
                        .payment(payment)
                        .build());
        var releasePaymentMsg = KeyValueWithHeaders.msg(paymentId.toString(),
                ReleasePaymentCommand.builder()
                                      .paymentId(paymentId)
                                      .build());
        var verifyPaymentMsg = KeyValueWithHeaders.msg(paymentId.toString(),
                RequestPaymentApprovalCommand.builder()
                                     .paymentId(paymentId)
                                             .reason("r")
                                     .build());

        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(createPaymentMsg,releasePaymentMsg, verifyPaymentMsg));

        expectedNumEvents+=3;
        stateVersion+=3;

        var events = brokerClient.readAllFromDestination(EVENTS_TOPIC, BasePaymentEventWithCause.class);
        assertThat(events.size(), is(expectedNumEvents));

        var lastEvent = events.get(expectedNumEvents - 1);
        assertThat(lastEvent, instanceOf(PaymentApprovalRequestedEvent.class));
        assertThat(lastEvent.getStateVersion(), is(stateVersion));
        assertThat(lastEvent.getKey(), is(paymentId.toString()));
        checkExpectedPaymentValues(lastEvent.getCarriedState(), PaymentStatus.ApprovalRequired);

        // Reject payment via web client

        client
                .mutateWith(mockAuthentication(user(USER+"1", Role.parse("payments/payments:ApprovePayment"))))
                .put()
                .uri(uri -> uri.path("/command/rejectPaymentApproval")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, String.valueOf(stateVersion))
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(RejectPaymentRequest.builder()
                        .paymentId(payment.getId())
                        .reason(REASON)
                        .build())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.stateVersion").isEqualTo(String.valueOf(stateVersion + 1))
                .jsonPath("$.carriedState.id").isEqualTo(payment.getKey())
                .jsonPath("$.carriedState.status").isEqualTo(PaymentStatus.Rejected.toString())
                .jsonPath("$.carriedState.approval.status").isEqualTo(Approval.Status.Rejected.toString())
                .jsonPath("$.carriedState.approval.reason").isEqualTo(REASON)
                .jsonPath("$.carriedState.approval.user").isEqualTo(USER+"1")
                .returnResult();

        expectedNumEvents++;
        stateVersion++;

        events = brokerClient.readAllFromDestination(EVENTS_TOPIC, BasePaymentEventWithCause.class);
        assertThat(events.size(), is(expectedNumEvents));

        lastEvent = events.get(expectedNumEvents - 1);
        assertThat(lastEvent, instanceOf(PaymentRejectedEvent.class));
        Payment createdPayment = lastEvent.getCarriedState();
        assertThat(lastEvent.getStateVersion(), is(stateVersion));
        assertThat(lastEvent.getKey(), is(paymentId.toString()));
        checkExpectedPaymentValues(createdPayment, PaymentStatus.Rejected);
        Assertions.assertThat(createdPayment.getApproval()).isEqualToIgnoringGivenFields(REJECTED_APPROVAL_EVENT,"timestamp");
        assertThat(createdPayment.getSubmissions().asJava(), is(empty()));
//        assertThat(createdPayment.getSettlementEvent(), is(expectedSettlementEvent));
//        assertThat(createdPayment.getReturnEvent(), is(nullValue()));
    }

    @Test
    @DisplayName("Test ApprovePayment then FailPaymentSubmission")
    void testApprovePaymentThenFailPaymentSubmissionPathway(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient) {

        var expectedNumEvents = 0;
        var stateVersion = 0L;

        // CreatePayment via Kafka

        var paymentId = UUID.randomUUID();
        var payment = getValidPayment(paymentId);

        var createPaymentMsg = KeyValueWithHeaders.msg(paymentId.toString(),
                RegisterPaymentCommand.builder()
                        .payment(payment)
                        .build());
        var releasePaymentMsg = KeyValueWithHeaders.msg(paymentId.toString(),
                ReleasePaymentCommand.builder()
                                     .paymentId(paymentId)
                                     .build());
        var verifyPaymentMsg = KeyValueWithHeaders.msg(paymentId.toString(),
                RequestPaymentApprovalCommand.builder()
                                             .paymentId(paymentId)
                                             .reason("r")
                                             .build());

        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(createPaymentMsg,releasePaymentMsg,verifyPaymentMsg));

        expectedNumEvents+=3;
        stateVersion+=3;

        // Approve payment via the web client

        client
                .mutateWith(mockAuthentication(user(USER+"1", Role.parse("payments/payments:ApprovePayment"))))
                .put()
                .uri(uri -> uri.path("/command/approvePayment")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, String.valueOf(stateVersion))
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue('"' + paymentId.toString() + '"')
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.stateVersion").isEqualTo(String.valueOf(stateVersion + 1))
                .jsonPath("$.carriedState.id").isEqualTo(payment.getKey())
                .jsonPath("$.carriedState.status").isEqualTo(PaymentStatus.ReadyForSubmission.toString())
                .jsonPath("$.carriedState.approval.status").isEqualTo(Approval.Status.Approved.toString())
                .jsonPath("$.carriedState.approval.user").isEqualTo(USER+"1")
                .returnResult();

        expectedNumEvents++;
        stateVersion++;

        var events = brokerClient.readAllFromDestination(EVENTS_TOPIC, BasePaymentEventWithCause.class);
        assertThat(events.size(), is(expectedNumEvents));

        var lastEvent = events.get(expectedNumEvents - 1);
        assertThat(lastEvent, instanceOf(PaymentReadyForSubmissionEvent.class));
        assertThat(lastEvent.getStateVersion(), is(stateVersion));
        assertThat(lastEvent.getKey(), is(paymentId.toString()));
        var createdPayment = lastEvent.getCarriedState();
        checkExpectedPaymentValues(createdPayment, PaymentStatus.ReadyForSubmission);
        Assertions.assertThat(createdPayment.getApproval()).isEqualToIgnoringGivenFields(APPROVED_APPROVAL_EVENT,"timestamp");
//        assertThat(createdPayment.getSettlementEvent(), is(expectedSettlementEvent));
//        assertThat(createdPayment.getReturnEvent(), is(nullValue()));

        // Fail payment submission via Kafka

        var failPaymentSubmissionMsg = KeyValueWithHeaders.msg(paymentId.toString(),
                FailPaymentSubmissionCommand.builder()
                        .paymentId(paymentId)
                        .reply(true)
                        .method(PaymentMethod.Bacs)
                        .failureDetails(List.of())
                        .build());

        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(failPaymentSubmissionMsg));

        expectedNumEvents++;
        stateVersion++;

        events = brokerClient.readAllFromDestination(EVENTS_TOPIC, BasePaymentEventWithCause.class);
        assertThat(events.size(), is(expectedNumEvents));

        lastEvent = events.get(expectedNumEvents - 1);
        assertThat(lastEvent, instanceOf(PaymentSubmissionFailedEvent.class));
        assertThat(lastEvent.getStateVersion(), is(stateVersion));
        assertThat(lastEvent.getKey(), is(paymentId.toString()));
        createdPayment = lastEvent.getCarriedState();
        checkExpectedPaymentValues(createdPayment, PaymentStatus.SubmissionFailed);
        Assertions.assertThat(createdPayment.getApproval()).isEqualToIgnoringGivenFields(APPROVED_APPROVAL_EVENT,"timestamp");
        Assertions.assertThat(createdPayment.getSubmissions().get(0)).isEqualToIgnoringGivenFields(FAILURE_SUBMISSION_EVENT,"timestamp");
//        assertThat(createdPayment.getSettlementEvent(), is(nullValue()));
//        assertThat(createdPayment.getReturnEvent(), is(nullValue()));
    }

    @Test
    @DisplayName("Test ApprovePayment, CompletePaymentSubmission then CompletePayment")
    void testApprovePaymentThenCompletePaymentSubmissionThenCompletePaymentPathway(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient) {

        var stateVersion = 0L;
        var expectedNumEvents = 0;

        // CreatePayment via Kafka

        var paymentId = UUID.randomUUID();
        var payment = getValidPayment(paymentId)
                .toBuilder()
                .amount(AMOUNT)
                .build();

        var createPaymentMsg = KeyValueWithHeaders.msg(paymentId.toString(),
                RegisterPaymentCommand.builder()
                        .payment(payment)
                        .build());
        var releasePaymentMsg = KeyValueWithHeaders.msg(paymentId.toString(),
                ReleasePaymentCommand.builder()
                        .paymentId(paymentId)
                        .build());
        var verifyPaymentMsg = KeyValueWithHeaders.msg(paymentId.toString(),
                RequestPaymentApprovalCommand.builder()
                        .paymentId(paymentId)
                        .reason("r")
                        .build());

        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(createPaymentMsg,releasePaymentMsg,verifyPaymentMsg));

        expectedNumEvents+=3;
        stateVersion+=3;

        // Approve payment via the web client

        client
                .mutateWith(mockAuthentication(user(USER+"1", Role.parse("payments/payments:ApprovePayment"))))
                .put()
                .uri(uri -> uri.path("/command/approvePayment")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, String.valueOf(stateVersion))
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue('"' + paymentId.toString() + '"')
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.stateVersion").isEqualTo(String.valueOf(stateVersion + 1))
                .jsonPath("$.carriedState.id").isEqualTo(payment.getKey())
                .jsonPath("$.carriedState.status").isEqualTo(PaymentStatus.ReadyForSubmission.toString())
                .jsonPath("$.carriedState.approval.status").isEqualTo(Approval.Status.Approved.toString())
                .jsonPath("$.carriedState.approval.user").isEqualTo(USER+"1")
                .returnResult();

        expectedNumEvents++;
        stateVersion++;

        var events = brokerClient.readAllFromDestination(EVENTS_TOPIC, BasePaymentEventWithCause.class);
        assertThat(events.size(), is(expectedNumEvents));

        var lastEvent = events.get(expectedNumEvents - 1);
        assertThat(lastEvent, instanceOf(PaymentReadyForSubmissionEvent.class));
        var createdPayment = lastEvent.getCarriedState();
        assertThat(lastEvent.getStateVersion(), is(stateVersion));
        assertThat(lastEvent.getKey(), is(paymentId.toString()));
        checkExpectedPaymentValues(createdPayment, PaymentStatus.ReadyForSubmission);
        Assertions.assertThat(createdPayment.getApproval()).isEqualToIgnoringGivenFields(APPROVED_APPROVAL_EVENT,"timestamp");
//        assertThat(createdPayment.getSettlementEvent(), is(nullValue()));
//        assertThat(createdPayment.getReturnEvent(), is(nullValue()));

        // Complete payment submission via Kafka

        var completePaymentSubmissionMsg = KeyValueWithHeaders.msg(paymentId.toString(),
                CompletePaymentSubmissionCommand.builder()
                        .paymentId(paymentId)
                        .method(PaymentMethod.Bacs)
                        .psp("psp")
                        .reply(true)
                        .build());

        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(completePaymentSubmissionMsg));

        expectedNumEvents++;
        stateVersion++;

        events = brokerClient.readAllFromDestination(EVENTS_TOPIC, BasePaymentEventWithCause.class);
        assertThat(events.size(), is(expectedNumEvents));

        lastEvent = events.get(expectedNumEvents - 1);
        assertThat(lastEvent, instanceOf(PaymentSubmissionSuccessEvent.class));
        assertThat(lastEvent.getStateVersion(), is(stateVersion));
        assertThat(lastEvent.getKey(), is(paymentId.toString()));
        createdPayment = lastEvent.getCarriedState();
        checkExpectedPaymentValues(createdPayment, PaymentStatus.Submitted);
        Assertions.assertThat(createdPayment.getApproval()).isEqualToIgnoringGivenFields(APPROVED_APPROVAL_EVENT,"timestamp");
        Assertions.assertThat(createdPayment.getSubmissions().get(0)).isEqualToIgnoringGivenFields(SUCCESS_SUBMISSION_EVENT,"timestamp");
//        assertThat(createdPayment.getSettlementEvent(), is(nullValue()));
//        assertThat(createdPayment.getReturnEvent(), is(nullValue()));

        // Complete payment via Kafka

        var completePaymentMsg = KeyValueWithHeaders.msg(paymentId.toString(),
                CompletePaymentCommand.builder()
                        .paymentId(paymentId)
                        .reply(true)
                        .method(PaymentMethod.Bacs)
                        .psp("psp")
                        .build());

        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(completePaymentMsg));

        expectedNumEvents++;
        stateVersion++;

        events = brokerClient.readAllFromDestination(EVENTS_TOPIC, BasePaymentEventWithCause.class);
        assertThat(events.size(), is(expectedNumEvents));

        lastEvent = events.get(expectedNumEvents - 1);
        assertThat(lastEvent, instanceOf(PaymentCompleteEvent.class));
        assertThat(lastEvent.getStateVersion(), is(stateVersion));
        assertThat(lastEvent.getKey(), is(paymentId.toString()));
        createdPayment = lastEvent.getCarriedState();
        checkExpectedPaymentValues(createdPayment, PaymentStatus.Paid);
        Assertions.assertThat(createdPayment.getApproval()).isEqualToIgnoringGivenFields(APPROVED_APPROVAL_EVENT,"timestamp");
        Assertions.assertThat(createdPayment.getSubmissions().get(0)).isEqualToIgnoringGivenFields(SUCCESS_SUBMISSION_EVENT,"timestamp");
//        assertThat(createdPayment.getSettlementEvent(), is(SETTLEMENT_EVENT));
//        assertThat(createdPayment.getReturnEvent(), is(nullValue()));
    }

    @Test
    @DisplayName("Test ApprovePayment, CompletePaymentSubmission then ReturnPayment")
    void testApprovePaymentThenCompletePaymentSubmissionThenReturnPaymentPathway(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient) {
        /*
         * Test ApprovePayment, CompletePaymentSubmission then CompletePayment
         */

        var stateVersion = 0L;
        var expectedNumEvents = 0;

        // CreatePayment via Kafka

        var paymentId = UUID.randomUUID();
        var payment = getValidPayment(paymentId)
                .toBuilder()
                .amount(AMOUNT)
                .build();

        var createPaymentMsg = KeyValueWithHeaders.msg(paymentId.toString(),
                RegisterPaymentCommand.builder()
                        .payment(payment)
                        .build());
        var releasePaymentMsg = KeyValueWithHeaders.msg(paymentId.toString(),
                ReleasePaymentCommand.builder()
                                     .paymentId(paymentId)
                                     .build());
        var verifyPaymentMsg = KeyValueWithHeaders.msg(paymentId.toString(),
                RequestPaymentApprovalCommand.builder()
                                             .paymentId(paymentId)
                                             .reason("r")
                                             .build());

        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(createPaymentMsg,releasePaymentMsg,verifyPaymentMsg));

        expectedNumEvents+=3;
        stateVersion+=3;

        // Approve payment via the web client

        client
                .mutateWith(mockAuthentication(user(USER+"1", Role.parse("payments/payments:ApprovePayment"))))
                .put()
                .uri(uri -> uri.path("/command/approvePayment")
                        .queryParam("reply", true)
                        .build())
                .header(HttpHeaders.IF_MATCH, String.valueOf(stateVersion))
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue('"' + paymentId.toString() + '"')
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.stateVersion").isEqualTo(String.valueOf(stateVersion + 1))
                .jsonPath("$.carriedState.id").isEqualTo(payment.getKey())
                .jsonPath("$.carriedState.status").isEqualTo(PaymentStatus.ReadyForSubmission.toString())
                .jsonPath("$.carriedState.approval.status").isEqualTo(Approval.Status.Approved.toString())
                .jsonPath("$.carriedState.approval.user").isEqualTo(USER+"1")
                .returnResult();

        expectedNumEvents++;
        stateVersion++;

        var events = brokerClient.readAllFromDestination(EVENTS_TOPIC, BasePaymentEventWithCause.class);
        assertThat(events.size(), is(expectedNumEvents));

        var lastEvent = events.get(expectedNumEvents - 1);
        assertThat(lastEvent, instanceOf(PaymentReadyForSubmissionEvent.class));
        var createdPayment = lastEvent.getCarriedState();
        assertThat(lastEvent.getStateVersion(), is(stateVersion));
        assertThat(lastEvent.getKey(), is(paymentId.toString()));
        checkExpectedPaymentValues(createdPayment, PaymentStatus.ReadyForSubmission);
        Assertions.assertThat(createdPayment.getApproval()).isEqualToIgnoringGivenFields(APPROVED_APPROVAL_EVENT,"timestamp");
//        assertThat(createdPayment.getSettlementEvent(), is(nullValue()));
//        assertThat(createdPayment.getReturnEvent(), is(nullValue()));

        // Complete payment submission via Kafka

        var completePaymentSubmissionMsg = KeyValueWithHeaders.msg(paymentId.toString(),
                CompletePaymentSubmissionCommand.builder()
                        .paymentId(paymentId)
                        .reply(true)
                        .method(PaymentMethod.Bacs)
                        .psp("psp")
                        .build());

        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(completePaymentSubmissionMsg));

        expectedNumEvents++;
        stateVersion++;

        events = brokerClient.readAllFromDestination(EVENTS_TOPIC, BasePaymentEventWithCause.class);
        assertThat(events.size(), is(expectedNumEvents));

        lastEvent = events.get(expectedNumEvents - 1);
        assertThat(lastEvent, instanceOf(PaymentSubmissionSuccessEvent.class));
        assertThat(lastEvent.getStateVersion(), is(stateVersion));
        assertThat(lastEvent.getKey(), is(paymentId.toString()));
        createdPayment = lastEvent.getCarriedState();
        checkExpectedPaymentValues(createdPayment, PaymentStatus.Submitted);
        Assertions.assertThat(createdPayment.getApproval()).isEqualToIgnoringGivenFields(APPROVED_APPROVAL_EVENT,"timestamp");
        Assertions.assertThat(createdPayment.getSubmissions().get(0)).isEqualToIgnoringGivenFields(SUCCESS_SUBMISSION_EVENT,"timestamp");
//        assertThat(createdPayment.getSettlementEvent(), is(nullValue()));
//        assertThat(createdPayment.getReturnEvent(), is(nullValue()));

        // Return payment via Kafka

        var returnPaymentMsg = KeyValueWithHeaders.msg(paymentId.toString(),
                ReturnPaymentCommand.builder()
                        .paymentId(paymentId)
                        .reply(true)
                                    .method(PaymentMethod.Bacs)
                                    .psp("psp")
                                    .message("m")
                                    .code("c")
                                    .amount(Money.parse("GBP 1"))
                        .build());

        brokerClient.sendKeyValuesToDestination(COMMANDS_TOPIC, List.of(returnPaymentMsg));

        expectedNumEvents++;
        stateVersion++;

        events = brokerClient.readAllFromDestination(EVENTS_TOPIC, BasePaymentEventWithCause.class);
        assertThat(events.size(), is(expectedNumEvents));

        lastEvent = events.get(expectedNumEvents - 1);
        assertThat(lastEvent, instanceOf(PaymentReturnedEvent.class));
        assertThat(lastEvent.getStateVersion(), is(stateVersion));
        assertThat(lastEvent.getKey(), is(paymentId.toString()));
        createdPayment = lastEvent.getCarriedState();
        checkExpectedPaymentValues(createdPayment, PaymentStatus.Returned);
        Assertions.assertThat(createdPayment.getApproval()).isEqualToIgnoringGivenFields(APPROVED_APPROVAL_EVENT,"timestamp");
        Assertions.assertThat(createdPayment.getSubmissions().get(0)).isEqualToIgnoringGivenFields(SUCCESS_SUBMISSION_EVENT,"timestamp");
//        assertThat(createdPayment.getSettlementEvent(), is(nullValue()));
//        assertThat(createdPayment.getReturnEvent(), is(nullValue()));
    }

    private Payment getValidPayment(UUID id) {

        return Payment.builder()
                .id(id)
                .batchId(BATCH_ID)
                .allowedMethods(ALLOWED_METHODS)
                .amount(AMOUNT)
                .latestExecutionDate(LocalDate.now().plusDays(7).atStartOfDay().toInstant(ZoneOffset.UTC))
                .product(PRODUCT)
                .methodProperties(Map.of(PaymentMethod.Bacs.name(),Map.of(PaymentMethodConfiguration.BACS_SERVICE_USER_NUMBER,"",PaymentMethodConfiguration.BACS_SERVICE_ACCOUNT_NAME,"")))
                .creditorAccount(
                        UKBankAccount.builder()
                                .name("acct")
                                .currency(CURRENCY_CODE)
                                .sortCode(new SortCode(SORT_CODE))
                                .accountNumber(new UKAccountNumber(ACCOUNT_NUMBER))
                                .build())
                .creditor(
                        PartyIdentification.builder()
                                .name(CREDITOR_NAME)
                                .build())
                .debtorAccount(
                        UKBankAccount.builder()
                                .name("acct")
                                .currency(CURRENCY_CODE)
                                .sortCode(new SortCode(SORT_CODE))
                                .accountNumber(new UKAccountNumber(ACCOUNT_NUMBER))
                                .build())
                .debtor(
                        PartyIdentification.builder()
                                .name(CREDITOR_NAME)
                                .build())
                .createdBy(USER)
                .build();
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends PaymentsCHApplication {}


}