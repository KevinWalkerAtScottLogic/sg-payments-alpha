package gov.scot.payments.payments.ch.app;

import gov.scot.payments.customers.model.aggregate.PaymentMethodConfiguration;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import io.vavr.collection.List;
import net.objectlab.kit.datecalc.common.DateCalculator;
import net.objectlab.kit.datecalc.common.DefaultHolidayCalendar;
import net.objectlab.kit.datecalc.jdk8.LocalDateKitCalculatorsFactory;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.threeten.extra.Days;

import java.time.*;
import java.time.temporal.TemporalAdjusters;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class PaymentValidatorTest {

    private PaymentValidator validator;

    @BeforeEach
    public void setUp(){
        validator = new PaymentValidator(new DefaultHolidayCalendar<>());
    }

    @Test
    public void testGeneralValidation(){
        Payment payment = Payment.builder()
                .status(PaymentStatus.ReadyForSubmission)
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.Bacs))
                .amount(Money.parse("GBP 1"))
                .product(new CompositeReference("cust","prod"))
                .latestExecutionDate(Instant.now().plus(Days.of(7)))
                .methodProperties(Map.of(PaymentMethod.Bacs.name(),Map.of(PaymentMethodConfiguration.BACS_SERVICE_USER_NUMBER,"123456",PaymentMethodConfiguration.BACS_SERVICE_ACCOUNT_NAME,"")))
                .debtor(PartyIdentification.builder().name("person").build())
                .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .creditor(PartyIdentification.builder().name("person").build())
                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .build();
        assertEquals(PaymentStatus.Valid,validator.validate(payment).getStatus());
        assertEquals(PaymentStatus.Invalid,validator.validate(payment.toBuilder().amount(Money.parse("GBP -1")).build()).getStatus());
        assertEquals(PaymentStatus.Invalid,validator.validate(payment.toBuilder().allowedMethods(List.of()).build()).getStatus());
        assertEquals(PaymentStatus.Invalid,validator.validate(payment.toBuilder().allowedMethods(List.of(PaymentMethod.Bacs,PaymentMethod.Bacs)).build()).getStatus());
        assertEquals(PaymentStatus.Invalid,validator.validate(payment.toBuilder().latestExecutionDate(Instant.now().minusSeconds(100)).build()).getStatus());
        assertEquals(PaymentStatus.Invalid,validator.validate(payment.toBuilder().latestExecutionDate(Instant.now().plusSeconds(10)).earliestExecutionDate(Instant.now().plusSeconds(20)).build()).getStatus());
    }

    @Test
    public void testValidateFasterPayments() {
        Payment payment = Payment.builder()
                .status(PaymentStatus.ReadyForSubmission)
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.FasterPayments))
                .amount(Money.parse("GBP 1"))
                .product(new CompositeReference("cust", "prod"))
                .latestExecutionDate(Instant.now().plus(Days.of(7)))
                .debtor(PartyIdentification.builder().name("person").build())
                .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .creditor(PartyIdentification.builder().name("person").build())
                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .build();
        assertEquals(PaymentStatus.Valid,validator.validate(payment).getStatus());
        assertEquals(PaymentStatus.Invalid,validator.validate(payment.toBuilder().amount(Money.parse("GBP 300000")).build()).getStatus());
        assertEquals(PaymentStatus.Invalid,validator.validate(payment.toBuilder().creditorAccount(SepaBankAccount.builder().name("act").iban("123").currency("EUR").build()).build()).getStatus());
    }

    @Test
    public void testValidateBacs() {
        Payment payment = Payment.builder()
                .status(PaymentStatus.ReadyForSubmission)
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.Bacs))
                .amount(Money.parse("GBP 1"))
                .product(new CompositeReference("cust", "prod"))
                .latestExecutionDate(Instant.now().plus(Days.of(7)))
                .methodProperties(Map.of(PaymentMethod.Bacs.name(),Map.of(PaymentMethodConfiguration.BACS_SERVICE_USER_NUMBER,"123456",PaymentMethodConfiguration.BACS_SERVICE_ACCOUNT_NAME,"")))
                .debtor(PartyIdentification.builder().name("person").build())
                .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .creditor(PartyIdentification.builder().name("person").build())
                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .build();
        assertEquals(PaymentStatus.Valid,validator.validate(payment).getStatus());
        assertEquals(PaymentStatus.Invalid,validator.validate(payment.toBuilder().amount(Money.parse("GBP 300000")).build()).getStatus());
        assertEquals(PaymentStatus.Invalid,validator.validate(payment.toBuilder().creditorAccount(SepaBankAccount.builder().name("act").iban("123").currency("EUR").build()).build()).getStatus());
    }

    @Test
    public void testValidateSepa() {
        Payment payment = Payment.builder()
                .status(PaymentStatus.ReadyForSubmission)
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.Sepa_DC))
                .amount(Money.parse("GBP 1"))
                .product(new CompositeReference("cust", "prod"))
                .latestExecutionDate(Instant.now().plus(Days.of(7)))
                .debtor(PartyIdentification.builder().name("person").build())
                .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .creditor(PartyIdentification.builder().name("person").build())
                .creditorAccount(SepaBankAccount.builder().name("act").iban("123").currency("EUR").build())
                .build();
        assertEquals(PaymentStatus.Valid,validator.validate(payment).getStatus());
        assertEquals(PaymentStatus.Invalid,validator.validate(payment.toBuilder().creditorAccount(SepaBankAccount.builder().name("act").iban("123").currency("GBP").build()).build()).getStatus());
        assertEquals(PaymentStatus.Invalid,validator.validate(payment.toBuilder().creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build()).build()).getStatus());
    }

    @Test
    public void testValidateiMovo() {
        Payment payment = Payment.builder()
                .status(PaymentStatus.ReadyForSubmission)
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.iMovo))
                .amount(Money.parse("GBP 1"))
                .product(new CompositeReference("cust", "prod"))
                .latestExecutionDate(Instant.now().plus(Days.of(7)))
                .debtor(PartyIdentification.builder().name("person").build())
                .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .creditor(PartyIdentification.builder().name("person").contactDetails(ContactDetails.builder().mobileNumber("12345123456").build()).build())
                .creditorAccount(SepaBankAccount.builder().name("act").iban("123").currency("EUR").build())
                .build();
        assertEquals(PaymentStatus.Valid,validator.validate(payment).getStatus());
        assertEquals(PaymentStatus.Invalid,validator.validate(payment.toBuilder().creditor(PartyIdentification.builder().name("joe").build()).build()).getStatus());
    }

    @Test
    public void testValidateChaps() {
        Payment payment = Payment.builder()
                .status(PaymentStatus.ReadyForSubmission)
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.Chaps))
                .amount(Money.parse("GBP 10000000"))
                .product(new CompositeReference("cust", "prod"))
                .latestExecutionDate(Instant.now().plus(Days.of(7)))
                .debtor(PartyIdentification.builder().name("person").build())
                .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .creditor(PartyIdentification.builder().name("person").build())
                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .build();
        assertEquals(PaymentStatus.Valid,validator.validate(payment).getStatus());
        assertEquals(PaymentStatus.Invalid,validator.validate(payment.toBuilder().creditorAccount(SepaBankAccount.builder().name("act").iban("123").currency("EUR").build()).build()).getStatus());
    }

    @Test
    public void testDateCalculation(){
        DateCalculator<LocalDate> cal = LocalDateKitCalculatorsFactory.backwardCalculator("UK");
        LocalDate today = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.MONDAY));
        LocalDate tPlus3 = today.plusDays(3);
        Payment payment = Payment.builder()
                .status(PaymentStatus.ReadyForSubmission)
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.Chaps))
                .amount(Money.parse("GBP 10000000"))
                .product(new CompositeReference("cust", "prod"))
                .latestExecutionDate(tPlus3.atStartOfDay().toInstant(ZoneOffset.UTC))
                .debtor(PartyIdentification.builder().name("person").build())
                .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .creditor(PartyIdentification.builder().name("person").build())
                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .build();

        assertFalse(validator.validatePaymentDate(payment,today.atStartOfDay(),4,"test").isSuccess());
        assertTrue(validator.validatePaymentDate(payment,today.atStartOfDay(),2,"test").isSuccess());
        assertTrue(validator.validatePaymentDate(payment,today.atStartOfDay(),3,"test").isSuccess());
        assertFalse(validator.validatePaymentDate(payment,today.atTime(LocalTime.of(16,30)),3,"test").isSuccess());
    }
}