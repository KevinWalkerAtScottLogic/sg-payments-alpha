package gov.scot.payments.payments.ch.app;

import gov.scot.payments.model.Command;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.command.*;
import gov.scot.payments.payments.model.command.CompletePaymentCommand;
import gov.scot.payments.payments.model.command.CompletePaymentSubmissionCommand;
import gov.scot.payments.payments.model.command.FailPaymentSubmissionCommand;
import gov.scot.payments.payments.model.command.ReturnPaymentCommand;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import org.assertj.core.api.Assertions;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

import static gov.scot.payments.payments.ch.app.PaymentTestUtils.*;
import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class PaymentsStateUpdateFuncTest {

    private static final String AMOUNT_VALUE = "100";
    private static final String BATCH_ID = "111";
    private static final String CREDITOR_NAME = "someName";
    private static final String CURRENCY_CODE = "GBP";
    private static final String CLIENT_REFERENCE = "ref1";
    private static final String PRODUCT_REFERENCE = "ref2";
    private static final String ACCOUNT_NUMBER = "12345678";
    private static final String SORT_CODE = "123456";
    private static final String CREATE_PAYMENT_ROLE = "payments/payments:Create";
    private static final String USER = "testUser";

    private static final List<PaymentMethod> ALLOWED_METHODS = List.of(PaymentMethod.Bacs);
    private static final Money AMOUNT = Money.of(new BigDecimal(AMOUNT_VALUE), CURRENCY_CODE);
    private static final CompositeReference PRODUCT = new CompositeReference(CLIENT_REFERENCE, PRODUCT_REFERENCE);

    private PaymentValidator validator;
    private PaymentsStateUpdateFunc paymentsStateUpdateFunc;

    @BeforeEach
    public void setUp(){
        validator = mock(PaymentValidator.class);
        doAnswer(i -> i.getArgument(0,Payment.class).toBuilder().validation(Validation.builder().build()).status(PaymentStatus.Valid).build()).when(validator).validate(any());
        paymentsStateUpdateFunc = new PaymentsStateUpdateFunc(validator);
    }

    @Test
    @DisplayName("Payments state update function creates payment from CreatePaymentRequest")
    void testPaymentCreatedFromRegisterPaymentRequest() {

        Payment payment = getValidPayment(PaymentStatus.Registered);

        Set<Role> roles = HashSet.of(Role.parse(CREATE_PAYMENT_ROLE));

        RegisterPaymentCommand command = RegisterPaymentCommand.builder()
                .payment(payment)
                .roles(roles)
                .build();

        var result = paymentsStateUpdateFunc.apply(command, PaymentsState.builder().build());

        assertEquals(Payment.class, result.getClass());
        assertNotNull(result.getId());
        assertEquals(AMOUNT, result.getAmount());
        assertEquals(CURRENCY_CODE, result.getAmount().getCurrency().getCurrencyCode());
        assertEquals(ALLOWED_METHODS, result.getAllowedMethods());
        assertEquals(PRODUCT, result.getProduct());
        assertEquals(CURRENCY_CODE, result.getCreditorAccount().getCurrency());
        assertThat(result.getCreditorAccount(),instanceOf(UKBankAccount.class));
        assertEquals(CREDITOR_NAME, result.getCreditor().getName());
        assertEquals(USER, result.getCreatedBy());

    }

    @Test
    @DisplayName("Payments state update function throws request when current state is not null")
    void testExceptionThrownWhenStateNotNullOnRegister() {

        PaymentsState state = PaymentsState.builder()
                .current(
                        Payment.builder()
                        .createdBy(USER)
                        .allowedMethods(ALLOWED_METHODS)
                        .amount(AMOUNT)
                        .product(PRODUCT)
                        .latestExecutionDate(Instant.now())
                               .debtor(PartyIdentification.builder().name("person").build())
                               .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                .creditor(PartyIdentification.builder().name("person").build())
                                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                        .build())
                .build();

        Set<Role> roles = HashSet.of(Role.parse(CREATE_PAYMENT_ROLE));

        Payment payment = getValidPayment(PaymentStatus.Registered);

        RegisterPaymentCommand command = RegisterPaymentCommand.builder()
                .payment(payment)
                .roles(roles)
                .build();

        assertThrows(IllegalStateException.class, () -> paymentsStateUpdateFunc
                .apply(command, state));
    }

    @Test
    void testRelease(){
        PaymentsState state = PaymentsState.builder()
                .build();
        Command command = ReleasePaymentCommand.builder().paymentId(UUID.randomUUID()).build();
        assertThrows(IllegalStateException.class, () -> paymentsStateUpdateFunc
                .apply(command, state));
        PaymentsState state1 = PaymentsState.builder()
                .current(
                        Payment.builder()
                                .status(PaymentStatus.ReadyForSubmission)
                                .createdBy(USER)
                                .allowedMethods(ALLOWED_METHODS)
                                .amount(AMOUNT)
                                .product(PRODUCT)
                                .latestExecutionDate(Instant.now())
                               .debtor(PartyIdentification.builder().name("person").build())
                               .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                .creditor(PartyIdentification.builder().name("person").build())
                                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                .build())
                .build();
        assertThrows(IllegalStateException.class, () -> paymentsStateUpdateFunc
                .apply(command, state1));

        PaymentsState state2 = PaymentsState.builder()
                .current(
                        Payment.builder()
                                .status(PaymentStatus.Registered)
                                .createdBy(USER)
                                .allowedMethods(ALLOWED_METHODS)
                                .amount(AMOUNT)
                                .product(PRODUCT)
                                .latestExecutionDate(Instant.now())
                                .debtor(PartyIdentification.builder().name("person").build())
                                .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                .creditor(PartyIdentification.builder().name("person").build())
                                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                .build())
                .build();
        doAnswer(i -> i.getArgument(0,Payment.class).toBuilder().status(PaymentStatus.Valid).build()).when(validator).validate(any());
        var updated = paymentsStateUpdateFunc
                .apply(command, state2);
        assertEquals(PaymentStatus.Valid,updated.getStatus());

        doAnswer(i -> i.getArgument(0,Payment.class).toBuilder().status(PaymentStatus.Invalid).build()).when(validator).validate(any());
        updated = paymentsStateUpdateFunc
                .apply(command, state2);
        assertEquals(PaymentStatus.Invalid,updated.getStatus());
    }

    @Test
    void testReject(){
        PaymentsState state = PaymentsState.builder()
                .build();
        Command command = RejectPaymentCommand.builder().paymentId(UUID.randomUUID()).reason("a").build();
        assertThrows(IllegalStateException.class, () -> paymentsStateUpdateFunc
                .apply(command, state));
        PaymentsState state1 = PaymentsState.builder()
                .current(
                        Payment.builder()
                                .status(PaymentStatus.ReadyForSubmission)
                                .createdBy(USER)
                                .allowedMethods(ALLOWED_METHODS)
                                .amount(AMOUNT)
                                .product(PRODUCT)
                                .latestExecutionDate(Instant.now())
                                .debtor(PartyIdentification.builder().name("person").build())
                                .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                .creditor(PartyIdentification.builder().name("person").build())
                                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                .build())
                .build();
        assertThrows(IllegalStateException.class, () -> paymentsStateUpdateFunc
                .apply(command, state1));

        PaymentsState state2 = PaymentsState.builder()
                .current(
                        Payment.builder()
                                .status(PaymentStatus.Valid)
                                .createdBy(USER)
                                .allowedMethods(ALLOWED_METHODS)
                                .amount(AMOUNT)
                                .product(PRODUCT)
                                .latestExecutionDate(Instant.now())
                                .debtor(PartyIdentification.builder().name("person").build())
                                .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                .creditor(PartyIdentification.builder().name("person").build())
                                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                .build())
                .build();
        var updated = paymentsStateUpdateFunc
                .apply(command, state2);
        assertEquals(PaymentStatus.Rejected,updated.getStatus());

    }

    @Test
    void testRequiresApproval(){
        PaymentsState state = PaymentsState.builder()
                .build();
        Command command = RequestPaymentApprovalCommand.builder().paymentId(UUID.randomUUID()).reason("a").build();
        assertThrows(IllegalStateException.class, () -> paymentsStateUpdateFunc
                .apply(command, state));
        PaymentsState state1 = PaymentsState.builder()
                .current(
                        Payment.builder()
                                .status(PaymentStatus.ReadyForSubmission)
                                .createdBy(USER)
                                .allowedMethods(ALLOWED_METHODS)
                                .amount(AMOUNT)
                                .product(PRODUCT)
                                .latestExecutionDate(Instant.now())
                                .debtor(PartyIdentification.builder().name("person").build())
                                .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                .creditor(PartyIdentification.builder().name("person").build())
                                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                .build())
                .build();
        assertThrows(IllegalStateException.class, () -> paymentsStateUpdateFunc
                .apply(command, state1));

        PaymentsState state2 = PaymentsState.builder()
                .current(
                        Payment.builder()
                                .status(PaymentStatus.Valid)
                                .createdBy(USER)
                                .allowedMethods(ALLOWED_METHODS)
                                .amount(AMOUNT)
                                .product(PRODUCT)
                                .latestExecutionDate(Instant.now())
                                .debtor(PartyIdentification.builder().name("person").build())
                                .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                .creditor(PartyIdentification.builder().name("person").build())
                                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                .build())
                .build();
        var updated = paymentsStateUpdateFunc
                .apply(command, state2);
        assertEquals(PaymentStatus.ApprovalRequired,updated.getStatus());

    }

    @Test
    void testSubmit(){
        PaymentsState state = PaymentsState.builder()
                .build();
        Command command = SubmitPaymentCommand.builder().paymentId(UUID.randomUUID()).build();
        assertThrows(IllegalStateException.class, () -> paymentsStateUpdateFunc
                .apply(command, state));
        PaymentsState state1 = PaymentsState.builder()
                .current(
                        Payment.builder()
                                .status(PaymentStatus.ReadyForSubmission)
                                .createdBy(USER)
                                .allowedMethods(ALLOWED_METHODS)
                                .amount(AMOUNT)
                                .product(PRODUCT)
                                .latestExecutionDate(Instant.now())
                               .debtor(PartyIdentification.builder().name("person").build())
                               .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                .creditor(PartyIdentification.builder().name("person").build())
                                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                .build())
                .build();
        assertThrows(IllegalStateException.class, () -> paymentsStateUpdateFunc
                .apply(command, state1));

        PaymentsState state2 = PaymentsState.builder()
                .current(
                        Payment.builder()
                                .status(PaymentStatus.Valid)
                                .createdBy(USER)
                                .allowedMethods(ALLOWED_METHODS)
                                .amount(AMOUNT)
                                .product(PRODUCT)
                                .latestExecutionDate(Instant.now())
                               .debtor(PartyIdentification.builder().name("person").build())
                               .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                .creditor(PartyIdentification.builder().name("person").build())
                                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                                .build())
                .build();
        var updated = paymentsStateUpdateFunc
                .apply(command, state2);
        assertEquals(PaymentStatus.ReadyForSubmission,updated.getStatus());

    }

    @Test
    @DisplayName("Given ApprovePaymentCommand with Payment in ApprovalRequired status then Payment is moved to ReadyForSubmission")
    void givenApprovePaymentCommandWithPaymentInApprovalRequiredStatusThenPaymentIsMovedToReadyForSubmission() {

        Payment payment = getValidPayment(PaymentStatus.ApprovalRequired)
                .toBuilder()
                .approval(Approval.builder().requiredBy("r").build())
                .build();

        var command = ApprovePaymentCommand.builder()
                .paymentId(payment.getId())
                .user(USER+"1")
                .build();

        var state = PaymentsState.builder()
                .current(payment)
                .build();

        var result = paymentsStateUpdateFunc.apply(command, state);

        assertEquals(Payment.class, result.getClass());
        assertNotNull(result.getId());
        checkExpectedPaymentValues(result, PaymentStatus.ReadyForSubmission);
        Assertions.assertThat(result.getApproval())
                .isEqualToIgnoringGivenFields(Approval.builder()
                        .status(Approval.Status.Approved)
                        .user(USER+"1")
                        .requiredBy("r")
                        .build(), "timestamp");
    }

    @Test
    @DisplayName("Given ApprovePaymentCommand with no matching Payment then an exception is thrown")
    void givenApprovePaymentCommandWithNoMatchingPaymentThenAnExceptionIsThrown() {

        var paymentId = UUID.randomUUID();

        var command = ApprovePaymentCommand.builder()
                .paymentId(paymentId)
                .user(USER)
                .build();

        var state = PaymentsState.builder().build();

        var e = assertThrows(IllegalStateException.class,
                () -> paymentsStateUpdateFunc.apply(command, state));
        assertThat(e.getMessage(), is("Command \"ApprovePaymentCommand\" cannot be carried out as payment "+ paymentId +" does not exist"));
    }

    @Test
    @DisplayName("Given ApprovePaymentCommand with Payment in wrong status then an exception is thrown")
    void givenApprovePaymentCommandWithPaymentInWrongStatusThenAnExceptionIsThrown() {

        Payment payment = getValidPayment(PaymentStatus.Paid);

        var command = ApprovePaymentCommand.builder()
                .paymentId(payment.getId())
                .user(USER+"1")
                .build();

        var state = PaymentsState.builder()
                .current(payment)
                .build();

        var e = assertThrows(IllegalStateException.class,
                () -> paymentsStateUpdateFunc.apply(command, state));

        assertThat(e.getMessage(), is(
                format("Command \"ApprovePaymentCommand\" cannot be carried out as payment %s has status %s instead of %s",
                        payment.getId(), PaymentStatus.Paid, PaymentStatus.ApprovalRequired)));
    }

    @Test
    @DisplayName("Given RejectPaymentCommand with Payment in ApprovalRequired status then Payment is moved to Rejected")
    void givenRejectPaymentCommandWithPaymentInApprovalRequiredStatusThenPaymentIsMovedToRejected() {

        Payment payment = getValidPayment(PaymentStatus.ApprovalRequired)
                .toBuilder()
                .approval(Approval.builder().requiredBy("r").build())
                .build();

        var command = RejectPaymentApprovalCommand.builder()
                .paymentId(payment.getId())
                .user(USER+"1")
                .reason(REASON)
                .build();

        var state = PaymentsState.builder()
                .current(payment)
                .build();

        var result = paymentsStateUpdateFunc.apply(command, state);

        assertEquals(Payment.class, result.getClass());
        assertNotNull(result.getId());
        checkExpectedPaymentValues(result, PaymentStatus.Rejected);
        Assertions.assertThat(result.getApproval())
                .isEqualToIgnoringGivenFields(Approval.builder()
                        .status(Approval.Status.Rejected)
                        .user(USER+"1")
                        .reason(REASON)
                        .requiredBy("r")
                        .build(), "timestamp");
    }

    @Test
    @DisplayName("Given RejectPaymentCommand with no matching Payment then an exception is thrown")
    void givenRejectPaymentCommandWithNoMatchingPaymentThenAnExceptionIsThrown() {

        var paymentId = UUID.randomUUID();

        var command = RejectPaymentApprovalCommand.builder()
                .paymentId(paymentId)
                .user(USER)
                .reason(REASON)
                .build();

        var state = PaymentsState.builder().build();

        var e = assertThrows(IllegalStateException.class,
                () -> paymentsStateUpdateFunc.apply(command, state));
        assertThat(e.getMessage(), is("Command \"RejectPaymentApprovalCommand\" cannot be carried out as payment "+ paymentId +" does not exist"));
    }

    @Test
    @DisplayName("Given RejectPaymentCommand with Payment in wrong status then an exception is thrown")
    void givenRejectPaymentCommandWithPaymentInWrongStatusThenAnExceptionIsThrown() {

        Payment payment = getValidPayment(PaymentStatus.Paid);

        var command = RejectPaymentCommand.builder()
                .paymentId(payment.getId())
                .reason(REASON)
                .build();

        var state = PaymentsState.builder()
                .current(payment)
                .build();

        var e = assertThrows(IllegalStateException.class,
                () -> paymentsStateUpdateFunc.apply(command, state));

        assertThat(e.getMessage(), is(
                format("Command \"RejectPaymentCommand\" cannot be carried out as payment %s has status %s instead of %s",
                        payment.getId(), PaymentStatus.Paid, PaymentStatus.Valid)));
    }

    @Test
    @DisplayName("Given FailPaymentSubmissionCommand with Payment in ReadyForSubmission status then Payment is moved to SubmissionFailed")
    void givenFailPaymentSubmissionCommandWithPaymentInReadyForSubmissionStatusThenPaymentIsMovedToSubmissionFailed() {

        Payment payment = getValidPayment(PaymentStatus.ReadyForSubmission).toBuilder()
                .build();

        var command = FailPaymentSubmissionCommand.builder()
                .paymentId(payment.getId())
                .failureDetails(List.of())
                .build();

        var state = PaymentsState.builder()
                .current(payment)
                .build();

        var result = paymentsStateUpdateFunc.apply(command, state);

        assertEquals(Payment.class, result.getClass());
        assertNotNull(result.getId());
        checkExpectedPaymentValues(result, PaymentStatus.SubmissionFailed);
        Assertions.assertThat(result.getSubmissions())
                .usingElementComparatorOnFields("status")
                .containsExactly(Submission.builder()
                        .status(Submission.Status.Failure)
                        .build());
    }

    @Test
    @DisplayName("Given FailPaymentSubmissionCommand with no matching Payment then an exception is thrown")
    void givenFailPaymentSubmissionCommandWithNoMatchingPaymentThenAnExceptionIsThrown() {

        var paymentId = UUID.randomUUID();

        var command = FailPaymentSubmissionCommand.builder()
                .paymentId(paymentId)
                .failureDetails(List.of())
                .build();

        var state = PaymentsState.builder()
                .build();

        var e = assertThrows(IllegalStateException.class,
                () -> paymentsStateUpdateFunc.apply(command, state));
        assertThat(e.getMessage(), is("Command \"FailPaymentSubmissionCommand\" cannot be carried out as payment "+ paymentId +" does not exist"));
    }

    @Test
    @DisplayName("Given FailPaymentSubmissionCommand with Payment in wrong status then an exception is thrown")
    void givenFailPaymentSubmissionCommandWithPaymentInWrongStatusThenAnExceptionIsThrown() {

        Payment payment = getValidPayment(PaymentStatus.Paid).toBuilder()
                .build();

        var command = FailPaymentSubmissionCommand.builder()
                .paymentId(payment.getId())
                .failureDetails(List.of())
                .build();

        var state = PaymentsState.builder()
                .current(payment)
                .build();

        var e = assertThrows(IllegalStateException.class,
                () -> paymentsStateUpdateFunc.apply(command, state));

        assertThat(e.getMessage(), is(
                format("Command \"FailPaymentSubmissionCommand\" cannot be carried out as payment %s has status %s instead of %s",
                        payment.getId(), PaymentStatus.Paid, PaymentStatus.ReadyForSubmission)));
    }

    @Test
    @DisplayName("Given CompletePaymentSubmissionCommand with Payment in ReadyForSubmission status then Payment is moved to Submitted")
    void givenCompletePaymentSubmissionCommandWithPaymentInReadyForSubmissionStatusThenPaymentIsMovedToSubmissionFailed() {

        Payment payment = getValidPayment(PaymentStatus.ReadyForSubmission).toBuilder()
                .build();

        var command = CompletePaymentSubmissionCommand.builder()
                .paymentId(payment.getId())
                .method(PaymentMethod.Bacs)
                .psp("psp")
                .build();

        var state = PaymentsState.builder()
                .current(payment)
                .build();

        var result = paymentsStateUpdateFunc.apply(command, state);

        assertEquals(Payment.class, result.getClass());
        assertNotNull(result.getId());
        checkExpectedPaymentValues(result, PaymentStatus.Submitted);
        Assertions.assertThat(result.getSubmissions())
                .usingElementComparatorOnFields("status","paymentMethod")
                .containsExactly(Submission.builder()
                    .status(Submission.Status.Success)
                    .paymentMethod(PaymentMethod.Bacs)
                    .build());

    }

    @Test
    @DisplayName("Given CompletePaymentSubmissionCommand with no matching Payment then an exception is thrown")
    void givenCompletePaymentSubmissionCommandWithNoMatchingPaymentThenAnExceptionIsThrown() {

        var paymentId = UUID.randomUUID();

        var command = CompletePaymentSubmissionCommand.builder()
                .paymentId(paymentId)
                .method(PaymentMethod.Bacs)
                .psp("psp")
                .build();

        var state = PaymentsState.builder()
                .build();

        var e = assertThrows(IllegalStateException.class,
                () -> paymentsStateUpdateFunc.apply(command, state));
        assertThat(e.getMessage(), is("Command \"CompletePaymentSubmissionCommand\" cannot be carried out as payment "+ paymentId +" does not exist"));
    }

    @Test
    @DisplayName("Given CompletePaymentSubmissionCommand with Payment in wrong status then an exception is thrown")
    void givenCompletePaymentSubmissionCommandWithPaymentInWrongStatusThenAnExceptionIsThrown() {

        Payment payment = getValidPayment(PaymentStatus.Paid).toBuilder()
                .build();

        var command = CompletePaymentSubmissionCommand.builder()
                .paymentId(payment.getId())
                .method(PaymentMethod.Bacs)
                .psp("psp")
                .build();

        var state = PaymentsState.builder()
                .current(payment)
                .build();

        var e = assertThrows(IllegalStateException.class,
                () -> paymentsStateUpdateFunc.apply(command, state));

        assertThat(e.getMessage(), is(
                format("Command \"CompletePaymentSubmissionCommand\" cannot be carried out as payment %s has status %s instead of %s",
                        payment.getId(), PaymentStatus.Paid, PaymentStatus.ReadyForSubmission)));
    }

    @Test
    @DisplayName("Given ReturnPaymentCommand with Payment in Submitted status then Payment is moved to Returned")
    void givenReturnPaymentCommandWithPaymentInSubmittedStatusThenPaymentIsMovedToReturned() {

        Payment payment = getValidPayment(PaymentStatus.Submitted);

        var command = ReturnPaymentCommand.builder()
                .paymentId(payment.getId())
                .method(PaymentMethod.Bacs)
                .psp("psp")
                .message("m")
                .code("c")
                .amount(Money.parse("GBP 1"))
                .build();

        var state = PaymentsState.builder()
                .current(payment)
                .build();

        var result = paymentsStateUpdateFunc.apply(command, state);

        assertEquals(Payment.class, result.getClass());
        assertNotNull(result.getId());
        checkExpectedPaymentValues(result, PaymentStatus.Returned);
//        assertThat(result.getReturnEvent(), is(
//                Return.builder()
//                        .amount(AMOUNT)
//                        .build())
//        );
    }

    @Test
    @DisplayName("Given ReturnPaymentCommand with no matching Payment then an exception is thrown")
    void givenReturnPaymentCommandWithNoMatchingPaymentThenAnExceptionIsThrown() {

        var paymentId = UUID.randomUUID();

        var command = ReturnPaymentCommand.builder()
                .paymentId(paymentId)
                .method(PaymentMethod.Bacs)
                .psp("psp")
                .message("m")
                .code("c")
                .amount(Money.parse("GBP 1"))
                .build();

        var state = PaymentsState.builder()
                .build();

        var e = assertThrows(IllegalStateException.class,
                () -> paymentsStateUpdateFunc.apply(command, state));
        assertThat(e.getMessage(), is("Command \"ReturnPaymentCommand\" cannot be carried out as payment "+ paymentId +" does not exist"));
    }

    @Test
    @DisplayName("Given ReturnPaymentCommand with Payment in wrong status then an exception is thrown")
    void givenReturnPaymentCommandWithPaymentInWrongStatusThenAnExceptionIsThrown() {

        Payment payment = getValidPayment(PaymentStatus.Paid).toBuilder()
                .build();

        var command = ReturnPaymentCommand.builder()
                .paymentId(payment.getId())
                .method(PaymentMethod.Bacs)
                .psp("psp")
                .message("m")
                .code("c")
                .amount(Money.parse("GBP 1"))
                .build();

        var state = PaymentsState.builder()
                .current(payment)
                .build();

        var e = assertThrows(IllegalStateException.class,
                () -> paymentsStateUpdateFunc.apply(command, state));

        assertThat(e.getMessage(), is(
                format("Command \"ReturnPaymentCommand\" cannot be carried out as payment %s has status %s instead of %s",
                        payment.getId(), PaymentStatus.Paid, PaymentStatus.Submitted)));
    }

    @Test
    @DisplayName("Given CompletePaymentCommand with Payment in Submitted status then Payment is moved to Paid")
    void givenCompletePaymentCommandWithPaymentInSubmittedStatusThenPaymentIsMovedToPaid() {

        Payment payment = getValidPayment(PaymentStatus.Submitted);

        var command = CompletePaymentCommand.builder()
                .paymentId(payment.getId())
                .method(PaymentMethod.Bacs)
                .psp("psp")
                .build();

        var state = PaymentsState.builder()
                .current(payment)
                .build();

        var result = paymentsStateUpdateFunc.apply(command, state);

        assertEquals(Payment.class, result.getClass());
        assertNotNull(result.getId());
        checkExpectedPaymentValues(result, PaymentStatus.Paid);
//        assertThat(result.getSettlementEvent(), is(
//                Settlement.builder()
//                        .amount(AMOUNT)
//                        .build())
//        );
    }

    @Test
    @DisplayName("Given CompletePaymentCommand with no matching Payment then an exception is thrown")
    void givenCompletePaymentCommandWithNoMatchingPaymentThenAnExceptionIsThrown() {

        var paymentId = UUID.randomUUID();

        var command = CompletePaymentCommand.builder()
                .paymentId(paymentId)
                .method(PaymentMethod.Bacs)
                .psp("psp")
                .build();

        var state = PaymentsState.builder()
                .build();

        var e = assertThrows(IllegalStateException.class,
                () -> paymentsStateUpdateFunc.apply(command, state));
        assertThat(e.getMessage(), is("Command \"CompletePaymentCommand\" cannot be carried out as payment "+ paymentId +" does not exist"));
    }

    @Test
    @DisplayName("Given CompletePaymentCommand with Payment in wrong status then an exception is thrown")
    void givenCompletePaymentCommandWithPaymentInWrongStatusThenAnExceptionIsThrown() {

        Payment payment = getValidPayment(PaymentStatus.Paid).toBuilder()
                .build();

        var command = CompletePaymentCommand.builder()
                .paymentId(payment.getId())
                .method(PaymentMethod.Bacs)
                .psp("psp")
                .build();

        var state = PaymentsState.builder()
                .current(payment)
                .build();

        var e = assertThrows(IllegalStateException.class,
                () -> paymentsStateUpdateFunc.apply(command, state));

        assertThat(e.getMessage(), is(
                format("Command \"CompletePaymentCommand\" cannot be carried out as payment %s has status %s instead of %s",
                        payment.getId(), PaymentStatus.Paid, PaymentStatus.Submitted)));
    }


    private Payment getValidPayment(PaymentStatus status) {

        return Payment.builder()
                .batchId(BATCH_ID)
                .allowedMethods(ALLOWED_METHODS)
                .status(status)
                .amount(AMOUNT)
                .latestExecutionDate(Instant.now())
                .product(new CompositeReference(
                        CLIENT_REFERENCE, PRODUCT_REFERENCE))
                .creditorAccount(
                        UKBankAccount.builder()
                                .name("acct")
                                .currency(CURRENCY_CODE)
                                .sortCode(new SortCode(SORT_CODE))
                                .accountNumber(new UKAccountNumber(ACCOUNT_NUMBER))
                                .build())
                .creditor(
                        PartyIdentification.builder()
                                .name(CREDITOR_NAME)
                                .build())
                .debtorAccount(
                        UKBankAccount.builder()
                                .name("acct")
                                .currency(CURRENCY_CODE)
                                .sortCode(new SortCode(SORT_CODE))
                                .accountNumber(new UKAccountNumber(ACCOUNT_NUMBER))
                                .build())
                .debtor(
                        PartyIdentification.builder()
                                .name(CREDITOR_NAME)
                                .build())
                .createdBy(USER)
                .build();
    }

}
