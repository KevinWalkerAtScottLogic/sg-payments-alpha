package gov.scot.payments.payments.ch.app;

import gov.scot.payments.model.CommandFailureInfo;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.GenericErrorEvent;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.command.*;
import gov.scot.payments.payments.model.event.*;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import org.javamoney.moneta.Money;
import gov.scot.payments.model.Command;
import gov.scot.payments.payments.model.aggregate.PaymentStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Map;
import java.util.stream.Stream;

import static gov.scot.payments.payments.ch.app.PaymentTestUtils.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class PaymentsEventGeneratorTest {

    private static final String AMOUNT_VALUE = "100";
    private static final String BATCH_ID = "111";
    private static final String CREDITOR_NAME = "someName";
    private static final String CURRENCY_CODE = "GBP";
    private static final String CLIENT_REFERENCE = "ref1";
    private static final String PRODUCT_REFERENCE = "ref2";
    private static final String ACCOUNT_NUMBER = "12345678";
    private static final String SORT_CODE = "123456";
    private static final String CREATE_PAYMENT_ROLE = "payments/payments:Create";
    private static final String USER = "testUser";
    private static final Set<Role> ROLES = HashSet.of(Role.parse(CREATE_PAYMENT_ROLE));

    private static final List<PaymentMethod> ALLOWED_METHODS = List.of(PaymentMethod.Sepa_DC);
    private static final Money AMOUNT = Money.of(new BigDecimal(AMOUNT_VALUE), CURRENCY_CODE);
    private static final CompositeReference PRODUCT = new CompositeReference(CLIENT_REFERENCE, PRODUCT_REFERENCE);

    private PaymentsEventGenerator paymentsEventGenerator = new PaymentsEventGenerator();

    @Test
    @DisplayName("Payments event generator emits an event when RegisterPaymentCommand is received")
    void testEventIsEmittedWhenRegisterPaymentCommandIsReceived() {

        UKBankAccount creditorAccount = UKBankAccount.builder()
                .currency(CURRENCY_CODE)
                .sortCode(new SortCode(SORT_CODE))
                .accountNumber(new UKAccountNumber(ACCOUNT_NUMBER))
                .name("name")
                .build();

        PartyIdentification creditor = PartyIdentification.builder()
                .name(CREDITOR_NAME)
                .build();

        RegisterPaymentCommand command = getValidPaymentCommand();

        var currentPayment = getValidPayment();

        var result = paymentsEventGenerator.apply(command, PaymentsState.builder()
                .current(currentPayment)
                .build());

        assertEquals(PaymentRegisteredEvent.class, result.get().getClass());

        PaymentRegisteredEvent event = (PaymentRegisteredEvent)result.get();
        Payment payment = event.getCarriedState();

        assertEquals(currentPayment, payment);
        assertEquals(AMOUNT, payment.getAmount());
        assertEquals(CURRENCY_CODE, payment.getAmount().getCurrency().getCurrencyCode());
        assertEquals(ALLOWED_METHODS, payment.getAllowedMethods());
        assertEquals(PRODUCT, payment.getProduct());
        assertEquals(creditorAccount, payment.getCreditorAccount());
        assertEquals(creditor, payment.getCreditor());
        assertEquals(USER, payment.getCreatedBy());
    }

    @Test
    void testReject() {
        var currentPayment = getValidPayment();
        RejectPaymentCommand command = RejectPaymentCommand.builder().paymentId(currentPayment.getId()).reason("a").build();

        var result = paymentsEventGenerator.apply(command, PaymentsState.builder()
                .current(currentPayment)
                .build());

        assertEquals(PaymentRejectedEvent.class, result.get().getClass());

        PaymentRejectedEvent event = (PaymentRejectedEvent)result.get();
        Payment payment = event.getCarriedState();

        assertEquals(currentPayment, payment);
        assertEquals(AMOUNT, payment.getAmount());
        assertEquals(CURRENCY_CODE, payment.getAmount().getCurrency().getCurrencyCode());
        assertEquals(ALLOWED_METHODS, payment.getAllowedMethods());
        assertEquals(PRODUCT, payment.getProduct());
        assertEquals(USER, payment.getCreatedBy());
    }

    @Test
    void testRequiresApproval() {
        var currentPayment = getValidPayment();
        RequestPaymentApprovalCommand command = RequestPaymentApprovalCommand.builder().paymentId(currentPayment.getId()).reason("a").build();

        var result = paymentsEventGenerator.apply(command, PaymentsState.builder()
                .current(currentPayment)
                .build());

        assertEquals(PaymentApprovalRequestedEvent.class, result.get().getClass());

        PaymentApprovalRequestedEvent event = (PaymentApprovalRequestedEvent)result.get();
        Payment payment = event.getCarriedState();

        assertEquals(currentPayment, payment);
        assertEquals(AMOUNT, payment.getAmount());
        assertEquals(CURRENCY_CODE, payment.getAmount().getCurrency().getCurrencyCode());
        assertEquals(ALLOWED_METHODS, payment.getAllowedMethods());
        assertEquals(PRODUCT, payment.getProduct());
        assertEquals(USER, payment.getCreatedBy());
    }

    @Test
    void testSubmit() {
        var currentPayment = getValidPayment();
        SubmitPaymentCommand command = SubmitPaymentCommand.builder().paymentId(currentPayment.getId()).build();

        var result = paymentsEventGenerator.apply(command, PaymentsState.builder()
                .current(currentPayment)
                .build());

        assertEquals(PaymentReadyForSubmissionEvent.class, result.get().getClass());

        PaymentReadyForSubmissionEvent event = (PaymentReadyForSubmissionEvent)result.get();
        Payment payment = event.getCarriedState();

        assertEquals(currentPayment, payment);
        assertEquals(AMOUNT, payment.getAmount());
        assertEquals(CURRENCY_CODE, payment.getAmount().getCurrency().getCurrencyCode());
        assertEquals(ALLOWED_METHODS, payment.getAllowedMethods());
        assertEquals(PRODUCT, payment.getProduct());
        assertEquals(USER, payment.getCreatedBy());
    }

    @Test
    void testReleaseInvalid() {
        var currentPayment = getValidPayment().toBuilder().status(PaymentStatus.Invalid).build();
        ReleasePaymentCommand command = ReleasePaymentCommand.builder().paymentId(currentPayment.getId()).build();

        var result = paymentsEventGenerator.apply(command, PaymentsState.builder()
                .current(currentPayment)
                .build());

        assertEquals(PaymentInvalidEvent.class, result.get().getClass());

        PaymentInvalidEvent event = (PaymentInvalidEvent)result.get();
        Payment payment = event.getCarriedState();

        assertEquals(currentPayment, payment);
        assertEquals(AMOUNT, payment.getAmount());
        assertEquals(CURRENCY_CODE, payment.getAmount().getCurrency().getCurrencyCode());
        assertEquals(ALLOWED_METHODS, payment.getAllowedMethods());
        assertEquals(PRODUCT, payment.getProduct());
        assertEquals(USER, payment.getCreatedBy());
    }

    @Test
    void testReleaseValid() {
        var currentPayment = getValidPayment().toBuilder().status(PaymentStatus.Valid).build();
        ReleasePaymentCommand command = ReleasePaymentCommand.builder().paymentId(currentPayment.getId()).build();

        var result = paymentsEventGenerator.apply(command, PaymentsState.builder()
                .current(currentPayment)
                .build());

        assertEquals(PaymentValidEvent.class, result.get().getClass());

        PaymentValidEvent event = (PaymentValidEvent)result.get();
        Payment payment = event.getCarriedState();

        assertEquals(currentPayment, payment);
        assertEquals(AMOUNT, payment.getAmount());
        assertEquals(CURRENCY_CODE, payment.getAmount().getCurrency().getCurrencyCode());
        assertEquals(ALLOWED_METHODS, payment.getAllowedMethods());
        assertEquals(PRODUCT, payment.getProduct());
        assertEquals(USER, payment.getCreatedBy());
    }

    @Test
    @DisplayName("Payments event generator throws an error upon receiving an invalid command")
    void testErrorIsThrownWhenInvalidCommandIsReceived() {

        RegisterPaymentCommand command = getValidPaymentCommand();

        PaymentsState state = PaymentsState.builder()
                .error(CommandFailureInfo.builder().errorType("").build())
                .build();

        var result = paymentsEventGenerator.apply(command,state).get(0);
        assertThat(result,instanceOf(GenericErrorEvent.class));
    }

    /**
     * Sets up test parameters, to check that when a given command is received then the correct event is emitted.
     *
     * Arguments are:
     *
     * <ol>
     *    <li>The type of the command being applied (only included to make the test name more useful)</li>
     *    <li>The command being applied</li>
     *    <li>The payment status after the command is applied (included in the emitted event)</li>
     *    <li>The expected type of the event emitted</li>
     * <ol/>
     *
     * @return Stream of arguments to feed to {@link #whenCommandIsReceivedThenTheCorrectEventIsEmitted}
     */

    /*
    ApproveCancelPaymentCommand.class,
    CancelPaymentCommand.class,
    CompleteCancelPaymentCommand.class,
    FailCancelPaymentCommand.class,
    RejectCancelPaymentCommand.class,
    */

    static Stream<Arguments> testParameters() {
        return Stream.of(
                arguments(
                        RegisterPaymentCommand.class,
                        RegisterPaymentCommand.builder()
                                .payment(getValidPayment().toBuilder().build())
                                .build(),
                        PaymentStatus.Registered,
                        PaymentRegisteredEvent.class
                ),
                arguments(
                        ReleasePaymentCommand.class,
                        ReleasePaymentCommand.builder()
                                .paymentId(PAYMENT_ID)
                                .build(),
                        PaymentStatus.Valid,
                        PaymentValidEvent.class
                ),
                arguments(
                        RejectPaymentCommand.class,
                        RejectPaymentCommand.builder()
                                .paymentId(PAYMENT_ID)
                                .reason(REASON)
                                .build(),
                        PaymentStatus.Rejected,
                        PaymentRejectedEvent.class
                ),
                arguments(
                        SubmitPaymentCommand.class,
                        SubmitPaymentCommand.builder()
                                .paymentId(PAYMENT_ID)
                                .build(),
                        PaymentStatus.ReadyForSubmission,
                        PaymentReadyForSubmissionEvent.class
                ),
                arguments(
                        RequestPaymentApprovalCommand.class,
                        RequestPaymentApprovalCommand.builder()
                                .paymentId(PAYMENT_ID)
                                .reason(REASON)
                                .build(),
                        PaymentStatus.ApprovalRequired,
                        PaymentApprovalRequestedEvent.class
                ),
                arguments(
                        ApprovePaymentCommand.class,
                        ApprovePaymentCommand.builder()
                                .paymentId(PAYMENT_ID)
                                .user(USER)
                                .build(),
                        PaymentStatus.ReadyForSubmission,
                        PaymentReadyForSubmissionEvent.class
                ),
                arguments(
                        RejectPaymentApprovalCommand.class,
                        RejectPaymentApprovalCommand.builder()
                                .paymentId(PAYMENT_ID)
                                .user(USER)
                                .reason(REASON)
                                .build(),
                        PaymentStatus.Rejected,
                        PaymentRejectedEvent.class
                ),
                arguments(
                        CompletePaymentSubmissionCommand.class,
                        CompletePaymentSubmissionCommand.builder()
                                .paymentId(PAYMENT_ID)
                                .method(PaymentMethod.Bacs)
                                .psp("psp")
                                .build(),
                        PaymentStatus.Submitted,
                        PaymentSubmissionSuccessEvent.class
                ),
                arguments(
                        FailPaymentSubmissionCommand.class,
                        FailPaymentSubmissionCommand.builder()
                                .paymentId(PAYMENT_ID)
                                .failureDetails(List.empty())
                                .build(),
                        PaymentStatus.SubmissionFailed,
                        PaymentSubmissionFailedEvent.class
                ),
                arguments(
                        CompletePaymentCommand.class,
                        CompletePaymentCommand.builder()
                                .paymentId(PAYMENT_ID)
                                .method(PaymentMethod.Bacs)
                                .psp("psp")
                                .build(),
                        PaymentStatus.Paid,
                        PaymentCompleteEvent.class
                ),
                arguments(
                        ReturnPaymentCommand.class,
                        ReturnPaymentCommand.builder()
                                .paymentId(PAYMENT_ID)
                                .method(PaymentMethod.Bacs)
                                .psp("psp")
                                .message("m")
                                .code("c")
                                .amount(Money.parse("GBP 1"))
                                .build(),
                        PaymentStatus.Returned,
                        PaymentReturnedEvent.class
                )
        );
    }

    @ParameterizedTest(name = "When a {0} is received then a {3} is emitted")
    @MethodSource("testParameters")
    @DisplayName("When a command is received then the correct event is emitted")
    void whenCommandIsReceivedThenTheCorrectEventIsEmitted(
            Class<? extends Command> commandClass,
            Command command,
            PaymentStatus paymentStatus,
            Class<? extends BasePaymentEventWithCause> expectedEventClass) {
        var currentPayment = getValidPayment().toBuilder().id(PAYMENT_ID).status(paymentStatus).build();

        var result = paymentsEventGenerator.apply(command, PaymentsState.builder()
                .current(currentPayment)
                .build());

        assertEquals(expectedEventClass, result.get().getClass());
        var event = (BasePaymentEventWithCause) result.get();
        assertThat(event.getCarriedState(), is(currentPayment));
    }

    private RegisterPaymentCommand getValidPaymentCommand() {
        return RegisterPaymentCommand.builder()
            .payment(getValidPayment())
            .roles(ROLES)
            .build();
    }

    private static Payment getValidPayment() {

        return Payment.builder()
            .batchId(BATCH_ID)
            .allowedMethods(ALLOWED_METHODS)
            .amount(AMOUNT)
            .latestExecutionDate(Instant.now())
            .product(new CompositeReference(
                CLIENT_REFERENCE, PRODUCT_REFERENCE))
            .creditorAccount(
                UKBankAccount.builder()
                    .currency(CURRENCY_CODE)
                    .sortCode(new SortCode(SORT_CODE))
                    .accountNumber(new UKAccountNumber(ACCOUNT_NUMBER))
                    .name("name")
                    .build())
            .creditor(
                PartyIdentification.builder()
                    .name(CREDITOR_NAME)
                    .build())
                .debtorAccount(
                        UKBankAccount.builder()
                                .name("acct")
                                .currency(CURRENCY_CODE)
                                .sortCode(new SortCode(SORT_CODE))
                                .accountNumber(new UKAccountNumber(ACCOUNT_NUMBER))
                                .name("name")
                                .build())
                .debtor(
                        PartyIdentification.builder()
                                .name(CREDITOR_NAME)
                                .build())
            .createdBy(USER)
            .build();
    }

}
