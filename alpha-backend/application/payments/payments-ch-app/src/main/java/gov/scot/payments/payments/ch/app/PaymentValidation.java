package gov.scot.payments.payments.ch.app;

import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.aggregate.PaymentStatus;
import gov.scot.payments.payments.model.aggregate.Validation;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Value;

import java.util.function.BiFunction;
import java.util.function.Function;

@Value
public class PaymentValidation {

    @NonNull private Payment payment;
    @NonNull private PaymentValidationResult result;

    public static PaymentValidation of(Payment payment){
        return new PaymentValidation(payment,PaymentValidationResult.success());
    }

    public PaymentValidation and(PaymentValidationResult result){
        return new PaymentValidation(payment, this.result.merge(result));
    }

    public PaymentValidation and(Function<Payment, PaymentValidationResult> validator){
        PaymentValidationResult result = validator.apply(payment);
        return and(result);
    }

    public PaymentValidation andIfSuccess(Function<Payment, PaymentValidation> validator){
        if(!result.isSuccess()){
            return this;
        }
        PaymentValidation other = validator.apply(payment);
        return new PaymentValidation(other.payment,result.merge(other.result));
    }

    public Payment getPaymentWithResult(){
        return payment.toBuilder()
                .status(result.isSuccess() ? PaymentStatus.Valid : PaymentStatus.Invalid)
                .validation(Validation.builder().message(result.getMessage()).build())
                .build();
    }

}
