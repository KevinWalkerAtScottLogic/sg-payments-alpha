package gov.scot.payments.payments.ch.app;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.application.component.commandhandler.stateful.AggregateState;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.model.CommandFailureInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;
import gov.scot.payments.payments.model.aggregate.Payment;

@SuperBuilder
@NoArgsConstructor
@ToString(callSuper = true)
public class PaymentsState extends AggregateState<Payment> {

    @Getter @Nullable private Payment previous;
    @Getter @Nullable private Payment current;

    @JsonCreator
    public PaymentsState(@JsonProperty("message") SerializedMessage message
            , @JsonProperty("error") CommandFailureInfo error
            , @JsonProperty("previous") Payment previous
            , @JsonProperty("current") Payment current
            , @JsonProperty("currentVersion") Long currentVersion) {
        super(message, error, currentVersion);
        this.previous = previous;
        this.current = current;
    }

}
