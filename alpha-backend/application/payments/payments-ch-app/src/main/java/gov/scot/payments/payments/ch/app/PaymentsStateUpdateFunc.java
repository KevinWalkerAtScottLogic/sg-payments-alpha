package gov.scot.payments.payments.ch.app;

import gov.scot.payments.application.component.commandhandler.stateful.StateUpdateFunction;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.HasUser;
import gov.scot.payments.payments.model.aggregate.Approval;
import gov.scot.payments.payments.model.aggregate.Cancellation;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.aggregate.PaymentStatus;
import gov.scot.payments.payments.model.aggregate.Return;
import gov.scot.payments.payments.model.aggregate.Settlement;
import gov.scot.payments.payments.model.aggregate.Submission;
import gov.scot.payments.payments.model.command.*;
import gov.scot.payments.payments.model.command.ApproveCancelPaymentCommand;
import gov.scot.payments.payments.model.command.CancelPaymentCommand;
import gov.scot.payments.payments.model.command.RejectCancelPaymentCommand;
import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.javamoney.moneta.Money;

import java.util.EnumSet;

import static gov.scot.payments.application.component.commandhandler.stateful.AclFilteringStatefulCommandHandlerFunction.aclAndLock;
import static gov.scot.payments.application.component.commandhandler.stateful.AclFilteringStatefulCommandHandlerFunction.lock;
import static java.time.Instant.now;

@Slf4j
@RequiredArgsConstructor
public class PaymentsStateUpdateFunc extends StateUpdateFunction.PatternMatching<PaymentsState, Payment> {

    private final PaymentValidator validator;

    @Override
    protected void handlers(PatternMatcher.Builder2<Command, PaymentsState, Payment> builder) {
        //registration
        builder.match2(RegisterPaymentCommand.class, (c, s) -> handleRegisterPayment(c, s.getCurrent()))
                .match2(ReleasePaymentCommand.class, (c, s) -> handleReleasePayment(c, s.getCurrent()))
                .match2(RejectPaymentCommand.class, (c, s) -> handleRejectPayment(c, s.getCurrent()))

                //approvals
                .match2(RequestPaymentApprovalCommand.class, (c, s) -> handleRequireApprovePayment(c, s.getCurrent()))
                .match2(ApprovePaymentCommand.class, aclAndLock(this::requireDifferentUserForApprovals, this::approvePayment))
                .match2(RejectPaymentApprovalCommand.class, aclAndLock(this::requireDifferentUserForApprovals, this::rejectPaymentApproval))

                //submissions
                .match2(SubmitPaymentCommand.class, (c, s) -> handleSubmitPayment(c, s.getCurrent()))
                .match2(FailPaymentSubmissionCommand.class, (c, s) -> handleFailPaymentSubmission(c, s.getCurrent()))
                .match2(CompletePaymentSubmissionCommand.class, (c, s) -> handleCompletePaymentSubmission(c, s.getCurrent()))
                .match2(ReturnPaymentCommand.class, (c, s) -> handleReturnPayment(c, s.getCurrent()))
                .match2(CompletePaymentCommand.class, (c, s) -> handleCompletePayment(c, s.getCurrent()))

                //cancel post execution
               .match2(FailCancelPaymentCommand.class, (c, s) -> handleFailCancelPayment(c, s.getCurrent()))
               .match2(CompleteCancelPaymentCommand.class, (c, s) -> handleCompleteCancelPayment(c, s.getCurrent()))

                //cancel approvals
               .match2(CancelPaymentCommand.class, lock(this::handleCancelPayment))
               .match2(ApproveCancelPaymentCommand.class, aclAndLock(this::requireDifferentUserForCancelApprovals, (c, s) -> handleApproveCancelPayment(c, s)))
               .match2(RejectCancelPaymentCommand.class, aclAndLock(this::requireDifferentUserForCancelApprovals, (c, s) -> handleRejectCancelPayment(c, s)));
    }

    private boolean requireDifferentUserForApprovals(HasUser command, Payment payment) {
        // Approvals and rejections must be by a different user than the one who created the request
        return !command.getUser().equals(Option.of(payment).map(Payment::getCreatedBy).getOrNull());
    }

    private boolean requireDifferentUserForCancelApprovals(HasUser command, Payment payment) {
        // Approvals and rejections must be by a different user than the one who created the request
        return !command.getUser().equals(Option.of(payment).map(Payment::getCancellation).map(Cancellation::getUser).getOrNull());
    }

    private Payment handleCancelPayment(final CancelPaymentCommand command, final Payment state) {
        final EnumSet<PaymentStatus> validStates = EnumSet.of(PaymentStatus.Registered
                , PaymentStatus.Valid
                , PaymentStatus.ApprovalRequired
                , PaymentStatus.ReadyForSubmission
                , PaymentStatus.Submitted);
        if(state == null || !validStates.contains(state.getStatus()) ){
            throw new IllegalStateException("Payment state must be in one of "+validStates);
        }
        if (state.getCancellation() != null) {
            throw new IllegalStateException(
                    String.format("Command \"%s\" cannot be carried out as payment %s has existing cancellation status",
                            command.getClass().getSimpleName(), state.getKey()));
        }
        return state.toBuilder()
                    .approval(Approval.builder().requiredBy("cancel").build())
                    .cancellation(Cancellation.builder()
                                              .timestamp(command.getTimestamp())
                                              .reason(command.getReason())
                                              .user(command.getUser())
                                              .build())
                    .build();
    }

    private Payment handleApproveCancelPayment(final ApproveCancelPaymentCommand command, final Payment state) {
        if (state == null) {
            throw new IllegalStateException(
                    String.format("Command \"%s\" cannot be carried out as payment %s does not exist",
                            command.getClass().getSimpleName(), command.getPaymentId()));
        }
        var currentPaymentStatus = state.getCancellation();
        if (currentPaymentStatus == null || currentPaymentStatus.getStatus() != Cancellation.Status.Requested) {
            throw new IllegalStateException(
                    String.format("Command \"%s\" cannot be carried out as payment %s has incorrect cancellation status",
                            command.getClass().getSimpleName(), state.getKey()));
        }
        var approval = state.getApproval().toBuilder()
                              .status(Approval.Status.Approved)
                              .user(command.getUser())
                              .timestamp(command.getTimestamp())
                              .build();
        return state.toBuilder()
                    .cancellationApproval(approval)
                    .build();
    }

    private Payment handleRejectCancelPayment(final RejectCancelPaymentCommand command, final Payment state) {
        if (state == null) {
            throw new IllegalStateException(
                    String.format("Command \"%s\" cannot be carried out as payment %s does not exist",
                            command.getClass().getSimpleName(), command.getPaymentId()));
        }
        var currentPaymentStatus = state.getCancellation();
        if (currentPaymentStatus == null || currentPaymentStatus.getStatus() != Cancellation.Status.Requested) {
            throw new IllegalStateException(
                    String.format("Command \"%s\" cannot be carried out as payment %s has no cancellation status",
                            command.getClass().getSimpleName(), state.getKey()));
        }
        var approval = state.getApproval().toBuilder()
                            .status(Approval.Status.Rejected)
                            .user(command.getUser())
                            .timestamp(command.getTimestamp())
                            .build();
        return state.toBuilder()
                    .cancellation(null)
                    .cancellationApproval(approval)
                    .build();
    }

    private Payment handleCompleteCancelPayment(final CompleteCancelPaymentCommand command, final Payment state) {
        if (state == null) {
            throw new IllegalStateException(
                    String.format("Command \"%s\" cannot be carried out as payment %s does not exist",
                            command.getClass().getSimpleName(), command.getPaymentId()));
        }
        if (state.getCancellation() == null) {
            throw new IllegalStateException(
                    String.format("Command \"%s\" cannot be carried out as payment %s has no cancellation status",
                            command.getClass().getSimpleName(), state.getKey()));
        }
        return state.toBuilder()
                    .cancellation(state.getCancellation().toBuilder()
                                       .status(Cancellation.Status.Complete)
                                       .method(command.getMethod())
                                       .psp(command.getPsp())
                                       .pspMetadata(command.getPspMetadata())
                                       .timestamp(command.getTimestamp())
                                       .build())
                    .build();
    }

    private Payment handleFailCancelPayment(final FailCancelPaymentCommand command, final Payment state) {
        if (state == null) {
            throw new IllegalStateException(
                    String.format("Command \"%s\" cannot be carried out as payment %s does not exist",
                            command.getClass().getSimpleName(), command.getPaymentId()));
        }
        if (state.getCancellation() == null) {
            throw new IllegalStateException(
                    String.format("Command \"%s\" cannot be carried out as payment %s has no cancellation status",
                            command.getClass().getSimpleName(), state.getKey()));
        }
        return state.toBuilder()
                    .cancellation(state.getCancellation()
                                       .toBuilder()
                                       .status(Cancellation.Status.Failed)
                                       .method(command.getMethod())
                                       .psp(command.getPsp())
                                       .pspMetadata(command.getPspMetadata())
                                       .timestamp(command.getTimestamp())
                                       .message(command.getMessage())
                                       .build())
                    .build();
    }

    private Payment handleRegisterPayment(RegisterPaymentCommand command, Payment state) {
        if(state != null){
            throw new IllegalStateException("Payment already exists " + command.getPayment().getId());
        }
        return command.getPayment().toBuilder()
                .status(PaymentStatus.Registered)
                .build();
    }

    private Payment handleReleasePayment(ReleasePaymentCommand c, Payment current) {
        checkPaymentState(current, PaymentStatus.Registered, c);
        return validator.validate(current);
    }

    //rejected because of verification failure not approval
    private Payment handleRejectPayment(RejectPaymentCommand c, Payment current) {
        checkPaymentState(current, PaymentStatus.Valid, c);
        return current.toBuilder()
                .status(PaymentStatus.Rejected)
                .approval(Approval.builder()
                        .requiredBy(c.getReason())
                        .reason(c.getReason())
                        .status(Approval.Status.Rejected)
                        .build())
                .build();
    }

    private Payment handleRequireApprovePayment(RequestPaymentApprovalCommand c, Payment current) {
        checkPaymentState(current, PaymentStatus.Valid, c);
        return current.toBuilder()
                      .status(PaymentStatus.ApprovalRequired)
                      .approval(Approval.builder()
                              .requiredBy(c.getReason())
                              .build())
                      .build();
    }

    private Payment approvePayment(ApprovePaymentCommand command, Payment payment) {
        checkPaymentState(payment, PaymentStatus.ApprovalRequired, command);
        var approval = payment.getApproval().toBuilder()
                .status(Approval.Status.Approved)
                .user(command.getUser())
                .timestamp(command.getTimestamp())
                .build();

        //we need to revalidate since it may have been sitting in approval for a while
        var reValidatedPayment = validator.validate(payment);
        if(reValidatedPayment.getStatus() == PaymentStatus.Valid){
            return payment.toBuilder()
                          .status(PaymentStatus.ReadyForSubmission)
                          .approval(approval)
                          .build();
        } else {
            return payment.toBuilder()
                          .status(PaymentStatus.Invalid)
                          .approval(approval)
                          .validation(reValidatedPayment.getValidation())
                          .build();
        }
    }

    private Payment rejectPaymentApproval(RejectPaymentApprovalCommand command, Payment payment) {
        checkPaymentState(payment, PaymentStatus.ApprovalRequired, command);
        var approval = payment.getApproval().toBuilder()
                .status(Approval.Status.Rejected)
                .user(command.getUser())
                .timestamp(command.getTimestamp())
                .reason(command.getReason())
                .build();
        return payment.toBuilder()
                .status(PaymentStatus.Rejected)
                .approval(approval)
                .build();
    }

    private Payment handleSubmitPayment(SubmitPaymentCommand c, Payment current) {
        if(current == null || !EnumSet.of(PaymentStatus.Valid).contains(current.getStatus()) ){
            throw new IllegalStateException("Payment must be in Valid state");
        }
        return current.toBuilder()
                      .status(PaymentStatus.ReadyForSubmission)
                      .build();
    }

    private Payment handleFailPaymentSubmission(FailPaymentSubmissionCommand command, Payment payment) {
        checkPaymentState(payment, PaymentStatus.ReadyForSubmission, command);
        List<Submission> submissions = command.getFailureDetails()
                .map(fd -> fd.toSubmission(command.getMethod()));
        submissions = submissions.isEmpty() ? List.of(Submission.builder()
                .status(Submission.Status.Failure)
                .paymentMethod(command.getMethod())
                .timestamp(command.getTimestamp())
                .build()) : submissions;
        return payment.toBuilder()
                .status(PaymentStatus.SubmissionFailed)
                .submissions(submissions)
                .build();
    }

    private Payment handleCompletePaymentSubmission(CompletePaymentSubmissionCommand command, Payment payment) {
        checkPaymentState(payment, PaymentStatus.ReadyForSubmission, command);
        var success = Submission.builder()
                                .paymentMethod(command.getMethod())
                                .psp(command.getPsp())
                                .timestamp(command.getTimestamp())
                                .pspMetadata(command.getPspMetadata())
                                .build();
        return payment.toBuilder()
                      .status(PaymentStatus.Submitted)
                      .submissions(command.getFailureDetails().map(fd -> fd.toSubmission(command.getMethod())).append(success))
                      .build();
    }

    private Payment handleReturnPayment(ReturnPaymentCommand command, Payment payment) {
        checkPaymentState(payment, PaymentStatus.Submitted, command);
        var returnEvent = Return.builder()
                                .code(command.getCode())
                                .reason(command.getMessage())
                                .paymentMethod(command.getMethod())
                                .psp(command.getPsp())
                                .pspMetadata(command.getPspMetadata())
                                .timestamp(command.getTimestamp())
                                .build();
        return payment.toBuilder()
                .status(PaymentStatus.Returned)
                .returns(returnEvent)
                .build();
    }

    private Payment handleCompletePayment(CompletePaymentCommand command, Payment payment) {
        checkPaymentState(payment, PaymentStatus.Submitted, command);
        var settlementEvent = Settlement.builder()
                                        .paymentMethod(command.getMethod())
                                        .pspMetadata(command.getPspMetadata())
                                        .psp(command.getPsp())
                                        .timestamp(command.getTimestamp())
                                        .build();
        return payment.toBuilder()
                .status(PaymentStatus.Paid)
                .settlement(settlementEvent)
                .build();
    }

    private <C extends HasPaymentId & HasKey<String>> void checkPaymentState(Payment payment, PaymentStatus requiredStatus, C command) {
        if (payment == null) {
            throw new IllegalStateException(
                    String.format("Command \"%s\" cannot be carried out as payment %s does not exist",
                            command.getClass().getSimpleName(), command.getPaymentId()));
        }
        var currentPaymentStatus = payment.getStatus();
        if (currentPaymentStatus != requiredStatus) {
            throw new IllegalStateException(
                    String.format("Command \"%s\" cannot be carried out as payment %s has status %s instead of %s",
                            command.getClass().getSimpleName(), payment.getKey(), currentPaymentStatus, requiredStatus));
        }
    }

}

