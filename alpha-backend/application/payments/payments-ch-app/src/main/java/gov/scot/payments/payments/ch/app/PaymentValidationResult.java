package gov.scot.payments.payments.ch.app;

import gov.scot.payments.payments.model.aggregate.Payment;
import lombok.NonNull;
import lombok.Value;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

@Value
public class PaymentValidationResult {

    private boolean success;
    private String message;

    public static PaymentValidationResult of(boolean valid, Supplier<String> message){
        if(valid){
            return success();
        }
        return failure(message.get());
    }

    public static PaymentValidationResult success(){
        return new PaymentValidationResult(true,null);
    }

    public static PaymentValidationResult failure(final String message) {
        if(message == null){
            throw new IllegalArgumentException("Message must not be null");
        }
        return new PaymentValidationResult(false,message);
    }

    public PaymentValidationResult merge(final PaymentValidationResult other) {
        if(success && other.success){
            if(message == null){
                return new PaymentValidationResult(true,other.message);
            } else if(other.message == null){
                return new PaymentValidationResult(true,message);
            } else {
                return new PaymentValidationResult(true,String.format("%s, %s",message,other.message));
            }
        }
        if(message == null){
            return failure(other.message);
        } else if(other.message == null){
            return failure(message);
        } else {
            return failure(String.format("%s, %s",message,other.message));
        }
    }
}
