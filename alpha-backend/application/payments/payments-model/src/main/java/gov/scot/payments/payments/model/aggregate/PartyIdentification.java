package gov.scot.payments.payments.model.aggregate;

import lombok.*;
import org.apache.avro.reflect.Nullable;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder(toBuilder = true)
//largely maps to partyIdentification135 in ISO20022 PAIN
public class PartyIdentification {

    //page 104 in MRD for PAIN

    @NonNull @Size(max = 140) private String name;
    @Nullable private PostalAddress address;
    @Nullable private PersonIdentification person;
    @Nullable private OrganizationIdentification organization;
    @Nullable @Pattern(regexp = "[A-Z]{2,2}") private String country;
    @Nullable private ContactDetails contactDetails;

}
