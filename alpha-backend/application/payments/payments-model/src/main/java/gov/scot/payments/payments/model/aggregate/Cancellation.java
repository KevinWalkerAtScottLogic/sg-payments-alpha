package gov.scot.payments.payments.model.aggregate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

import java.util.Map;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@SuperBuilder(toBuilder = true)
public class Cancellation extends PaymentEvent{

    @NonNull @Builder.Default private Status status = Status.Requested;
    @NonNull private String user;
    @NonNull private String reason;

    @NonNull private PaymentMethod method;
    @Nullable private String psp;
    @Nullable private String message;
    @Nullable private String code;
    @Nullable private Map<String,String> pspMetadata;

    public enum Status {
        Requested, Complete, Failed
    }

}
