package gov.scot.payments.payments.model.command;

import java.util.UUID;

public interface HasPaymentId {

    UUID getPaymentId();
}
