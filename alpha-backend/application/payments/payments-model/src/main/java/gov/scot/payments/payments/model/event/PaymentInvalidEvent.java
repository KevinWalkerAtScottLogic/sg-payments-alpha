package gov.scot.payments.payments.model.event;

import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.Payment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "payments", type = "paymentInvalid")
@NoArgsConstructor
public class PaymentInvalidEvent extends BasePaymentEventWithCause {

    public PaymentInvalidEvent(Payment payment) {
        super(payment);
    }
}
