package gov.scot.payments.payments.model.event;

import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.Payment;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "payments", type = "paymentApprovalRequested")
@NoArgsConstructor
@ToString
public class PaymentApprovalRequestedEvent extends BasePaymentEventWithCause {

    public PaymentApprovalRequestedEvent(Payment payment) {
        super(payment);
    }
}
