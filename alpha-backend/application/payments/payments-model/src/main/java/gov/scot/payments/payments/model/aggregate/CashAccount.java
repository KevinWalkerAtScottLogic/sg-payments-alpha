package gov.scot.payments.payments.model.aggregate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Union;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.validation.constraints.Size;
import java.util.HashMap;
import java.util.Map;

@Getter
//@AllArgsConstructor
@NoArgsConstructor
@ToString
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Union({PrepaidCard.class,SepaBankAccount.class,UKBankAccount.class,UKBuildingSocietyAccount.class,Void.class})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@type")
public abstract class CashAccount {

    //largely based on CashAccount38 from ISO20022 MDR for PAIN
    @NonNull
    @EqualsAndHashCode.Include
    @Size(max = 70)
    private String name;

//    @JsonDeserialize(using = CurrencyUnitDeserializer.class)
//    @JsonSerialize(using = CurrencyUnitSerializer.class)
    @NonNull
//    @Union(JDKCurrencyAdapter.class)
//    private CurrencyUnit currency;
    @Size(min = 3, max = 3)
    private String currency;

    @NonNull @Builder.Default
    private Map<String,String> properties = new HashMap<>();

    public CashAccount(String name, String currency) {
        this.name = name;

        // Throws UnknownCurrencyException for invalid currency
        this.currency = Monetary.getCurrency(currency).getCurrencyCode();
    }

    public CashAccount(String name, CurrencyUnit currency) {
        this.name = name;
        this.currency = currency.getCurrencyCode();
    }

    @JsonIgnore
    public CurrencyUnit getCurrencyUnit() {
        return Monetary.getCurrency(currency);
    }

    public static abstract class CashAccountBuilder<C extends CashAccount, B extends CashAccountBuilder<C, B>> {

        // Add .currencyUnit(CurrencyUnit) to the builder.
        public B currencyUnit(@NonNull CurrencyUnit currency) {
            this.currency = currency.getCurrencyCode();
            return self();
        }
    }
}
