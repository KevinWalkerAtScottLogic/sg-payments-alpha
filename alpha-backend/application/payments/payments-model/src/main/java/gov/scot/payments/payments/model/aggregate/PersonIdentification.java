package gov.scot.payments.payments.model.aggregate;

import io.vavr.collection.List;
import lombok.*;
import org.apache.avro.reflect.Nullable;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder(toBuilder = true)
public class PersonIdentification {

    //largely based on PersonIdentification13 from ISO20022 PAIN

    private DateAndPlaceOfBirth dateAndPlaceOfBirth;
    @Nullable private List<OtherIdentifiers> otherIdentifiers;

    @Getter
    @AllArgsConstructor
    @NoArgsConstructor
    @EqualsAndHashCode
    @ToString
    @Builder(toBuilder = true)
    public static class DateAndPlaceOfBirth {
        @NonNull private LocalDate birthDate;
        @Nullable @Size(max = 35) private String province;
        @NonNull @Size(max = 35) private String city;
        @NonNull @Pattern(regexp = "[A-Z]{2,2}") private String country;

    }

    @Getter
    @AllArgsConstructor
    @NoArgsConstructor
    @EqualsAndHashCode
    @ToString
    @Builder(toBuilder = true)
    public static class OtherIdentifiers {
        @NonNull @Size(max = 35) private String id;
        @Size(max = 35) private String issuer;
    }
}
