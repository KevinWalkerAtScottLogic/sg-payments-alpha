package gov.scot.payments.payments.model.aggregate;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;
import org.javamoney.moneta.Money;

import javax.validation.constraints.NotNull;
import java.util.Map;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@SuperBuilder(toBuilder = true)
public class Submission extends PaymentEvent {

    @NonNull @Builder.Default private Status status = Status.Success;
    @Nullable private PaymentMethod paymentMethod;
    @Nullable private String psp;
    @Nullable private String message;
    @Nullable private String code;
    @Nullable private Map<String,String> pspMetadata;

    public enum Status {
        Failure,
        Success
    }
}
