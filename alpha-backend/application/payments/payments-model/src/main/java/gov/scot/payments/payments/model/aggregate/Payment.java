package gov.scot.payments.payments.model.aggregate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.*;
import org.apache.avro.reflect.Nullable;
import org.javamoney.moneta.Money;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Builder(toBuilder = true)
@MessageType(context = "payments", type = "payment")
public class Payment implements HasKey<String>{

    public static final String METHOD_HEADER = "paymentMethod";
    //inspired by ISO20022 pain.001.001.09 CustomerCreditTransferInitiationV09

    //payment service specific fields
    @Nullable private String batchId;
    @NonNull private String createdBy;
    @NonNull @Builder.Default private Instant createdAt = Instant.now();
    @NonNull @Builder.Default @EqualsAndHashCode.Include private UUID id = UUID.randomUUID();
    @NonNull private List<PaymentMethod> allowedMethods;
    @NonNull @Builder.Default private Map<String,Map<String,String>> methodProperties = new HashMap<>();
    @NonNull @Builder.Default private PaymentStatus status = PaymentStatus.ReadyForSubmission;
    @Nullable private Approval approval;
    @Nullable private Approval cancellationApproval;
    @Nullable private Validation validation;
    @NonNull @Builder.Default private List<Submission> submissions = List.empty();
    @Nullable private Settlement settlement;
    @Nullable private Return returns;
    @Nullable private Cancellation cancellation;

    //payment fields
    @NonNull private String amountString;
    @NonNull @Builder.Default private Instant earliestExecutionDate  = Instant.now();
    @NonNull private Instant latestExecutionDate;

    //payee / payer fields
    @NonNull private CompositeReference product;

    @NonNull private CashAccount creditorAccount;
    @NonNull private PartyIdentification creditor;

    @NonNull private UKBankAccount debtorAccount;
    @NonNull private PartyIdentification debtor;

    //supplementary information
    @NonNull @Builder.Default private List<MetadataField> metadata = List.empty();
    @NonNull @Builder.Default private List<MetadataField> creditorMetadata = List.empty();

    @JsonIgnore
    public Money getAmount(){
        return Money.parse(amountString);
    }

    @JsonIgnore
    public Instant getUpdatedAt() {

        //TODO returns, cancellations etc..
        if (submissions.nonEmpty()) {
            return submissions.map(PaymentEvent::getTimestamp)
                    .max()
                    .get();
        } else if (approval != null) {
            return approval.getTimestamp();
        } else return createdAt;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return id.toString();
    }

    @JsonIgnore
    public <T extends CashAccount> Option<T> getCreditorAccountAs(Class<T> clazz){
        if(clazz.isAssignableFrom(creditorAccount.getClass())){
            return Option.of((T)creditorAccount);
        }
        return Option.none();
    }

    @JsonIgnore
    public Option<MetadataField> getCreditorMetadata(String name){
        return creditorMetadata.find(m -> m.getName().equals(name));
    }

    @JsonIgnore
    public Option<MetadataField> getMetadata(String name){
        return metadata.find(m -> m.getName().equals(name));
    }

    @JsonIgnore
    public io.vavr.collection.Map<String,String> getMethodProperties(PaymentMethod method){
        return io.vavr.collection.HashMap.ofAll(methodProperties.getOrDefault(method.name(),new HashMap<>()));
    }

    @JsonIgnore
    public Option<String> getMethodProperty(PaymentMethod method, String property){
        return io.vavr.collection.HashMap.ofAll(methodProperties.getOrDefault(method.name(),new HashMap<>())).get(property);
    }

    @JsonIgnore
    public io.vavr.collection.Map<String,MetadataField> getCreditorMetadataAsMap(){
        return creditorMetadata.toMap(m -> new Tuple2<>(m.getName(),m));
    }

    public static class PaymentBuilder {

        public PaymentBuilder amount(Money amount){
            this.amountString = amount.toString();
            return this;
        }

    }
}