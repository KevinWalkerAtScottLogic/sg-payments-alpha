package gov.scot.payments.payments.model.aggregate;

public enum PaymentStatus {

    Registered,
    Invalid,
    Valid,
    ApprovalRequired,
    Rejected,
    ReadyForSubmission,
    Submitted,
    SubmissionFailed,
    Returned,
    Paid
}
