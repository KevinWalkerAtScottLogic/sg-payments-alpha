package gov.scot.payments.payments.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.*;
import lombok.*;

import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Builder
@MessageType(context = "payments", type = "releasePayment")
@NoArgsConstructor
@AllArgsConstructor
public class ReleasePaymentCommand extends CommandImpl implements HasKey<String>, HasPaymentId {

    @NonNull private UUID paymentId;

    public ReleasePaymentCommand(UUID paymentId, boolean reply) {
        super(reply);
        this.paymentId = paymentId;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }

}
