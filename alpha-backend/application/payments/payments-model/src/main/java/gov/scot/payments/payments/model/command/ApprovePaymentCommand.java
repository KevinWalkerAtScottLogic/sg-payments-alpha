package gov.scot.payments.payments.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.*;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Subject;
import io.vavr.collection.Set;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "payments", type = "approvePayment")
@NoArgsConstructor
@AllArgsConstructor
public class ApprovePaymentCommand extends CommandImpl implements HasPaymentId, HasUser, HasStateVersion, HasKey<String> {

    @NonNull private UUID paymentId;
    @NonNull private String user;
    @Nullable private Set<Role> roles;
    @Nullable private Long stateVersion;

    public ApprovePaymentCommand(UUID paymentId, String user, Set<Role> roles, Long stateVersion, boolean reply) {
        super(reply);
        this.paymentId = paymentId;
        this.user = user;
        this.roles = roles;
        this.stateVersion = stateVersion;
    }

    @MessageConstructor(role = "payments:ApprovePayment")
    public static ApprovePaymentCommand fromRequest(String paymentId, boolean reply, Long stateVersion, Subject principal){
        return new ApprovePaymentCommand(UUID.fromString(paymentId), principal.getName(), principal.getRoles(), stateVersion, reply);
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }
}
