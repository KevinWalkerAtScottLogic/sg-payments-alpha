package gov.scot.payments.payments.model.aggregate;

import lombok.*;
import org.apache.avro.reflect.Nullable;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder(toBuilder = true)
public class ContactDetails {

    //largely based on Contact4 from ISO20022 PAIN

    public enum NamePrefix { DOCT, MADM, MISS, MIST, MIKS};

    @Nullable private NamePrefix namePrefix;
    @Size(max = 140) private String name;
    @Nullable @Pattern(regexp = "\\+[0-9]{1,3}-[0-9()+\\-]{1,30}") private String phoneNumber;
    @Nullable @Pattern(regexp = "\\+[0-9]{1,3}-[0-9()+\\-]{1,30}") private String mobileNumber;
    @Nullable @Pattern(regexp = "\\+[0-9]{1,3}-[0-9()+\\-]{1,30}") private String faxNumber;
    @Nullable @Size(max = 2048)private String emailAddress;
    @Nullable @Size(max = 35) private String emailPurpose;
    @Nullable @Size(max = 35) private String jobTitle;
    @Nullable @Size(max = 35) private String responsibility;
    @Nullable @Size(max = 70) private String department;

}
