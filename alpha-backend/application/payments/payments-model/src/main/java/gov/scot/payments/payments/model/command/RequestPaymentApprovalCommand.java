package gov.scot.payments.payments.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import lombok.*;

import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Builder
@MessageType(context = "payments", type = "requestPaymentApproval")
@NoArgsConstructor
@AllArgsConstructor
public class RequestPaymentApprovalCommand extends CommandImpl implements HasKey<String>, HasPaymentId {

    @NonNull private UUID paymentId;
    @NonNull private String reason;

    public RequestPaymentApprovalCommand(UUID paymentId, String reason, boolean reply) {
        super(reply);
        this.paymentId = paymentId;
        this.reason = reason;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }

}
