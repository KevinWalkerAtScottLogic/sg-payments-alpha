package gov.scot.payments.payments.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.HasState;
import gov.scot.payments.payments.model.aggregate.Payment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@NoArgsConstructor
public abstract class BasePaymentEventWithCause extends EventWithCauseImpl implements HasState<Payment>, HasKey<String> {

    @Getter private Payment carriedState;
    @Getter private Long stateVersion;

    public BasePaymentEventWithCause(Payment carriedState){
        this.carriedState = carriedState;
    }

    @Override
    @JsonIgnore
    public void setCarriedState(Payment state, Long version) {
        this.carriedState = state;
        this.stateVersion = version;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return getCarriedState().getId().toString();
    }
}
