package gov.scot.payments.payments.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.*;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.payments.model.api.CancelPaymentRequest;
import io.vavr.collection.Set;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "payments", type = "cancelPayment")
@NoArgsConstructor
@AllArgsConstructor
public class CancelPaymentCommand extends CommandImpl implements HasPaymentId, HasUser, HasStateVersion, HasKey<String> {

    @NonNull private UUID paymentId;
    @NonNull private String user;
    @NonNull private Set<Role> roles;
    @Nullable private Long stateVersion;
    @NonNull private String reason;

    public CancelPaymentCommand(UUID paymentId, String user, Set<Role> roles, String reason, Long stateVersion, boolean reply) {
        super(reply);
        this.paymentId = paymentId;
        this.user = user;
        this.roles = roles;
        this.reason = reason;
        this.stateVersion = stateVersion;
    }

    @MessageConstructor(role = "payments:CancelPayment")
    public static CancelPaymentCommand fromRequest(CancelPaymentRequest request, boolean reply, Long stateVersion, Subject principal){
        return new CancelPaymentCommand(request.getPaymentId(), principal.getName(), principal.getRoles(), request.getReason(), stateVersion, reply);
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }
}
