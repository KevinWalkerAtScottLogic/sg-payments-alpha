package gov.scot.payments.payments.model.event;

import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.event.BasePaymentEventWithCause;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "payments", type = "paymentCancellationFailed")
@NoArgsConstructor
public class PaymentCancellationFailedEvent extends BasePaymentEventWithCause {

    public PaymentCancellationFailedEvent(Payment payment) {
        super(payment);
    }
}
