package gov.scot.payments.payments.model.aggregate;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.type.WritableTypeId;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;

import javax.money.CurrencyUnit;
import java.io.IOException;

import static com.fasterxml.jackson.core.JsonToken.START_OBJECT;

public class CurrencyUnitSerializer extends JsonSerializer<CurrencyUnit> {

    @Override
    public void serialize(CurrencyUnit value, JsonGenerator gen, SerializerProvider serializers) throws IOException {

        gen.writeString(value.getCurrencyCode());
    }

    @Override
    public void serializeWithType(CurrencyUnit value, JsonGenerator gen, SerializerProvider serializers, TypeSerializer typeSer) throws IOException {
        WritableTypeId typeId = typeSer.typeId(value, START_OBJECT);

        typeSer.writeTypePrefix(gen, typeId);
        gen.writeFieldName("currencyUnit");
        serialize(value, gen, serializers);
        typeSer.writeTypeSuffix(gen, typeId);
    }
}
