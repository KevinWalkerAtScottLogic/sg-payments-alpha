package gov.scot.payments.payments.model.aggregate;

public enum PaymentMethod {

    Chaps,
    Bacs,
    FasterPayments,
    Sepa_DC,
    iMovo;
}
