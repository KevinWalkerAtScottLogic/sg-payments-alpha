package gov.scot.payments.payments.model.aggregate;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.*;
import org.apache.avro.reflect.Stringable;

import java.io.IOException;
import java.util.regex.Pattern;

@Getter
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Stringable
public class UKAccountNumber {

    private static int ACCOUNT_NUMBER_LENGTH = 8;

    private static final Pattern regexNumeric = Pattern.compile("^[0-9]*$");

    @NonNull private String value;

    public UKAccountNumber(@NonNull String value) {
        if(value.length() != ACCOUNT_NUMBER_LENGTH){
            throw new IllegalArgumentException("Invalid account number "+ value + " - must be 8 digits");
        }
        boolean isNumeric = regexNumeric.matcher(value).matches();
        if(!isNumeric){
            throw new IllegalArgumentException("Invalid account number "+ value + " - must be numeric");
        }
        this.value = value;
    }

    public static UKAccountNumber fromString(String accountNumberStr) throws IllegalArgumentException {
        return new UKAccountNumber(accountNumberStr);
    }

    @Override
    public String toString() {
        return value;
    }

    public static class UKAccountNumberSerializer extends JsonSerializer<UKAccountNumber> {


        @Override
        public void serialize(UKAccountNumber value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeString(value.toString());
        }
    }

    public static class UKAccountNumberDeserializer extends JsonDeserializer<UKAccountNumber> {
        @Override
        public UKAccountNumber deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
            return UKAccountNumber.fromString(p.getText());
        }
    }
}
