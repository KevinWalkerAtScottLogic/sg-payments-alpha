package gov.scot.payments.payments.model.aggregate;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.io.IOException;

public class CurrencyUnitDeserializer extends JsonDeserializer<CurrencyUnit> {

    @Override
    public CurrencyUnit deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        var oc = p.getCodec();
        JsonNode node = oc.readTree(p);
        String code = node.asText();
        return Monetary.getCurrency(code);
    }

    @Override
    public Object deserializeWithType(JsonParser p, DeserializationContext ctxt,
                                      TypeDeserializer typeDeserializer) throws IOException {
        var oc = p.getCodec();
        JsonNode node = oc.readTree(p);
        String code;
        if (node.isTextual()) {
            code = node.asText();
        } else {
            code = node.get(1).get("currencyUnit").asText();
        }
        return Monetary.getCurrency(code);
    }
}
