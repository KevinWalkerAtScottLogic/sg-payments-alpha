package gov.scot.payments.payments.model.api;

import lombok.*;

import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Builder(toBuilder = true)
public class RejectPaymentRequest {

    @NonNull @EqualsAndHashCode.Include private UUID paymentId;
    @NonNull private String reason;
}
