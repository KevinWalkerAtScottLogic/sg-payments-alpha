package gov.scot.payments.payments.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.*;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.payments.model.aggregate.Payment;
import io.vavr.collection.Set;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "payments", type = "registerPayment")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RegisterPaymentCommand extends CommandImpl implements HasUser,  HasKey<String> {

    @NonNull private Payment payment;
    @Nullable private Set<Role> roles;

    public RegisterPaymentCommand(Payment payment, boolean reply) {
        super(reply);
        this.payment = payment;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return payment.getKey();
    }

    @Override
    @JsonIgnore
    public String getUser() {
        return payment.getCreatedBy();
    }
}
