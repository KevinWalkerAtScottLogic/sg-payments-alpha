package gov.scot.payments.payments.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.*;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.payments.model.api.RejectPaymentRequest;
import io.vavr.collection.Set;
import lombok.*;
import org.apache.avro.reflect.Nullable;

import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Builder
@MessageType(context = "payments", type = "rejectPaymentApproval")
@NoArgsConstructor
@AllArgsConstructor
public class RejectPaymentApprovalCommand extends CommandImpl implements HasPaymentId, HasUser, HasStateVersion, HasKey<String> {

    @NonNull private UUID paymentId;
    @NonNull private String reason;
    @NonNull private String user;
    @Nullable private Set<Role> roles;
    @Nullable private Long stateVersion;

    public RejectPaymentApprovalCommand(UUID paymentId, String reason, String user, Set<Role> roles, Long stateVersion, boolean reply) {
        super(reply);
        this.paymentId = paymentId;
        this.reason = reason;
        this.user = user;
        this.roles = roles;
        this.stateVersion = stateVersion;
    }

    @MessageConstructor(role = "payments:ApprovePayment")
    public static RejectPaymentApprovalCommand fromRequest(RejectPaymentRequest request, boolean reply, Long stateVersion, Subject principal){
        return new RejectPaymentApprovalCommand(request.getPaymentId(),request.getReason(), principal.getName(), principal.getRoles(), stateVersion, reply);
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }

}
