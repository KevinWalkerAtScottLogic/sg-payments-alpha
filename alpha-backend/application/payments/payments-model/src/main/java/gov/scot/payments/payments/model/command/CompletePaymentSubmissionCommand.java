package gov.scot.payments.payments.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.HasStateVersion;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.PaymentSubmissionFailureDetails;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.payments.model.command.HasPaymentId;
import io.vavr.collection.List;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

import java.util.Map;
import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "payments", type = "completePaymentSubmission")
@NoArgsConstructor
@AllArgsConstructor
public class CompletePaymentSubmissionCommand extends CommandImpl implements HasPaymentId, HasKey<String> {

    @NonNull private UUID paymentId;
    @NonNull private PaymentMethod method;
    @NonNull private String psp;
    @NonNull @Builder.Default private Map<String,String> pspMetadata = Map.of();
    @NonNull @Builder.Default private List<PaymentSubmissionFailureDetails> failureDetails = List.empty();

    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }
}
