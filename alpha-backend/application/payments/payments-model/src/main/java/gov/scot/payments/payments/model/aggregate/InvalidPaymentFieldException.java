package gov.scot.payments.payments.model.aggregate;

public class InvalidPaymentFieldException extends RuntimeException {

    public InvalidPaymentFieldException(String errorMessage){
        super(errorMessage);
    }

}
