package gov.scot.payments.payments.model.aggregate;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
@SuperBuilder(toBuilder = true)
public class UKBuildingSocietyAccount extends UKBankAccount{

    @NonNull private String rollNumber;

}
