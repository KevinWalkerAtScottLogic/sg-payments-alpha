package gov.scot.payments.payments.proj.model;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.event.PaymentInvalidEvent;
import gov.scot.payments.payments.model.event.PaymentReadyForSubmissionEvent;
import gov.scot.payments.payments.model.event.PaymentRegisteredEvent;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.HOURS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

class PaymentSummaryTest {

    private static final UUID TEST_ID = UUID.randomUUID();
    private static final String BATCH_ID = "batch_id";
    public static final String CURRENCY_CODE = "GBP";
    public static final long AMOUNT = 123L;
    private static final String ACCOUNT_NUMBER = "12345678";
    private static final String SORT_CODE = "123456";
    private static final String CREDITOR_NAME = "creditorName";
    private static final String CREATED_BY = "Test User";
    private static final Instant CREATED_AT = Instant.parse("2020-01-03T12:00:00.00Z");
    private static final CompositeReference PRODUCT = CompositeReference.parse("Customer.Product");
    private static final Instant TIMESTAMP_1 = Instant.parse("2020-01-03T12:01:00.00Z");
    private static final Instant TIMESTAMP_2 = Instant.parse("2020-01-03T12:02:00.00Z");


    // These two dates should be the same
    private static final Instant LATEST_EXECUTION_DATE = Instant.parse("2020-01-06T00:00:00.00Z");
    private static final LocalDate PAYMENT_DATE = LocalDate.of(2020, 1, 6);

    @Test
    @DisplayName("Give a minimal PaymentCreatedEvent, when calling fromCreateEvent then a minimal Payment Summary is created")
    void givenMinimalPaymentCreatedEventWhenCallFromCreateEventThenPaymentSummaryCreated() {

        var event = PaymentRegisteredEvent.builder()
                .carriedState(Payment.builder()
                        .id(TEST_ID)
                        .createdBy(CREATED_BY)
                        .createdAt(CREATED_AT)
                        .status(PaymentStatus.ApprovalRequired)
                        .amount(Money.of(AMOUNT, CURRENCY_CODE))
                        .earliestExecutionDate(CREATED_AT)
                        .latestExecutionDate(LATEST_EXECUTION_DATE)
                        .product(PRODUCT)
                        .debtorAccount(getCreditorAccount())
                        .debtor(getCreditor())
                        .creditorAccount(getCreditorAccount())
                        .creditor(getCreditor())
                        .allowedMethods(List.empty())
                        .build())
                .timestamp(TIMESTAMP_1)
                .build();

        var paymentSummary = PaymentSummary.fromRegisterEvent(event);

        assertThat(paymentSummary.getId(), is(TEST_ID));
        assertThat(paymentSummary.getBatchId(), is(nullValue()));
        assertThat(paymentSummary.getCreatedAt(), is(CREATED_AT));
        assertThat(paymentSummary.getStatus(), is(PaymentSummaryStatus.AwaitingApproval));
        assertThat(paymentSummary.getMessage(), is(nullValue()));
        assertThat(paymentSummary.getAmount(), is(BigDecimal.valueOf(AMOUNT)));
        assertThat(paymentSummary.getCurrency(), is(CURRENCY_CODE));
        assertThat(paymentSummary.getPaymentDate(), is(PAYMENT_DATE));
        assertThat(paymentSummary.getProcessingTime(), is(TIMESTAMP_1));
    }

    @Test
    @DisplayName("Give a maximal PaymentCreatedEvent, when calling fromCreateEvent then a minimal Payment Summary is created")
    void givenMaximalPaymentCreatedEventWhenCallFromCreateEventThenPaymentSummaryCreated() {

        var event = PaymentRegisteredEvent.builder()
                .carriedState(Payment.builder()
                        .id(TEST_ID)
                        .batchId(BATCH_ID)
                        .createdBy(CREATED_BY)
                        .createdAt(CREATED_AT)
                        .status(PaymentStatus.ApprovalRequired)
                        .amount(Money.of(AMOUNT, CURRENCY_CODE))
                        .earliestExecutionDate(CREATED_AT)
                        .latestExecutionDate(LATEST_EXECUTION_DATE)
                        .product(PRODUCT)
                        .debtorAccount(getCreditorAccount())
                        .debtor(getCreditor())
                        .creditorAccount(getCreditorAccount())
                        .creditor(getCreditor())
                        .allowedMethods(List.empty())
                        .build())
                .timestamp(TIMESTAMP_1)
                .build();

        var paymentSummary = PaymentSummary.fromRegisterEvent(event);

        assertThat(paymentSummary.getId(), is(TEST_ID));
        assertThat(paymentSummary.getBatchId(), is(BATCH_ID));
        assertThat(paymentSummary.getCreatedAt(), is(CREATED_AT));
        assertThat(paymentSummary.getStatus(), is(PaymentSummaryStatus.AwaitingApproval));
        assertThat(paymentSummary.getMessage(), is(nullValue()));
        assertThat(paymentSummary.getAmount(), is(BigDecimal.valueOf(AMOUNT)));
        assertThat(paymentSummary.getCurrency(), is(CURRENCY_CODE));
        assertThat(paymentSummary.getPaymentDate(), is(PAYMENT_DATE));
        assertThat(paymentSummary.getProcessingTime(), is(TIMESTAMP_1));
    }

    @Test
    @DisplayName("Give a PaymentInvalidEvent, when calling updateProjectionfromInvalidEvent then an updated Payment Summary is created")
    void givenPaymentInvalidEventWhenUpdateProjectionFromInvalidEventThenPaymentSummaryUpdated() {

        var paymentSummary = PaymentSummary.builder()
                .id(TEST_ID)
                .batchId(BATCH_ID)
                .createdAt(CREATED_AT)
                .status(PaymentSummaryStatus.AwaitingApproval)
                .amount(BigDecimal.valueOf(AMOUNT))
                .currency(CURRENCY_CODE)
                .paymentDate(PAYMENT_DATE)
                .customer(PRODUCT.getComponent0())
                .product(PRODUCT.toString())
                .processingTime(now().minus(5, HOURS))
                .build();

        var event = PaymentInvalidEvent.builder()
                .carriedState(Payment.builder()
                        .id(TEST_ID)
                        .batchId(BATCH_ID)
                        .createdBy(CREATED_BY)
                        .createdAt(CREATED_AT)
                        .status(PaymentStatus.Invalid)
                        .amount(Money.of(AMOUNT, CURRENCY_CODE))
                        .earliestExecutionDate(CREATED_AT)
                        .latestExecutionDate(LATEST_EXECUTION_DATE)
                        .product(PRODUCT)
                        .debtorAccount(getCreditorAccount())
                        .debtor(getCreditor())
                        .creditorAccount(getCreditorAccount())
                        .creditor(getCreditor())
                        .allowedMethods(List.empty())
                        .validation(Validation.builder().status(Validation.Status.Failure).message("a").build())
                        .build())
                .timestamp(TIMESTAMP_2)
                .build();

        var updatedPaymentSummary = paymentSummary.updateStatusAndMessage(event,event.getCarriedState().getValidation().getMessage());

        assertThat(updatedPaymentSummary.getId(), is(TEST_ID));
        assertThat(updatedPaymentSummary.getBatchId(), is(BATCH_ID));
        assertThat(updatedPaymentSummary.getCreatedAt(), is(CREATED_AT));
        assertThat(updatedPaymentSummary.getStatus(), is(PaymentSummaryStatus.Error));
        assertThat(updatedPaymentSummary.getMessage(), is("a"));
        assertThat(updatedPaymentSummary.getAmount(), is(BigDecimal.valueOf(AMOUNT)));
        assertThat(updatedPaymentSummary.getCurrency(), is(CURRENCY_CODE));
        assertThat(updatedPaymentSummary.getPaymentDate(), is(PAYMENT_DATE));
        assertThat(updatedPaymentSummary.getProcessingTime(), is(TIMESTAMP_2));
    }

    @Test
    @DisplayName("Give a PaymentReadyForSubmissionEvent, when calling updateProjectionFromReadyForSubmissionEvent then an updated Payment Summary is created")
    void givenPaymentReadyForSubmissionEventWhenUpdateProjectionFromReadyForSubmissionEventThenPaymentSummaryUpdated() {

        var paymentSummary = PaymentSummary.builder()
                .id(TEST_ID)
                .batchId(BATCH_ID)
                .createdAt(CREATED_AT)
                .status(PaymentSummaryStatus.AwaitingApproval)
                .amount(BigDecimal.valueOf(AMOUNT))
                .currency(CURRENCY_CODE)
                .paymentDate(PAYMENT_DATE)
                .customer(PRODUCT.getComponent0())
                .product(PRODUCT.toString())
                .processingTime(now().minus(5, HOURS))
                .build();

        var event = PaymentReadyForSubmissionEvent.builder()
                .carriedState(Payment.builder()
                        .id(TEST_ID)
                        .batchId(BATCH_ID)
                        .createdBy(CREATED_BY)
                        .createdAt(CREATED_AT)
                        .status(PaymentStatus.ReadyForSubmission)
                        .amount(Money.of(AMOUNT, CURRENCY_CODE))
                        .earliestExecutionDate(CREATED_AT)
                        .latestExecutionDate(LATEST_EXECUTION_DATE)
                        .product(PRODUCT)
                        .debtorAccount(getCreditorAccount())
                        .debtor(getCreditor())
                        .creditorAccount(getCreditorAccount())
                        .creditor(getCreditor())
                        .allowedMethods(List.empty())
                        .build())
                .timestamp(TIMESTAMP_2)
                .build();

        var updatedPaymentSummary = paymentSummary.updateStatus(event);

        assertThat(updatedPaymentSummary.getId(), is(TEST_ID));
        assertThat(updatedPaymentSummary.getBatchId(), is(BATCH_ID));
        assertThat(updatedPaymentSummary.getCreatedAt(), is(CREATED_AT));
        assertThat(updatedPaymentSummary.getStatus(), is(PaymentSummaryStatus.InProgress));
        assertThat(updatedPaymentSummary.getMessage(), is(nullValue()));
        assertThat(updatedPaymentSummary.getAmount(), is(BigDecimal.valueOf(AMOUNT)));
        assertThat(updatedPaymentSummary.getCurrency(), is(CURRENCY_CODE));
        assertThat(updatedPaymentSummary.getPaymentDate(), is(PAYMENT_DATE));
        assertThat(updatedPaymentSummary.getProcessingTime(), is(TIMESTAMP_2));
    }

    private UKBankAccount getCreditorAccount() {
        return UKBankAccount.builder()
                .name("acct")
                .currency(CURRENCY_CODE)
                .sortCode(new SortCode(SORT_CODE))
                .accountNumber(new UKAccountNumber(ACCOUNT_NUMBER))
                .build();
    }

    private PartyIdentification getCreditor() {
        return PartyIdentification.builder()
                .name(CREDITOR_NAME)
                .build();
    }
}
