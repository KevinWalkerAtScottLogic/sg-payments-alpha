package gov.scot.payments.payments.proj.model;

public enum RecipientType {

    UKBankAccount,
    UKBuildingSociety,
    SepaDirectCredit

}
