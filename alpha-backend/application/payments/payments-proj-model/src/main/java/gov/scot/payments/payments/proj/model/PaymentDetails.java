package gov.scot.payments.payments.proj.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.Projection;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.payments.model.aggregate.PaymentStatus;
import gov.scot.payments.payments.model.event.BasePaymentEventWithCause;
import gov.scot.payments.payments.model.event.PaymentRegisteredEvent;
import lombok.*;
import org.apache.avro.reflect.Nullable;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode(of = "id")
public class PaymentDetails implements Projection<UUID>, HasKey<String> {

    @NonNull @Id private UUID id;
    @NonNull private Instant processingTime;

    // transaction information
    @Nullable private String batchId;
    @NonNull private Instant createdAt;
    @NonNull private LocalDate paymentDate;
    @NonNull private String product;
    @NonNull private String customer;

    @NonNull private BigDecimal amount;
    @NonNull private String currency;
    @Enumerated(EnumType.STRING)
    @NonNull private PaymentStatus status;

    // what should this type be by default?
    @Enumerated(EnumType.STRING)
    @Nullable private PaymentMethod type;
    @Nullable private String message;
    @Nullable private Long stateVersion;

    @Embedded
    @NonNull RecipientInformation recipientInfo;

    //is this ordered?
    @Embedded
    @ElementCollection(fetch = FetchType.EAGER)
    @NonNull List<TransactionEvent> events;


    public static PaymentDetails fromRegisterEvent(PaymentRegisteredEvent event) {

        var payment = event.getCarriedState();

        var recipientInfo = RecipientInformation.fromAccount(payment.getCreditorAccount(), payment.getCreditor());

        var transactionEvent = TransactionEvent.builder()
                .date(event.getTimestamp())
                .status(TransactionEvent.getPaymentStatusDisplayString(payment.getStatus()))
                .user(payment.getCreatedBy())
                .build();

        return PaymentDetails.builder()
                .id(payment.getId())
                .processingTime(event.getTimestamp())
                .batchId(payment.getBatchId())
                .createdAt(payment.getCreatedAt())
                .status(payment.getStatus())
                .amount(new BigDecimal(payment.getAmount().getNumber().toString()))
                .currency(payment.getAmount().getCurrency().getCurrencyCode())
                .paymentDate(LocalDate.ofInstant(payment.getLatestExecutionDate(), ZoneOffset.UTC))
                .product(payment.getProduct().toString())
                .recipientInfo(recipientInfo)
                .events(List.of(transactionEvent))
                .customer(payment.getProduct().getComponent0())
                .stateVersion(event.getStateVersion())
                .build();
    }

    public PaymentDetails updateFromEvent(BasePaymentEventWithCause event){

        var payment = event.getCarriedState();
        var transactionEvent = TransactionEvent.fromPayment(payment, event.getTimestamp());

        var updatedEvents = getEvents();
        updatedEvents.add(transactionEvent);

        return toBuilder()
                .status(payment.getStatus())
                .events(updatedEvents)
                .build();
    }

    @Override
    @JsonIgnore
    public String getKey(){
        return id.toString();
    }

}
