package gov.scot.payments.payments.proj.model;

import gov.scot.payments.payments.model.aggregate.PaymentStatus;

public enum PaymentSummaryStatus {

    AwaitingApproval("Awaiting approval"),
    Cancelled("Cancelled"),
    Error("Error"),
    InProgress("In progress"),
    Paid("Paid"),
    Returned("Returned");

    private final String display;

    PaymentSummaryStatus(String display) {
        this.display = display;
    }

    public String toDisplay() {
        return display;
    }

    public static PaymentSummaryStatus fromPaymentStatus(PaymentStatus paymentStatus) {
        /*
         * TODO, we probably need more PaymentSummaryStatus cases, and more mapping logic.
         */

        switch (paymentStatus) {
            case Valid:
            case ReadyForSubmission:
            case Registered:
            case Submitted: return PaymentSummaryStatus.InProgress;
            case Invalid:
            case Rejected:
            case SubmissionFailed: return PaymentSummaryStatus.Error;
            case ApprovalRequired: return PaymentSummaryStatus.AwaitingApproval;
            case Returned: return PaymentSummaryStatus.Returned;
            case Paid: return PaymentSummaryStatus.Paid;

            default: throw new IllegalStateException("Unhandled PaymentStatus: " + paymentStatus);
        }
    }
}

