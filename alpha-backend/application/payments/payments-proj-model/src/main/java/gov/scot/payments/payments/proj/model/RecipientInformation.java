package gov.scot.payments.payments.proj.model;

import gov.scot.payments.payments.model.aggregate.*;
import lombok.*;
import org.apache.avro.reflect.Nullable;

import javax.persistence.Embeddable;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode(of = "name")
@Embeddable
public class RecipientInformation {

    @NonNull  private String name;

    @Nullable private String clientReference;
    @Nullable  private String sortCode;
    @Nullable  private String accountNumber;
    @Nullable  private String rollNumber;
    @Nullable  private String iban;

    public static RecipientInformation fromAccount(CashAccount account, PartyIdentification creditor){

        var builder = RecipientInformation.builder();

        builder.name(creditor.getName());

        if((UKBankAccount.class).isAssignableFrom(account.getClass())){
            var ukAccount = (UKBankAccount)account;
            builder.accountNumber(ukAccount.getAccountNumber().toString());
            builder.sortCode(ukAccount.getSortCode().toString());
        }
        if((UKBuildingSocietyAccount.class).isAssignableFrom(account.getClass())){
            var buildingSocietyAccount = (UKBuildingSocietyAccount)account;
            builder.rollNumber(buildingSocietyAccount.getRollNumber());
        }
        if((SepaBankAccount.class).isAssignableFrom(account.getClass())){
            var sepaBankAccount = (SepaBankAccount)account;
            builder.iban(sepaBankAccount.getIban());
        }
        return builder.build();

    }


}
