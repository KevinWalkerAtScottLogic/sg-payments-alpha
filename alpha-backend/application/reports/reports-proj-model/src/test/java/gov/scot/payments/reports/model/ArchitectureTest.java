package gov.scot.payments.reports.model;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchRules;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import gov.scot.payments.testing.architecture.CodingStandardsRules;
import gov.scot.payments.testing.architecture.ComponentRules;
import gov.scot.payments.testing.architecture.ModelRules;

@AnalyzeClasses(importOptions = {ImportOption.DoNotIncludeTests.class, ImportOption.DoNotIncludeArchives.class})
public class ArchitectureTest{

    @ArchTest
    public static final ArchRules generalRules = ArchRules.in(CodingStandardsRules.class);

    @ArchTest
    public static final ArchRules modelRules = ArchRules.in(ModelRules.class);

    @ArchTest
    public static final ArchRule anyAnnotatedMessagesMustHaveSameContext
            = ComponentRules.anyAnnotatedMessagesMustHaveSameContext("reports");

    @ArchTest
    public static final ArchRule noCommandsMustBeDefined = ComponentRules.noCommandsDefined;

}