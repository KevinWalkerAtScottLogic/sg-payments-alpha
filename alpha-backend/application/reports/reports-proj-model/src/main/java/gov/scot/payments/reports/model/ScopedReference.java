package gov.scot.payments.reports.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Scope;
import gov.scot.payments.model.user.User;
import io.vavr.collection.Set;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ScopedReference {

    private String customers;
    private String products;

    private static Role REPORT_SUPER_USER = Role.parse("./payments:Read");

    @JsonIgnore
    public static ScopedReference fromUser(User user){

        if(user.hasAccess(REPORT_SUPER_USER)){
            // set to null for both columns so quicksight gives access
            // to all data
            return ScopedReference.builder().build();
        };

        var scopes = user.getScopes();
        var customerStr = getCommaSeparatedCustomers(scopes);
        var custProductStr = getCommaSeparatedCustomerAndProducts(scopes);

        var builder = ScopedReference.builder();
        builder.customers(customerStr);
        if(!custProductStr.isEmpty()){
            builder.products(custProductStr);
        }
        return builder.build();
    }

    @JsonIgnore
    public static String doesUserHaveGlobalAccess(Set<Scope> scopes){

        var customersWithNoProduct = scopes.filter(s -> s.getParent() == null).map(s -> s.getName());
        var customersWithProduct = scopes.filter(s -> s.getParent() != null).map(s -> s.getParent().getName());
        var allCustomers = customersWithNoProduct.addAll(customersWithProduct);
        return String.join(",", allCustomers);

    }

    @JsonIgnore
    public static String getCommaSeparatedCustomers(Set<Scope> scopes){

        var customersWithNoProduct = scopes.filter(s -> s.getParent() == null).map(s -> s.getName());
        var customersWithProduct = scopes.filter(s -> s.getParent() != null).map(s -> s.getParent().getName());
        var allCustomers = customersWithNoProduct.addAll(customersWithProduct);
        return String.join(",", allCustomers);

    }

    @JsonIgnore
    public static String getCommaSeparatedCustomerAndProducts(Set<Scope> scopes){

        var customersWithProduct = scopes.filter(s -> s.getParent() != null).map(s -> s.getQualifiedName());
        return String.join(",", customersWithProduct);

    }

}
