package gov.scot.payments.reports.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.Projection;
import lombok.*;
import org.apache.avro.reflect.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.Instant;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode(of = "email")
public class ReportUserProjection implements Projection<String>, HasKey<String>{

    @NonNull @Builder.Default Instant processingTime = Instant.now();

    @Column(name="UserName")
    @NonNull @Id private String email;

    @Nullable private String customer;
    @Nullable private String product;

    @Override
    @JsonIgnore
    public String getKey(){
        return email;
    }

    @Override
    public String getId() {
        return email;
    }
}