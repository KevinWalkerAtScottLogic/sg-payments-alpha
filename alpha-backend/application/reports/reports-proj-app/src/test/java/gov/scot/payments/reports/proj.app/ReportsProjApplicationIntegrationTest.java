package gov.scot.payments.reports.proj.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.reports.proj.app.quicksight.QuickSightApplicationListener;
import gov.scot.payments.reports.proj.app.quicksight.QuickSightService;
import gov.scot.payments.reports.proj.app.quicksight.config.QuickSightConfigFile;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import org.apache.kafka.streams.KafkaStreams;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.io.IOException;
import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ApplicationIntegrationTest(classes = {ReportsProjApplicationIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.PROJECTOR
        , properties = {"cloud.aws.credentials.accessKey=test"
        , "cloud.aws.credentials.secretKey=test"
        , "cloud.aws.region.static=eu-west-2"
        , "quicksight.role-arn=testRole"
        , "quicksight.create-datasets=true"
        , "file.event.queue=test"
        , "spring.jpa.hibernate.naming.physical-strategy=org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl"
        , "AWS_QUICKSIGHT_ACCOUNT_ID=123"}
)
public class ReportsProjApplicationIntegrationTest {


    @Autowired
    QuickSightService service;

    @Autowired
    QuickSightApplicationListener listener;

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, EmbeddedBrokerClient brokerClient){
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
    }

    @Test
    public void testCreateDataSetsCalled(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient) throws IOException {

        listener.onApplicationEvent(null);

        ArgumentCaptor<QuickSightConfigFile> configsCapture = ArgumentCaptor.forClass(QuickSightConfigFile.class);
        verify(service, times(1)).createDataSetsFromConfigFile(configsCapture.capture());

        var config = configsCapture.getValue();
        assertEquals(2, config.getDatasets().size());
        assertEquals("submissions_dataset", config.getDatasets().get(0).getName());
        assertEquals("payments_dataset", config.getDatasets().get(1).getName());
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    @EnableAutoConfiguration(exclude = {HypermediaAutoConfiguration.class})
    public static class TestApplication extends ReportsProjApplication{

        @MockBean
        QuickSightService service;

    }
}