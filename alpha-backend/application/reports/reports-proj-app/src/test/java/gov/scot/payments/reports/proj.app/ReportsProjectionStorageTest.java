package gov.scot.payments.reports.proj.app;

import gov.scot.payments.application.component.projector.springdata.ProjectionRepository;
import gov.scot.payments.application.component.projector.springdata.RepositoryMutatingStorageService;
import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.user.Group;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.User;
import gov.scot.payments.reports.model.ReportUserProjection;
import gov.scot.payments.users.model.UserAddedEvent;
import gov.scot.payments.users.model.UserRemovedEvent;
import gov.scot.payments.users.model.UserRolesAssignedEvent;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.collection.HashSet;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;


@AutoConfigurationPackage
@EntityScan("gov.scot.payments.reports.model")
@DataJpaTest(properties = {"spring.datasource.url=jdbc:h2:mem:ReportsProjectionStorageTest"})
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=validate",
        "spring.flyway.locations=classpath:db/migration/h2",
        "spring.flyway.schemas=",
        "spring.jpa.properties.hibernate.default_schema="
})
@ContextConfiguration(classes = ReportsProjectionStorageTest.TestConfiguration.class)
public class ReportsProjectionStorageTest {

    @Autowired
    private RepositoryMutatingStorageService<String, ReportUserProjection> service;

    @Autowired
    private ProjectionRepository<String, ReportUserProjection> jpaRepository;

    @Autowired
    private JpaRepository<ReportUserProjection, String> repository;

    @Test
    @DisplayName("Given a UserAddEvent then User Project stored")
    public void givenUserAddedEventThenUserCreated() {

        var user = User.builder().name("test-user").email("test-email")
                .groups(HashSet.of(new Group("customer.product")))
                .roles(HashSet.of(Role.parse("customer.product/payment:Read")))
                .build();

        var event = new UserAddedEvent(user);
        var result = service.apply(event.getKey(), event);
        assertEquals(1, repository.count());

        var userRepo = repository.getOne("test-email");
        var expected = ReportUserProjection.builder()
                .email("test-email")
                .customer("customer")
                .product("customer.product")
                .build();

        assertThat(expected).isEqualToIgnoringGivenFields(userRepo, "processingTime");
    }

    @Test
    @DisplayName("Given a UserAddEvent with multiple products then  User Projection stored")
    public void givenUserAddedEventWithScopesThenUserCreated() {

        var user = User.builder().name("test-user").email("test-email")
                .groups(HashSet.of(new Group("customer.product")))
                .roles(HashSet.of(Role.parse("customer.product/payment:Read"), Role.parse("customer.product2/payment:Read")))
                .build();

        var event = new UserAddedEvent(user);
        var result = service.apply(event.getKey(), event);
        assertEquals(1, repository.count());

        var userRepo = repository.getOne("test-email");
        var expected = ReportUserProjection.builder()
                .email("test-email")
                        .customer("customer")
                        .product("customer.product2,customer.product")
                .build();

        assertThat(expected).isEqualToIgnoringGivenFields(userRepo, "processingTime");
    }


    @Test
    @DisplayName("Given a UserAddEvent with multiple customers then  User Projection stored")
    public void givenUserAddedEventWithCustomersThenUserCreated() {

        var user = User.builder().name("test-user").email("test-email")
                .groups(HashSet.of(new Group("customer.product")))
                .roles(HashSet.of(Role.parse("customer.product/payment:Read"), Role.parse("customer2.product2/payment:Read")))
                .build();

        var event = new UserAddedEvent(user);
        var result = service.apply(event.getKey(), event);
        assertEquals(1, repository.count());

        var userRepo = repository.getOne("test-email");
        var expected = ReportUserProjection.builder()
                .email("test-email")
                .customer("customer2,customer")
                .product("customer2.product2,customer.product")
                .build();

        assertThat(expected).isEqualToIgnoringGivenFields(userRepo, "processingTime");
    }

    @Test
    @DisplayName("Give a UserAddEvent with multiple scopes then User Project stored as comma separate list")
    public void givenUserWithMultipleScopesAddedEventThenUserCreated() {

        var user = User.builder().name("test-user").email("test-email")
                .groups(HashSet.of(new Group("customer")))
                .roles(HashSet.of(Role.parse("customer/payment:Read")))
                .build();

        var event = new UserAddedEvent(user);
        var result = service.apply(event.getKey(), event);

        assertEquals(1, repository.count());

        var userRepo = repository.getOne("test-email");
        var expected = ReportUserProjection.builder()
                .email("test-email")
                .customer("customer")
                .product(null)
                .processingTime(Instant.now())
                .build();

        assertThat(expected).isEqualToIgnoringGivenFields(userRepo, "processingTime");
    }

    @Test
    @DisplayName("Given a Super user then fields are created with null values")
    public void givenSuperUserThenUserCreated() {

        var user = User.builder().name("test-user").email("test-email")
                .groups(HashSet.of(new Group(".")))
                .roles(HashSet.of(Role.parse("./payments:Read")))
                .build();

        var event = new UserAddedEvent(user);
        var result = service.apply(event.getKey(), event);
        assertEquals(1, repository.count());

        var userRepo = repository.getOne("test-email");
        var expected = ReportUserProjection.builder()
                .email("test-email")
                .processingTime(Instant.now())
                .build();

        assertThat(expected).isEqualToIgnoringGivenFields(userRepo, "processingTime");
    }


    @Test
    @DisplayName("Given a User Updated Event then scopes updated")
    public void givenUserUpdateEventTheUserAcLUpdated() {

        var user = User.builder().name("test-user").email("test-email")
                .groups(HashSet.of(new Group("customer")))
                .roles(HashSet.of(Role.parse("customer.product/payment:Read")))
                .build();

        var event = new UserAddedEvent(user);
        var result = service.apply(event.getKey(), event);

        user = user.toBuilder() .groups(HashSet.of(new Group("customer2")))
                .roles(HashSet.of(Role.parse("customer2.product2/payment:Read")))
                .build();

        var eventUpdated = new UserRolesAssignedEvent(user);
        service.apply(eventUpdated.getKey(), eventUpdated);
        assertEquals(1, repository.count());

        var userRepo = repository.getOne("test-email");
        var expected = ReportUserProjection.builder()
                .email("test-email")
                .customer("customer2")
                .product("customer2.product2")
                .processingTime(Instant.now())
                .build();

        assertThat(expected).isEqualToIgnoringGivenFields(userRepo, "processingTime");
    }

    @Test
    @DisplayName("Given a User Deleted Event then user deleted")
    public void givenUserDeletedEventTheUserDeleted() {

        var user = User.builder().name("test-user").email("test-email")
                .groups(HashSet.of(new Group("customer")))
                .roles(HashSet.of(Role.parse("customer.product/payment:Read")))
                .build();

        var event = new UserAddedEvent(user);
        service.apply(event.getKey(), event);
        assertEquals(1, repository.count());

        var deleteEvent = new UserRemovedEvent("test-user", "test-email");
        service.apply(deleteEvent.getKey(), deleteEvent);
        assertEquals(0, repository.count());

    }

    @Configuration
    public static class TestConfiguration {
        @Bean
        public RepositoryMutatingStorageService<String, ReportUserProjection> service(ReportsRepository repository){
            return new ReportsStorageService(new MicrometerMetrics(new SimpleMeterRegistry()), repository);
        }
    }

}
