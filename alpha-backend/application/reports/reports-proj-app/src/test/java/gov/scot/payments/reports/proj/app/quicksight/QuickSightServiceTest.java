package gov.scot.payments.reports.proj.app.quicksight;

import gov.scot.payments.reports.proj.app.quicksight.config.QuickSightConfigFile;
import gov.scot.payments.reports.proj.app.quicksight.config.QuickSightDataSetConfig;
import io.vavr.collection.List;
import io.vavr.control.Option;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import software.amazon.awssdk.services.quicksight.model.DataSetSummary;
import software.amazon.awssdk.services.quicksight.model.InputColumn;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class QuickSightServiceTest {

    private QuickSightService service;
    private QuickSightWrapper mockWrapper;

    @BeforeEach
    void setUp() {

        mockWrapper = Mockito.mock(QuickSightWrapper.class);
        service = QuickSightService.builder()
                .datasourceName("testds")
                .aclDatasetName("aclds")
                .environmentName("testEnv")
                .quickSightWrapper(mockWrapper)
                .quickSightRoleArn("principalTestArn")
                .build();
    }

    @Test
    void createDataSetsFromConfigFileWhenNotExists() {


        QuickSightConfigFile dsConfig = createExampleConfig();

        when(mockWrapper.getDatasourceArnFromName("testds")).thenReturn("testds_arn");
        when(mockWrapper.getDatasetArnFromName("aclds")).thenReturn("aclds_arn");
        when(mockWrapper.getDataSetByName(any())).thenReturn(Option.none());

        var createInfo = QuickSightDataSetInfo.builder()
                .datasetName("testEnv_dataset_1")
                .dataSourceArn("testds_arn")
                .aclDatasetArn("aclds_arn")
                .principalArn("principalTestArn")
                .sqlQuery("select * from ds1")
                .columns(List.of(InputColumn.builder()
                        .name("id")
                        .type("STRING")
                        .build()))
                .build();


        when(mockWrapper.createDataSetFromSqlQueryTable(any())).thenReturn("testds_id");
        var response = service.createDataSetsFromConfigFile(dsConfig);
        assertEquals("testds_id", response.get(0));
        verify(mockWrapper).getDataSetByName("testEnv_dataset_1");
        var capture = ArgumentCaptor.forClass(QuickSightDataSetInfo.class);
        verify(mockWrapper).createDataSetFromSqlQueryTable(capture.capture());
        assertThat(createInfo).isEqualToComparingFieldByField(capture.getValue());

    }

    private QuickSightConfigFile createExampleConfig() {
        var column = new QuickSightDataSetConfig.InputColumnConfig("id", "STRING");

        var dataset1 = QuickSightDataSetConfig.builder()
                .name("dataset_1")
                .query("select * from ds1")
                .columns(List.of(column).toJavaList())
                .build();

        return QuickSightConfigFile.builder()
                .datasets(List.of(dataset1).toJavaList())
                .build();
    }

    @Test
    void createDataSetFromConfigIfNotExists() {

        QuickSightConfigFile dsConfig = createExampleConfig();

        when(mockWrapper.getDatasourceArnFromName("testds")).thenReturn("testds_arn");

        var summary = DataSetSummary.builder()
                .arn("testArn")
                .dataSetId("testds_id")
                .build();

        when(mockWrapper.getDataSetByName("testEnv_dataset_1")).thenReturn(Option.of(summary));

        var response = service.createDataSetsFromConfigFile(dsConfig);
        assertEquals("testds_id", response.get(0));
        verify(mockWrapper, times(0)).createDataSetFromSqlQueryTable(any());

    }
}