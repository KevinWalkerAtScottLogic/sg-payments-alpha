package gov.scot.payments.reports.proj.app.quicksight;

import io.vavr.collection.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import software.amazon.awssdk.services.quicksight.QuickSightClient;
import software.amazon.awssdk.services.quicksight.model.CreateDataSetRequest;
import software.amazon.awssdk.services.quicksight.model.CreateDataSetResponse;
import software.amazon.awssdk.services.quicksight.model.InputColumn;
import software.amazon.awssdk.services.quicksight.model.RowLevelPermissionPolicy;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class QuickSightWrapperTest {

    QuickSightClient quickSightClient;
    QuickSightWrapper quickSightWrapper;

    @BeforeEach
    void setUp() {
        quickSightClient = Mockito.mock(QuickSightClient.class);
        quickSightWrapper = QuickSightWrapper.builder()
                .awsAccountId("testid")
                .quickSightClient(quickSightClient)
                .build();
        when(quickSightClient.createDataSet((CreateDataSetRequest)any()))
                .thenReturn(CreateDataSetResponse.builder()
                        .arn("createdArn")
                        .dataSetId("createdId")
                        .build());
    }


    @Test
    void createDataSetFromSqlQueryTable() {

        var columns = List.of(InputColumn.builder().type("STRING").name("id").build());
        var dsInfo = QuickSightDataSetInfo.builder()
                .sqlQuery("select * from query")
                .principalArn("testPrincipalArn")
                .dataSourceArn("testDsArn")
                .datasetName("mydataset")
                .aclDatasetArn("testAclArn")
                .columns(columns)
                .build();

        quickSightWrapper.createDataSetFromSqlQueryTable(dsInfo);
        var captor =  ArgumentCaptor.forClass(CreateDataSetRequest.class);
        verify(quickSightClient).createDataSet(captor.capture());
        var request = captor.getValue();

        assertEquals(request.awsAccountId(), "testid");
        assertEquals(request.dataSetId(), "mydataset");
        assertEquals(request.name(), "mydataset");
        assertEquals("testPrincipalArn", request.permissions().get(0).principal());
        assertEquals("testAclArn", request.rowLevelPermissionDataSet().arn());
        assertEquals(RowLevelPermissionPolicy.GRANT_ACCESS, request.rowLevelPermissionDataSet().permissionPolicy());

        var mapSet = request.physicalTableMap().entrySet();
        assertEquals(1, mapSet.size());
        var entry = mapSet.iterator().next();
        assertEquals("mydataset", entry.getKey());
        var tableSql = entry.getValue().customSql();
        assertEquals("select * from query", tableSql.sqlQuery());
        assertEquals("testDsArn", tableSql.dataSourceArn());
        assertEquals(columns.toJavaList(), tableSql.columns());



    }


    @Test
    void verifyNameSantisedWhenDataSetIsInvalid() {

        var columns = List.of(InputColumn.builder().type("STRING").name("id").build());
        var dsInfo = QuickSightDataSetInfo.builder()
                .sqlQuery("select * from query")
                .principalArn("testArn")
                .dataSourceArn("testDsArn")
                .datasetName("my_invalid_____dataset_name")
                .columns(columns)
                .build();

        quickSightWrapper.createDataSetFromSqlQueryTable(dsInfo);
        var captor =  ArgumentCaptor.forClass(CreateDataSetRequest.class);
        verify(quickSightClient).createDataSet(captor.capture());
        var request = captor.getValue();

        assertEquals(request.dataSetId(), "my_invalid_____dataset_name");
        assertEquals(request.name(), "my_invalid_____dataset_name");
        var mapSet = request.physicalTableMap().entrySet();
        assertEquals(1, mapSet.size());
        var entry = mapSet.iterator().next();
        assertEquals("myinvaliddatasetname", entry.getKey());

    }
}