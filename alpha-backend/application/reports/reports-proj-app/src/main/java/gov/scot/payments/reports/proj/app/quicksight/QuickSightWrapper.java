package gov.scot.payments.reports.proj.app.quicksight;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.Builder;
import lombok.NonNull;
import software.amazon.awssdk.services.quicksight.QuickSightClient;
import software.amazon.awssdk.services.quicksight.model.*;

@Builder
public class QuickSightWrapper {

    @NonNull private final String awsAccountId;
    @NonNull private final QuickSightClient quickSightClient;

    private static final int MAX_KEY_LENGTH = 54;

    private final static List<String> DATASET_PERMISSIONS = List.of("quicksight:DescribeDataSet",
            "quicksight:DescribeDataSetPermissions",
            "quicksight:PassDataSet",
            "quicksight:DescribeIngestion",
            "quicksight:ListIngestions");

    public String getDatasourceArnFromName(String datasourceName) {

        var dataSourceResponse = quickSightClient.describeDataSource(DescribeDataSourceRequest.builder()
                .awsAccountId(awsAccountId)
                .dataSourceId(datasourceName)
                .build());

        return dataSourceResponse.dataSource().arn();

    }

    public String getDatasetArnFromName(String datasetName) {

        var dataSourceResponse = quickSightClient.describeDataSet(DescribeDataSetRequest.builder()
                .awsAccountId(awsAccountId)
                .dataSetId(datasetName)
                .build());

        return dataSourceResponse.dataSet().arn();

    }

    public Option<DataSetSummary> getDataSetByName(String datasetName) {
        var dataSourceResponse = quickSightClient.listDataSets(ListDataSetsRequest.builder()
                .awsAccountId(awsAccountId)
                .build());

        var summaries = List.ofAll(dataSourceResponse.dataSetSummaries());

        return Option.of(summaries
                .filter(s -> s.name().equals(datasetName)).getOrNull());
    }


    public String santiseNameForMapKey(String datasetName){

        var newDatasetName = datasetName.replaceAll("_", "");
        newDatasetName = newDatasetName.replaceAll(" ", "");
        if(newDatasetName.length() >= MAX_KEY_LENGTH){
            newDatasetName = newDatasetName.substring(0, MAX_KEY_LENGTH-1);
        }
        return newDatasetName;

    }

    public String createDataSetFromSqlQueryTable(QuickSightDataSetInfo info) {

        var table = PhysicalTable.builder()
                .customSql(CustomSql.builder()
                        .dataSourceArn(info.getDataSourceArn())
                        .sqlQuery(info.getSqlQuery())
                        .columns(info.getColumns().toJavaList())
                        .name(info.getDatasetName())
                        .build())
                .build();

        var tableMap = HashMap.of(santiseNameForMapKey(info.getDatasetName()), table);


        var response = quickSightClient.createDataSet(CreateDataSetRequest.builder()
                .awsAccountId(awsAccountId)
                .name(info.getDatasetName())
                .dataSetId(info.getDatasetName())
                .physicalTableMap(tableMap.toJavaMap())
                .importMode(DataSetImportMode.DIRECT_QUERY)
                .permissions(ResourcePermission.builder()
                        .principal(info.getPrincipalArn())
                        .actions(DATASET_PERMISSIONS.toJavaList())
                        .build())
                .rowLevelPermissionDataSet(RowLevelPermissionDataSet.builder()
                        .arn(info.getAclDatasetArn())
                        .permissionPolicy(RowLevelPermissionPolicy.GRANT_ACCESS)
                        .build())
                .build());


        return response.dataSetId();

    }

}
