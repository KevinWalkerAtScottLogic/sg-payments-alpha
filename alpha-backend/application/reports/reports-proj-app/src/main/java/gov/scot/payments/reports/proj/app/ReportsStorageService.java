package gov.scot.payments.reports.proj.app;

import gov.scot.payments.application.component.projector.springdata.ProjectionRepository;
import gov.scot.payments.application.component.projector.springdata.RepositoryMutatingStorageService;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.user.User;
import gov.scot.payments.reports.model.ReportUserProjection;
import gov.scot.payments.reports.model.ScopedReference;
import gov.scot.payments.users.model.UserAddedEvent;
import gov.scot.payments.users.model.UserRemovedEvent;
import gov.scot.payments.users.model.UserRolesAssignedEvent;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;

@Slf4j
public class ReportsStorageService extends RepositoryMutatingStorageService<String, ReportUserProjection>{

    public ReportsStorageService(final Metrics metrics, final ProjectionRepository<String, ReportUserProjection> repository) {
        super(metrics, repository);
    }

    @Override
    protected boolean shouldDelete(Event event) {
        if(UserRemovedEvent.class.isAssignableFrom(event.getClass())){
            return true;
        }
        return false;
    }

    @Override
    protected void idExtractors(PatternMatcher.Builder2<Event,String,String> builder) {
        builder.match2(UserAddedEvent.class, (e, k) -> e.getUser().getEmail())
                .match2(UserRemovedEvent.class, (e, k) -> e.getEmail())
                .match2(UserRolesAssignedEvent.class, (e, k) -> e.getUser().getEmail());
    }

    @Override
    protected void createHandlers(PatternMatcher.Builder<Event, ReportUserProjection> builder) {
        builder.match(UserAddedEvent.class, e -> createProjectionFromUser(e.getUser()));
    }

    @Override
    protected void updateHandlers(PatternMatcher.Builder2<Event, ReportUserProjection, ReportUserProjection> builder) {
        builder.match2(UserRolesAssignedEvent.class, (e, s) -> createProjectionFromUser(e.getUser()));
    }

    public ReportUserProjection createProjectionFromUser(User user){

        log.info("Creating projection from user {}", user.getName());

        var scopes = ScopedReference.fromUser(user);
        return ReportUserProjection.builder()
                .email(user.getEmail())
                .processingTime(Instant.now())
                .customer(scopes.getCustomers())
                .product(scopes.getProducts())
                .build();
    }


}
