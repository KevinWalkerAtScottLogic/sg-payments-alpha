package gov.scot.payments.reports.proj.app.quicksight;

import gov.scot.payments.reports.proj.app.quicksight.config.QuickSightConfigException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.reactive.context.ReactiveWebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.io.Resource;

import java.io.IOException;


@Slf4j
public class QuickSightApplicationListener implements ApplicationListener<ReactiveWebServerInitializedEvent> {

    private QuickSightService service;
    private final Resource configFile;
    private final boolean createDataSets;

    public QuickSightApplicationListener(QuickSightService service, Resource configFile, boolean createDataSets){
        this.service = service;
        this.configFile = configFile;
        this.createDataSets = createDataSets;
    }


    @Override
    public void onApplicationEvent(ReactiveWebServerInitializedEvent event) {

        if(!createDataSets){
            log.info("No Quicksight datasets will be created.");
            return;
        }

        try {
            var file = configFile.getFile();
            var config = QuickSightService.readConfigFile(file);
            log.info("Found config {} ", config);
            var datasetIds = service.createDataSetsFromConfigFile(config);
            log.info("Created datasets {}", datasetIds);
        } catch (IOException e) {
            throw new QuickSightConfigException("Unable to load config file for building Quicksight Datasources");
        }

    }
}
