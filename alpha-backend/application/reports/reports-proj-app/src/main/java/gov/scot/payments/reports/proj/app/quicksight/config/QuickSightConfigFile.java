package gov.scot.payments.reports.proj.app.quicksight.config;

import lombok.*;

import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
public class QuickSightConfigFile {

    private List<QuickSightDataSetConfig> datasets;

}
