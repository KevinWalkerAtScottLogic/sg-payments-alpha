package gov.scot.payments.reports.proj.app.quicksight.config;

import lombok.*;
import software.amazon.awssdk.services.quicksight.model.InputColumn;

import java.util.List;


@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class QuickSightDataSetConfig {

    private String name;
    private String query;
    private List<InputColumnConfig> columns;

    @Getter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class InputColumnConfig {

        private String name;
        private String type;

        public InputColumn toInputColumn(){
            return InputColumn.builder()
                    .name(name)
                    .type(type)
                    .build();
        }

    }


}
