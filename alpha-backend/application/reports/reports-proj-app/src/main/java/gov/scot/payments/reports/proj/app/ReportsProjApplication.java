package gov.scot.payments.reports.proj.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.component.projector.springdata.RepositoryMutatingStorageService;
import gov.scot.payments.application.component.projector.springdata.jpa.JpaMutatingProjectorApplication;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.reports.model.ReportUserProjection;
import gov.scot.payments.reports.proj.app.quicksight.QuickSightApplicationListener;
import gov.scot.payments.reports.proj.app.quicksight.QuickSightService;
import gov.scot.payments.reports.proj.app.quicksight.QuickSightWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.quicksight.QuickSightClient;

@EnableAutoConfiguration(exclude = {HypermediaAutoConfiguration.class})
@EnableJpaRepositories(basePackageClasses = ReportsRepository.class)
@EntityScan(basePackageClasses = ReportUserProjection.class)
@ApplicationComponent
@Slf4j
public class ReportsProjApplication extends JpaMutatingProjectorApplication<String, ReportUserProjection> {

        /*
    @Bean
    public StreamJoiner<Event,Event,Event> joiner(SchemaRegistryClient confluentSchemaRegistryClient){
        HeaderPreservingCompositeNonNativeSerde<Event> eventSerde = valueSerde(Event.class);
        return StreamJoiner.<Event, Event, Event>builder()
            .repartitionTopicName()
            .joinWindow()
            .joiner()
            .rekeyFunction()
            .leftValueSerde(eventSerde)
            .rightValueSerde(eventSerde)
            .stateSerde(stateSerde(confluentSchemaRegistryClient,SerializedMessage.class))
            .build();
    }
     */

    @Override
    protected Specification<ReportUserProjection> aclCheck(Subject subject) {
        return null;
    }

    @Override
    protected Class<ReportUserProjection> projectionClass() {
        return null;
    }


    @Bean
    public QuickSightClient cognitoClient(
              @Value("${cloud.aws.region.static}") String region
            , @Value("${cloud.aws.credentials.accessKey}") String accessKey
            , @Value("${cloud.aws.credentials.secretKey}") String secretKey) {

        AwsCredentials credentials = AwsBasicCredentials.create(accessKey,secretKey);
        AwsCredentialsProvider credentialsProvider = StaticCredentialsProvider.create(credentials);

        var builder = QuickSightClient.builder()
                .credentialsProvider(credentialsProvider)
                .region(Region.EU_WEST_2);
        return builder.build();
    }

    @Bean
    public QuickSightWrapper quickSightWrapper(
             @Value("${cloud.aws.credentials.accountId}") String accountId
            , @Value("${ENVIRONMENT}") String environment
            , QuickSightClient quickSightClient){

        return QuickSightWrapper.builder()
                .awsAccountId(accountId)
                .quickSightClient(quickSightClient)
                .build();
    }

    @Bean
    public QuickSightService quickSightService(QuickSightWrapper wrapper
                    , @Value("${quicksight-datasource.name}") String datasourceName
                    , @Value("${quicksight-acl.name}") String aclDatasetName
                    , @Value("${quicksight.role-arn}") String roleArn
                    , @Value("${ENVIRONMENT}") String environment) {

        return QuickSightService.builder()
                .quickSightWrapper(wrapper)
                .datasourceName(datasourceName)
                .aclDatasetName(aclDatasetName)
                .environmentName(environment)
                .quickSightRoleArn(roleArn)
                .build();
    }

    @Bean
    public QuickSightApplicationListener appStartListener(QuickSightService service
            , @Value("${quicksight.create-datasets}") boolean createDataSets
            , @Value("classpath:quicksight/datasource-config.yaml") Resource configFile) {

        return new QuickSightApplicationListener(service, configFile, createDataSets);
    }

    @Bean
    public RepositoryMutatingStorageService<String, ReportUserProjection> handler(Metrics metrics, ReportsRepository repository){
        return new ReportsStorageService(metrics,repository);
    }


    public static void main(String[] args){
        BaseApplication.run(ReportsProjApplication.class,args);
    }


}