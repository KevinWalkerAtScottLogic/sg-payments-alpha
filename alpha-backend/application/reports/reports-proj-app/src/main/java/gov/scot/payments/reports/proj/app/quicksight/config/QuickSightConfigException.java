package gov.scot.payments.reports.proj.app.quicksight.config;

public class QuickSightConfigException extends RuntimeException {
    public QuickSightConfigException(String s) {
        super(s);
    }
}
