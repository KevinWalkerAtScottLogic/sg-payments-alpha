package gov.scot.payments.reports.proj.app;

import gov.scot.payments.application.component.projector.springdata.jpa.JpaProjectionRepository;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.reports.model.ReportUserProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ReportsRepository extends JpaProjectionRepository<String, ReportUserProjection>{

    //TODO: apply acl to queries here
    @Query("from ReportUserProjection r")
    Page<ReportUserProjection> findAll(Pageable pageable, Subject subject);

    //TODO: apply acl to queries here
    @Query("from ReportUserProjection r where r.id = ?1")
    Optional<ReportUserProjection> findById(String id, Subject subject);

}
