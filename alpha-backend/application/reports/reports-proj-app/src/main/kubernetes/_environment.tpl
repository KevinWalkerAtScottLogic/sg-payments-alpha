{{- define "reports-reports-proj-app.env" -}}
- name: AWS_QUICKSIGHT_ACCOUNT_ID
  valueFrom:
    secretKeyRef:
      name: aws-credentials
      key: aws_account_id
- name: QUICKSIGHT_ROLE_ARN
  valueFrom:
     configMapKeyRef:
      name: quicksight-info
      key: role_arn
- name: SPRING_DATASOURCE_URL
  valueFrom:
    secretKeyRef:
      name: rds-credentials
      key: rds_url
- name: SPRING_DATASOURCE_PASSWORD
  valueFrom:
    secretKeyRef:
      name: rds-credentials
      key: rds_password
- name: SPRING_DATASOURCE_USERNAME
  valueFrom:
    secretKeyRef:
      name: rds-credentials
      key: rds_user_name
- name: CREATE_QUICKSIGHT_DATASETS
  value: {{ .Values.${projectValuesName}.createDataSets | quote }}
{{- end -}}