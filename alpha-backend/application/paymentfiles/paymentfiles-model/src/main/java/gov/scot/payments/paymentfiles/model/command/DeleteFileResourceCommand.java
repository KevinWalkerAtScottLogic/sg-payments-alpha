package gov.scot.payments.paymentfiles.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "paymentfiles", type = "deleteFileResource")
@NoArgsConstructor
public class DeleteFileResourceCommand extends CommandImpl implements HasKey<String> {

    @NonNull private CompositeReference product;

    public DeleteFileResourceCommand(CompositeReference product, boolean reply){
        super(reply);
        this.product = product;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return product.toString();
    }
}
