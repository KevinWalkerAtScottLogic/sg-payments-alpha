package gov.scot.payments.paymentfiles.pm.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.application.component.processmanager.EventTransformer;
import gov.scot.payments.application.component.processmanager.ProcessManagerApplication;
import gov.scot.payments.customers.model.aggregate.OutboundConfiguration;
import gov.scot.payments.customers.model.event.*;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.paymentfiles.model.command.CreateFileResourceCommand;
import gov.scot.payments.paymentfiles.model.command.DeleteFileResourceCommand;
import io.vavr.collection.List;
import io.vavr.control.Option;
import org.apache.kafka.common.header.Headers;
import org.springframework.context.annotation.Bean;

import java.util.function.Predicate;


@ApplicationComponent
public class PaymentfilesPMApplication extends ProcessManagerApplication {

    @Bean
    public EventTransformer eventTransformFunction() {
        return EventTransformer.patternMatching(b ->
                b.match(CreateCustomerApprovedEvent.class, e -> List.of(new CreateFileResourceCommand(new CompositeReference(e.getKey(), null), false)))
                        .match(DeleteCustomerApprovedEvent.class, e -> List.of(new DeleteFileResourceCommand(new CompositeReference(e.getKey(), null), false)))
                        .match(CreateProductApprovedEvent.class, this::handleCreateProduct)
                        .match(DeleteProductApprovedEvent.class, this::handleDeleteProduct)
                        .match(ModifyProductApprovedEvent.class, this::handleModifyProduct)
                .orElse(List.empty())
        );
    }

    private List<Command> handleModifyProduct(ModifyProductApprovedEvent e) {
        boolean hasFileImport= e.getCarriedState().getProduct(e.getProductId())
                .flatMap(p -> Option.of(p.getProductConfiguration().getOutboundConfiguration()))
                .map(OutboundConfiguration::isFileImports)
                .getOrElse(false);
        if(!hasFileImport){
            return List.of(new DeleteFileResourceCommand(new CompositeReference(e.getKey(), e.getProductId()), false));
        }
        return List.of(new CreateFileResourceCommand(new CompositeReference(e.getKey(), e.getProductId()), false));
    }

    private List<Command> handleDeleteProduct(DeleteProductApprovedEvent e) {
        boolean hasFileImport= e.getCarriedState().getProduct(e.getProductId())
                .flatMap(p -> Option.of(p.getProductConfiguration().getOutboundConfiguration()))
                .map(OutboundConfiguration::isFileImports)
                .getOrElse(false);
        if(!hasFileImport){
            return List.empty();
        }
        return List.of(new DeleteFileResourceCommand(new CompositeReference(e.getKey(), e.getProductId()), false));
    }

    private List<Command> handleCreateProduct(CreateProductApprovedEvent e) {
        boolean hasFileImport= e.getCarriedState().getProduct(e.getProductId())
                .flatMap(p -> Option.of(p.getProductConfiguration().getOutboundConfiguration()))
                .map(OutboundConfiguration::isFileImports)
                .getOrElse(false);
        if(!hasFileImport){
            return List.empty();
        }
        return List.of(new CreateFileResourceCommand(new CompositeReference(e.getKey(), e.getProductId()), false));
    }

    @Bean
    public Predicate<Headers> externalEventHeaderFilter(MessageTypeRegistry messageTypeRegistry){
        return h -> messageTypeRegistry.matchesType(h
                , CreateProductApprovedEvent.class
                ,DeleteProductApprovedEvent.class
                ,ModifyProductApprovedEvent.class
                ,CreateCustomerApprovedEvent.class
                ,DeleteCustomerApprovedEvent.class);
    }


    public static void main(String[] args) {
        BaseApplication.run(PaymentfilesPMApplication.class, args);
    }
}