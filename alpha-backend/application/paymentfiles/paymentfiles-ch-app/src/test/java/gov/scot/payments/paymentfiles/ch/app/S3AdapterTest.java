package gov.scot.payments.paymentfiles.ch.app;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.assertj.core.api.AssertionsForClassTypes.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class S3AdapterTest {
    private S3Client s3Client;
    private String branchName;
    private String s3BucketName;
    private String region;
    private S3Adapter s3Adapter;

    @BeforeEach
    public void setUp() {
        s3Client = mock(S3Client.class);
        branchName = "branchName";
        s3BucketName = "bucketname";
        region = "region";
        s3Adapter = new S3Adapter(s3Client, branchName, s3BucketName, region);
    }

    @Test
    @DisplayName("If deleting files fails, then an exception containing the files that were not deleted is returned")
    public void deleteFilesCallExceptionContainsNonDeletedFiles() {
        String deletedPath = "abc/def.file";
        String nonDeletedPath = "abcd/efgh.csv";
        String deletedPath2 = "ab/ab/c.txt";

        List<S3Object> filesToDelete = new ArrayList<>();
        filesToDelete.add(S3Object.builder().key(deletedPath).build());
        filesToDelete.add(S3Object.builder().key(nonDeletedPath).build());
        filesToDelete.add(S3Object.builder().key(deletedPath2).build());

        when(s3Client.deleteObject(any(DeleteObjectRequest.class)))
                .thenReturn(DeleteObjectResponse.builder().build())
                .thenThrow(SdkClientException.builder().build())
                .thenReturn(DeleteObjectResponse.builder().build());
        try {
            s3Adapter.deleteS3Contents(filesToDelete);
            fail("Expected exception");
        } catch (UnableToDeleteException e) {
            assertEquals(1, e.getFolderPaths().size());
            assertTrue(e.getFolderPaths().contains(nonDeletedPath));
        }
    }

    @Test
    @DisplayName("If deleting folders fails, then an exception containing the folders that were not deleted is returned")
    public void deletingFoldersCallExceptionContainsNonDeletedFiles() {
        String deletedPath = "abc/def/file/";
        String nonDeletedPath = "abcd/efgh/";
        String nonDeletedPath2 = "ab/ab/c/";

        List<S3Object> filesToDelete = new ArrayList<>();
        filesToDelete.add(S3Object.builder().key(deletedPath).build());
        filesToDelete.add(S3Object.builder().key(nonDeletedPath).build());
        filesToDelete.add(S3Object.builder().key(nonDeletedPath2).build());

        when(s3Client.deleteObject(any(DeleteObjectRequest.class)))
                .thenReturn(DeleteObjectResponse.builder().build())
                .thenThrow(SdkClientException.builder().build())
                .thenThrow(SdkClientException.builder().build());
        try {
            s3Adapter.deleteS3Contents(filesToDelete);
            fail("Expected exception");
        } catch (UnableToDeleteException e) {
            assertEquals(2, e.getFolderPaths().size());
            assertTrue(e.getFolderPaths().contains(nonDeletedPath));
            assertTrue(e.getFolderPaths().contains(nonDeletedPath2));
        }
    }

    @Test
    @DisplayName("No Exception is thrown if all folders are deleted successfully")
    public void doNotThrowExceptionIfAllFoldersAreDeleted() {
        String folderPath = "test/path/";
        String folderPathTwo = "test/path/";

        List<S3Object> foldersToDelete = new ArrayList<>();
        foldersToDelete.add(S3Object.builder().key(folderPath).build());
        foldersToDelete.add(S3Object.builder().key(folderPathTwo).build());

        when(s3Client.deleteObject(any(DeleteObjectRequest.class)))
                .thenReturn(DeleteObjectResponse.builder().build());

        s3Adapter.deleteS3Contents(foldersToDelete);
    }

    @Test
    @DisplayName("getObjectsInS3Folder will call listNextBatchOfObjects until results are no longer truncated")
    public void allTruncatedResultsAreFetchedAndCombinedCorrectly() {
        List<S3Object> filesToDelete = new ArrayList<>();
        filesToDelete.add(S3Object.builder().build());
        filesToDelete.add(S3Object.builder().build());
        filesToDelete.add(S3Object.builder().build());

        var listObjectsResponse = ListObjectsResponse.builder().isTruncated(false).contents(filesToDelete).build();
        var listObjectsResponseTruncated = ListObjectsResponse.builder().contents(filesToDelete).isTruncated(true).build();

        when(s3Client.listObjects(any(ListObjectsRequest.class))).thenReturn(listObjectsResponseTruncated,listObjectsResponseTruncated, listObjectsResponse);

        List<S3Object> s3Objects = s3Adapter.getObjectsInS3Folder("test/path");
        assertEquals(9, s3Objects.size());
        verify(s3Client, times(3)).listObjects(any(ListObjectsRequest.class));
    }
}
