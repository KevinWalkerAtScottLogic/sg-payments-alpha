package gov.scot.payments.paymentfiles.ch.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.model.CommandFailureInfo;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.GenericErrorEvent;
import gov.scot.payments.paymentfiles.model.command.CreateFileResourceCommand;
import gov.scot.payments.paymentfiles.model.command.DeleteFileResourceCommand;
import gov.scot.payments.paymentfiles.model.event.FileResourceCreatedEvent;
import gov.scot.payments.paymentfiles.model.event.FileResourceDeletedEvent;
import gov.scot.payments.paymentfiles.model.event.FileUploadedEvent;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.vavr.collection.List;
import org.apache.kafka.streams.KafkaStreams;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.messaging.MessagingAutoConfiguration;
import org.springframework.context.annotation.Import;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ApplicationIntegrationTest(classes = {PaymentfilesCHApplicationIntegrationTest.TestApplication.class}
        , componentType = ApplicationIntegrationTest.ComponentType.COMMAND_HANDLER
        , properties = {"cloud.aws.credentials.accessKey=test"
        , "cloud.aws.credentials.secretKey=test"
        , "cloud.aws.region.static=eu-west-2"
        , "file.event.queue=test"
        , "FILE_BUCKET_NAME=bucketName"})
@EnableAutoConfiguration(exclude = {ContextStackAutoConfiguration.class, MessagingAutoConfiguration.class})
public class PaymentfilesCHApplicationIntegrationTest {

    @MockBean
    S3Client s3Client;

    @Autowired
    S3EventListener s3EventListener;

    @Autowired
    S3Adapter s3Adapter;

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, EmbeddedBrokerClient brokerClient, @Value("${namespace}") String namespace) {
        brokerClient.getBroker().addTopicsIfNotExists(namespace + "-paymentfiles-command-errors");
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
    }

    @Test
    public void testCreateFileResourceSuccess(EmbeddedBrokerClient brokerClient) throws MalformedURLException {
        String customerName = "customerName";
        String productName = "productName";
        var compRef =new CompositeReference(customerName, productName);

        when(s3Client.putObject(any(PutObjectRequest.class), any(RequestBody.class))).thenReturn(PutObjectResponse.builder().build());
        var createFileResourceCommand = new CreateFileResourceCommand(compRef, true);
        brokerClient.sendToDestination("commands", List.of(createFileResourceCommand));

        List<FileResourceCreatedEvent> createdEvents = brokerClient.readAllFromDestination("events", FileResourceCreatedEvent.class);
        assertEquals(1, createdEvents.size());
        assertEquals(productName, createdEvents.head().getProduct().getComponent1());
        assertEquals(customerName, createdEvents.head().getProduct().getComponent0());
        String expectedUrl = String.format("%s/%s/%s/%s/", s3Adapter.getBucketUrl(), s3Adapter.getBranchName(), customerName, productName);
        assertEquals(new URL(expectedUrl), createdEvents.head().getPath());
    }

    @Test
    public void testCreateFileResourceSuccessWithoutProduct(EmbeddedBrokerClient brokerClient) throws MalformedURLException {
        String customerName = "customerName";
        String productName = null;
        var compRef =new CompositeReference(customerName, productName);

        when(s3Client.putObject(any(PutObjectRequest.class), any(RequestBody.class))).thenReturn(PutObjectResponse.builder().build());
        var createFileResourceCommand = new CreateFileResourceCommand(compRef, true);
        brokerClient.sendToDestination("commands", List.of(createFileResourceCommand));

        List<FileResourceCreatedEvent> createdEvents = brokerClient.readAllFromDestination("events", FileResourceCreatedEvent.class);
        assertEquals(1, createdEvents.size());
        assertEquals(productName, createdEvents.head().getProduct().getComponent1());
        assertEquals(customerName, createdEvents.head().getProduct().getComponent0());
        String expectedUrl = String.format("%s/%s/%s/", s3Adapter.getBucketUrl(), s3Adapter.getBranchName(), customerName);
        assertEquals(new URL(expectedUrl), createdEvents.head().getPath());
    }

    @Test
    public void testCreateFileResourceFailure(EmbeddedBrokerClient brokerClient) throws MalformedURLException {
        String customerName = "customerName";
        String productName = "productName";
        var compRef =new CompositeReference(customerName, productName);

        String expectedKey = String.format("%s.%s", customerName, productName);

        when(s3Client.putObject(any(PutObjectRequest.class), any(RequestBody.class))).thenThrow(SdkClientException.builder().build());

        var createFileResourceCommand = new CreateFileResourceCommand(compRef, true);
        brokerClient.sendToDestination("commands", List.of(createFileResourceCommand));
        List<GenericErrorEvent> errorEvents = brokerClient.readAllFromDestination("events", GenericErrorEvent.class);
        CommandFailureInfo commandFailureInfo = errorEvents.head().getError();
        assertEquals(1, errorEvents.size());
        assertEquals(expectedKey, errorEvents.head().getKey());
        assertEquals("UnableToAddException", commandFailureInfo.getErrorType());
    }

    @Test
    public void testDeleteFileResourceSuccess(EmbeddedBrokerClient brokerClient) throws MalformedURLException {
        String customerName = "customerName";
        String productName = "productName";
        var compRef =new CompositeReference(customerName, productName);

        var listObjectResponse = ListObjectsResponse.builder()
                .isTruncated(false)
                .contents(Collections.emptyList())
                .build();

        when(s3Client.listObjects(any(ListObjectsRequest.class))).thenReturn(listObjectResponse);
        when(s3Client.deleteObject(any(DeleteObjectRequest.class))).thenReturn(DeleteObjectResponse.builder().build());

        var deleteFileResourceCommand = new DeleteFileResourceCommand(compRef, true);
        brokerClient.sendToDestination("commands", List.of(deleteFileResourceCommand));

        List<FileResourceDeletedEvent> createdEvents = brokerClient.readAllFromDestination("events", FileResourceDeletedEvent.class);
        assertEquals(1, createdEvents.size());
        assertEquals(productName, createdEvents.head().getProduct().getComponent1());
        assertEquals(customerName, createdEvents.head().getProduct().getComponent0());
        String expectedUrl = String.format("%s/%s/%s/%s/", s3Adapter.getBucketUrl(), s3Adapter.getBranchName(), customerName, productName);
        assertEquals(new URL(expectedUrl), createdEvents.head().getPath());
    }

    @Test
    public void testDeleteFileResourceSuccessWithoutProduct(EmbeddedBrokerClient brokerClient) throws MalformedURLException {
        String customerName = "customerName";
        String productName = null;
        var compRef =new CompositeReference(customerName, productName);

        var listObjectResponse = ListObjectsResponse.builder()
                .isTruncated(false)
                .contents(Collections.emptyList())
                .build();

        when(s3Client.listObjects(any(ListObjectsRequest.class))).thenReturn(listObjectResponse);
        when(s3Client.deleteObject(any(DeleteObjectRequest.class))).thenReturn(DeleteObjectResponse.builder().build());

        var deleteFileResourceCommand = new DeleteFileResourceCommand(compRef, true);
        brokerClient.sendToDestination("commands", List.of(deleteFileResourceCommand));

        List<FileResourceDeletedEvent> createdEvents = brokerClient.readAllFromDestination("events", FileResourceDeletedEvent.class);
        assertEquals(1, createdEvents.size());
        assertEquals(productName, createdEvents.head().getProduct().getComponent1());
        assertEquals(customerName, createdEvents.head().getProduct().getComponent0());
        String expectedUrl = String.format("%s/%s/%s/", s3Adapter.getBucketUrl(), s3Adapter.getBranchName(), customerName);
        assertEquals(new URL(expectedUrl), createdEvents.head().getPath());
    }

    @Test
    public void testDeleteFileResourceFailure(EmbeddedBrokerClient brokerClient) throws MalformedURLException {
        String customerName = "customerName";
        String productName = "productName";
        var compRef =new CompositeReference(customerName, productName);
        String expectedKey = String.format("%s.%s", customerName, productName);
        var s3Object = S3Object.builder().key("testKey").build();

        var listObjectResponse = ListObjectsResponse.builder()
                .isTruncated(false)
                .contents(Collections.singletonList(s3Object))
                .build();

        when(s3Client.listObjects(any(ListObjectsRequest.class))).thenReturn(listObjectResponse);
        when(s3Client.deleteObject(any(DeleteObjectRequest.class))).thenThrow(SdkClientException.builder().build());

        var deleteFileResourceCommand = new DeleteFileResourceCommand(compRef, true);
        brokerClient.sendToDestination("commands", List.of(deleteFileResourceCommand));

        List<GenericErrorEvent> errorEvents = brokerClient.readAllFromDestination("events", GenericErrorEvent.class);
        CommandFailureInfo commandFailureInfo = errorEvents.head().getError();
        assertEquals(1, errorEvents.size());
        assertEquals(expectedKey, errorEvents.head().getKey());
        assertEquals("UnableToDeleteException", commandFailureInfo.getErrorType());
    }

    @Test
    public void testS3ListenerEmitsCorrectEvent(EmbeddedBrokerClient brokerClient) throws InterruptedException {
        String type = "ObjectCreated:CompleteMultipartUpload";
        String s3Bucket = "bucketName";
        String branchName = "dummy-branch-name";
        String fileName = "dummyFileName";
        String productId = "dummyProductId";
        String customerId = "dummyCustomerId";
        String folderAndFile = String.format("%s/%s/%s/%s", branchName, customerId, productId, fileName);
        var s3Notification = S3EventListenerTest.generateNotification(type, s3Bucket, folderAndFile);
        s3EventListener.listen(s3Notification.toJson());

        List<FileUploadedEvent> fileUploadedEvents = brokerClient.readAllFromDestination("events", FileUploadedEvent.class);
        assertEquals(1, fileUploadedEvents.size());
        assertEquals(customerId, fileUploadedEvents.head().getProduct().getComponent0());
        assertEquals(productId, fileUploadedEvents.head().getProduct().getComponent1());
        String uriPath = String.format("s3://%s/%s/%s/%s/%s#1", s3Bucket, branchName, customerId, productId, fileName);
        assertEquals(uriPath, fileUploadedEvents.head().getPath().toString());
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends PaymentfilesCHApplication {

    }
}