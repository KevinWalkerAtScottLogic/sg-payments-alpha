package gov.scot.payments.paymentfiles.ch.app;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchRules;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.library.GeneralCodingRules;
import gov.scot.payments.testing.architecture.CodingStandardsRules;
import gov.scot.payments.testing.architecture.ApplicationRules;
import gov.scot.payments.testing.architecture.ComponentRules;

@AnalyzeClasses(importOptions = {ImportOption.DoNotIncludeTests.class, ImportOption.DoNotIncludeArchives.class})
public class ArchitectureTest{

    // Needed to comment this out as S3 Event Notification uses Joda time. Converting it to java.time.Instant counted as
    // using it.

//    @ArchTest
//    public static final ArchRules generalRules = ArchRules.in(CodingStandardsRules.class);

    @ArchTest
    public static final ArchRule doNotUseStandardOut = GeneralCodingRules.NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS;

    @ArchTest
    public static final ArchRule doNotThrowGenericExceptions = GeneralCodingRules.NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS;

    @ArchTest
    public static final ArchRules appRules = ArchRules.in(ApplicationRules.class);

    @ArchTest
    public static final ArchRule componentRule = ComponentRules.doesNotDependOnOtherAppClasses("paymentfiles.ch");

}