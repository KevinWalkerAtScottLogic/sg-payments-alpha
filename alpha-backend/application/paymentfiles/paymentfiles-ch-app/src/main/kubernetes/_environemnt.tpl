{{- define "paymentfiles-paymentfiles-ch-app.env" -}}
- name: FILE_BUCKET_NAME
  valueFrom:
    configMapKeyRef:
      name: sftp-info
      key: bucket_name
- name: SQS_QUEUE_URL
  valueFrom:
    configMapKeyRef:
      name: sftp-info
      key: sqs_queue_url
- name: S3_ENDPOINT_OVERRIDE
  value: {{ .Values.${projectValuesName}.s3Override | quote }}
{{- end -}}
