package gov.scot.payments.paymentfiles.ch.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.component.commandhandler.CommandHandlerApplication;
import gov.scot.payments.application.component.commandhandler.CommandHandlerFunction;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.paymentfiles.model.command.CreateFileResourceCommand;
import gov.scot.payments.paymentfiles.model.command.DeleteFileResourceCommand;
import gov.scot.payments.paymentfiles.model.event.FileResourceCreatedEvent;
import gov.scot.payments.paymentfiles.model.event.FileResourceDeletedEvent;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextInstanceDataAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.context.config.annotation.ContextResourceLoaderConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.MessageChannel;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3ClientBuilder;

import java.net.URI;


@ApplicationComponent
@EnableAutoConfiguration(exclude = {ContextStackAutoConfiguration.class, ContextResourceLoaderAutoConfiguration.class
        , ContextInstanceDataAutoConfiguration.class})
public class PaymentfilesCHApplication extends CommandHandlerApplication<EventWithCauseImpl> {

    @Override
    protected Set<String> registeredVerbs() {
        return HashSet.of("Write");
    }

    @Bean
    public CommandHandlerFunction<EventWithCauseImpl> chFunction(S3Adapter s3Adapter) {
        return CommandHandlerFunction.patternMatching(b ->
            b
                .match(CreateFileResourceCommand.class, c -> List.of(new FileResourceCreatedEvent(c.getProduct()
                        , s3Adapter.createFileResource(c.getProduct().getComponent0(), c.getProduct().getComponent1()))
                    )
                )
                .match(DeleteFileResourceCommand.class, c -> List.of(new FileResourceDeletedEvent(c.getProduct()
                        , s3Adapter.deleteFileResource(c.getProduct().getComponent0(), c.getProduct().getComponent1()))
                    )
                )
        );
    }

    @Bean
    public S3EventListener s3EventListener(ObjectMapper mapper, @Qualifier("send-events") MessageChannel channel) {
        return new S3EventListener(mapper, channel);
    }

    @Bean
    public S3Client s3Client(@Value("${S3_ENDPOINT_OVERRIDE:}") String s3Endpoint
            , @Value("${cloud.aws.region.static}") String region
            , @Value("${cloud.aws.credentials.accessKey}") String accessKey
            , @Value("${cloud.aws.credentials.secretKey}") String secretKey){
        AwsCredentials credentials = AwsBasicCredentials.create(accessKey,secretKey);
        AwsCredentialsProvider credentialsProvider = StaticCredentialsProvider.create(credentials);
        S3ClientBuilder builder = S3Client.builder()
                .credentialsProvider(credentialsProvider)
                .region(Region.of(region));
        if(!StringUtils.isEmpty(s3Endpoint)){
            builder = builder.endpointOverride(URI.create(s3Endpoint));
        }
        return builder.build();
    }

    @Bean
    public S3Adapter s3Adapter(
            S3Client s3Client,
            @Value("${namespace}") String branchName,
            @Value("${s3.bucket.name}") String bucketName,
            @Value("${cloud.aws.region.static}") String region
    ) {
        String bucketUrl = String.format("https://%s.s3.%s.amazonaws.com", bucketName, region);
        return new S3Adapter(s3Client, branchName, bucketName, bucketUrl);
    }

    public static void main(String[] args) {
        BaseApplication.run(PaymentfilesCHApplication.class, args);
    }
}