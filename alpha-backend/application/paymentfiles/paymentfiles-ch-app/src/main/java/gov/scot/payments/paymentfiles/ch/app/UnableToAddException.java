package gov.scot.payments.paymentfiles.ch.app;

import lombok.Getter;

public class UnableToAddException extends RuntimeException {

    @Getter
    private String notAddedPath;

    public UnableToAddException(Throwable cause, String notAddedPath) {
        super(cause);
        this.notAddedPath = notAddedPath;
    }

    @Override
    public String toString() {
        return "Unable to add path: " + notAddedPath;
    }

}
