package gov.scot.payments.paymentfiles.ch.app;

import lombok.AllArgsConstructor;
import lombok.Getter;
import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.ListObjectsRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.S3Object;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

@AllArgsConstructor
public class S3Adapter {
    private S3Client s3Client;
    @Getter
    private String branchName;
    private String s3BucketName;
    @Getter
    private String bucketUrl;

    URL createFileResource(String customerName, String productName) {
        return productName == null ? createCustomerFolder(customerName) : createProductFolder(customerName, productName);
    }

    URL deleteFileResource(String customerName, String productName) {
        return productName == null ? deleteCustomerFolder(customerName) : deleteProductFolder(customerName, productName);
    }


    private URL createProductFolder(String customerName, String productName) {
        String s3Path = createProductPath(branchName, customerName, productName);
        try {
            var putObject = createPutObjectRequest(s3Path);
            URL url = createS3ResourceUrl(customerName, productName);
            s3Client.putObject(putObject, RequestBody.empty());
            return url;
        } catch (SdkException | MalformedURLException ex) {
            throw new UnableToAddException(ex, s3Path);
        }
    }

    private URL createCustomerFolder(String customerReference) {
        String s3Path = createCustomerPath(branchName, customerReference);
        try {
            var putObject = createPutObjectRequest(s3Path);
            URL url = createS3ResourceUrl(customerReference);
            s3Client.putObject(putObject, RequestBody.empty());
            return url;
        } catch (SdkException | MalformedURLException ex) {
            throw new UnableToAddException(ex, s3Path);
        }
    }

    private URL deleteProductFolder(String customerName, String productName) {
        String s3Path = createProductPath(branchName, customerName, productName);
        try {
            URL url = createS3ResourceUrl(customerName, productName);
            emptyAndDeleteS3Folder(s3Path);
            return url;
        } catch (SdkException | MalformedURLException e) {
            throw new UnableToDeleteException(e, Collections.singletonList(s3Path));
        }
    }

    private URL deleteCustomerFolder(String customerReference) {
        String s3Path = createCustomerPath(branchName, customerReference);
        try {
            URL url = createS3ResourceUrl(customerReference);
            emptyAndDeleteS3Folder(s3Path);
            return url;
        } catch (SdkException | MalformedURLException e) {
            throw new UnableToDeleteException(e, Collections.singletonList(s3Path));
        }

    }

    private void emptyAndDeleteS3Folder(String folderPath) throws UnableToDeleteException {
        List<S3Object> s3Objects = getObjectsInS3Folder(folderPath);

        Map<Boolean, List<S3Object>> partitioned = s3Objects.stream()
                .collect(Collectors.partitioningBy(s3Object -> s3Object.key().endsWith("/")));

        List<S3Object> filesToDelete = partitioned.get(false);
        List<S3Object> foldersToDelete = partitioned.get(true);

        if (filesToDelete.size() > 0) {
            deleteS3Contents(filesToDelete);
        }

        foldersToDelete.sort((Comparator.comparing(S3Object::key)).reversed());
        deleteS3Contents(foldersToDelete);
    }


    void deleteS3Contents(List<S3Object> itemsToDelete) {
        List<String> remainingItems = new ArrayList<>();
        for (S3Object s3Object : itemsToDelete) {
            try {
                deleteS3Item(s3Object);
            } catch (SdkException ex) {
                remainingItems.add(s3Object.key());
            }
        }
        if (remainingItems.size() > 0) {
            throw new UnableToDeleteException("Unable to delete the following from S3: ", remainingItems);
        }
    }

    private void deleteS3Item(S3Object s3Object) throws SdkException {
        var deleteObjectRequest = createDeleteObjectRequest(s3Object);
        s3Client.deleteObject(deleteObjectRequest);
    }

    private DeleteObjectRequest createDeleteObjectRequest(S3Object s3Object) {
        return DeleteObjectRequest.builder()
                .bucket(s3BucketName)
                .key(s3Object.key())
                .build();
    }

    List<S3Object> getObjectsInS3Folder(String folderPath) {
        var listObjectsRequest = ListObjectsRequest.builder()
                .bucket(s3BucketName)
                .prefix(folderPath)
                .build();

        var listObjectsResponse = s3Client.listObjects(listObjectsRequest);
        List<S3Object> s3Objects = new ArrayList<>(listObjectsResponse.contents());

        while (listObjectsResponse.isTruncated()) {
            listObjectsRequest = ListObjectsRequest.builder()
                    .bucket(s3BucketName)
                    .prefix(folderPath)
                    .marker(listObjectsResponse.nextMarker())
                    .build();
            listObjectsResponse = s3Client.listObjects(listObjectsRequest);
            s3Objects.addAll(listObjectsResponse.contents());
        }

        return s3Objects;
    }

    private PutObjectRequest createPutObjectRequest(String key) {
        return PutObjectRequest.builder()
                .bucket(s3BucketName)
                .key(key)
                .build();
    }

    private String createProductPath(String branchName, String customerReference, String productReference) {
        return String.format("%s/%s/%s/", branchName, customerReference, productReference);
    }

    private String createCustomerPath(String branchName, String customerReference) {
        return String.format("%s/%s/", branchName, customerReference);
    }

    private URL createS3ResourceUrl(String customerName, String productName) throws MalformedURLException {
        return new URL(String.format("%s/%s/%s/%s/", bucketUrl, branchName, customerName, productName));
    }

    private URL createS3ResourceUrl(String customerName) throws MalformedURLException {
        return new URL(String.format("%s/%s/%s/", bucketUrl, branchName, customerName));
    }

}