package gov.scot.payments.psps.adapter.bacs;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.bacs.BacsServiceConfiguration;
import gov.scot.payments.bacs.ReportRepository;
import gov.scot.payments.bacs.standard18.input.ServiceUserNumber;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.psps.adapter.BasePspAdapterApplication;
import gov.scot.payments.psps.adapter.PostSubmissionEventSender;
import gov.scot.payments.psps.adapter.bacs.mock.DailyBacsReportsEntityRepository;
import gov.scot.payments.psps.adapter.bacs.mock.JpaBacsReportRepository;
import gov.scot.payments.psps.adapter.bacs.mock.DataRecordEntityRepository;
import gov.scot.payments.psps.adapter.bacs.mock.MockBacsConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.math.BigDecimal;

@ApplicationComponent
@Import({MockBacsConfiguration.class,BacsAdapterConfiguration.class})
@EnableJpaRepositories(basePackageClasses = BacsPaymentKeyRepository.class)
@EntityScan(basePackageClasses = BacsPaymentKey.class)
public class BacsAdapterApplication extends BasePspAdapterApplication{

    @Override
    protected PaymentMethod paymentMethod() {
        return PaymentMethod.Bacs;
    }

    public static void main(String[] args){
        BaseApplication.run(BacsAdapterApplication.class,args);
    }
}