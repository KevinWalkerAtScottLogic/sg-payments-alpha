package gov.scot.payments.psps.adapter.bacs;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface BacsReportStatusRepository extends JpaRepository<BacsReportStatus,LocalDate> {

    Optional<BacsReportStatus> findTopByOrderByIdAsc();
}
