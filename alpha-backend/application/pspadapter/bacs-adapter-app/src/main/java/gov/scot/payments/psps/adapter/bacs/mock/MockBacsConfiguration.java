package gov.scot.payments.psps.adapter.bacs.mock;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.bacs.BacsServiceConfiguration;
import gov.scot.payments.bacs.ReportRepository;
import gov.scot.payments.json.ObjectMapperProvider;
import gov.scot.payments.psps.adapter.bacs.BacsReportStatusRepository;
import gov.scot.payments.psps.adapter.bacs.BacsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

@Import(BacsServiceConfiguration.class)
public class MockBacsConfiguration {

    @Bean
    public BacsService mockBacsService(gov.scot.payments.bacs.BacsService bacsService){
        return new MockBacsService(bacsService);
    }

    @Bean
    public ReportRepository reportRepository(BacsReportStatusRepository bacsReportStatusRepository
            , DataRecordEntityRepository paymentFileEntityRepository
            , DailyBacsReportsEntityRepository dailyBacsReportsEntityRepository){
        return new JpaBacsReportRepository(bacsReportStatusRepository
                , paymentFileEntityRepository
                , dailyBacsReportsEntityRepository);
    }
}
