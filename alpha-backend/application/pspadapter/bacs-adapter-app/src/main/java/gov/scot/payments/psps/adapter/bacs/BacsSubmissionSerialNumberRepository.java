package gov.scot.payments.psps.adapter.bacs;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BacsSubmissionSerialNumberRepository extends JpaRepository<BacsSubmissionSerialNumber, String> {
}
