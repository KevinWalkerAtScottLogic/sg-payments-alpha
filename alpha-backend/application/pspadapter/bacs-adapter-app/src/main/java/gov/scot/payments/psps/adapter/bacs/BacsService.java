package gov.scot.payments.psps.adapter.bacs;

import gov.scot.payments.bacs.model.DailyBacsReports;
import gov.scot.payments.bacs.standard18.input.BacsSubmissionFile;

import java.time.LocalDate;
import java.util.Optional;

public interface BacsService {
    void submit(BacsSubmissionFile bacsSubmissionFile);

    Optional<DailyBacsReports> getReports(LocalDate date);

    void updateProcessingDay(LocalDate date);
}
