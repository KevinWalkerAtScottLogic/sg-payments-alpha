package gov.scot.payments.psps.adapter.bacs;

import gov.scot.payments.bacs.standard18.input.BacsSubmissionFile;
import gov.scot.payments.bacs.standard18.input.ServiceUserNumber;
import gov.scot.payments.bacs.standard18.input.SubmissionSerialNumber;
import gov.scot.payments.psps.method.model.PspSubmissionRequest;
import gov.scot.payments.psps.method.model.PspTransactionCostEstimateResponse;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.javamoney.moneta.Money;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.money.Monetary;
import java.math.BigDecimal;
import java.time.LocalDate;

@RequiredArgsConstructor
@RequestMapping
public class BacsAdapterService {

    private final BigDecimal costPerTransaction;
    private final String psp;
    private final BacsReportService bacsReportService;
    private final BacsService bacsService;
    private final BacsSubmissionFileBuilder bacsSubmissionFileBuilder;
    private final ServiceUserNumber bureauNumber;
    private final BacsSerialNumberGenerator serialNumberGenerator;

    @RequestMapping(method = RequestMethod.POST, value = "/estimateTransactionCosts")
    @ResponseBody
    public Mono<ResponseEntity<PspTransactionCostEstimateResponse>> estimateTransactionCosts (@RequestBody PspSubmissionRequest request) {

        BigDecimal amount = costPerTransaction.multiply(new BigDecimal(request.getPayments().size()));

        return Mono.just(ResponseEntity.ok()
                .body(PspTransactionCostEstimateResponse
                .builder()
                .psp(psp)
                .amount(Money.of(amount, Monetary.getCurrency("GBP")))
                .build()));
    }

    @PostMapping(value = {"/submit"})
    @ResponseBody
    @Transactional
    public Mono<ResponseEntity> submitBacsFile(@RequestBody PspSubmissionRequest pspSubmissionRequest) {
        bacsReportService.storePaymentKeys(pspSubmissionRequest);
        SubmissionSerialNumber submissionSerialNumber = serialNumberGenerator.generate();
        BacsSubmissionFile bacsSubmissionFile = bacsSubmissionFileBuilder.buildBacsPaymentFile(bureauNumber,submissionSerialNumber,pspSubmissionRequest.getPayments());
        bacsService.submit(bacsSubmissionFile);
        return Mono.just(ResponseEntity.ok().build());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/pollReports")
    @ResponseBody
    @Transactional
    public Mono<ResponseEntity<Object>> pollReports(@RequestParam(value = "date",required = false) LocalDate date) {
        LocalDate dateToUse = date == null ? LocalDate.now() : date;
        return Mono.fromCallable(() -> {
            bacsReportService.pollReports(dateToUse);
            return new Object();
        })
                .map(o -> ResponseEntity.ok().build())
                .onErrorResume(t -> Mono.just(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(t.getMessage())));
    }

}
