package gov.scot.payments.psps.adapter.bacs;

import gov.scot.payments.bacs.standard18.input.ServiceUserNumber;
import gov.scot.payments.psps.adapter.PostSubmissionEventSender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Configuration
public class BacsAdapterConfiguration {

    @Bean
    public BacsSubmissionFileBuilder bacsSubmissionFileBuilder(){
        return new BacsSubmissionFileBuilder();
    }

    @Bean
    public BacsSerialNumberGenerator bacsSerialNumberGenerator(BacsSubmissionSerialNumberRepository bacsSubmissionSerialNumberRepository){
        return new JpaBacsSerialNumberGenerator(bacsSubmissionSerialNumberRepository);
    }
    @Bean
    public BacsAdapterService bacsAdapterService(@Value("${application.transaction-cost}") BigDecimal costPerTransaction
            , @Value("${component.name}") String psp
            , BacsReportService bacsReportService
            , BacsService bacsService
            , @Value("${userNumber}") String bureauNumber
            , BacsSubmissionFileBuilder bacsSubmissionFileBuilder
            , BacsSerialNumberGenerator bacsSerialNumberGenerator) {
        return new BacsAdapterService(costPerTransaction
                , psp
                , bacsReportService
                ,bacsService
                , bacsSubmissionFileBuilder
                , ServiceUserNumber.fromString(bureauNumber)
                , bacsSerialNumberGenerator);
    }

    @Bean
    public BacsReportService bacsReportService(BacsReportStatusRepository reportStatusRepository
            , BacsPaymentKeyRepository paymentKeyRepository
            , PostSubmissionEventSender eventSender
            , BacsService bacsService){
        return new BacsReportService(reportStatusRepository,paymentKeyRepository,eventSender, bacsService);
    }

}
