package gov.scot.payments.psps.adapter.bacs;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.LongType;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Properties;

public class SubmissionSerialNumberGenerator extends SequenceStyleGenerator {

    @Override
    public Serializable generate(SharedSessionContractImplementor session,
                                 Object object) throws HibernateException {
        Long generated = (Long)super.generate(session, object);
        String base36 = Long.toString(generated,36).toUpperCase();
        if(base36.length() > 6){
            throw new HibernateException("No more bacs submission serial numbers remaining, can not make BACS submission");
        }
        return StringUtils.leftPad(base36,6,'0');
    }

    @Override
    public void configure(Type type, Properties params,
                          ServiceRegistry serviceRegistry) throws MappingException {
        super.configure(LongType.INSTANCE, params, serviceRegistry);
    }

}
