package gov.scot.payments.psps.adapter.bacs.mock;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface DailyBacsReportsEntityRepository extends JpaRepository<DailyBacsReportsEntity, LocalDate> {
}
