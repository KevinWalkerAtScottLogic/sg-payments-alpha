package gov.scot.payments.psps.adapter.bacs;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode(of = "id")
public class BacsSubmissionSerialNumber {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bacs_seq")
    @GenericGenerator(name = "bacs_seq", strategy = "gov.scot.payments.psps.adapter.bacs.SubmissionSerialNumberGenerator")
    private String id;

}
