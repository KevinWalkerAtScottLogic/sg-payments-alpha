package gov.scot.payments.psps.adapter.bacs;

import gov.scot.payments.bacs.model.DailyBacsReports;
import gov.scot.payments.bacs.xmladvices.advices.*;
import gov.scot.payments.payments.model.aggregate.MetadataField;
import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.aggregate.UKBankAccount;
import gov.scot.payments.payments.model.aggregate.UKBuildingSocietyAccount;
import gov.scot.payments.psps.adapter.PostSubmissionEventSender;
import gov.scot.payments.psps.method.model.PspCompleteResponse;
import gov.scot.payments.psps.method.model.PspReturnResponse;
import gov.scot.payments.psps.method.model.PspSubmissionRequest;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.javamoney.moneta.Money;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Objects;
import java.util.UUID;

@RequiredArgsConstructor
@Slf4j
public class BacsReportService {

    private final BacsReportStatusRepository reportStatusRepository;
    private final BacsPaymentKeyRepository paymentKeyRepository;
    private final PostSubmissionEventSender eventSender;
    private final BacsService bacsService;

    @Transactional
    public void pollReports(LocalDate now) {
        List<LocalDate> reportDates = getUnreportedDates(now);
        for(LocalDate date : reportDates){
            pollReport(date);
        }
    }

    @Transactional
    public void storePaymentKeys(PspSubmissionRequest request) {
        List<BacsPaymentKey> keys = request.getPayments().map(this::buildBacsKey);
        paymentKeyRepository.saveAll(keys);
    }

    List<LocalDate> getUnreportedDates(LocalDate now){
        LocalDate latestProcessed = reportStatusRepository.findTopByOrderByIdAsc().map(BacsReportStatus::getId).map(d -> d.plusDays(1)).orElse(now);
        if(latestProcessed.isAfter(now)){
            return List.empty();
        }
        return List.ofAll(latestProcessed.datesUntil(now.plusDays(1)));
    }

    void pollReport(LocalDate date) {
        bacsService.updateProcessingDay(date);
        Option<DailyBacsReports> reports = Option.ofOptional(bacsService.getReports(date));
        if(reports.isDefined()){
            Map<String,BacsPaymentKey> paymentsForDate = List.ofAll(paymentKeyRepository.findByProcessingDate(date)).toMap(b -> new Tuple2<>(b.getKey(),b));
            List<PspReturnResponse> awacsReturns = reports.map(DailyBacsReports::getAwacs).filter(Objects::nonNull).map(r -> r.getData().getMessagingAdvices()).toList().flatMap(a -> handleAwacsReport(a,paymentsForDate));
            List<PspReturnResponse> arucsReturns = reports.map(DailyBacsReports::getArucs).filter(Objects::nonNull).map(r -> r.getData().getARUCS()).toList().flatMap(a -> handleArucsReport(a,paymentsForDate));
            List<PspReturnResponse> returns = arucsReturns.appendAll(awacsReturns);
            List<UUID> returnIds = returns.map(PspReturnResponse::getPaymentId);
            List<PspCompleteResponse> complete = paymentsForDate
                    .toList()
                    .filter(p -> !returnIds.contains(p._2.getId()))
                    .map(p -> PspCompleteResponse.builder()
                                                 .paymentId(p._2.getId())
                                                 .build());
            sendEventsAndWait(returns,complete);
            paymentKeyRepository.deleteByProcessingDate(date);
        }
        reportStatusRepository.saveAndFlush(BacsReportStatus.builder().id(date).build());
    }

    BacsPaymentKey buildBacsKey(Payment payment) {
        UKBankAccount creditorAccount = payment.getCreditorAccountAs(UKBankAccount.class).get();
        LocalDate processingDate = LocalDate.ofInstant(payment.getLatestExecutionDate(), ZoneId.of("UTC"));
        String truncatedRef;
        if(creditorAccount instanceof UKBuildingSocietyAccount){
            truncatedRef = String.format("%.18s",((UKBuildingSocietyAccount)creditorAccount).getRollNumber());
        } else {
            truncatedRef = String.format("%.18s",payment.getCreditorMetadata(MetadataField.CLIENT_REF).map(MetadataField::getValue).getOrElse(""));
        }
        String payerAccount = String.format("%s-%s",payment.getDebtorAccount().getSortCode(),payment.getDebtorAccount().getAccountNumber());
        String receiverAccount = String.format("%s-%s",creditorAccount.getSortCode(),creditorAccount.getAccountNumber());
        String amount = payment.getAmount().scaleByPowerOfTen(2).stripTrailingZeros().toString();

        String key = String.format("%s;%s;%s;%s;%s",processingDate,payerAccount,receiverAccount,amount,truncatedRef).toUpperCase();

        return BacsPaymentKey.builder()
                             .id(payment.getId())
                             .processingDate(processingDate)
                             .key(key)
                             .build();
    }

    private void sendEventsAndWait(List<PspReturnResponse> returns, List<PspCompleteResponse> complete) {
        Flux<Void> returnFlux = Flux.fromIterable(returns).flatMap(eventSender::sendReturn);
        Flux<Void> completeFlux = Flux.fromIterable(complete).flatMap(eventSender::sendComplete);
        returnFlux.mergeWith(completeFlux).blockLast();
    }

    private List<PspReturnResponse> handleArucsReport(ARUCS arucs,Map<String,BacsPaymentKey> paymentsForDate) {
      return List.ofAll(arucs.getAdvice().getOriginatingAccountRecords().getOriginatingAccountRecord())
              .flatMap(OriginatingAccountRecord::getReturnedCreditItem)
              .map(ri -> buildResponseFromArucsItem(ri,paymentsForDate))
              .filter(Objects::nonNull);
    }

    private List<PspReturnResponse> handleAwacsReport(MessagingAdvices awacs,Map<String,BacsPaymentKey> paymentsForDate) {
       return List.ofAll(awacs.getMessagingAdvice())
               .map(mi -> buildResponseFromAwacsItem(mi,paymentsForDate))
               .filter(Objects::nonNull);
    }

    private PspReturnResponse buildResponseFromAwacsItem(MessagingAdvice mi,Map<String,BacsPaymentKey> paymentsForDate) {
        String key = buildBacsKey(mi);
        BacsPaymentKey record = paymentsForDate.getOrElse(key,null);
        if(record == null){
            log.error("No Bacs Payment record found for: {} manual intervention needed",key);
            return null;
        }
        return PspReturnResponse.builder()
                .code(mi.getReasonCode())
                .message(mi.getReasonCode())
                .paymentId(record.getId())
                .build();
    }

    private PspReturnResponse buildResponseFromArucsItem(ReturnedItemType ri,Map<String,BacsPaymentKey> paymentsForDate) {
        String key = buildBacsKey(ri);
        BacsPaymentKey record = paymentsForDate.getOrElse(key,null);
        if(record == null){
            log.error("No Bacs Payment record found for: {} manual intervention needed",key);
            return null;
        }
        return PspReturnResponse.builder()
                .amount(Money.of(ri.getValueOf().divide(new BigDecimal("100")),"GBP"))
                .code(ri.getReturnCode())
                .message(ri.getReturnDescription())
                .paymentId(record.getId())
                .build();
    }

    private String buildBacsKey(ReturnedItemType ri) {
        LocalDate processingDate = LocalDate.of(
                ri.getOriginalProcessingDate().getYear(),
                ri.getOriginalProcessingDate().getMonth(),
                ri.getOriginalProcessingDate().getDay());
        String payerAccount = String.format("%s-%s",ri.getPayerAccount().getSortCode().replace("-",""),ri.getPayerAccount().getNumber());
        String receiverAccount = String.format("%s-%s",ri.getReceiverAccount().getSortCode().replace("-",""),ri.getReceiverAccount().getNumber());
        String amount = String.format("GBP %s",ri.getValueOf().scaleByPowerOfTen(2).stripTrailingZeros().toPlainString());
        return String.format("%s;%s;%s;%s;%s",processingDate,payerAccount,receiverAccount,amount,ri.getRef().trim()).toUpperCase();
    }

    private String buildBacsKey(MessagingAdvice mi) {
        LocalDate processingDate = LocalDate.of(
                mi.getOriginalProcDate().getYear(),
                mi.getOriginalProcDate().getMonth(),
                mi.getOriginalProcDate().getDay());
        String payerAccount = String.format("%s-%s",mi.getPayerSortCode(),mi.getPayerAccountNumber());
        String receiverAccount = String.format("%s-%s",mi.getOrigSortCode(),mi.getOrigAccountNumber());
        String amount = String.format("GBP %s",mi.getAmountOfPayment());
        return String.format("%s;%s;%s;%s;%s",processingDate,payerAccount,receiverAccount,amount,mi.getReference().trim()).toUpperCase();
    }

}
