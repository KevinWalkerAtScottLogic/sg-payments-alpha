package gov.scot.payments.psps.adapter.bacs.mock;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.bacs.ReportRepository;
import gov.scot.payments.bacs.model.DailyBacsReports;
import gov.scot.payments.bacs.standard18.common.BacsDate;
import gov.scot.payments.bacs.standard18.common.model.DataRecord;
import gov.scot.payments.bacs.standard18.input.MultiPayDayDataRecord;
import gov.scot.payments.bacs.standard18.input.PaymentFile;
import gov.scot.payments.bacs.standard18.input.SubmissionDataRecord;
import gov.scot.payments.bacs.xmladvices.BACSAdviceDocumentWrapper;
import gov.scot.payments.bacs.xmladvices.BACSParsingException;
import gov.scot.payments.bacs.xmladvices.advices.BACSDocument;
import gov.scot.payments.psps.adapter.bacs.BacsReportStatus;
import gov.scot.payments.psps.adapter.bacs.BacsReportStatusRepository;
import lombok.RequiredArgsConstructor;

import java.io.UncheckedIOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class JpaBacsReportRepository implements ReportRepository {

    private final BacsReportStatusRepository bacsReportStatusRepository;
    private final DataRecordEntityRepository paymentFileEntityRepository;
    private final DailyBacsReportsEntityRepository dailyBacsReportsEntityRepository;

    @Override
    public LocalDate getCurrentDate(){
        return bacsReportStatusRepository.findTopByOrderByIdAsc()
                                         .map(BacsReportStatus::getId)
                                  .orElseGet(LocalDate::now);
    }

    @Override
    public LocalDate saveCurrentDate(LocalDate newProcessingDate){
        return bacsReportStatusRepository.save(BacsReportStatus.builder().id(newProcessingDate).build()).getId();
    }

    //paymentfile - sub serial number, date, string
    @Override
    public void savePaymentFile(final PaymentFile paymentFile) {
        final String id = String.format("%s-%s",paymentFile.getHeaders().getHeaderLabelOne().getSetIdentification().getValue()
                ,paymentFile.getHeaders().getHeaderLabelOne().getFileSequenceNumber());
        LocalDate processingDate = paymentFile.getHeaders().getProcessingDate();
        boolean multiDay = paymentFile.getHeaders().isMultiProcessingDayFile();
        for(int i=0;i<paymentFile.getDataRecords().size();i++){
            DataRecord record = paymentFile.getDataRecords().get(i);
            LocalDate dateToUse = multiDay ? ((MultiPayDayDataRecord) record).getProcessingDate().getValue() : processingDate;
            DataRecordEntity toSave = DataRecordEntity.builder()
                                                      .id(String.format("%s-%s",id,i))
                                                      .processingDate(dateToUse)
                                                      .multiDay(multiDay)
                                                      .content(record.toString())
                                                      .build();
            paymentFileEntityRepository.save(toSave);
        }
    }

    @Override
    public List<SubmissionDataRecord> findSpdSubmissionRecordsByDate(final LocalDate currentDate) {
        return paymentFileEntityRepository.findByProcessingDateAndMultiDay(currentDate,false)
                .stream()
                .map(e -> SubmissionDataRecord.fromString(e.getContent()))
                .collect(Collectors.toList());
    }

    @Override
    public List<MultiPayDayDataRecord> findMpdSubmissionRecordsByDate(final LocalDate currentDate) {
        return paymentFileEntityRepository.findByProcessingDateAndMultiDay(currentDate,true)
                                                                     .stream()
                                                                     .map(e -> MultiPayDayDataRecord.fromString(e.getContent(),BacsDate.fromLocalDate(e.getProcessingDate())))
                                                                     .collect(Collectors.toList());
    }

    //dailyreports - date, string, string, string
    @Override
    public DailyBacsReports saveDailyReports(final LocalDate localDate, final DailyBacsReports dailyBacsReports) {
        DailyBacsReportsEntity saved = dailyBacsReportsEntityRepository.save(DailyBacsReportsEntity.builder()
                                                                                                   .id(localDate)
                                                                                                   .arucs(dailyBacsReports.getArucs() != null ? BACSAdviceDocumentWrapper.fromJaxbXmlObject(dailyBacsReports.getArucs()).toXmlString() : null)
                                                                                                   .awacs(dailyBacsReports.getAwacs() != null ? BACSAdviceDocumentWrapper.fromJaxbXmlObject(dailyBacsReports.getAwacs()).toXmlString() : null)
                                                                                                   .submission(dailyBacsReports.getBacsSubmission())
                                                                                                   .build());
        return getDailyReport(localDate).get();
    }

    @Override
    public Optional<DailyBacsReports> getDailyReport(final LocalDate localDate) {
        return dailyBacsReportsEntityRepository.findById(localDate)
                .map(e -> {
                    try {
                        BACSDocument arucs = e.getArucs() != null ? BACSAdviceDocumentWrapper.fromXmlString(e.getArucs()).toJaxbXmlObject() : null;
                        BACSDocument awacs = e.getAwacs() != null ? BACSAdviceDocumentWrapper.fromXmlString(e.getAwacs()).toJaxbXmlObject() : null;
                        return new DailyBacsReports(arucs,awacs,e.getSubmission());
                    } catch (BACSParsingException ex) {
                        throw new RuntimeException(ex);
                    }
                });
    }

    //not needed
    @Override
    public String saveArrivalReport(final String s, final String s1) {
        return s1;
    }

    @Override
    public Optional<String> getArrivalReport(final String s) {
        return Optional.empty();
    }

    public void clear(){

    }

}
