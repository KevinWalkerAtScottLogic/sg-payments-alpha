package gov.scot.payments.psps.adapter.bacs;

import gov.scot.payments.bacs.standard18.common.AccountName;
import gov.scot.payments.bacs.standard18.common.BacsDate;
import gov.scot.payments.bacs.standard18.common.TransactionCode;
import gov.scot.payments.bacs.standard18.common.model.AccountNumber;
import gov.scot.payments.bacs.standard18.common.model.DataRecord;
import gov.scot.payments.bacs.standard18.input.*;
import gov.scot.payments.bacs.standard18.input.SortCode;
import gov.scot.payments.payments.model.aggregate.*;
import io.vavr.Tuple;
import io.vavr.collection.List;

import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.stream.Collectors;

public class BacsSubmissionFileBuilder {

    private static final String BACS_SERVICE_USER_NUMBER = "BACS_SUN";
    private static final String BACS_SERVICE_ACCOUNT_NAME= "BACS_SAN";

    // Common constants
    private static final String ACCESSIBILITY_INDICATOR = "";
    private static final String ACCOUNT_NAME = "1234567";
    private static final String BLOCK_COUNT = "0".repeat(6);
    private static final int BLOCK_LENGTH = 00106; // Not sure if correct; not validated
    private static final String GENERATION_NUMBER = "00";
    private static final String GENERATION_VERSION_NUMBER = "00";
    private static final String SYSTEM_CODE = "A".repeat(13);
    private static final String FILE_IDENTIFIER_REQUIRED_CHARACTER_A = "A";
    private static final String FILE_IDENTIFIER_REQUIRED_CHARACTER_S = "S";
    private static final String FILE_SECTION_NUMBER = "0001";
    private static final String TIME_ZONE_ID = "UTC";

    // Volume Header Label One constants
    private static final String VOLUME_HEADER_LABEL_IDENTIFIER = "VOL";
    private static final String VOLUME_HEADER_LABEL_NUMBER = "1";
    private static final String LABEL_STANDARD_LEVEL = "1";

    // Header Label constants
    private static final String HEADER_LABEL_IDENTIFIER = "HDR";
    private static final String USER_HEADER_LABEL_IDENTIFIER = "UHL";
    private static final String HEADER_LABEL_ONE_NUMBER = "1";
    private static final String HEADER_LABEL_TWO_NUMBER = "2";
    private static final String RECORD_FORMAT = "F";
    private static final String IDENTIFYING_NUMBER = "999999" + " ".repeat(4);
    private static final String STERLING_CURRENCY_CODE = "00";
    private static final String COUNTRY_CODE = "000000";

    // Trailer Label constants
    private static final String EOF_LABEL_IDENTIFIER = "EOF";
    private static final String USER_TRAILER_LABEL_IDENTIFIER = "UTL";
    private static final String TRAILER_LABEL_ONE_NUMBER = "1";
    private static final String TRAILER_LABEL_TWO_NUMBER = "2";

    public BacsSubmissionFile buildBacsPaymentFile(ServiceUserNumber bureauNumber,
                                            SubmissionSerialNumber submissionSerialNumber,
                                            List<Payment> payments) {

        List<PaymentFile> submissionFiles = payments
                .groupBy(p -> Tuple.of(
                        p.getLatestExecutionDate(),
                        p.getMethodProperty(PaymentMethod.Bacs,BACS_SERVICE_USER_NUMBER).getOrElse("")))
                .toList()
                .zipWithIndex()
                .map( t -> {
                    List<Payment> paymentsInBatch = t._1._2;
                    BacsDate paymentDate = getBacsDateFromInstant(t._1._1._1);
                    ServiceUserNumber serviceUserNumber = ServiceUserNumber.fromString(t._1._1._2);
                    int batchIndex = t._2;
                    return buildBacsPaymentFile(
                            bureauNumber,
                            serviceUserNumber,
                            submissionSerialNumber,
                            paymentDate,
                            batchIndex,
                            paymentsInBatch);
                });

        return BacsSubmissionFile.builder()
                .volumeHeaderLabelOne(getVolumeHeaderLabelOne(
                        bureauNumber,
                        submissionSerialNumber))
                .paymentFiles(submissionFiles.toJavaList())
                .build();
    }

    public static VolumeHeaderLabelOne getVolumeHeaderLabelOne(ServiceUserNumber bureauNumber, SubmissionSerialNumber submissionSerialNumber) {
        return VolumeHeaderLabelOne.builder()
                .labelIdentifier(VOLUME_HEADER_LABEL_IDENTIFIER)
                .labelNumber(VOLUME_HEADER_LABEL_NUMBER)
                .submissionSerialNumber(submissionSerialNumber)
                .accessibilityIndicator(ACCESSIBILITY_INDICATOR)
                .firstBlankSpaces(" ".repeat(30))
                .serviceUserNumber(bureauNumber)
                .secondBlankSpaces(" ".repeat(32))
                .labelStandardLevel(LABEL_STANDARD_LEVEL)
                .build();
    }

    private PaymentFile buildBacsPaymentFile(ServiceUserNumber bureauNumber,
                                                    ServiceUserNumber serviceUserNumber,
                                                    SubmissionSerialNumber submissionSerialNumber,
                                                    BacsDate processingDate,
                                                    int paymentFileIndex,
                                                    List<Payment> paymentList) {

        String nextFileSerialNumber = String.valueOf((paymentFileIndex % 9)+1);

        HeaderLabelOne headerLabelOne = buildHeaderLabelOne(
                bureauNumber,
                serviceUserNumber,
                submissionSerialNumber,
                nextFileSerialNumber,
                paymentFileIndex,
                processingDate);
        HeaderLabelTwo headerLabelTwo = buildHeaderLabelTwo();
        UserHeaderLabelOne userHeaderLabelOne = buildUserHeaderLabelOne(processingDate, paymentFileIndex);
        Headers headers = new Headers(headerLabelOne, headerLabelTwo, userHeaderLabelOne);

        java.util.List<MultiPayDayDataRecord> dataRecords = paymentList
                .map(p -> buildPaymentInstruction(p))
                .collect(Collectors.toList());

        EndOfFileLabelOne eofTrailerLabelOne = buildEndOfFileTrailerLabelOne(
                bureauNumber,
                serviceUserNumber,
                nextFileSerialNumber,
                submissionSerialNumber,
                paymentFileIndex,
                processingDate);
        EndOfFileLabelTwo eofTrailerLabelTwo = buildEndOfFileTrailerLabelTwo();
        UserTrailerLabelOne userTrailerLabelOne = buildUserTrailerLabelOne(dataRecords.size(), getCreditValueTotal(dataRecords));
        Trailers trailers = new Trailers(eofTrailerLabelOne, eofTrailerLabelTwo, userTrailerLabelOne);

        return PaymentFile.builder()
                .headers(headers)
                .dataRecords(paymentList
                        .map(p -> buildPaymentInstruction(p))
                        .collect(Collectors.toList()))
                .trailers(trailers)
                .build();
    }

    private BigInteger getCreditValueTotal(java.util.List<MultiPayDayDataRecord> paymentInstructions) {

        BigInteger creditValueTotal = BigInteger.ZERO;
        for (DataRecord paymentInstruction: paymentInstructions) {
            creditValueTotal = creditValueTotal.add(paymentInstruction.getAmount());
        }

        return creditValueTotal;
    }

    private MultiPayDayDataRecord buildPaymentInstruction(Payment payment) {

        UKBankAccount creditorAccount = payment.getCreditorAccountAs(UKBankAccount.class).get();
        UKBankAccount debtorAccount = payment.getDebtorAccount();

        String reference;
        if (creditorAccount instanceof UKBuildingSocietyAccount) {
            reference = ((UKBuildingSocietyAccount)creditorAccount).getRollNumber();
        } else {
            reference =  payment.getCreditorMetadata(MetadataField.CLIENT_REF).map(MetadataField::getValue).getOrElse("");
        }

        return MultiPayDayDataRecord
                .builder()
                .destinationSortCode(SortCode.fromString(creditorAccount.getSortCode().getValue()))
                .destinationAccountNumber(AccountNumber.fromString(creditorAccount.getAccountNumber().getValue()))
                .destinationAccountType(0) // Different only if requested by bank or b. society
                .transactionCode(TransactionCode.DIRECT_CREDIT_SO_AND_DEBIT_CONTRAS)
                .originSortCode(SortCode.fromString(debtorAccount.getSortCode().getValue()))
                .originAccountNumber(AccountNumber.fromString(debtorAccount.getAccountNumber().getValue()))
                .freeFormat(" ".repeat(4))
                .amount(payment.getAmount().scaleByPowerOfTen(2).getNumberStripped().toBigInteger())
                .serviceUserName(ServiceUserName.fromString(payment
                        .getMethodProperty(PaymentMethod.Bacs,BACS_SERVICE_ACCOUNT_NAME).getOrElse("")))
                .notValidated(" ".repeat(8))
                .reference(String.format("%18s", reference))
                .accountName(AccountName.fromString(ACCOUNT_NAME))
                .secondNotValidated(" ")
                .processingDate(getBacsDateFromInstant(payment.getLatestExecutionDate()))
                .build();
    }

    private HeaderLabelOne buildHeaderLabelOne(ServiceUserNumber bureauNumber,
                                                      ServiceUserNumber serviceUserNumber,
                                                      SubmissionSerialNumber submissionSerialNumber,
                                                      String nextFileSerialNumber,
                                                      int fileSequenceNumber,
                                                      BacsDate processingDate) {
        return HeaderLabelOne.builder()
                .labelIdentifier(HEADER_LABEL_IDENTIFIER)
                .labelNumber(HEADER_LABEL_ONE_NUMBER)
                .fileIdentifier(FileIdentifier.builder()
                        .charOne(FILE_IDENTIFIER_REQUIRED_CHARACTER_A)
                        .serviceUserNumber(serviceUserNumber) // sun
                        .charTwo(FILE_IDENTIFIER_REQUIRED_CHARACTER_S)
                        .validChars("00") // Any valid characters
                        .nextFileSerialNumber(nextFileSerialNumber)
                        .serviceUserNumberTwo(bureauNumber)
                        .build())
                .setIdentification(submissionSerialNumber)
                .fileSectionNumber(FILE_SECTION_NUMBER)
                .fileSequenceNumber(fileSequenceNumber)
                .generationNumber(GENERATION_NUMBER) // Numeric or blank
                .generationVersionNumber(GENERATION_VERSION_NUMBER) // Numeric or blank
                .creationDate(BacsDate.fromLocalDate(LocalDate.now()))
                .expirationDate(processingDate)
                .accessibilityIndicator(ACCESSIBILITY_INDICATOR)
                .blockCount(BLOCK_COUNT)
                .systemCode(SYSTEM_CODE)
                .reservedBlankSpace(" ".repeat(7))
                .build();
    }

    private HeaderLabelTwo buildHeaderLabelTwo() {
        return HeaderLabelTwo.builder()
                .labelIdentifier(HEADER_LABEL_IDENTIFIER)
                .labelNumber(HEADER_LABEL_TWO_NUMBER)
                .recordFormat(RECORD_FORMAT)
                .blockLength(BLOCK_LENGTH)
                .recordLength(FileProcessingDayType.MULTI_PROCESSING_DAY_FILE)
                .reservedChars(" ".repeat(35))
                .bufferOffset("0".repeat(2))
                .reservedChars(" ".repeat(28))
                .build();
    }

    private UserHeaderLabelOne buildUserHeaderLabelOne(BacsDate processingDate, int paymentFileIndex) {
        String fileNumber = String.format("%03d", paymentFileIndex);
        return UserHeaderLabelOne.builder()
                .labelIdentifier(USER_HEADER_LABEL_IDENTIFIER)
                .labelNumber(HEADER_LABEL_ONE_NUMBER)
                .processingDate(processingDate)
                .identifyingNumber(IDENTIFYING_NUMBER)
                .currencyCode(STERLING_CURRENCY_CODE)
                .countryCode(COUNTRY_CODE)
                .workCode(FileProcessingDayType.MULTI_PROCESSING_DAY_FILE)
                .fileNumber(fileNumber)
                .blankSpaces(" ".repeat(7))
                .auditPrintIdentifier(AuditPrintIdentifier.fromString(" ".repeat(7)))
                .reservedChars(" ".repeat(26)) // Any valid characters
                .build();
    }

    private EndOfFileLabelOne buildEndOfFileTrailerLabelOne(ServiceUserNumber bureauNumber,
                                                                    ServiceUserNumber serviceUserNumber,
                                                                    String nextFileSerialNumber,
                                                                    SubmissionSerialNumber submissionSerialNumber,
                                                                    int fileSequenceNumber,
                                                                    BacsDate processingDate) {

        return EndOfFileLabelOne.builder()
                .labelIdentifier(EOF_LABEL_IDENTIFIER)
                .labelNumber(TRAILER_LABEL_ONE_NUMBER)
                .charOne(FILE_IDENTIFIER_REQUIRED_CHARACTER_A)
                .serviceUserNumber(serviceUserNumber)
                .charTwo(FILE_IDENTIFIER_REQUIRED_CHARACTER_S)
                .validChars("00") // Any valid characters
                .nextFileSerialNumber(nextFileSerialNumber)
                .serviceUserNumberTwo(bureauNumber)
                .setIdentification(submissionSerialNumber)
                .fileSectionNumber(FILE_SECTION_NUMBER)
                .fileSequenceNumber(fileSequenceNumber)
                .generationNumber(GENERATION_NUMBER)
                .generationVersionNumber(GENERATION_VERSION_NUMBER)
                .creationDate(BacsDate.fromLocalDate(LocalDate.now()))
                .expirationDate(processingDate)
                .accessibilityIndicator(ACCESSIBILITY_INDICATOR)
                .blockCount(BLOCK_COUNT)
                .systemCode(SYSTEM_CODE)
                .reservedBlankSpace(" ".repeat(7))
                .build();
    }

    private EndOfFileLabelTwo buildEndOfFileTrailerLabelTwo() {

        return EndOfFileLabelTwo.builder()
                .labelIdentifier(EOF_LABEL_IDENTIFIER)
                .labelNumber(TRAILER_LABEL_TWO_NUMBER)
                .recordFormat(RECORD_FORMAT)
                .blockLength(BLOCK_LENGTH)
                .recordLength(FileProcessingDayType.MULTI_PROCESSING_DAY_FILE)
                .reservedChars(" ".repeat(35))
                .bufferOffset("0".repeat(2))
                .reservedChars(" ".repeat(28))
                .build();
    }

    private UserTrailerLabelOne buildUserTrailerLabelOne(int creditItemCount, BigInteger creditValueTotal) {

        return UserTrailerLabelOne.builder()
                .labelIdentifier(USER_TRAILER_LABEL_IDENTIFIER)
                .labelNumber(TRAILER_LABEL_ONE_NUMBER)
                .debitValueTotal(BigInteger.valueOf(0))
                .creditValueTotal(creditValueTotal)
                .debitItemCount(0)
                .creditItemCount(creditItemCount)
                .blankSpaces(" ".repeat(8))
                .ddiCount(0)
                .validChars(" ".repeat(21))
                .build();
    }

    private BacsDate getBacsDateFromInstant(Instant instantDate) {
        return BacsDate.fromLocalDate(LocalDate.ofInstant(instantDate, ZoneId.of(TIME_ZONE_ID)));
    }
}
