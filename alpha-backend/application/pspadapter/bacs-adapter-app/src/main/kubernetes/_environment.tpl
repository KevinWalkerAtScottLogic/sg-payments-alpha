{{- define "pspadapter-bacs-adapter-app.env" -}}
- name: SERVER_PORT
  value: {{ .Values.${projectValuesName}.port | quote }}
- name: CURRENT_PROCESSING_DATE
  value: {{ .Values.${projectValuesName}.processingDate }}
- name: ENABLE_SCHEDULER
  value: {{ .Values.${projectValuesName}.enableScheduler }}
- name: CRON_EXPRESSION_TO_ADVANCE_DAY
  value: {{ .Values.${projectValuesName}.cronExpressionToAdvanceDay }}
- name: SERVICE_BUREAU_NUMBER
  value: {{ .Values.${projectValuesName}.bureauNumber | quote }}
{{- end -}}