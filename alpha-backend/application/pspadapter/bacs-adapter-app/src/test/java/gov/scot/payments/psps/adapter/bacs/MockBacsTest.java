package gov.scot.payments.psps.adapter.bacs;

import gov.scot.payments.bacs.ReportRepository;
import gov.scot.payments.bacs.model.DailyBacsReports;
import gov.scot.payments.bacs.standard18.common.BacsCharacterUtils;
import gov.scot.payments.bacs.standard18.common.BacsDate;
import gov.scot.payments.bacs.standard18.common.model.DataRecord;
import gov.scot.payments.bacs.standard18.input.*;
import gov.scot.payments.bacs.xmladvices.BACSAdviceDocumentWrapper;
import gov.scot.payments.bacs.xmladvices.BACSParsingException;
import gov.scot.payments.psps.adapter.bacs.mock.MockBacsConfiguration;
import io.vavr.collection.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import java.io.File;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@AutoConfigurationPackage
@EntityScan({"gov.scot.payments.psps.adapter.bacs","gov.scot.payments.psps.adapter.bacs.mock"})
@DataJpaTest(properties = {"spring.datasource.url=jdbc:h2:mem:MockBacsTest"})
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=validate",
        "spring.flyway.locations=classpath:db/migration/h2",
        "spring.flyway.schemas=",
        "spring.jpa.properties.hibernate.default_schema=",
        "component.name=bacs",
        "userNumber=123456",
        "application.transaction-cost=0.01"
})
@ContextConfiguration(classes = MockBacsTest.TestApplication.class)
class MockBacsTest {

    @Autowired
    BacsService bacsService;

    @Autowired
    ReportRepository reportRepository;

    @Autowired
    BacsSerialNumberGenerator serialNumberGenerator;

    @Test
    public void testCurrentDate(){
        assertEquals(LocalDate.now(),reportRepository.getCurrentDate());
        reportRepository.saveCurrentDate(LocalDate.now().plusDays(1));
        assertEquals(LocalDate.now().plusDays(1),reportRepository.getCurrentDate());
    }

    @Test
    public void testSaveReports() throws BACSParsingException {
        assertFalse(reportRepository.getDailyReport(LocalDate.now()).isPresent());
        assertFalse(bacsService.getReports(LocalDate.now()).isPresent());
        BACSAdviceDocumentWrapper arucs = BACSAdviceDocumentWrapper.fromXmlFile(getFileFromResources("arucs-parsing-test.xml"));
        BACSAdviceDocumentWrapper awacs = BACSAdviceDocumentWrapper.fromXmlFile(getFileFromResources("awacs-parsing-test.xml"));
        reportRepository.saveDailyReports(LocalDate.now(),new DailyBacsReports(arucs.toJaxbXmlObject(),awacs.toJaxbXmlObject(),null));
        DailyBacsReports reports = bacsService.getReports(LocalDate.now()).get();
        assertEquals(arucs.toXmlString(),BACSAdviceDocumentWrapper.fromJaxbXmlObject(reports.getArucs()).toXmlString());
        assertEquals(awacs.toXmlString(),BACSAdviceDocumentWrapper.fromJaxbXmlObject(reports.getAwacs()).toXmlString());
    }

    @Test
    public void testSavePaymentFile() {
        final LocalDate now = LocalDate.now();
        PaymentFile spdFile = makePaymentFile("1",false, now,List.of(createSpdRecord("1"),createSpdRecord("2")));
        PaymentFile mpdFile = makePaymentFile("2",true, now,List.of(createMpdRecord("3",now),createMpdRecord("4",now),createMpdRecord("5",now.plusDays(1)),createMpdRecord("6",now.plusDays(2))));
        reportRepository.savePaymentFile(spdFile);
        reportRepository.savePaymentFile(mpdFile);
        assertEquals(0, reportRepository.findSpdSubmissionRecordsByDate(now.plusDays(1)).size());
        assertEquals(0, reportRepository.findMpdSubmissionRecordsByDate(now.plusDays(3)).size());
        java.util.List<SubmissionDataRecord> spdRecords = reportRepository.findSpdSubmissionRecordsByDate(now);
        assertEquals(2,spdRecords.size());
        java.util.List<MultiPayDayDataRecord> mpdRecords = reportRepository.findMpdSubmissionRecordsByDate(now);
        assertEquals(2,mpdRecords.size());
        mpdRecords = reportRepository.findMpdSubmissionRecordsByDate(now.plusDays(1));
        assertEquals(1,mpdRecords.size());

    }

    @Test
    public void testMockService(){
        ServiceUserNumber bureauNumber = ServiceUserNumber.fromString("123456");
        final LocalDate now = LocalDate.now();
        reportRepository.saveCurrentDate(now.plusDays(1));
        PaymentFile spdFile = makePaymentFile("1",false, now,List.of(createSpdRecord("1","112233","11223344"),createSpdRecord("2")));
        PaymentFile mpdFile1 = makePaymentFile("2",true, now,List.of(createMpdRecord("3",now),createMpdRecord("4",now),createMpdRecord("5",now.plusDays(1),"111111","11111111"),createMpdRecord("6",now.plusDays(2))));
        PaymentFile mpdFile2 = makePaymentFile("3",true, now,List.of(createMpdRecord("7",now,"111111","11111111"),createMpdRecord("8",now),createMpdRecord("9",now.plusDays(1)),createMpdRecord("10",now.plusDays(2))));
        bacsService.submit(BacsSubmissionFile.builder()
                                             .paymentFile(spdFile)
                                             .volumeHeaderLabelOne(BacsSubmissionFileBuilder.getVolumeHeaderLabelOne(bureauNumber,serialNumberGenerator.generate()))
                                             .build());
        bacsService.submit(BacsSubmissionFile.builder()
                                             .paymentFile(mpdFile1)
                                             .paymentFile(mpdFile2)
                                             .volumeHeaderLabelOne(BacsSubmissionFileBuilder.getVolumeHeaderLabelOne(bureauNumber,serialNumberGenerator.generate()))
                                             .build());
        assertEquals(0, reportRepository.findSpdSubmissionRecordsByDate(now.plusDays(1)).size());
        assertEquals(0, reportRepository.findMpdSubmissionRecordsByDate(now.plusDays(3)).size());
        java.util.List<SubmissionDataRecord> spdRecords = reportRepository.findSpdSubmissionRecordsByDate(now);
        assertEquals(2,spdRecords.size());
        java.util.List<MultiPayDayDataRecord> mpdRecords = reportRepository.findMpdSubmissionRecordsByDate(now);
        assertEquals(4,mpdRecords.size());
        mpdRecords = reportRepository.findMpdSubmissionRecordsByDate(now.plusDays(1));
        assertEquals(2,mpdRecords.size());

        bacsService.updateProcessingDay(now.plusDays(2));
        DailyBacsReports reports = bacsService.getReports(now.plusDays(2)).get();
        assertEquals(1,reports.getAwacs().getData().getMessagingAdvices().getMessagingAdvice().size());
        assertEquals("1",reports.getAwacs().getData().getMessagingAdvices().getMessagingAdvice().get(0).getReference().trim());
        assertEquals(1,reports.getArucs().getData().getARUCS().getAdvice().getOriginatingAccountRecords().getOriginatingAccountRecord().get(0).getReturnedCreditItem().size());
        assertEquals("7",reports.getArucs().getData().getARUCS().getAdvice().getOriginatingAccountRecords().getOriginatingAccountRecord().get(0).getReturnedCreditItem().get(0).getRef().trim());
    }

    private SubmissionDataRecord createSpdRecord(String ref){
        return createSpdRecord(ref,"123456","12345678");
    }

    private SubmissionDataRecord createSpdRecord(String ref,String sortCode,String accountNumber){
        String spdRowString = createSpdString(ref,sortCode, accountNumber);
        return SubmissionDataRecord.fromString(spdRowString);
    }

    private MultiPayDayDataRecord createMpdRecord(String ref, LocalDate date){
        return createMpdRecord(ref, date,"123456","12345678");
    }

    private MultiPayDayDataRecord createMpdRecord(String ref, LocalDate date,String sortCode,String accountNumber){
        final BacsDate processingDate = BacsDate.fromLocalDate(date);
        return MultiPayDayDataRecord.fromString(createSpdString(ref, sortCode, accountNumber)+processingDate.toString(), processingDate);
    }

    private String createSpdString(String ref,String sortCode,String accountNumber) {
        String accountType ="0";
        String transactionCode="99";
        String amount = "00000000123";
        String serviceUserName ="1234567890";
        String reference = String.format("%18s",ref);
        String accountName ="12"+ BacsCharacterUtils.createEmptyCharString(15);
        return sortCode + accountNumber + accountType + transactionCode + sortCode + accountNumber +
                BacsCharacterUtils.createEmptyCharString(4) + amount + serviceUserName +
                BacsCharacterUtils.createEmptyCharString(8) + reference + accountName + " ";
    }

    private PaymentFile makePaymentFile(String id, boolean multiDay,LocalDate date, List<DataRecord> records){
        final Headers headers = new Headers(HeaderLabelOne.builder()
                                                          .setIdentification(SubmissionSerialNumber.fromString(id)).fileSequenceNumber(1)
                                                          .build()
                , null
                , UserHeaderLabelOne.builder()
                                    .processingDate(BacsDate.fromLocalDate(date))
                                    .workCode(multiDay ? FileProcessingDayType.MULTI_PROCESSING_DAY_FILE : FileProcessingDayType.SINGLE_PROCESSING_DAY_FILE)
                                    .build());
        return PaymentFile.builder()
                          .headers(headers)
                          .dataRecords(records.toJavaList())
                          .build();

    }

    private File getFileFromResources(String fileName) {
        return new File(this.getClass().getClassLoader().getResource(fileName).getFile());
    }

    @Import(MockBacsConfiguration.class)
    @EnableAutoConfiguration(exclude = {HypermediaAutoConfiguration.class})
    public static class TestApplication {

        @Bean
        public BacsSerialNumberGenerator bacsSerialNumberGenerator(BacsSubmissionSerialNumberRepository bacsSubmissionSerialNumberRepository){
            return new JpaBacsSerialNumberGenerator(bacsSubmissionSerialNumberRepository);
        }
    }
}