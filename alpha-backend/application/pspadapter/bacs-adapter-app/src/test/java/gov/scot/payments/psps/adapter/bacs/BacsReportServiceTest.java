package gov.scot.payments.psps.adapter.bacs;

import gov.scot.payments.bacs.model.DailyBacsReports;
import gov.scot.payments.bacs.standard18.input.SubmissionSerialNumber;
import gov.scot.payments.bacs.xmladvices.advices.ARUCS;
import gov.scot.payments.bacs.xmladvices.advices.AccountType;
import gov.scot.payments.bacs.xmladvices.advices.Advice;
import gov.scot.payments.bacs.xmladvices.advices.BACSDocument;
import gov.scot.payments.bacs.xmladvices.advices.MessagingAdvice;
import gov.scot.payments.bacs.xmladvices.advices.MessagingAdvices;
import gov.scot.payments.bacs.xmladvices.advices.OriginatingAccountRecord;
import gov.scot.payments.bacs.xmladvices.advices.OriginatingAccountRecords;
import gov.scot.payments.bacs.xmladvices.advices.ReturnedItemType;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.psps.adapter.PostSubmissionEventSender;
import gov.scot.payments.psps.method.model.PspCompleteResponse;
import gov.scot.payments.psps.method.model.PspReturnResponse;
import gov.scot.payments.psps.method.model.PspSubmissionRequest;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import reactor.core.publisher.Mono;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

@AutoConfigurationPackage
@EntityScan("gov.scot.payments.psps.adapter.bacs")
@DataJpaTest(properties = {"spring.datasource.url=jdbc:h2:mem:BacsReportServiceTest"})
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=validate",
        "spring.flyway.locations=classpath:db/migration/h2",
        "spring.flyway.schemas=",
        "spring.jpa.properties.hibernate.default_schema=",
        "component.name=bacs",
        "userNumber=123456",
        "application.transaction-cost=0.01"
})
@ContextConfiguration(classes = BacsReportServiceTest.TestApplication.class)
class BacsReportServiceTest {

    @MockBean
    BacsService bacsService;

    @MockBean
    PostSubmissionEventSender postSubmissionEventSender;

    @Autowired BacsReportService bacsReportService;
    @Autowired BacsPaymentKeyRepository paymentKeyRepository;
    @Autowired BacsReportStatusRepository bacsReportStatusRepository;

    @Test
    public void testStorePayentKeys(){
        var payment1 = makePayment("1");
        var payment2 = makePayment("2",UKBuildingSocietyAccount.builder()
                .name("act")
                .sortCode(SortCode.fromString("123456"))
                .accountNumber(UKAccountNumber.fromString("12345678"))
                .currency("GBP")
                .rollNumber("12345")
                .build());
        final LocalDate now = LocalDate.parse("2020-02-27");
        LocalDate nextWeek = now.plusDays(7);
        var payment3 = makePayment("3").toBuilder().latestExecutionDate(nextWeek.atStartOfDay().toInstant(ZoneOffset.UTC)).build();

        PspSubmissionRequest request = PspSubmissionRequest
                .builder()
                .payments(List.of(payment1,payment2,payment3))
                .build();

        bacsReportService.storePaymentKeys(request);
        assertEquals(3,paymentKeyRepository.count());
        assertEquals(LocalDate.ofInstant(payment1.getLatestExecutionDate(), ZoneId.of("UTC")),paymentKeyRepository.findById(payment1.getId()).get().getProcessingDate());
        assertEquals("2020-02-27;123456-12345678;123456-12345678;GBP 100;1",paymentKeyRepository.findById(payment1.getId()).get().getKey());
        assertEquals(LocalDate.ofInstant(payment2.getLatestExecutionDate(), ZoneId.of("UTC")),paymentKeyRepository.findById(payment2.getId()).get().getProcessingDate());
        assertEquals("2020-02-27;123456-12345678;123456-12345678;GBP 100;12345",paymentKeyRepository.findById(payment2.getId()).get().getKey());
        assertEquals(1,paymentKeyRepository.findByProcessingDate(nextWeek).size());
        paymentKeyRepository.deleteByProcessingDate(nextWeek);
        assertEquals(0,paymentKeyRepository.findByProcessingDate(nextWeek).size());
    }


    @Test
    public void testGenerateDatesEmptyToday(){
        List<LocalDate> dates = bacsReportService.getUnreportedDates(LocalDate.now());
        assertEquals(1,dates.size());
        assertEquals(LocalDate.now(),dates.get(0));
    }

    @Test
    public void testGenerateDatesNowBeforeLatest(){
        bacsReportStatusRepository.save(BacsReportStatus.builder().id(LocalDate.now().plusDays(1)).build());
        List<LocalDate> dates = bacsReportService.getUnreportedDates(LocalDate.now());
        assertEquals(0,dates.size());
    }

    @Test
    public void testGenerateDatesNowEqualLatest(){
        bacsReportStatusRepository.save(BacsReportStatus.builder().id(LocalDate.now()).build());
        List<LocalDate> dates = bacsReportService.getUnreportedDates(LocalDate.now());
        assertEquals(0,dates.size());
    }

    @Test
    public void testGenerateDatesNowAfterLatest(){
        bacsReportStatusRepository.save(BacsReportStatus.builder().id(LocalDate.now().minusDays(7)).build());
        List<LocalDate> dates = bacsReportService.getUnreportedDates(LocalDate.now());
        assertEquals(7,dates.size());
    }

    @Test
    public void testPollReportNoReports(){
        LocalDate date = LocalDate.now();
        when(bacsService.getReports(any())).thenReturn(Optional.empty());
        bacsReportService.pollReport(date);
        assertEquals(0,paymentKeyRepository.count());
        verifyNoInteractions(postSubmissionEventSender);
        assertTrue(bacsReportStatusRepository.findById(date).isPresent());
    }

    @Test
    public void testPollReportWithReports() throws DatatypeConfigurationException {
        LocalDate date = LocalDate.parse("2020-02-27");
        var payment1 = makePayment("1");
        var payment2 = makePayment("2");
        var payment3 = makePayment("3");
        var payment4 = makePayment("4");
        paymentKeyRepository.saveAll(List.of(bacsReportService.buildBacsKey(payment1)
                ,bacsReportService.buildBacsKey(payment2)
                ,bacsReportService.buildBacsKey(payment3)
                ,bacsReportService.buildBacsKey(payment4)));
        BACSDocument awacsReport = new BACSDocument();
        final BACSDocument.Data awacsData = new BACSDocument.Data();
        MessagingAdvices awacs = mock(MessagingAdvices.class);
        MessagingAdvice awacsreturn1 = new MessagingAdvice();
        awacsreturn1.setReasonCode("ret1");
        awacsreturn1.setPayerAccountNumber(12345678);
        awacsreturn1.setPayerSortCode("123456");
        awacsreturn1.setOrigAccountNumber(12345678);
        awacsreturn1.setOrigSortCode("123456");
        awacsreturn1.setAmountOfPayment(100);
        awacsreturn1.setReference("4");
        awacsreturn1.setOriginalProcDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(date.toString()));
        when(awacs.getMessagingAdvice()).thenReturn(List.of(awacsreturn1).toJavaList());
        awacsData.setMessagingAdvices(awacs);
        awacsReport.setData(awacsData);
        BACSDocument arucsReport = new BACSDocument();
        final BACSDocument.Data arucsData = new BACSDocument.Data();
        ARUCS arucs = new ARUCS();
        Advice arucsAdvice = new Advice();
        OriginatingAccountRecords arucsOriginatingRecords = mock(OriginatingAccountRecords.class);
        OriginatingAccountRecord arucsOriginatingRecord = mock(OriginatingAccountRecord.class);
        ReturnedItemType arucsReturn1 = new ReturnedItemType();
        arucsReturn1.setValueOf(new BigDecimal("1"));
        arucsReturn1.setReturnCode("ret");
        arucsReturn1.setReturnDescription("returned");
        arucsReturn1.setOriginalProcessingDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(date.toString()));
        arucsReturn1.setRef("2");
        AccountType accountType = new AccountType();
        accountType.setSortCode("123456");
        accountType.setNumber("12345678");
        arucsReturn1.setPayerAccount(accountType);
        arucsReturn1.setReceiverAccount(accountType);

        when(arucsOriginatingRecord.getReturnedCreditItem()).thenReturn(List.of(arucsReturn1).toJavaList());
        when(arucsOriginatingRecords.getOriginatingAccountRecord()).thenReturn(List.of(arucsOriginatingRecord).toJavaList());
        arucsAdvice.setOriginatingAccountRecords(arucsOriginatingRecords);
        arucs.setAdvice(arucsAdvice);
        arucsData.setARUCS(arucs);
        arucsReport.setData(arucsData);
        DailyBacsReports reports = new DailyBacsReports(arucsReport,awacsReport,null);
        when(bacsService.getReports(any())).thenReturn(Optional.of(reports));
        when(postSubmissionEventSender.sendComplete(any())).thenReturn(Mono.empty());
        when(postSubmissionEventSender.sendReturn(any())).thenReturn(Mono.empty());
        bacsReportService.pollReport(date);
        ArgumentCaptor<PspCompleteResponse> completeCaptor = ArgumentCaptor.forClass(PspCompleteResponse.class);
        ArgumentCaptor<PspReturnResponse> returnsCaptor = ArgumentCaptor.forClass(PspReturnResponse.class);
        verify(postSubmissionEventSender,times(2)).sendComplete(completeCaptor.capture());
        verify(postSubmissionEventSender,times(2)).sendReturn(returnsCaptor.capture());
        assertEquals(0,paymentKeyRepository.findByProcessingDate(date).size());
        assertTrue(bacsReportStatusRepository.findById(date).isPresent());
        List<PspCompleteResponse> completeResponses = List.ofAll(completeCaptor.getAllValues());
        List<PspReturnResponse> returnResponses = List.ofAll(returnsCaptor.getAllValues());
        assertEquals(payment1.getId(),completeResponses.get(0).getPaymentId());
        assertEquals(payment3.getId(),completeResponses.get(1).getPaymentId());
        assertEquals(payment2.getId(),returnResponses.get(0).getPaymentId());
        assertEquals("ret",returnResponses.get(0).getCode());
        assertEquals(payment4.getId(),returnResponses.get(1).getPaymentId());
        assertEquals("ret1",returnResponses.get(1).getCode());
    }


    private Payment makePayment(String ref) {
        return makePayment(ref,UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build());
    }

    private Payment makePayment(String ref, CashAccount account) {
        return Payment.builder()
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.Bacs))
                .debtor(PartyIdentification.builder().name("").build())
                .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                .creditor(PartyIdentification.builder().name("").build())
                .creditorAccount(account)
                .amount(Money.parse("GBP 1.00"))
                .latestExecutionDate(LocalDate.parse("2020-02-27").atStartOfDay().toInstant(ZoneOffset.UTC))
                .creditorMetadata(List.of(MetadataField.clientRef(ref)))
                .status(PaymentStatus.ReadyForSubmission)
                .product(new CompositeReference("", ""))
                .build();
    }

    @Import(BacsAdapterConfiguration.class)
    @EnableAutoConfiguration(exclude = {HypermediaAutoConfiguration.class})
    public static class TestApplication {

    }
}