package gov.scot.payments.psps.adapter.bacs;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.psps.method.model.PspSubmissionRequest;
import gov.scot.payments.psps.method.model.PspTransactionCostEstimateResponse;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import javax.money.Monetary;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ApplicationIntegrationTest(classes = {BacsAdapterApplicationIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.CUSTOM
        ,bindings = {"responses"}
        ,properties = {"application.transaction_cost=0.01"})
class BacsAdapterApplicationIntegrationTest {

    @Test
    public void testEstimateTransactionCosts(@Autowired WebTestClient client){

        List<Payment> payments = List.of(makePayment(), makePayment(), makePayment(), makePayment(), makePayment(), makePayment());

        var costEstimateResponse = client
                .post()
                .uri("/estimateTransactionCosts")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(PspSubmissionRequest.builder().payments(payments).build())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(PspTransactionCostEstimateResponse.class)
                .returnResult();

        PspTransactionCostEstimateResponse costEstimate = costEstimateResponse.getResponseBody();
        assertEquals("bacs-adapter", costEstimate.getPsp());
        assertEquals(Money.of(payments.size()*0.01, Monetary.getCurrency("GBP")), costEstimate.getAmount());
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends BacsAdapterApplication{

    }

    private Payment makePayment() {
        return Payment.builder()
                .createdBy("user")
                .allowedMethods(List.of(PaymentMethod.Bacs))
                .creditor(PartyIdentification.builder().name("").build())
                .creditorAccount(UKBankAccount.builder()
                        .currency("GBP")
                        .accountNumber(UKAccountNumber.builder()
                                .value("12345678")
                                .build())
                        .sortCode(SortCode.fromString("123456"))
                        .name("Account Name")
                        .build())
                .debtor(PartyIdentification.builder()
                        .name("Debtor Name")
                        .build())
                .debtorAccount(UKBankAccount.builder()
                        .currency("GBP")
                        .sortCode(SortCode.fromString("654321"))
                        .accountNumber(UKAccountNumber.builder()
                                .value("87654321")
                                .build())
                        .name("Account Name")
                        .build())
                .amount(Money.parse("GBP 1.00"))
                .latestExecutionDate(Instant.now())
                .product(new CompositeReference("",""))
                .build();
    }

}