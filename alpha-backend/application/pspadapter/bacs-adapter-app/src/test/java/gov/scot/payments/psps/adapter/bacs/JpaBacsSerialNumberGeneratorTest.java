package gov.scot.payments.psps.adapter.bacs;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.bacs.standard18.input.SubmissionSerialNumber;
import gov.scot.payments.psps.adapter.PostSubmissionEventSender;
import gov.scot.payments.testing.spring.TestConfiguration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.*;

@AutoConfigurationPackage
@EntityScan("gov.scot.payments.psps.adapter.bacs")
@DataJpaTest(properties = {"spring.datasource.url=jdbc:h2:mem:JpaBacsSerialNumberGeneratorTest"})
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=validate",
        "spring.flyway.locations=classpath:db/migration/h2",
        "spring.flyway.schemas=",
        "spring.jpa.properties.hibernate.default_schema=",
        "component.name=bacs",
        "userNumber=123456",
        "application.transaction-cost=0.01"
})
@ContextConfiguration(classes = JpaBacsSerialNumberGeneratorTest.TestApplication.class)
class JpaBacsSerialNumberGeneratorTest {

    @MockBean
    BacsService bacsService;

    @MockBean
    PostSubmissionEventSender postSubmissionEventSender;

    @Autowired
    BacsSerialNumberGenerator bacsSerialNumberGenerator;

    @Test
    public void test(){
        SubmissionSerialNumber ssn = bacsSerialNumberGenerator.generate();
        assertEquals("000001",ssn.getValue());
        ssn = bacsSerialNumberGenerator.generate();
        assertEquals("000002",ssn.getValue());
        ssn = bacsSerialNumberGenerator.generate();
        assertEquals("000003",ssn.getValue());
    }

    @Import(BacsAdapterConfiguration.class)
    @EnableAutoConfiguration(exclude = {HypermediaAutoConfiguration.class})
    public static class TestApplication {

    }
}