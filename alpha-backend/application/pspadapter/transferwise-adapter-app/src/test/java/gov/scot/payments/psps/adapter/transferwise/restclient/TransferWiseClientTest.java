package gov.scot.payments.psps.adapter.transferwise.restclient;

import gov.scot.payments.psps.adapter.transferwise.model.TransferWisePaymentInstruction;
import gov.scot.payments.psps.adapter.transferwise.model.balance.TransferWiseAccountBalance;
import gov.scot.payments.psps.adapter.transferwise.model.balance.TransferWiseCheckBalanceResponse;
import gov.scot.payments.psps.adapter.transferwise.model.balance.TransferWiseCurrencyAmount;
import gov.scot.payments.psps.adapter.transferwise.model.quote.SubmitQuoteException;
import gov.scot.payments.psps.adapter.transferwise.model.quote.TransferWiseQuote;
import gov.scot.payments.psps.adapter.transferwise.model.quote.TransferWiseQuoteResponse;
import gov.scot.payments.psps.adapter.transferwise.model.recipient.TransferWiseAddRecipientException;
import gov.scot.payments.psps.adapter.transferwise.model.recipient.TransferWiseCurrencyException;
import gov.scot.payments.psps.adapter.transferwise.model.recipient.TransferWiseRecipient;
import gov.scot.payments.psps.adapter.transferwise.model.recipient.TransferWiseRecipientResponse;
import gov.scot.payments.psps.adapter.transferwise.model.recipient.bankdetails.TransferWiseEURBankDetails;
import gov.scot.payments.psps.adapter.transferwise.model.recipient.bankdetails.TransferWiseGBPBankDetails;
import gov.scot.payments.psps.adapter.transferwise.model.recipient.bankdetails.TransferWiseUSDBankDetails;
import gov.scot.payments.psps.adapter.transferwise.model.transfer.TransferWiseFundTransferException;
import gov.scot.payments.psps.adapter.transferwise.model.transfer.TransferWiseTransfer;
import gov.scot.payments.psps.adapter.transferwise.model.transfer.TransferWiseTransferResponse;
import gov.scot.payments.psps.adapter.transferwise.model.transfer.TransferWiseFundTransferResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static gov.scot.payments.testing.matchers.BigDecimalMatcher.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TransferWiseClientTest {

    @Mock
    private TransferWiseApi transferwiseApi;

    private int profileId = 0;

    @Captor
    private ArgumentCaptor<TransferWiseRecipient<TransferWiseGBPBankDetails>> captorGBPRecipient;

    @Captor
    private ArgumentCaptor<TransferWiseRecipient<TransferWiseUSDBankDetails>> captorUSDRecipient;

    @Captor
    private ArgumentCaptor<TransferWiseRecipient<TransferWiseEURBankDetails>> captorEURRecipient;

    @Captor
    private ArgumentCaptor<TransferWiseQuote> captorQuote;

    @InjectMocks
    private TransferWiseClient transferwiseClient = new TransferWiseClient(transferwiseApi, profileId);

    private int transferId;
    private TransferWiseFundTransferResponse transferStatusResponse;
    private int accountId;
    private String currencyGBP = "GBP";
    private BigDecimal amountGBP;

    void setUpTransferTest() throws TransferWiseAddRecipientException, SubmitQuoteException, TransferWiseFundTransferException {

        transferId = 5;
        transferStatusResponse = new TransferWiseFundTransferResponse();
        when(transferwiseApi.addRecipient(any(TransferWiseRecipient.class))).thenReturn(this.createRecipientResponse(1));
        when(transferwiseApi.submitQuote(any(TransferWiseQuote.class))).thenReturn(createQuoteResponse(2));
        when(transferwiseApi.submitTransfer(any(TransferWiseTransfer.class))).thenReturn(createTransferResponse(transferId));
        when(transferwiseApi.fundTransfer(transferId)).thenReturn(transferStatusResponse);
    }

    void setUpCurrencyTest(){
        TransferWiseCheckBalanceResponse checkBalanceResponse;checkBalanceResponse = new TransferWiseCheckBalanceResponse();
        accountId = 1;
        String currencyEUR = "EUR";
        amountGBP = new BigDecimal("500.00");
        BigDecimal amountEUR = new BigDecimal("200.20");
        List<TransferWiseAccountBalance> accountBalances = Arrays.asList(
                this.createAccountBalance(currencyGBP, amountGBP)
                , this.createAccountBalance(currencyEUR, amountEUR)
        );
        checkBalanceResponse.setBalances(accountBalances);
        when(transferwiseApi.checkAccountBalance(accountId)).thenReturn(checkBalanceResponse);
    }

    @Test
    @DisplayName("Check that a GBP transfer is created for the correct amount.")
    void checkGBPTransferCreated() throws TransferWiseAddRecipientException, SubmitQuoteException, TransferWiseCurrencyException, TransferWiseFundTransferException {
        this.setUpTransferTest();
        TransferWisePaymentInstruction paymentInstruction = this.getGBPPaymentInstruction(new UUID(1, 2));
        assertEquals(transferwiseClient.payInstruction(paymentInstruction), transferStatusResponse);
        verify(transferwiseApi, times(1)).addRecipient(captorGBPRecipient.capture());
        verify(transferwiseApi, times(1)).submitQuote(captorQuote.capture());
        verify(transferwiseApi, times(1)).submitTransfer(any(TransferWiseTransfer.class));
        verify(transferwiseApi, times(1)).fundTransfer(transferId);

        assertEquals(captorGBPRecipient.getValue().getDetails().getClass(), TransferWiseGBPBankDetails.class);
        assertEquals(captorQuote.getValue().getSourceAmount(), paymentInstruction.getAmount());
    }

// USD Payments - not applicable to alpha

//    @Test
//    @DisplayName("Check that a USD transfer is created for the correct amount.")
//    void checkUSDTransferCreated() throws TransferWiseAddRecipientException, SubmitQuoteException, TransferWiseCurrencyException, TransferWiseFundTransferException {
//        this.setUpTransferTest();
//        TransferWisePaymentInstruction paymentInstruction = this.getUSDPaymentInstruction(new UUID(1, 2));
//        assertEquals(transferwiseClient.payInstruction(paymentInstruction), transferStatusResponse);
//
//        verify(transferwiseApi, times(1)).addRecipient(captorUSDRecipient.capture());
//        verify(transferwiseApi, times(1)).submitQuote(captorQuote.capture());
//        verify(transferwiseApi, times(1)).submitTransfer(any(TransferWiseTransfer.class));
//        verify(transferwiseApi, times(1)).fundTransfer(transferId);
//        assertEquals(captorUSDRecipient.getValue().getDetails().getClass(), TransferWiseUSDBankDetails.class);
//        assertEquals(captorQuote.getValue().getSourceAmount(), paymentInstruction.getAmount());
//    }

    @Test
    @DisplayName("Check that a EUR transfer is created for the correct amount.")
    void checkEURTransferCreated() throws TransferWiseAddRecipientException, SubmitQuoteException, TransferWiseCurrencyException, TransferWiseFundTransferException {
        this.setUpTransferTest();
        TransferWisePaymentInstruction paymentInstruction = this.getEURPaymentInstruction(new UUID(1, 2));
        assertEquals(transferwiseClient.payInstruction(paymentInstruction), transferStatusResponse);

        verify(transferwiseApi, times(1)).addRecipient(captorEURRecipient.capture());
        verify(transferwiseApi, times(1)).submitQuote(captorQuote.capture());
        verify(transferwiseApi, times(1)).submitTransfer(any(TransferWiseTransfer.class));
        verify(transferwiseApi, times(1)).fundTransfer(transferId);
        assertEquals(captorEURRecipient.getValue().getDetails().getClass(), TransferWiseEURBankDetails.class);
        assertEquals(captorQuote.getValue().getSourceAmount(), paymentInstruction.getAmount());
    }

    @Test
    @DisplayName("Check that a payment request for an unrecognised currency throws an error.")
    void checkUnknownCurrencyThrowsCurrencyError() {
        TransferWisePaymentInstruction paymentInstruction = TransferWisePaymentInstruction.builder()
                .id(new UUID(1,2))
                .targetCurrency("CAN")
                .amount(new BigDecimal("20.04"))
                .accountHolderName("Bill Thomas")
                .sortCode("11-22-05")
                .accountNumber("12345678")
                .build();
        assertThrows(TransferWiseCurrencyException.class, () -> {
            transferwiseClient.payInstruction(paymentInstruction);
        });
    }

    @Test
    @DisplayName("Check that the correct currency balance is returned")
    void checkCorrectCurrencyBalanceReturned() throws TransferWiseCurrencyException {
        setUpCurrencyTest();
        assertThat(transferwiseClient.checkBalanceByCurrency(currencyGBP, accountId), is(equalTo(amountGBP)));
    }

    @Test
    @DisplayName("Check that an unrecognised currency throws a TransferWiseCurrencyException")
    void checkIncorrectCurrencyThrowsTransferWiseCurrencyException(){
        setUpCurrencyTest();
        assertThrows(TransferWiseCurrencyException.class, () -> {
            transferwiseClient.checkBalanceByCurrency("CAN", accountId);
        });
    }

    private TransferWisePaymentInstruction getGBPPaymentInstruction(UUID id) {
        return TransferWisePaymentInstruction.builder()
                .id(id)
                .targetCurrency("GBP")
                .amount(new BigDecimal("45.24"))
                .accountHolderName("Bill Smith")
                .sortCode("40-30-20")
                .accountNumber("12345678")
                .build();
    }

    private TransferWisePaymentInstruction getUSDPaymentInstruction(UUID id) {
        return TransferWisePaymentInstruction.builder()
                .id(id)
                .targetCurrency("USD")
                .amount(new BigDecimal("45.00"))
                .accountHolderName("Mike Bobby")
                .accountNumber("12345678")
                .country("GB")
                .city("London")
                .postCode("10025")
                .firstLine("50 Branson Ave")
                .abartn("111000025")
                .build();
    }

    private TransferWiseAccountBalance createAccountBalance(String currency, BigDecimal balance) {
        return TransferWiseAccountBalance.builder()
                .balanceType("")
                .currency(currency)
                .amount(new TransferWiseCurrencyAmount(balance, currency))
                .build();
    }

    private TransferWisePaymentInstruction getEURPaymentInstruction(UUID id) {
        return TransferWisePaymentInstruction.builder()
                .id(id)
                .targetCurrency("EUR")
                .amount(new BigDecimal("45.64"))
                .accountHolderName("Rob Taylor")
                .IBAN("DE89370400440532013000")
                .build();
    }

    private TransferWiseRecipientResponse createRecipientResponse(int id) {
        TransferWiseRecipientResponse recipientResponse = new TransferWiseRecipientResponse();
        recipientResponse.setId(id);
        return recipientResponse;
    }

    private TransferWiseQuoteResponse createQuoteResponse(int id) {
        TransferWiseQuoteResponse quoteResponse = new TransferWiseQuoteResponse();
        quoteResponse.setId(id);
        return quoteResponse;
    }

    private TransferWiseTransferResponse createTransferResponse(int id) {
        TransferWiseTransferResponse transferResponse = new TransferWiseTransferResponse();
        transferResponse.setId(id);
        return transferResponse;
    }
}
