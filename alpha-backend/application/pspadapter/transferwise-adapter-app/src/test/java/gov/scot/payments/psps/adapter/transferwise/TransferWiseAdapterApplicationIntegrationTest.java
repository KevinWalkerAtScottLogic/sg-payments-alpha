package gov.scot.payments.psps.adapter.transferwise;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.psps.adapter.PostSubmissionEventSender;
import gov.scot.payments.psps.adapter.transferwise.restclient.AuthToken;
import gov.scot.payments.psps.adapter.transferwise.restclient.TransferWiseApi;
import gov.scot.payments.psps.adapter.transferwise.restclient.TransferWiseClient;
import gov.scot.payments.psps.method.model.PspTransactionCostEstimateResponse;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.TestConfiguration;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@ApplicationIntegrationTest(classes = {TransferWiseAdapterApplicationIntegrationTest.TestApplication.class}
        , componentType = ApplicationIntegrationTest.ComponentType.CUSTOM
        , bindings = {"responses"}
        , properties = {"context.name=transferwise"
        , "component.name=transferwise"
        , "transferwise.profile_id=0"
        , "transferwise.token=123"
        , "transferwise.fixed_transaction_cost=0.80"
        , "transferwise.transaction_cost_percentage=0.35"}
)
class TransferWiseAdapterApplicationIntegrationTest {

    @Test
    @DisplayName("Given a GBP amount when call estimateTransactionCosts endpoint then the correct cost is returned")
    public void whenCallEstimateTransactionCostsThenCorrectCostReturned(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient, @Autowired PostSubmissionEventSender eventSender) {

        client
                .get()
                .uri(uri -> uri.path("/estimateTransactionCosts")
                        .queryParam("amount", "100.00")
                        .queryParam("currency", "GBP")
                        .build())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(PspTransactionCostEstimateResponse.class)
                .isEqualTo(
                        PspTransactionCostEstimateResponse.builder()
                                .psp("transferwise")
                                .amount(Money.parse("GBP 1.15"))  // 0.80 + 0.35
                                .build()
                )
        ;
    }

    @Test
    @DisplayName("Given a non GBP amount when call estimateTransactionCosts endpoint then error is returned")
    public void givenNonGBPAmountWhenCallEstimateTransactionCostsThenErrorReturned(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient, @Autowired PostSubmissionEventSender eventSender) {

        client
                .get()
                .uri(uri -> uri.path("/estimateTransactionCosts")
                        .queryParam("amount", "100.00")
                        .queryParam("currency", "EUR")
                        .build())
                .exchange()
                .expectStatus()
                .is5xxServerError()
                .expectBody()
                .jsonPath("$.message").isEqualTo("Currency EUR is not supported")
        ;
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends TransferWiseAdapterApplication {

        @Bean
        TransferWiseApi transferWiseApi() {
            return new TransferWiseApi();
        }

        @Bean
        public TransferWiseClient transferwiseClient(TransferWiseApi transferWiseApi, @Value("${transferwise.profile_id}") int profileId) {
            return new TransferWiseClient(transferWiseApi, profileId);
        }

        @Bean
        public AuthToken authToken() {
            return new AuthToken();
        }

        @Bean
        RestOperations restOperations() {
            return new RestTemplate();
        }
    }
}