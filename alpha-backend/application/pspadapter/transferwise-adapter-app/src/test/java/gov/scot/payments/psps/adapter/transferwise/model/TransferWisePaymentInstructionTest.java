package gov.scot.payments.psps.adapter.transferwise.model;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import io.vavr.collection.List;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.money.Monetary;
import java.time.Instant;

import static gov.scot.payments.payments.model.aggregate.PaymentMethod.FasterPayments;
import static gov.scot.payments.payments.model.aggregate.PaymentMethod.Sepa_DC;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransferWisePaymentInstructionTest {

    @Test
    @DisplayName("TransferWise payment instruction generated from valid SEPA payment")
    void givenValidSEPAPaymentGeneratesAPaymentInstruction() {

        String name = "Some Name";
        String iban = "SE35 5000 0000 0549 1000 0003";
        String currency = "EUR";
        Money amount = Money.of(123, Monetary.getCurrency(currency));
        var creditor = PartyIdentification.builder().name("creditor").build();
        var debtor = PartyIdentification.builder().name("debtor").build();

        SepaBankAccount sepaBankAccount = SepaBankAccount.builder()
                .name(name)
                .currency(Monetary.getCurrency(currency).getCurrencyCode())
                .iban(iban)
                .build();

        UKBankAccount ukBankAccount = UKBankAccount.builder()
                .name(name)
                .currency(Monetary.getCurrency("GBP").getCurrencyCode())
                .sortCode(SortCode.fromString("123456"))
                .accountNumber(UKAccountNumber.fromString("12345678"))
                .build();

        Payment payment = Payment.builder()
                .createdBy("Test")
                .allowedMethods(List.of(Sepa_DC))
                .amount(amount)
                .latestExecutionDate(Instant.now())
                .product(new CompositeReference())
                .creditorAccount(sepaBankAccount)
                .creditor(creditor)
                .debtorAccount(ukBankAccount)
                .debtor(debtor)
                .build();

        TransferWisePaymentInstruction tInstruction = TransferWisePaymentInstruction.fromPayment(payment);

        assertEquals(amount.getNumberStripped(), tInstruction.getAmount());
        assertEquals(currency, tInstruction.getTargetCurrency());
        assertEquals(name, tInstruction.getAccountHolderName());
        assertEquals(payment.getId(), tInstruction.getId());
        assertEquals(iban, tInstruction.getIBAN());
    }

    @Test
    @DisplayName("TransferWise payment instruction generated from valid UK payment")
    void givenValidUKPaymentGeneratesAPaymentInstruction() {

        String name = "Some Name";
        SortCode sortCode = new SortCode("123456");
        UKAccountNumber accountNumber = new UKAccountNumber("12345678");
        String currency = "GBP";
        Money amount = Money.of(123, Monetary.getCurrency(currency));
        var creditor = PartyIdentification.builder().name("creditor").build();
        var debtor = PartyIdentification.builder().name("debtor").build();


        UKBankAccount ukBankAccount = UKBankAccount.builder()
                .name(name)
                .currency(Monetary.getCurrency(currency).getCurrencyCode())
                .sortCode(sortCode)
                .accountNumber(accountNumber)
                .build();

        Payment payment = Payment.builder()
                .createdBy("Test")
                .allowedMethods(List.of(FasterPayments))
                .amount(amount)
                .latestExecutionDate(Instant.now())
                .product(new CompositeReference())
                .creditorAccount(ukBankAccount)
                .creditor(creditor)
                .debtorAccount(ukBankAccount)
                .debtor(debtor)
                .build();

        TransferWisePaymentInstruction tInstruction = TransferWisePaymentInstruction.fromPayment(payment);

        assertEquals(amount.getNumberStripped(), tInstruction.getAmount());
        assertEquals(currency, tInstruction.getTargetCurrency());
        assertEquals(name, tInstruction.getAccountHolderName());
        assertEquals(payment.getId(), tInstruction.getId());
        assertEquals(sortCode.getValue(), tInstruction.getSortCode());
        assertEquals(accountNumber.getValue(), tInstruction.getAccountNumber());
    }
}
