package gov.scot.payments.psps.adapter.transferwise.model.transfer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransferWiseTransferDetails {
    private String reference;
    private String transferPurpose;
    private String sourceOfFunds;
}
