package gov.scot.payments.psps.adapter.transferwise.model.balance;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TransferWiseCurrencyAmount {
    private BigDecimal value;
    private String currency;
}
