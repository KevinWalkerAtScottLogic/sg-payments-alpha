package gov.scot.payments.psps.adapter.transferwise.model.recipient.bankdetails;


import gov.scot.payments.psps.adapter.transferwise.model.TransferWisePaymentInstruction;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransferWiseGBPBankDetails extends TransferWiseBankDetails {
    private String sortCode;
    private String accountNumber;
    public TransferWiseGBPBankDetails(String sortCode, String accountNumber, String legalType){
        super(legalType);
        this.sortCode= sortCode;
        this.accountNumber=accountNumber;
    }

    public static TransferWiseGBPBankDetails createBankGBPDetails(TransferWisePaymentInstruction paymentInstruction) {
        return new TransferWiseGBPBankDetails(
                paymentInstruction.getSortCode()
                , paymentInstruction.getAccountNumber()
                , "BUSINESS"
        );
    }
}
