package gov.scot.payments.psps.adapter.transferwise.model.transfer;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransferWiseTransferStatusResponse {
    private Integer id;
    private Integer user;
    private Integer targetAccount;
    private Integer sourceAccount;
    private Integer quote;
    private TransferStatus status;
    private String reference;
    private BigDecimal rate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date created;
    private Integer business;
    private Integer transferRequest;
    private Details details;
    private Boolean hasActiveIssues;
    private BigDecimal sourceValue;
    @Size(min = 3, max = 3) private String sourceCurrency;
    private BigDecimal targetValue;
    @Size(min = 3, max = 3) private String targetCurrency;
    private UUID customerTransactionId;

    @Getter
    @AllArgsConstructor
    @NoArgsConstructor
    private class Details {
        private String reference;
    }
}
