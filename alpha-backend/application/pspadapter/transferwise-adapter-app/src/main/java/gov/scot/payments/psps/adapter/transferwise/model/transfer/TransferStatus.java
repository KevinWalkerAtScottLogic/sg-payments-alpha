package gov.scot.payments.psps.adapter.transferwise.model.transfer;

public enum TransferStatus {
    incoming_payment_waiting,
    waiting_recipient_input_to_proceed,
    processing,
    funds_converted,
    outgoing_payment_sent,
    cancelled,
    funds_refunded,
    unrefundable,
    bounced_back
}
