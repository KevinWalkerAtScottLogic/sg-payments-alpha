package gov.scot.payments.psps.adapter.transferwise.model;

public class TransferWiseAuthException extends Exception {
    public TransferWiseAuthException(String message) {
        super(message);
    }
}
