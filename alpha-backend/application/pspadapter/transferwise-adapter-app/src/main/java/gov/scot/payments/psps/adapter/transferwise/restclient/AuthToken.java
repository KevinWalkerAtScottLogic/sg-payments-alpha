package gov.scot.payments.psps.adapter.transferwise.restclient;


import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@Setter
@Getter
public class AuthToken {

    private final static String HEADER_PREFIX = "Bearer ";

    @Value("${transferwise.token}")
    private String AUTH_TOKEN;

    public HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", HEADER_PREFIX + AUTH_TOKEN);
        return headers;
    }

    public HttpHeaders postHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", HEADER_PREFIX + AUTH_TOKEN);
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

}
