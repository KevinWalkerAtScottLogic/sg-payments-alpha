package gov.scot.payments.psps.adapter.transferwise.restclient;

import gov.scot.payments.psps.adapter.transferwise.model.TransferWisePaymentInstruction;
import gov.scot.payments.psps.adapter.transferwise.model.balance.TransferWiseCheckBalanceResponse;
import gov.scot.payments.psps.adapter.transferwise.model.quote.SubmitQuoteException;
import gov.scot.payments.psps.adapter.transferwise.model.quote.TransferWiseQuote;
import gov.scot.payments.psps.adapter.transferwise.model.quote.TransferWiseQuoteResponse;
import gov.scot.payments.psps.adapter.transferwise.model.recipient.*;
import gov.scot.payments.psps.adapter.transferwise.model.recipient.bankdetails.TransferWiseEURBankDetails;
import gov.scot.payments.psps.adapter.transferwise.model.recipient.bankdetails.TransferWiseGBPBankDetails;
import gov.scot.payments.psps.adapter.transferwise.model.recipient.bankdetails.TransferWiseUSDBankDetails;
import gov.scot.payments.psps.adapter.transferwise.model.transfer.*;

import java.math.BigDecimal;
import java.util.List;

public class TransferWiseClient {
    final private static String GBP_CURRENCY_CODE = "GBP";
    final private static String EUR_CURRENCY_CODE = "EUR";
    final private static String USD_CURRENCY_CODE = "USD";

    private static final String INCOMING_PAYMENT_WAITING_STATUS = "incoming_payment_waiting";
    private static final String PROCESSING_STATUS = "processing";
    private static final String FUNDS_CONVERTED_STATUS = "funds_converted";
    private static final String OUTGOING_PAYMENT_SENT_STATUS = "outgoing_payment_sent";
    private static final String CANCELLED_PAYMENT_STATUS = "cancelled";
    private static final String FUNDS_REFUNDED_STATUS = "funds_refunded";
    private static final String BOUNCED_BACK_STATUS = "bounced_back";

    private int profileId;

    private TransferWiseApi transferwiseApi;

    public TransferWiseClient(TransferWiseApi transferWiseApi, int profileId) {
        this.transferwiseApi = transferWiseApi;
        this.profileId = profileId;
    }

    public TransferWiseFundTransferResponse payInstruction(TransferWisePaymentInstruction paymentInstruction) throws
            TransferWiseCurrencyException,
            TransferWiseAddRecipientException,
            SubmitQuoteException,
            TransferWiseFundTransferException {
        TransferWiseRecipient recipient = this.createRecipient(profileId, paymentInstruction);
        TransferWiseRecipientResponse recipientResponse = this.submitRecipient(recipient);
        TransferWiseQuote quote = TransferWiseQuote.createQuote(profileId, paymentInstruction);
        TransferWiseQuoteResponse quoteResponse = this.submitQuote(quote);
        TransferWiseTransfer transfer = TransferWiseTransfer.createTransfer(recipientResponse.getId(), quoteResponse.getId(), paymentInstruction.getCustomerReferenceId());
        TransferWiseTransferResponse transferResponse = this.submitTransfer(transfer);
        return this.fundTransfer(transferResponse.getId(), profileId);
    }

    private TransferWiseQuoteResponse submitQuote(TransferWiseQuote transferwiseQuote) throws SubmitQuoteException {
        return this.transferwiseApi.submitQuote(transferwiseQuote);
    }

    private TransferWiseRecipient createRecipient(int profileId, TransferWisePaymentInstruction paymentInstruction) throws TransferWiseCurrencyException {

        String targetCurrency = paymentInstruction.getTargetCurrency();
        switch (targetCurrency) {
            case GBP_CURRENCY_CODE:
                return new TransferWiseRecipient<>(
                        targetCurrency
                        , TransferWiseRecipientType.sort_code
                        , profileId
                        , paymentInstruction.getAccountHolderName()
                        , TransferWiseGBPBankDetails.createBankGBPDetails(paymentInstruction)
                );
            case EUR_CURRENCY_CODE:
                return new TransferWiseRecipient<>(
                        targetCurrency
                        , TransferWiseRecipientType.iban
                        , profileId
                        , paymentInstruction.getAccountHolderName()
                        , TransferWiseEURBankDetails.createBankEURDetails(paymentInstruction)
                );
            case USD_CURRENCY_CODE:
                return new TransferWiseRecipient<>(
                        targetCurrency
                        , TransferWiseRecipientType.aba
                        , profileId
                        , paymentInstruction.getAccountHolderName()
                        , TransferWiseUSDBankDetails.createBankUSDDetails(paymentInstruction)
                );
            default:
                throw new TransferWiseCurrencyException("Currency Not Supported");
        }
    }

    private TransferWiseRecipientResponse submitRecipient(TransferWiseRecipient transferwiseRecipient) throws TransferWiseAddRecipientException {
        return transferwiseApi.addRecipient(transferwiseRecipient);
    }

    private TransferWiseTransferResponse submitTransfer(TransferWiseTransfer transfer) {
        return this.transferwiseApi.submitTransfer(transfer);
    }

    private TransferWiseFundTransferResponse fundTransfer(int transferId, int accountId) throws TransferWiseFundTransferException {
        return this.transferwiseApi.fundTransfer(transferId);
    }

    BigDecimal checkBalanceByCurrency(String currency, int accountId) throws TransferWiseCurrencyException {
        TransferWiseCheckBalanceResponse checkBalanceResponse = this.transferwiseApi.checkAccountBalance(accountId);
        return checkBalanceResponse.getBalances().stream()
                .filter(balance -> balance.getCurrency().equals(currency))
                .map(balanceResponse-> balanceResponse.getAmount().getValue())
                .findFirst()
                .orElseThrow(()->new TransferWiseCurrencyException("No Account setup for this Currency"));
    }

    public TransferWiseTransferStatusResponse checkTransferStatus(int transferId) {
        return this.transferwiseApi.checkTransferStatus(transferId);
    }

    public List<TransferWiseTransferResponse> getIncompleteTransfers() {
        return this.transferwiseApi.getTransfersByStatus(
                profileId
                , INCOMING_PAYMENT_WAITING_STATUS
                , PROCESSING_STATUS
                , FUNDS_CONVERTED_STATUS
                , FUNDS_REFUNDED_STATUS
                , BOUNCED_BACK_STATUS
        );
    }

    public List<TransferWiseTransferResponse> getCompletedTransfers() {
        return this.transferwiseApi.getTransfersByStatus(profileId, OUTGOING_PAYMENT_SENT_STATUS);
    }

}
