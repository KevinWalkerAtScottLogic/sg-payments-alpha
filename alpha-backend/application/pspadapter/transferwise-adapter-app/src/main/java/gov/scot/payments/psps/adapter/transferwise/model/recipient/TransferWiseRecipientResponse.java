package gov.scot.payments.psps.adapter.transferwise.model.recipient;

import gov.scot.payments.psps.adapter.transferwise.model.recipient.bankdetails.TransferWiseBankDetails;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransferWiseRecipientResponse<T extends TransferWiseBankDetails> {
    private Integer id;
    private Integer profile;
    private String acccountHolderName;
    private String currency;
    private String country;
    private TransferWiseRecipientType type;
    private T details;

}
