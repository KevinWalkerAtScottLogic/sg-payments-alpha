package gov.scot.payments.psps.adapter.transferwise.model.recipient;

import gov.scot.payments.psps.adapter.transferwise.model.recipient.bankdetails.TransferWiseBankDetails;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransferWiseRecipient<T extends TransferWiseBankDetails>  {
    private String currency;
    private TransferWiseRecipientType type;
    private Integer profile;
    private String accountHolderName;
    private T details;
}
