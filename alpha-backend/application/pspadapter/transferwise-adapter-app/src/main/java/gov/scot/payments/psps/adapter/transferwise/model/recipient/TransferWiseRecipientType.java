package gov.scot.payments.psps.adapter.transferwise.model.recipient;

public enum TransferWiseRecipientType {
    iban, sort_code, aba
}
