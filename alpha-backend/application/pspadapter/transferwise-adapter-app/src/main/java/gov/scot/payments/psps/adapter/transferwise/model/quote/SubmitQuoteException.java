package gov.scot.payments.psps.adapter.transferwise.model.quote;

public class SubmitQuoteException extends Exception {
    public SubmitQuoteException(String message) {
        super(message);
    }
}
