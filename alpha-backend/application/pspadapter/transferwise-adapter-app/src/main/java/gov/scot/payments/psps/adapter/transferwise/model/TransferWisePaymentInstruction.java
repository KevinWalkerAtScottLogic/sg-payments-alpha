package gov.scot.payments.psps.adapter.transferwise.model;

import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.payments.model.aggregate.SepaBankAccount;
import gov.scot.payments.payments.model.aggregate.UKBankAccount;
import lombok.*;
import org.apache.avro.reflect.Nullable;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransferWisePaymentInstruction {

    final private static String GBP_CURRENCY_CODE = "GBP";
    final private static String EUR_CURRENCY_CODE = "EUR";
    final private static String USD_CURRENCY_CODE = "USD";

    private UUID id;
    private String targetCurrency;
    private BigDecimal amount;
    private String accountHolderName;
    @Nullable private String sortCode;
    @Nullable private String accountNumber;
    @Nullable private String IBAN;
    @Nullable private String country; // Only applies to USD accounts - not applicable
    @Nullable private String city; // Only applies to USD accounts - not applicable
    @Nullable private String postCode; // Only applies to USD accounts - not applicable
    @Nullable private String firstLine; // Only applies to USD accounts - not applicable
    @Nullable private String abartn; // Only applies to USD accounts - not applicable
    @Nullable private String accountType; // Only applies to USD accounts - not applicable
    private UUID customerReferenceId;

    public static TransferWisePaymentInstruction fromPayment(Payment payment) {

        UUID id = payment.getId();
        BigDecimal amount = payment.getAmount().getNumberStripped();
        String currency = payment.getAmount().getCurrency().getCurrencyCode();
        String accountHolderName = payment.getCreditorAccount().getName();

        String sortCode = null;
        String accountNumber = null;
        String iban = null;

        if (payment.getCreditorAccount().getClass() == UKBankAccount.class) {
            UKBankAccount account = (UKBankAccount)payment.getCreditorAccount();
            sortCode = account.getSortCode().getValue();
            accountNumber = account.getAccountNumber().getValue();
        } else if (payment.getCreditorAccount().getClass() == SepaBankAccount.class) {
            SepaBankAccount account = (SepaBankAccount)payment.getCreditorAccount();
            iban = account.getIban();
        }

        return TransferWisePaymentInstruction.builder()
                .id(id)
                .amount(amount)
                .targetCurrency(currency)
                .accountHolderName(accountHolderName)
                .sortCode(sortCode)
                .accountNumber(accountNumber)
                .IBAN(iban)
                .customerReferenceId(payment.getId())
                .build();
    }
}
