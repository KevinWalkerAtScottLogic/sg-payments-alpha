package gov.scot.payments.psps.adapter.transferwise.model.transfer;

import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransferWiseFundTransferResponse {
    private String type;
    private String status;
    private String errorCode;
}
