package gov.scot.payments.psps.adapter.transferwise.model.recipient;

public class TransferWiseAddRecipientException extends Exception {
    public TransferWiseAddRecipientException(String message){
        super(message);
    }
}
