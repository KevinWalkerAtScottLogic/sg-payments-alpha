package gov.scot.payments.psps.adapter.transferwise.model.recipient.bankdetails;

import gov.scot.payments.psps.adapter.transferwise.model.TransferWisePaymentInstruction;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransferWiseUSDBankDetails extends TransferWiseBankDetails {
    private String abartn;
    private String accountNumber;
    private String accountType;
    private TransferWiseBankDetailsAddress address;
    public TransferWiseUSDBankDetails(String abartn, String accountNumber, String accountType,
                                      TransferWiseBankDetailsAddress address, String legalType){
        super(legalType);
        this.abartn = abartn;
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.address = address;
    }

    public static TransferWiseUSDBankDetails createBankUSDDetails(TransferWisePaymentInstruction paymentInstruction) {
        return new TransferWiseUSDBankDetails(
                paymentInstruction.getAbartn()
                , paymentInstruction.getAccountNumber()
                , "CHECKING"
                , new TransferWiseBankDetailsAddress(
                paymentInstruction.getCountry()
                , paymentInstruction.getCity()
                , paymentInstruction.getPostCode()
                , paymentInstruction.getFirstLine())
                , "PRIVATE"
        );
    }

}
