package gov.scot.payments.psps.adapter.transferwise.model.events;

import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.psps.adapter.transferwise.model.TransferWiseResource;
import gov.scot.payments.psps.adapter.transferwise.model.transfer.TransferStatus;
import lombok.*;

import java.time.Instant;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
@ToString
public class TransferWiseTransferStatusChangeEvent {

    @NonNull @JsonProperty("data") private StatusChangeData data;
    @NonNull @JsonProperty("subscription_id") private UUID subscriptionId;
    @NonNull @JsonProperty("event_type") private String eventType;
    @NonNull @JsonProperty("schema_version") private String schemaVersion;
    @NonNull @JsonProperty("sent_at") private Instant sentAt;

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Builder
    @ToString
    public static class StatusChangeData {
        @NonNull @JsonProperty("resource") private TransferWiseResource resource;
        @NonNull @JsonProperty("current_state") private TransferStatus currentState;
        @NonNull @JsonProperty("previous_state") private TransferStatus previousState;
        @NonNull @JsonProperty("occurred_at") private Instant occurredAt;
    }
}
