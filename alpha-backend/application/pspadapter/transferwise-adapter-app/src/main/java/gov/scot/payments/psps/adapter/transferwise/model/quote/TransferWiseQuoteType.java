package gov.scot.payments.psps.adapter.transferwise.model.quote;

public enum TransferWiseQuoteType {
    BALANCE_PAYOUT,
    BALANCE_CONVERSION,
}
