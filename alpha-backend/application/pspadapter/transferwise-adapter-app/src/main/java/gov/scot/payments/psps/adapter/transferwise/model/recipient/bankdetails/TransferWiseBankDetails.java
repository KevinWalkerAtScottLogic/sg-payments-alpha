package gov.scot.payments.psps.adapter.transferwise.model.recipient.bankdetails;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class
TransferWiseBankDetails {
    private String legalType;
}
