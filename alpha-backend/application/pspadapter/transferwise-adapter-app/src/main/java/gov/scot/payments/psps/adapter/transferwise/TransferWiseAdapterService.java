package gov.scot.payments.psps.adapter.transferwise;

import gov.scot.payments.payments.model.aggregate.Payment;
import gov.scot.payments.psps.adapter.PostSubmissionEventSender;
import gov.scot.payments.psps.adapter.transferwise.model.TransferWisePaymentInstruction;
import gov.scot.payments.psps.adapter.transferwise.model.events.TransferWiseTransferStatusChangeEvent;
import gov.scot.payments.psps.adapter.transferwise.model.quote.SubmitQuoteException;
import gov.scot.payments.psps.adapter.transferwise.model.recipient.TransferWiseAddRecipientException;
import gov.scot.payments.psps.adapter.transferwise.model.recipient.TransferWiseCurrencyException;
import gov.scot.payments.psps.adapter.transferwise.model.transfer.TransferWiseFundTransferException;
import gov.scot.payments.psps.adapter.transferwise.model.transfer.TransferWiseFundTransferResponse;
import gov.scot.payments.psps.adapter.transferwise.model.transfer.TransferWiseTransferResponse;
import gov.scot.payments.psps.adapter.transferwise.model.transfer.TransferWiseTransferStatusResponse;
import gov.scot.payments.psps.adapter.transferwise.restclient.TransferWiseClient;
import gov.scot.payments.psps.method.model.PspCompleteResponse;
import gov.scot.payments.psps.method.model.PspReturnResponse;
import gov.scot.payments.psps.method.model.PspTransactionCostEstimateResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.javamoney.moneta.Money;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.money.Monetary;
import java.math.BigDecimal;
import java.util.List;

import static gov.scot.payments.psps.adapter.transferwise.model.transfer.TransferStatus.funds_refunded;
import static gov.scot.payments.psps.adapter.transferwise.model.transfer.TransferStatus.outgoing_payment_sent;

@AllArgsConstructor
@RequestMapping()
@Slf4j
public class TransferWiseAdapterService {

    public static final List<String> SUPPORTED_TRANSFERWISE_SCHEMAS = List.of("2.0.0");
    public static final String STATE_CHANGE_EVENT = "transfers#state-change";
    public static final String TRANSFER_RESOURCE_TYPE = "transfer";
    public static final List<Object> INTERESTING_TRANSFER_EVENTS = List.of(
            outgoing_payment_sent,
            funds_refunded
    );

    private TransferWiseClient transferwiseClient;
    private BigDecimal fixedCostPerTransaction;
    private BigDecimal amountPercentageCost;
    private String psp;

    @GetMapping({"/estimateTransactionCosts"})
    @ResponseBody
    public Mono<ResponseEntity<PspTransactionCostEstimateResponse>> estimateTransactionCosts (
            @RequestParam("amount") BigDecimal amount, @RequestParam("currency") String currency) {

        // TODO this is a bit ugly/non-idiomatic, and results in a 500 Server Error, should probably be a 4XX error, e.g. 400 Bad request or 403 Forbidden
        if (!currency.equals("GBP")) {
            return Mono.error(() -> new IllegalArgumentException("Currency " + currency + " is not supported"));
        }

        BigDecimal cost = fixedCostPerTransaction.add(amountPercentageCost.multiply(amount).movePointLeft(2));

        return Mono.just(ResponseEntity.ok()
                .body(PspTransactionCostEstimateResponse
                        .builder()
                        .psp(psp)
                        .amount(Money.of(cost, Monetary.getCurrency("GBP")))
                        .build()));
    }

    @GetMapping({"/complete"})
    @ResponseBody
    public Mono<ResponseEntity<List<TransferWiseTransferResponse>>> getCompleteTransfers() {
        return Mono.just(ResponseEntity.ok()
                .body(transferwiseClient.getCompletedTransfers()));
    }

    @GetMapping({"/incomplete"})
    @ResponseBody
    public Mono<ResponseEntity<List<TransferWiseTransferResponse>>> getIncompleteTransfers() {
        return Mono.just(ResponseEntity.ok()
                .body(transferwiseClient.getIncompleteTransfers()));
    }

    @PostMapping({"/submit"})
    @ResponseBody
    public Mono<ResponseEntity<TransferWiseFundTransferResponse>> submitPayment(@RequestBody Payment payment) {
        try {
            TransferWisePaymentInstruction transferWisePaymentInstruction = TransferWisePaymentInstruction.fromPayment(payment);
            TransferWiseFundTransferResponse transferStatusResponse = transferwiseClient.payInstruction(transferWisePaymentInstruction);
            return Mono.just(ResponseEntity.ok()
                    .body(transferStatusResponse));
        } catch (TransferWiseAddRecipientException | SubmitQuoteException | TransferWiseFundTransferException | TransferWiseCurrencyException e) {
            return Mono.error(e);
        }
    }

    @PostMapping({"/transferStatusChange"})
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void statusChange(@RequestBody TransferWiseTransferStatusChangeEvent event, PostSubmissionEventSender postSubmissionEventSender) {

        if (!isSupportedSchema(event.getSchemaVersion())) {
            throw new IllegalArgumentException("Schema " + event.getSchemaVersion() + " is not supported");
        }
        if (!event.getEventType().equals(STATE_CHANGE_EVENT)) {
            throw new IllegalArgumentException("Event type " + event.getEventType() + " is not supported");
        }
        if (!event.getData().getResource().getType().equals(TRANSFER_RESOURCE_TYPE)) {
            throw new IllegalArgumentException("Resource type " + event.getData().getResource().getType() + " is not supported");
        }

        log.info("Status of transfer {} changed from {} to {}",
                event.getData().getResource().getId(), event.getData().getPreviousState(), event.getData().getCurrentState());

        if (INTERESTING_TRANSFER_EVENTS.contains(event.getData().getCurrentState())) {
            updatePaymentState(transferwiseClient.checkTransferStatus(event.getData().getResource().getId()), postSubmissionEventSender);
        }
    }

    private void updatePaymentState(TransferWiseTransferStatusResponse transferStatus, PostSubmissionEventSender postSubmissionEventSender) {
        log.info("Updating payment {} due to transfer becoming {}", transferStatus.getCustomerTransactionId(), transferStatus.getStatus());
        switch (transferStatus.getStatus()) {
            case outgoing_payment_sent:
                log.info("Sending complete for payment {}", transferStatus.getCustomerTransactionId());
                postSubmissionEventSender.sendComplete(
                        PspCompleteResponse.builder()
                                .paymentId(transferStatus.getCustomerTransactionId())
                                .build());
                break;
            case funds_refunded:
                log.info("Sending return for payment {}", transferStatus.getCustomerTransactionId());
                postSubmissionEventSender.sendReturn(
                        PspReturnResponse.builder()
                                .paymentId(transferStatus.getCustomerTransactionId())
                                .code("Returned")
                                .message("TransferWise transaction in state: " + transferStatus.getStatus())
                                .amount(Money.of(transferStatus.getSourceValue(), transferStatus.getSourceCurrency()))
                                .build());
                break;
        }
    }

    private boolean isSupportedSchema(String schema) {
        return SUPPORTED_TRANSFERWISE_SCHEMAS.contains(schema);
    }
}
