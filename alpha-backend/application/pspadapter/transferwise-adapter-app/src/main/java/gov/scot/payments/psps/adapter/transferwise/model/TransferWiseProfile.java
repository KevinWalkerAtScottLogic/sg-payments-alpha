package gov.scot.payments.psps.adapter.transferwise.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TransferWiseProfile {
    Integer id;
    String type;
}
