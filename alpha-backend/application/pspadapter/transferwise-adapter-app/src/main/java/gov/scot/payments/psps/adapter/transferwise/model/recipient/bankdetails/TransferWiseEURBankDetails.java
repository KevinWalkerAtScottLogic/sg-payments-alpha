package gov.scot.payments.psps.adapter.transferwise.model.recipient.bankdetails;

import gov.scot.payments.psps.adapter.transferwise.model.TransferWisePaymentInstruction;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransferWiseEURBankDetails extends TransferWiseBankDetails {
    private String IBAN;
    public TransferWiseEURBankDetails(String IBAN, String legalType){
        super(legalType);
        this.IBAN=IBAN;
    }

    public static TransferWiseEURBankDetails createBankEURDetails(TransferWisePaymentInstruction paymentInstruction) {
        return new TransferWiseEURBankDetails(
                paymentInstruction.getIBAN()
                , "BUSINESS"
        );
    }
}
