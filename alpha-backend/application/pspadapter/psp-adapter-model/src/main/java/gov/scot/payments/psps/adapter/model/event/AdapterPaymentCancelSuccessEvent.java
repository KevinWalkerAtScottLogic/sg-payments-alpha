package gov.scot.payments.psps.adapter.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.EventImpl;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "pspadapter", type = "paymentCancelSuccess")
@NoArgsConstructor
@RequiredArgsConstructor
public class AdapterPaymentCancelSuccessEvent extends EventImpl implements HasKey<String> {

    @NonNull private UUID paymentId;
    @NonNull private PaymentMethod method;
    @NonNull private String psp;
    @NonNull private Map<String,String> pspMetadata;

    @Override
    @JsonIgnore
    public String getKey() {
        return paymentId.toString();
    }
}
