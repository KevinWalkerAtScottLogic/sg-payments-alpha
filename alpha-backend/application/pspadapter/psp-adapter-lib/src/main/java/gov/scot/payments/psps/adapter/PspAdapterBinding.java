package gov.scot.payments.psps.adapter;

import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface PspAdapterBinding {

    @Output("responses")
    MessageChannel responses();

}
