package gov.scot.payments.psps.adapter;

import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.psps.adapter.model.event.AdapterPaymentCompleteEvent;
import gov.scot.payments.psps.adapter.model.event.AdapterPaymentReturnedEvent;
import gov.scot.payments.psps.method.model.PspCancelResponse;
import gov.scot.payments.psps.method.model.PspCompleteResponse;
import gov.scot.payments.psps.method.model.PspReturnResponse;
import gov.scot.payments.psps.method.model.event.MethodPaymentReturnedEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.stream.reactive.FluxSender;
import org.springframework.kafka.core.KafkaOperations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class PostSubmissionEventSender {

    private final FluxSender sender;
    private final PaymentMethod paymentMethod;
    private final String psp;

    public Mono<Void> sendReturn(PspReturnResponse ret){
        var event = AdapterPaymentReturnedEvent.builder()
                                               .amount(ret.getAmount())
                                               .code(ret.getCode())
                                               .message(ret.getMessage())
                                               .method(paymentMethod)
                                               .paymentId(ret.getPaymentId())
                                               .psp(psp)
                                               .pspMetadata(ret.getMetadata())
                                               .build();
        return sender.send(Flux.just(event));
    }

    public Mono<Void> sendComplete(PspCompleteResponse ret){
        var event = AdapterPaymentCompleteEvent.builder()
                                               .method(paymentMethod)
                                               .paymentId(ret.getPaymentId())
                                               .psp(psp)
                                               .pspMetadata(ret.getMetadata())
                                               .build();
        return sender.send(Flux.just(event));
    }
}
