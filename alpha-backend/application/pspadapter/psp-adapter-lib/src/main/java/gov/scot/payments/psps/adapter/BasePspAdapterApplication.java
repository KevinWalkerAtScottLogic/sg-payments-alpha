package gov.scot.payments.psps.adapter;

import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.model.Event;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.reactive.FluxSender;
import org.springframework.cloud.stream.reactive.MessageChannelToFluxSenderParameterAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.util.ResourceUtils;
import reactor.core.publisher.Flux;

@PropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "psp-adapter.properties")
@EnableBinding(PspAdapterBinding.class)
public abstract class BasePspAdapterApplication extends BaseApplication {

    protected abstract PaymentMethod paymentMethod();


    public void stubMethodForParameterResolution(Flux<Message<Event>> param){

    }

    @Bean
    public PostSubmissionEventSender postSubmissionEventSender(@Qualifier("responses") MessageChannel responses
        , @Value("${component.name}")String psp){
        FluxSender eventSender = new MessageChannelToFluxSenderParameterAdapter().adapt(responses,null);
        return new PostSubmissionEventSender(eventSender,paymentMethod(),psp);
    }
}
