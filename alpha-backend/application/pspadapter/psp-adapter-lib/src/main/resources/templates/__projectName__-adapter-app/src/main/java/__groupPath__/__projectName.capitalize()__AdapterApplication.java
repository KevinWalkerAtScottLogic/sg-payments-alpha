package @group@;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.psps.adapter.BasePspAdapterApplication;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;

@ApplicationComponent
public class @projectName.capitalize()@AdapterApplication extends BasePspAdapterApplication{

    @Override
    protected PaymentMethod paymentMethod() {
        return PaymentMethod.@paymentMethod@;
    }

    public static void main(String[] args){
        BaseApplication.run(@projectName.capitalize()@AdapterApplication.class,args);
    }
}