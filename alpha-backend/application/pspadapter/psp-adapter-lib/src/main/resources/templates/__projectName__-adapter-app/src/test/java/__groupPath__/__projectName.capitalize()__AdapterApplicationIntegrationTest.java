package @group@;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.TestConfiguration;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.Import;

import static org.junit.jupiter.api.Assertions.*;

@ApplicationIntegrationTest(classes = {@projectName.capitalize()@AdapterApplicationIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.CUSTOM
        ,bindings = {"responses"}
        ,properties = {"context.name=@contextName@"
        ,"component.name=@projectName@"})
class @projectName.capitalize()@AdapterApplicationIntegrationTest {


    @Test
    public void test(EmbeddedBrokerClient brokerClient, @Autowired PostSubmissionEventSender eventSender){

    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends @projectName.capitalize()@AdapterApplication{

    }

}