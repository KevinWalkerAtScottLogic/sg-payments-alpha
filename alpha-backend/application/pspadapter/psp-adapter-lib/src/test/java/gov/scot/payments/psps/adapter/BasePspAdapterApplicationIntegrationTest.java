package gov.scot.payments.psps.adapter;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.psps.adapter.model.event.AdapterPaymentCompleteEvent;
import gov.scot.payments.psps.method.model.PspCompleteResponse;
import gov.scot.payments.psps.method.model.PspReturnResponse;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.TestConfiguration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import java.util.HashMap;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ApplicationIntegrationTest(classes = {BasePspAdapterApplicationIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.CUSTOM
        ,bindings = {"responses"}
        ,properties = {"context.name=bacs"
        ,"component.name=bacs-adapter"})
class BasePspAdapterApplicationIntegrationTest {


    @Test
    public void test(EmbeddedBrokerClient brokerClient, @Autowired PostSubmissionEventSender eventSender){
        var response = PspCompleteResponse.builder()
                                          .metadata(new HashMap<>())
                                          .paymentId(UUID.randomUUID())
                                          .build();
        eventSender.sendComplete(response).subscribe();
        var events = brokerClient.readAllFromDestination("responses", AdapterPaymentCompleteEvent.class);
        assertEquals(1,events.size());
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends BasePspAdapterApplication{

        @Override
        protected PaymentMethod paymentMethod() {
            return PaymentMethod.Bacs;
        }

    }

}