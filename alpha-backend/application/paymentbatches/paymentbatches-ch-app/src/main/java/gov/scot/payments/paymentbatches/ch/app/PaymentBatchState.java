package gov.scot.payments.paymentbatches.ch.app;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.application.component.commandhandler.stateful.AggregateState;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.model.CommandFailureInfo;
import gov.scot.payments.paymentbatches.model.aggregate.PaymentBatch;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

@SuperBuilder
@NoArgsConstructor
@ToString(callSuper = true)
public class PaymentBatchState extends AggregateState<PaymentBatch> {

    @Getter @Nullable private PaymentBatch previous;
    @Getter @Nullable private PaymentBatch current;

    @JsonCreator
    public PaymentBatchState(@JsonProperty("message") SerializedMessage message
            , @JsonProperty("error") CommandFailureInfo error
            , @JsonProperty("previous") PaymentBatch previous
            , @JsonProperty("current") PaymentBatch current
            , @JsonProperty("currentVersion") Long currentVersion) {
        super(message, error, currentVersion);
        this.previous = previous;
        this.current = current;
    }

}
