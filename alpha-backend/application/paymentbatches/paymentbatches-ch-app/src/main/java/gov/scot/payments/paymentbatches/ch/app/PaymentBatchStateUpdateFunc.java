package gov.scot.payments.paymentbatches.ch.app;

import gov.scot.payments.application.component.commandhandler.stateful.StateUpdateFunction;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.model.Command;
import gov.scot.payments.paymentbatches.model.aggregate.PaymentBatch;
import gov.scot.payments.paymentbatches.model.command.*;

import static gov.scot.payments.application.component.commandhandler.stateful.AclFilteringStatefulCommandHandlerFunction.lock;

public class PaymentBatchStateUpdateFunc extends StateUpdateFunction.PatternMatching<PaymentBatchState, PaymentBatch> {


    @Override
    protected void handlers(PatternMatcher.Builder2<Command, PaymentBatchState, PaymentBatch> builder) {

        builder
                .match2(CreatePaymentBatchCommand.class, lock((c, s) -> createPaymentBatch(c, s)))
                .match2(FailPaymentBatchCommand.class, lock((c, s) -> failPaymentBatch(c, s)))
                .match2(OpenPaymentBatchCommand.class, lock((c, s) -> openPaymentBatch(c, s)))
                .match2(AddPaymentToBatchCommand.class, lock((c, s) -> addPaymentToBatch(c, s)))
                .match2(ClosePaymentBatchCommand.class, lock((c, s) -> closePaymentBatch(c, s)));
    }

    private PaymentBatch closePaymentBatch(ClosePaymentBatchCommand c, PaymentBatch current) {
        if(current == null || current.getStatus() != PaymentBatch.Status.Open){
            throw new IllegalPaymentBatchStateException("Cannot close payment batch as it is not in open state");
        }
        return current.toBuilder().status(PaymentBatch.Status.Closed).build();
    }

    private PaymentBatch addPaymentToBatch(AddPaymentToBatchCommand c, PaymentBatch current) {

        if(current == null || current.getStatus() != PaymentBatch.Status.Open){
            throw new IllegalPaymentBatchStateException("Cannot add to payment batch as it is not in open state");
        }

        var updatedPayments = current.getPayments().append(c.getPayment().getId());
        return current.toBuilder().payments(updatedPayments).build();
    }

    private PaymentBatch openPaymentBatch(OpenPaymentBatchCommand c, PaymentBatch current) {

        if(current == null || current.getStatus() != PaymentBatch.Status.New){
            throw new IllegalPaymentBatchStateException("Cannot open payment batch as it is not in 'New' state");
        }
        return current.toBuilder().status(PaymentBatch.Status.Open).build();
    }

    private PaymentBatch failPaymentBatch(FailPaymentBatchCommand c, PaymentBatch current) {

        if(current == null || current.getStatus() != PaymentBatch.Status.New){
            throw new IllegalPaymentBatchStateException("Cannot fail payment batch as it is not in 'New' state");
        }
        return current.toBuilder().status(PaymentBatch.Status.Failed).build();
    }

    private PaymentBatch createPaymentBatch(CreatePaymentBatchCommand c, PaymentBatch current) {

        if(current != null ){
            throw new IllegalPaymentBatchStateException("Cannot create payment batch as it already exists");
        }
        return c.getBatch().toBuilder().status(PaymentBatch.Status.New).build();
    }
}
