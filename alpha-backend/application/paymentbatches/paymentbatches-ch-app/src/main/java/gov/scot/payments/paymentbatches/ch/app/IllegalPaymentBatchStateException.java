package gov.scot.payments.paymentbatches.ch.app;

public class IllegalPaymentBatchStateException extends RuntimeException {
    public IllegalPaymentBatchStateException(String s) {
        super(s);
    }
}
