package gov.scot.payments.paymentbatches.ch.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.application.component.commandhandler.CommandHandlerBaseApplication;
import gov.scot.payments.application.component.commandhandler.stateful.EventGeneratorFunction;
import gov.scot.payments.application.component.commandhandler.stateful.StateUpdateFunction;
import gov.scot.payments.application.component.commandhandler.stateful.StatefulCommandHandlerApplication;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.model.CommandFailureInfo;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.user.Scope;
import gov.scot.payments.paymentbatches.model.aggregate.PaymentBatch;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.reactive.FluxSender;
import org.springframework.cloud.stream.reactive.MessageChannelToFluxSenderParameterAdapter;
import org.springframework.cloud.stream.reactive.MessageChannelToInputFluxParameterAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.core.MethodParameter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.messaging.converter.CompositeMessageConverter;
import reactor.core.publisher.Flux;

@ApplicationComponent
public class PaymentBatchesCHApplication extends StatefulCommandHandlerApplication<PaymentBatch, PaymentBatchState, EventWithCauseImpl> {

    @Override
    protected Set<String> registeredVerbs() {
        return HashSet.of("Read","CreateBatch");
    }

    @Override
    protected Class<PaymentBatch> stateEntityClass() {
        return PaymentBatch.class;
    }

    @Override
    protected Class<PaymentBatchState> stateClass() {
        return PaymentBatchState.class;
    }

    @Override
    protected PaymentBatchState createNewAggregateInstance(SerializedMessage command, CommandFailureInfo error, PaymentBatch previous, PaymentBatch current, Long currentVersion) {
        return new PaymentBatchState(command, error, previous, current, currentVersion);
    }

    @Override
    protected Scope extractScope(PaymentBatchState state) {
        return new Scope(state.getCurrent().getCustomerId());
    }

    @Bean
    public StateUpdateFunction<PaymentBatchState,PaymentBatch> stateUpdateFunction() {
        return new PaymentBatchStateUpdateFunc();
    }

    @Bean
    public EventGeneratorFunction<PaymentBatchState, EventWithCauseImpl> eventGenerationFunction() {
        return new PaymentBatchEventGenerator();
    }

    @Bean
    public PaymentBatchService paymentBatchService(@Qualifier("send-commands") MessageChannel requests
            , @Qualifier("receive-events") SubscribableChannel responses
            , CompositeMessageConverter messageConverter
            , MessageTypeRegistry registry) throws NoSuchMethodException {
        FluxSender commandSender = new MessageChannelToFluxSenderParameterAdapter().adapt(requests,null);

        Flux<Message<Event>> responseStream = (Flux<Message<Event>>) new MessageChannelToInputFluxParameterAdapter(messageConverter)
                .adapt(responses,new MethodParameter(CommandHandlerBaseApplication.class.getMethod("stubMethodForParameterResolution",Flux.class),0));

        return new PaymentBatchService(commandSender, responseStream,registry);
    }
    public static void main(String[] args){
        BaseApplication.run(PaymentBatchesCHApplication.class,args);
    }
}