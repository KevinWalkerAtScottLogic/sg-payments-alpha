package gov.scot.payments.paymentbatches.ch.app;

import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.application.component.commandhandler.CommandHandlerResource;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.paymentbatches.model.aggregate.PaymentBatch;
import gov.scot.payments.paymentbatches.model.api.CreatePaymentBatchRequest;
import gov.scot.payments.paymentbatches.model.command.AddPaymentToBatchCommand;
import gov.scot.payments.paymentbatches.model.command.ClosePaymentBatchCommand;
import gov.scot.payments.paymentbatches.model.command.CreatePaymentBatchCommand;
import gov.scot.payments.paymentbatches.model.command.OpenPaymentBatchCommand;
import gov.scot.payments.payments.model.aggregate.Payment;
import io.vavr.collection.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.reactive.FluxSender;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.Message;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;

@RequestMapping("/dev")
@Slf4j
public class PaymentBatchService extends CommandHandlerResource {


    public PaymentBatchService(FluxSender commandSender, Flux<Message<Event>> responses, MessageTypeRegistry registry) {
        super(commandSender, responses, registry);
    }

    @RequestMapping(method = RequestMethod.POST, path= "/createBatch")
    @PreAuthorize("principal.hasAccess(T(gov.scot.payments.model.user.Role).parse('./paymentbatches:CreateBatch'))")
    @ResponseBody
    public Mono<ResponseEntity<String>> sendCreateBatch (@RequestBody CreatePaymentBatchRequest request, @AuthenticationPrincipal  Subject authSubject) throws InterruptedException {

       return  Mono.just(request)
                .map(r -> PaymentBatch.builder()
                        .status(PaymentBatch.Status.New)
                        .id(request.getId())
                        .name(request.getName())
                        .createdBy(authSubject.getName())
                        .createdAt(Instant.now())
                        .message("message")
                        .customerId(request.getProduct().getComponent0())
                        .build())
                .map(b -> CreatePaymentBatchCommand.builder()
                        .user("test-user")
                        .batch(b)
                        .build())
                .flatMap(c -> sendCommand(c))
                .map(r ->  OpenPaymentBatchCommand.builder()
                        .user(authSubject.getName())
                        .batchId(request.getId())
                        .build())
                .flatMap(c -> sendCommand(c))
                .flatMapIterable(v -> getMap(request))
               .map(p -> AddPaymentToBatchCommand.builder()
                        .user(authSubject.getName())
                        .payment(p)
                        .build())
                .flatMap(c -> sendCommand(c))
                .collectList()
                .map(results -> ClosePaymentBatchCommand.builder()
                        .user(authSubject.getName())
                        .batchId(request.getId())
                        .reply(true)
                        .build())
                .flatMap(c -> sendCommand(c).map(r -> ResponseEntity.ok(request.getId().toString())))
                // TODO throw error and wait for command
                .onErrorResume( c -> Mono.just(ResponseEntity.badRequest().body(c.getMessage())));
    }

    private List<Payment> getMap(@RequestBody CreatePaymentBatchRequest request) {
        return request.getPayments().map(p -> p.toBuilder().batchId(request.getId().toString()).build());
    }

}
