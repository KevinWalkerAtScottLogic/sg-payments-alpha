package gov.scot.payments.paymentbatches.ch.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.paymentbatches.model.aggregate.PaymentBatch;
import gov.scot.payments.paymentbatches.model.api.CreatePaymentBatchRequest;
import gov.scot.payments.paymentbatches.model.command.*;
import gov.scot.payments.paymentbatches.model.event.*;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.vavr.collection.List;
import org.apache.kafka.streams.KafkaStreams;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.net.MalformedURLException;
import java.time.Duration;
import java.time.Instant;
import java.util.UUID;

import static gov.scot.payments.testing.security.MockSubjectAuthentication.user;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockAuthentication;

@ApplicationIntegrationTest(classes = {PaymentbatchesCHApplicationIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.COMMAND_HANDLER)
public class PaymentbatchesCHApplicationIntegrationTest {

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, EmbeddedBrokerClient brokerClient, @Value("${namespace}") String namespace) {
        brokerClient.getBroker().addTopicsIfNotExists(namespace + "-paymentbatches-command-errors");
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
    }

    @Test
    void testCreateBatchAuthCommand(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient) {

        var batch = CreatePaymentBatchRequest.builder()
                .id("batch id")
                .product(CompositeReference.parse("test.product"))
                .payments(List.of())
                .name("my-batch")
                .build();

        client
                .mutateWith(mockAuthentication(user("testuser", Role.parse("./payments:Read"))))
                .post()
                .uri(uri -> uri.path("/dev/createBatch").build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(batch)
                .exchange()
                .expectStatus()
                .isForbidden();

    }

    @Test
    void testCreateBatchCommand(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient) {

        var payment =  Payment.builder()
                .amount(Money.of(12.00, "GBP"))
                .batchId(null)
                .createdBy("test-user")
                .allowedMethods(List.of(PaymentMethod.Bacs))
                .latestExecutionDate(Instant.now())
                .product(CompositeReference.parse("customer.product"))
                .creditor(PartyIdentification.builder().name("person").build())
                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                              .debtor(PartyIdentification.builder().name("person").build())
                              .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                              .build();

        var payment2 =  Payment.builder()
                .amount(Money.of(12.00, "GBP"))
                .batchId(null)
                .createdBy("test-user")
                .allowedMethods(List.of(PaymentMethod.Bacs))
                .latestExecutionDate(Instant.now())
                .product(CompositeReference.parse("customer.product"))
                .creditor(PartyIdentification.builder().name("person").build())
                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                               .debtor(PartyIdentification.builder().name("person").build())
                               .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                               .build();

        var batch = CreatePaymentBatchRequest.builder()
                .id("batch id")
                .product(CompositeReference.parse("test.product"))
                .payments(List.of(payment, payment2))
                .name("my-batch")
                .build();

        client
                .mutateWith(mockAuthentication(user("testuser", Role.parse("./paymentbatches:CreateBatch"))))
                .post()
                .uri(uri -> uri.path("/dev/createBatch").build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(batch)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .equals(batch.getId());

        var events = brokerClient.readAllFromDestination("events", Event.class, Duration.ofSeconds(10));
        assertEquals(5, events.size());

    }

    @Test
    public void testEndToEndPaymentBatch(EmbeddedBrokerClient brokerClient) throws MalformedURLException {

        var batch = PaymentBatchExampleData.createExampleBatch();
        var createPayment = CreatePaymentBatchCommand.builder()
                .batch(batch)
                .user("test-user")
                .build();

        brokerClient.sendKeyValuesToDestination("commands", List.of(KeyValueWithHeaders.msg(createPayment.getKey(), createPayment)));
        var allEvents = brokerClient.readAllFromDestination("events", BasePaymentBatchEventWithCause.class);
        assertEquals(1, allEvents.size());

        var openPayment = OpenPaymentBatchCommand.builder()
                .batchId(batch.getId())
                .user("test-user")
                .stateVersion(1L)
                .build();

        brokerClient.sendKeyValuesToDestination("commands", List.of(KeyValueWithHeaders.msg(openPayment.getKey(), openPayment)));
        allEvents = brokerClient.readAllFromDestination("events", BasePaymentBatchEventWithCause.class);
        assertEquals(2, allEvents.size());

        var payment1 = PaymentBatchExampleData.createExamplePayment(batch.getId());
        var addToPayment = AddPaymentToBatchCommand.builder()
                .payment(payment1)
                .user("test-user")
                .stateVersion(2L)
                .build();

        brokerClient.sendKeyValuesToDestination("commands", List.of(KeyValueWithHeaders.msg(addToPayment.getKey(), addToPayment)));
        allEvents = brokerClient.readAllFromDestination("events", BasePaymentBatchEventWithCause.class);
        assertEquals(3, allEvents.size());

        var payment2 = PaymentBatchExampleData.createExamplePayment(batch.getId());
        addToPayment = AddPaymentToBatchCommand.builder()
                .payment(payment2)
                .user("test-user")
                .stateVersion(3L)
                .build();

        brokerClient.sendKeyValuesToDestination("commands", List.of(KeyValueWithHeaders.msg(addToPayment.getKey(), addToPayment)));
        allEvents = brokerClient.readAllFromDestination("events", BasePaymentBatchEventWithCause.class);
        assertEquals(4, allEvents.size());

        var closePaymentBatch = ClosePaymentBatchCommand.builder()
                .batchId(batch.getId())
                .user("test-user")
                .stateVersion(4L)
                .build();

        brokerClient.sendKeyValuesToDestination("commands", List.of(KeyValueWithHeaders.msg(closePaymentBatch.getKey(), closePaymentBatch)));
        allEvents = brokerClient.readAllFromDestination("events", BasePaymentBatchEventWithCause.class);

        assertEquals(5, allEvents.size());
        PaymentBatchCreatedEvent event = (PaymentBatchCreatedEvent) allEvents.get(0);
        assertEquals(PaymentBatch.Status.New, event.getCarriedState().getStatus());
        PaymentBatchOpenedEvent event2 = (PaymentBatchOpenedEvent) allEvents.get(1);
        assertEquals(PaymentBatch.Status.Open, event2.getCarriedState().getStatus());
        PaymentBatchUpdatedEvent event3 = (PaymentBatchUpdatedEvent) allEvents.get(3);
        assertEquals(PaymentBatch.Status.Open, event3.getCarriedState().getStatus());
        assertEquals(2, event3.getCarriedState().getPayments().size());
        assertEquals(payment1.getId(), event3.getCarriedState().getPayments().get(0));
        assertEquals(payment2.getId(), event3.getCarriedState().getPayments().get(1));
        PaymentBatchClosedEvent event4 = (PaymentBatchClosedEvent) allEvents.get(4);
        assertEquals(PaymentBatch.Status.Closed, event4.getCarriedState().getStatus());

    }


    @Test
    public void testCreatePaymentAndFailPaymentBatch(EmbeddedBrokerClient brokerClient) throws MalformedURLException {

        var batch = PaymentBatchExampleData.createExampleBatch();
        var createPayment = CreatePaymentBatchCommand.builder()
                .batch(batch)
                .user("test-user")
                .stateVersion(1L)
                .build();

        brokerClient.sendKeyValuesToDestination("commands", List.of(KeyValueWithHeaders.msg(createPayment.getKey(), createPayment)));
        var allEvents = brokerClient.readAllFromDestination("events", BasePaymentBatchEventWithCause.class);
        assertEquals(1, allEvents.size());

        var failPayment = FailPaymentBatchCommand.builder()
                .batchId(batch.getId())
                .user("test-user")
                .stateVersion(1L)
                .build();

        brokerClient.sendKeyValuesToDestination("commands", List.of(KeyValueWithHeaders.msg(failPayment.getKey(), failPayment)));
        allEvents = brokerClient.readAllFromDestination("events", BasePaymentBatchEventWithCause.class);
        assertEquals(2, allEvents.size());
        PaymentBatchCreatedEvent event = (PaymentBatchCreatedEvent) allEvents.get(0);
        assertEquals(PaymentBatch.Status.New, event.getCarriedState().getStatus());
        PaymentBatchFailedEvent event2 = (PaymentBatchFailedEvent) allEvents.get(1);
        assertEquals(PaymentBatch.Status.Failed, event2.getCarriedState().getStatus());
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends PaymentBatchesCHApplication{

    }
}