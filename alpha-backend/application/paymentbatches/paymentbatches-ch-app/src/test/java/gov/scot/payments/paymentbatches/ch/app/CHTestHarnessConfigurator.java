package gov.scot.payments.paymentbatches.ch.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import gov.scot.payments.application.component.commandhandler.stateful.*;
import gov.scot.payments.application.kafka.MessageHeaderEnricher;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.*;
import gov.scot.payments.payments.model.aggregate.CashAccount;
import gov.scot.payments.payments.model.aggregate.PartyIdentification;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.Function3;
import io.vavr.Function5;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.jackson.datatype.VavrModule;
import lombok.Builder;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.Stores;
import org.springframework.kafka.support.JacksonUtils;
import org.springframework.kafka.support.converter.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.messaging.MessageHeaders;

import javax.money.CurrencyUnit;
import java.util.Map;

import static org.mockito.Mockito.mock;

@Builder
public class CHTestHarnessConfigurator<S extends HasKey<String>,  AS extends AggregateState<S>> {

    Class<S> stateClass;
    Class<AS> aggregateStateClass;
    List<Class<? extends Command>> commandClasses;
    List<Class<? extends EventWithCauseImpl>> eventClasses;

    Function5<SerializedMessage, CommandFailureInfo, S, S, Long, AS> aggregateStateSupplier;
    StateUpdateFunction<AS, S> stateUpdateFunction;
    EventGeneratorFunction<AS, EventWithCauseImpl> eventGeneratorFunction;
    Function3<Command, S, Throwable, List<EventWithCauseImpl>> errorHandler;

    public KafkaStreamsTestHarness createAndSetUpTestHarness() {

        var mappings = List.of(GenericErrorEvent.class, CommandFailureInfo.class, AccessDeniedEvent.class, stateClass, aggregateStateClass)
                .appendAll(commandClasses)
                .appendAll(eventClasses);

        var harness =  KafkaStreamsTestHarness.builderWithMappings(
                Serdes.ByteArray()
                , new gov.scot.payments.testing.kafka.JsonSerde<>()
                , mappings.toJavaList().toArray(new Class[0]))
                .build();

        setupProcessor(harness);
        return harness;
    }

    private void setupProcessor(KafkaStreamsTestHarness testHarness) {

        @SuppressWarnings("unchecked")
        AggregateLookupService<S, AS> mockAggregateLookupService = mock(AggregateLookupService.class);

        final MicrometerMetrics metrics = new MicrometerMetrics(new SimpleMeterRegistry());
        ObjectMapper mapper = JacksonUtils.enhancedObjectMapper();
        mapper.activateDefaultTyping(
                BasicPolymorphicTypeValidator.builder()
                        .allowIfBaseType(Command.class)
                        .allowIfBaseType(Set.class)
                        .allowIfBaseType(Map.class)
                        .allowIfBaseType(List.class)
                        .allowIfSubType(stateClass)
                        .allowIfSubType(CashAccount.class)
                        .allowIfSubType(PartyIdentification.class)
                        .allowIfBaseType(CurrencyUnit.class)
                        .build()
        );
        mapper.registerModule(new VavrModule());
        JsonSerde<AS> stateSerde = new JsonSerde<>(aggregateStateClass, mapper);
        JsonSerde<Command> commandSerde = new JsonSerde<>(Command.class, mapper);
        DefaultJackson2JavaTypeMapper typeMapper = (DefaultJackson2JavaTypeMapper) ((JsonDeserializer<Command>) commandSerde.deserializer()).getTypeMapper();
        typeMapper.setClassIdFieldName(MessageHeaders.CONTENT_TYPE);
        final String mappingConfig = KafkaStreamsTestHarness.buildMappingsConfig(commandClasses.toJavaList().toArray(new Class[0]));
        commandSerde.configure(HashMap.of(JsonSerializer.TYPE_MAPPINGS, mappingConfig).toJavaMap(), false);

        Materialized<byte[], AS, KeyValueStore<Bytes, byte[]>> store = Materialized.<byte[], AS>as(Stores.inMemoryKeyValueStore(KafkaStreamsTestHarness.STATE_STORE))
                .withKeySerde(Serdes.ByteArray())
                .withValueSerde(stateSerde);

        PerCommandStatefulCommandHandlerDelegate<S, AS, EventWithCauseImpl> processor =
                PerCommandStatefulCommandHandlerDelegate
                        .<S, AS, EventWithCauseImpl>builder()
                        .aggregateQueryService(mockAggregateLookupService)
                        .metrics(metrics)
                        .aggregateStateSupplier(aggregateStateSupplier)
                        .eventGenerateFunction(eventGeneratorFunction)
                        .errorHandler(errorHandler)
                        .stateUpdateFunction(stateUpdateFunction)
                        .stateStore(store)
                        .commandSerde(commandSerde)
                        .stateTopic("state")
                        .stateHeaderEnricher(() -> new MessageHeaderEnricher<>("paymentbatches"))
                        .serializedMessageSerde(new JsonSerde<>(SerializedMessage.class, mapper))
                        .build();

        processor.apply(testHarness.stream()).to(KafkaStreamsTestHarness.OUTPUT_TOPIC);
    }

}
