package gov.scot.payments.paymentbatches.ch.app;


import gov.scot.payments.model.Command;
import gov.scot.payments.paymentbatches.model.aggregate.PaymentBatch;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchCreatedEvent;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchFailedEvent;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchOpenedEvent;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchUpdatedEvent;
import io.vavr.collection.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.net.MalformedURLException;
import java.util.UUID;

import static gov.scot.payments.paymentbatches.ch.app.PaymentBatchExampleData.STATE_VERSION;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

public class PaymentBatchesCHApplicationTest {

    private PaymentBatchTestHarness harness;

    @BeforeEach
    void setUp() {
        harness = new PaymentBatchTestHarness();
    }

    @Test
    @DisplayName("Create a payment Batch ")
    public void testCreatePaymentBatchCommand() throws MalformedURLException {

        PaymentBatch batch = PaymentBatchExampleData.createExampleBatch();
        Command createBatchCommand = PaymentBatchExampleData.createPaymentBatchCreateCommand(batch);
        var result = harness.setNullStateAndSendCommand(createBatchCommand);

        var expectedEvent = PaymentBatchCreatedEvent.builder()
                .stateVersion(STATE_VERSION)
                .correlationId(createBatchCommand.getMessageId())
                .executionCount(1)
                .carriedState(batch)
                .build();
        var event = result.getEvent();
        assertEquals(PaymentBatchCreatedEvent.class, result.getEvent().getClass());
        assertThat(expectedEvent)
                .usingRecursiveComparison()
                .ignoringFields("messageId", "timestamp")
                .isEqualTo(event);

        var state = result.getState();
        assertThat(state.getCurrent()).isEqualToIgnoringGivenFields(batch);
        assertEquals(state.getCurrentVersion(), STATE_VERSION);

        assertEquals(1, harness.getNumberOfStateUpdateCalls());
        assertEquals(1, harness.getPaymentBatchEventGeneratorCalls());

    }

    @Test
    @DisplayName("Open Payment Batch when state is null")
    public void testOpenPaymentBatchWhenStateNull() throws MalformedURLException {

        PaymentBatch batch = PaymentBatchExampleData.createExampleBatch();
        var initialStateVersion = 1L;

        var openBatchCommand = PaymentBatchExampleData.createPaymentBatchOpenCommand(batch.getId());
        var result = harness.setNullStateAndSendCommand(openBatchCommand );

        var state = result.getState();
        assertEquals("IllegalPaymentBatchStateException", state.getError().getErrorType());
        assertEquals("Cannot open payment batch as it is not in 'New' state", state.getError().getErrorMessage());

    }

    @ParameterizedTest
    @EnumSource(value=PaymentBatch.Status.class, names = {"New" }, mode = EnumSource.Mode.EXCLUDE)
    @DisplayName("Open Payment Batch throw error when state is invalid")
    public void testOpenPaymentBatchWhenStateInvalid(PaymentBatch.Status status) throws MalformedURLException {

        PaymentBatch batch = PaymentBatchExampleData.createExampleBatch(status);

        var openBatchCommand = PaymentBatchExampleData.createPaymentBatchOpenCommand(batch.getId());
        var result = harness.setNullStateAndSendCommand(openBatchCommand );

        var state = result.getState();
        assertEquals("IllegalPaymentBatchStateException", state.getError().getErrorType());
        assertEquals("Cannot open payment batch as it is not in 'New' state", state.getError().getErrorMessage());

    }


    @Test
    @DisplayName("Open Payment Batch ")
    public void testOpenPaymentBatchCommand() throws MalformedURLException {

        PaymentBatch batch = PaymentBatchExampleData.createExampleBatch();
        var initialStateVersion = 1L;

        var openBatchCommand = PaymentBatchExampleData.createPaymentBatchOpenCommand(batch.getId());
        var result = harness.setStateAndSendCommand(batch, initialStateVersion, openBatchCommand );

        var expectedBatch = batch.toBuilder().status(PaymentBatch.Status.Open).build();
        var expectedEvent = PaymentBatchOpenedEvent.builder()
                .stateVersion(initialStateVersion + 1)
                .correlationId(openBatchCommand.getMessageId())
                .executionCount(1)
                .carriedState(batch)
                .build();

        var event = result.getEvent();
        assertEquals(PaymentBatchOpenedEvent.class, result.getEvent().getClass());

        assertThat(expectedEvent)
                .usingRecursiveComparison()
                .ignoringFields("messageId", "timestamp")
                .isEqualTo(event);

        var state = result.getState();
        assertThat(state.getCurrent()).isEqualToIgnoringGivenFields(expectedBatch);
        assertEquals(state.getCurrentVersion(), Long.valueOf(initialStateVersion + 1));

        assertEquals(1, harness.getNumberOfStateUpdateCalls());
        assertEquals(1, harness.getPaymentBatchEventGeneratorCalls());

    }

    @ParameterizedTest
    @EnumSource(value=PaymentBatch.Status.class)
    @DisplayName("Create Payment Batch throw error when state exists")
    public void testCreatePaymentBatchWhenStateInvalid(PaymentBatch.Status status) throws MalformedURLException {

        PaymentBatch batch = PaymentBatchExampleData.createExampleBatch(status);
        var openBatchCommand = PaymentBatchExampleData.createPaymentBatchCreateCommand();
        var result = harness.setStateAndSendCommand(batch, 1L, openBatchCommand );

        var state = result.getState();
        assertEquals("IllegalPaymentBatchStateException", state.getError().getErrorType());
        assertEquals("Cannot create payment batch as it already exists", state.getError().getErrorMessage());

    }

    @Test
    @DisplayName("Fail Payment Batch ")
    public void testFailPaymentBatchCommand() throws MalformedURLException {

        PaymentBatch batch = PaymentBatchExampleData.createExampleBatch();
        var initialStateVersion = 1L;

        var openBatchCommand = PaymentBatchExampleData.createPaymentBatchFailCommand(batch.getId());
        var result = harness.setStateAndSendCommand(batch, initialStateVersion, openBatchCommand );

        var expectedBatch = batch.toBuilder().status(PaymentBatch.Status.Failed).build();
        var expectedEvent = PaymentBatchFailedEvent.builder()
                .stateVersion(initialStateVersion + 1)
                .correlationId(openBatchCommand.getMessageId())
                .executionCount(1)
                .carriedState(batch)
                .build();

        var event = result.getEvent();
        assertEquals(PaymentBatchFailedEvent.class, result.getEvent().getClass());

        assertThat(expectedEvent)
                .usingRecursiveComparison()
                .ignoringFields("messageId", "timestamp")
                .isEqualTo(event);

        var state = result.getState();
        assertThat(state.getCurrent()).isEqualToIgnoringGivenFields(expectedBatch);
        assertEquals(state.getCurrentVersion(), Long.valueOf(initialStateVersion + 1));

        assertEquals(1, harness.getNumberOfStateUpdateCalls());
        assertEquals(1, harness.getPaymentBatchEventGeneratorCalls());

    }

    @Test
    @DisplayName("Fail Payment Batch when state is null")
    public void testFailPaymentBatchCommandWhenStateNull() throws MalformedURLException {

        var openBatchCommand = PaymentBatchExampleData.createPaymentBatchFailCommand("batch id");
        var result = harness.setNullStateAndSendCommand(openBatchCommand );

        var state = result.getState();
        assertEquals("IllegalPaymentBatchStateException", state.getError().getErrorType());
        assertEquals("Cannot fail payment batch as it is not in 'New' state", state.getError().getErrorMessage());

    }

    @ParameterizedTest
    @EnumSource(value=PaymentBatch.Status.class, names = {"New" }, mode = EnumSource.Mode.EXCLUDE)
    @DisplayName("Fail Payment Batch throws error when state is invalid")
    public void testFailPaymentBatchCommandWhenStateInvalid(PaymentBatch.Status status) throws MalformedURLException {

        PaymentBatch batch = PaymentBatchExampleData.createExampleBatch(status);
        var openBatchCommand = PaymentBatchExampleData.createPaymentBatchFailCommand(batch.getId());
        var result = harness.setStateAndSendCommand(batch, 1L, openBatchCommand );

        var state = result.getState();
        assertEquals("IllegalPaymentBatchStateException", state.getError().getErrorType());
        assertEquals("Cannot fail payment batch as it is not in 'New' state", state.getError().getErrorMessage());

    }

    @Test
    @DisplayName("Add Payment to Batch")
    public void testAddPaymentToBatch() throws MalformedURLException {

        PaymentBatch batch = PaymentBatchExampleData.createExampleBatch();
        batch = batch.toBuilder().status(PaymentBatch.Status.Open).build();

        var initialStateVersion = 1L;

        var examplePayment = PaymentBatchExampleData.createExamplePayment(batch.getId());
        var addToPaymentCommand = PaymentBatchExampleData.createAddToPaymentBatchCommand(examplePayment, batch.getId());
        var expectedBatch = batch.toBuilder().payments(List.of(examplePayment.getId())).build();

        var expectedEvent = PaymentBatchUpdatedEvent.builder()
                .stateVersion(initialStateVersion + 1)
                .correlationId(addToPaymentCommand.getMessageId())
                .executionCount(1)
                .carriedState(batch)
                .payment(examplePayment)
                .user("test-user")
                .build();

        var result = harness.setStateAndSendCommand(batch, initialStateVersion, addToPaymentCommand );

        var event = result.getEvent();
        assertEquals(PaymentBatchUpdatedEvent.class, result.getEvent().getClass());

        assertThat(expectedEvent)
                .usingRecursiveComparison()
                .ignoringFields("messageId", "timestamp")
                .isEqualTo(event);

        var state = result.getState();
        assertThat(state.getCurrent()).isEqualToIgnoringGivenFields(expectedBatch);
        assertEquals(state.getCurrentVersion(), Long.valueOf(initialStateVersion + 1));

        assertEquals(1, harness.getNumberOfStateUpdateCalls());
        assertEquals(1, harness.getPaymentBatchEventGeneratorCalls());

    }

    @ParameterizedTest
    @ValueSource(strings = {"New", "Failed", "Closed"})
    public void testAddPaymentToInvalidBatch(String input) throws MalformedURLException {

        PaymentBatch batch = PaymentBatchExampleData.createExampleBatch(PaymentBatch.Status.valueOf(input));

        var examplePayment = PaymentBatchExampleData.createExamplePayment(batch.getId());
        var addToPaymentCommand = PaymentBatchExampleData.createAddToPaymentBatchCommand(examplePayment, batch.getId());
        var result = harness.setNullStateAndSendCommand(addToPaymentCommand );

        var state = result.getState();
        assertEquals("IllegalPaymentBatchStateException", state.getError().getErrorType());
        assertEquals("Cannot add to payment batch as it is not in open state", state.getError().getErrorMessage());

    }

    @Test
    @DisplayName("Add Payment to Batch")
    public void testAddPaymentToNewBatch() throws MalformedURLException {

        PaymentBatch batch = PaymentBatchExampleData.createExampleBatch();

        var examplePayment = PaymentBatchExampleData.createExamplePayment(batch.getId());
        var addToPaymentCommand = PaymentBatchExampleData.createAddToPaymentBatchCommand(examplePayment, batch.getId());
        var result = harness.setNullStateAndSendCommand(addToPaymentCommand );

        var state = result.getState();
        assertEquals("IllegalPaymentBatchStateException", state.getError().getErrorType());
        assertEquals("Cannot add to payment batch as it is not in open state", state.getError().getErrorMessage());

    }


}