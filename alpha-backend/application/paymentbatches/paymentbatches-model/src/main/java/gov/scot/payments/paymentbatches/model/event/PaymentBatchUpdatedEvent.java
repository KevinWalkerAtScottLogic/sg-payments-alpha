package gov.scot.payments.paymentbatches.model.event;

import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.Payment;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "paymentbatches", type = "batchUpdated")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PaymentBatchUpdatedEvent extends BasePaymentBatchEventWithCause {

    @NotNull String user;

    @NotNull Payment payment;

}

