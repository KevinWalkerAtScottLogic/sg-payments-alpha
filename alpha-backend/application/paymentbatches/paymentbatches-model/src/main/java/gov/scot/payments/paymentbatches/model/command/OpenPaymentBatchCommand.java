package gov.scot.payments.paymentbatches.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.HasStateVersion;
import gov.scot.payments.model.MessageType;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "paymentbatches", type = "openBatch")
@NoArgsConstructor
@ToString
public class OpenPaymentBatchCommand extends CommandImpl implements HasStateVersion, HasKey<String> {

    @NonNull private String batchId;
    @Nullable private Long stateVersion;
    @NonNull private String user;

    public OpenPaymentBatchCommand(String batchId, Long stateVersion, String user, boolean reply) {
        super(reply);
        this.batchId = batchId;
        this.stateVersion = stateVersion;
        this.user = user;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return batchId;
    }
}
