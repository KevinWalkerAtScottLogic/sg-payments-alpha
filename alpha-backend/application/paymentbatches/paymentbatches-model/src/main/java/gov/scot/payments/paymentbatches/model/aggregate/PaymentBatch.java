package gov.scot.payments.paymentbatches.model.aggregate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.MessageType;
import io.vavr.collection.List;
import lombok.*;
import org.apache.avro.reflect.Nullable;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Builder(toBuilder = true)
@MessageType(context = "paymentbatches", type = "batch")
public class PaymentBatch implements HasKey<String> {

    @EqualsAndHashCode.Include @NonNull private String id;

    @Nullable private String fileString;
    @NonNull private String name;
    @NonNull @Builder.Default private List<UUID> payments = List.empty();
    @NonNull private String customerId;
    @NonNull @Builder.Default private Instant createdAt = Instant.now();
    @NonNull private String createdBy;
    @Nullable private String message;
    @NonNull @Builder.Default private Status status = Status.New;

    public enum Status {
        New,
        Open,
        Failed,
        Closed
    }

    @JsonIgnore
    public URL getFile(){
        try {
            return new URL(fileString);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return id;
    }

    public static class PaymentBatchBuilder {

        public PaymentBatchBuilder file(URL url){
            this.fileString = url.toString();
            return this;
        }
    }
}
