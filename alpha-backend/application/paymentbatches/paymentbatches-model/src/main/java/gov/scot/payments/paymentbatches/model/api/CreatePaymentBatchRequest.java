package gov.scot.payments.paymentbatches.model.api;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.Payment;
import io.vavr.collection.List;
import lombok.*;

import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Builder
public class CreatePaymentBatchRequest {

    @EqualsAndHashCode.Include @NonNull @Builder.Default private String id = UUID.randomUUID().toString();
    @NonNull private String name;
    @NonNull @Builder.Default private List<Payment> payments = List.empty();
    @NonNull private CompositeReference product;
}
