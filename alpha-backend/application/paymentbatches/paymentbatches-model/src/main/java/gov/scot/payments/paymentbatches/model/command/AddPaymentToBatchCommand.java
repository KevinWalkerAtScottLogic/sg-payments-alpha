package gov.scot.payments.paymentbatches.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.HasStateVersion;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.payments.model.aggregate.Payment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "paymentbatches", type = "addPaymentToBatch")
@NoArgsConstructor
public class AddPaymentToBatchCommand extends CommandImpl implements HasStateVersion, HasKey<String> {

    @NonNull private Payment payment;
    @Nullable private Long stateVersion;
    @NonNull private String user;

    public AddPaymentToBatchCommand(Payment payment, Long stateVersion, String user, boolean reply) {
        super(reply);
        this.stateVersion = stateVersion;
        this.user = user;
        this.payment = payment;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return payment.getBatchId();
    }
}
