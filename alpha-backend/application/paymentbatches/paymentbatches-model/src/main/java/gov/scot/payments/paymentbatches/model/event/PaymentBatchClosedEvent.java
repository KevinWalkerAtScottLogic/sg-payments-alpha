package gov.scot.payments.paymentbatches.model.event;

import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "paymentbatches", type = "batchClosed")
@NoArgsConstructor
public class PaymentBatchClosedEvent extends  BasePaymentBatchEventWithCause {

}
