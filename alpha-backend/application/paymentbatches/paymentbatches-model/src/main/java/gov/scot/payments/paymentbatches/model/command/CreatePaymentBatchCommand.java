package gov.scot.payments.paymentbatches.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.HasStateVersion;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.paymentbatches.model.aggregate.PaymentBatch;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "paymentbatches", type = "createBatch")
@NoArgsConstructor
@ToString
public class CreatePaymentBatchCommand extends CommandImpl implements HasStateVersion, HasKey<String> {

    @NonNull private PaymentBatch batch;
    @Nullable private Long stateVersion;
    @NonNull private String user;

    public CreatePaymentBatchCommand(PaymentBatch batch, Long stateVersion,String user, boolean reply) {
        super(reply);
        this.batch = batch;
        this.stateVersion = stateVersion;
        this.user = user;
    }


    @Override
    @JsonIgnore
    public String getKey() {
        return batch.getKey();
    }
}
