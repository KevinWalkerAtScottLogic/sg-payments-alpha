package gov.scot.payments.paymentbatches.proj.app;

import gov.scot.payments.application.component.projector.springdata.jpa.JpaProjectionRepository;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.paymentbatches.proj.model.PaymentBatchProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PaymentBatchesRepository extends JpaProjectionRepository<String, PaymentBatchProjection>{

    @Query("from PaymentBatchProjection p where " +
            " p.product = (?#{#principal.getScopesAsString()}) "+
            "or  p.customer in (?#{#principal.getRootScopesAsString()}) " +
            "or ?#{#principal.hasGlobalAccess()} = true ")
    Page<PaymentBatchProjection> findAll(Pageable pageable, @Param("principal") Subject subject);

    @Query("from PaymentBatchProjection p where p.id = ?#{#id} and " +
            "( p.product = (?#{#principal.getScopesAsString()}) "+
            "or p.customer in (?#{#principal.getRootScopesAsString()}) " +
            "or ?#{#principal.hasGlobalAccess()} = true )")
    Optional<PaymentBatchProjection> findById(@Param("id") String id,  @Param("principal") Subject subject);
}
