--CREATE domain IF NOT EXISTS jsonb AS other;

CREATE TABLE PAYMENT_BATCH_PROJECTION (
    id VARCHAR(255) NOT NULL PRIMARY KEY,
    processing_time TIMESTAMP NOT NULL,
    total_number_of_payments INTEGER NOT NULL,
    total_error_payments INTEGER NOT NULL,
    total_pending_payments INTEGER NOT NULL,
    status VARCHAR(20) NOT NULL,
    name VARCHAR(255) NULL,
    error_message VARCHAR(255) NULL
);

CREATE TABLE PAYMENT_BATCH_PROJECTION_CURRENCY_TOTALS (
    currency_str VARCHAR(255) NOT NULL,
    total_amount NUMERIC NOT NULL,
    payment_batch_projection_id VARCHAR(255) NOT NULL REFERENCES PAYMENT_BATCH_PROJECTION (id),
    PRIMARY KEY (payment_batch_projection_id, currency_str)
);

CREATE TABLE PAYMENT_BATCH_PROJECTION_PRODUCTS (
    products VARCHAR(255) NOT NULL,
    payment_batch_projection_id VARCHAR(255) NOT NULL REFERENCES PAYMENT_BATCH_PROJECTION (id),
    PRIMARY KEY (payment_batch_projection_id, products)
);

CREATE INDEX payment_batches_processing_time ON PAYMENT_BATCH_PROJECTION(processing_time DESC);