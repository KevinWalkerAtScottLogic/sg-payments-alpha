package gov.scot.payments.paymentbatches.proj.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.component.projector.springdata.RepositoryMutatingStorageService;
import gov.scot.payments.application.component.projector.springdata.jpa.JpaProjectionRepository;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.paymentbatches.model.aggregate.PaymentBatch;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchCreatedEvent;
import gov.scot.payments.paymentbatches.model.event.PaymentBatchUpdatedEvent;
import gov.scot.payments.paymentbatches.proj.model.PaymentBatchProjection;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import gov.scot.payments.testing.security.MockSubjectAuthentication;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.vavr.collection.List;
import org.apache.kafka.streams.KafkaStreams;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.util.UriBuilder;

import java.math.BigDecimal;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;
import java.util.function.Function;

import static gov.scot.payments.testing.security.MockSubjectAuthentication.user;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockAuthentication;

@ApplicationIntegrationTest(classes = {PaymentBatchesProjApplicationIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.PROJECTOR)
public class PaymentBatchesProjApplicationIntegrationTest  {

    @Autowired
    PaymentBatchesRepository repository;

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, EmbeddedBrokerClient brokerClient){
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(20));
        repository.deleteAll();
    }


    @Test
    @DisplayName("Given one product check projection ACL when querying all ")
    public void testRepositoryFindAllAcl(@Autowired WebTestClient client, @Autowired PaymentBatchesRepository repository) throws InterruptedException {

        CompositeReference scope1 = CompositeReference.parse("customer.product");
        CompositeReference scope2 = CompositeReference.parse("customer.product2");
        CompositeReference scope3 = CompositeReference.parse("customer2.product");

        var exampleBatch = PaymentBatchExampleData.createExampleBatch(PaymentBatch.Status.New);

        var currencyTotals = new HashMap<String, BigDecimal>();
        currencyTotals.put("GBP", new BigDecimal(12.00));

        var prodcuts = new HashSet<String>();
        prodcuts.add("product");

        PaymentBatchProjection projection = PaymentBatchProjection.builder()
                .id(exampleBatch.getId())
                .status(PaymentBatch.Status.Closed)
                .totalNumberOfPayments(10)
                .currencyTotals(currencyTotals)
                .products(prodcuts)
                .product("customer.product")
                .processingTime(Instant.now())
                .customer("customer")
                .name("mybatch")
                .build();


        JpaProjectionRepository saveRepo = repository;
        saveRepo.save(projection);

        var projection2 = projection.toBuilder().id(UUID.randomUUID().toString()).customer(scope2.getComponent0()).product(scope2.toString()).build();
        saveRepo.save(projection2);
        var projection3 = projection.toBuilder().id(UUID.randomUUID().toString()).customer(scope3.getComponent0()).product(scope3.toString()).build();
        saveRepo.save(projection3);

        var idPath = "/";
        webQueryRepository(client, idPath, "./paymentbatches:Read")
                .jsonPath("$._embedded.paymentBatchProjectionList.size()").isEqualTo(3);
        webQueryRepository(client, idPath, "customer/paymentbatches:Read")
                .jsonPath("$._embedded.paymentBatchProjectionList.size()").isEqualTo(2);
        webQueryRepository(client, idPath, "customer.product/paymentbatches:Read")
                .jsonPath("$._embedded.paymentBatchProjectionList.size()").isEqualTo(1);
        webQueryRepository(client, idPath, "customer.product2/paymentbatches:Read")
                .jsonPath("$._embedded.paymentBatchProjectionList.size()").isEqualTo(1);
        webQueryRepository(client, idPath, "customer2/paymentbatches:Read")
                .jsonPath("$._embedded.paymentBatchProjectionList.size()").isEqualTo(1);

        webQueryRepository(client, idPath, "customer.product3/paymentbatches:Read")
                .jsonPath("$._embedded.paymentBatchProjectionList").doesNotExist();
        webQueryRepository(client, idPath, "customer2.product2/paymentbatches:Read")
                .jsonPath("$._embedded.paymentBatchProjectionList").doesNotExist();
        webQueryRepository(client, idPath, "customer3/paymentbatches:Read")
                .jsonPath("$._embedded.paymentBatchProjectionList").doesNotExist();

    }

    @Test
    @DisplayName("Given one product check projection ACL when querying by id ")
    public void testRepositoryFindByIdAcl(@Autowired WebTestClient client, @Autowired PaymentBatchesRepository repository) throws InterruptedException {

        var exampleBatch = PaymentBatchExampleData.createExampleBatch(PaymentBatch.Status.New);

        var currencyTotals = new HashMap<String, BigDecimal>();
        currencyTotals.put("GBP", new BigDecimal(12.00));

        var prodcuts = new HashSet<String>();
        prodcuts.add("product");

        PaymentBatchProjection projection = PaymentBatchProjection.builder()
                .id(exampleBatch.getId().toString())
                .status(PaymentBatch.Status.Closed)
                .totalNumberOfPayments(10)
                .currencyTotals(currencyTotals)
                .products(prodcuts)
                .product("customer.product")
                .processingTime(Instant.now())
                .customer("customer")
                .name("mybatch")
                .build();

        JpaProjectionRepository saveRepo = repository;
        saveRepo.save(projection);

        var idPath = "/"+exampleBatch.getId().toString();
        webQueryRepository(client, idPath, "./paymentbatches:Read")
                .jsonPath("$.id").isEqualTo(exampleBatch.getId().toString());
        webQueryRepository(client, idPath, "customer/paymentbatches:Read")
                .jsonPath("$.id").isEqualTo(exampleBatch.getId().toString());
        webQueryRepository(client, idPath, "customer.product/paymentbatches:Read")
                .jsonPath("$.id").isEqualTo(exampleBatch.getId().toString());

        webQueryRepositoryExpectNotFound(client, idPath, "customer.product2/paymentbatches:Read");
        webQueryRepositoryExpectNotFound(client, idPath, "customer2.product/paymentbatches:Read");
        webQueryRepositoryExpectNotFound(client, idPath, "customer2/paymentbatches:Read");

    }

    @Test
    @DisplayName("Check projection ACL when using search query")
    public void testRepositorySearchAcl(@Autowired WebTestClient client, @Autowired PaymentBatchesRepository repository) throws InterruptedException {

        CompositeReference scope1 = CompositeReference.parse("customer.product");
        CompositeReference scope2 = CompositeReference.parse("customer.product2");
        CompositeReference scope3 = CompositeReference.parse("customer2.product");

        var exampleBatch = PaymentBatchExampleData.createExampleBatch(PaymentBatch.Status.New);

        var currencyTotals = new HashMap<String, BigDecimal>();
        currencyTotals.put("GBP", new BigDecimal(12.00));

        var prodcuts = new HashSet<String>();
        prodcuts.add("product");

        PaymentBatchProjection projection = PaymentBatchProjection.builder()
                .id(exampleBatch.getId().toString())
                .status(PaymentBatch.Status.Closed)
                .totalNumberOfPayments(10)
                .currencyTotals(currencyTotals)
                .products(prodcuts)
                .product(scope1.toString())
                .processingTime(Instant.now())
                .customer(scope1.getComponent0())
                .name("mybatch")
                .build();

        JpaProjectionRepository saveRepo = repository;
        saveRepo.save(projection);
        var projection2 = projection.toBuilder().id(UUID.randomUUID().toString()).customer(scope2.getComponent0()).product(scope2.toString()).build();
        saveRepo.save(projection2);
        var projection3 = projection.toBuilder().id(UUID.randomUUID().toString()).customer(scope3.getComponent0()).product(scope3.toString()).build();
        saveRepo.save(projection3);

        Function<UriBuilder, URI> searchQuery = uri -> uri.path("/search")
                .queryParam("query", "id==*")
                .build();

        webQueryRepository(client, searchQuery, "./paymentbatches:Read")
                .jsonPath("$._embedded.paymentBatchProjectionList.size()").isEqualTo(3);
        webQueryRepository(client, searchQuery, "customer/paymentbatches:Read")
                .jsonPath("$._embedded.paymentBatchProjectionList.size()").isEqualTo(2);
        webQueryRepository(client, searchQuery, "customer.product/paymentbatches:Read")
                .jsonPath("$._embedded.paymentBatchProjectionList.size()").isEqualTo(1);
        webQueryRepository(client, searchQuery, "customer.product2/paymentbatches:Read")
                .jsonPath("$._embedded.paymentBatchProjectionList.size()").isEqualTo(1);
        webQueryRepository(client, searchQuery, "customer2/paymentbatches:Read")
                .jsonPath("$._embedded.paymentBatchProjectionList.size()").isEqualTo(1);

        webQueryRepository(client, searchQuery, "customer.product3/paymentbatches:Read")
                .jsonPath("$._embedded.paymentBatchProjectionList").doesNotExist();
        webQueryRepository(client, searchQuery, "customer2.product2/paymentbatches:Read")
                .jsonPath("$._embedded.paymentBatchProjectionList").doesNotExist();
        webQueryRepository(client, searchQuery, "customer3/paymentbatches:Read")
                .jsonPath("$._embedded.paymentBatchProjectionList").doesNotExist();

    }


    @Test
    public void testGetMultiplePaymentBatches(@Autowired WebTestClient client,  EmbeddedBrokerClient brokerClient){

        var exampleBatch = PaymentBatchExampleData.createExampleBatch(PaymentBatch.Status.New);
        var event = PaymentBatchCreatedEvent.builder()
                .stateVersion(1L)
                .correlationId(UUID.randomUUID())
                .executionCount(1)
                .carriedState(exampleBatch)
                .build();

        var kv = KeyValueWithHeaders.msg(event.getKey(), event);
        brokerClient.sendKeyValuesToDestination("events-internal", List.of(kv));

        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("./paymentbatches:Read"))))
                .get()
                .uri("/")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$._embedded.paymentBatchProjectionList.size()").isEqualTo(1);

    }


    @Test
    public void test(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){

        var exampleBatch = PaymentBatchExampleData.createExampleBatch(PaymentBatch.Status.New);
        var event = PaymentBatchCreatedEvent.builder()
                .stateVersion(1L)
                .correlationId(UUID.randomUUID())
                .executionCount(1)
                .carriedState(exampleBatch)
                .build();

        var kv = KeyValueWithHeaders.msg(event.getKey(), event);
        brokerClient.sendKeyValuesToDestination("events-internal", List.of(kv));

        client
            .mutateWith(mockAuthentication(user("pass", Role.parse("./paymentbatches:Read"))))
            .get()
            .uri("/"+exampleBatch.getId().toString())
            .exchange()
            .expectStatus()
            .isOk()
            .expectBody()
            .jsonPath("$.id").isEqualTo(exampleBatch.getId().toString());

    }

    @Test
    public void testUpdatedPayment(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient) throws InterruptedException {

        var exampleBatch = PaymentBatchExampleData.createExampleBatch(PaymentBatch.Status.Open);
        var payment = PaymentBatchExampleData.createExamplePayment(exampleBatch.getId());
        var event = PaymentBatchCreatedEvent.builder()
                .stateVersion(1L)
                .correlationId(UUID.randomUUID())
                .executionCount(1)
                .carriedState(exampleBatch)
                .build();

        var eventUpdated = PaymentBatchUpdatedEvent.builder()
                .user("test-user")
                .stateVersion(1L)
                .correlationId(UUID.randomUUID())
                .executionCount(1)
                .carriedState(exampleBatch)
                .payment(payment)
                .build();

        var kv = KeyValueWithHeaders.msg(event.getKey(), event);
        var kv2 = KeyValueWithHeaders.msg(eventUpdated.getKey(), eventUpdated);
        brokerClient.sendKeyValuesToDestination("events-internal", List.of(kv, kv2));
        client
            .mutateWith(mockAuthentication(user("pass", Role.parse("./paymentbatches:Read"))))
            .get()
            .uri("/"+exampleBatch.getId().toString())
            .exchange()
            .expectStatus()
            .isOk()
            .expectBody()
            .jsonPath("$.id").isEqualTo(exampleBatch.getId().toString())
            .jsonPath("$.totalNumberOfPayments").isEqualTo("1")
            .jsonPath("$.currencyTotals.GBP").isEqualTo("12.00")
            .jsonPath("$.products").isEqualTo("product");


    }

    @Test
    public void testSearch(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){

        var exampleBatch = PaymentBatchExampleData.createExampleBatch(PaymentBatch.Status.New);
        var event = PaymentBatchCreatedEvent.builder()
                .stateVersion(1L)
                .correlationId(UUID.randomUUID())
                .executionCount(1)
                .carriedState(exampleBatch)
                .build();

        var exampleBatch2 = PaymentBatchExampleData.createExampleBatch(PaymentBatch.Status.New);
        var event2 = PaymentBatchCreatedEvent.builder()
                .stateVersion(1L)
                .correlationId(UUID.randomUUID())
                .executionCount(1)
                .carriedState(exampleBatch2)
                .build();

        brokerClient.sendToDestination("events-internal", List.of(event, event2));

        var results = client
            .mutateWith(mockAuthentication(user("pass", Role.parse("./paymentbatches:Read"))))
            .get()
            .uri(uri -> uri.path("/search")
                    .queryParam("query","id==*")
                    .build())
            .exchange()
            .expectStatus()
            .isOk()
            .expectBody()
            .jsonPath("$._embedded.paymentBatchProjectionList.size()").isEqualTo(2)
            .returnResult();

    }


    private WebTestClient.BodyContentSpec webQueryRepository(WebTestClient client, String uri, String role) {
        return webQueryRepository(client, uriBuilder -> uriBuilder.path(uri).build(), role);
    }

    private WebTestClient.BodyContentSpec webQueryRepository(WebTestClient client, String uri) {
        return webQueryRepository(client, uriBuilder -> uriBuilder.path(uri).build(), "./payments:Read");
    }

    private WebTestClient.BodyContentSpec webQueryRepository(WebTestClient client, Function<UriBuilder, URI> uriFunction) {
        return webQueryRepository(client, uriFunction, "./payments:Read");
    }

    private WebTestClient.BodyContentSpec webQueryRepository(WebTestClient client, Function<UriBuilder, URI> uriFunction, String role) {
        return client
                .mutateWith(mockAuthentication(user("pass", Role.parse(role))))
                .get()
                .uri(uriFunction)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody();
    }
    private WebTestClient.BodyContentSpec webQueryRepository(WebTestClient client, String uri, Role ...roles) {
        return client
                .mutateWith(mockAuthentication(user("pass", roles)))
                .get()
                .uri(uriBuilder -> uriBuilder.path(uri).build())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody();
    }

    private WebTestClient.BodyContentSpec webQueryRepository(WebTestClient client, Function<UriBuilder, URI> uriFunction, Role ...roles) {
        return client
                .mutateWith(mockAuthentication(user("pass", roles)))
                .get()
                .uri(uriFunction)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody();
    }

    private void webQueryRepositoryExpectNotFound(WebTestClient client, String uri, String role) {
        client
                .mutateWith(mockAuthentication(user("pass", Role.parse(role))))
                .get()
                .uri(uriBuilder -> uriBuilder.path(uri).build())
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    @EnableAutoConfiguration(exclude = {HypermediaAutoConfiguration.class})
    public static class TestApplication extends PaymentBatchesProjApplication {

        @Bean
        public RepositoryMutatingStorageService<String, PaymentBatchProjection> handler(Metrics metrics, PaymentBatchesRepository repository){
            return new PaymentBatchesStorageService(metrics,repository);
        }

    }

}