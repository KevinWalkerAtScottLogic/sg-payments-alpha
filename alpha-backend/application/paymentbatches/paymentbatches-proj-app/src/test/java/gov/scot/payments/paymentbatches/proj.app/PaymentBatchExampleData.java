package gov.scot.payments.paymentbatches.proj.app;

import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.paymentbatches.model.aggregate.PaymentBatch;
import gov.scot.payments.paymentbatches.model.command.*;
import gov.scot.payments.payments.model.aggregate.*;
import io.vavr.collection.List;
import lombok.SneakyThrows;
import org.javamoney.moneta.Money;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.util.UUID;

public class PaymentBatchExampleData {

    public static final Long STATE_VERSION = 1L;

    public static Payment createExamplePayment(String batchId) {

        return Payment.builder()
                .amount(Money.of(12.00, "GBP"))
                .batchId(batchId)
                .createdBy("test-user")
                .amount(Money.of(new BigDecimal("12.0"), "GBP"))
                .allowedMethods(List.of(PaymentMethod.Bacs))
                .latestExecutionDate(Instant.now())
                .status(PaymentStatus.Paid)
                .creditor(PartyIdentification.builder().name("person").build())
                .creditorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                      .debtor(PartyIdentification.builder().name("person").build())
                      .debtorAccount(UKBankAccount.builder().name("act").sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).currency("GBP").build())
                      .product(CompositeReference.parse("customer.product"))
                .build();

    }

    public static PaymentBatch createExampleBatch()  {
        return createExampleBatch(PaymentBatch.Status.New);
    }

    @SneakyThrows
    public static PaymentBatch createExampleBatch(PaymentBatch.Status status)  {
        var file = new URL("http://myfile.csv");
        // Request to create a customer
        return PaymentBatch.builder()
                .payments(List.empty())
                .id("id")
                .file(file)
                .customerId("customer")
                .createdBy("test-user")
                .createdAt(Instant.now())
                .message("this is a test message")
                .name("mybatch")
                .status(status)
                .build();
    }

    public static CreatePaymentBatchCommand createPaymentBatchCreateCommand() throws MalformedURLException {
        return createPaymentBatchCreateCommand(createExampleBatch());
    }

    public static CreatePaymentBatchCommand createPaymentBatchCreateCommand(PaymentBatch batch) throws MalformedURLException {
        return CreatePaymentBatchCommand.builder()
                .batch(batch)
                .user("test-user")
                .stateVersion(STATE_VERSION)
                .build();
    }

    public static OpenPaymentBatchCommand createPaymentBatchOpenCommand(String batchId) {

        return OpenPaymentBatchCommand.builder()
                .batchId(batchId)
                .user("test-user")
                .stateVersion(STATE_VERSION)
                .build();
    }

    public static FailPaymentBatchCommand createPaymentBatchFailCommand(String batchId) {

        return FailPaymentBatchCommand.builder()
                .batchId(batchId)
                .user("test-user")
                .stateVersion(STATE_VERSION)
                .build();
    }

    public static ClosePaymentBatchCommand createPaymentBatchCloseCommand(String batchId) {

        return ClosePaymentBatchCommand.builder()
                .batchId(batchId)
                .user("test-user")
                .stateVersion(STATE_VERSION)
                .build();
    }

    public static AddPaymentToBatchCommand createAddToPaymentBatchCommand(Payment payment, String batchId) {

        return AddPaymentToBatchCommand.builder()
                .payment(payment)
                .user("test-user")
                .stateVersion(STATE_VERSION)
                .build();
    }

}
