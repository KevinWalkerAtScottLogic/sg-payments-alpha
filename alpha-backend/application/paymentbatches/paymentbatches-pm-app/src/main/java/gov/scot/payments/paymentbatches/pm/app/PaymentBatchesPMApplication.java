package gov.scot.payments.paymentbatches.pm.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.application.component.processmanager.EventTransformer;
import gov.scot.payments.application.component.processmanager.ProcessManagerApplication;
import gov.scot.payments.customers.model.event.CustomerPaymentCreatedEvent;
import gov.scot.payments.customers.model.event.PaymentFileParsingEndEvent;
import gov.scot.payments.customers.model.event.PaymentFileParsingFailedEvent;
import gov.scot.payments.customers.model.event.PaymentFileParsingStartEvent;
import gov.scot.payments.model.Command;
import gov.scot.payments.paymentbatches.model.aggregate.PaymentBatch;
import gov.scot.payments.paymentbatches.model.command.*;
import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.header.Headers;
import org.springframework.context.annotation.Bean;

import java.net.URI;
import java.net.URL;
import java.util.function.Predicate;

@ApplicationComponent
@Slf4j
public class PaymentBatchesPMApplication extends ProcessManagerApplication {

    /*
    @Bean
    public StreamJoiner<Event,Event,Event> joiner(SchemaRegistryClient confluentSchemaRegistryClient){
        HeaderPreservingCompositeNonNativeSerde<Event> eventSerde = valueSerde(Event.class);
        return StreamJoiner.<Event, Event, Event>builder()
            .repartitionTopicName()
            .joinWindow()
            .joiner()
            .rekeyFunction()
            .leftValueSerde(eventSerde)
            .rightValueSerde(eventSerde)
            .stateSerde(stateSerde(confluentSchemaRegistryClient,SerializedMessage.class))
            .build();
    }
     */

    @Bean
    public EventTransformer eventTransformFunction() {
        return EventTransformer.patternMatching(b ->
                b.match(PaymentFileParsingStartEvent.class, e -> createPaymentBatchCommand(e))
                        .match(PaymentFileParsingEndEvent.class, e -> createClosePaymentBatchCommand(e))
                        .match(PaymentFileParsingFailedEvent.class, e -> createFailPaymentBatchCommand(e))
                        .match(CustomerPaymentCreatedEvent.class, e -> createAddPaymentToBatchCommand(e))
                        .orElse(List.empty())
        );
    }

    private List<Command> createPaymentBatchCommand(PaymentFileParsingStartEvent e) {

        String newBatchId= createBatchId(e.getUri(), e.getCustomerPaymentBatchId());

        var newBatch = PaymentBatch.builder()
                .id(newBatchId)
                .customerId(e.getKey())
                .createdBy(e.getUser())
                .name(e.getCustomerPaymentBatchId())
                .build();

        var createCommand = CreatePaymentBatchCommand.builder()
                .batch(newBatch)
                .user(e.getKey())
                .build();

        var openCommand = OpenPaymentBatchCommand.builder()
                .batchId(newBatch.getId())
                .user(e.getKey())
                .build();

        log.info("Creating create/open commands {} {} , ", createCommand.getKey(), openCommand.getKey());

        return List.of(createCommand, openCommand);
    }

    private List<Command> createFailPaymentBatchCommand(PaymentFileParsingFailedEvent e) {

        var paymentBatchBuilder =  PaymentBatch.builder()
                .customerId(e.getKey())
                .createdBy(e.getUser())
                .name(getFileNameFromUri(e.getUri()))
                .id(e.getUri().toString())
                .message(e.getErrorMessage());

        var fileURl = getFileURL(e.getUri());
        if(!fileURl.isEmpty()){
            paymentBatchBuilder = paymentBatchBuilder.file(fileURl.get());
        }

        var newBatch = paymentBatchBuilder.build();
        var createCommand = CreatePaymentBatchCommand.builder()
                .batch(newBatch)
                .user(e.getKey())
                .build();

        var failCommand = FailPaymentBatchCommand.builder()
                .batchId(newBatch.getId())
                .user(e.getKey())
                .build();

        log.info("Creating create/fail commands {} {} , ", createCommand.getKey(), failCommand.getKey());

        return List.of(createCommand, failCommand);
    }

    private Option<URL> getFileURL(URI uri) {
        try{
            return Option.of(uri.toURL());
        }  catch (Exception e) {
            return Option.none();
        }
    }

    private List<Command> createClosePaymentBatchCommand(PaymentFileParsingEndEvent e) {

        String newBatchId= createBatchId(e.getUri(), e.getCustomerPaymentBatchId());

        var closeCommand = ClosePaymentBatchCommand.builder()
                .batchId(newBatchId)
                .user(e.getKey())
                .build();

        log.info("Creating close command {}", closeCommand.getKey());

        return List.of(closeCommand);
    }


    private List<Command> createAddPaymentToBatchCommand(CustomerPaymentCreatedEvent e)  {
        String newBatchId= createBatchId(e.getUri(), e.getCustomerPaymentBatchId());

        var addPayment = AddPaymentToBatchCommand.builder()
                .payment(e.getPayment().toBuilder().batchId(newBatchId).build())
                .user(e.getUser())
                .build();

        return List.of(addPayment);
    }

    private String createBatchId(URI uri, String batchId) {
        String[] uriParts = uri.toString().split("/");
        String fileName = uriParts[uriParts.length - 1];
        return fileName.equals(batchId) ? uri.toString() : String.format("%s%s",uri.toString(), batchId);
    }

    private String getFileNameFromUri(URI uri){
        String[] uriParts = uri.toString().split("/");
        String fileNameWithVersion = uriParts[uriParts.length - 1];
        String[] fileParts = fileNameWithVersion.split("#");
        return fileParts[0];
    }

    @Bean
    public Predicate<Headers> externalEventHeaderFilter(MessageTypeRegistry messageTypeRegistry){
        return h -> messageTypeRegistry.matchesType(h
                , PaymentFileParsingStartEvent.class
                ,PaymentFileParsingEndEvent.class
                ,PaymentFileParsingFailedEvent.class
                ,CustomerPaymentCreatedEvent.class);
    }

    public static void main(String[] args){
        BaseApplication.run(PaymentBatchesPMApplication.class,args);
    }
}




