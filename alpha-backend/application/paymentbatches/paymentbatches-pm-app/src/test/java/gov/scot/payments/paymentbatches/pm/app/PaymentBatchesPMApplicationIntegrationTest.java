package gov.scot.payments.paymentbatches.pm.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.avro.URIConversion;
import gov.scot.payments.customers.model.aggregate.Customer;
import gov.scot.payments.customers.model.aggregate.CustomerStatus;
import gov.scot.payments.customers.model.event.CustomerPaymentCreatedEvent;
import gov.scot.payments.customers.model.event.PaymentFileParsingEndEvent;
import gov.scot.payments.customers.model.event.PaymentFileParsingFailedEvent;
import gov.scot.payments.customers.model.event.PaymentFileParsingStartEvent;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.paymentbatches.model.command.*;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import org.apache.kafka.streams.KafkaStreams;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;

import java.net.MalformedURLException;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ApplicationIntegrationTest(classes = {PaymentBatchesPMApplicationIntegrationTest.TestApplication.class}
        , componentType = ApplicationIntegrationTest.ComponentType.PROCESS_MANAGER)
public class PaymentBatchesPMApplicationIntegrationTest {

    private String fileName = "file.csv";
    private String userName = "customerUser";
    private String customerName = "customerName";
    private String uriString = "/test/" + fileName;
    private String batchId = "13456";
    private String id = uriString + batchId;

    @BeforeEach
    public void setUp(EmbeddedBrokerClient brokerClient
            , @Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter
            , @Value("${commands.destination}") String commandTopic) {
        String retryTopic = commandTopic + "-retry-1";
        brokerClient.getBroker().addTopicsIfNotExists(commandTopic, retryTopic);
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(26));
    }

    @Test
    public void testFileStartedEvent(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException {

        var startEvent = PaymentFileParsingStartEvent.builder()
                .user(userName)
                .uri(URI.create(uriString))
                .carriedState(createCustomer(customerName))
                .productId("productId")
                .customerPaymentBatchId(batchId)
                .paymentIds(List.of())
                .correlationId(UUID.randomUUID())
                .stateVersion(Long.parseLong("456"))
                .build();

        brokerClient.sendToDestination("events-external", List.of(startEvent));

        var commands = brokerClient.readAllFromTopic(commandTopic, Command.class, Duration.ofSeconds(8));
        assertEquals(2, commands.size());

        assertEquals(CreatePaymentBatchCommand.class, commands.get(0).getClass());
        assertEquals(OpenPaymentBatchCommand.class, commands.get(1).getClass());

        var createCommand = (CreatePaymentBatchCommand) commands.get(0);
        var openCommand = (OpenPaymentBatchCommand) commands.get(1);

        assertEquals(customerName, createCommand.getUser());
        assertEquals(id, createCommand.getKey());
        assertEquals(id, createCommand.getBatch().getId());
        assertEquals(userName, createCommand.getBatch().getCreatedBy());
        assertEquals(id, openCommand.getBatchId());
        assertEquals(customerName, openCommand.getUser());

    }

    @Test
    public void testFileFailedEvent(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException {

        var endEvent = PaymentFileParsingEndEvent.builder()
                .uri(URI.create(uriString))
                .carriedState(createCustomer(customerName))
                .productId("productId")
                .correlationId(UUID.randomUUID())
                .customerPaymentBatchId(batchId)
                .user(customerName)
                .stateVersion(Long.parseLong("456"))
                .build();


        brokerClient.sendToDestination("events-external", List.of(endEvent));

        var commands = brokerClient.readAllFromTopic(commandTopic, ClosePaymentBatchCommand.class, Duration.ofSeconds(10));
        assertEquals(1, commands.size());

        var closePaymentBatchCommand = (ClosePaymentBatchCommand) commands.head();

        assertEquals(customerName, closePaymentBatchCommand.getUser());
        assertEquals(id, closePaymentBatchCommand.getBatchId());

    }

    @Test
    public void testCreatePaymentAddedEvent(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException {

        var payment = Payment.builder()
                .amount(Money.of(12.00, "GBP"))
                .batchId(id)
                .createdBy(userName)
                .allowedMethods(List.of(PaymentMethod.Bacs))
                .latestExecutionDate(Instant.now())
                .product(CompositeReference.parse("customer.product"))
                .creditorAccount(UKBankAccount.builder().sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).name("account name").currency("USD").build())
                .creditor(PartyIdentification.builder().name("id").build())
                .debtorAccount(UKBankAccount.builder().sortCode(SortCode.fromString("123456")).accountNumber(UKAccountNumber.fromString("12345678")).name("account name").currency("USD").build())
                .debtor(PartyIdentification.builder().name("id").build())
                .build();

        var customerObj = createCustomer(customerName);

        var addPayment = CustomerPaymentCreatedEvent.builder()
                .payment(payment)
                .carriedState(customerObj)
                .user(customerObj.getName())
                .stateVersion(1L)
                .correlationId(UUID.randomUUID())
                .productId("productid")
                .uri(URI.create(uriString))
                .customerPaymentBatchId(batchId)
                .build();

        brokerClient.sendToDestination("events-external", List.of(addPayment));

        var commands = brokerClient.readAllFromTopic(commandTopic, AddPaymentToBatchCommand.class, Duration.ofSeconds(3));
        assertEquals(1, commands.size());

        var addPaymentToBatchCommand = (AddPaymentToBatchCommand) commands.head();

        assertEquals(customerName, addPaymentToBatchCommand.getUser());
        assertEquals(payment, addPaymentToBatchCommand.getPayment());
        assertEquals(id, addPaymentToBatchCommand.getPayment().getBatchId());
    }

    @Test
    public void testFileFailedEndEvent(EmbeddedBrokerClient brokerClient, @Value("${commands.destination}") String commandTopic) throws MalformedURLException {

        String errorMsg = "Could not get payment for file";

        var failEvent = PaymentFileParsingFailedEvent.builder()
                .errorMessage(errorMsg)
                .carriedState(createCustomer(customerName))
                .uri(URI.create(uriString))
                .correlationId(UUID.randomUUID())
                .productId("productId")
                .user(customerName)
                .stateVersion(Long.parseLong("456"))
                .build();

        brokerClient.sendToDestination("events-external", List.of(failEvent));

        var commands = brokerClient.readAllFromTopic(commandTopic, Command.class, Duration.ofSeconds(10));
        assertEquals(2, commands.size());

        assertEquals(CreatePaymentBatchCommand.class, commands.get(0).getClass());
        assertEquals(FailPaymentBatchCommand.class, commands.get(1).getClass());

        var createCommand = (CreatePaymentBatchCommand) commands.get(0);
        var failCommand = (FailPaymentBatchCommand) commands.get(1);

        //failed events are keyed by uri only
        assertEquals(uriString, createCommand.getBatch().getId());
        assertEquals(createCommand.getBatch().getKey(), failCommand.getKey());
        assertEquals(customerName, failCommand.getUser());
        assertEquals(errorMsg, createCommand.getBatch().getMessage());
        assertEquals(fileName, createCommand.getBatch().getName());

    }

    private Customer createCustomer(String name) {
        return Customer.builder()
                .name(name)
                .status(CustomerStatus.Live)
                .createdAt(Instant.now())
                .createdBy("creatorname")
                .lastModifiedBy("modifiername")
                .products(HashSet.empty())
                .build();
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends PaymentBatchesPMApplication {

    }
}