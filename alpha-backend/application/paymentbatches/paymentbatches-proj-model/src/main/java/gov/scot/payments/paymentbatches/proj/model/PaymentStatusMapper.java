package gov.scot.payments.paymentbatches.proj.model;

import gov.scot.payments.payments.model.aggregate.PaymentStatus;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;

import static gov.scot.payments.payments.model.aggregate.PaymentStatus.*;

public class PaymentStatusMapper {

    private static final Set<PaymentStatus> errorStatus = HashSet.of(Invalid, Rejected, SubmissionFailed, Returned);
    private static final Set<PaymentStatus> pendingStatus = HashSet.of(ApprovalRequired, ReadyForSubmission);

    public static boolean isErrorStatus(PaymentStatus status){
        return errorStatus.contains(status);
    }
    public static boolean isPendingStatus(PaymentStatus status){
        return pendingStatus.contains(status);
    }

}
