SCRIPT_PATH=$(dirname "$0")
BASE_PATH="$SCRIPT_PATH"/../core

echo Enter a context name
read CONTEXT_NAME

CONTEXT_PATH="$SCRIPT_PATH"/"$CONTEXT_NAME"
if [ ! -d "$CONTEXT_PATH" ]; then
    echo Context does not exist, exiting
    exit
fi

echo Enter a component name
read COMPONENT_NAME

COMPONENT_PATH="$CONTEXT_PATH"/"$COMPONENT_NAME"-app
if [ -d "$COMPONENT_PATH" ]; then
    echo Component already exists, exiting
    exit
fi

echo Enter a component type "(processmanager,commandhandler,projector,temporal-projector)"
read COMPONENT_TYPE

if [ "$COMPONENT_TYPE" = "projector" ]; then
  ../gradlew :core:projector:cleanArch :core:projector:generate -Dgroup=gov.scot.payments."$CONTEXT_NAME" -Dname="$COMPONENT_NAME" -Dversion=1.0-SNAPSHOT -Dcom.orctom.gradle.archetype.binding.contextName="$CONTEXT_NAME"
  GENERATED_PATH="$BASE_PATH"/projector/generated
  cp -R "$GENERATED_PATH"/* "$CONTEXT_PATH"
  exit
elif [ "$COMPONENT_TYPE" = "temporal-projector" ]; then
  ../gradlew :core:temporal-projector:cleanArch :core:temporal-projector:generate -Dgroup=gov.scot.payments."$CONTEXT_NAME" -Dname="$COMPONENT_NAME" -Dversion=1.0-SNAPSHOT -Dcom.orctom.gradle.archetype.binding.contextName="$CONTEXT_NAME"
  GENERATED_PATH="$BASE_PATH"/temporal-projector/generated
  cp -R "$GENERATED_PATH"/* "$CONTEXT_PATH"
  exit
elif [ "$COMPONENT_TYPE" = "commandhandler" ]; then
  ../gradlew :core:command-handler:cleanArch :core:command-handler:generate -Dgroup=gov.scot.payments."$CONTEXT_NAME" -Dname="$COMPONENT_NAME" -Dversion=1.0-SNAPSHOT -Dcom.orctom.gradle.archetype.binding.contextName="$CONTEXT_NAME"
  GENERATED_PATH="$BASE_PATH"/command-handler/generated
  cp -R "$GENERATED_PATH"/* "$CONTEXT_PATH"
  exit
elif [ "$COMPONENT_TYPE" = "processmanager" ]; then
  ../gradlew :core:process-manager:cleanArch :core:process-manager:generate -Dgroup=gov.scot.payments."$CONTEXT_NAME" -Dname="$COMPONENT_NAME" -Dversion=1.0-SNAPSHOT -Dcom.orctom.gradle.archetype.binding.contextName="$CONTEXT_NAME"
  GENERATED_PATH="$BASE_PATH"/process-manager/generated
  cp -R "$GENERATED_PATH"/* "$CONTEXT_PATH"
  exit
else
    echo Type not recognized, exiting
    exit
fi