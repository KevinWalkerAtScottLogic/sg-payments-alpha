package gov.scot.payments.model;

import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
public abstract class EventImpl extends MessageImpl implements Event {

    public EventImpl(){

    }

    public EventImpl(UUID messageId, Instant timestamp) {
        super(messageId, timestamp);
    }
}
