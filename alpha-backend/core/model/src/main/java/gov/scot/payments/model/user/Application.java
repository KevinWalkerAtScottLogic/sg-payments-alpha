package gov.scot.payments.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.apache.avro.reflect.Nullable;
import org.springframework.security.core.GrantedAuthority;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Builder(toBuilder = true)
public class Application implements Subject {

    @EqualsAndHashCode.Include @NonNull private String name;
    private String clientId;
    @Nullable private String clientSecret;
    @NonNull private Set<Role> roles;
    @Nullable private String sshPublicKey;

    @JsonIgnore
    @Override
    public List<? extends GrantedAuthority> getAuthorities() {
        return roles.flatMap(Role::toAuthorities).toList();
    }

}
