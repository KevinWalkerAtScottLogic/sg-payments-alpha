package gov.scot.payments.model;

import java.util.UUID;

public interface HasCause {

    String CORRELATION_ID_HEADER = "correlationId";
    String EXECUTION_COUNT_HEADER = "executionCount";

    UUID getCorrelationId();
    int getExecutionCount();
    boolean isReply();

    void setCauseDetails(Command command);
}
