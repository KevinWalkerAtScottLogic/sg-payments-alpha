package gov.scot.payments.model;

import java.time.Instant;
import java.util.UUID;

public interface Message {

    String CONTEXT_HEADER = "sourceContext";
    String TYPE_HEADER = "messageType";

    UUID getMessageId();
    Instant getTimestamp();
}
