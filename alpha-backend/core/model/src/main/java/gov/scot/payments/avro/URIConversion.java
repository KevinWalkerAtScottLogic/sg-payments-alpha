package gov.scot.payments.avro;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.Conversion;
import org.apache.avro.LogicalType;
import org.apache.avro.Schema;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

public class URIConversion extends Conversion<URI> {
    @Override
    public Class<URI> getConvertedType() {
        return URI.class;
    }

    @Override
    public String getLogicalTypeName() {
        return "uri";
    }

    @Override
    public Schema getRecommendedSchema() {
        return  new LogicalType("uri").addToSchema(Schema.create(Schema.Type.STRING));
    }

    @Override
    public URI fromCharSequence(CharSequence value, Schema schema, LogicalType type) {

        try {
            return URI.create(value.toString());
        } catch (RuntimeException e) {
            throw new AvroRuntimeException("Invalid URI: " + value);
        }
    }

    @Override
    public CharSequence toCharSequence(URI value, Schema schema, LogicalType type) {
        return value.toString();
    }
}
