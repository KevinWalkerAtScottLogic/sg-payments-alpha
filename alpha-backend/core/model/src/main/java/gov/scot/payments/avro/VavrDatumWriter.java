package gov.scot.payments.avro;

import io.vavr.Tuple2;
import io.vavr.collection.Map;
import io.vavr.collection.Traversable;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.apache.avro.reflect.MapEntry;
import org.apache.avro.reflect.ReflectData;
import org.apache.avro.reflect.ReflectDatumWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class VavrDatumWriter<T> extends ReflectDatumWriter<T> {

    public VavrDatumWriter() {
        this(VavrReflectData.get());
    }

    public VavrDatumWriter(Class<T> c) {
        this(c, VavrReflectData.get());
    }

    public VavrDatumWriter(Class<T> c, VavrReflectData data) {
        this(data.getSchema(c), data);
    }

    public VavrDatumWriter(Schema root) {
        this(root, VavrReflectData.get());
    }

    public VavrDatumWriter(final Schema schema, final VavrReflectData vavrReflectData) {
        super(schema,vavrReflectData);
    }

    protected VavrDatumWriter(VavrReflectData reflectData) {
        super(reflectData);
    }

    @Override
    protected void write(final Schema schema, final Object datum, final Encoder out) throws IOException {
        Object datumToUse = datum;
        if (datumToUse instanceof Map) {
            datumToUse = ((Map)datumToUse).toJavaMap();
        } else if (datumToUse instanceof Traversable) {
            datumToUse = ((Traversable)datumToUse).toJavaList();
        }
        super.write(schema, datumToUse, out);
    }

    @Override
    protected long getArraySize(final Object array) {
        if(array instanceof Traversable){
            return ((Traversable)array).size();
        }
        return super.getArraySize(array);
    }

    @Override
    protected Iterator<?> getArrayElements(final Object array) {
        if(array instanceof Traversable){
            return ((Traversable)array).iterator();
        }
        return super.getArrayElements(array);
    }

    @Override
    protected int getMapSize(final Object map) {
        if(map instanceof Map){
            return ((Map)map).size();
        }
        return super.getMapSize(map);
    }

    @Override
    protected Iterable<java.util.Map.Entry<Object, Object>> getMapEntries(final Object map) {
        if(map instanceof Map){
            return ((Traversable<Tuple2<Object,Object>>)map).map(t -> t.toEntry());
        }
        return super.getMapEntries(map);
    }
}
