package gov.scot.payments.model;

public interface Command extends Message {

    boolean isReply();
    int getExecutionCount();

}
