package gov.scot.payments.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.UUID;

@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public abstract class CommandImpl extends MessageImpl implements Command{

    @Getter
    private boolean reply;

    @Getter
    private int executionCount;

    public CommandImpl(){
        this.reply = true;
        this.executionCount = 0;
    }

    public CommandImpl(boolean reply){
        this.reply = reply;
        this.executionCount = 0;
    }

    public CommandImpl(boolean reply, int executionCount){
        this.reply = reply;
        this.executionCount = executionCount;
    }

    public CommandImpl(UUID messageId, Instant timestamp, boolean reply, int executionCount) {
        super(messageId, timestamp);
        this.reply = reply;
        this.executionCount = executionCount;
    }
}
