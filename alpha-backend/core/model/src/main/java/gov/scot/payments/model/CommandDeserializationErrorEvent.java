package gov.scot.payments.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "*", type = "deserializationError")
public class CommandDeserializationErrorEvent extends GenericErrorEvent{

    @NonNull private byte[] cause;

    public CommandDeserializationErrorEvent(){}
}
