package gov.scot.payments.model;

import java.time.Instant;

public interface TemporalProjection<K,PK> extends Projection<PK> {

    K getLogicalId();
    Instant getEventTime();

}
