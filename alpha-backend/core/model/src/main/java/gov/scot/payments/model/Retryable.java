package gov.scot.payments.model;

public interface Retryable {

    int getRetryCount();
}
