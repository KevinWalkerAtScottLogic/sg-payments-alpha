package gov.scot.payments.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.UUID;

@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@SuperBuilder
public abstract class MessageImpl implements Message{

    @EqualsAndHashCode.Include @NonNull
    @Builder.Default private UUID messageId = UUID.randomUUID();
    @Builder.Default @NonNull private Instant timestamp = Instant.now();

    public MessageImpl(){
        messageId = UUID.randomUUID();
        timestamp = Instant.now();
    }

}
