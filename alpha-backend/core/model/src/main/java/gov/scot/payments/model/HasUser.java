package gov.scot.payments.model;

import gov.scot.payments.model.user.Role;
import io.vavr.collection.Set;


public interface HasUser {

    String getUser();
    Set<Role> getRoles();
}
