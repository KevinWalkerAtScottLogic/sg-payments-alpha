package gov.scot.payments.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.apache.avro.reflect.Nullable;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Scope {

    public static final Scope GLOBAL_SCOPE = new Scope(".");

    @NonNull private String name;
    @Nullable private Scope parent;

    public Scope(String name){
        this(name,null);
    }

    @JsonIgnore
    public String getQualifiedName() {
        return toString();
    }

    @Override
    public String toString(){
        if(parent == null){
            return name;
        }
        return String.format("%s.%s",parent.toString(),name);
    }

    public static Scope parse(String s){
        if(".".equals(s)){
            return GLOBAL_SCOPE;
        }
        String[] components = s.split("\\.");
        if(components.length == 0){
            return GLOBAL_SCOPE;
        }
        Scope current = new Scope(components[0]);
        for(int i=1;i<components.length;i++){
            current = new Scope(components[i],current);
        }
        return current;
    }

    // a scope matches if it is equal
    // or if it has less depth that the provided one, and matches up to that depth
    public boolean matches(Scope scope) {
        if(this.equals(GLOBAL_SCOPE) || this.equals(scope)){
            return true;
        }
        int thisDepth = getDepth();
        int otherDepth = scope.getDepth();
        if(thisDepth < otherDepth){
            return matches(scope.superScope(thisDepth));
        }
        return false;
    }

    @JsonIgnore
    public Scope getRoot() {
        if(this.getParent() == null){
            return this;
        }
        return this.getParent().getRoot();
    }

    private Scope superScope(int requestedDepth) {
        int toRemove = getDepth() - requestedDepth;
        Scope scope = this;
        for(int i=0;i<toRemove;i++){
            scope = scope.parent;
        }
        return scope;
    }

    @JsonIgnore
    private int getDepth() {
        if(parent == null){
            return 1;
        }
        return parent.getDepth() + 1;
    }

}
