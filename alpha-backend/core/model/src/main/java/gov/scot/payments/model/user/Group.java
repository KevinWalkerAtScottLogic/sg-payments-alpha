package gov.scot.payments.model.user;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;

@Getter
@RequiredArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Group {

    @NonNull private String name;
}
