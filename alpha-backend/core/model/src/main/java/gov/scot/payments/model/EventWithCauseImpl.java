package gov.scot.payments.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@Getter
public abstract class EventWithCauseImpl extends EventImpl implements HasCause{

    private UUID correlationId;
    private int executionCount;
    private boolean reply;

    public EventWithCauseImpl(){
        this(UUID.randomUUID(),0,true);
    }

    public EventWithCauseImpl(final UUID correlationId
            , final int executionCount
            , boolean reply){
        this.correlationId = correlationId;
        this.executionCount = executionCount;
        this.reply = reply;
    }

    public EventWithCauseImpl(UUID messageId
            , Instant timestamp
            , final UUID correlationId
            , final int executionCount
            , boolean reply) {
        super(messageId, timestamp);
        this.correlationId = correlationId;
        this.executionCount = executionCount;
        this.reply = reply;
    }

    @Override
    public void setCauseDetails(final Command command) {
        this.reply = command.isReply();
        this.executionCount = command.getExecutionCount() + 1;
        this.correlationId = command.getMessageId();
    }
}
