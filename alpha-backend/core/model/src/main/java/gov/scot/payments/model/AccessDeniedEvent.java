package gov.scot.payments.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@SuperBuilder
@MessageType(context = "*", type = "accessDenied")
public class AccessDeniedEvent extends EventWithCauseImpl {

    private String reason;

    public AccessDeniedEvent(){}

    @JsonCreator
    public AccessDeniedEvent(@JsonProperty("messageId") final UUID messageId
            , @JsonProperty("timestamp") final Instant timestamp
            , @JsonProperty("correlationId") final UUID correlationId
            , @JsonProperty("executionCount") final int executionCount
            , @JsonProperty("reply") final boolean reply
            , @NonNull @JsonProperty("reason") final String reason) {
        super(messageId, timestamp,correlationId,executionCount,reply);
        this.reason = reason;
    }

    public static AccessDeniedEvent from(final Command command, String reason) {
        return AccessDeniedEvent.builder()
                                .correlationId(command.getMessageId())
                                .executionCount(command.getExecutionCount())
                                .reply(command.isReply())
                                .reason(reason)
                                .build();
    }

}
