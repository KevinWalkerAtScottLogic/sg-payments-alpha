package gov.scot.payments.model;

public interface HasStateVersion {

    Long getStateVersion();
}
