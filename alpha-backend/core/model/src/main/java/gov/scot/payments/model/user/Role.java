package gov.scot.payments.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Getter
@RequiredArgsConstructor
@Builder
@NoArgsConstructor
@EqualsAndHashCode
public class Role {

    private static final Pattern ROLE = Pattern.compile("(.+)/(.+):(.+)");

    @NonNull private Set<Action> actions;
    @NonNull private Scope scope;

    public Role(Action action, Scope scope){
        this(HashSet.of(action),scope);
    }

    public Set<? extends GrantedAuthority> toAuthorities() {
        return actions.map(Action::toAuthority);
    }

    @Override
    public String toString(){
        return String.format("%s/%s",scope.toString(),actions.map(Action::toString).collect(Collectors.joining(",")));
    }

    public static Role parse(String s){
        Matcher matcher = ROLE.matcher(s);
        if(!matcher.matches()){
            throw new IllegalArgumentException("Roles must match pattern (scope)/(resource):(verb)");
        }
        return new Role(new Action(matcher.group(2),matcher.group(3)),Scope.parse(matcher.group(1)));
    }

    @JsonIgnore
    public boolean hasPermissionForResource(String resource){
        return actions.filter(a -> a.getResource().equals(resource)).size() > 0;
    }

    @JsonIgnore
    public boolean hasAction(Action action){
        return actions.contains(action);
    }

    public boolean matches(Role role) {
        return actions.containsAll(role.actions) && scope.matches(role.scope);
    }


}
