package gov.scot.payments.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.apache.avro.reflect.Nullable;
import org.apache.commons.lang3.exception.ExceptionUtils;

@Getter
@Builder
@EqualsAndHashCode
@ToString
public class CommandFailureInfo {

    private String errorType;
    @Nullable private String errorMessage;
    @Nullable private String errorDetail;

    public CommandFailureInfo(){}

    @JsonCreator
    public CommandFailureInfo(@JsonProperty("errorType") String errorType
            , @JsonProperty("errorMessage") String errorMessage
            , @JsonProperty("errorDetail") String errorDetail) {
        this.errorType = errorType;
        this.errorMessage = errorMessage;
        this.errorDetail = errorDetail;
    }

    public static CommandFailureInfo from(Throwable e){
        return CommandFailureInfo.builder()
                                 .errorDetail(ExceptionUtils.getStackTrace(e))
                                 .errorType(e.getClass().getSimpleName())
                                 .errorMessage(e.getMessage())
                                 .build();
    }

}
