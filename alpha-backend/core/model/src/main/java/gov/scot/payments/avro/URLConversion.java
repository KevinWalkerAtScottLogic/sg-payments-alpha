package gov.scot.payments.avro;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.Conversion;
import org.apache.avro.LogicalType;
import org.apache.avro.Schema;

import java.net.MalformedURLException;
import java.net.URL;

public class URLConversion extends Conversion<URL> {
    @Override
    public Class<URL> getConvertedType() {
        return URL.class;
    }

    @Override
    public String getLogicalTypeName() {
        return "url";
    }

    @Override
    public Schema getRecommendedSchema() {
        return  new LogicalType("url").addToSchema(Schema.create(Schema.Type.STRING));
    }

    @Override
    public URL fromCharSequence(CharSequence value, Schema schema, LogicalType type) {

        try {
            return new URL(value.toString());
        } catch (MalformedURLException e) {
            throw new AvroRuntimeException("Invalid URL: " + value);
        }
    }

    @Override
    public CharSequence toCharSequence(URL value, Schema schema, LogicalType type) {
        return value.toString();
    }
}
