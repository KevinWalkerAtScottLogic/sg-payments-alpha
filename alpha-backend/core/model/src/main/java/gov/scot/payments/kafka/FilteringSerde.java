package gov.scot.payments.kafka;

import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertySource;

import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class FilteringSerde extends Serdes.WrapperSerde<byte[]>{

    public static final byte[] FILTERED_PLACEHOLDER = new byte[0];

    public FilteringSerde(BeanFactory beanFactory,final Serializer<byte[]> serializer) {
        super(serializer, new FilteringDeserializer(beanFactory));
    }

    @RequiredArgsConstructor
    private static class FilteringDeserializer extends ByteArrayDeserializer {

        private static final String FILTER_BEAN_CONFIG = "header-filter-bean";

        private Predicate<Headers> filter;
        private final BeanFactory beanFactory;

        @SuppressWarnings("unchecked")
        @Override
        public void configure(final Map<String, ?> configs, final boolean isKey) {

            super.configure(configs, isKey);
            String beanName = (String)configs.get(FILTER_BEAN_CONFIG);
            log.info("Configuring filtering with bean {}",beanName);
            filter = getFilter(beanName,null);
        }

        @Override
        public byte[] deserialize(final String topic, final Headers headers, final byte[] data) {
            if(filter == null){
                String beanName = lookupFilterBean(topic);
                filter = getFilter(beanName,topic);
            }
            if( !filter.test(headers)){
                log.debug("Skipping message on {} with headers {}",topic,headers);
                return FILTERED_PLACEHOLDER;
            }
            return super.deserialize(topic, headers, data);
        }

        private String lookupFilterBean(String topic) {
            log.info("Looking up filter bean for topic: {}",topic);
            ConfigurableEnvironment environment = beanFactory.getBean(ConfigurableEnvironment.class);
            String destination = getDestination(environment,topic);
            log.info("Destination: {} matches topic: {}",destination, topic);
            String filterProperty = String.format("spring.cloud.stream.bindings.%s.consumer.configuration.header-filter-bean",destination);
            String filterBean = environment.getProperty(filterProperty);
            log.info("Filter bean property {} -> {}",filterProperty,filterBean);
            return filterBean;
        }

        private String getDestination(ConfigurableEnvironment environment, String topic){
            Pattern regex = Pattern.compile("spring\\.cloud\\.stream\\.bindings\\.(.+)\\.destination");
            for (final PropertySource<?> propertySource : environment.getPropertySources()) {
                if (!(propertySource instanceof EnumerablePropertySource))
                    continue;
                for (final String name : ((EnumerablePropertySource<?>) propertySource).getPropertyNames()){
                    Matcher matcher = regex.matcher(name);
                    if(matcher.matches()){
                        String value = environment.getProperty(name,String.class);
                        log.info("Found binding for destination: {}, topic : {}",name,value);
                        if(topic.equals(value)){
                            return matcher.group(1);
                        }
                    }
                }
            }
            return null;
        }

        private Predicate getFilter(String beanName, String topic) {
            if(beanName == null){
                log.info("No filter bean found for {}, not applying filtering",topic);
                return h -> true;
            }
            return Try.of(() -> {
                log.info("Found filter bean {} for {}",beanName,topic);
                return beanFactory.getBean(beanName, Predicate.class);
            }).getOrElse(() -> {
                log.warn("Error retrieving filter bean {} for {} not applying filtering",beanName,topic);
                return h -> true;
            });
        }

    }
}
