package gov.scot.payments.model;

public interface HasKey<K> {

    String PARTITION_KEY_HEADER = "partitionKey";

    K getKey();
}
