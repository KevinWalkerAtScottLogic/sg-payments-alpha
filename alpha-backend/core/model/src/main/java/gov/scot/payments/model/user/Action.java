package gov.scot.payments.model.user;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@Getter
@RequiredArgsConstructor
@Builder
@NoArgsConstructor
@EqualsAndHashCode
public class Action {

    @NonNull private String resource;
    @NonNull private String verb;

    @Override
    public String toString(){
        return String.format("%s:%s",resource,verb);
    }

    public static Action parse(String s){
        String[] components = s.split(":");
        return new Action(components[0],components[1]);
    }

    public GrantedAuthority toAuthority() {
        return new SimpleGrantedAuthority(toString());
    }
}
