package gov.scot.payments.avro;

import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;
import io.vavr.collection.Traversable;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.AvroTypeException;
import org.apache.avro.Conversions;
import org.apache.avro.Schema;
import org.apache.avro.SchemaNormalization;
import org.apache.avro.data.TimeConversions;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.reflect.ReflectData;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.util.ClassUtils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;

public class VavrReflectData extends ReflectData {

    private static final VavrReflectData INSTANCE = new VavrReflectData();
    static{
        INSTANCE.addLogicalTypeConversion(new Conversions.UUIDConversion());
        INSTANCE.addLogicalTypeConversion(new TimeConversions.TimestampMillisConversion());
        INSTANCE.addLogicalTypeConversion(new TimeConversions.DateConversion());
        INSTANCE.addLogicalTypeConversion(new TimeConversions.TimeMillisConversion());
        INSTANCE.addLogicalTypeConversion(new CurrencyUnitConversion());
        INSTANCE.addLogicalTypeConversion(new MoneyConversion());
        INSTANCE.addLogicalTypeConversion(new URLConversion());
        INSTANCE.addLogicalTypeConversion(new URIConversion());
        INSTANCE.addLogicalTypeConversion(new BigDecimalConversion());
    }

    private static final String NS_MAP_ARRAY_RECORD = // record name prefix
            "org.apache.avro.reflect.Pair";
    private static final String NS_MAP_KEY = "key"; // name of key field
    public static final int NS_MAP_KEY_INDEX = 0;
    private static final String NS_MAP_VALUE = "value"; // name of value field
    public static final int NS_MAP_VALUE_INDEX = 1;
    private static final java.util.Map<String, Class> CLASS_CACHE = new ConcurrentHashMap<>();

    public static VavrReflectData get() {
        return INSTANCE;
    }

    public static Class getClassProp(Schema schema, String prop) {
        String name = schema.getProp(prop);
        if (name == null)
            return null;
        Class c = CLASS_CACHE.get(name);
        if (c != null)
            return c;
        try {
            c = ClassUtils.forName(name);
            CLASS_CACHE.put(name, c);
        } catch (ClassNotFoundException e) {
            throw new AvroRuntimeException(e);
        }
        return c;
    }

    public VavrReflectData() {
        super();
        stringableClasses = new HashSet<>();
        {
            stringableClasses.add(java.math.BigDecimal.class);
            stringableClasses.add(java.math.BigInteger.class);
            stringableClasses.add(java.io.File.class);
        }
    }

    public VavrReflectData(ClassLoader classLoader) {
        super(classLoader);
        stringableClasses = new HashSet<>();
        {
            stringableClasses.add(java.math.BigDecimal.class);
            stringableClasses.add(java.math.BigInteger.class);
            stringableClasses.add(java.io.File.class);
        }
    }

    @Override
    protected Schema createSchema(final Type type, final java.util.Map<String, Schema> names) {
        if (type instanceof ParameterizedType) {
            ParameterizedType ptype = (ParameterizedType) type;
            Class raw = (Class) ptype.getRawType();
            Type[] params = ptype.getActualTypeArguments();
            if (Map.class.isAssignableFrom(raw)) { // Map
                Class key = (Class) params[0];
                if (isStringable(key)) { // Stringable key
                    Schema schema = Schema.createMap(createSchema(params[1], names));
                    schema.addProp(SpecificData.KEY_CLASS_PROP, key.getName());
                    return schema;
                } else if (key != String.class) {
                    Schema schema = createNonStringMapSchema(params[0], params[1], names);
                    schema.addProp(SpecificData.CLASS_PROP, raw.getName());
                    return schema;
                }
            } else if (Traversable.class.isAssignableFrom(raw)) { // Collection
                if (params.length != 1)
                    throw new AvroTypeException("No array type specified.");
                Schema schema = Schema.createArray(createSchema(params[0], names));
                schema.addProp(SpecificData.CLASS_PROP, raw.getName());
                return schema;
            }
        }
        return super.createSchema(type, names);
    }

    @Override
    public DatumReader createDatumReader(final Schema schema) {
        return new VavrDatumReader(schema, schema, this);
    }

    @Override
    public DatumReader createDatumReader(final Schema writer, final Schema reader) {
        return new VavrDatumReader(writer, reader, this);
    }

    @Override
    public DatumWriter createDatumWriter(final Schema schema) {
        return new VavrDatumWriter(schema, this);
    }

    @Override
    protected Collection getArrayAsCollection(final Object datum) {
        if(datum instanceof Map){
            return ((Map) datum).toJavaMap().entrySet();
        }
        if( datum instanceof Traversable ){
            return ((Traversable)datum).toJavaList();
        }
        return super.getArrayAsCollection(datum);
    }

    @Override
    protected boolean isRecord(final Object datum) {
        if (datum == null)
            return false;
        if (datum instanceof Traversable)
            return false;
        return super.isRecord(datum);
    }

    @Override
    protected boolean isArray(final Object datum) {
        if (datum == null)
            return false;
        if( ( (datum instanceof Set) || (datum instanceof Seq)) || isNonStringMap(datum)){
            return true;
        }
        return super.isArray(datum);
    }

    @Override
    protected boolean isMap(final Object datum) {
        if((datum instanceof Map) && !isNonStringMap(datum)){
            return true;
        }
        return super.isMap(datum);
    }

    private boolean isNonStringMap(Object datum) {
        if (datum instanceof Map) {
            Map m = (Map) datum;
            if (m.size() > 0) {
                Class keyClass = m.keySet().iterator().next().getClass();
                return !isStringable(keyClass) && !isStringType(keyClass);
            }
        }
        return false;
    }

    Schema createNonStringMapSchema(Type keyType, Type valueType, java.util.Map<String, Schema> names) {
        Schema keySchema = createSchema(keyType, names);
        Schema valueSchema = createSchema(valueType, names);
        Schema.Field keyField = new Schema.Field(NS_MAP_KEY, keySchema, null, null);
        Schema.Field valueField = new Schema.Field(NS_MAP_VALUE, valueSchema, null, null);
        String name = getNameForNonStringMapRecord(keyType, valueType, keySchema, valueSchema);
        Schema elementSchema = Schema.createRecord(name, null, null, false);
        elementSchema.setFields(Arrays.asList(keyField, valueField));
        Schema arraySchema = Schema.createArray(elementSchema);
        return arraySchema;
    }

    private String getNameForNonStringMapRecord(Type keyType, Type valueType, Schema keySchema, Schema valueSchema) {

        // Generate a nice name for classes in java* package
        if (keyType instanceof Class && valueType instanceof Class) {

            Class keyClass = (Class) keyType;
            Class valueClass = (Class) valueType;
            Package pkg1 = keyClass.getPackage();
            Package pkg2 = valueClass.getPackage();

            if (pkg1 != null && pkg1.getName().startsWith("java") && pkg2 != null && pkg2.getName().startsWith("java")) {
                return NS_MAP_ARRAY_RECORD + keyClass.getSimpleName() + valueClass.getSimpleName();
            }
        }

        String name = keySchema.getFullName() + valueSchema.getFullName();
        long fingerprint = SchemaNormalization.fingerprint64(name.getBytes(StandardCharsets.UTF_8));

        if (fingerprint < 0)
            fingerprint = -fingerprint; // ignore sign
        String fpString = Long.toString(fingerprint, 16); // hex
        return NS_MAP_ARRAY_RECORD + fpString;
    }

    public static boolean isNonStringMapSchema(Schema s) {
        if (s != null && s.getType() == Schema.Type.ARRAY) {
            Class c = getClassProp(s, SpecificData.CLASS_PROP);
            return c != null && Map.class.isAssignableFrom(c);
        }
        return false;
    }
}
