package gov.scot.payments.avro;

import io.vavr.collection.HashMap;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.Conversion;
import org.apache.avro.LogicalType;
import org.apache.avro.Schema;
import org.apache.avro.generic.IndexedRecord;
import org.apache.avro.io.ResolvingDecoder;
import org.apache.avro.reflect.ReflectData;
import org.apache.avro.reflect.ReflectDatumReader;
import org.apache.avro.specific.SpecificData;

import java.io.IOException;

public class VavrDatumReader<T> extends ReflectDatumReader<T> {

    public VavrDatumReader() {
        this(null, null, VavrReflectData.get());
    }

    public VavrDatumReader(final Class<T> c) {
        this(VavrReflectData.get());
        setSchema(getSpecificData().getSchema(c));
    }

    public VavrDatumReader(VavrReflectData data) {
        super(data);
    }

    public VavrDatumReader(Schema root) {
        this(root, root, VavrReflectData.get());
    }

    public VavrDatumReader(Schema writer, Schema reader) {
        this(writer, reader, VavrReflectData.get());
    }

    public VavrDatumReader(final Schema schema, final Schema schema1, final VavrReflectData vavrReflectData) {
        super(schema,schema1,vavrReflectData);
    }

    @Override
    protected Object newArray(final Object old, final int size, final Schema schema) {
        Class<?> collectionClass = VavrReflectData.getClassProp(schema, SpecificData.CLASS_PROP);
        if (collectionClass != null && !collectionClass.isArray()) {
            if (collectionClass.isAssignableFrom(List.class))
                return List.of();
            if (collectionClass.isAssignableFrom(Set.class))
                return HashSet.of();
            if (collectionClass.isAssignableFrom(Map.class))
                return HashMap.empty();
        }
        return super.newArray(old, size, schema);
    }

    @Override
    protected Object readArray(final Object old, final Schema expected, final ResolvingDecoder in) throws IOException {
        Schema expectedType = expected.getElementType();
        Object array = newArray(old, 0, expected);
        if (array instanceof List) {
            @SuppressWarnings("unchecked")
            List<Object> c = (List<Object>) array;
            return readVavrCollection(c, expectedType, in.readArrayStart(), in);
        } else if (array instanceof Set) {
            @SuppressWarnings("unchecked")
            Set<Object> c = (Set<Object>) array;
            return readVavrCollection(c.toList(), expectedType, in.readArrayStart(), in).toSet();
        } else if (array instanceof Map) {
            // Only for non-string keys, we can use NS_MAP_* fields
            // So we check the samee explicitly here
            if (VavrReflectData.isNonStringMapSchema(expected)) {
                List<Object> c = readVavrCollection(List.of(), expectedType, in.readArrayStart(), in);
                Map m = (Map) array;
                for (Object ele : c) {
                    IndexedRecord rec = ((IndexedRecord) ele);
                    Object key = rec.get(VavrReflectData.NS_MAP_KEY_INDEX);
                    Object value = rec.get(VavrReflectData.NS_MAP_VALUE_INDEX);
                    m = m.put(key, value);
                }
                return m;
            } else {
                String msg = "Expected a schema of map with non-string keys but got " + expected;
                throw new AvroRuntimeException(msg);
            }
        } else {
            return super.readArray(old, expected, in);
        }
    }

    private List<Object> readVavrCollection(final List<Object> list, final Schema expectedType, long l, final ResolvingDecoder in) throws IOException {
        if(l <= 0){
            return list;
        }
        List<Object> toReturn = list;
        LogicalType logicalType = expectedType.getLogicalType();
        Conversion<?> conversion = getData().getConversionFor(logicalType);
        if (logicalType != null && conversion != null) {
            do {
                for (int i = 0; i < l; i++) {
                    Object element = readWithConversion(null, expectedType, logicalType, conversion, in);
                    toReturn = toReturn.append(element);
                }
            } while ((l = in.arrayNext()) > 0);
        } else {
            do {
                for (int i = 0; i < l; i++) {
                    Object element = readWithoutConversion(null, expectedType, in);
                    toReturn = toReturn.append(element);
                }
            } while ((l = in.arrayNext()) > 0);
        }
        return toReturn;
    }
}
