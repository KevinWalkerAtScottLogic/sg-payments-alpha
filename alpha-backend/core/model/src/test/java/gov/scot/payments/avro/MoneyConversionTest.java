package gov.scot.payments.avro;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.LogicalType;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static gov.scot.payments.avro.MoneyConversion.MONEY_LOGICAL_TYPE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static uk.co.datumedge.hamcrest.json.SameJSONAs.sameJSONAs;

class MoneyConversionTest {

    public static final LogicalType RECORD_TYPE = new LogicalType(MONEY_LOGICAL_TYPE);

    @Test
    @DisplayName("Given a valid money string, then fromCharSequence creates a money")
    void givenValidMoneyStringThenFromCharSequenceCreatesMoney() {

        String moneyString = "GBP 1.00";

        Money money = new MoneyConversion().fromCharSequence(moneyString, null, RECORD_TYPE);

        assertThat(moneyString, startsWith(money.toString()));
    }

    @Test
    @DisplayName("Given an invalid money string, then fromCharSequence throws an AvroRuntimeException")
    void givenInvalidMoneyStringThenFromCharSequenceThrowsAvroRuntimeException() {

        String moneyString = "GBP1111sdf 1.00";

        var exception = assertThrows(AvroRuntimeException.class,
                () -> new MoneyConversion().fromCharSequence(moneyString, null, RECORD_TYPE));

        assertThat(exception.getMessage(), is("Invalid Money: " + moneyString));
    }

    @Test
    @DisplayName("Given a money, then toCharSequence returns a String")
    void toCharSequence()  {

        String expectedMoneyString = "GBP 1.00";
        Money money = Money.parse(expectedMoneyString);

        CharSequence moneyString = new MoneyConversion().toCharSequence(money, null, RECORD_TYPE);

        assertThat(expectedMoneyString, startsWith(money.toString()));
    }
}
