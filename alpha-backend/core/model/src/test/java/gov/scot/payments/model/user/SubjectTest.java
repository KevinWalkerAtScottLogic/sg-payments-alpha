package gov.scot.payments.model.user;

import io.vavr.collection.HashSet;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SubjectTest {

    @Test
    void hasAction() {

        Subject subject = User.builder()
                .name("test-name")
                .email("test-email")
                .groups(HashSet.of(new Group("testGroup")))
                .roles(HashSet.of(Role.parse("customer.product/resource:Read")))
                .build();

        assertTrue(subject.hasAction(Action.parse("resource:Read")));
        assertFalse(subject.hasAction(Action.parse("resource:Write")));
    }

    @Test
    void hasAccess() {

        Subject subject = User.builder()
                .name("test-name")
                .email("test-email")
                .groups(HashSet.of(new Group("testGroup")))
                .roles(HashSet.of(Role.parse("customer.product/resource:Read")))
                .build();

        assertTrue(subject.hasAccess(Role.parse("customer.product/resource:Read")));
    }

    @Test
    void getRootScopes() {

        Subject subject = User.builder()
                .name("test-name")
                .email("test-email")
                .groups(HashSet.of(new Group("customer.product")))
                .roles(HashSet.of(Role.parse("customer.product/resource:Read")))
                .build();

        assertEquals("", subject.getRootScopesAsString());

        Subject subjectRoot = User.builder()
                .name("test-name")
                .email("test-email")
                .groups(HashSet.of(new Group("customer")))
                .roles(HashSet.of(Role.parse("customer/resource:Read")))
                .build();

        assertEquals("customer", subjectRoot.getRootScopesAsString());
    }

    @Test
    void hasGlobalAccess() {

        Subject subject = User.builder()
                .name("test-name")
                .email("test-email")
                .groups(HashSet.of(new Group(".")))
                .roles(HashSet.of(Role.parse("./resource:Read")))
                .build();

        assertTrue(subject.hasGlobalAccess());
    }
}