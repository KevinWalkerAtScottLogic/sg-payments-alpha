package gov.scot.payments.testing.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.UUID;

@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@MessageType(context = "test",type = "testUpdateEvent")
public class TestUpdateEvent extends TestEvent {

    public TestUpdateEvent(String key) {
        super(key);
    }

    @JsonCreator
    public TestUpdateEvent(@JsonProperty("messageId") final UUID messageId
            , @JsonProperty("timestamp") final Instant timestamp
            , @JsonProperty("key") final String key
            , @JsonProperty("correlationId") final UUID correlationId
            , @JsonProperty("executionCount") final int executionCount
            , @JsonProperty("reply") final boolean reply) {
        super(messageId, timestamp,key,correlationId,executionCount,reply);
    }
}
