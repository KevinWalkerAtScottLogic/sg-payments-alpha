package gov.scot.payments.testing;

import gov.scot.payments.testing.spring.KafkaStreamsOverridingContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootContextLoader;

public class ApplicationIntegrationTestContextLoader extends SpringBootContextLoader {

    @Override
    protected SpringApplication getSpringApplication() {
        final SpringApplication springApplication = new ApplicationIntegrationTestSpringApplication();
        springApplication.setApplicationContextClass(KafkaStreamsOverridingContext.class);
        return springApplication;
    }
}
