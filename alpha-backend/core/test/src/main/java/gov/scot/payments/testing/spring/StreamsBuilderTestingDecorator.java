package gov.scot.payments.testing.spring;

import gov.scot.payments.kafka.FilteringSerde;
import gov.scot.payments.testing.kafka.StubKStream;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.ValueTransformerWithKey;
import org.apache.kafka.streams.kstream.ValueTransformerWithKeySupplier;
import org.apache.kafka.streams.kstream.internals.ConsumedInternal;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.ProcessorSupplier;
import org.apache.kafka.streams.processor.StateStore;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;
import org.apache.kafka.streams.state.WindowBytesStoreSupplier;
import org.apache.kafka.streams.state.internals.InMemoryKeyValueStore;
import org.apache.kafka.streams.state.internals.InMemoryWindowStore;
import org.apache.kafka.streams.state.internals.KeyValueStoreBuilder;
import org.apache.kafka.streams.state.internals.WindowStoreBuilder;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.core.env.Environment;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Pattern;

@RequiredArgsConstructor
public class StreamsBuilderTestingDecorator extends StreamsBuilder {

    private static final String REGEX_PREFIX = "<REGEX>";

    private final BeanFactory beanFactory;
    private final CleanUpProcessor cleanUpProcessor = new CleanUpProcessor();

    @SuppressWarnings("unchecked")
    @Override
    public synchronized <K, V> KTable<K, V> table(String topic
            , Consumed<K, V> consumed
            , Materialized<K, V, KeyValueStore<Bytes, byte[]>> materialized) {
        return super.table(topic, consumed, (Materialized<K, V, KeyValueStore<Bytes, byte[]>>)new MaterializedWrapper(materialized)
                .withLoggingDisabled());
    }

    @Override
    public synchronized StreamsBuilder addStateStore(final StoreBuilder builder) {
        return super.addStateStore(storeBuilder(builder));
    }

    @Override
    public synchronized StreamsBuilder addGlobalStore(final StoreBuilder storeBuilder, final String topic, final Consumed consumed, final ProcessorSupplier stateUpdateSupplier) {
        return super.addGlobalStore(storeBuilder(storeBuilder), topic, consumed, stateUpdateSupplier);
    }

    @SuppressWarnings("unchecked")
    @Override
    public synchronized <K, V> KStream<K, V> stream(Collection<String> topics, Consumed<K, V> consumed) {
        if(topics.isEmpty()){
            return new StubKStream<>();
        }
        Consumed<K, V> filtered = applyFiltering(consumed);
        Pattern pattern = getPattern(topics);
        KStream<K, V> stream;
        if(pattern != null){
            stream = stream(pattern,filtered);
        } else{
            stream = super.stream(topics, filtered);
        }
        return stream.filter( (k,v) -> v != FilteringSerde.FILTERED_PLACEHOLDER).transformValues(cleanUpProcessor);
    }

    @SuppressWarnings("unchecked")
    private <K, V> Consumed<K, V> applyFiltering(final Consumed<K, V> consumed) {
        ConsumedInternal<K,V> accessible = new ConsumedInternal<>(consumed);
        if(accessible.valueDeserializer() != null && ByteArrayDeserializer.class.isAssignableFrom(accessible.valueDeserializer().getClass())){
            Serde<V> valueSerde = (Serde<V>) new FilteringSerde(beanFactory, (Serializer<byte[]>) accessible.valueSerde().serializer());
            return Consumed.<K,V>as(accessible.name())
                    .withTimestampExtractor(accessible.timestampExtractor())
                    .withOffsetResetPolicy(accessible.offsetResetPolicy())
                    .withKeySerde(accessible.keySerde())
                    .withValueSerde(valueSerde);
        }
        return consumed;
    }

    public void cleanStateStore(String name){
        cleanUpProcessor.cleanStateStore(name);
    }

    @SuppressWarnings("unchecked")
    private StoreBuilder storeBuilder(final StoreBuilder builder) {
        //kv
        if(builder instanceof KeyValueStoreBuilder){
            //noinspection unchecked
            return Stores.keyValueStoreBuilder(Stores.inMemoryKeyValueStore(builder.name()),extractKeySerde(builder),extractValueSerde(builder))
                         .withLoggingDisabled();
        } else if (builder instanceof WindowStoreBuilder){
            WindowBytesStoreSupplier windowBytesStoreSupplier = extractWindowStoreSupplier((WindowStoreBuilder)builder);
            return Stores.windowStoreBuilder(Stores.inMemoryWindowStore(builder.name()
                    , Duration.ofMillis(windowBytesStoreSupplier.retentionPeriod())
                    , Duration.ofMillis(windowBytesStoreSupplier.windowSize())
                    ,windowBytesStoreSupplier.retainDuplicates()),extractKeySerde(builder),extractValueSerde(builder))
                         .withLoggingDisabled();
        } else {
            return builder;
        }

    }

    private WindowBytesStoreSupplier extractWindowStoreSupplier(final WindowStoreBuilder builder) {
        return (WindowBytesStoreSupplier)ReflectionTestUtils.getField(builder,"storeSupplier");
    }

    private Serde extractValueSerde(final StoreBuilder builder) {
        return (Serde)ReflectionTestUtils.getField(builder,"valueSerde");
    }

    private Serde extractKeySerde(final StoreBuilder builder) {
        return (Serde)ReflectionTestUtils.getField(builder,"keySerde");
    }

    private Pattern getPattern(Collection<String> topics) {
        if(topics.size() == 1){
            String pattern = List.ofAll(topics).get(0);
            if(pattern.startsWith(REGEX_PREFIX)){
                pattern = pattern.substring(REGEX_PREFIX.length());
                return Pattern.compile(pattern);
            }
        }
        return null;
    }

    private static class MaterializedWrapper  extends Materialized<Bytes, byte[], KeyValueStore<Bytes, byte[]>> {

        @SuppressWarnings("unchecked")
        protected MaterializedWrapper(Materialized materialized) {
            super(materialized);
            this.storeSupplier = Stores.inMemoryKeyValueStore(storeName);
        }
    }

    private static class CleanUpProcessor implements ValueTransformerWithKey, ValueTransformerWithKeySupplier {

        private ProcessorContext context;

        private final java.util.List<CleanUpProcessor> children = new ArrayList<>();

        @Override
        public void init(final ProcessorContext context) {
            this.context = context;
        }

        @Override
        public Object transform(final Object readOnlyKey, final Object value) {
            return value;
        }

        @Override
        public void close() {

        }

        public void cleanStateStore(final String name) {
            try{
                StateStore stateStore = context.getStateStore(name);
                if(stateStore instanceof InMemoryKeyValueStore){
                    stateStore.close();
                    stateStore.init(null,null);
                } else if(stateStore instanceof InMemoryWindowStore){
                    stateStore.close();
                    stateStore.init(null,null);
                }
            } catch (Exception e){

            }
            children.forEach(c -> c.cleanStateStore(name));
        }

        @Override
        public ValueTransformerWithKey get() {
            final CleanUpProcessor cleanUpProcessor = new CleanUpProcessor();
            children.add(cleanUpProcessor);
            return cleanUpProcessor;
        }

    }
}
