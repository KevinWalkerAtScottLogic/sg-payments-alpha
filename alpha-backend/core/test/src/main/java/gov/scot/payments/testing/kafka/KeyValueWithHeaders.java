package gov.scot.payments.testing.kafka;

import com.google.common.base.Charsets;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.streams.KeyValue;

import java.util.Collections;

public class KeyValueWithHeaders<K, V> extends KeyValue<K, V> {

    /**
     * Timestamp of Kafka message (milliseconds since the epoch).
     */
    public final Headers headers;
    public final long timestamp;

    public KeyValueWithHeaders(final K key, final V value, long timestamp, final Headers headers) {
        super(key, value);
        this.headers = headers;
        this.timestamp = timestamp;
    }

    public KeyValueWithHeaders(final K key, final V value) {
        super(key, value);
        this.headers = new RecordHeaders();
        this.timestamp = System.currentTimeMillis();
    }

    public static <V> KeyValueWithHeaders<String, V> msg(V value, Header... headers){
        return new KeyValueWithHeaders<>(null,value,0L,new RecordHeaders(headers));
    }

    public static <V> KeyValueWithHeaders<String, V> msg(V value, long timestamp, Header... headers){
        return new KeyValueWithHeaders<>(null,value,timestamp,new RecordHeaders(headers));
    }

    public static <V> KeyValueWithHeaders<String, V> msg(String key, V value, Header... headers){
        return new KeyValueWithHeaders<>(key,value,0L,new RecordHeaders(headers));
    }

    public static <V> KeyValueWithHeaders<String, V> msg(String key, V value, String headerName, String headerValue){
        return new KeyValueWithHeaders<>(key,value,0L,new RecordHeaders(Collections.singleton(new RecordHeader(headerName,headerValue.getBytes(Charsets.UTF_8)))));
    }

    public static <K,V> KeyValueWithHeaders<K, V> msg(ConsumerRecord<K,V> record){
        return new KeyValueWithHeaders<>(record.key(),record.value(),record.timestamp(),record.headers());
    }

    public ProducerRecord<K,V> toProducerRecord(final String topic,List<Header> defaultHeaders) {
        java.util.Map<String,Header> headers = defaultHeaders.toMap(h -> new Tuple2<>(h.key(),h)).toJavaMap();
        this.headers.forEach(h -> headers.put(h.key(),h));
        final RecordHeaders headersToUse = new RecordHeaders(headers.values());
        return new ProducerRecord<>(topic,0,timestamp,key,value, headersToUse);
    }
}
