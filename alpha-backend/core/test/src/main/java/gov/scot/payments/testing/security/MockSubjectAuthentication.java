package gov.scot.payments.testing.security;

import gov.scot.payments.model.user.Application;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.model.user.User;
import io.vavr.collection.HashSet;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.AbstractOAuth2TokenAuthenticationToken;

import java.util.Map;

import static org.springframework.security.oauth2.jwt.JwtClaimNames.SUB;

public class MockSubjectAuthentication extends AbstractOAuth2TokenAuthenticationToken<Jwt> {

    public MockSubjectAuthentication(final Jwt token, final Subject principal) {
        super(token, principal, token, principal.getAuthorities().toJavaSet());
        this.setAuthenticated(true);
    }

    @Override
    public Map<String, Object> getTokenAttributes() {
        return getToken().getClaims();
    }

    public static MockSubjectAuthentication user(String name, Role... roles){
        Jwt jwt = createJwt();
        User user = User.builder()
                        .email("test@test.com")
                        .name(name)
                        .roles(HashSet.of(roles))
                        .groups(HashSet.of())
                        .build();
        return new MockSubjectAuthentication(jwt,user);
    }

    public static MockSubjectAuthentication apiClient(String name, Role... roles){
        Jwt jwt = createJwt();
        Application application = Application.builder()
                        .clientId(name)
                        .name(name)
                        .roles(HashSet.of(roles))
                        .build();
        return new MockSubjectAuthentication(jwt,application);
    }

    private static Jwt createJwt() {
        Jwt.Builder jwtBuilder = Jwt.withTokenValue("token")
                                    .header("alg", "none")
                                    .claim(SUB, "user")
                                    .claim("scope", "read");
        return jwtBuilder.build();
    }

}
