package gov.scot.payments.testing.spring;

import io.vavr.Tuple2;
import io.vavr.collection.List;
import lombok.NonNull;
import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.kafka.config.KafkaStreamsConfiguration;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;
import org.springframework.kafka.core.CleanupConfig;

import java.util.function.Supplier;

public class KafkaStreamsOverridingBeanFactory extends DefaultListableBeanFactory {

    @SuppressWarnings("unchecked")
    @Override
    public void registerBeanDefinition(@NonNull String beanName, BeanDefinition beanDefinition) throws BeanDefinitionStoreException {
        BeanDefinition def = beanDefinition;
        if(StreamsBuilderFactoryBean.class.getName().equalsIgnoreCase(beanDefinition.getBeanClassName())){
            Supplier<StreamsBuilderFactoryBean> instanceSupplier = (Supplier<StreamsBuilderFactoryBean>)((AbstractBeanDefinition)beanDefinition).getInstanceSupplier();
            def = BeanDefinitionBuilder.genericBeanDefinition(StreamsBuilderFactoryBean.class, () -> {
                StreamsBuilderFactoryBean streamsBuilderFactoryBean = instanceSupplier.get();
                KafkaStreamsConfiguration streamsConfig = new KafkaStreamsConfiguration(List.ofAll(streamsBuilderFactoryBean.getStreamsConfiguration().entrySet()).toJavaMap(v -> new Tuple2<>(v.getKey().toString(),v.getValue())));
                CleanupConfig cleanupConfig = new CleanupConfig();
                StreamsBuilderFactoryBeanDecorator streamsBuilderFactoryBeanDecorator = new StreamsBuilderFactoryBeanDecorator(streamsConfig,cleanupConfig,this);
                streamsBuilderFactoryBeanDecorator.setAutoStartup(false);
                return streamsBuilderFactoryBeanDecorator;
            })
                            .getRawBeanDefinition();

        }
        super.registerBeanDefinition(beanName,def);
    }
}
