package gov.scot.payments.testing.event;

import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasKey;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.UUID;

@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public class TestEvent extends EventWithCauseImpl implements HasKey<String>{

    @Getter private String key;

    public TestEvent(String key){
        this.key = key;
    }

    public TestEvent(final UUID messageId
            , final Instant timestamp
            , final String key
            , final UUID correlationId
            , final int executionCount
            , final boolean reply) {
        super(messageId, timestamp,correlationId,executionCount,reply);
        this.key = key;
    }
}
