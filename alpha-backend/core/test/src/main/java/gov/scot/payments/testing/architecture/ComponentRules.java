package gov.scot.payments.testing.architecture;

import com.tngtech.archunit.base.DescribedPredicate;
import com.tngtech.archunit.core.domain.JavaAnnotation;
import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.lang.*;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

public class ComponentRules {


    private static class MessageConstructorRoleStartsWith extends DescribedPredicate<JavaAnnotation> {

        private final String context;

        public MessageConstructorRoleStartsWith(final String context) {
            super("MessageConstructor role must start with %s", context);
            this.context = context;
        }

        @Override
        public boolean apply(final JavaAnnotation input) {
            if(!input.getRawType().isEquivalentTo(MessageConstructor.class)){
                return true;
            }
            String role = input.as(MessageConstructor.class).role();
            return role.isEmpty() || role.startsWith(context);
        }
    }

    private static class MessageTypeContextNameEquals extends DescribedPredicate<JavaAnnotation> {

        private final String context;

        public MessageTypeContextNameEquals(final String context) {
            super("MessageType context must be %s", context);
            this.context = context;
        }

        @Override
        public boolean apply(final JavaAnnotation input) {
            if(!input.getRawType().isEquivalentTo(MessageType.class)){
                return true;
            }
            return context.equals(input.as(MessageType.class).context());
        }
    }

    public static ArchRule doesNotDependOnOtherAppClasses(String component){
        DescribedPredicate<JavaClass> areOtherAppClasses =  JavaClass.Predicates.resideInAPackage("gov.scot.payments..app..")
                            .and(DescribedPredicate.not(JavaClass.Predicates.resideInAPackage("gov.scot.payments.."+component+".app..")));

        return noClasses().should()
                   .dependOnClassesThat(areOtherAppClasses);
    }

    public static ArchRule anyAnnotatedMessagesMustHaveSameContext(String context){
        return classes().that().areAnnotatedWith(MessageType.class)
                 .should().beAnnotatedWith(new MessageTypeContextNameEquals(context));
    }

    public static ArchRule anyMessageConstructorsWithRoleMustHaveSameContext(String context){
        return classes().that().areAnnotatedWith(MessageConstructor.class)
                        .should().beAnnotatedWith(new MessageConstructorRoleStartsWith(context+":"));
    }

    public static final ArchRule noCommandsDefined = noClasses().should().implement(Command.class);
}
