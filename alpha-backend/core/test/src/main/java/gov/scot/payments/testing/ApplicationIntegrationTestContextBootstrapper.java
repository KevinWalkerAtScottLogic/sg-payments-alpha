package gov.scot.payments.testing;

import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.kafka.EmbeddedKafkaBroker;
import gov.scot.payments.testing.kafka.EmbeddedKafkaExtension;
import io.vavr.Tuple2;
import io.vavr.collection.HashMap;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTestContextBootstrapper;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.test.context.ContextLoader;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

public class ApplicationIntegrationTestContextBootstrapper extends SpringBootTestContextBootstrapper {

    static {
        System.setProperty("h2.serializeJavaObject","false");
    }

    protected SpringBootTest.WebEnvironment getWebEnvironment(Class<?> testClass) {
        ApplicationIntegrationTest annotation = getOverriddenAnnotation(testClass);
        return (annotation != null) ? annotation.webEnvironment() : null;
    }

    protected Class<?>[] getClasses(Class<?> testClass) {
        ApplicationIntegrationTest annotation = getOverriddenAnnotation(testClass);
        return (annotation != null) ? annotation.classes() : null;
    }

    protected ApplicationIntegrationTest getOverriddenAnnotation(Class<?> testClass) {
        return MergedAnnotations.from(testClass, MergedAnnotations.SearchStrategy.INHERITED_ANNOTATIONS).get(ApplicationIntegrationTest.class)
                .synthesize(MergedAnnotation::isPresent).orElse(null);
    }

    @Override
    protected Class<? extends ContextLoader> getDefaultContextLoaderClass(final Class<?> testClass) {
        return ApplicationIntegrationTestContextLoader.class;
    }

    @Override
    protected String[] getProperties(Class<?> testClass) {
        ApplicationIntegrationTest annotation = getOverriddenAnnotation(testClass);
        if(annotation == null){
            return new String[0];
        }

        Map<String,String> props = getDefaultProperties(annotation.componentType());

        EmbeddedKafkaBroker broker = EmbeddedKafkaExtension.getBroker();
        if(broker == null){
            return toPropertiesArray(mergeAnnotationProperties(props,annotation.properties()));
        }
        props = addBrokerProperties(props,broker);
        props = mergeAnnotationProperties(props,annotation.properties());
        props = createBindings(props
                ,broker
                ,annotation.bindings()
                ,annotation.componentType()
                ,annotation.additionalTopics());
        return toPropertiesArray(props);
    }

    private Map<String, String> createBindings(final Map<String, String> props
            , final EmbeddedKafkaBroker broker
            , final String[] bindings
            , final ApplicationIntegrationTest.ComponentType componentType
            , final int additionalTopics) {

        String[] combinedBindings = getBindings(bindings,componentType);
        Map<String,String> bindingProps = HashSet.of(combinedBindings)
                                                 .map(b -> new Tuple2<>(b,UUID.randomUUID().toString()))
                                                 .map(t -> t.map1(b -> List.of(b.split(":"))))
                                                 .flatMap(t -> t._1.map(b -> new Tuple2<>(String.format("spring.cloud.stream.bindings.%s.destination",b),t._2)))
                                                 .toMap(t -> t);
        bindingProps.values().toSet().forEach(broker::addTopicsIfNotExists);
        Map<String,String> additionalTopicMap = List.range(0,additionalTopics)
            .toMap(i -> new Tuple2<>(String.format("embedded.broker.additional.topic.%s",i+1),UUID.randomUUID().toString()));
        additionalTopicMap.values().toSet().forEach(broker::addTopicsIfNotExists);
        java.util.Map<String, String> mutableProps = props.toJavaMap();
        mutableProps.putAll(bindingProps.toJavaMap());
        mutableProps.putAll(additionalTopicMap.toJavaMap());
        if(componentType == ApplicationIntegrationTest.ComponentType.PROCESS_MANAGER){
            int retryCount = props.get("retry.count").map(Integer::parseInt).getOrElse(0);
            String commandTopic = UUID.randomUUID().toString();
            List<String> retryTopics = List.range(1,retryCount+1).map(i -> commandTopic+"-retry-"+i);
            broker.addTopicsIfNotExists(commandTopic);
            retryTopics.forEach(broker::addTopicsIfNotExists);
            mutableProps.put("commands.destination",commandTopic);
        }
        if(componentType == ApplicationIntegrationTest.ComponentType.COMMAND_HANDLER){
            String stateTopic = UUID.randomUUID().toString();
            broker.addTopicsIfNotExists(stateTopic);
            mutableProps.put("state.topic",stateTopic);
        }

        return HashMap.ofAll(mutableProps);
    }

    public static String[] getBindings(final String[] bindings, final ApplicationIntegrationTest.ComponentType componentType) {
        if(bindings.length > 0){
            return bindings;
        }
        String[] defaultBindings= new String[0];
        switch(componentType){
            case CUSTOM:
                break;
            case PROJECTOR:
                defaultBindings = new String[]{"events-internal","events-external","output"};
                break;
            case PROCESS_MANAGER:
                defaultBindings = new String[]{"events-internal","events-external"};
                break;
            case COMMAND_HANDLER:
                defaultBindings = new String[]{"commands:send-commands"
                        ,"events:receive-events:send-error-events:send-events"
                        ,"responses:send-error-responses"
                        ,"receive-command-errors"};
                break;
        }
        return ArrayUtils.addAll(bindings,defaultBindings);
    }

    private Map<String, String> addBrokerProperties(final Map<String, String> props, final EmbeddedKafkaBroker broker) {
        Path tmpPath;
        try {
            tmpPath = Files.createTempDirectory("kafka-streams").toAbsolutePath();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        tmpPath.toFile().deleteOnExit();

        return props
                .put("spring.kafka.bootstrap-servers",broker.getBrokersAsString())
                .put("spring.cloud.stream.kafka.streams.binder.brokers",broker.getBrokersAsString())
                .put("spring.cloud.stream.kafka.streams.binder.configuration.state.dir",tmpPath.toString().replace("\\","/"));
    }

    private String[] toPropertiesArray(final Map<String, String> props) {
        return props.toArray()
             .map(t -> String.format("%s=%s",t._1,t._2))
             .toJavaArray(String[]::new);
    }

    private Map<String, String> mergeAnnotationProperties(final Map<String, String> props, final String[] properties) {
        Map<String,String> annotationProps = List.of(properties).toMap(s -> {
            String[] kv = s.split("=");
            String val = kv.length > 1 ? kv[1] : "";
            return new Tuple2<>(kv[0],val);
        });
        java.util.Map<String, String> mutableProps = props.toJavaMap();
        mutableProps.putAll(annotationProps.toJavaMap());
        return HashMap.ofAll(mutableProps);
    }

    private Map<String,String> getDefaultProperties(final ApplicationIntegrationTest.ComponentType componentType) {
        Map<String,String> props = HashMap.<String,String>empty()
                .put("spring.cloud.stream.kafka.streams.binder.configuration.commit.interval.ms","100")
                .put("kafka.securiuty.protocol","PLAINTEXT")
                .put("kafka.replication-factor","1")
                .put("spring.kafka.properties.sasl.mechanism","GSSAPI")
                .put("spring.kafka.properties.sasl.jaas.config","na")
                .put("spring.kafka.properties.ssl.endpoint.identification.algorithm","na")
                .put("spring.cloud.kubernetes.discovery.enabled","false")
                .put("spring.cloud.kubernetes.enabled","false")
                .put("spring.cloud.config.discovery.enabled","false")
                .put("TOKEN_SERVICE_ISSUER","test")
                .put("AUTH_SERVER_DOMAIN","test")
                .put("ENVIRONMENT", RandomStringUtils.random(8, true, false))
                .put("POD_HOST", "localhost")
                .put("INGRESS_HOST", "localhost")
                .put("INGRESS_PROTOCOL", "http")
                .put("INGRESS_PATH", "")
                .put("POD_PORT", "80")
                .put("SCHEMA_REGISTRY_URL","test")
                .put("h2.serializeJavaObject","false")
                .put("SCHEMA_REGISTRY_API_KEY","test")
                .put("SCHEMA_REGISTRY_API_SECRET","test")
                .put("KAFKA_API_KEY","test")
                .put("KAFKA_API_SECRET","test")
                .put("partitioned","false")
                .put("partition-count","1");
        switch(componentType){
            case CUSTOM:
                break;
            case PROJECTOR:
                props = props.put("retry.count","0")
                        .put("retry.initial-delay","PT0S")
                        .put("spring.flyway.schemas","")
                        .put("spring.jpa.properties.hibernate.default_schema","");
                break;
            case PROCESS_MANAGER:
                props = props.put("retry.count","0")
                             .put("retry.initial-delay","PT0S");
                break;
            case COMMAND_HANDLER:
                props = props.put("token.service.client-id","test")
                             .put("token.service.client-secret","test")
                             .put("token.service.domain","test");
                break;
        }
        return props;
    }
}
