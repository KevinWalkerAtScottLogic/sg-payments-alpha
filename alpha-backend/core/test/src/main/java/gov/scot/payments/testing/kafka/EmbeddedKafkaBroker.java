package gov.scot.payments.testing.kafka;

import io.vavr.collection.List;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Delegate;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.common.TopicPartition;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@RequiredArgsConstructor
public class EmbeddedKafkaBroker implements InitializingBean, DisposableBean {

    @Delegate
    @Getter private final org.springframework.kafka.test.EmbeddedKafkaBroker delegate;

    @Getter private Map<String,Long> offsets = new HashMap<>();

    public void addTopicsIfNotExists(String... topics){
        Set<String> existingTopics = getTopics();
        Set<String> newTopics = new HashSet<>(Set.of(topics));
        newTopics.removeAll(existingTopics);
        addTopics(newTopics.toArray(String[]::new));
    }

    public void consumeFromAnEmbeddedTopicWithOffset(Consumer<?, ?> consumer, String topic) {
        HashSet<String> diff = new HashSet<>(Collections.singleton(topic));
        diff.removeAll(new HashSet<>(delegate.getTopics()));
        assertThat(delegate.getTopics())
                .as("topic(s):'" + diff + "' are not in embedded topic list")
                .containsAll(new HashSet<>(Collections.singleton(topic)));
        TopicPartition partition = new TopicPartition(topic, 0);
        consumer.assign(List.of(partition).toJavaList());
        consumer.seek(partition,offsets.getOrDefault(topic,0L));
    }

    public void updateCurrentOffsets() {
        Set<String> topics = getTopics();
        for(String topic : topics){
            long offset = getCurrentOffset(topic);
            offsets.put(topic,offset);
        }
    }

    private <K,V> long getCurrentOffset(String topic) {
        Map<String, Object> consumerProps = KafkaTestUtils.consumerProps(UUID.randomUUID().toString(), "false", delegate);
        DefaultKafkaConsumerFactory<K,V> cf = new DefaultKafkaConsumerFactory<>(consumerProps);
        org.apache.kafka.clients.consumer.Consumer<K,V> consumer = cf.createConsumer();
        consumeFromAnEmbeddedTopic(consumer,topic);
        TopicPartition topicPartition = new TopicPartition(topic, 0);
        consumer.seekToEnd(Collections.singletonList(topicPartition));
        try{
            return consumer.position(topicPartition);
        } finally {
            consumer.close();
        }
    }
}
