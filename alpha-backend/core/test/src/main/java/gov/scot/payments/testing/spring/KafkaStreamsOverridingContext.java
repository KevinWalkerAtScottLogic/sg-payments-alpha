package gov.scot.payments.testing.spring;

import org.springframework.boot.web.reactive.context.GenericReactiveWebApplicationContext;

public class KafkaStreamsOverridingContext extends GenericReactiveWebApplicationContext {

    public KafkaStreamsOverridingContext() {
        super(new KafkaStreamsOverridingBeanFactory());
    }

}
