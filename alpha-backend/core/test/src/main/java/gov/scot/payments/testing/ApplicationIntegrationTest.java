package gov.scot.payments.testing;

import gov.scot.payments.testing.kafka.EmbeddedKafka;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebFlux;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.context.ServerPortInfoApplicationContextInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AliasFor;
import org.springframework.core.env.Environment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.BootstrapWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.annotation.*;

@ExtendWith({ApplicationIntegrationTestExtension.class, SpringExtension.class})
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@EmbeddedKafka
@AutoConfigureWebTestClient(timeout = "PT10S")
@AutoConfigureWebFlux
@BootstrapWith(ApplicationIntegrationTestContextBootstrapper.class)
@ContextConfiguration(initializers = ServerPortInfoApplicationContextInitializer.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public @interface ApplicationIntegrationTest {

    /**
     * Alias for {@link #properties()}.
     * @return the properties to apply
     */
    @AliasFor( value = "properties")
    String[] value() default {};

    /**
     * Properties in form {@literal key=value} that should be added to the Spring
     * {@link Environment} before the test runs.
     * @return the properties to add
     */
    @AliasFor(value = "value")
    String[] properties() default {};


    String[] bindings() default {};

    int additionalTopics() default  0;
    /**
     * The <em>component classes</em> to use for loading an
     * {@link org.springframework.context.ApplicationContext ApplicationContext}. Can also
     * be specified using
     * {@link ContextConfiguration#classes() @ContextConfiguration(classes=...)}. If no
     * explicit classes are defined the test will look for nested
     * {@link Configuration @Configuration} classes, before falling back to a
     * {@link SpringBootConfiguration @SpringBootConfiguration} search.
     * @see ContextConfiguration#classes()
     * @return the component classes used to load the application context
     */
    Class<?>[] classes() default {};

    /**
     * The type of web environment to create when applicable. Defaults to
     * {@link SpringBootTest.WebEnvironment#MOCK}.
     * @return the type of web environment
     */
    SpringBootTest.WebEnvironment webEnvironment() default SpringBootTest.WebEnvironment.RANDOM_PORT;

    ComponentType componentType() default ComponentType.CUSTOM;

    enum ComponentType {
        CUSTOM, PROJECTOR, PROCESS_MANAGER, COMMAND_HANDLER
    }


}
