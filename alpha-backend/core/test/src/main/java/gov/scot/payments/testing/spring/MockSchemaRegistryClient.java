package gov.scot.payments.testing.spring;

import lombok.Getter;
import org.springframework.cloud.stream.schema.SchemaReference;
import org.springframework.cloud.stream.schema.SchemaRegistrationResponse;
import org.springframework.cloud.stream.schema.client.SchemaRegistryClient;

import java.util.HashMap;
import java.util.Map;


public class MockSchemaRegistryClient implements SchemaRegistryClient {

    private static int MAX_ID = 0;
    @Getter private final Map<SchemaReference,String> schemas = new HashMap<>();
    private final Map<Integer,String> schemasById = new HashMap<>();
    private final Map<String,Integer> latestSchemaVersionBySubject = new HashMap<>();

    @Override
    public synchronized SchemaRegistrationResponse register(final String subject, final String format, final String schema) {
       int id = MAX_ID++;
       int version = latestSchemaVersionBySubject.getOrDefault(subject,0) + 1;
        SchemaReference reference = new SchemaReference(subject,version,format);
       schemas.put(reference,schema);
        schemasById.put(id,schema);
        latestSchemaVersionBySubject.put(subject,version);
        SchemaRegistrationResponse response = new SchemaRegistrationResponse();
        response.setId(id);
        response.setSchemaReference(reference);
        return response;
    }

    @Override
    public  String fetch(final SchemaReference schemaReference) {
       return schemas.get(schemaReference);
    }

    @Override
    public  String fetch(final int id) {
        return schemasById.get(id);
    }

    public synchronized void clear(){
        schemas.clear();
        schemasById.clear();
        latestSchemaVersionBySubject.clear();
    }
}
