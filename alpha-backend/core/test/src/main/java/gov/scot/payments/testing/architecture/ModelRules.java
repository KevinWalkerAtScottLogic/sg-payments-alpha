package gov.scot.payments.testing.architecture;

import com.tngtech.archunit.base.DescribedPredicate;
import com.tngtech.archunit.core.domain.JavaAnnotation;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.domain.JavaModifier;
import com.tngtech.archunit.core.domain.JavaPackage;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.*;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.Message;
import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import io.vavr.collection.List;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.methods;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

public class ModelRules {

    @ArchTest
    public static final ArchRule allClassesInModelPackage = classes()
            .should().resideInAPackage("gov.scot.payments..model..");

    @ArchTest
    public static final ArchRule anyMessagesMustBeAnnotated = classes().that().implement(Message.class)
                                                                       .should().beAnnotatedWith(MessageType.class);

    @ArchTest
    public static final ArchRule anyMessageConstructorsAreValid = methods().that().areAnnotatedWith(MessageConstructor.class)
                                                                       .should().beStatic()
                                                                       .andShould().bePublic()
                                                                       .andShould().beDeclaredInClassesThat().implement(Command.class);

    @ArchTest
    public static final ArchRule doesNotDependOnAppClasses = noClasses().should().dependOnClassesThat()
                                                                        .resideInAPackage("gov.scot.payments..app..");

    @ArchTest
    public static final ArchRule messageTypesMustBeValid = classes().that().areAnnotatedWith(MessageType.class)
                                                              .should().beAnnotatedWith(new ValidMessageTypes());

    @ArchTest
    public static final ArchRule messagesMustBeConcrete = classes().that().areAnnotatedWith(MessageType.class)
                                                                    .should().notHaveModifier(JavaModifier.ABSTRACT)
                                                                    .andShould().notBeInterfaces();

    @ArchTest
    public static ArchRule annotatedMessagesMustHaveUniqueType() {
        return ArchRuleDefinition.all(module()).should(haveUniqueMessageTypes);
    }

    @ArchTest
    public static final ArchRule eventsMustBeNamedEndingWithEvent = classes().that().doNotHaveModifier(JavaModifier.ABSTRACT)
                                                                   .and().areNotInterfaces()
                                                                   .and().implement(Event.class)
                                                                   .should().haveSimpleNameEndingWith("Event");

    @ArchTest
    public static final ArchRule commandsMustBeNamedEndingWithCommand = classes().that().doNotHaveModifier(JavaModifier.ABSTRACT)
                                                                             .and().areNotInterfaces()
                                                                             .and().implement(Command.class)
                                                                             .should().haveSimpleNameEndingWith("Command");



    private static class ValidMessageTypes extends DescribedPredicate<JavaAnnotation> {

        public ValidMessageTypes() {
            super("MessageType values must be Alphanumeric");
        }

        @Override
        public boolean apply(final JavaAnnotation input) {
            if(!input.getRawType().isEquivalentTo(MessageType.class)){
                return true;
            }
            MessageType messageType = input.as(MessageType.class);
            return messageType.context().matches("[A-Za-z0-9]+")
                    && messageType.type().matches("[A-Za-z0-9]+");
        }
    }

    private static ArchCondition<JavaPackage> haveUniqueMessageTypes =
            new ArchCondition<>("have unique @MessageType 'type' elements") {

                @Override
                public void check(JavaPackage item, ConditionEvents events) {

                    List.ofAll(item.getAllClasses())
                            .flatMap(javaClass -> javaClass.getAnnotations())
                            .filter(javaAnnotation -> javaAnnotation.getRawType().isEquivalentTo(MessageType.class))
                            .groupBy(javaAnnotation -> (String) (javaAnnotation.get("type").get()))
                            .filterValues(javaAnnotations -> javaAnnotations.length() > 1)
                            .keySet()
                            .forEach(s -> events.add(
                                    SimpleConditionEvent.violated(s,
                                            String.format("Multiple @MessageType annotations have 'type = \"%s\"'", s)))
                            );
                }
            };

    private static ClassesTransformer<JavaPackage> module() {
        return new AbstractClassesTransformer<>("module") {
            @Override
            public List<JavaPackage> doTransform(JavaClasses classes) {
                return List.of(classes.getDefaultPackage());
            }
        };
    }
}
