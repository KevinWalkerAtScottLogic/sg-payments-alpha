package gov.scot.payments.testing;

import gov.scot.payments.testing.kafka.CleanupStateStore;
import gov.scot.payments.testing.kafka.EmbeddedKafkaBroker;
import gov.scot.payments.testing.kafka.EmbeddedKafkaExtension;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static gov.scot.payments.testing.ApplicationIntegrationTestContextBootstrapper.getBindings;

public class ApplicationIntegrationTestExtension extends EmbeddedKafkaExtension implements BeforeEachCallback {

    @Override
    public boolean supportsParameter(final ParameterContext parameterContext, final ExtensionContext extensionContext) throws ParameterResolutionException {
        return parameterContext.getParameter().getType().equals(EmbeddedBrokerClient.class)
                || super.supportsParameter(parameterContext, extensionContext);
    }

    @Override
    public Object resolveParameter(final ParameterContext parameterContext, final ExtensionContext context) throws ParameterResolutionException {
        if(parameterContext.getParameter().getType().equals(EmbeddedBrokerClient.class)){
            return buildClient(context);
        }
        return super.resolveParameter(parameterContext, context);
    }

    @Override
    public void beforeEach(final ExtensionContext context) throws Exception {
        EmbeddedKafkaExtension.getBroker().updateCurrentOffsets();
        CleanupStateStore cleanUp = AnnotatedElementUtils.findMergedAnnotation(context.getRequiredTestMethod(), CleanupStateStore.class);
        if (cleanUp != null) {
            EmbeddedBrokerClient client = buildClient(context);
            client.cleanStateStore(cleanUp.name(),cleanUp.type());
        }
    }

    private EmbeddedBrokerClient buildClient(final ExtensionContext context) {
        ApplicationIntegrationTest annotation = AnnotatedElementUtils.findMergedAnnotation(context.getRequiredTestClass(), ApplicationIntegrationTest.class);
        EmbeddedKafkaBroker broker = EmbeddedKafkaExtension.getBroker();
        ApplicationContext springContext = SpringExtension.getApplicationContext(context);
        return new EmbeddedBrokerClient(springContext,broker,getBindings(annotation.bindings(),annotation.componentType()));
    }

}
