package gov.scot.payments.testing.command;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.model.MessageType;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.UUID;

@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@MessageType(context = "test",type = "testDeleteCommand")
public class TestDeleteCommand extends TestCommand {

    public TestDeleteCommand(String key) {
        super(key);
    }

    public TestDeleteCommand(String key, boolean reply) {
        super(key,reply);
    }

    public TestDeleteCommand(String key, boolean reply, int executionCount) {
        super(key,reply,executionCount);
    }

    @JsonCreator
    public TestDeleteCommand(@JsonProperty("messageId") final UUID messageId
            , @JsonProperty("timestamp") final Instant timestamp
            , @JsonProperty("reply") final boolean reply
            , @JsonProperty("executionCount") final int executionCount
            , @JsonProperty("key") final String key) {
        super(messageId, timestamp,reply,executionCount,key);
    }
}
