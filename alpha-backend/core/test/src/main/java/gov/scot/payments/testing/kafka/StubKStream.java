package gov.scot.payments.testing.kafka;

import gov.scot.payments.kafka.EmptyStream;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.processor.ProcessorSupplier;
import org.apache.kafka.streams.processor.TopicNameExtractor;

import java.util.Arrays;

public class StubKStream<K,V> implements KStream<K,V>, EmptyStream {
    @Override
    public KStream<K, V> filter(final Predicate<? super K, ? super V> predicate) {
        return null;
    }

    @Override
    public KStream<K, V> filter(final Predicate<? super K, ? super V> predicate, final Named named) {
        return null;
    }

    @Override
    public KStream<K, V> filterNot(final Predicate<? super K, ? super V> predicate) {
        return null;
    }

    @Override
    public KStream<K, V> filterNot(final Predicate<? super K, ? super V> predicate, final Named named) {
        return null;
    }

    @Override
    public <KR> KStream<KR, V> selectKey(final KeyValueMapper<? super K, ? super V, ? extends KR> mapper) {
        return null;
    }

    @Override
    public <KR> KStream<KR, V> selectKey(final KeyValueMapper<? super K, ? super V, ? extends KR> mapper, final Named named) {
        return null;
    }

    @Override
    public <KR, VR> KStream<KR, VR> map(final KeyValueMapper<? super K, ? super V, ? extends KeyValue<? extends KR, ? extends VR>> mapper) {
        return null;
    }

    @Override
    public <KR, VR> KStream<KR, VR> map(final KeyValueMapper<? super K, ? super V, ? extends KeyValue<? extends KR, ? extends VR>> mapper, final Named named) {
        return null;
    }

    @Override
    public <VR> KStream<K, VR> mapValues(final ValueMapper<? super V, ? extends VR> mapper) {
        return (KStream<K, VR>)this;
    }

    @Override
    public <VR> KStream<K, VR> mapValues(final ValueMapper<? super V, ? extends VR> mapper, final Named named) {
        return null;
    }

    @Override
    public <VR> KStream<K, VR> mapValues(final ValueMapperWithKey<? super K, ? super V, ? extends VR> mapper) {
        return (KStream<K, VR>)this;
    }

    @Override
    public <VR> KStream<K, VR> mapValues(final ValueMapperWithKey<? super K, ? super V, ? extends VR> mapper, final Named named) {
        return null;
    }

    @Override
    public <KR, VR> KStream<KR, VR> flatMap(final KeyValueMapper<? super K, ? super V, ? extends Iterable<? extends KeyValue<? extends KR, ? extends VR>>> mapper) {
        return null;
    }

    @Override
    public <KR, VR> KStream<KR, VR> flatMap(final KeyValueMapper<? super K, ? super V, ? extends Iterable<? extends KeyValue<? extends KR, ? extends VR>>> mapper, final Named named) {
        return null;
    }

    @Override
    public <VR> KStream<K, VR> flatMapValues(final ValueMapper<? super V, ? extends Iterable<? extends VR>> mapper) {
        return null;
    }

    @Override
    public <VR> KStream<K, VR> flatMapValues(final ValueMapper<? super V, ? extends Iterable<? extends VR>> mapper, final Named named) {
        return null;
    }

    @Override
    public <VR> KStream<K, VR> flatMapValues(final ValueMapperWithKey<? super K, ? super V, ? extends Iterable<? extends VR>> mapper) {
        return null;
    }

    @Override
    public <VR> KStream<K, VR> flatMapValues(final ValueMapperWithKey<? super K, ? super V, ? extends Iterable<? extends VR>> mapper, final Named named) {
        return null;
    }

    @Override
    public void print(final Printed<K, V> printed) {

    }

    @Override
    public void foreach(final ForeachAction<? super K, ? super V> action) {

    }

    @Override
    public void foreach(final ForeachAction<? super K, ? super V> action, final Named named) {

    }

    @Override
    public KStream<K, V> peek(final ForeachAction<? super K, ? super V> action) {
        return null;
    }

    @Override
    public KStream<K, V> peek(final ForeachAction<? super K, ? super V> action, final Named named) {
        return null;
    }

    @Override
    public KStream<K, V>[] branch(final Predicate<? super K, ? super V>... predicates) {
        final KStream[] a = new KStream[predicates.length];
        Arrays.fill(a,this);
        return a;
    }

    @Override
    public KStream<K, V>[] branch(final Named named, final Predicate<? super K, ? super V>... predicates) {
        return new KStream[0];
    }

    @Override
    public KStream<K, V> merge(final KStream<K, V> stream) {
        return null;
    }

    @Override
    public KStream<K, V> merge(final KStream<K, V> stream, final Named named) {
        return null;
    }

    @Override
    public KStream<K, V> through(final String topic) {
        return null;
    }

    @Override
    public KStream<K, V> through(final String topic, final Produced<K, V> produced) {
        return null;
    }

    @Override
    public void to(final String topic) {

    }

    @Override
    public void to(final String topic, final Produced<K, V> produced) {

    }

    @Override
    public void to(final TopicNameExtractor<K, V> topicExtractor) {

    }

    @Override
    public void to(final TopicNameExtractor<K, V> topicExtractor, final Produced<K, V> produced) {

    }

    @Override
    public <K1, V1> KStream<K1, V1> transform(final TransformerSupplier<? super K, ? super V, KeyValue<K1, V1>> transformerSupplier, final String... stateStoreNames) {
        return null;
    }

    @Override
    public <K1, V1> KStream<K1, V1> transform(final TransformerSupplier<? super K, ? super V, KeyValue<K1, V1>> transformerSupplier, final Named named, final String... stateStoreNames) {
        return null;
    }

    @Override
    public <K1, V1> KStream<K1, V1> flatTransform(final TransformerSupplier<? super K, ? super V, Iterable<KeyValue<K1, V1>>> transformerSupplier, final String... stateStoreNames) {
        return null;
    }

    @Override
    public <K1, V1> KStream<K1, V1> flatTransform(final TransformerSupplier<? super K, ? super V, Iterable<KeyValue<K1, V1>>> transformerSupplier, final Named named, final String... stateStoreNames) {
        return null;
    }

    @Override
    public <VR> KStream<K, VR> transformValues(final ValueTransformerSupplier<? super V, ? extends VR> valueTransformerSupplier, final String... stateStoreNames) {
        return (KStream<K, VR>)this;
    }

    @Override
    public <VR> KStream<K, VR> transformValues(final ValueTransformerSupplier<? super V, ? extends VR> valueTransformerSupplier, final Named named, final String... stateStoreNames) {
        return null;
    }

    @Override
    public <VR> KStream<K, VR> transformValues(final ValueTransformerWithKeySupplier<? super K, ? super V, ? extends VR> valueTransformerSupplier, final String... stateStoreNames) {
        return (KStream<K, VR>)this;
    }

    @Override
    public <VR> KStream<K, VR> transformValues(final ValueTransformerWithKeySupplier<? super K, ? super V, ? extends VR> valueTransformerSupplier, final Named named, final String... stateStoreNames) {
        return null;
    }

    @Override
    public <VR> KStream<K, VR> flatTransformValues(final ValueTransformerSupplier<? super V, Iterable<VR>> valueTransformerSupplier, final String... stateStoreNames) {
        return null;
    }

    @Override
    public <VR> KStream<K, VR> flatTransformValues(final ValueTransformerSupplier<? super V, Iterable<VR>> valueTransformerSupplier, final Named named, final String... stateStoreNames) {
        return null;
    }

    @Override
    public <VR> KStream<K, VR> flatTransformValues(final ValueTransformerWithKeySupplier<? super K, ? super V, Iterable<VR>> valueTransformerSupplier, final String... stateStoreNames) {
        return null;
    }

    @Override
    public <VR> KStream<K, VR> flatTransformValues(final ValueTransformerWithKeySupplier<? super K, ? super V, Iterable<VR>> valueTransformerSupplier, final Named named, final String... stateStoreNames) {
        return null;
    }

    @Override
    public void process(final ProcessorSupplier<? super K, ? super V> processorSupplier, final String... stateStoreNames) {

    }

    @Override
    public void process(final ProcessorSupplier<? super K, ? super V> processorSupplier, final Named named, final String... stateStoreNames) {

    }

    @Override
    public KGroupedStream<K, V> groupByKey() {
        return null;
    }

    @Override
    public KGroupedStream<K, V> groupByKey(final Serialized<K, V> serialized) {
        return null;
    }

    @Override
    public KGroupedStream<K, V> groupByKey(final Grouped<K, V> grouped) {
        return null;
    }

    @Override
    public <KR> KGroupedStream<KR, V> groupBy(final KeyValueMapper<? super K, ? super V, KR> selector) {
        return null;
    }

    @Override
    public <KR> KGroupedStream<KR, V> groupBy(final KeyValueMapper<? super K, ? super V, KR> selector, final Serialized<KR, V> serialized) {
        return null;
    }

    @Override
    public <KR> KGroupedStream<KR, V> groupBy(final KeyValueMapper<? super K, ? super V, KR> selector, final Grouped<KR, V> grouped) {
        return null;
    }

    @Override
    public <VO, VR> KStream<K, VR> join(final KStream<K, VO> otherStream, final ValueJoiner<? super V, ? super VO, ? extends VR> joiner, final JoinWindows windows) {
        return null;
    }

    @Override
    public <VO, VR> KStream<K, VR> join(final KStream<K, VO> otherStream, final ValueJoiner<? super V, ? super VO, ? extends VR> joiner, final JoinWindows windows, final Joined<K, V, VO> joined) {
        return null;
    }

    @Override
    public <VO, VR> KStream<K, VR> join(final KStream<K, VO> otherStream, final ValueJoiner<? super V, ? super VO, ? extends VR> joiner, final JoinWindows windows, final StreamJoined<K, V, VO> streamJoined) {
        return null;
    }

    @Override
    public <VO, VR> KStream<K, VR> leftJoin(final KStream<K, VO> otherStream, final ValueJoiner<? super V, ? super VO, ? extends VR> joiner, final JoinWindows windows) {
        return null;
    }

    @Override
    public <VO, VR> KStream<K, VR> leftJoin(final KStream<K, VO> otherStream, final ValueJoiner<? super V, ? super VO, ? extends VR> joiner, final JoinWindows windows, final Joined<K, V, VO> joined) {
        return null;
    }

    @Override
    public <VO, VR> KStream<K, VR> leftJoin(final KStream<K, VO> otherStream, final ValueJoiner<? super V, ? super VO, ? extends VR> joiner, final JoinWindows windows, final StreamJoined<K, V, VO> streamJoined) {
        return null;
    }

    @Override
    public <VO, VR> KStream<K, VR> outerJoin(final KStream<K, VO> otherStream, final ValueJoiner<? super V, ? super VO, ? extends VR> joiner, final JoinWindows windows) {
        return null;
    }

    @Override
    public <VO, VR> KStream<K, VR> outerJoin(final KStream<K, VO> otherStream, final ValueJoiner<? super V, ? super VO, ? extends VR> joiner, final JoinWindows windows, final Joined<K, V, VO> joined) {
        return null;
    }

    @Override
    public <VO, VR> KStream<K, VR> outerJoin(final KStream<K, VO> otherStream, final ValueJoiner<? super V, ? super VO, ? extends VR> joiner, final JoinWindows windows, final StreamJoined<K, V, VO> streamJoined) {
        return null;
    }

    @Override
    public <VT, VR> KStream<K, VR> join(final KTable<K, VT> table, final ValueJoiner<? super V, ? super VT, ? extends VR> joiner) {
        return null;
    }

    @Override
    public <VT, VR> KStream<K, VR> join(final KTable<K, VT> table, final ValueJoiner<? super V, ? super VT, ? extends VR> joiner, final Joined<K, V, VT> joined) {
        return null;
    }

    @Override
    public <VT, VR> KStream<K, VR> leftJoin(final KTable<K, VT> table, final ValueJoiner<? super V, ? super VT, ? extends VR> joiner) {
        return null;
    }

    @Override
    public <VT, VR> KStream<K, VR> leftJoin(final KTable<K, VT> table, final ValueJoiner<? super V, ? super VT, ? extends VR> joiner, final Joined<K, V, VT> joined) {
        return null;
    }

    @Override
    public <GK, GV, RV> KStream<K, RV> join(final GlobalKTable<GK, GV> globalKTable, final KeyValueMapper<? super K, ? super V, ? extends GK> keyValueMapper, final ValueJoiner<? super V, ? super GV, ? extends RV> joiner) {
        return null;
    }

    @Override
    public <GK, GV, RV> KStream<K, RV> join(final GlobalKTable<GK, GV> globalKTable, final KeyValueMapper<? super K, ? super V, ? extends GK> keyValueMapper, final ValueJoiner<? super V, ? super GV, ? extends RV> joiner, final Named named) {
        return null;
    }

    @Override
    public <GK, GV, RV> KStream<K, RV> leftJoin(final GlobalKTable<GK, GV> globalKTable, final KeyValueMapper<? super K, ? super V, ? extends GK> keyValueMapper, final ValueJoiner<? super V, ? super GV, ? extends RV> valueJoiner) {
        return null;
    }

    @Override
    public <GK, GV, RV> KStream<K, RV> leftJoin(final GlobalKTable<GK, GV> globalKTable, final KeyValueMapper<? super K, ? super V, ? extends GK> keyValueMapper, final ValueJoiner<? super V, ? super GV, ? extends RV> valueJoiner, final Named named) {
        return null;
    }
}
