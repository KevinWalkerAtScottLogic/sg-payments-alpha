package gov.scot.payments.testing.command;

import gov.scot.payments.model.CommandImpl;
import gov.scot.payments.model.HasKey;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.UUID;

@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public class TestCommand extends CommandImpl implements HasKey<String> {

    @Getter private String key;

    public TestCommand(String key){
        this.key = key;
    }

    public TestCommand(String key, boolean reply){
        super(reply);
        this.key = key;
    }

    public TestCommand(String key, boolean reply,int executionCount){
        super(reply,executionCount);
        this.key = key;
    }

    public TestCommand(final UUID messageId
            , final Instant timestamp
            , final boolean reply
            , final int executionCount
            , final String key) {
        super(messageId, timestamp,reply,executionCount);
        this.key = key;
    }
}
