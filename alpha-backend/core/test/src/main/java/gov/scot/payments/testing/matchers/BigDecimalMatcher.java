package gov.scot.payments.testing.matchers;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.math.BigDecimal;

public class BigDecimalMatcher extends TypeSafeMatcher<BigDecimal> {

    private final BigDecimal firstValue;

    private BigDecimalMatcher(BigDecimal firstValue) {
        this.firstValue = firstValue;
    }

    @Override
    protected boolean matchesSafely(BigDecimal secondValue) {
        return firstValue.compareTo(secondValue) == 0;
    }

    @Override
    public void describeTo(Description description) {
        description.appendValue(firstValue);
    }

    public static Matcher<BigDecimal> equalTo(BigDecimal firstValue) {
        return new BigDecimalMatcher(firstValue);
    }
}
