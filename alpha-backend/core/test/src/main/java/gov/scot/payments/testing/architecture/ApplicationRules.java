package gov.scot.payments.testing.architecture;

import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import gov.scot.payments.model.Message;
import gov.scot.payments.model.MessageType;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

public class ApplicationRules {

    @ArchTest
    public static final ArchRule allClassesInAppPackage = classes()
            .should().resideInAPackage("gov.scot.payments..app..");

    @ArchTest
    public static final ArchRule noMessageTypesDefined = noClasses()
            .should().beAnnotatedWith(MessageType.class);

}
