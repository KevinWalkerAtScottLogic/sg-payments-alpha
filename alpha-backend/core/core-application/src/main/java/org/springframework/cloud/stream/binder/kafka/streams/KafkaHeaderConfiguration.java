package org.springframework.cloud.stream.binder.kafka.streams;

import gov.scot.payments.model.HasCause;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.Message;
import io.vavr.collection.HashMap;
import org.springframework.cloud.stream.binder.kafka.streams.properties.KafkaStreamsBinderConfigurationProperties;
import org.springframework.cloud.stream.converter.CompositeMessageConverterFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.kafka.support.DefaultKafkaHeaderMapper;
import org.springframework.kafka.support.KafkaHeaderMapper;
import org.springframework.messaging.MessageHeaders;

@Configuration
public class KafkaHeaderConfiguration {

    @Bean
    public KafkaHeaderMapper headerMapper(){
        final DefaultKafkaHeaderMapper mapper = new DefaultKafkaHeaderMapper("!" + MessageHeaders.ID,
                "!" + MessageHeaders.TIMESTAMP,
                "!scst_partition",
                "*");
        final HashMap<String, Boolean> stringHeaders = HashMap
                .of(Message.CONTEXT_HEADER, true
                        , Message.TYPE_HEADER, true
                        , HasKey.PARTITION_KEY_HEADER,true
                        , HasCause.CORRELATION_ID_HEADER,true
                        , HasCause.EXECUTION_COUNT_HEADER,true
                        , MessageHeaders.CONTENT_TYPE,true);
        mapper.setRawMappedHeaders(stringHeaders.toJavaMap());
        return mapper;
    }

    @Bean
    @Primary
    public WrappedKafkaStreamsBindingInformationCatalogue wrappedKafkaStreamsBindingInformationCatalogue(KafkaStreamsBindingInformationCatalogue kafkaStreamsBindingInformationCatalogue){
        return new WrappedKafkaStreamsBindingInformationCatalogue();
    }

    @Bean
    @Primary
    public KafkaStreamsMessageConversionDelegate headerMessageConversionDelegate(CompositeMessageConverterFactory compositeMessageConverterFactory,
                                                                                 SendToDlqAndContinue sendToDlqAndContinue,
                                                                                 WrappedKafkaStreamsBindingInformationCatalogue wrappedKafkaStreamsBindingInformationCatalogue,
                                                                                 KafkaStreamsBinderConfigurationProperties binderConfigurationProperties,
                                                                                 KafkaHeaderMapper headerMapper) {
        return new HeaderPropogatingKafkaStreamsMessageConversionDelegate(compositeMessageConverterFactory, sendToDlqAndContinue,
                wrappedKafkaStreamsBindingInformationCatalogue, binderConfigurationProperties,headerMapper);
    }

}
