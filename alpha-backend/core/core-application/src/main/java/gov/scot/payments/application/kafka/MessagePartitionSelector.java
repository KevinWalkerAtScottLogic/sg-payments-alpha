package gov.scot.payments.application.kafka;

import org.springframework.cloud.stream.binder.PartitionSelectorStrategy;

public class MessagePartitionSelector implements PartitionSelectorStrategy {

    @Override
    public int selectPartition(Object key, int partitionCount) {
        int hashCode = key.hashCode();
        if (hashCode == Integer.MIN_VALUE) {
            hashCode = 0;
        }
        return Math.abs(hashCode);
    }
}
