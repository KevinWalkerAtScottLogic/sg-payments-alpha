package gov.scot.payments.application.kafka;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.processor.ProcessorContext;

import java.util.function.Predicate;

@RequiredArgsConstructor
public class HeaderFilter<T> implements ValueTransformer<T,T> {

    private ProcessorContext context;
    private final Predicate<Headers> filter;

    @Override
    public void init(final ProcessorContext context) {
        this.context = context;
    }

    @Override
    public T transform(final T value) {
        Headers headers = context.headers();
        if(filter.test(headers)){
            return value;
        }
        return null;
    }

    @Override
    public void close() {

    }
}
