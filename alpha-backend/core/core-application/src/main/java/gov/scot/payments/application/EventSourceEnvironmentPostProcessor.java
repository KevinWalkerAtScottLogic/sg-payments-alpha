package gov.scot.payments.application;

import io.vavr.collection.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.util.StringUtils;

import java.util.Map;

@Slf4j
public class EventSourceEnvironmentPostProcessor implements EnvironmentPostProcessor {

    private static boolean hasRun;

    @Override
    public synchronized void postProcessEnvironment(final ConfigurableEnvironment environment
            , final SpringApplication application) {
        String namespace = System.getenv("ENVIRONMENT");

        if(namespace != null && !hasRun){
            String sourceContexts = environment.getProperty("events.destinations");
            log.info("POST PROCESSING ENV - Source Contexts: {}",sourceContexts);
            String topics;
            if("*".equals(sourceContexts)){
                topics = String.format("<REGEX>%s-(.+)-events",namespace);
            } else {
                String[] contexts = StringUtils.commaDelimitedListToStringArray(sourceContexts);
                log.info("POST PROCESSING ENV - Contexts: {}",List.of(contexts));
                topics = List.of(contexts)
                        .map(c -> String.format("%s-%s-events",namespace,c))
                        .intersperse(",")
                        .foldLeft(new StringBuilder(), StringBuilder::append).toString();
            }
            log.info("POST PROCESSING ENV: {}",topics);
            environment.getPropertySources()
                    .addFirst(new MapPropertySource("eventContextResolver", Map.of("events.destinations",topics)));
        }
        hasRun = true;
    }
}
