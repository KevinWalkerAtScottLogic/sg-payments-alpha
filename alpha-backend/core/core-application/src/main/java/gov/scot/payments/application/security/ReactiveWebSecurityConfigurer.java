package gov.scot.payments.application.security;

import io.vavr.collection.List;
import lombok.Setter;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import reactor.core.publisher.Mono;

@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
public class ReactiveWebSecurityConfigurer implements BeanFactoryAware {

    @Setter
    private BeanFactory beanFactory;

    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        http.headers().frameOptions().disable()
            .and().formLogin().disable()
            .httpBasic().disable()
            .csrf().disable()
            .cors().configurationSource(corsConfigurationSource());

        ServerHttpSecurity.AuthorizeExchangeSpec registry
                = http.authorizeExchange()
                      .pathMatchers(HttpMethod.GET,"/actuator/health**").permitAll()
                      .pathMatchers("/resources/**").permitAll()
                        .pathMatchers("/webjars/**").permitAll();
        ((ListableBeanFactory)beanFactory).getBeansOfType(SecurityCustomizer.class)
                   .values()
                   .forEach(c -> c.accept(registry));

        registry.anyExchange().authenticated()
                .and().oauth2ResourceServer().jwt().jwtAuthenticationConverter(jwt -> Mono.just(new CognitoJwt(jwt).toAuthentication()));
        return http.build();
    }

    @Bean
    SecurityCustomizer unauthenticatedEndpoints(
            @Value("${api.endpoints.no-auth:}") String[] unauthenticatedEndpoints){
        return r -> List.of(unauthenticatedEndpoints).forEach(e -> r.pathMatchers(e).permitAll());
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration().applyPermitDefaultValues();
        config.setAllowedMethods(List.of(HttpMethod.GET, HttpMethod.PUT, HttpMethod.POST, HttpMethod.DELETE).map(Enum::name).toJavaList());
        config.setAllowCredentials(true);
        //config.setAllowedHeaders(List.of(HttpHeaders.ORIGIN,HttpHeaders.CONTENT_TYPE,HttpHeaders.AUTHORIZATION,HttpHeaders.ACCEPT,HttpHeaders.ACCEPT_LANGUAGE,HttpHeaders.CONTENT_LANGUAGE));
        source.registerCorsConfiguration("/**", config);
        return source;
    }
}
