package gov.scot.payments.application.security;

import org.springframework.security.config.web.server.ServerHttpSecurity;

import java.util.function.Consumer;

public interface SecurityCustomizer extends Consumer<ServerHttpSecurity.AuthorizeExchangeSpec> {
}
