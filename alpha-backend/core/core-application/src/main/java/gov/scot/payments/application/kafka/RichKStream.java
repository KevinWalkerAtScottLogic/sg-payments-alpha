package gov.scot.payments.application.kafka;

import io.vavr.Function1;
import io.vavr.Tuple2;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Delegate;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.processor.ProcessorContext;

import java.util.function.Function;
import java.util.function.Predicate;

@RequiredArgsConstructor
public class RichKStream<K,V> implements KStream<K,V> {

    @Delegate
    protected final KStream<K,V> delegate;

    public RichKStream<K,V> repartition(String topic
            ,Function1<KeyValueWithHeaders<K,V>,K> rekeyFunction
            , Produced<K,V> produced){
        return rich(richMap( kvh -> new Tuple2<>(rekeyFunction.apply(kvh),kvh.value))
                .through(topic,produced));
    }

    public <V1> RichKStream<K,V1> richMap(Function1<KeyValueWithHeaders<K, V>,Tuple2<K, V1>> func){
        return rich(delegate.transform(() -> new MappingTransformer<>(func)));
    }

    public <V1> RichKStream<K,V1> richMapValues(Function1<KeyValueWithHeaders<K, V>,V1> func){
        return rich(delegate.transformValues(() -> new MappingValueTransformer<>(func)));
    }

    public RichKStream<K,V> richFilter(Predicate<KeyValueWithHeaders<K, V>> filter){
        return rich(delegate.transformValues(() -> new RichFilter<>(filter))
                                         .filter((k,v) -> v != null));
    }

    public RichKStream<K,V> enrich(ValueTransformerWithKeySupplier<K,V, V> messageEnricher){
        return rich(delegate.transformValues(messageEnricher));
    }

    public <T> RichKStream<K,T> delegatedTransform(Function<KStream<K,V>, KStream<K,T>> transform){
        return rich(transform.apply(this));
    }

    @SuppressWarnings("unchecked")
    public KStream<K,V> unwrap(){
        if(delegate instanceof RichKStream){
            return ((RichKStream)delegate).unwrap();
        }
        return delegate;
    }

    public static <K,V> RichKStream<K,V> rich(KStream<K,V> stream){
        return new RichKStream<>(stream);
    }


    @RequiredArgsConstructor
    private class MappingTransformer<V1> implements Transformer<K, V, KeyValue<K, V1>> {

        private ProcessorContext context;
        private final Function1<KeyValueWithHeaders<K, V>,Tuple2<K, V1>> func;

        @Override
        public void init(final ProcessorContext context) {
            this.context = context;
        }

        @Override
        public KeyValue<K, V1> transform(final K key, final V value) {
            KeyValueWithHeaders<K,V> v = new KeyValueWithHeaders<>(key,value,context.timestamp(),context.headers());
            Tuple2<K,V1> ret = func.apply(v);
            return new KeyValue<>(ret._1,ret._2);
        }

        @Override
        public void close() {

        }
    }

    @RequiredArgsConstructor
    private class MappingValueTransformer<V1> implements ValueTransformerWithKey<K, V, V1> {

        private ProcessorContext context;
        private final Function1<KeyValueWithHeaders<K, V>,V1> func;

        @Override
        public void init(final ProcessorContext context) {
            this.context = context;
        }

        @Override
        public V1 transform(final K key, final V value) {
            KeyValueWithHeaders<K,V> v = new KeyValueWithHeaders<>(key,value,context.timestamp(),context.headers());
            return func.apply(v);
        }

        @Override
        public void close() {

        }
    }
}
