package gov.scot.payments.application.kafka;

import gov.scot.payments.model.Message;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.ValueTransformerWithKeySupplier;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.springframework.messaging.MessageHeaders;

import java.util.function.Consumer;
import java.util.function.Function;

public class MessageKStream<V extends Message> extends RichKStream<byte[],V> {

    public MessageKStream(final KStream<byte[], V> delegate) {
        super(delegate);
    }

    public RichKStream<byte[],MessageWithInfo<V>> embedHeaders(){
        return rich(delegate)
                .richMapValues(kvh -> new MessageWithInfo<>(kvh.value, MessageInfo.fromHeaders(kvh.headers)));
    }

    public RichKStream<byte[],MessageWithInfo<V>> embedHeaders(ValueTransformerWithKeySupplier<byte[],V,V> enricher){
        return rich(delegate)
                .enrich(enricher)
                .richMapValues(kvh -> new MessageWithInfo<>(kvh.value,MessageInfo.fromHeaders(kvh.headers)));
    }

    public MessageKStream<V> deduplicate(MessageDuplicateFilterFactory<V> duplicateFilterFactory){
        return messageStream(delegate.transformValues(duplicateFilterFactory,duplicateFilterFactory.getStateStore())
                                     .filter((k,v) -> v != null));
    }

    public <T> KStream<byte[],T> wrap(MessageDuplicateFilterFactory<V> duplicateFilterFactory
            , Function<KStream<byte[],V>, KStream<byte[],T>> transform
            , ValueTransformerWithKeySupplier<byte[],T, T> messageEnricher){
        return deduplicate(duplicateFilterFactory)
                .delegatedTransform(transform)
                .enrich(messageEnricher)
                .unwrap();
    }

    public <T> void consume(MessageDuplicateFilterFactory<V> duplicateFilterFactory
            , Consumer<KStream<byte[],V> > transform){
         transform.accept(deduplicate(duplicateFilterFactory));
    }

    public static <V extends Message> MessageKStream<V> messageStream(KStream<byte[],V> stream){
        return new MessageKStream<>(stream);
    }

}
