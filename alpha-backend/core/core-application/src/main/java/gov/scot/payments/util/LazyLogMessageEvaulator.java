package gov.scot.payments.util;

import io.vavr.Lazy;
import io.vavr.control.Option;

import java.util.function.Supplier;

public class LazyLogMessageEvaulator {

    private final Lazy<String> supplier;

    public LazyLogMessageEvaulator(Supplier<?> supplier) {
        this.supplier = Lazy.of(() -> Option.of(supplier.get()).map(Object::toString).getOrNull());
    }

    @Override
    public String toString() {
        return supplier.get();
    }

    public static LazyLogMessageEvaulator msg(Supplier<?> supplier) {
        return new LazyLogMessageEvaulator(supplier);
    }
}
