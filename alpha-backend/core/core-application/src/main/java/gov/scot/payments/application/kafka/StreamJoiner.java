package gov.scot.payments.application.kafka;

import gov.scot.payments.model.Message;
import io.vavr.Function1;
import io.vavr.Function2;
import lombok.Builder;
import lombok.NonNull;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.WindowBytesStoreSupplier;

import java.time.Duration;
import java.util.function.Function;

import static gov.scot.payments.application.kafka.MessageKStream.messageStream;
import static gov.scot.payments.application.kafka.RichKStream.rich;

@Builder
public class StreamJoiner<T extends Message,T1 extends Message,V> {

    @NonNull private final String repartitionTopicName;
    @NonNull private final Function1<KeyValueWithHeaders<byte[],T1>,byte[]> rekeyFunction;
    @NonNull private final Duration joinWindow;
    @NonNull private final Function2<T,T1,V> joiner;
    @NonNull private final Serde<T> leftValueSerde;
    @NonNull private final Serde<T1> rightValueSerde;
    @NonNull private final Serde<SerializedMessage> stateSerde;
    @NonNull private final Function2<String,JoinWindows,WindowBytesStoreSupplier> storeSupplier;

    public KStream<byte[],V> join(KStream<byte[],T> left, KStream<byte[],T1> right){

        Produced<byte[],T1> produced = Produced.with(Serdes.ByteArray(), rightValueSerde);
        KStream<byte[],T1> repartitioned = rich(right)
                .repartition(repartitionTopicName,rekeyFunction, produced)
                .unwrap();
        KStream<byte[],SerializedMessage> serializedRight = messageStream(repartitioned)
                .embedHeaders()
                .unwrap()
                .mapValues(v -> v.serialize(rightValueSerde));
        KStream<byte[],SerializedMessage> serializedLeft = messageStream(left)
                .embedHeaders()
                .unwrap()
                .mapValues(v -> v.serialize(leftValueSerde));

        JoinWindows joinWindows = JoinWindows.of(joinWindow);
        return serializedLeft
                .leftJoin(serializedRight
                        , this::doJoin
                        , joinWindows
                        , StreamJoined.with(Serdes.ByteArray(),stateSerde,stateSerde)
                            .withOtherStoreSupplier(storeSupplier.apply("other",joinWindows))
                            .withThisStoreSupplier(storeSupplier.apply("this",joinWindows)));
    }

    private V doJoin(SerializedMessage l, SerializedMessage r) {
        T left =  l == null ? null : l.deseralize(leftValueSerde);
        T1 right = r == null ? null : r.deseralize(rightValueSerde);
        return joiner.apply(left,right);
    }

}
