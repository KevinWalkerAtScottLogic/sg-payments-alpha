package gov.scot.payments.application.kafka;

import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Message;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.ValueTransformerWithKey;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.WindowStore;
import org.apache.kafka.streams.state.WindowStoreIterator;

import java.time.Duration;

import static gov.scot.payments.util.LazyLogMessageEvaulator.msg;

@Slf4j
public class MessageDuplicateFilter<T extends Message> implements ValueTransformerWithKey<byte[], T, T> {

    private ProcessorContext processorContext;
    private WindowStore<String, Long> eventIdStore;
    private final long leftDurationMs;
    private final long rightDurationMs;
    private final String storeName;
    private final Metrics metrics;

    public MessageDuplicateFilter(Duration duplicateCheckWindow, String storeName,Metrics metrics){
        this.storeName = storeName;
        leftDurationMs = duplicateCheckWindow.toMillis() / 2;
        rightDurationMs = duplicateCheckWindow.toMillis() - leftDurationMs;
        this.metrics = metrics;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void init(final ProcessorContext context) {
        this.processorContext = context;
        eventIdStore = (WindowStore<String, Long>) context.getStateStore(storeName);

    }

    //TODO: this sill update the state store with the event before performing furhter processing, so if this fails then reply of that event later
    //will fail since it will be marked as a duplicate. Should we move updating the duplicate store to the end of the processing pipeline?
    //in which case we could not guarantee that we would not get a duplicate
    @Override
    public T transform(final byte[] readOnlyKey, final T value) {
        final String eventId = value.getMessageId().toString();
        if (eventId == null) {
            return value;
        } else {
            final T output;
            if (isDuplicate(eventId)) {
                log.warn("Event {} is a duplicate within the last {}, dropping",eventId,msg(() -> Duration.ofMillis(leftDurationMs * 2)));
                output = null;
                updateTimestampOfExistingEventToPreventExpiry(eventId, processorContext.timestamp());
                metrics.increment("duplicate.count");
            } else {
                output = value;
                rememberNewEvent(eventId, processorContext.timestamp());
            }
            return output;
        }
    }

    private boolean isDuplicate(final String eventId) {
        final long eventTime = processorContext.timestamp();
        final WindowStoreIterator<Long> timeIterator = eventIdStore.fetch(
                eventId,
                eventTime - leftDurationMs,
                eventTime + rightDurationMs);
        final boolean isDuplicate = timeIterator.hasNext();
        timeIterator.close();
        return isDuplicate;
    }

    private void updateTimestampOfExistingEventToPreventExpiry(final String eventId, final long newTimestamp) {
        eventIdStore.put(eventId, newTimestamp, newTimestamp);
    }

    private void rememberNewEvent(final String eventId, final long timestamp) {
        eventIdStore.put(eventId, timestamp, timestamp);
    }

    @Override
    public void close() {

    }
}
