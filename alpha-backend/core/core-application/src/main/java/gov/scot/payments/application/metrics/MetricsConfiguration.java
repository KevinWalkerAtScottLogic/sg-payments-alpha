package gov.scot.payments.application.metrics;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.distribution.pause.ClockDriftPauseDetector;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.management.MBeanServer;
import java.time.Duration;
import java.util.Collections;

@Configuration
public class MetricsConfiguration {

    @Bean
    MeterRegistryCustomizer<MeterRegistry> customizeGCPauseOffset() {
        return registry -> registry.config().pauseDetector(new ClockDriftPauseDetector(Duration.ofMillis(100), Duration.ofMillis(100)));
    }

    @Bean
    @ConditionalOnBean(MBeanServer.class)
    public KafkaStreamsMetrics kafkaStreamsMetrics(MBeanServer mbeanServer) {
        return new KafkaStreamsMetrics(mbeanServer, Collections.emptyList());
    }

    @Bean
    public Metrics metrics(MeterRegistry registry){
        return new MicrometerMetrics(registry);
    }
}
