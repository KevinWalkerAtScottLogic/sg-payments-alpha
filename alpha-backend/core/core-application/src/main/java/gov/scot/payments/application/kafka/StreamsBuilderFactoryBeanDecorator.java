package gov.scot.payments.application.kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.StreamsBuilder;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.core.env.Environment;
import org.springframework.kafka.config.KafkaStreamsConfiguration;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;
import org.springframework.kafka.core.CleanupConfig;
import org.springframework.util.Assert;

@Slf4j
public class StreamsBuilderFactoryBeanDecorator extends StreamsBuilderFactoryBean {

    private final BeanFactory beanFactory;

    public StreamsBuilderFactoryBeanDecorator(KafkaStreamsConfiguration streamsConfig
            , CleanupConfig cleanupConfig
            ,BeanFactory beanFactory) {
        super(streamsConfig, cleanupConfig);
        this.beanFactory = beanFactory;
    }

    @Override
    protected StreamsBuilder createInstance() {
        if (this.isAutoStartup()) {
            Assert.state(this.getStreamsConfiguration() != null,
                    "'streams configuration properties must not be null");
        }
        return new StreamsBuilderPropertiesOverridingDecorator(beanFactory);
    }

}
