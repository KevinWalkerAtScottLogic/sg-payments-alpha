package gov.scot.payments.application.metrics;

import io.vavr.collection.HashMap;
import io.vavr.collection.Map;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;

public interface Metrics {

    void count(String name, Map<String,String> tags, double value);

    default void increment(String name) {
        count(name, 1);
    }

    default void increment(String name, Map<String,String> tags){
        count(name, tags,1);
    }

    default void count(String name, double value){
        count(name, HashMap.empty(),value);
    }

    default <T,K> Function<T,K> time(String name, Map<String,String> tags, Function<T,K> func){
        return arg -> time(name, tags, () -> func.apply(arg)).get();
    }

    void time(String name, Map<String,String> tags, Runnable runnable);

    default void time(String name, Runnable runnable) {
        time(name,HashMap.empty(),runnable);
    }

    <T> Supplier<T> time(String name, Map<String,String> tags, Supplier<T> supplier);

    default <T> Supplier<T> time(String name, Supplier<T> supplier){
        return time(name,HashMap.empty(),supplier);
    }

    default <T> T execute(String name, Supplier<T> supplier){
        return time(name,HashMap.empty(),supplier).get();
    }

    default <T,K> Function<T,K> time(String name, Function<T,K> func){
        return time(name,HashMap.empty(),func);
    }

    <T> T gauged(String name, Map<String,String> tags, T obj, ToDoubleFunction<T> valueExtractor);

    default <T> T gauged(String name, T obj, ToDoubleFunction<T> valueExtractor){
        return gauged(name,HashMap.empty(), obj, valueExtractor);
    }

}
