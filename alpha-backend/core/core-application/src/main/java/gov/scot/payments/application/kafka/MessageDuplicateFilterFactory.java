package gov.scot.payments.application.kafka;

import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Message;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.ValueTransformerWithKey;
import org.apache.kafka.streams.kstream.ValueTransformerWithKeySupplier;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;
import org.apache.kafka.streams.state.WindowBytesStoreSupplier;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;

import java.time.Duration;

@Slf4j
public class MessageDuplicateFilterFactory<V extends Message> implements ValueTransformerWithKeySupplier<byte[],V,V> {

    //need to be able to pass in in mem / persistent store

    private final Duration duplicateWindow;
    @Getter private final String stateStore;
    private final Metrics metrics;
    private final WindowBytesStoreSupplier store;

    public MessageDuplicateFilterFactory(Duration duplicateWindow, String stateStore, Metrics metrics, WindowBytesStoreSupplier store){
        this.duplicateWindow = duplicateWindow;
        this.stateStore = stateStore;
        this.metrics = metrics;
        this.store = store;
    }

    public void registerStateStore(BeanFactory beanFactory,String methodName) throws Exception {
        String factoryName = String.format("&stream-builder-%s",methodName);
        StreamsBuilder streamsBuilder = beanFactory.getBean(factoryName,StreamsBuilderFactoryBean.class).getObject();
        log.info("Registering state store: {} for stream factory: {}",stateStore,factoryName);
        registerStateStore(streamsBuilder);
    }

    private void registerStateStore(final StreamsBuilder streamsBuilder) {
        StoreBuilder builder = Stores.windowStoreBuilder(store, Serdes.String(), Serdes.Long());
        streamsBuilder.addStateStore(builder);
    }

    @Override
    public ValueTransformerWithKey<byte[], V, V> get() {
        return new MessageDuplicateFilter<>(duplicateWindow,stateStore,metrics);
    }
}
