package gov.scot.payments.application.kafka.serializer;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class ReflectionAvroSerde<T> implements Serde<T> {

    private final Serde<T> inner;

    public ReflectionAvroSerde(final SchemaRegistryClient client, Class<T> type) {
        if (client == null) {
            throw new IllegalArgumentException("schema registry client must not be null");
        }
        inner = Serdes.serdeFrom(
                new ReflectionAvroSerializer<>(client),
                new ReflectionAvroDeserializer<>(client, type));
    }

    @Override
    public Serializer<T> serializer() {
        return inner.serializer();
    }

    @Override
    public Deserializer<T> deserializer() {
        return inner.deserializer();
    }

    @Override
    public void configure(final Map<String, ?> serdeConfig, final boolean isSerdeForRecordKeys) {
        inner.serializer().configure(serdeConfig, isSerdeForRecordKeys);
        inner.deserializer().configure(serdeConfig, isSerdeForRecordKeys);
    }

    @Override
    public void close() {
        inner.serializer().close();
        inner.deserializer().close();
    }
}