package gov.scot.payments.application.func;

import io.vavr.*;
import lombok.experimental.UtilityClass;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

import static io.vavr.API.$;
import static io.vavr.API.Case;
import static io.vavr.Patterns.$Tuple2;
import static io.vavr.Patterns.$Tuple3;
import static io.vavr.Patterns.$Tuple4;
import static io.vavr.Predicates.instanceOf;

@UtilityClass
public class Matchers {

    public static <T1, T2, _1 extends T1, _2 extends T2,K> Matcher<Tuple2<T1,T2>, K> function2(Predicate<_1> p1, Predicate<_2> p2, Function2<_1,_2,K> func) {
        API.Match.Pattern2<Tuple2<T1, T2>, _1, _2> pattern = $Tuple2($(p1),$(p2));
        return () -> Case(pattern, func);
    }

    public static <T1, T2, _1 extends T1, _2 extends T2,K> Matcher<Tuple2<T1,T2>, K> function2(Predicate<_1> p1, Function2<_1,_2,K> func) {
        return function2(p1,t -> true,func);
    }

    public static <T1, T2, _1 extends T1, _2 extends T2,K> Matcher<Tuple2<T1,T2>, K> function2(Class<_1> type, Function2<_1,_2,K> func) {
        return function2(instanceOf(type),func);
    }

    public static <T1, T2,T3, _1 extends T1, _2 extends T2, _3 extends T3,K> Matcher<Tuple3<T1,T2,T3>, K> function3(Predicate<_1> p1, Predicate<_2> p2, Predicate<_3> p3, Function3<_1,_2,_3,K> func) {
        API.Match.Pattern3<Tuple3<T1, T2,T3>, _1, _2,_3> pattern = $Tuple3($(p1),$(p2),$(p3));
        return () -> Case(pattern, func);
    }

    public static <T1, T2,T3,T4, _1 extends T1, _2 extends T2, _3 extends T3, _4 extends T4,K> Matcher<Tuple4<T1,T2,T3,T4>, K> function4(Predicate<_1> p1, Predicate<_2> p2, Predicate<_3> p3, Predicate<_4> p4, Function4<_1,_2,_3,_4,K> func) {
        API.Match.Pattern4<Tuple4<T1, T2,T3,T4>, _1, _2,_3,_4> pattern = $Tuple4($(p1),$(p2),$(p3),$(p4));
        return () -> Case(pattern, func);
    }

    //tuple predicates
    public static <T1,T2> Predicate<Tuple2<T1,T2>> test(BiPredicate<T1,T2> predicate){
        return t -> predicate.test(t._1,t._2);
    }

    public static <T1,T2> Predicate<Tuple2<T1,T2>> test1(Predicate<T1> predicate){
        return t -> predicate.test(t._1);
    }

    public static <T1,T2> Predicate<Tuple2<T1,T2>> test2(Predicate<T2> predicate){
        return t -> predicate.test(t._2);
    }

    public static <T1,T2,_1 extends T1> Predicate<Tuple2<T1,T2>> test1(Class<_1> predicate){
        return test1(instanceOf(predicate));
    }

}
