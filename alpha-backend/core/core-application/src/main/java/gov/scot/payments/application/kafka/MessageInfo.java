package gov.scot.payments.application.kafka;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Charsets;
import gov.scot.payments.model.Message;
import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.avro.reflect.Nullable;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.springframework.messaging.MessageHeaders;


@Getter
@NoArgsConstructor
@Builder
@ToString
public class MessageInfo {

    @Nullable private String context;
    @Nullable private String type;
    @Nullable private String contentType;

    @JsonCreator
    public MessageInfo(@JsonProperty("context") final String context
            , @JsonProperty("type") final String type
            , @JsonProperty("contentType") final String contentType) {
        this.context = context;
        this.type = type;
        this.contentType = contentType;
    }

    public Headers toHeaders(){
        Option<Header> context = toHeader(Message.CONTEXT_HEADER,this.context);
        Option<Header> type = toHeader(Message.TYPE_HEADER,this.type);
        Option<Header> contentType = toHeader(MessageHeaders.CONTENT_TYPE,this.contentType);
        return new RecordHeaders(List.of(context,type,contentType).filter(Option::isDefined).map(Option::get));
    }

    private Option<Header> toHeader(final String key, final String value) {
        return Option.of(value)
              .map(v -> value.getBytes(Charsets.UTF_8))
              .map(v -> new RecordHeader(key,v));
    }

    public static MessageInfo fromHeaders(Headers headers){
        String type = getHeader(headers, Message.TYPE_HEADER);
        String context = getHeader(headers, Message.CONTEXT_HEADER);
        String contentType = getHeader(headers, MessageHeaders.CONTENT_TYPE);
        return new MessageInfo(context,type,contentType);
    }

    private static String getHeader(final Headers headers, final String contextHeader) {
        return Option.of(headers.lastHeader(contextHeader))
                     .map(h -> new String(h.value()))
                     .getOrNull();
    }
}
