package gov.scot.payments.application;

import gov.scot.payments.application.kafka.MessageDuplicateFilterFactory;
import gov.scot.payments.application.kafka.RichKStream;
import gov.scot.payments.model.Message;
import lombok.Builder;
import lombok.NonNull;
import lombok.Setter;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.ValueTransformerWithKeySupplier;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;

import java.util.function.Consumer;
import java.util.function.Function;

import static gov.scot.payments.application.kafka.MessageKStream.messageStream;

@Builder
public class MessageDeDuplicator<Input extends Message,Output> implements BeanFactoryAware {

    @NonNull private final ValueTransformerWithKeySupplier<byte[],Output, Output> messageEnricher;
    @NonNull private final MessageDuplicateFilterFactory<Input> duplicateFilterFactory;

    @Setter private BeanFactory beanFactory;

    public final KStream<byte[],Output> deduplicate(KStream<byte[],Input> stream
            , Function<KStream<byte[],Input>, KStream<byte[],Output>> transform, String method) throws Exception {
        duplicateFilterFactory.registerStateStore(beanFactory,method);
        return messageStream(stream)
                .wrap(duplicateFilterFactory,transform,messageEnricher);
    }

    public final void deduplicateAndConsume(KStream<byte[],Input> stream
            , Consumer<KStream<byte[],Input>> transform, String method) throws Exception {
        duplicateFilterFactory.registerStateStore(beanFactory,method);
        messageStream(stream)
                .consume(duplicateFilterFactory,transform);
    }
}
