package gov.scot.payments.application.kafka.serializer;

import gov.scot.payments.avro.VavrDatumReader;
import gov.scot.payments.avro.VavrReflectData;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;
import io.confluent.kafka.serializers.AbstractKafkaAvroDeserializer;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.subject.strategy.SubjectNameStrategy;
import io.vavr.control.Option;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.reflect.ReflectData;
import org.apache.avro.reflect.ReflectDatumReader;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Map;

public class ReflectionAvroDeserializer<T> extends AbstractKafkaAvroDeserializer implements Deserializer<T> {

    private final Schema schema;

    ReflectionAvroDeserializer(final SchemaRegistryClient client, Class<T> type) {
        this.schema = VavrReflectData.get().getSchema(type);
        this.schemaRegistry = client;
    }

    @Override
    public void configure(final Map<String, ?> configs, final boolean isKey) {
        Option.of(configs.get(AbstractKafkaAvroSerDeConfig.VALUE_SUBJECT_NAME_STRATEGY))
              .map(s -> (SubjectNameStrategy)s)
              .forEach(s -> this.valueSubjectNameStrategy = s);
    }

    @SuppressWarnings("unchecked")
    @Override
    public T deserialize(final String topic, final byte[] data) {
        if (data == null) {
            return null;
        }
        DeserializationContext context = new DeserializationContext(data,topic);
        Schema writerSchema = context.schemaFromRegistry();
        return (T)context.read(writerSchema, schema);
    }

    private ByteBuffer getByteBuffer(byte[] payload) {
        ByteBuffer buffer = ByteBuffer.wrap(payload);
        if (buffer.get() != MAGIC_BYTE) {
            throw new SerializationException("Unknown magic byte!");
        }
        return buffer;
    }

    private DatumReader<?> getDatumReader(Schema writerSchema, Schema readerSchema) {
        boolean writerSchemaIsPrimitive = ReflectionAvroSerializer.getPrimitiveSchemas().containsValue(writerSchema);
        // do not use SpecificDatumReader if writerSchema is a primitive
        if (!writerSchemaIsPrimitive) {
            if (readerSchema == null) {
                throw new SerializationException(
                        "Reader schema cannot be null when using Avro schema reflection");
            }
            return VavrReflectData.get().createDatumReader(writerSchema, readerSchema);
        } else {
            if (readerSchema == null) {
                return new GenericDatumReader<>(writerSchema);
            }
            return new GenericDatumReader<>(writerSchema, readerSchema);
        }
    }

    class DeserializationContext {
        private final ByteBuffer buffer;
        private final int schemaId;
        private final String topic;

        DeserializationContext(final byte[] payload,String topic) {
            this.buffer = getByteBuffer(payload);
            this.schemaId = buffer.getInt();
            this.topic = topic;
        }

        Schema schemaFromRegistry() {
            try {
                String subject = getSubjectName(topic,false,new Object(),schema);
                return schemaRegistry.getBySubjectAndId(subject,schemaId);
            } catch (RestClientException | IOException e) {
                throw new SerializationException("Error retrieving Avro schema for id " + schemaId, e);
            }
        }

        Object read(Schema writerSchema, Schema readerSchema) {
            DatumReader<?> reader = getDatumReader(writerSchema, readerSchema);
            int length = buffer.limit() - 1 - idSize;
            if (writerSchema.getType().equals(Schema.Type.BYTES)) {
                byte[] bytes = new byte[length];
                buffer.get(bytes, 0, length);
                return bytes;
            } else {
                int start = buffer.position() + buffer.arrayOffset();
                try {
                    Object result = reader.read(null, DecoderFactory.get().binaryDecoder(buffer.array(),
                            start, length, null));
                    if (writerSchema.getType().equals(Schema.Type.STRING)) {
                        return result.toString();
                    } else {
                        return result;
                    }
                } catch (IOException | RuntimeException e) {
                    // avro deserialization may throw AvroRuntimeException, NullPointerException, etc
                    throw new SerializationException("Error deserializing Avro message for id "
                            + schemaId, e);
                }
            }
        }
    }
}
