package gov.scot.payments.application.metrics;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Timer;
import io.vavr.collection.Map;
import lombok.RequiredArgsConstructor;

import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;

@RequiredArgsConstructor
public class MicrometerMetrics implements Metrics {

    private final MeterRegistry registry;

    @Override
    public void count(String name, Map<String, String> tags, double value) {
        Counter counter = registry.counter(name,kvToTags(tags));
        counter.increment(value);
    }

    @Override
    public void time(String name, Map<String, String> tags, Runnable runnable) {
        Timer metric = registry.timer(name,kvToTags(tags));
        metric.record(runnable);
    }

    @Override
    public <T> Supplier<T> time(String name, Map<String, String> tags, Supplier<T> supplier) {
        Timer metric = registry.timer(name,kvToTags(tags));
        return metric.wrap(supplier);
    }

    @Override
    public <T> T gauged(String name, Map<String, String> tags, T obj, ToDoubleFunction<T> valueExtractor) {
       return registry.gauge(name, kvToTags(tags),obj,valueExtractor);
    }

    private Iterable<Tag> kvToTags(Map<String, String> tags) {
        return tags.map( t -> Tag.of(t._1,t._2));
    }
}
