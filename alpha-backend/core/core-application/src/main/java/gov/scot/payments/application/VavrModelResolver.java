package gov.scot.payments.application;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.fasterxml.jackson.databind.type.SimpleType;
import io.swagger.v3.core.converter.AnnotatedType;
import io.swagger.v3.core.converter.ModelConverter;
import io.swagger.v3.core.converter.ModelConverterContext;
import io.swagger.v3.core.jackson.ModelResolver;
import io.swagger.v3.oas.models.media.Schema;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.collection.Traversable;
import lombok.Data;
import org.javamoney.moneta.Money;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Iterator;

public class VavrModelResolver extends ModelResolver {
    public VavrModelResolver(final ObjectMapper mapper) {
        super(mapper);
    }

    @Override
    protected boolean _isOptionalType(final JavaType propType) {
        return super._isOptionalType(propType) || "io.vavr.Option".equals(propType.getRawClass().getCanonicalName());
    }

    @Override
    protected boolean _isSetType(final Class<?> cls) {
        return super._isSetType(cls) || Set.class.isAssignableFrom(cls);
    }

    @Override
    public Schema resolve(final AnnotatedType annotatedType, final ModelConverterContext context, final Iterator<ModelConverter> next) {
        AnnotatedType typeToUse = annotatedType;
        if(annotatedType.getType() instanceof SimpleType && ((SimpleType)annotatedType.getType()).getRawClass() == Money.class){
            typeToUse.setType(MoneyType.class);
        } else if(annotatedType.getType() instanceof CollectionLikeType){
            final CollectionLikeType type = (CollectionLikeType) annotatedType.getType();
            final Class<?> collectionType = type.getRawClass();
            if(List.class.isAssignableFrom(collectionType) || Set.class.isAssignableFrom(collectionType)){
                typeToUse.setType(CollectionLikeType.upgradeFrom(SimpleType.constructUnsafe(java.util.List.class),type.getContentType()));
            }
        }
        return super.resolve(typeToUse, context, next);
    }

    @Data
    private static class MoneyType {

        private String currency;
        private BigDecimal amount;
    }
}
