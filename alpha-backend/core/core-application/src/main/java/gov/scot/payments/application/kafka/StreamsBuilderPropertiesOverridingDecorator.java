package gov.scot.payments.application.kafka;

import gov.scot.payments.kafka.FilteringSerde;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.internals.ConsumedInternal;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.core.env.Environment;

import java.util.Collection;
import java.util.regex.Pattern;

@RequiredArgsConstructor
@Slf4j
public class StreamsBuilderPropertiesOverridingDecorator extends StreamsBuilder {

    private static final String REGEX_PREFIX = "<REGEX>";

    private final BeanFactory beanFactory;

    @Override
    public synchronized <K, V> KStream<K, V> stream(Collection<String> topics, Consumed<K, V> consumed) {
        log.info("applying filtering to {}",topics);
        if(topics.isEmpty()){
            return new StubKStream<>();
        }
        Consumed<K, V> filtered = applyFiltering(consumed);
        Pattern pattern = getPattern(topics);
        KStream<K, V> stream;
        if(pattern != null){
            stream =  stream(pattern,filtered);
        } else{
            stream =  super.stream(topics, filtered);
        }
        return stream.filter( (k,v) -> v != FilteringSerde.FILTERED_PLACEHOLDER);
    }

    @SuppressWarnings("unchecked")
    private <K, V> Consumed<K, V> applyFiltering(final Consumed<K, V> consumed) {
        ConsumedInternal<K,V> accessible = new ConsumedInternal<>(consumed);
        log.info("applying filtering to deserializer {}",accessible.valueDeserializer());
        if(accessible.valueDeserializer() != null && ByteArrayDeserializer.class.isAssignableFrom(accessible.valueDeserializer().getClass())){
            return Consumed.<K,V>as(accessible.name())
                    .withTimestampExtractor(accessible.timestampExtractor())
                           .withOffsetResetPolicy(accessible.offsetResetPolicy())
                    .withKeySerde(accessible.keySerde())
                    .withValueSerde((Serde<V>)new FilteringSerde(beanFactory,(Serializer<byte[]>) accessible.valueSerde().serializer()));
        }
        return consumed;
    }

    private Pattern getPattern(Collection<String> topics) {
        if(topics.size() == 1){
            String pattern = List.ofAll(topics).get(0);
            if(pattern.startsWith(REGEX_PREFIX)){
                pattern = pattern.substring(REGEX_PREFIX.length());
                return Pattern.compile(pattern);
            }
        }
        return null;
    }

}
