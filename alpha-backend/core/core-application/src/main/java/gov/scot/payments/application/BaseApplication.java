package gov.scot.payments.application;

import gov.scot.payments.application.kafka.HeaderPreservingCompositeNonNativeSerde;
import gov.scot.payments.application.kafka.KafkaStreamsOverridingContext;
import gov.scot.payments.application.kafka.MessageDuplicateFilterFactory;
import gov.scot.payments.application.kafka.MessageHeaderEnricher;
import gov.scot.payments.application.kafka.serializer.ReflectionAvroSerde;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Message;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.subject.TopicRecordNameStrategy;
import io.vavr.Function4;
import io.vavr.collection.HashMap;
import io.vavr.collection.HashSet;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import org.apache.kafka.streams.kstream.ValueTransformerWithKeySupplier;
import org.apache.kafka.streams.state.WindowBytesStoreSupplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.stream.converter.CompositeMessageConverterFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.support.KafkaHeaderMapper;
import org.springframework.util.StringUtils;

import java.time.Duration;

public abstract class BaseApplication<Input extends Message,Output> {

    public static void run(Class<?> app, String[] args) {
        SpringApplication application = new SpringApplication(app);
        application.setApplicationContextClass(KafkaStreamsOverridingContext.class);
        application.run(args);
    }

    @Autowired CompositeMessageConverterFactory compositeMessageConverterFactory;
    @Autowired KafkaHeaderMapper headerMapper;

    protected <T> ReflectionAvroSerde<T> stateSerde(SchemaRegistryClient client, Class<T> clazz){
        final ReflectionAvroSerde<T> serde = new ReflectionAvroSerde<>(client, clazz);
        java.util.Map<String,Object> config = new java.util.HashMap<>();
        config.put(AbstractKafkaAvroSerDeConfig.AUTO_REGISTER_SCHEMAS,true);
        config.put(AbstractKafkaAvroSerDeConfig.VALUE_SUBJECT_NAME_STRATEGY,new TopicRecordNameStrategy());
        serde.configure(config,false);
        return serde;
    }

    protected <T> HeaderPreservingCompositeNonNativeSerde<T> valueSerde(Class<T> clazz){
        HeaderPreservingCompositeNonNativeSerde<T> commandSerde = new HeaderPreservingCompositeNonNativeSerde<>(compositeMessageConverterFactory,headerMapper);
        Map<String, Object> commandConfig = HashMap.of("valueClass", clazz,"contentType", "application/*+avro");
        commandSerde.configure(commandConfig.toJavaMap(),false);
        return commandSerde;
    }

    @Bean
    public MessageDeDuplicator<Input,Output> deDuplicator(MessageDuplicateFilterFactory<Input> duplicateFilterFactory
        , ValueTransformerWithKeySupplier<byte[], Output,Output> eventHeaderEnricher){
        return MessageDeDuplicator.<Input,Output>builder()
                                  .duplicateFilterFactory(duplicateFilterFactory)
                                  .messageEnricher(eventHeaderEnricher)
                                  .build();
    }

    @Bean
    public MessageDuplicateFilterFactory<Input> duplicateFilterFactory(@Value("${duplicate.window}") Duration duplicateWindow
            , @Value("${spring.application.name}") String topic
            , Metrics metrics
            , Function4<String, Duration,Duration,Boolean, WindowBytesStoreSupplier> windowBytesStoreSupplier){
        String stateStore = String.format("%s-messageIds",topic);
        return new MessageDuplicateFilterFactory<>(duplicateWindow
                ,stateStore
                ,metrics
                ,windowBytesStoreSupplier.apply(stateStore,duplicateWindow,duplicateWindow,false));
    }

    @Bean
    public ValueTransformerWithKeySupplier<byte[], Output,Output> eventHeaderEnricher(@Value("${context.name}") String context
            ,MessageTypeRegistry registry){
        return () -> new MessageHeaderEnricher<>(context,registry::getInfo);
    }

}
