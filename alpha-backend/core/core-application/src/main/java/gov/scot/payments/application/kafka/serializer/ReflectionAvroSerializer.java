package gov.scot.payments.application.kafka.serializer;

import gov.scot.payments.avro.VavrDatumWriter;
import gov.scot.payments.avro.VavrReflectData;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerializer;
import io.confluent.kafka.serializers.subject.strategy.SubjectNameStrategy;
import io.vavr.control.Option;
import org.apache.avro.Schema;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.reflect.ReflectData;
import org.apache.avro.reflect.ReflectDatumWriter;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ReflectionAvroSerializer<T> extends AbstractKafkaAvroSerializer implements Serializer<T> {

    private static final Map<String, Schema> primitiveSchemas;

    static {
        Schema.Parser parser = new Schema.Parser();
        primitiveSchemas = new HashMap<>();
        primitiveSchemas.put("Null", createPrimitiveSchema(parser, "null"));
        primitiveSchemas.put("Boolean", createPrimitiveSchema(parser, "boolean"));
        primitiveSchemas.put("Integer", createPrimitiveSchema(parser, "int"));
        primitiveSchemas.put("Long", createPrimitiveSchema(parser, "long"));
        primitiveSchemas.put("Float", createPrimitiveSchema(parser, "float"));
        primitiveSchemas.put("Double", createPrimitiveSchema(parser, "double"));
        primitiveSchemas.put("String", createPrimitiveSchema(parser, "string"));
        primitiveSchemas.put("Bytes", createPrimitiveSchema(parser, "bytes"));
    }

    private static Schema createPrimitiveSchema(Schema.Parser parser, String type) {
        String schemaString = String.format("{\"type\" : \"%s\"}", type);
        return parser.parse(schemaString);
    }

    static Map<String, Schema> getPrimitiveSchemas() {
        return Collections.unmodifiableMap(primitiveSchemas);
    }

    public ReflectionAvroSerializer(SchemaRegistryClient client) {
        schemaRegistry = client;
    }

    @Override
    public void configure(final Map<String, ?> configs, final boolean isKey) {
        final Object autoRegisterSchema = configs.get(AbstractKafkaAvroSerDeConfig.AUTO_REGISTER_SCHEMAS);
        this.autoRegisterSchema = Option.of(autoRegisterSchema).map(b -> (Boolean) b).getOrElse(false);
        Option.of(configs.get(AbstractKafkaAvroSerDeConfig.VALUE_SUBJECT_NAME_STRATEGY))
              .map(s -> (SubjectNameStrategy)s)
              .forEach(s -> this.valueSubjectNameStrategy = s);
    }

    @Override
    public byte[] serialize(final String topic, final T object) {
        Schema schema = null;
        if (object == null) {
            return null;
        }
        String restClientErrorMsg = "";
        try {
            int id;
            schema = getSchema(object);
            String subject = getSubjectName(topic, false, object,schema);
            if (autoRegisterSchema) {
                restClientErrorMsg = "Error registering Avro schema: ";
                id = schemaRegistry.register(subject, schema);
            } else {
                restClientErrorMsg = "Error retrieving Avro schema: ";
                id = schemaRegistry.getId(subject, schema);
            }
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            out.write(MAGIC_BYTE);
            out.write(ByteBuffer.allocate(idSize).putInt(id).array());
            if (object instanceof byte[]) {
                out.write((byte[]) object);
            } else {
                BinaryEncoder encoder = EncoderFactory.get().directBinaryEncoder(out, null);
                DatumWriter<Object> writer = VavrReflectData.get().createDatumWriter(schema);
                writer.write(object, encoder);
                encoder.flush();
            }
            byte[] bytes = out.toByteArray();
            out.close();
            return bytes;
        } catch (IOException | RuntimeException e) {
            // avro serialization can throw AvroRuntimeException, NullPointerException,
            // ClassCastException, etc
            throw new SerializationException("Error serializing Avro message", e);
        } catch (RestClientException e) {
            throw new SerializationException(restClientErrorMsg + schema, e);
        }
    }

    private Schema getSchema(Object object) {
        if (object == null) {
            return primitiveSchemas.get("Null");
        } else if (object instanceof Boolean) {
            return primitiveSchemas.get("Boolean");
        } else if (object instanceof Integer) {
            return primitiveSchemas.get("Integer");
        } else if (object instanceof Long) {
            return primitiveSchemas.get("Long");
        } else if (object instanceof Float) {
            return primitiveSchemas.get("Float");
        } else if (object instanceof Double) {
            return primitiveSchemas.get("Double");
        } else if (object instanceof CharSequence) {
            return primitiveSchemas.get("String");
        } else if (object instanceof byte[]) {
            return primitiveSchemas.get("Bytes");
        } else {
            Schema schema = VavrReflectData.get().getSchema(object.getClass());
            if (schema == null) {
                throw new SerializationException("Schema is null for object of class "
                        + object.getClass().getCanonicalName());
            } else {
                return schema;
            }
        }
    }

}
