package gov.scot.payments.application.kafka;

import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.Projection;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.binder.PartitionKeyExtractorStrategy;
import org.springframework.messaging.Message;

@Slf4j
public class MessagePartitionKeyExtractor implements PartitionKeyExtractorStrategy {

    @Override
    public Object extractKey(Message<?> message) {
        String key = message.getHeaders().get(HasKey.PARTITION_KEY_HEADER,String.class);
        if(key != null){
            log.debug("using partition key: {} extracted from headers",key);
           return key;
        }
        if(HasKey.class.isAssignableFrom(message.getPayload().getClass())){
            final Object messageKey = ((HasKey) message.getPayload()).getKey();
            log.debug("using partition key: {} extracted from message",messageKey);
            return messageKey;
        }
        if(Projection.class.isAssignableFrom(message.getPayload().getClass())){
            final Object projectionId = ((Projection) message.getPayload()).getId();
            log.debug("using partition key: {} extracted from projection",projectionId);
            return projectionId;
        }
        return null;
    }

}
