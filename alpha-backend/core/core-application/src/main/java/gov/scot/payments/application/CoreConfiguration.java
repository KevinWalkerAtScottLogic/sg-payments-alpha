package gov.scot.payments.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.application.kafka.KafkaConfiguration;
import gov.scot.payments.application.kafka.KafkaStreamsHealthIndicator;
import gov.scot.payments.application.metrics.MetricsConfiguration;
import gov.scot.payments.application.security.ReactiveWebSecurityConfigurer;
import io.swagger.v3.core.converter.ModelConverters;
import io.swagger.v3.core.jackson.ModelResolver;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.Scopes;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import io.vavr.jackson.datatype.VavrModule;
import org.springdoc.api.OpenApiCustomiser;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.SwaggerUiConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.ReactiveHealthIndicator;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.ResourceUtils;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.config.ResourceHandlerRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.reactive.resource.WebJarsResourceResolver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springdoc.core.Constants.API_DOCS_URL;
import static org.springdoc.core.Constants.SPRINGDOC_SWAGGER_UI_ENABLED;
import static org.springdoc.core.Constants.SWAGGGER_CONFIG_FILE;
import static org.springframework.util.AntPathMatcher.DEFAULT_PATH_SEPARATOR;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

@Configuration
@EnableWebFlux
@EnableScheduling
@EnableTransactionManagement
@EnableAutoConfiguration
@Import({MessageRegistryConfiguration.class
        , KafkaConfiguration.class
        , MetricsConfiguration.class
        , ReactiveWebSecurityConfigurer.class})
@PropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "application-core.properties")
public class CoreConfiguration {

    @Bean
    public OpenApiCustomiser apiDefinition(@Value("${component.name}") String component
            , @Value("${token.service.domain}") String tokenServer
            , @Value("${ingress.protocol}://${INGRESS_HOST}${ingress.path}") String hostPath
            , ObjectMapper mapper) {
        ModelConverters.getInstance().addConverter(new VavrModelResolver(mapper));
        return api -> {
            api.servers(List.of(new Server().url(hostPath)))
                    .info(new Info().title(component))
                    .security(List.of(new SecurityRequirement().addList("oauth2")));
            api.getComponents().addSecuritySchemes("oauth2",
                    new SecurityScheme()
                            .type(SecurityScheme.Type.OAUTH2)
                            .flows(new OAuthFlows()
                                    .clientCredentials(new OAuthFlow()
                                            .tokenUrl(tokenServer)
                                            .scopes(new Scopes()))));
        };
    }

    @Bean
    @ConditionalOnProperty(name = SPRINGDOC_SWAGGER_UI_ENABLED, matchIfMissing = true)
    RouterFunction<ServerResponse> getSwaggerUiConfigIngress(@Value(API_DOCS_URL) String apiDocsUrl
            , SwaggerUiConfigProperties swaggerUiConfig) {
        final String configUrl = apiDocsUrl + DEFAULT_PATH_SEPARATOR + SWAGGGER_CONFIG_FILE;
        return RouterFunctions.route(GET(configUrl)
                .and(accept(MediaType.APPLICATION_JSON)), req -> ServerResponse.ok()
                                                                               .contentType(MediaType.APPLICATION_JSON)
                                                                               .bodyValue(swaggerUiConfig.getConfigParameters()));
    }

    @Bean
    public VavrModule vavrModule(){
        return new VavrModule();
    }

    @Bean
    Jackson2JsonEncoder jackson2JsonEncoder(ObjectMapper mapper) {
        return new Jackson2JsonEncoder(mapper);
    }

    @Bean
    Jackson2JsonDecoder jackson2JsonDecoder(ObjectMapper mapper) {
        Jackson2JsonDecoder jackson2JsonDecoder = new Jackson2JsonDecoder(mapper);
        jackson2JsonDecoder.setMaxInMemorySize(32 * 1024 *1024);
        return jackson2JsonDecoder;
    }

    @Bean
    public WebFluxConfigurer webFluxConfigurer(Jackson2JsonEncoder encoder, Jackson2JsonDecoder decoder) {
        return new WebFluxConfigurer() {
            @Override
            public void configureHttpMessageCodecs(ServerCodecConfigurer configurer) {
                configurer.defaultCodecs().jackson2JsonEncoder(encoder);
                configurer.defaultCodecs().jackson2JsonDecoder(decoder);
            }

            @Override
            public void addResourceHandlers(final ResourceHandlerRegistry registry) {
                registry.addResourceHandler("/webjars/**")
                        .addResourceLocations("classpath:/META-INF/resources/webjars/")
                        .resourceChain(true)
                        .addResolver(new WebJarsResourceResolver());
            }
        };
    }

    @Bean
    public ReactiveHealthIndicator kafkaStreamsHealthIndicator(){
        return new KafkaStreamsHealthIndicator();
    }
}
