package gov.scot.payments.application.kafka;

import org.springframework.boot.web.reactive.context.AnnotationConfigReactiveWebServerApplicationContext;

public class KafkaStreamsOverridingContext extends AnnotationConfigReactiveWebServerApplicationContext {

    public KafkaStreamsOverridingContext() {
        super(new KafkaStreamsOverridingBeanFactory());
    }

}
