package gov.scot.payments.application.kafka;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import lombok.Setter;
import org.apache.kafka.streams.KafkaStreams;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.ReactiveHealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.EnumSet;
import java.util.Objects;

public class KafkaStreamsHealthIndicator implements ReactiveHealthIndicator, BeanFactoryAware {

    private ListableBeanFactory beanFactory;

    @Override
    public Mono<Health> health() {
         return Flux.fromIterable(HashMap.ofAll(beanFactory.getBeansOfType(StreamsBuilderFactoryBean.class))
                 .toList())
                 .map(sb -> sb._2.getKafkaStreams())
                 .filter(Objects::nonNull)
                 .map(ks -> {
                     if(EnumSet.of(KafkaStreams.State.RUNNING).contains( ks.state())){
                         return Health.up().build();
                     } else {
                         return Health.down().build();
                     }
                 })
                 .reduce(this::merge);
    }

    private Health merge(Health h1, Health h2) {
        if(h1.getStatus() == Status.UP && h2.getStatus() == Status.UP){
            return Health.up().build();
        }
        return Health.down().build();
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = (ListableBeanFactory)beanFactory;
    }
}
