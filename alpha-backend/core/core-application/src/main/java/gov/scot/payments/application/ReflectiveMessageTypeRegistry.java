package gov.scot.payments.application;

import com.google.common.base.Predicate;
import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Subject;
import io.vavr.Tuple2;
import io.vavr.collection.HashMap;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import lombok.extern.slf4j.Slf4j;
import org.reflections.ReflectionUtils;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.stream.Collectors;

@Slf4j
public class ReflectiveMessageTypeRegistry implements MessageTypeRegistry {

    private final Map<MessageTypeInfo,Class<?>> messageTypes;
    private final Map<Class<?>,MessageTypeInfo> reverseMessageTypes;

    public ReflectiveMessageTypeRegistry(Map<String,String> aliases,String... pkgs) {
        this(aliases,List.of(pkgs).flatMap(pkg -> ClasspathHelper.forPackage(pkg)));
    }

    public ReflectiveMessageTypeRegistry(String... pkgs) {
        this(HashMap.empty(),pkgs);
    }

    @SuppressWarnings("unchecked")
    public ReflectiveMessageTypeRegistry(Map<String,String> aliases,Iterable<URL> urls) {
        log.info("Scanning packages for message types: {}",urls);
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setUrls(List.ofAll(urls).toJavaList())
                .setScanners(new TypeAnnotationsScanner(),new SubTypesScanner()));
        final List<Tuple2<MessageTypeInfo, Class>> messageList = (List)List.ofAll(reflections.getTypesAnnotatedWith(MessageType.class))
                                                                                  .map(c -> new Tuple2<>(getInfoFromAnnotation(c), c))
                                                                                  .filter(t -> t._1 != null);
        if(messageList.distinct().size() != messageList.size()){
            throw new IllegalStateException("Duplicate message type detected");
        }
        java.util.Map<MessageTypeInfo,Class<?>> messageTypes = messageList.toMap( t -> (Tuple2)t).toJavaMap();
        aliases.forEach( (p,c) -> {
            java.util.List<Class<?>> clazzes = messageTypes
                    .entrySet().stream()
                    .filter(e -> e.getKey().getContext().equals(p))
                    .map(e -> e.getValue())
                    .collect(Collectors.toList());
            clazzes.forEach(clazz -> {
                MessageTypeInfo info = getInfoFromAnnotation(clazz);
                messageTypes.remove(info);
                messageTypes.put(new MessageTypeInfo(c,info.getType()),clazz);
            });
        });
        this.messageTypes = HashMap.ofAll(messageTypes);
        this.reverseMessageTypes = this.messageTypes.toList().map(Tuple2::swap).toMap(t -> t);
        log.info("Registered {} message types",messageTypes.size());
    }

    @Override
    public Option<Class<?>> getForType(String context, String type) {
       return messageTypes.get(new MessageTypeInfo(context,type));
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Class<?>> getAllForContext(final String context) {
        return (List) messageTypes
                .filterKeys(mi -> mi.getContext().equals(context))
                .values()
                .toList();
    }

    @Override
    public Option<MessageTypeInfo> getInfo(final Class<?> clazz) {
        return reverseMessageTypes.get(clazz);
    }

    private MessageTypeInfo getInfoFromAnnotation(final Class<?> clazz) {
        MessageType annotation = clazz.getAnnotation(MessageType.class);
        if(annotation != null){
            return new MessageTypeInfo(annotation.context(), annotation.type());
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> Option<MessageConstructorSpec<? extends T,?>> getConstructor(final Class<T> messageType) {
        return HashSet.ofAll(ReflectionUtils.getMethods(messageType
                ,ReflectionUtils.withAnnotation(MessageConstructor.class)
                ,ReflectionUtils.withParametersCount(4)
                ,withParameterAssignableTo(1,boolean.class)
                ,withParameterAssignableTo(2,Long.class)
                ,withParameterAssignableTo(3, Subject.class)
                ,ReflectionUtils.withReturnType(messageType)
                ,ReflectionUtils.withModifier(Modifier.PUBLIC)
                ,ReflectionUtils.withModifier(Modifier.STATIC)
        ))
                      .headOption()
                      .map(m -> new MessageConstructorSpec(messageType,m,m.getAnnotation(MessageConstructor.class),m.getParameters()[0].getType()));
    }

    private Predicate<Method> withParameterAssignableTo(int paramNum, final Class type) {
        return input -> {
            if (input != null) {
                Class<?> parameterType = input.getParameterTypes()[paramNum];
                return parameterType.isAssignableFrom(type) &&
                        (parameterType != Object.class || type == Object.class);
            }
            return false;
        };
    }

}
