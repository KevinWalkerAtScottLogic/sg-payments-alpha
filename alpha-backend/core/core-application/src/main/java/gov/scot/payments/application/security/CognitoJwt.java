package gov.scot.payments.application.security;

import gov.scot.payments.model.user.Action;
import gov.scot.payments.model.user.Application;
import gov.scot.payments.model.user.Group;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Scope;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.model.user.User;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.OAuth2ErrorCodes;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtValidationException;

public class CognitoJwt extends Jwt {

    public CognitoJwt(Jwt token) {
        super(token.getTokenValue()
                , token.getIssuedAt()
                , token.getExpiresAt()
                , token.getHeaders()
                , token.getClaims());
    }

    @Override
    public String getSubject() {
        if("id".equals(getClaimAsString("token_use"))){
            return getClaimAsString("cognito:username");
        } else if("access".equals(getClaimAsString("token_use")) && !containsClaim("username")){
            return getClaimAsString("client_id");
        } else {
            return null;
        }
    }

    public SubjectAuthentication toAuthentication() {
        Option<Subject> principal = Option.none();
        if("id".equals(getClaimAsString("token_use"))){
            User user = createUser();
            principal = Option.of(user);
        } else if("access".equals(getClaimAsString("token_use")) && !containsClaim("username")){
            Application apiClient = createApiClient();
            principal = Option.of(apiClient);
        }
        return principal
                .map(u -> new SubjectAuthentication(this, u))
                .getOrNull();
    }

    Application createApiClient() {
        String clientId = getClaimAsString("client_id");
        if(clientId == null){
            throw new JwtValidationException("No email or client_id claim found in token", List.of(new OAuth2Error(OAuth2ErrorCodes.INVALID_REQUEST)).toJavaList());
        }

        Set<Role> roles =  Try.of(() -> Option.of(getClaimAsString("scope")))
                              .map(o -> o.map(s -> List.of(s.split(";"))).getOrElse(List.of()))
                              .map(l -> l.map(this::createRoleFromScope).toSet())
                              .getOrElseThrow(e -> new JwtValidationException("Could not map scopes", List.of(new OAuth2Error(OAuth2ErrorCodes.INVALID_REQUEST)).toJavaList()));

        return Application.builder()
                          .name(clientId)
                          .clientId(clientId)
                          .roles(roles)
                          .build();
    }

    User createUser() {
        String email = getClaimAsString("email");
        String userName = getClaimAsString("cognito:username");
        if(email == null || userName == null){
            throw new JwtValidationException("No email or cognito:username claim found in token", List.of(new OAuth2Error(OAuth2ErrorCodes.INVALID_REQUEST)).toJavaList());
        }
        Set<Group> groups = Try.of(() -> Option.of(getClaimAsStringList("cognito:groups")))
                               .map(o -> o.getOrElse(java.util.List.of()))
                               .map(List::ofAll)
                               .map(s -> s.map(Group::new).toSet())
                               .getOrElseThrow(e -> new JwtValidationException("Could not map cognito:groups", List.of(new OAuth2Error(OAuth2ErrorCodes.INVALID_REQUEST)).toJavaList()));

        Set<Action> actions =  Try.of(() -> Option.of(getClaimAsString("custom:permitted_actions")))
                                  .map(o -> o.map(s -> List.of(s.split(";"))).getOrElse(List.of()))
                                  .map(l -> l.map(Action::parse).toSet())
                                  .getOrElseThrow(e -> new JwtValidationException("Could not map permitted_actions", List.of(new OAuth2Error(OAuth2ErrorCodes.INVALID_REQUEST)).toJavaList()));

        Set<Role> roles = groups.map(g -> createRole(g,actions));

        return User.builder()
                   .email(email)
                   .name(userName)
                   .groups(groups)
                   .roles(roles)
                   .build();
    }

    private Role createRole(Group group, Set<Action> actions) {
        return new Role(actions,Scope.parse(group.getName()));
    }

    private Role createRoleFromScope(String def) {
        String[] components = def.split("/");
        Scope scope = Scope.parse(components[0]);
        Action action = Action.parse(components[1]);
        return new Role(action,scope);
    }
}
