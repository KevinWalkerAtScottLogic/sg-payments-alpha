package gov.scot.payments.application.func;

import io.vavr.API;
import io.vavr.Function1;
import io.vavr.Function2;
import io.vavr.Tuple2;
import io.vavr.control.Option;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static io.vavr.API.$;
import static io.vavr.API.Case;
import static io.vavr.Predicates.instanceOf;

public class PatternMatcher<T,K> {

    private API.Match.Case<T,K>[] cases;

    private PatternMatcher(API.Match.Case<T,K>[] cases) {
        this.cases = cases;
    }

    public static <T,K> Builder<T,K> builder() {
        return new Builder<>();
    }

    public static <T1,T2,K> Builder2<T1,T2,K> builder2() {
        return new Builder2<>();
    }

    public K of(T value){
        return API.Match(value)
                .of(cases);
    }

    public Option<K> option(T value){
        return API.Match(value)
                .option(cases);
    }

    public static class Builder2<T1,T2,K> extends Builder<Tuple2<T1,T2>,K> {

        public <_1 extends T1,_2 extends T2> Builder2<T1,T2,K> match2(Predicate<_1> p1, Predicate<_2> p2, Function2<_1,_2,K> func) {
            Matcher<Tuple2<T1,T2>, K> matcher = Matchers.function2(p1,p2,func);
            if (this.matchers == null) this.matchers = new ArrayList<>();
            this.matchers.add(matcher.toCase());
            return this;
        }

        public <_1 extends T1,_2 extends T2> Builder2<T1,T2,K> match2(Predicate<_1> p1, Function2<_1,_2,K> func) {
            return match2(p1,t -> true,func);
        }

        public <_1 extends T1,_2 extends T2> Builder2<T1,T2,K> match2(Class<_1> type, Function2<_1,_2,K> func) {
            return match2(instanceOf(type),func);
        }

        public <_1 extends T1,_2 extends T2> Builder2<T1,T2,K> match2(Class<_1> type, Predicate<_2> p2, Function2<_1,_2,K> func) {
            return match2(instanceOf(type),p2,func);
        }

        public Builder2<T1,T2,K> apply2(Consumer<Builder2<T1,T2,K>> consumer) {
            consumer.accept(this);
            return this;
        }
    }

    public static class Builder<T,K> {
        protected ArrayList<API.Match.Case<? extends T, K>> matchers;

        Builder() {
        }

        public Builder<T,K> apply(Consumer<Builder<T,K>> consumer) {
           consumer.accept(this);
            return this;
        }

        public <T1 extends T> Builder<T,K> match(API.Match.Case<T1, K> match) {
            if (this.matchers == null) this.matchers = new ArrayList<>();
            this.matchers.add(match);
            return this;
        }

        public <T1 extends T> Builder<T,K> match(Matcher<T1,K> matcher) {
            return match(matcher.toCase());
        }

        public <T1 extends T> Builder<T,K> match(Predicate<T1> type, K value) {
            return match(Case($(type), value));
        }

        public <T1 extends T> Builder<T,K> match(Predicate<T1> type, Function1<T1,K> func) {
            return match(Case($(type), func));
        }

        public <T1 extends T> Builder<T,K> match(Class<T1> type, K value) {
            return match(instanceOf(type),value);
        }

        public <T1 extends T> Builder<T,K> match(Class<T1> type, Function1<T1,K> func) {
            return match(instanceOf(type),func);
        }


        public Builder<T,K> orElse(K other) {
            return match(Case($(), other));
        }

        public Builder<T,K> matchers(Collection<? extends API.Match.Case<T, K>> matchers) {
            if (this.matchers == null) this.matchers = new ArrayList<>();
            this.matchers.addAll(matchers);
            return this;
        }

        public Builder<T,K> clearMatchers() {
            if (this.matchers != null)
                this.matchers.clear();
            return this;
        }

        @SuppressWarnings("unchecked")
        public PatternMatcher<T,K> build() {
            List<API.Match.Case<? extends T, K>> matchers;
            switch (this.matchers == null ? 0 : this.matchers.size()) {
                case 0:
                    matchers = List.of();
                    break;
                case 1:
                    matchers = List.of(this.matchers.get(0));
                    break;
                default:
                    matchers = List.copyOf(new ArrayList<>(this.matchers));
            }

            return new PatternMatcher<>(matchers.toArray(API.Match.Case[]::new));
        }

        public String toString() {
            return "PatternMatcher.PatternMatcherBuilder(matchers=" + this.matchers + ")";
        }
    }
}
