package gov.scot.payments.application.func;

import java.util.function.Consumer;

public abstract class PatternMatchingConsumerDelegate<T> implements Consumer<T> {

    protected abstract void handlers(PatternMatcher.Builder<T,Void> builder);

    private final PatternMatcher<T,Void> matcher;

    public PatternMatchingConsumerDelegate(){
        matcher = PatternMatcher.<T,Void>builder()
                .apply(this::handlers)
                .build();
    }

    @Override
    public void accept(final T value) {
        matcher.of(value);
    }

}
