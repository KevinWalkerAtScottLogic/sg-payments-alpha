package gov.scot.payments.application.kafka;

import gov.scot.payments.application.kafka.MessageInfo;
import gov.scot.payments.model.Message;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.kafka.common.serialization.Serde;

@AllArgsConstructor
@Getter
@NoArgsConstructor
public class MessageWithInfo<T extends Message> {

    private T message;
    private MessageInfo info;

    public SerializedMessage serialize(Serde<T> serde){
        byte[] data = serde.serializer().serialize("",info.toHeaders(),message);
        return new SerializedMessage(data,info);
    }
}
