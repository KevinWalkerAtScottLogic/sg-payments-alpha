package gov.scot.payments.application.kafka;

public class InvalidContextException extends RuntimeException {

    public InvalidContextException(final String message) {
        super(message);
    }
}
