package gov.scot.payments.application.func;

import io.vavr.API;

public interface Matcher<E,T>{

    API.Match.Case<E,T> toCase();
}
