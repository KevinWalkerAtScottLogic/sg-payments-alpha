package gov.scot.payments.application.kafka;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.HasCause;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.Message;
import gov.scot.payments.model.MessageImpl;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.control.Option;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

class MessageHeaderEnricherTest {

    @Test
    public void test() {
        MessageTypeRegistry registry = mock(MessageTypeRegistry.class);
        doAnswer(i -> {
            Class<?> arg = i.getArgument(0,Class.class);
            MessageType ann = arg.getAnnotation(MessageType.class);
            return ann == null ? Option.none() : Option.of(new MessageTypeRegistry.MessageTypeInfo(ann.context(),ann.type()));
        }).when(registry).getInfo(any());
        final UUID correlationId = UUID.randomUUID();
        final KeyValueWithHeaders<String, Message> noType = KeyValueWithHeaders.msg(new MessageNoType());
        final KeyValueWithHeaders<String,Message> withTypeNullKey = KeyValueWithHeaders.msg(new MessageWithType());
        final KeyValueWithHeaders<String,Message> withTypeWithKey = KeyValueWithHeaders.msg("k",new MessageWithType());
        final KeyValueWithHeaders<String,Message> withCause = KeyValueWithHeaders.msg("k",new MessageWithCause(correlationId,3,true));

        KafkaStreamsTestHarness harness = KafkaStreamsTestHarness.builderWithMappings(MessageNoType.class,MessageWithType.class,MessageWithCause.class)
                                                                 .build();
        harness.<byte[],Message>stream().transformValues(() -> new MessageHeaderEnricher<>("test",registry::getInfo))
                                        .to(KafkaStreamsTestHarness.OUTPUT_TOPIC);

        try (final TopologyTestDriver topology = harness.toTopology()) {
            harness.sendKeyValues(topology,noType,withTypeNullKey,withTypeWithKey,withCause);
            final List<KeyValueWithHeaders<String, Message>> actualValues = harness.drainKeyValues(topology);
            final RecordHeader expectedContextHeader = header(Message.CONTEXT_HEADER, "test");
            final RecordHeader expectedTypeHeader = header(Message.TYPE_HEADER, "testType");
            final RecordHeader expectedKeyHeader = header(HasKey.PARTITION_KEY_HEADER, "k");
            assertThat(actualValues.get(0).headers).containsExactly(expectedContextHeader);
            assertThat(actualValues.get(1).headers).containsExactlyInAnyOrder(expectedContextHeader, expectedTypeHeader);
            assertThat(actualValues.get(2).headers).containsExactlyInAnyOrder(expectedContextHeader, expectedTypeHeader, expectedKeyHeader);
            assertThat(actualValues.get(3).headers).containsExactlyInAnyOrder(expectedContextHeader, header(Message.TYPE_HEADER, "testType1"), expectedKeyHeader,header(HasCause.CORRELATION_ID_HEADER,correlationId.toString()),header(HasCause.EXECUTION_COUNT_HEADER,"3"));
        }
    }

    private RecordHeader header(final String contextHeader, final String test) {
        return new RecordHeader(contextHeader, test.getBytes(StandardCharsets.UTF_8));
    }


    private static class MessageNoType extends MessageImpl {

    }

    @MessageType(context = "test",type = "testType")
    private static class MessageWithType extends MessageImpl {

    }

    @MessageType(context = "test",type = "testType1")
    private static class MessageWithCause extends EventWithCauseImpl {

        @JsonCreator
        public MessageWithCause(@JsonProperty("correlationId") final UUID correlationId
                , @JsonProperty("executionCount") final int executionCount
                , @JsonProperty("reply") final boolean reply) {
            super(correlationId, executionCount, reply);
        }
    }
}