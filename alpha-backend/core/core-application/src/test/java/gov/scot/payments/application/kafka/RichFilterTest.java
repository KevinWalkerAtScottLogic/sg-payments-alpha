package gov.scot.payments.application.kafka;

import gov.scot.payments.model.Message;
import gov.scot.payments.model.MessageImpl;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.vavr.collection.List;
import io.vavr.control.Option;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

class RichFilterTest {

    @Test
    public void shouldFilter() {
        final KeyValueWithHeaders<String,Message> notPresent = KeyValueWithHeaders.msg(new TestMessage());
        final KeyValueWithHeaders<String,Message> nullValue = KeyValueWithHeaders.msg(new TestMessage(),new RecordHeader("test",(byte[])null));
        final KeyValueWithHeaders<String,Message> fail = KeyValueWithHeaders.msg(new TestMessage(),new RecordHeader("test","reject".getBytes()));
        final KeyValueWithHeaders<String,Message> pass = KeyValueWithHeaders.msg(new TestMessage(),new RecordHeader("test","accept".getBytes()));

        KafkaStreamsTestHarness harness = KafkaStreamsTestHarness.builderWithMappings(TestMessage.class)
                .build();
        harness.<String,Message>stream().transformValues(() -> new RichFilter<>(kvh -> Arrays.equals("accept".getBytes(), Option.of(kvh.headers.lastHeader("test")).map(r -> r.value()).getOrNull())))
                                        .filter((k,v) -> v != null)
                                        .to(KafkaStreamsTestHarness.OUTPUT_TOPIC);

        try (final TopologyTestDriver topology = harness.toTopology()) {
            harness.sendKeyValues(topology,notPresent, nullValue,fail, pass);
            final List<Message> actualValues = harness.drain(topology);
            assertThat(actualValues).containsExactly(pass.value);
        }
    }

    private static class TestMessage extends MessageImpl {

    }
}