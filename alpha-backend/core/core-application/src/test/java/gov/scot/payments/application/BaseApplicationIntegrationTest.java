package gov.scot.payments.application;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.application.kafka.MessageDuplicateFilterFactory;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.Message;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.command.TestCommand;
import gov.scot.payments.testing.event.TestEvent;
import gov.scot.payments.testing.kafka.EmbeddedKafkaBroker;
import gov.scot.payments.testing.kafka.EmbeddedKafkaExtension;
import gov.scot.payments.testing.spring.MockSchemaRegistryClient;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.vavr.collection.List;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.ValueTransformerWithKey;
import org.apache.kafka.streams.kstream.ValueTransformerWithKeySupplier;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.schema.client.SchemaRegistryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.UUID;

import static gov.scot.payments.testing.security.MockSubjectAuthentication.user;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockAuthentication;


@ApplicationIntegrationTest(classes = {BaseApplicationIntegrationTest.TestApplication.class}
    ,bindings = {"input","output"}
    ,properties = {"api.endpoints.no-auth=/test/noAuth"
        ,"spring.cloud.stream.bindings.input.destination=test"
        ,"spring.cloud.stream.bindings.output.destination=test"
        ,"component.name=test"
        ,"context.name=test"})
class BaseApplicationIntegrationTest {


    @Test
    public void testHealthEndpoint(@Autowired WebTestClient webClient) {
        webClient
                .get()
                .uri("/actuator/health")
                .exchange()
                .expectStatus()
                .isOk();
    }

    @Test
    public void testUnauthentictedEndpoint(@Autowired WebTestClient webClient) {
        webClient
                .get()
                .uri("/test/noAuth")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(String.class)
                .isEqualTo("hello");
    }

    @Test
    public void testAuthenticatedEndpointsDenied(@Autowired WebTestClient webClient) {
        webClient
                .get()
                .uri("/test/withAuthnNoAuthz")
                .exchange()
                .expectStatus()
                .isUnauthorized();
    }

    @Test
    public void testAuthenticatedEndpointsAuthenticated(@Autowired WebTestClient webClient) {
        webClient
                //auth with incorrect name
                .mutateWith(mockAuthentication(user("test")))
                .get()
                .uri("/test/withAuthnNoAuthz")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(String.class)
                .isEqualTo("hello1");
        webClient
                //auth with incorrect name
                .mutateWith(mockAuthentication(user("test")))
                .get()
                .uri("/test/withAuthnAndAuthz")
                .exchange()
                .expectStatus()
                .isForbidden();
        webClient
                //auth with correct name
                .mutateWith(mockAuthentication(user("test1")))
                .get()
                .uri("/test/withAuthnAndAuthz")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(String.class)
                .isEqualTo("hello2");
    }

    @Test
    public void testStreamProcessing(EmbeddedBrokerClient brokerClient) throws InterruptedException {
        final TestMessage firstId = new TestMessage("1");
        final TestMessage secondId = new TestMessage("2");
        final TestMessage thirdId = new TestMessage("3");
        brokerClient.sendToDestination("input",List.of(firstId, secondId,  thirdId));
        final List<TestOutput> actualValues = brokerClient.readAllFromDestination("output",TestOutput.class);
        assertThat(actualValues.map(TestOutput::getData)).containsExactly(firstId.getMessageId().toString(), secondId.getMessageId().toString(), thirdId.getMessageId().toString());
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    @EnableBinding(TestBinding.class)
    public static class TestApplication extends BaseApplication<TestMessage, TestOutput> {

        @Bean
        public TestResource testResource() {
            return new TestResource();
        }

        @Bean
        public TestStreamListener testStreamListener(MessageDeDuplicator<TestMessage,TestOutput> deDuplicator) {
            return new TestStreamListener(deDuplicator);
        }

    }

    public interface TestBinding {

        @Output("output")
        KStream<?, ?> output();

        @Input("input")
        KStream<?, ?> input();

    }

    @RequestMapping("/test")
    public static class TestResource {

        @ResponseBody
        @GetMapping("/noAuth")
        public Mono<String> getNoAuth() {
            return Mono.just("hello");
        }

        @ResponseBody
        @GetMapping("/withAuthnNoAuthz")
        public Mono<String> getWithAuthnNoAuthz() {
            return Mono.just("hello1");
        }

        @ResponseBody
        @GetMapping("/withAuthnAndAuthz")
        @PreAuthorize("principal.getName().equals('test1')")
        public Mono<String> getWithAuthnAndAuthz() {
            return Mono.just("hello2");
        }
    }

    @RequiredArgsConstructor
    public static class TestStreamListener {

        @NonNull private final MessageDeDuplicator<TestMessage,TestOutput> deDuplicator;

        @StreamListener
        @SendTo("output")
        public KStream<byte[], TestOutput> handle(@Input("input") KStream<byte[], TestMessage> events) throws Exception {
            return deDuplicator.deduplicate(events
                    , s -> s.mapValues(v -> new TestOutput(v.getMessageId().toString()))
                    ,"handle");
        }
    }

    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestOutput {

        @Getter private String data;
    }

    @MessageType(context = "test", type = "message")
    @SuperBuilder
    @EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
    public static class TestMessage extends TestCommand {

        public TestMessage() {
            super();
        }

        public TestMessage(String key) {
            super(key);
        }

        @JsonCreator
        public TestMessage(@JsonProperty("messageId") final UUID messageId
                , @JsonProperty("timestamp") final Instant timestamp
                , @JsonProperty("reply") final boolean reply
                , @JsonProperty("executionCount") final int executionCount
                , @JsonProperty("key") final String key) {
            super(messageId, timestamp, reply, executionCount, key);

        }
    }
}