package gov.scot.payments.application.kafka;

import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.Message;
import gov.scot.payments.model.MessageImpl;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.collection.List;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;

class MessageDuplicateFilterTest {

    @Test
    public void shouldRemoveDuplicatesFromTheInput() {
        final Message firstId = new TestMessage();
        final Message secondId = new TestMessage();
        final Message thirdId = new TestMessage();

        final Duration windowSize = Duration.ofMinutes(10);
        KafkaStreamsTestHarness harness = KafkaStreamsTestHarness.builderWithMappings(TestMessage.class)
                .build()
                .withWindowStore(windowSize,windowSize);
        harness.<byte[],Message>stream().transformValues(
                () -> new MessageDuplicateFilter<>(windowSize, KafkaStreamsTestHarness.STATE_STORE,new MicrometerMetrics(new SimpleMeterRegistry())),
                KafkaStreamsTestHarness.STATE_STORE)
                                        .filter((k,v) -> v != null)
                                        .to(KafkaStreamsTestHarness.OUTPUT_TOPIC);

        try (final TopologyTestDriver topology = harness.toTopology()) {
            harness.send(topology,firstId, secondId, firstId, firstId, secondId, thirdId,
                    thirdId, firstId, secondId);
            final List<Message> actualValues = harness.drain(topology);
            assertThat(actualValues).containsExactly(firstId, secondId, thirdId);
        }
    }

    private static class TestMessage extends MessageImpl {

    }


}