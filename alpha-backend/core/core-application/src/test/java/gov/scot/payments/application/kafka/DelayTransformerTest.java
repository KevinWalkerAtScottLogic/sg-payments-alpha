package gov.scot.payments.application.kafka;

import gov.scot.payments.model.Message;
import gov.scot.payments.model.MessageImpl;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.vavr.collection.List;
import org.apache.kafka.streams.TopologyTestDriver;
import org.assertj.core.data.Percentage;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;

class DelayTransformerTest {

    @Test
    public void test(){
        KafkaStreamsTestHarness harness = KafkaStreamsTestHarness.builder()
                .streamsConfiguration(KafkaStreamsTestHarness.defaultProperties(TestMessage.class))
                .serde(KafkaStreamsTestHarness.defaultSerde(TestMessage.class))
                .build();
        harness.<String,Message>stream().transform(() -> new DelayTransformer<>(Duration.ofSeconds(1)))
                                        .to(KafkaStreamsTestHarness.OUTPUT_TOPIC);

        long time = System.currentTimeMillis();
        final KeyValueWithHeaders<String, Message> value1 = KeyValueWithHeaders.msg(new TestMessage(),time);
        try (final TopologyTestDriver topology = harness.toTopology()) {
            harness.sendKeyValues(topology,value1);
            final List<KeyValueWithHeaders<String,Message>> actualValues = harness.drainKeyValues(topology);
            assertThat(actualValues.get(0).timestamp).isCloseTo(time+1000, Percentage.withPercentage(5));
        }
    }

    private static class TestMessage extends MessageImpl {

    }

}