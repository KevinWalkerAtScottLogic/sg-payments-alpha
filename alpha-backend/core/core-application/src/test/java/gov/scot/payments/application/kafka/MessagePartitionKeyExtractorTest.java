package gov.scot.payments.application.kafka;

import gov.scot.payments.model.HasKey;
import gov.scot.payments.testing.event.TestCreateEvent;
import org.junit.jupiter.api.Test;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class MessagePartitionKeyExtractorTest {

    @Test
    public void testExtract(){
        MessagePartitionKeyExtractor extractor = new MessagePartitionKeyExtractor();
        Message message = new GenericMessage<>(new TestCreateEvent("a"), Map.of(HasKey.PARTITION_KEY_HEADER,"part"));
        assertEquals("part",extractor.extractKey(message));

        message = new GenericMessage<>(new TestCreateEvent("a"));
        assertEquals("a",extractor.extractKey(message));

        message = new GenericMessage<>(new Object());
        assertNull(extractor.extractKey(message));
    }

}