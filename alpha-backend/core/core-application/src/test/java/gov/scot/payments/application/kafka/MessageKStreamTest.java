package gov.scot.payments.application.kafka;

import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.Message;
import gov.scot.payments.model.MessageImpl;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.collection.List;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.state.Stores;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.Arrays;
import java.util.UUID;

import static gov.scot.payments.application.kafka.MessageKStream.messageStream;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class MessageKStreamTest {

    private Message firstId;
    private Message secondId;
    private Message thirdId;
    private Duration windowSize;
    private KafkaStreamsTestHarness harness;
    private MessageDuplicateFilterFactory<Message> duplicateFilterFactory;

    @BeforeEach
    public void setUp(){
        firstId = new TestMessage();
        secondId = new TestMessage();
        thirdId = new TestMessage();
        windowSize = Duration.ofMinutes(10);
        harness = KafkaStreamsTestHarness.builderWithMappings(TestMessage.class)
                                         .build()
                                         .withWindowStore(windowSize,windowSize);
        duplicateFilterFactory  = new MessageDuplicateFilterFactory<>(windowSize
                ,KafkaStreamsTestHarness.STATE_STORE
                ,new MicrometerMetrics(new SimpleMeterRegistry())
                , Stores.inMemoryWindowStore(KafkaStreamsTestHarness.STATE_STORE, windowSize, windowSize, false));
    }

    @Test
    public void testDeduplicate() {
        messageStream(harness.stream())
                .deduplicate(duplicateFilterFactory)
                .to(KafkaStreamsTestHarness.OUTPUT_TOPIC);

        try (final TopologyTestDriver topology = harness.toTopology()) {
            harness.send(topology,firstId, secondId, firstId, firstId, secondId, thirdId,
                    thirdId, firstId, secondId);
            final List<Message> actualValues = harness.drain(topology);
            assertThat(actualValues).containsExactly(firstId, secondId, thirdId);
        }
    }

    @Test
    public void testConsume() {
        messageStream(harness.stream())
                .consume(duplicateFilterFactory,s -> s.to(KafkaStreamsTestHarness.OUTPUT_TOPIC) );

        try (final TopologyTestDriver topology = harness.toTopology()) {
            harness.send(topology,firstId, secondId, firstId, firstId, secondId, thirdId,
                    thirdId, firstId, secondId);
            final List<Message> actualValues = harness.drain(topology);
            assertThat(actualValues).containsExactly(firstId, secondId, thirdId);
        }
    }

    @Test
    public void testWrap() {
        messageStream(harness.stream())
                .wrap(duplicateFilterFactory
                        ,s -> s.mapValues(Message::getMessageId)
                        ,() -> new MessageHeaderEnricher<>("test"))
                .to(KafkaStreamsTestHarness.OUTPUT_TOPIC);

        try (final TopologyTestDriver topology = harness.toTopology()) {
            harness.send(topology,firstId, secondId, firstId, firstId, secondId, thirdId,
                    thirdId, firstId, secondId);
            final List<KeyValueWithHeaders<String, UUID>> actualValues = harness.drainKeyValues(topology);
            assertThat(actualValues.map(kvh -> kvh.value)).containsExactly(firstId.getMessageId(), secondId.getMessageId(), thirdId.getMessageId());
            assertThat(actualValues.map(kvh -> kvh.headers)).allMatch(h -> Arrays.equals(h.lastHeader(Message.CONTEXT_HEADER).value(),"test".getBytes()));
        }
    }


    private static class TestMessage extends MessageImpl {

    }
}