package gov.scot.payments.application.kafka;

import gov.scot.payments.kafka.FilteringSerde;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.common.serialization.Serdes;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

class FilteringSerdeTest {

    private FilteringSerde serde;
    private BeanFactory factory;

    @BeforeEach
    void setUp() {
        factory = mock(BeanFactory.class);
        serde = new FilteringSerde(factory,Serdes.ByteArray().serializer());

    }

    @Test
    public void testNoBean(){
        doThrow(new NoSuchBeanDefinitionException("")).when(factory).getBean(anyString(),eq(Predicate.class));
        serde.configure(Map.of("header-filter-bean","filter"),false);
        byte[] data = new byte[]{1,2,3};
        assertEquals(data,serde.deserializer().deserialize("",new RecordHeaders(),data));
    }

    @Test
    public void testFiltering(){
        Predicate<Headers> test = h -> h.lastHeader("header") != null && Arrays.equals(h.lastHeader("header").value(),"test".getBytes());
        doReturn(test).when(factory).getBean(anyString(),eq(Predicate.class));
        serde.configure(Map.of("header-filter-bean","filter"),false);
        byte[] data = new byte[]{1,2,3};
        assertEquals(FilteringSerde.FILTERED_PLACEHOLDER,serde.deserializer().deserialize("",new RecordHeaders(),data));
        assertEquals(FilteringSerde.FILTERED_PLACEHOLDER,serde.deserializer().deserialize("",new RecordHeaders(new RecordHeader[]{new RecordHeader("header","123".getBytes())}),data));
        assertEquals(data,serde.deserializer().deserialize("",new RecordHeaders(new RecordHeader[]{new RecordHeader("header","test".getBytes())}),data));
    }
}