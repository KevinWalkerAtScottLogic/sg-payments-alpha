package gov.scot.payments.application.component.projector.temporal.springdata.jpa;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Entity;

@Entity(name = "GenericTemporalEntity")
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@TypeDef(name = "jsonb",typeClass = JsonBinaryType.class)
public class GenericTemporalEntity extends GenericTemporalProjection<TestEntity> {

    @Type(type = "jsonb")
    private TestEntity payload;

}
