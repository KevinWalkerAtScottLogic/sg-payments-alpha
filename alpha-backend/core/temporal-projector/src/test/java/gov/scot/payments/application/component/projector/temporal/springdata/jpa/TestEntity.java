package gov.scot.payments.application.component.projector.temporal.springdata.jpa;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.Projection;
import gov.scot.payments.testing.command.TestUpdateCommand;
import gov.scot.payments.testing.event.TestUpdateEvent;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.Instant;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode(of = "id")
public class TestEntity implements Projection<String>, HasKey<String> {

    @NonNull @Id private String id;

    private Instant processingTime;
    private String data;

    public TestEntity(String id, String data){
        this(id,Instant.now(),data);
    }

    public TestEntity merge(TestUpdateEvent e) {
        return toBuilder().data(e.getMessageId().toString()).build();
    }

    public TestEntity merge(TestUpdateCommand e) {
        return toBuilder().data(e.getMessageId().toString()).build();
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return id;
    }
}
