package gov.scot.payments.application.component.projector.temporal.springdata.jpa;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.component.projector.springdata.ProjectionRepository;
import gov.scot.payments.application.component.projector.springdata.QueryByIdResource;
import gov.scot.payments.application.component.projector.springdata.jpa.JpaConsumingProjectorApplication;
import gov.scot.payments.application.component.projector.springdata.jpa.SearchResource;
import gov.scot.payments.application.component.projector.springdata.jpa.SubjectSpecification;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.kafka.HeaderPreservingCompositeNonNativeSerde;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.kafka.StreamJoiner;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.application.security.SecurityCustomizer;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.command.TestDeleteCommand;
import gov.scot.payments.testing.event.*;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.vavr.Function2;
import io.vavr.Function3;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.state.Stores;
import org.apache.kafka.streams.state.WindowBytesStoreSupplier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.HttpMethod;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
import java.time.Instant;
import java.util.function.Predicate;

import static gov.scot.payments.testing.security.MockSubjectAuthentication.user;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockAuthentication;

@EnableAutoConfiguration(exclude = {HypermediaAutoConfiguration.class})
@ApplicationIntegrationTest(classes = {JpaConsumingProjectorIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.PROJECTOR
        ,additionalTopics = 1
        ,properties = {"spring.datasource.url=jdbc:h2:mem:JpaConsumingProjectorIntegrationTest"
        ,"component.name=test"
        ,"context.name=test"
        ,"component.schemaname=testschema"
        ,"spring.flyway.schemas="
        ,"spring.jpa.properties.hibernate.default_schema="
        ,"events.destinations=test"})
class JpaConsumingProjectorIntegrationTest {

    @BeforeEach
    public void setUp(EmbeddedBrokerClient brokerClient
            , @Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter){
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING,Duration.ofSeconds(10));
    }

    @Test
    public void test(EmbeddedBrokerClient brokerClient, @Autowired WebTestClient client){
        KeyValueWithHeaders<String,TestCreateEvent> create1 = KeyValueWithHeaders.msg("1",new TestCreateEvent("1"));
        KeyValueWithHeaders<String,TestUpdateEvent> update1 = KeyValueWithHeaders.msg("2",new TestUpdateEvent("2"));
        TestDeleteEvent testDeleteEvent = new TestDeleteEvent("3");
        testDeleteEvent.setCauseDetails(new TestDeleteCommand("3",true,0));
        KeyValueWithHeaders<String,TestDeleteEvent> delete1 = KeyValueWithHeaders.msg("3", testDeleteEvent);
        KeyValueWithHeaders<String,TestCreateEvent> create2 = KeyValueWithHeaders.msg("1",new TestCreateEvent("1"));
        brokerClient.sendKeyValuesToDestination("events-internal",List.of(create1,update1,create1));
        brokerClient.sendKeyValuesToDestination("events-external",List.of(delete1,create2));

        try {
            Thread.sleep(2000L);
        } catch (InterruptedException e) {}

        client
                .get()
                .uri("/entities/1")
                .exchange()
                .expectStatus()
                .isUnauthorized();

        client
                .get()
                .uri("/entities/search")
                .exchange()
                .expectStatus()
                .isUnauthorized();

        client
                .get()
                .uri("/temporalTestEntity/1/payload")
                .exchange()
                .expectStatus()
                .isUnauthorized();


        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("world/entity:Write"))))
                .get()
                .uri("/entities/1")
                .exchange()
                .expectStatus()
                .isForbidden();

        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("world/entity:Write"))))
                .get()
                .uri("/entities/search")
                .exchange()
                .expectStatus()
                .isForbidden();

        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("world/entity:Read"))))
                .get()
                .uri("/entities/4")
                .exchange()
                .expectStatus()
                .isNotFound();

        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("world/entity:Read"))))
                .get()
                .uri("/entities/1")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.id").isEqualTo("1");

        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("world/entity:Read"))))
                .get()
                .uri(uri -> uri.path("/entities/search")
                               .queryParam("query","id=in=(1,2)")
                               .build())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.page.totalElements").isEqualTo(1)
                .jsonPath("$._embedded.testEntityList[0].id").isEqualTo("1");


        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("world/entity:Write"))))
                .get()
                .uri("/temporalTestEntity/1/payload")
                .exchange()
                .expectStatus()
                .isForbidden();

        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("10/entity:Read"))))
                .get()
                .uri("/temporalTestEntity/1/payload")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.page.totalElements").isEqualTo(0);


        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("1/entity:Read"))))
                .get()
                .uri("/temporalTestEntity/1/latest/payload")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.id").isEqualTo("1");

        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("1/entity:Read"))))
                .get()
                .uri("/temporalTestEntity/1/payload")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$._embedded.testEntityList[0].id").isEqualTo("1");





    }




    @EnableJpaRepositories(basePackageClasses = {TestEntityRepository.class})
    @EntityScan(basePackageClasses = TestEntity.class)
    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends JpaConsumingProjectorApplication {

        @Bean
        public StreamJoiner<Event,Event,Event> joiner(@Value("${embedded.broker.additional.topic.1}") String externalEventTopic
                , SchemaRegistryClient confluentSchemaRegistryClient){
            HeaderPreservingCompositeNonNativeSerde<Event> eventSerde = valueSerde(Event.class);
            Function2<String, JoinWindows, WindowBytesStoreSupplier> storeSupplier = (n, w) -> Stores.inMemoryWindowStore(n + "-JpaConsumingProjectorIntegrationTest-store", Duration.ofMillis(w.size() + w.gracePeriodMs()), Duration.ofMillis(w.size()), true);
            return StreamJoiner.<Event, Event, Event>builder()
                    .repartitionTopicName(externalEventTopic)
                    .joinWindow(Duration.ofMinutes(1))
                    .joiner((e1,e2) -> e2 == null ? e1 : e2)
                    .rekeyFunction(kvh -> kvh.key)
                    .leftValueSerde(eventSerde)
                    .rightValueSerde(eventSerde)
                    .stateSerde(stateSerde(confluentSchemaRegistryClient, SerializedMessage.class))
                    .storeSupplier(storeSupplier)
                    .build();
        }

        @Bean
        @Primary
        public Predicate<gov.scot.payments.application.kafka.KeyValueWithHeaders<String,Event>> externalEventFilter(){
            return (kvh) -> !"1".equals(kvh.key);
        }

        @Bean
        public GenericTemporalProjectionStorageService<TestEntity,GenericTemporalEntity> temporalEntityStorageService(Metrics metrics
                , final GenericRepository repository){
            PatternMatcher<Event,TestEntity> delegate = PatternMatcher.<Event,TestEntity>builder()
                    .match(TestErrorEvent.class, e -> {throw new RuntimeException();})
                    .match(TestEvent.class, e -> new TestEntity(e.getKey(), Instant.now(),e.getMessageId().toString()))
                    .build();
            Function3<Event,Instant,TestEntity,GenericTemporalEntity> entitySupplier = (e,i,t) ->  GenericTemporalEntity.builder()
                    .payload(t)
                    .id(e.getMessageId())
                    .logicalId(t.getKey())
                    .eventTime(i)
                    .processingTime(e.getTimestamp())
                    .build();
            return new GenericTemporalProjectionStorageService<>(metrics,repository, e -> new Tuple2<>(Instant.now(),delegate.of(e)), entitySupplier);
        }

        @Bean
        public TestRepositoryMutatingStorageService repositoryMutatingStorageService(Metrics metrics
                , final TestEntityRepository repository){
            return new TestRepositoryMutatingStorageService(repository,metrics);
        }

        @Bean
        public PhysicalNamingStrategy physicalNamingStrategy() {
            return new GenericEntityNamingStrategy("temporalTestEntity");
        }

        @Bean
        public QueryById queryById(TestEntityRepository repository, PagedResourcesAssembler<TestEntity> assembler){
            return new QueryById(repository, assembler);
        }

        @Bean
        public Search search(TestEntityRepository repository, PagedResourcesAssembler<TestEntity> assembler){
            return new Search(repository, assembler);
        }

        @Bean
        public QueryResource temporalEntityQueryResource(GenericRepository repository, PagedResourcesAssembler<TestEntity> assembler){
            return new QueryResource(repository,assembler);
        }

        @Bean
        public SecurityCustomizer lookupServiceSecurity(){
            return r -> r.pathMatchers(HttpMethod.GET,"/entities/**").hasAuthority("entity:Read")
                         .pathMatchers(HttpMethod.GET,"/temporalTestEntity/**").hasAuthority("entity:Read");
        }

    }

    @RequestMapping("/entities")
    @RestController
    public static class QueryById extends QueryByIdResource<String,TestEntity> {

        public QueryById(ProjectionRepository<String, TestEntity> repository, PagedResourcesAssembler<TestEntity> assembler) {
            super(repository,assembler);
        }
    }

    @RequestMapping("/entities")
    public static class Search extends SearchResource<TestEntity> {

        public Search(JpaSpecificationExecutor<TestEntity> repository, PagedResourcesAssembler<TestEntity> assembler) {
            super(repository,assembler, s -> SubjectSpecification.any());
        }
    }

    @RequestMapping("/temporalTestEntity")
    public static class QueryResource extends GenericTemporalProjectionQueryResource<TestEntity,GenericTemporalEntity> {

        public QueryResource(GenericRepository repository, PagedResourcesAssembler<TestEntity> assembler) {
            super(repository,assembler);
        }
    }

}