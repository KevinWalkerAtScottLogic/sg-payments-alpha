CREATE domain IF NOT EXISTS jsonb AS other;

CREATE TABLE TEST_ENTITY (
    id varchar(255) NOT NULL,
    processing_time TIMESTAMP NOT NULL,
    data VARCHAR(255) NOT NULL
);

CREATE TABLE TEMPORAL_TEST_ENTITY (
    logical_id varchar(255) NOT NULL,
    id UUID NOT NULL,
    processing_time TIMESTAMP NOT NULL,
    event_time TIMESTAMP NOT NULL,
    payload jsonb NOT NULL
);