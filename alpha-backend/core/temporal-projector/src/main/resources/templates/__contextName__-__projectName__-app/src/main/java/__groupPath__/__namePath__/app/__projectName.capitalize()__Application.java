package @group@.@namePackage@.app;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import gov.scot.payments.application.BaseApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.kafka.StreamJoiner;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.kafka.HeaderPreservingCompositeNonNativeSerde;
import gov.scot.payments.application.kafka.KeyValueWithHeaders;
import io.vavr.Function1;
import io.vavr.Function3;
import io.vavr.Tuple2;
import gov.scot.payments.model.Event;
import org.springframework.context.annotation.Bean;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import gov.scot.payments.application.component.projector.temporal.springdata.jpa.JpaTemporalProjectorApplication;
import @group@.@namePackage@.model.@projectName.capitalize()@;
import @group@.@namePackage@.model.@projectName.capitalize()@TemporalEntity;

import java.util.function.Predicate;
import java.time.Instant;

@EnableAutoConfiguration(exclude = {HypermediaAutoConfiguration.class})
@EnableJpaRepositories(basePackageClasses = @projectName.capitalize()@Repository.class)
@EntityScan(basePackageClasses = @projectName.capitalize()@.class)
@ApplicationComponent
public class @projectName.capitalize()@Application extends JpaTemporalProjectorApplication<@projectName.capitalize()@,@projectName.capitalize()@TemporalEntity> {

         /*
    @Bean
    public StreamJoiner<Event,Event,Event> joiner(SchemaRegistryClient confluentSchemaRegistryClient){
        HeaderPreservingCompositeNonNativeSerde<Event> eventSerde = valueSerde(Event.class);
        return StreamJoiner.<Event, Event, Event>builder()
            .repartitionTopicName()
            .joinWindow()
            .joiner()
            .rekeyFunction()
            .leftValueSerde(eventSerde)
            .rightValueSerde(eventSerde)
            .stateSerde(stateSerde(confluentSchemaRegistryClient,SerializedMessage.class))
            .build();
    }
     */

        @Bean
        public Function1<Event, Tuple2<Instant,@projectName.capitalize()@>> delegateFunction(){

        }

        @Bean
        public Function3<Event, Instant, @projectName.capitalize()@, @projectName.capitalize()@TemporalEntity> entitySupplier(){
                return (e,i,t) -> @projectName.capitalize()@TemporalEntity.builder()
                        .payload(t)
                        .id(e.getMessageId())
                        .logicalId(t.getKey())
                        .eventTime(i)
                        .processingTime(e.getTimestamp())
                        .build();
        }

        public static void main(String[] args){
                BaseApplication.run(@projectName.capitalize()@Application.class,args);
        }
}