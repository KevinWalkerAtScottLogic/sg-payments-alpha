CREATE TABLE @projectTableName@_TEMPORAL_ENTITY (
    logical_id varchar(255) PRIMARY KEY,
    id UUID NOT NULL,
    processing_time TIMESTAMP NOT NULL,
    event_time TIMESTAMP NOT NULL,
    payload jsonb NOT NULL
);

CREATE INDEX @projectTableName@_TEMPORAL_ENTITY_logical_id ON @projectTableName@_TEMPORAL_ENTITY(logical_id);
CREATE INDEX @projectTableName@_TEMPORAL_ENTITY_processing_time ON @projectTableName@_TEMPORAL_ENTITY(processing_time DESC);
CREATE INDEX @projectTableName@_TEMPORAL_ENTITY_event_time ON @projectTableName@_TEMPORAL_ENTITY(event_time DESC);