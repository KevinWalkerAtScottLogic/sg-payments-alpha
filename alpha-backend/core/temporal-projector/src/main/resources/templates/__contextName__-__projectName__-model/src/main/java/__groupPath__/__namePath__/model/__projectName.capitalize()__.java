package @group@.@namePackage@.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import java.time.Instant;

import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.Projection;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode(of = "id")
public class @projectName.capitalize()@ implements Projection<String>, HasKey<String>{

    @NonNull private String id;

    private Instant processingTime;

    @Override
    @JsonIgnore
    public String getKey(){
        return id;
    }
}