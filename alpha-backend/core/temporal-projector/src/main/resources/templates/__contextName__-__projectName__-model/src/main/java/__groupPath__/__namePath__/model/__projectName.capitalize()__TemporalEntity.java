package @group@.@namePackage@.model;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import gov.scot.payments.application.component.projector.temporal.springdata.jpa.GenericTemporalProjection;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Entity;

@Entity(name = "GenericTemporalEntity")
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@TypeDef(name = "jsonb",typeClass = JsonBinaryType.class)
public class @projectName.capitalize()@TemporalEntity extends GenericTemporalProjection<@projectName.capitalize()@> {

    @Type(type = "jsonb")
    private @projectName.capitalize()@ payload;

}