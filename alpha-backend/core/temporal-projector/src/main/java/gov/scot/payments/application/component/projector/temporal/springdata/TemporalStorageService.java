package gov.scot.payments.application.component.projector.temporal.springdata;

import gov.scot.payments.application.component.projector.springdata.RepositoryAppendingStorageService;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.TemporalProjection;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class TemporalStorageService<ID,PK,T extends TemporalProjection<ID,PK>> extends RepositoryAppendingStorageService<PK,T> {

    public TemporalStorageService(Metrics metrics, TemporalProjectionRepository<ID,PK,T> repository) {
        super(metrics,repository);
    }

}
