package gov.scot.payments.application.component.projector.temporal.springdata.jpa;

import gov.scot.payments.application.component.projector.temporal.springdata.TemporalProjectionRepository;
import gov.scot.payments.application.component.projector.temporal.springdata.TemporalStorageService;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.HasKey;
import io.vavr.Function1;
import io.vavr.Function3;
import io.vavr.Tuple2;

import java.time.Instant;
import java.util.UUID;

public class GenericTemporalProjectionStorageService<V extends HasKey<String>,E extends GenericTemporalProjection<V>> extends TemporalStorageService<String, UUID,E> {

    private final Function1<Event, Tuple2<Instant,V>> delegate;
    private final Function3<Event,Instant,V,E> entitySupplier;

    public GenericTemporalProjectionStorageService(final Metrics metrics
            , final TemporalProjectionRepository<String, UUID, E> repository
            , final Function1<Event, Tuple2<Instant, V>> delegate
            , final Function3<Event, Instant, V, E> entitySupplier) {
        super(metrics, repository);
        this.delegate = delegate;
        this.entitySupplier = entitySupplier;
    }


    private E wrap(final Event e, final Tuple2<Instant,V> payload) {
        return entitySupplier.apply(e,payload._1,payload._2);
    }

    @Override
    protected void createHandlers(final PatternMatcher.Builder<Event, E> builder) {
        builder.match(t -> true,e -> wrap(e,delegate.apply(e)));
    }

}
