package gov.scot.payments.application.component.projector.temporal.springdata.jpa;

import gov.scot.payments.application.component.projector.temporal.springdata.TemporalQueryResource;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.user.Subject;
import io.vavr.control.Option;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.UUID;

public abstract class GenericTemporalProjectionQueryResource<V extends HasKey<String>,E extends GenericTemporalProjection<V>> extends TemporalQueryResource<String, UUID,E> {

    private final PagedResourcesAssembler<V> assembler;
    private final GenericTemporalProjectionJpaRepository<V,E> repository;

    public GenericTemporalProjectionQueryResource(GenericTemporalProjectionJpaRepository<V,E> repository, PagedResourcesAssembler<V> assembler) {
        super(repository, (PagedResourcesAssembler)assembler);
        this.assembler = assembler;
        this.repository = repository;
    }

    @GetMapping("/{id}/latest/payload")
    @ResponseBody
    public Mono<ResponseEntity<V>> getLatestPayloadById(@PathVariable String id
            , @RequestParam(name = "processingTime",required = false) Instant processingTime
            , @RequestParam(name = "eventTime",required = false) Instant eventTime
            , @AuthenticationPrincipal Subject subject){
        Instant now = Instant.now();
        Instant processingToUse = Option.of(processingTime).getOrElse(now);
        Instant eventToUse = Option.of(eventTime).getOrElse(now);
        return Mono.fromCallable(() -> repository.findLatestById(id
                ,processingToUse
                ,eventToUse
                , PageRequest.of(0, 1, Sort.by(Sort.Direction.DESC, "processingTime"))
                , subject))
                    .flatMap(p -> Mono.justOrEmpty(p.stream().findFirst()))
                   .map(GenericTemporalProjection::getPayload)
                   .map(ResponseEntity::ok)
                   .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping("/{id}/payload")
    @ResponseBody
    public Mono<ResponseEntity<PagedModel<EntityModel<V>>>> getAllPayloadsById(@PathVariable String id
            , @RequestParam(name = "processingTimeFrom",required = false) Instant processingTimeFrom
            , @RequestParam(name = "eventTimeFrom",required = false) Instant eventTimeFrom
            , @RequestParam(name = "processingTimeTo",required = false) Instant processingTimeTo
            , @RequestParam(name = "eventTimeTo",required = false) Instant eventTimeTo
            , @PageableDefault(size = 100,sort = "processingTime",direction = Sort.Direction.DESC) Pageable paging
            , @AuthenticationPrincipal Subject subject){
        Instant now = Instant.now();
        Instant processingFromToUse = Option.of(processingTimeFrom).getOrElse(Instant.EPOCH);
        Instant eventFromToUse = Option.of(eventTimeFrom).getOrElse(Instant.EPOCH);
        Instant processingToToUse = Option.of(processingTimeTo).getOrElse(now);
        Instant eventToToUse = Option.of(eventTimeTo).getOrElse(now);
        return Mono.fromCallable(() -> repository.findAllById(id
                ,processingFromToUse
                ,processingToToUse
                ,eventFromToUse
                ,eventToToUse
                ,paging
                ,subject)
                .map(GenericTemporalProjection::getPayload))
                .map(assembler::toModel)
                   .map(ResponseEntity::ok)
                   .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    /*
    TODO need to create query methods in repository for PG11
    @GetMapping("/query/latest")
    @ResponseBody
    public Mono<ResponseEntity<PagedModel<EntityModel<V>>>> queryLatestPayload(@RequestParam(name = "query") String jsonPath
            , @RequestParam(name = "processingTime",required = false) LocalDateTime processingTime
            , @RequestParam(name = "eventTime",required = false) LocalDateTime eventTime
            , @PageableDefault(size = 100,sort = "processingTime",direction = Sort.Direction.DESC) Pageable paging){
        LocalDateTime processingToUse = Option.of(processingTime).getOrElse(LocalDateTime.now());
        LocalDateTime eventToUse = Option.of(eventTime).getOrElse(LocalDateTime.now());
        return Mono.fromCallable(() -> repository.queryLatestByPayloadContent(jsonPath,processingTime,eventTime,paging)
                .map(GenericTemporalEntity::getPayload))
                .map(p -> assembler.toModel(p))
                   .map(ResponseEntity::ok)
                   .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping("/query")
    @ResponseBody
    public Mono<ResponseEntity<PagedModel<EntityModel<V>>>> queryAllPayloads(@RequestParam(name = "query") String jsonPath
            , @RequestParam(name = "processingTimeFrom",required = false) LocalDateTime processingTimeFrom
            , @RequestParam(name = "eventTimeFrom",required = false) LocalDateTime eventTimeFrom
            , @RequestParam(name = "processingTimeTo",required = false) LocalDateTime processingTimeTo
            , @RequestParam(name = "eventTimeTo",required = false) LocalDateTime eventTimeTo
            , @PageableDefault(size = 100,sort = "processingTime",direction = Sort.Direction.DESC) Pageable paging){
        LocalDateTime processingFromToUse = Option.of(processingTimeFrom).getOrElse(LocalDateTime.now());
        LocalDateTime eventFromToUse = Option.of(eventTimeFrom).getOrElse(LocalDateTime.now());
        LocalDateTime processingToToUse = Option.of(processingTimeTo).getOrElse(LocalDateTime.now());
        LocalDateTime eventToToUse = Option.of(eventTimeTo).getOrElse(LocalDateTime.now());
        return Mono.fromCallable(() -> repository.queryAllByPayloadContent(jsonPath,processingFromToUse,processingToToUse,eventFromToUse,eventToToUse,paging)
                .map(GenericTemporalEntity::getPayload))
                .map(p -> assembler.toModel(p))
                   .map(ResponseEntity::ok)
                   .defaultIfEmpty(ResponseEntity.notFound().build());
    }
     */
}
