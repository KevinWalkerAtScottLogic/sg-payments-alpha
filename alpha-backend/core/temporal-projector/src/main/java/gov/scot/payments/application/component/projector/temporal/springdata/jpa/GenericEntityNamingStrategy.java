package gov.scot.payments.application.component.projector.temporal.springdata.jpa;

import lombok.RequiredArgsConstructor;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.util.StringUtils;

@RequiredArgsConstructor
public class GenericEntityNamingStrategy extends SpringPhysicalNamingStrategy {

    private final String entityName;

    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        Identifier toUse = name;
        if(name.getText().equals("GenericTemporalEntity")){
            toUse = Identifier.toIdentifier(StringUtils.capitalize(entityName));
        }
        return super.toPhysicalTableName(toUse, jdbcEnvironment);
    }
}
