package gov.scot.payments.application.component.projector.temporal.springdata.jpa;

import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.TemporalProjection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.time.Instant;
import java.util.UUID;

@MappedSuperclass
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public abstract class GenericTemporalProjection<T extends HasKey<String>> implements TemporalProjection<String,UUID> {

    public abstract T getPayload();
    public abstract void setPayload(T payload);
    private String logicalId;
    @LastModifiedDate
    private Instant processingTime;
    private Instant eventTime;
    @NonNull @Id
    @Builder.Default private UUID id  = UUID.randomUUID();

}
