package gov.scot.payments.application.component.projector.temporal.springdata;

import gov.scot.payments.application.component.projector.springdata.ProjectionRepository;
import gov.scot.payments.model.TemporalProjection;
import gov.scot.payments.model.user.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

import java.time.Instant;

@NoRepositoryBean
public interface TemporalProjectionRepository<K, PK,T extends TemporalProjection<K,PK>> extends ProjectionRepository<PK,T> {

    Page<T> findAllById(K id, Instant processingTimeFrom
            , Instant processingTimeTo
            , Instant eventTimeFrom
            , Instant eventTimeTo
            , Pageable pageable
            , Subject subject);

}
