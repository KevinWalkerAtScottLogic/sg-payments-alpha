package gov.scot.payments.application.component.projector.temporal.springdata.jpa;


import gov.scot.payments.application.component.projector.temporal.springdata.TemporalProjectionRepository;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.user.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import java.time.Instant;
import java.util.UUID;

@NoRepositoryBean
public interface GenericTemporalProjectionJpaRepository<V extends HasKey<String>,E extends GenericTemporalProjection<V>> extends TemporalProjectionRepository<String, UUID,E>, JpaRepository<E,UUID> {

    String LATEST_BY_ID_QUERY = "select e from GenericTemporalEntity e where e.logicalId = ?1 and e.processingTime < ?2 and e.eventTime < ?3 ";
    String ALL_BY_ID_QUERY = "select e from GenericTemporalEntity e where e.logicalId = ?1 and e.processingTime > ?2 and e.processingTime <= ?3 and e.eventTime > ?4 and e.eventTime <= ?5 ";

    String TEMPORAL_ORDER = " order by e.processingTime desc";

    @Query(LATEST_BY_ID_QUERY + TEMPORAL_ORDER)
    Page<E> findLatestById(String id
            , Instant processingTime
            , Instant eventTime
            , Pageable pageable
            , Subject subject);

    @Override
    @Query(ALL_BY_ID_QUERY + TEMPORAL_ORDER)
    Page<E> findAllById(String id
            , Instant processingTimeFrom
            , Instant processingTimeTo
            , Instant eventTimeFrom
            , Instant eventTimeTo
            , Pageable pageable
            , Subject subject);

    //TODO: native queries using PG11 json path support
    /*
    @Query("")
    Page<GenericTemporalEntity<V>> queryLatestByPayloadContent(String jsonPath,Instant processingTime, Instant eventTime, Pageable pageable);

    @Query("")
    Page<GenericTemporalEntity<V>> queryAllByPayloadContent(String jsonPath
            ,Instant processingTimeFrom
            , Instant processingTimeTo
            , Instant eventTimeFrom
            , Instant eventTimeTo
            , Pageable pageable);

     */

}
