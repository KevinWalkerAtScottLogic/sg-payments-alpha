package gov.scot.payments.application.component.projector.temporal.springdata;

import gov.scot.payments.model.TemporalProjection;
import gov.scot.payments.model.user.Subject;
import io.vavr.control.Option;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.time.Instant;

public abstract class TemporalQueryResource<K, PK,T extends TemporalProjection<K,PK>> {

    protected final TemporalProjectionRepository<K,PK,T> repository;
    private final PagedResourcesAssembler<T> assembler;

    public TemporalQueryResource(TemporalProjectionRepository<K, PK, T> repository, PagedResourcesAssembler<T> assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    @GetMapping("/{id}/latest")
    @ResponseBody
    public Mono<ResponseEntity<T>> getLatestById(@PathVariable K id
            , @RequestParam(name = "processingTime",required = false) Instant processingTime
            , @RequestParam(name = "eventTime",required = false) Instant eventTime
            , @AuthenticationPrincipal Subject subject){
        Instant now = Instant.now();
        Instant processingToUse = Option.of(processingTime).getOrElse(now);
        Instant eventToUse = Option.of(eventTime).getOrElse(now);
        return Mono.fromCallable(() -> repository.findAllById(id
                ,processingToUse
                , Instant.EPOCH
                , eventToUse
                ,Instant.EPOCH
                , PageRequest.of(0, 1, Sort.by(Sort.Direction.DESC, "processingTime"))
                , subject))
                   .flatMap(p -> Mono.justOrEmpty(p.stream().findFirst()))
                   .map(ResponseEntity::ok)
                   .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Mono<ResponseEntity<PagedModel<EntityModel<T>>>> getAllById(@PathVariable K id
            , @RequestParam(name = "processingTimeFrom",required = false) Instant processingTimeFrom
            , @RequestParam(name = "eventTimeFrom",required = false) Instant eventTimeFrom
            , @RequestParam(name = "processingTimeTo",required = false) Instant processingTimeTo
            , @RequestParam(name = "eventTimeTo",required = false) Instant eventTimeTo
            , @PageableDefault(size = 100,sort = "processingTime",direction = Sort.Direction.DESC) Pageable paging
            , @AuthenticationPrincipal Subject subject){
        Instant now = Instant.now();
        Instant processingFromToUse = Option.of(processingTimeFrom).getOrElse(Instant.EPOCH);
        Instant eventFromToUse = Option.of(eventTimeFrom).getOrElse(Instant.EPOCH);
        Instant processingToToUse = Option.of(processingTimeTo).getOrElse(now);
        Instant eventToToUse = Option.of(eventTimeTo).getOrElse(now);

        return Mono.fromCallable(() -> assembler.toModel(repository.findAllById(id
                ,processingFromToUse
                ,processingToToUse
                ,eventFromToUse
                ,eventToToUse
                ,paging
                ,subject)))
                   .map(ResponseEntity::ok)
                   .defaultIfEmpty(ResponseEntity.notFound().build());
    }

}
