package gov.scot.payments.application.component.projector.temporal.springdata.jpa;

import gov.scot.payments.application.component.projector.ProjectorApplication;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.application.security.SecurityCustomizer;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.HasKey;
import io.vavr.Function1;
import io.vavr.Function3;
import io.vavr.Tuple2;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.HttpMethod;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.Instant;
import java.util.UUID;

@PropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "application-projector-jpa.properties")
public abstract class JpaTemporalProjectorApplication<V extends HasKey<String>,E extends GenericTemporalProjection<V>> extends ProjectorApplication<UUID,E> {

    //define payload
    //define entity
    //define repository
    //define delegate function

    @Value("${projection.name}")
    private String projectionName;

    @Bean
    public PhysicalNamingStrategy physicalNamingStrategy() {
        return new GenericEntityNamingStrategy(projectionName+"TemporalEntity");
    }

    @Bean
    public GenericTemporalProjectionStorageService<V,E> temporalEntityStorageService(Metrics metrics
            , final GenericTemporalProjectionJpaRepository<V,E> repository
            , final Function1<Event, Tuple2<Instant,V>> delegate
            , final Function3<Event, Instant, V, E> entitySupplier){
        return new GenericTemporalProjectionStorageService<>(metrics, repository, delegate, entitySupplier);
    }

    @Bean
    public QueryResource temporalEntityQueryResource(GenericTemporalProjectionJpaRepository<V,E> repository, PagedResourcesAssembler<V> assembler){
        return new QueryResource(repository,assembler);
    }

    @Bean
    public SecurityCustomizer projectionServiceSecurity(@Value("${projection.query.authority:}") String authority){
        return r -> {
            if(!StringUtils.isEmpty(authority)){
                r.pathMatchers(HttpMethod.GET,"/**").hasAuthority(authority);
            }
        };
    }

    @RequestMapping
    public class QueryResource extends GenericTemporalProjectionQueryResource<V,E> {
        public QueryResource(GenericTemporalProjectionJpaRepository<V,E> repository, PagedResourcesAssembler<V> assembler) {
            super(repository,assembler);
        }
    }

}
