CREATE TABLE @projectTableName@ (
    id VARCHAR(255) NOT NULL PRIMARY KEY,
    processing_time TIMESTAMP NOT NULL
);

CREATE INDEX @projectTableName@_id ON @projectTableName@(id);
CREATE INDEX @projectTableName@_processing_time ON @projectTableName@(processing_time DESC);
