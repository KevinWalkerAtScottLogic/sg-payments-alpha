CREATE domain IF NOT EXISTS jsonb AS other;

CREATE TABLE @projectTableName@ (
    id VARCHAR(255) NOT NULL PRIMARY KEY,
    processing_time TIMESTAMP NOT NULL
);