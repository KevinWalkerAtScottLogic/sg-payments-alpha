package @group@.@namePackage@.app;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import gov.scot.payments.application.kafka.StreamJoiner;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.kafka.HeaderPreservingCompositeNonNativeSerde;
import gov.scot.payments.application.kafka.KeyValueWithHeaders;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.model.Event;
import org.springframework.context.annotation.Bean;
import gov.scot.payments.application.component.projector.springdata.RepositoryMutatingStorageService;
import gov.scot.payments.application.component.projector.springdata.jpa.JpaMutatingProjectorApplication;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import @group@.@namePackage@.model.@projectName.capitalize()@;

import java.util.function.Predicate;

@EnableAutoConfiguration(exclude = {HypermediaAutoConfiguration.class})
@EnableJpaRepositories(basePackageClasses = @projectName.capitalize()@Repository.class)
@EntityScan(basePackageClasses = @projectName.capitalize()@.class)
@ApplicationComponent
public class @projectName.capitalize()@ProjApplication extends JpaMutatingProjectorApplication<String,@projectName.capitalize()@> {

        /*
    @Bean
    public StreamJoiner<Event,Event,Event> joiner(SchemaRegistryClient confluentSchemaRegistryClient){
        HeaderPreservingCompositeNonNativeSerde<Event> eventSerde = valueSerde(Event.class);
        return StreamJoiner.<Event, Event, Event>builder()
            .repartitionTopicName()
            .joinWindow()
            .joiner()
            .rekeyFunction()
            .leftValueSerde(eventSerde)
            .rightValueSerde(eventSerde)
            .stateSerde(stateSerde(confluentSchemaRegistryClient,SerializedMessage.class))
            .build();
    }
     */

        @Bean
        public RepositoryMutatingStorageService<String,@projectName.capitalize()@> handler(Metrics metrics, @projectName.capitalize()@Repository repository){
            return new @projectName.capitalize()@StorageService(metrics,repository);
        }

        public static void main(String[] args){
            BaseApplication.run(@projectName.capitalize()@ProjApplication.class,args);
        }
}