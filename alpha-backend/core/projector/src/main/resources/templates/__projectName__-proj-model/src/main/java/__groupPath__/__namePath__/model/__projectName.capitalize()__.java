package @group@.@namePackage@.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.Instant;

import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.Projection;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode(of = "id")
public class @projectName.capitalize()@ implements Projection<String>, HasKey<String>{

    @NonNull @Id private String id;

    private Instant processingTime;

    @Override
    @JsonIgnore
    public String getKey(){
        return id;
    }
}