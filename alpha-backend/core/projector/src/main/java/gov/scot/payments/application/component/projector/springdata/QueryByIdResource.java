package gov.scot.payments.application.component.projector.springdata;

import gov.scot.payments.model.Projection;
import gov.scot.payments.model.user.Subject;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

public abstract class QueryByIdResource<ID,T extends Projection<ID>> {

    private final ProjectionRepository<ID,T> repository;
    protected final PagedResourcesAssembler<T> assembler;

    public QueryByIdResource(ProjectionRepository<ID,T> repository, PagedResourcesAssembler<T> assembler){
        this.repository = repository;
        this.assembler = assembler;
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<T>> getById(@PathVariable ID id, @AuthenticationPrincipal Subject subject){
        return Mono.defer(() -> Mono.justOrEmpty(repository.findById(id,subject)))
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping
    @ResponseBody
    public Mono<ResponseEntity<PagedModel<EntityModel<T>>>> all(@PageableDefault(size = 100,sort = "processingTime",direction = Sort.Direction.DESC) Pageable paging
            , @AuthenticationPrincipal Subject subject){
        return Mono.fromCallable(() -> assembler.toModel(repository.findAll(paging,subject)))
                   .map(ResponseEntity::ok);
    }

}
