package gov.scot.payments.application.component.projector.springdata.jpa;

import gov.scot.payments.application.component.projector.ConsumingProjectorApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.util.ResourceUtils;


@PropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "application-projector-jpa.properties")
public abstract class JpaConsumingProjectorApplication extends ConsumingProjectorApplication {

    //storage service
}
