package gov.scot.payments.application.component.projector;

import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Event;
import io.vavr.Tuple2;
import io.vavr.Value;
import io.vavr.control.Option;
import io.vavr.control.Try;

public abstract class SingleEntityCrudStorageService<T,ID> {

    protected final Metrics metrics;
    private final PatternMatcher<Tuple2<Event,String>,ID> idExtractor;
    private final PatternMatcher<Event,T> createHandlers;
    private final PatternMatcher<Tuple2<Event,T>,T> updateHandlers;

    protected abstract boolean shouldDelete(Event event);
    protected abstract void idExtractors(PatternMatcher.Builder<Tuple2<Event,String>,ID> builder);
    protected abstract void createEventHandlers(PatternMatcher.Builder<Event,T> builder);
    protected abstract void updateEventHandlers(PatternMatcher.Builder<Tuple2<Event,T>,T> builder);

    protected abstract boolean delete(ID id);
    protected abstract Option<T> find(ID id);
    protected abstract T upsert(T state);

    public SingleEntityCrudStorageService(Metrics metrics) {
        this.metrics = metrics;
        idExtractor = PatternMatcher.<Tuple2<Event,String>,ID>builder().apply(this::idExtractors).build();
        createHandlers = PatternMatcher.<Event,T>builder().apply(this::createEventHandlers).build();
        updateHandlers = PatternMatcher.<Tuple2<Event,T>,T>builder().apply(this::updateEventHandlers).build();
    }

    //need whole method to be transactional in case of jpa & kafka - see example for how this is done with streams
    @SuppressWarnings("unchecked")
    public Tuple2<String,T> apply(String key, Event event){
        return idExtractor.option(new Tuple2<>(event,key))
                .map(id -> {
                    if(shouldDelete(event)){
                        boolean deleted = metrics.execute("storage.delete",() -> delete(id));
                        return deleted ? new Tuple2<String,T>(key,null) : null;
                    }
                    return Try.ofSupplier(metrics.time("storage.find",() -> find(id)))
                            .map(o -> o.fold(() -> createProjection(event), s -> updateProjection(s,event)))
                            .map(o -> o.map(metrics.time("storage.upsert",v -> new Tuple2(key,upsert(v)))))
                            .map(Value::getOrNull)
                            .get();
                })
                .getOrNull();
    }


    private Option<T> updateProjection(T current, final Event event){
        return updateHandlers.option(new Tuple2<>(event,current));
    }

    private Option<T> createProjection(final Event event){
        return createHandlers.option(event);
    }


}
