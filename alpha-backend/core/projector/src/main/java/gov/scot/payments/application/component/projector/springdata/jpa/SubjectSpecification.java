package gov.scot.payments.application.component.projector.springdata.jpa;

import gov.scot.payments.model.user.Subject;
import io.vavr.Function2;
import io.vavr.Function3;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@RequiredArgsConstructor
public abstract class SubjectSpecification<T> implements Specification<T> {

    private final Subject subject;

    protected abstract Predicate test(final CriteriaBuilder criteriaBuilder, final Root<T> root, final Subject subject);

    @Override
    public Predicate toPredicate(final Root<T> root, final CriteriaQuery<?> query, final CriteriaBuilder criteriaBuilder) {
        return test(criteriaBuilder,root,subject);
    }

    public static <T> Specification<T> of(Subject subject,Function3<CriteriaBuilder,Root<T>,Subject,Predicate> f){
        return new SubjectSpecification<T>(subject) {
            @Override
            protected Predicate test(final CriteriaBuilder criteriaBuilder, final Root<T> root, final Subject subject) {
                return f.apply(criteriaBuilder,root,subject);
            }
        };
    }

    public static <T> Specification<T> any(){
        return Specification.where(null);
    }
}
