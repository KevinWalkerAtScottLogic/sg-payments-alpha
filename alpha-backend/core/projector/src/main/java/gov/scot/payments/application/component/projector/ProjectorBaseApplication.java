package gov.scot.payments.application.component.projector;

import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.component.projector.springdata.PageableHandlerMethodArgumentResolver;
import gov.scot.payments.application.kafka.KeyValueWithHeaders;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.Projection;
import org.apache.kafka.common.header.Headers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.support.WebStack;
import org.springframework.util.ResourceUtils;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.reactive.result.method.annotation.ArgumentResolverConfigurer;

import java.util.function.Predicate;

@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL,stacks = WebStack.WEBFLUX)
@PropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "application-consuming-projector.properties")
public abstract class ProjectorBaseApplication<K,T extends Projection<K>> extends BaseApplication<Event,T> implements WebFluxConfigurer {

    @Override
    public void configureArgumentResolvers(final ArgumentResolverConfigurer configurer) {
        configurer.addCustomResolver(new PageableHandlerMethodArgumentResolver());
    }

    @Bean
    public Predicate<Headers> defaultExternalEventHeaderFilter(){
        return h -> true;
    }

    @Bean
    public Predicate<KeyValueWithHeaders<byte[],Event>> defaultExternalEventFilter(){
        return s -> true;
    }

    @Bean
    public ErrorHandler defaultErrorHandler(){
        return (e,t) -> {};
    }

}
