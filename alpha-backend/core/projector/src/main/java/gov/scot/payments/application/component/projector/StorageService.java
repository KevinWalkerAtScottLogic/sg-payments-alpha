package gov.scot.payments.application.component.projector;

import gov.scot.payments.model.Event;
import gov.scot.payments.model.Projection;
import io.vavr.Tuple2;

import java.util.function.BiFunction;

public interface StorageService<K,T extends Projection<K>> extends BiFunction<String, Event, Tuple2<String,T>> {
}
