package gov.scot.payments.application.component.projector;

import gov.scot.payments.model.Event;
import gov.scot.payments.model.Projection;
import org.apache.kafka.streams.kstream.KStream;

import java.util.function.Function;

public interface ProjectorDelegate<K,T extends Projection<K>> extends Function<KStream<byte[], Event>, KStream<byte[],T>> {
}
