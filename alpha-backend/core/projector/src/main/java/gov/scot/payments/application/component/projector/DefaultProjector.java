package gov.scot.payments.application.component.projector;

import gov.scot.payments.kafka.EmptyStream;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.Projection;
import io.vavr.Tuple2;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.SendTo;

import static gov.scot.payments.application.kafka.RichKStream.rich;

@Slf4j
@SuperBuilder
public class DefaultProjector<K,T extends Projection<K>> extends Projector<K,T>{

    @StreamListener
    @SendTo("output")
    public KStream<byte[],T> handle(@Input("events-internal") KStream<byte[], Event> internalEvents
            , @Input("events-external") KStream<byte[], Event> externalEvents) throws Exception {
        final KStream<byte[], Event> mergedEvents;
        if(externalEvents instanceof EmptyStream){
            mergedEvents = internalEvents;
        } else{
            mergedEvents = internalEvents.merge(rich(externalEvents).richFilter(externalEventFilter).unwrap());
        }
        return rich(handle(mergedEvents,"handle"))
                .richMap(kvh -> {
                    if(kvh.value instanceof HasKey){
                        Object key = ((HasKey)kvh.value).getKey();
                        kvh.headers.remove(HasKey.PARTITION_KEY_HEADER);
                        if(key != null){
                            byte[] sKey = key.toString().getBytes();
                            kvh.headers.add(HasKey.PARTITION_KEY_HEADER,sKey);
                            return new Tuple2<>(sKey,kvh.value);
                        } else {
                            return new Tuple2<>(null,kvh.value);
                        }
                    } else {
                        return new Tuple2<>(kvh.key,kvh.value);
                    }
                });
    }

}
