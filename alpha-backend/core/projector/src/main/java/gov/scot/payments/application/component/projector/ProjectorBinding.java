package gov.scot.payments.application.component.projector;

import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Output;

public interface ProjectorBinding extends ConsumingProjectorBinding {

    @Output("output")
    KStream<?,?> output();

}
