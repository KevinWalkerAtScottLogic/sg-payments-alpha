package gov.scot.payments.application.component.projector;

import gov.scot.payments.model.Event;
import org.apache.kafka.streams.kstream.KStream;

import java.util.function.Consumer;

public interface ConsumingProjectorDelegate extends Consumer<KStream<byte[], Event>> {
}
