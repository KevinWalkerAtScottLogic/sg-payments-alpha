package gov.scot.payments.application.component.projector;

import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.Projection;
import io.vavr.Tuple2;
import lombok.experimental.SuperBuilder;
import org.springframework.transaction.annotation.Transactional;

@SuperBuilder
public abstract class AbstractStorageService<K,T extends Projection<K>> implements StorageService<K,T>{

    protected abstract Tuple2<String,T> doApply(String key, Event event);

    protected final Metrics metrics;

    public AbstractStorageService(Metrics metrics) {
        this.metrics = metrics;
    }

    @Transactional("transactionManager")
    @Override
    public Tuple2<String,T> apply(String key, Event event){
        return doApply(key, event);
    }

}
