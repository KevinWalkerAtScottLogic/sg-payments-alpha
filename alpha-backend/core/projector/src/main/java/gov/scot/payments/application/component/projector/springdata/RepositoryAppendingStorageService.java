package gov.scot.payments.application.component.projector.springdata;

import gov.scot.payments.application.component.projector.AppendingStorageService;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.Projection;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import java.util.function.Consumer;

@Slf4j
@SuperBuilder
public abstract class RepositoryAppendingStorageService<ID,T extends Projection<ID>> extends AppendingStorageService<ID,T> {

    private final ProjectionRepository<ID,T> repository;

    public RepositoryAppendingStorageService(Metrics metrics, ProjectionRepository<ID,T> repository) {
        super(metrics);
        this.repository = repository;
    }

    @Override
    @Transactional("transactionManager")
    protected T insert(final T state) {
        log.debug("Inserting {}",state);
        return repository.save(state);
    }

    public static <ID,T extends Projection<ID>> RepositoryAppendingStorageService<ID,T> patternMatching(Metrics metrics
            , ProjectionRepository<ID,T> repository
            , Consumer<PatternMatcher.Builder<Event,T>> c){
        return new RepositoryAppendingStorageService<>(metrics,repository) {
            @Override
            protected void createHandlers(PatternMatcher.Builder<Event, T> builder) {
                c.accept(builder);
            }
        };
    }
}
