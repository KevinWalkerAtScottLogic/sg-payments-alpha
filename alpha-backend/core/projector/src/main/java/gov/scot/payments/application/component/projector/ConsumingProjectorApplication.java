package gov.scot.payments.application.component.projector;

import gov.scot.payments.application.MessageDeDuplicator;
import gov.scot.payments.application.kafka.KeyValueWithHeaders;
import gov.scot.payments.application.kafka.StreamJoiner;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Event;
import io.vavr.collection.HashSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collection;
import java.util.function.Predicate;

@EnableBinding(ConsumingProjectorBinding.class)
public abstract class ConsumingProjectorApplication extends ProjectorBaseApplication {

    //storage service delegates
    //stream joiner
    /*
    @Bean
    @ConditionalOnMissingBean
    public StreamJoiner<Event,Event,Event> joiner(Predicate<KeyValueWithHeaders<String,Event>> externalEventFilter
            , String repartitionTopicName
            , Function1<Event,String> rekeyFunction
            , Duration joinWindow
            , Function2<Event,Event,Event> joiner
    ){
        return StreamJoiner.<Event, Event, Event>builder()
                .joinEventFilter(externalEventFilter)
                .rekeyFunction(rekeyFunction)
                .joiner(joiner)
                .joinWindow(joinWindow)
                .repartitionTopicName(repartitionTopicName)
                .build();
    }

     */

    @Bean
    public PagedResourcesAssembler assembler(@Value("${ingress.path}") String ingressPath){
        final String ingressPath1 = StringUtils.isEmpty(ingressPath) ? "/" : ingressPath;
        return new PagedResourcesAssembler<>(new HateoasPageableHandlerMethodArgumentResolver(), UriComponentsBuilder.fromPath(ingressPath1).build());
    }

    @Bean
    public ConsumingStorageService storageService(Metrics metrics, Collection<StorageService<?,?>> services){
        return new CompositeConsumingStorageService(metrics, HashSet.ofAll(services).toJavaSet());
    }

    @Bean
    @ConditionalOnMissingBean
    public ConsumingProjectorDelegate delegate(ErrorHandler errorHandler
            , ConsumingStorageService storageService
            , Metrics metrics){
            return PerEventConsumingProjectorDelegate.builder()
                                                     .errorHandler(errorHandler)
                                                     .recordTransformer(storageService)
                                                     .metrics(metrics)
                                                     .build();
    }

    @Bean
    @ConditionalOnMissingBean
    public ConsumingProjector projector(MessageDeDuplicator<Event,?>deDuplicator
            , @Autowired(required = false) StreamJoiner<Event,Event,Event> joiner
            , ConsumingProjectorDelegate delegate
            , Predicate<KeyValueWithHeaders<byte[],Event>> externalEventFilter){
        if(joiner != null){
            return JoiningConsumingProjector.builder()
                                            .joiner(joiner)
                                            .delegate(delegate)
                                            .deDuplicator(deDuplicator)
                                            .externalEventFilter(externalEventFilter)
                                            .build();
        } else {
            return DefaultConsumingProjector.builder()
                                            .delegate(delegate)
                                            .deDuplicator(deDuplicator)
                                            .externalEventFilter(externalEventFilter)
                                            .build();
        }
    }

}
