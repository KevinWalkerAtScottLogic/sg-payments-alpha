package gov.scot.payments.application.component.projector.springdata.jpa;

import gov.scot.payments.application.component.projector.springdata.ProjectionRepository;
import gov.scot.payments.model.Projection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface JpaProjectionRepository<ID,T extends Projection<ID>> extends ProjectionRepository<ID,T>, JpaRepository<T,ID>, JpaSpecificationExecutor<T> {

}
