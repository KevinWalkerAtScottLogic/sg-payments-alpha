package gov.scot.payments.application.component.projector.springdata.jpa;

import gov.scot.payments.application.component.projector.ProjectorApplication;
import gov.scot.payments.application.component.projector.springdata.ProjectionRepository;
import gov.scot.payments.application.component.projector.springdata.QueryByIdResource;
import gov.scot.payments.application.security.SecurityCustomizer;
import gov.scot.payments.model.Projection;
import gov.scot.payments.model.user.Subject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.HttpMethod;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.function.Function;

@PropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "application-projector-jpa.properties")
public abstract class JpaMutatingProjectorApplication<ID,T extends Projection<ID>> extends ProjectorApplication<ID,T> {

    //enable repositories, entity scan
    //define repository
    //define entity
    //define mutating storage service
    //aclcheck

    protected abstract Specification<T> aclCheck(Subject subject);

    @Bean
    public QueryById queryById(ProjectionRepository<ID, T> repository, PagedResourcesAssembler<T> assembler){
        return new QueryById(repository,assembler);
    }

    @Bean
    public Search search(JpaSpecificationExecutor<T> repository, PagedResourcesAssembler<T> assembler){
        return new Search(repository,assembler,this::aclCheck);
    }

    @Bean
    public SecurityCustomizer projectionServiceSecurity(@Value("${projection.query.authority:}") String authority){
        return r -> {
            if(!StringUtils.isEmpty(authority)){
                r.pathMatchers(HttpMethod.GET,"/**").hasAuthority(authority);
            }
        };
    }

    @RequestMapping
    public class QueryById extends QueryByIdResource<ID,T> {

        public QueryById(ProjectionRepository<ID, T> repository, PagedResourcesAssembler<T> assembler) {
            super(repository,assembler);
        }
    }

    @RequestMapping
    public class Search extends SearchResource<T> {

        public Search(JpaSpecificationExecutor<T> repository, PagedResourcesAssembler<T> assembler, Function<Subject, Specification<T>> aclCheck) {
            super(repository,assembler,aclCheck);
        }
    }

}
