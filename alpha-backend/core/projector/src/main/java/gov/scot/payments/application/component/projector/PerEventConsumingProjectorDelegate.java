package gov.scot.payments.application.component.projector;

import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Event;
import io.vavr.control.Try;
import lombok.Builder;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;

import java.util.function.BiConsumer;

@Slf4j
@Builder
public class PerEventConsumingProjectorDelegate implements ConsumingProjectorDelegate {

    @Builder.Default private final BiConsumer<Event,Throwable> errorHandler = (e, t) -> {};
    @NonNull private final BiConsumer<String,Event> recordTransformer;
    @NonNull private final Metrics metrics;

    @Override
    public void accept(final KStream<byte[], Event> stream) {
        stream
                .foreach(this::wrap);
    }

    private void wrap(final byte[] key, final Event event) {
        log.debug("Handling event: {}",event);
        Try.ofSupplier(metrics.time("projection.update",() -> {
            recordTransformer.accept(key == null ? null : new String(key), event);
            return null;
        })).onFailure(e -> log.warn("Handling error: ",e))
                  .onFailure(e -> metrics.increment("projection.update.error"))
                  .onFailure(e -> errorHandler.accept(event,e))
                  .get();
    }
}
