package gov.scot.payments.application.component.projector;

import gov.scot.payments.application.MessageDeDuplicator;
import gov.scot.payments.application.kafka.KeyValueWithHeaders;
import gov.scot.payments.application.kafka.RichKStream;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.Projection;
import lombok.Builder;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;

import java.util.function.Predicate;

@Slf4j
@SuperBuilder
public abstract class Projector<K,T extends Projection<K>> {

    @NonNull private final MessageDeDuplicator<Event,T> deDuplicator;
    @NonNull private final ProjectorDelegate<K,T> delegate;
    @Builder.Default protected final Predicate<KeyValueWithHeaders<byte[], Event>> externalEventFilter = v -> true;

    protected final KStream<byte[],T> handle(KStream<byte[], Event> events, String method) throws Exception {
        return deDuplicator.deduplicate(events,delegate,method);
    }

}
