package gov.scot.payments.application.component.projector;

import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Event;
import io.vavr.collection.HashSet;
import lombok.Singular;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@SuperBuilder
public class CompositeConsumingStorageService extends AbstractConsumingStorageService {

    @Singular private final Set<StorageService<?,?>> delegates;

    public CompositeConsumingStorageService(Metrics metrics, Set<StorageService<?,?>> delegates) {
        super(metrics);
        this.delegates = delegates;
    }

    public CompositeConsumingStorageService(Metrics metrics, StorageService<?,?>... delegates) {
        this(metrics, HashSet.of(delegates).toJavaSet());
    }

    @Override
    protected void doAccept(String key, Event event) {
        delegates.forEach(s -> s.apply(key, event));
    }
}
