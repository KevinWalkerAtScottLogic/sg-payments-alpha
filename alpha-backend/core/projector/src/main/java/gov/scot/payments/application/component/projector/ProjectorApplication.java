package gov.scot.payments.application.component.projector;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.application.MessageDeDuplicator;
import gov.scot.payments.application.VavrModelResolver;
import gov.scot.payments.application.kafka.KeyValueWithHeaders;
import gov.scot.payments.application.kafka.StreamJoiner;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.Projection;
import io.swagger.v3.core.converter.AnnotatedType;
import io.swagger.v3.core.converter.ModelConverter;
import io.swagger.v3.core.converter.ModelConverterContext;
import io.swagger.v3.core.converter.ModelConverterContextImpl;
import io.swagger.v3.oas.models.media.ArraySchema;
import io.swagger.v3.oas.models.media.Schema;
import org.springdoc.api.OpenApiCustomiser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;
import java.util.function.Predicate;

@EnableBinding(ProjectorBinding.class)
@PropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "application-projector.properties")
public abstract class ProjectorApplication<K, T extends Projection<K>> extends ProjectorBaseApplication<K,T> {

    //storage service

    protected abstract Class<T> projectionClass();

    /*
    @Bean
    @ConditionalOnMissingBean
    public StreamJoiner<Event,Event,Event> joiner(Predicate<KeyValueWithHeaders<String,Event>> externalEventFilter
            , String repartitionTopicName
            , Function1<Event,String> rekeyFunction
            , Duration joinWindow
            , Function2<Event,Event,Event> joiner
    ){
        return StreamJoiner.<Event, Event, Event>builder()
                .joinEventFilter(externalEventFilter)
                .rekeyFunction(rekeyFunction)
                .joiner(joiner)
                .joinWindow(joinWindow)
                .repartitionTopicName(repartitionTopicName)
                .build();
    }

     */

    @Bean
    public OpenApiCustomiser projectionApiCustomizer(ObjectMapper mapper){
        ModelConverter converter = new VavrModelResolver(mapper);
        ModelConverterContext converterContext = new ModelConverterContextImpl(java.util.List.of(converter));
        return api -> {
            Map<String,Schema> schemas = api.getComponents().getSchemas();
            schemas.remove("EntityModelProjectionObject");
            schemas.remove("ProjectionObject");
            schemas.remove("MonoResponseEntityProjectionObject");

            Schema projectionSchema = converterContext.resolve(new AnnotatedType().type(projectionClass()));
            final Schema projectionSchemaRef = new Schema<>().$ref("#/components/schemas/"+projectionSchema.getName());

            Schema pagedProjection = schemas.get("PagedModelEntityModelProjectionObject");
            ArraySchema content = (ArraySchema)pagedProjection.getProperties().get("content");
            content.setItems(projectionSchemaRef);
            converterContext.getDefinedModels().forEach((key, value) -> api.getComponents().addSchemas(key, value));

            api.getPaths().get("/{id}").getGet().getResponses().get("200").getContent().get("*/*").setSchema(projectionSchemaRef);
        };
    }

    @Bean
    public PagedResourcesAssembler<T> assembler( @Value("${ingress.path}") String ingressPath){
        final String ingressPath1 = StringUtils.isEmpty(ingressPath) ? "/" : ingressPath;
        return new PagedResourcesAssembler<>(new HateoasPageableHandlerMethodArgumentResolver(), UriComponentsBuilder.fromPath(ingressPath1).build());
    }

    @Bean
    @ConditionalOnMissingBean
    public ProjectorDelegate<K, T> delegate(ErrorHandler errorHandler
            , StorageService<K,T> storageService
            , Metrics metrics) {
        return PerEventProjectorDelegate.<K, T>builder()
                .errorHandler(errorHandler)
                .recordTransformer(storageService)
                .metrics(metrics)
                .build();
    }

    @Bean
    @ConditionalOnMissingBean
    public Projector<K,T> projector(MessageDeDuplicator<Event,T> deDuplicator
            , @Autowired(required = false) StreamJoiner<Event,Event,Event> joiner
            , ProjectorDelegate<K, T> delegate
            , Predicate<KeyValueWithHeaders<byte[],Event>> externalEventFilter){
        if(joiner != null){
            return JoiningProjector.<K,T>builder()
                    .joiner(joiner)
                    .delegate(delegate)
                    .deDuplicator(deDuplicator)
                    .externalEventFilter(externalEventFilter)
                    .build();
        } else {
            return DefaultProjector.<K,T>builder()
                    .delegate(delegate)
                    .deDuplicator(deDuplicator)
                    .externalEventFilter(externalEventFilter)
                    .build();
        }

    }

}
