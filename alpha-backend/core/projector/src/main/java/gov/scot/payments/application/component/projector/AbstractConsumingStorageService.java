package gov.scot.payments.application.component.projector;

import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Event;
import lombok.experimental.SuperBuilder;
import org.springframework.transaction.annotation.Transactional;

@SuperBuilder
public abstract class AbstractConsumingStorageService implements ConsumingStorageService {

    protected abstract void doAccept(String key, Event event);

    protected final Metrics metrics;

    public AbstractConsumingStorageService(Metrics metrics) {
        this.metrics = metrics;
    }

    @Transactional("transactionManager")
    @Override
    public void accept(String key, Event event){
        doAccept(key, event);
    }

}
