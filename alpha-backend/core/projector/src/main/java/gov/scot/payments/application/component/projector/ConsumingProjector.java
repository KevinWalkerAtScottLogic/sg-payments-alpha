package gov.scot.payments.application.component.projector;

import gov.scot.payments.application.MessageDeDuplicator;
import gov.scot.payments.application.kafka.KeyValueWithHeaders;
import gov.scot.payments.model.Event;
import lombok.Builder;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;

import java.util.function.Predicate;

@Slf4j
@SuperBuilder
public abstract class ConsumingProjector {

    @NonNull private final MessageDeDuplicator<Event,?> deDuplicator;
    @NonNull private final ConsumingProjectorDelegate delegate;
    @Builder.Default protected final Predicate<KeyValueWithHeaders<byte[], Event>> externalEventFilter = v -> true;

    protected final void handle(KStream<byte[], Event> events,String method) throws Exception {
        deDuplicator.deduplicateAndConsume(events,delegate,method);
    }

}
