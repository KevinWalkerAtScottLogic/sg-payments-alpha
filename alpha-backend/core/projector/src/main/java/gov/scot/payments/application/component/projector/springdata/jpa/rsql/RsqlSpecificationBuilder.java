package gov.scot.payments.application.component.projector.springdata.jpa.rsql;

import cz.jirutka.rsql.parser.ast.ComparisonNode;
import cz.jirutka.rsql.parser.ast.LogicalNode;
import cz.jirutka.rsql.parser.ast.LogicalOperator;
import cz.jirutka.rsql.parser.ast.Node;
import io.vavr.collection.List;
import org.springframework.data.jpa.domain.Specification;

import java.util.Objects;


public class RsqlSpecificationBuilder<T> {

    public Specification<T> createSpecification(final Node node) {
        if (node instanceof LogicalNode) {
            return createSpecification((LogicalNode) node);
        }
        if (node instanceof ComparisonNode) {
            return createSpecification((ComparisonNode) node);
        }
        return null;
    }

    public Specification<T> createSpecification(final LogicalNode logicalNode) {
        final List<Specification<T>> specs = List.ofAll(logicalNode.getChildren())
                .map(this::createSpecification)
                .filter(Objects::nonNull);

        Specification<T> result = specs.get(0);

        if (logicalNode.getOperator() == LogicalOperator.AND) {
            for (int i = 1; i < specs.size(); i++) {
                result = Specification.where(result).and(specs.get(i));
            }
        } else if (logicalNode.getOperator() == LogicalOperator.OR) {
            for (int i = 1; i < specs.size(); i++) {
                result = Specification.where(result).or(specs.get(i));
            }
        }

        return result;
    }

    public Specification<T> createSpecification(final ComparisonNode comparisonNode) {
        Specification<T> spec = new RsqlSpecification<>(comparisonNode.getSelector(), comparisonNode.getOperator(), comparisonNode.getArguments());
        return Specification.where(spec);
    }
}
