CREATE domain IF NOT EXISTS jsonb AS other;

CREATE TABLE TEST_ENTITY (
    id varchar(255) NOT NULL,
    processing_time TIMESTAMP NOT NULL,
    data VARCHAR(255) NOT NULL
);