package gov.scot.payments.application.component.projector;

import gov.scot.payments.application.MessageDeDuplicator;
import gov.scot.payments.application.component.projector.springdata.RepositoryMutatingStorageService;
import gov.scot.payments.application.component.projector.springdata.TestEntity;
import gov.scot.payments.application.component.projector.springdata.TestEntityRepository;
import gov.scot.payments.application.component.projector.springdata.TestRepositoryMutatingStorageService;
import gov.scot.payments.application.kafka.MessageDuplicateFilterFactory;
import gov.scot.payments.application.kafka.MessageHeaderEnricher;
import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.Event;
import gov.scot.payments.testing.event.TestCreateEvent;
import gov.scot.payments.testing.event.TestDeleteEvent;
import gov.scot.payments.testing.event.TestErrorEvent;
import gov.scot.payments.testing.event.TestUpdateEvent;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.collection.List;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.state.Stores;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;
import org.springframework.test.context.ContextConfiguration;

import java.time.Duration;
import java.util.function.BiConsumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@DataJpaTest(properties = {"spring.datasource.url=jdbc:h2:mem:ConsumingProjectorTest"})
@AutoConfigurationPackage
@ContextConfiguration(classes = DefaultConsumingProjectorTest.TestConfiguration.class)
class DefaultConsumingProjectorTest {

    private DefaultConsumingProjector processor;
    private KafkaStreamsTestHarness harness;
    private BiConsumer<Event,Throwable> errorHandler;

    @SpyBean
    private CompositeConsumingStorageService storageService;

    @BeforeEach
    public void setUp() throws Exception {
        final Duration windowSize = Duration.ofMinutes(10);

        errorHandler = mock(BiConsumer.class);
        BeanFactory beanFactory = mock(BeanFactory.class);
        StreamsBuilderFactoryBean factory = mock(StreamsBuilderFactoryBean.class);
        when(beanFactory.getBean(anyString(),eq(StreamsBuilderFactoryBean.class))).thenReturn(factory);

        final MicrometerMetrics metrics = new MicrometerMetrics(new SimpleMeterRegistry());
        MessageDuplicateFilterFactory duplicateFilterFactory = new MessageDuplicateFilterFactory(windowSize,KafkaStreamsTestHarness.STATE_STORE
                ,metrics
                , Stores.inMemoryWindowStore(KafkaStreamsTestHarness.STATE_STORE, windowSize, windowSize, false));
        MessageDeDuplicator deDuplicator = MessageDeDuplicator.builder()
                                                              .messageEnricher(() -> new MessageHeaderEnricher<>("test"))
                                                              .duplicateFilterFactory(duplicateFilterFactory)
                                                              .build();
        deDuplicator.setBeanFactory(beanFactory);

        PerEventConsumingProjectorDelegate delegate = PerEventConsumingProjectorDelegate.builder()
                .errorHandler(errorHandler)
                .metrics(metrics)
                .recordTransformer(storageService::accept)
                .build();

        processor = (DefaultConsumingProjector) DefaultConsumingProjector.builder()
                                                                         .deDuplicator(deDuplicator)
                                                                         .delegate(delegate)
                                                                         .build();
        harness = KafkaStreamsTestHarness.builderWithMappingsAndKeySerde(Serdes.ByteArraySerde.class,TestCreateEvent.class, TestUpdateEvent.class, TestDeleteEvent.class, TestErrorEvent.class, TestEntity.class)
                .build();

        when(factory.getObject()).thenReturn(harness.getBuilder());
        processor.handle(harness.stream(),harness.stream("external"));
    }

    @Test
    public void test(){
        try (final TopologyTestDriver topology = harness.toTopology()) {
            KeyValueWithHeaders<String,TestCreateEvent> create1 = KeyValueWithHeaders.msg("1",new TestCreateEvent("1"));
            KeyValueWithHeaders<String,TestUpdateEvent> update1 = KeyValueWithHeaders.msg("1",new TestUpdateEvent("1"));
            KeyValueWithHeaders<String,TestDeleteEvent> delete1 = KeyValueWithHeaders.msg("1",new TestDeleteEvent("1"));

            KeyValueWithHeaders<String,TestCreateEvent> create1a = KeyValueWithHeaders.msg("1",new TestCreateEvent("1"));
            KeyValueWithHeaders<String,TestUpdateEvent> update2 = KeyValueWithHeaders.msg("2",new TestUpdateEvent("2"));
            KeyValueWithHeaders<String,TestDeleteEvent> delete2 = KeyValueWithHeaders.msg("2",new TestDeleteEvent("2"));

            harness.sendKeyValues(topology,create1,update1,create1a,update2,delete1,create1,delete2);
            List<KeyValueWithHeaders<String,TestEntity>> entities = harness.drainKeyValues(topology);
            assertEquals(0,entities.size());
        }
        verify(storageService,times(6)).accept(anyString(),any(Event.class));
    }

    @Test
    public void testError(){
        try (final TopologyTestDriver topology = harness.toTopology()) {
            KeyValueWithHeaders<String,TestErrorEvent> create1 = KeyValueWithHeaders.msg("1",new TestErrorEvent("1"));

            assertThrows(RuntimeException.class,() -> harness.sendKeyValues(topology,create1));
        }
        verify(errorHandler,times(1)).accept(any(),any());
    }

    @Configuration
    public static class TestConfiguration {

        @Bean
        public CompositeConsumingStorageService compositeConsumingStorageService(RepositoryMutatingStorageService<String,TestEntity> service){
            return new CompositeConsumingStorageService(new MicrometerMetrics(new SimpleMeterRegistry()),service);
        }

        @Bean
        public RepositoryMutatingStorageService<String,TestEntity> service(TestEntityRepository repository){
            return new TestRepositoryMutatingStorageService(repository,new MicrometerMetrics(new SimpleMeterRegistry()));
        }

    }
}