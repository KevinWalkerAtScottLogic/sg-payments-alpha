package gov.scot.payments.application.component.projector.springdata;

import gov.scot.payments.application.func.Matchers;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.Event;
import gov.scot.payments.testing.event.TestCreateEvent;
import gov.scot.payments.testing.event.TestDeleteEvent;
import gov.scot.payments.testing.event.TestErrorEvent;
import gov.scot.payments.testing.event.TestEvent;
import gov.scot.payments.testing.event.TestUpdateEvent;
import io.vavr.Tuple2;

import java.time.Instant;

public class TestRepositoryMutatingStorageService extends RepositoryMutatingStorageService<String,TestEntity> {

    public TestRepositoryMutatingStorageService(TestEntityRepository repository, Metrics micrometerMetrics) {
        super(micrometerMetrics,repository);
    }

    @Override
    protected boolean shouldDelete(Event event) {
        return TestDeleteEvent.class.isAssignableFrom(event.getClass());
    }

    @Override
    protected void idExtractors(PatternMatcher.Builder2<Event,String,String> builder) {
        builder.match2(TestEvent.class, (e, k) -> e.getKey());
    }

    @Override
    protected void createHandlers(PatternMatcher.Builder<Event,TestEntity> builder) {
        builder.match(TestCreateEvent.class, e -> new TestEntity(e.getKey(), Instant.now(),e.getMessageId().toString()))
               .match(TestErrorEvent.class, e -> {throw new RuntimeException();});
    }


    @Override
    protected void updateHandlers(PatternMatcher.Builder2<Event,TestEntity,TestEntity> builder) {
        builder.match2(TestUpdateEvent.class,(e, s) -> s.merge(e));
    }
}