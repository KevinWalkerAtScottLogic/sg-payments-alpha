package gov.scot.payments.application.component.projector.springdata.jpa;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.component.projector.ProjectorApplication;
import gov.scot.payments.application.component.projector.springdata.*;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.kafka.HeaderPreservingCompositeNonNativeSerde;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.kafka.StreamJoiner;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.application.security.SecurityCustomizer;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.command.TestCreateCommand;
import gov.scot.payments.testing.command.TestDeleteCommand;
import gov.scot.payments.testing.command.TestUpdateCommand;
import gov.scot.payments.testing.event.*;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.vavr.collection.List;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.state.Stores;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.HttpMethod;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
import java.time.Instant;
import java.util.function.Predicate;

import static gov.scot.payments.testing.security.MockSubjectAuthentication.user;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockAuthentication;

@EnableAutoConfiguration(exclude = {HypermediaAutoConfiguration.class})
@ApplicationIntegrationTest(classes = {JpaProjectorIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.PROJECTOR
        ,additionalTopics = 1
        ,properties = {"spring.datasource.url=jdbc:h2:mem:JpaProjectorIntegrationTest"
        ,"component.name=test"
        ,"context.name=test"
        ,"events.destinations=test"})
@Disabled("causes issues on windows")
class JpaProjectorIntegrationTest {

    @BeforeEach
    public void setUp(EmbeddedBrokerClient brokerClient
            , @Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter){
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING,Duration.ofSeconds(10));
    }

    @Test
    public void test(EmbeddedBrokerClient brokerClient, @Autowired WebTestClient client){
        KeyValueWithHeaders<String,TestCreateEvent> create1 = KeyValueWithHeaders.msg("1",new TestCreateEvent("1"));
        KeyValueWithHeaders<String,TestUpdateEvent> update1 = KeyValueWithHeaders.msg("2",new TestUpdateEvent("2"));
        TestDeleteEvent testDeleteEvent = new TestDeleteEvent("1");
        testDeleteEvent.setCauseDetails(new TestDeleteCommand("1",true,0));
        KeyValueWithHeaders<String,TestDeleteEvent> delete1 = KeyValueWithHeaders.msg("1", testDeleteEvent);
        KeyValueWithHeaders<String,TestCreateEvent> create2 = KeyValueWithHeaders.msg("2",new TestCreateEvent("2"));
        brokerClient.sendKeyValuesToDestination("events-internal",List.of(create1,update1,create1));
        brokerClient.sendKeyValuesToDestination("events-external",List.of(delete1,create2));

        List<KeyValueWithHeaders<String,TestEntity>> actual = brokerClient.readAllKeyValuesFromDestination("output",TestEntity.class);
        assertEquals(3,actual.size());
        assertThat(actual.get(0).value).isEqualToComparingOnlyGivenFields(new TestEntity("1","1"),"id");
        assertThat(actual.get(1).value).isEqualToComparingOnlyGivenFields(new TestEntity("2","2"),"id");
        assertThat(actual.get(2).value).isEqualToComparingOnlyGivenFields(new TestEntity("1","1"),"id");

        client
                .get()
                .uri("/entities/1")
                .exchange()
                .expectStatus()
                .isUnauthorized();

        client
                .get()
                .uri("/entities/search")
                .exchange()
                .expectStatus()
                .isUnauthorized();

        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("world/entity:Write"))))
                .get()
                .uri("/entities/1")
                .exchange()
                .expectStatus()
                .isForbidden();

        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("world/entity:Write"))))
                .get()
                .uri("/entities/search")
                .exchange()
                .expectStatus()
                .isForbidden();


        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("world/entity:Read"))))
                .get()
                .uri("/entities/4")
                .exchange()
                .expectStatus()
                .isNotFound();

        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("world/entity:Read"))))
                .get()
                .uri("/entities/1")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.id").isEqualTo("1");

        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("world/entity:Read"))))
                .get()
                .uri(uri -> uri.path("/entities/search")
                        .queryParam("query","id=in=(1,2)")
                        .build())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.page.totalElements").isEqualTo(2)
                .jsonPath("$._embedded.testEntityList[0].id").isEqualTo("1")
                .jsonPath("$._embedded.testEntityList[1].id").isEqualTo("2");

        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("world/entity:Read"))))
                .get()
                .uri(uri -> uri.path("/entities/search")
                               .build())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.page.totalElements").isEqualTo(2)
                .jsonPath("$._embedded.testEntityList[0].id").isEqualTo("1")
                .jsonPath("$._embedded.testEntityList[1].id").isEqualTo("2");

        client
                .mutateWith(mockAuthentication(user("pass", Role.parse("world/entity:Read"))))
                .get()
                .uri(uri -> uri.path("/entities")
                               .build())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.page.totalElements").isEqualTo(2)
                .jsonPath("$._embedded.testEntityList[0].id").isEqualTo("1")
                .jsonPath("$._embedded.testEntityList[1].id").isEqualTo("2");

    }




    @EnableJpaRepositories(basePackageClasses = TestEntityRepository.class)
    @EntityScan(basePackageClasses = TestEntity.class)
    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends ProjectorApplication<String, TestEntity> {

        @Override
        protected Class<TestEntity> projectionClass() {
            return TestEntity.class;
        }

        @Bean
        public StreamJoiner<Event,Event,Event> joiner(@Value("${embedded.broker.additional.topic.1}") String externalEventTopic
                                                      //, ValueTransformerWithKeySupplier<String, Event,Event> eventHeaderEnricher
                , SchemaRegistryClient confluentSchemaRegistryClient){
            HeaderPreservingCompositeNonNativeSerde<Event> eventSerde = valueSerde(Event.class);
            return StreamJoiner.<Event, Event, Event>builder()
                    .repartitionTopicName(externalEventTopic)
                    .joinWindow(Duration.ofMinutes(1))
                    .joiner((e1,e2) -> e2 == null ? e1 : e2)
                    .rekeyFunction(kvh -> kvh.key)
                    .leftValueSerde(eventSerde)
                    .rightValueSerde(eventSerde)
                    .stateSerde(stateSerde(confluentSchemaRegistryClient, SerializedMessage.class))
                    .storeSupplier( (n,w) -> Stores.inMemoryWindowStore(n+"-JpaProjectorIntegrationTest-store", Duration.ofMillis(w.size() + w.gracePeriodMs()), Duration.ofMillis(w.size()), true))
                    .build();
        }

        @Bean
        @Primary
        public Predicate<gov.scot.payments.application.kafka.KeyValueWithHeaders<String,Event>> externalEventFilter(){
            return (kvh) -> !"2".equals(kvh.key);
        }

        @Bean
        public TestRepositoryAppendingStorageService handler(Metrics metrics, TestEntityRepository repository){
            return new TestRepositoryAppendingStorageService(metrics,repository);
        }

        @Bean
        public QueryById queryById(TestEntityRepository repository, PagedResourcesAssembler<TestEntity> assembler){
            return new QueryById(repository,assembler);
        }

        @Bean
        public Search search(TestEntityRepository repository, PagedResourcesAssembler<TestEntity> assembler){
            return new Search(repository,assembler);
        }


        @Bean
        public SecurityCustomizer lookupServiceSecurity(){
            return r -> r.pathMatchers(HttpMethod.GET,"/entities/**").hasAuthority("entity:Read");
        }

    }

    @RequestMapping("/entities")
    @RestController
    public static class QueryById extends QueryByIdResource<String,TestEntity> {

        public QueryById(ProjectionRepository<String, TestEntity> repository, PagedResourcesAssembler<TestEntity> assembler) {
            super(repository,assembler);
        }
    }

    @RequestMapping("/entities")
    public static class Search extends SearchResource<TestEntity> {

        public Search(JpaSpecificationExecutor<TestEntity> repository, PagedResourcesAssembler<TestEntity> assembler) {
            super(repository,assembler, s -> SubjectSpecification.any());
        }
    }

    public static class TestRepositoryAppendingStorageService extends RepositoryAppendingStorageService<String,TestEntity> {

        public TestRepositoryAppendingStorageService(Metrics metrics, ProjectionRepository<String,TestEntity> repository) {
            super(metrics, repository);
        }

        @Override
        protected void createHandlers(PatternMatcher.Builder<Event,TestEntity> builder) {
            builder.match(TestErrorEvent.class, e -> {throw new RuntimeException();})
                    .match(TestEvent.class, e -> new TestEntity(e.getKey(), Instant.now(),e.getMessageId().toString()));
        }
    }

}