package gov.scot.payments.application.component.projector.springdata;

import gov.scot.payments.application.func.Matchers;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.Event;
import gov.scot.payments.testing.event.*;
import io.vavr.Tuple2;

import java.time.Instant;

public class TestRepositoryAppendingStorageService extends RepositoryAppendingStorageService<String,TestEntity> {

    public TestRepositoryAppendingStorageService(TestEntityRepository repository, Metrics micrometerMetrics) {
        super(micrometerMetrics,repository);
    }

    @Override
    protected void createHandlers(PatternMatcher.Builder<Event,TestEntity> builder) {
        builder.match(TestErrorEvent.class, e -> {throw new RuntimeException();})
                .match(TestEvent.class, e -> new TestEntity(e.getKey(), Instant.now(),e.getMessageId().toString()));
    }

}