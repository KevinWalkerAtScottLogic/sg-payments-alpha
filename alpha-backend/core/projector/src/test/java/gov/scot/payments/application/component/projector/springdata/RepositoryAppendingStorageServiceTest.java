package gov.scot.payments.application.component.projector.springdata;

import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.testing.event.TestCreateEvent;
import gov.scot.payments.testing.event.TestDeleteEvent;
import gov.scot.payments.testing.event.TestUpdateEvent;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.Tuple2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest(properties = {"spring.datasource.url=jdbc:h2:mem:RepositoryAppendingStorageServiceTest"})
@AutoConfigurationPackage
@ContextConfiguration(classes = RepositoryAppendingStorageServiceTest.TestConfiguration.class)
class RepositoryAppendingStorageServiceTest {

    @Autowired
    private RepositoryAppendingStorageService<String, TestEntity> service;

    @Autowired
    private JpaRepository<TestEntity,String> repository;

    @Test
    public void testCreateNotExists(){
        TestCreateEvent event = new TestCreateEvent("1");
        Tuple2<String,TestEntity> entity = service.apply("a",event);
        assertEquals("1",entity._2.getId());
        assertEquals(event.getMessageId().toString(),entity._2.getData());
        assertEquals(1,repository.count());
        assertEquals(entity._2,repository.getOne("1"));
    }

    @Test
    public void testUpdateAlreadyExists(){
        repository.save(new TestEntity("3","a"));
        TestUpdateEvent event = new TestUpdateEvent("3");
        Tuple2<String,TestEntity> entity = service.apply("a",event);
        assertEquals("3",entity._2.getId());
        assertEquals(event.getMessageId().toString(),entity._2.getData());
        assertEquals(1,repository.count());
        assertEquals(entity._2,repository.getOne("3"));
    }

    @Test
    public void testUpdateNotExists(){
        TestUpdateEvent event = new TestUpdateEvent("4");
        Tuple2<String,TestEntity> entity = service.apply("a",event);
        assertEquals("4",entity._2.getId());
        assertEquals(event.getMessageId().toString(),entity._2.getData());
        assertEquals(1,repository.count());
        assertEquals(entity._2,repository.getOne("4"));
    }

    @Configuration
    public static class TestConfiguration {

        @Bean
        public RepositoryAppendingStorageService<String,TestEntity> service(TestEntityRepository repository){
            return new TestRepositoryAppendingStorageService(repository,new MicrometerMetrics(new SimpleMeterRegistry()));
        }

    }

}