package gov.scot.payments.application.component.projector;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import gov.scot.payments.application.MessageDeDuplicator;
import gov.scot.payments.application.component.projector.springdata.RepositoryMutatingStorageService;
import gov.scot.payments.application.component.projector.springdata.TestEntity;
import gov.scot.payments.application.component.projector.springdata.TestEntityRepository;
import gov.scot.payments.application.component.projector.springdata.TestRepositoryMutatingStorageService;
import gov.scot.payments.application.kafka.MessageDuplicateFilterFactory;
import gov.scot.payments.application.kafka.MessageHeaderEnricher;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.kafka.StreamJoiner;
import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.Event;
import gov.scot.payments.testing.event.TestCreateEvent;
import gov.scot.payments.testing.event.TestDeleteEvent;
import gov.scot.payments.testing.event.TestErrorEvent;
import gov.scot.payments.testing.event.TestUpdateEvent;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.Function2;
import io.vavr.collection.List;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.state.Stores;
import org.apache.kafka.streams.state.WindowBytesStoreSupplier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;
import org.springframework.kafka.support.JacksonUtils;
import org.springframework.kafka.support.converter.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.test.context.ContextConfiguration;

import java.time.Duration;
import java.time.Instant;
import java.util.function.BiConsumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DataJpaTest(properties = {"spring.datasource.url=jdbc:h2:mem:ProjectorTest"})
@AutoConfigurationPackage
@ContextConfiguration(classes = JoiningProjectorTest.TestConfiguration.class)
class JoiningProjectorTest {

    private JoiningProjector<String,TestEntity> processor;
    private KafkaStreamsTestHarness harness;
    private BiConsumer<Event,Throwable> errorHandler;

    @SpyBean
    private MutatingStorageService<String,TestEntity> storageService;

    @BeforeEach
    public void setUp() throws Exception {
        final Duration windowSize = Duration.ofMinutes(10);

        errorHandler = mock(BiConsumer.class);
        BeanFactory beanFactory = mock(BeanFactory.class);
        StreamsBuilderFactoryBean factory = mock(StreamsBuilderFactoryBean.class);
        when(beanFactory.getBean(anyString(),eq(StreamsBuilderFactoryBean.class))).thenReturn(factory);

        harness = KafkaStreamsTestHarness.builderWithMappings(Serdes.ByteArray(),new gov.scot.payments.testing.kafka.JsonSerde(),TestCreateEvent.class, TestUpdateEvent.class, TestDeleteEvent.class, TestErrorEvent.class, TestEntity.class)
                                         .build();

        final MicrometerMetrics metrics = new MicrometerMetrics(new SimpleMeterRegistry());
        MessageDuplicateFilterFactory duplicateFilterFactory = new MessageDuplicateFilterFactory(windowSize,KafkaStreamsTestHarness.STATE_STORE
                ,metrics
                , Stores.inMemoryWindowStore(KafkaStreamsTestHarness.STATE_STORE, windowSize, windowSize, false));
        MessageDeDuplicator deDuplicator = MessageDeDuplicator.builder()
                                                              .messageEnricher(() -> new MessageHeaderEnricher<>("test"))
                                                              .duplicateFilterFactory(duplicateFilterFactory)
                                                              .build();
        deDuplicator.setBeanFactory(beanFactory);

        PerEventProjectorDelegate<String,TestEntity> delegate = PerEventProjectorDelegate.<String,TestEntity>builder()
                .errorHandler(errorHandler)
                .metrics(metrics)
                .recordTransformer(storageService::apply)
                .build();

        ObjectMapper mapper = JacksonUtils.enhancedObjectMapper();
        mapper.activateDefaultTyping(BasicPolymorphicTypeValidator.builder().allowIfBaseType(Event.class).build());
        JsonSerde<SerializedMessage> stateSerde = new JsonSerde<>(SerializedMessage.class, mapper);
        ((DefaultJackson2JavaTypeMapper)((JsonDeserializer)stateSerde.deserializer()).getTypeMapper()).setClassIdFieldName("contentType");
        ((DefaultJackson2JavaTypeMapper)((JsonSerializer)stateSerde.serializer()).getTypeMapper()).setClassIdFieldName("contentType");

        Function2<String, JoinWindows, WindowBytesStoreSupplier> storeSupplier = (n, w) -> Stores.inMemoryWindowStore(n + "-JoiningProjectorTest-store", Duration.ofMillis(w.size() + w.gracePeriodMs()), Duration.ofMillis(w.size()), true);
        StreamJoiner<Event,Event,Event> joiner = StreamJoiner.<Event,Event,Event>builder()
                .repartitionTopicName("external-repartitioned")
                .joinWindow(Duration.ofMinutes(1))
                .joiner( (e1,e2) -> e2 == null ? e1 : e2)
                .rekeyFunction(e -> e.key)
                .leftValueSerde(harness.getSerde())
                .rightValueSerde(harness.getSerde())
                .stateSerde(stateSerde)
                .storeSupplier(storeSupplier)
                .build();

        processor = (JoiningProjector<String, TestEntity>) JoiningProjector.<String,TestEntity>builder()
                .delegate(delegate)
                .deDuplicator(deDuplicator)
                .joiner(joiner)
                .build();


        when(factory.getObject()).thenReturn(harness.getBuilder());
        KStream<byte[],TestEntity> output = processor.handle(harness.stream(),harness.stream("external"));
        output.to(KafkaStreamsTestHarness.OUTPUT_TOPIC);
    }

    @Test
    public void test(){
        try (final TopologyTestDriver topology = harness.toTopology()) {
            KeyValueWithHeaders<String,TestCreateEvent> create1 = KeyValueWithHeaders.msg("1",new TestCreateEvent("1"));
            KeyValueWithHeaders<String,TestUpdateEvent> update1 = KeyValueWithHeaders.msg("1",new TestUpdateEvent("1"));
            KeyValueWithHeaders<String,TestDeleteEvent> delete1 = KeyValueWithHeaders.msg("1",new TestDeleteEvent("1"));

            KeyValueWithHeaders<String,TestCreateEvent> create1a = KeyValueWithHeaders.msg("1",new TestCreateEvent("1"));
            KeyValueWithHeaders<String,TestUpdateEvent> update2 = KeyValueWithHeaders.msg("2",new TestUpdateEvent("2"));
            KeyValueWithHeaders<String,TestDeleteEvent> delete2 = KeyValueWithHeaders.msg("2",new TestDeleteEvent("2"));

            harness.sendKeyValues(topology,create1,update1,create1a,update2,delete1,create1,delete2);
            List<KeyValueWithHeaders<String,TestEntity>> entities = harness.drainKeyValues(topology);
            assertEquals(3,entities.size());
            assertThat(entities).allMatch(kv -> kv.key.equals("1"));
            assertThat(entities.get(0).value).isEqualToIgnoringGivenFields(new TestEntity("1", Instant.now(),create1.value.getMessageId().toString()),"processingTime");
            assertThat(entities.get(1).value).isEqualToIgnoringGivenFields(new TestEntity("1", Instant.now(),update1.value.getMessageId().toString()),"processingTime");
            assertNull(entities.get(2).value);
        }
        verify(storageService,times(6)).apply(anyString(),any(Event.class));
    }

    @Test
    public void testError(){
        try (final TopologyTestDriver topology = harness.toTopology()) {
            KeyValueWithHeaders<String,TestErrorEvent> create1 = KeyValueWithHeaders.msg("1",new TestErrorEvent("1"));

            assertThrows(RuntimeException.class,() -> harness.sendKeyValues(topology,create1));
        }
        verify(errorHandler,times(1)).accept(any(),any());
    }

    @Configuration
    public static class TestConfiguration {

        @Bean
        public RepositoryMutatingStorageService<String,TestEntity> service(TestEntityRepository repository){
            return new TestRepositoryMutatingStorageService(repository,new MicrometerMetrics(new SimpleMeterRegistry()));
        }

    }
}