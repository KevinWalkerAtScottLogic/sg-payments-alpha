package gov.scot.payments.application.component.projector;

import gov.scot.payments.application.component.projector.springdata.RepositoryMutatingStorageService;
import gov.scot.payments.application.component.projector.springdata.TestEntity;
import gov.scot.payments.application.component.projector.springdata.TestEntityRepository;
import gov.scot.payments.application.component.projector.springdata.TestRepositoryMutatingStorageService;
import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.Event;
import gov.scot.payments.testing.event.TestCreateEvent;
import gov.scot.payments.testing.event.TestDeleteEvent;
import gov.scot.payments.testing.event.TestErrorEvent;
import gov.scot.payments.testing.event.TestUpdateEvent;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.collection.List;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.kstream.KStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;

import java.time.Instant;
import java.util.function.BiConsumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@DataJpaTest(properties = {"spring.datasource.url=jdbc:h2:mem:PerEventConsumingProjectorDelegateTest"})
@AutoConfigurationPackage
@ContextConfiguration(classes = PerEventConsumingProjectorDelegateTest.TestConfiguration.class)
class PerEventConsumingProjectorDelegateTest {

    private PerEventConsumingProjectorDelegate processor;
    private KafkaStreamsTestHarness harness;
    private BiConsumer<Event,Throwable> errorHandler;

    @SpyBean
    private MutatingStorageService<String,TestEntity> storageService;

    @BeforeEach
    public void setUp() throws Exception {
        errorHandler = mock(BiConsumer.class);
        final MicrometerMetrics metrics = new MicrometerMetrics(new SimpleMeterRegistry());

        processor = PerEventConsumingProjectorDelegate.<String,TestEntity>builder()
                .errorHandler(errorHandler)
                .metrics(metrics)
                .recordTransformer(storageService::apply)
                .build();
        harness = KafkaStreamsTestHarness.builderWithMappings(TestCreateEvent.class, TestUpdateEvent.class, TestDeleteEvent.class, TestErrorEvent.class, TestEntity.class)
                .build();

        processor.accept(harness.stream());
    }

    @Test
    public void test(){
        try (final TopologyTestDriver topology = harness.toTopology()) {
            KeyValueWithHeaders<String,TestCreateEvent> create1 = KeyValueWithHeaders.msg("1",new TestCreateEvent("1"));
            KeyValueWithHeaders<String,TestUpdateEvent> update1 = KeyValueWithHeaders.msg("1",new TestUpdateEvent("1"));
            KeyValueWithHeaders<String,TestDeleteEvent> delete1 = KeyValueWithHeaders.msg("1",new TestDeleteEvent("1"));

            KeyValueWithHeaders<String,TestCreateEvent> create1a = KeyValueWithHeaders.msg("1",new TestCreateEvent("1"));
            KeyValueWithHeaders<String,TestUpdateEvent> update2 = KeyValueWithHeaders.msg("2",new TestUpdateEvent("2"));
            KeyValueWithHeaders<String,TestDeleteEvent> delete2 = KeyValueWithHeaders.msg("2",new TestDeleteEvent("2"));

            harness.sendKeyValues(topology,create1,update1,create1a,update2,delete1,delete2);
            List<KeyValueWithHeaders<String,TestEntity>> entities = harness.drainKeyValues(topology);
            assertEquals(0,entities.size());
        }
        verify(storageService,times(6)).apply(anyString(),any(Event.class));
    }

    @Configuration
    public static class TestConfiguration {

        @Bean
        public RepositoryMutatingStorageService<String,TestEntity> service(TestEntityRepository repository){
            return new TestRepositoryMutatingStorageService(repository,new MicrometerMetrics(new SimpleMeterRegistry()));
        }

    }
}