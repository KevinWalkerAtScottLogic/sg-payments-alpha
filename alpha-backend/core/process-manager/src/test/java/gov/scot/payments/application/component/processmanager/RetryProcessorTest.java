package gov.scot.payments.application.component.processmanager;

import gov.scot.payments.model.Message;
import gov.scot.payments.model.MessageImpl;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.TopologyTestDriver;
import org.assertj.core.data.Percentage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.config.KafkaStreamsConfiguration;
import org.springframework.kafka.core.CleanupConfig;
import org.springframework.kafka.support.serializer.JsonSerde;

import java.time.Duration;
import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;

class RetryProcessorTest {

    private RetryProcessor retryProcessor;
    private KafkaStreamsTestHarness harness;

    @BeforeEach
    public void setUp(){
        final Properties properties = KafkaStreamsTestHarness.defaultProperties(TestMessage.class);
        final Serde<Object> serde = KafkaStreamsTestHarness.defaultSerde(TestMessage.class);

        retryProcessor = new RetryProcessor(
                new KafkaStreamsConfiguration(List.ofAll(properties.stringPropertyNames()).toJavaMap(k -> new Tuple2(k,properties.get(k))))
                , new CleanupConfig()
                , KafkaStreamsTestHarness.OUTPUT_TOPIC
                , 2
                ,i -> Duration.ofSeconds(1).multipliedBy(i)
                , null
                ,serde);

        harness = KafkaStreamsTestHarness.builder()
                                         .builder(retryProcessor.createInstance())
                                         .streamsConfiguration(properties)
                                         .serde(serde)
                                         .build();


    }

    @Test
    public void test(){
        long time = System.currentTimeMillis();
        final KeyValueWithHeaders<String,Message> value1 = KeyValueWithHeaders.msg(new TestMessage(),time);
        final KeyValueWithHeaders<String,Message> value2 = KeyValueWithHeaders.msg(new TestMessage(),time);
        //write to delay topics, and shold get written to main topic with correct delay
        try (final TopologyTestDriver topology = harness.toTopology()) {
            String topic1 = retryProcessor.getRetryTopic(1);
            String topic2 = retryProcessor.getRetryTopic(2);
            harness.sendKeyValues(topology,topic1,value1);
            harness.sendKeyValues(topology,topic2,value2);
            final List<KeyValueWithHeaders<String,Message>> actualValues = harness.drainKeyValues(topology);
            assertThat(actualValues.get(0).timestamp).isCloseTo(time+1000, Percentage.withPercentage(5));
            assertThat(actualValues.get(1).timestamp).isCloseTo(time+2000, Percentage.withPercentage(5));
        }
    }

    private static class TestMessage extends MessageImpl {

    }
}