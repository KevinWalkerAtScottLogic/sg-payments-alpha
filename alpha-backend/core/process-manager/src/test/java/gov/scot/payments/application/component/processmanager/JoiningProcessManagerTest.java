package gov.scot.payments.application.component.processmanager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import gov.scot.payments.application.MessageDeDuplicator;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.func.PatternMatchingFunctionDelegate;
import gov.scot.payments.application.kafka.MessageDuplicateFilterFactory;
import gov.scot.payments.application.kafka.MessageHeaderEnricher;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.kafka.StreamJoiner;
import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.GenericErrorEvent;
import gov.scot.payments.testing.command.TestCreateCommand;
import gov.scot.payments.testing.command.TestDeleteCommand;
import gov.scot.payments.testing.command.TestErrorCommand;
import gov.scot.payments.testing.command.TestUpdateCommand;
import gov.scot.payments.testing.event.TestCreateEvent;
import gov.scot.payments.testing.event.TestDeleteEvent;
import gov.scot.payments.testing.event.TestErrorEvent;
import gov.scot.payments.testing.event.TestUpdateEvent;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.Function2;
import io.vavr.collection.List;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.errors.StreamsException;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.state.Stores;
import org.apache.kafka.streams.state.WindowBytesStoreSupplier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;
import org.springframework.kafka.support.JacksonUtils;
import org.springframework.kafka.support.converter.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.nio.file.DirectoryNotEmptyException;
import java.time.Duration;
import java.util.Arrays;
import java.util.function.BiConsumer;

import static gov.scot.payments.testing.kafka.KeyValueWithHeaders.msg;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class JoiningProcessManagerTest {

    private ProcessManagerDelegate delegate;
    private JoiningProcessManager processor;
    private KafkaStreamsTestHarness harness;
    private TestHandler handler;

    @BeforeEach
    void setUp() throws Exception {
        final Duration windowSize = Duration.ofMinutes(10);
        handler = spy(new TestHandler());
        BeanFactory beanFactory = mock(BeanFactory.class);
        StreamsBuilderFactoryBean factory = mock(StreamsBuilderFactoryBean.class);
        when(beanFactory.getBean(anyString(),eq(StreamsBuilderFactoryBean.class))).thenReturn(factory);

        final MicrometerMetrics metrics = new MicrometerMetrics(new SimpleMeterRegistry());
        MessageDuplicateFilterFactory<Event> duplicateFilterFactory = new MessageDuplicateFilterFactory<>(windowSize, KafkaStreamsTestHarness.STATE_STORE
                ,metrics
                , Stores.inMemoryWindowStore(KafkaStreamsTestHarness.STATE_STORE, windowSize, windowSize, false));
        MessageDeDuplicator<Event,Command> deDuplicator = MessageDeDuplicator.<Event,Command>builder()
                                                              .messageEnricher(() -> new MessageHeaderEnricher<>("test"))
                                                              .duplicateFilterFactory(duplicateFilterFactory)
                                                              .build();


        delegate = new PerEventStatelessProcessManagerDelegate(mock(BiConsumer.class),handler,metrics);

        harness = KafkaStreamsTestHarness.builderWithMappings(Serdes.ByteArray(),new gov.scot.payments.testing.kafka.JsonSerde(),TestCreateEvent.class, TestUpdateEvent.class, TestDeleteEvent.class, TestErrorEvent.class, GenericErrorEvent.class
                ,TestCreateCommand.class,TestUpdateCommand.class,TestDeleteCommand.class, TestErrorCommand.class)
                .build();

        ObjectMapper mapper = JacksonUtils.enhancedObjectMapper();
        mapper.activateDefaultTyping(BasicPolymorphicTypeValidator.builder().allowIfBaseType(Event.class).build());
        JsonSerde<SerializedMessage> stateSerde = new JsonSerde<>(SerializedMessage.class, mapper);
        ((DefaultJackson2JavaTypeMapper)((JsonDeserializer)stateSerde.deserializer()).getTypeMapper()).setClassIdFieldName("contentType");
        ((DefaultJackson2JavaTypeMapper)((JsonSerializer)stateSerde.serializer()).getTypeMapper()).setClassIdFieldName("contentType");

        when(factory.getObject()).thenReturn(harness.getBuilder());
        deDuplicator.setBeanFactory(beanFactory);

        RetryHandler retryHandler = RetryHandler.builder()
                                                .valueSerde(harness.getSerde())
                                                .topicNameExtractor((k,v,r) -> KafkaStreamsTestHarness.OUTPUT_TOPIC)
                                                .build();

        Function2<String, JoinWindows, WindowBytesStoreSupplier> storeSupplier = (n, w) -> Stores.inMemoryWindowStore(n + "-JoiningProcessManagerTest-store", Duration.ofMillis(w.size() + w.gracePeriodMs()), Duration.ofMillis(w.size()), true);
        StreamJoiner<Event,Event,Event> joiner = StreamJoiner.<Event,Event,Event>builder()
                                                             .repartitionTopicName("external-repartitioned")
                                                             .joinWindow(Duration.ofMinutes(1))
                                                             .joiner( (e1,e2) -> e2 == null ? e1 : e2)
                                                             .rekeyFunction(e -> e.key)
                                                             .leftValueSerde(harness.getSerde())
                                                             .rightValueSerde(harness.getSerde())
                                                             .stateSerde(stateSerde)
                                                             .storeSupplier(storeSupplier)
                                                             .build();
        processor = JoiningProcessManager.builder()
                                                                 .joiner(joiner)
                                                                 .deDuplicator(deDuplicator)
                                                                 .retryHandler(retryHandler)
                                                                 .externalEventFilter((kvh) -> !Arrays.equals("2".getBytes(), kvh.key))
                                                                 .joinPredicate((k,v) -> true)
                                                                 .delegate(delegate)
                                                                 .build();

        processor.handle(harness.stream("internal"),harness.stream("external"));
    }

    @Test
    public void test(){
        try (final TopologyTestDriver topology = harness.toTopology()) {
            KeyValueWithHeaders<String,TestCreateEvent> create1 = msg("1",new TestCreateEvent("1"),"contentType",TestCreateEvent.class.getName());
            KeyValueWithHeaders<String,TestUpdateEvent> update1 = msg("2",new TestUpdateEvent("2"),"contentType",TestUpdateEvent.class.getName());
            KeyValueWithHeaders<String,TestDeleteEvent> delete1 = msg("1",new TestDeleteEvent("3"),"contentType",TestDeleteEvent.class.getName());
            KeyValueWithHeaders<String,TestCreateEvent> create2 = msg("2",new TestCreateEvent("5"),"contentType",TestCreateEvent.class.getName());

            harness.sendKeyValues(topology,"internal",create1,update1,create1);
            harness.sendKeyValues(topology,"external",delete1,create2);
            List<KeyValueWithHeaders<String,Command>> entities = harness.drainKeyValues(topology);

            assertEquals(3,entities.size());
            assertThat(entities.get(0).value).isEqualToComparingOnlyGivenFields(new TestCreateCommand("1"),"key");
            assertThat(entities.get(1).value).isEqualToComparingOnlyGivenFields(new TestUpdateCommand("2"),"key");
            assertThat(entities.get(2).value).isEqualToComparingOnlyGivenFields(new TestDeleteCommand("2"),"key");

        } catch (StreamsException e) {
            if(! (e.getCause() instanceof DirectoryNotEmptyException)){
                throw e;
            }
        }
        verify(handler,times(3)).apply(any());
    }

    public static class TestHandler extends PatternMatchingFunctionDelegate<Event, List<Command>> {

        @Override
        protected void handlers(final PatternMatcher.Builder<Event, List<Command>> builder) {
            builder.match(TestCreateEvent.class, c -> List.of(new TestCreateCommand(c.getKey())))
                    .match(TestUpdateEvent.class, c -> List.of(new TestUpdateCommand(c.getKey()),new TestDeleteCommand(c.getKey())))
                    .match(TestDeleteEvent.class, c -> List.empty())
                    .match(TestErrorEvent.class, c -> {throw new RuntimeException();});
        }
    }

}