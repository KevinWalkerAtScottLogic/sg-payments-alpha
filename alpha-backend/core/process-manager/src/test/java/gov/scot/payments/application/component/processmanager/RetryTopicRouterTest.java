package gov.scot.payments.application.component.processmanager;

import gov.scot.payments.model.Command;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.retry.ExhaustedRetryException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RetryTopicRouterTest {

    private RetryTopicRouter router;

    @BeforeEach
    void setUp() {
        router = new RetryTopicRouter("test",2);
    }

    @Test
    public void testNoExecutions(){
        Command command = mock(Command.class);
        when(command.getExecutionCount()).thenReturn(0);
        assertEquals("test",router.extract(new byte[0],command,null));
    }

    @Test
    public void testTooManyExecutions(){
        Command command = mock(Command.class);
        when(command.getExecutionCount()).thenReturn(3);
        assertThrows(ExhaustedRetryException.class,() -> router.extract(new byte[0],command,null));
    }

    @Test
    public void testRetry(){
        Command command = mock(Command.class);
        when(command.getExecutionCount()).thenReturn(1);
        assertEquals("test-retry-1",router.extract(new byte[0],command,null));
    }
}