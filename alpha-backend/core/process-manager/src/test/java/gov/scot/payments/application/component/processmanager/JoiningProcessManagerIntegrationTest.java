package gov.scot.payments.application.component.processmanager;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.kafka.HeaderPreservingCompositeNonNativeSerde;
import gov.scot.payments.application.kafka.MessageHeaderEnricher;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.kafka.StreamJoiner;
import gov.scot.payments.application.kafka.serializer.ReflectionAvroSerde;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.command.*;
import gov.scot.payments.testing.event.*;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.vavr.collection.List;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.state.Stores;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;

import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ApplicationIntegrationTest(classes = {JoiningProcessManagerIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.PROCESS_MANAGER
        ,additionalTopics = 1
        ,properties = {"component.name=test"
        , "command.packages=gov.scot.payments.model,gov.scot.payments.testing.command,gov.scot.payments.testing.event"
        ,"context.name=test"
        ,"events.destinations=test"
        ,"retry.count=1"})
@Disabled
class JoiningProcessManagerIntegrationTest {

    //test retries getting consumed correctly by the delay processor - how??

    @BeforeEach
    public void setUp(EmbeddedBrokerClient brokerClient
            , @Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter
            , @Value("${commands.destination}") String commandTopic){
        String retryTopic = commandTopic + "-retry-1";
        brokerClient.getBroker().addTopicsIfNotExists(commandTopic,retryTopic);
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING,Duration.ofSeconds(30));
    }

    @Test
    public void test(EmbeddedBrokerClient brokerClient
            , @Value("${commands.destination}") String commandTopic){
        String retryTopic = commandTopic + "-retry-1";
        KeyValueWithHeaders<String,TestCreateEvent> create1 = KeyValueWithHeaders.msg("1",new TestCreateEvent("1"));
        KeyValueWithHeaders<String,TestUpdateEvent> update1 = KeyValueWithHeaders.msg("2",new TestUpdateEvent("2"));
        TestDeleteEvent testDeleteEvent = new TestDeleteEvent("1");
        testDeleteEvent.setCauseDetails(new TestDeleteCommand("1",true,0));
        KeyValueWithHeaders<String,TestDeleteEvent> delete1 = KeyValueWithHeaders.msg("1", testDeleteEvent);
        KeyValueWithHeaders<String,TestCreateEvent> create2 = KeyValueWithHeaders.msg("2",new TestCreateEvent("2"));

        brokerClient.sendKeyValuesToDestination("events-internal",List.of(create1,update1,create1));
        brokerClient.sendKeyValuesToDestination("events-external",List.of(delete1,create2));
        List<KeyValueWithHeaders<String,Command>> actual = brokerClient.readAllKeyValuesFromTopic(commandTopic,Command.class,Duration.ofSeconds(2));
        List<KeyValueWithHeaders<String,Command>> retries = brokerClient.readAllKeyValuesFromTopic(retryTopic,Command.class);
        assertEquals(4,actual.size());
        assertThat(actual.get(0).value).isEqualToComparingOnlyGivenFields(new TestCreateCommand("1"),"key");
        assertThat(actual.get(1).value).isEqualToComparingOnlyGivenFields(new TestUpdateCommand("2"),"key");
        assertThat(actual.get(2).value).isEqualToComparingOnlyGivenFields(new TestDeleteCommand("2"),"key");
        assertEquals(1,retries.size());
        assertThat(retries.get(0).value).isEqualToComparingOnlyGivenFields(new TestDeleteCommand("1"),"key");
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends ProcessManagerApplication {

        @Bean
        @Primary
        public EventFilter externalEventFilter(){
            return (kvh) -> !"2".equals(kvh.key);
        }

        @Bean
        public TestHandler handler(){
            return new TestHandler();
        }

        @Bean
        public StreamJoiner<Event,Event,Event> joiner(@Value("${embedded.broker.additional.topic.1}") String externalEventTopic
                //, ValueTransformerWithKeySupplier<String, Event,Event> eventHeaderEnricher
                , SchemaRegistryClient confluentSchemaRegistryClient){
            HeaderPreservingCompositeNonNativeSerde<Event> eventSerde = valueSerde(Event.class);
            return StreamJoiner.<Event, Event, Event>builder()
                    .repartitionTopicName(externalEventTopic)
                    .joinWindow(Duration.ofMinutes(1))
                    .joiner((e1,e2) -> e2 == null ? e1 : e2)
                    .rekeyFunction(kvh -> kvh.key)
                    .leftValueSerde(eventSerde)
                    .rightValueSerde(eventSerde)
                    .stateSerde(stateSerde(confluentSchemaRegistryClient,SerializedMessage.class))
                    .storeSupplier( (n,w) -> Stores.inMemoryWindowStore(n+"-JoiningProcessManagerIntegrationTest-store", Duration.ofMillis(w.size() + w.gracePeriodMs()), Duration.ofMillis(w.size()), true))
                    .build();
        }

    }

    public static class TestHandler extends EventTransformer.PatternMatching {

        @Override
        protected void handlers(final PatternMatcher.Builder<Event, List<Command>> builder) {
            builder.match(TestCreateEvent.class, c -> List.of(new TestCreateCommand(c.getKey(),c.isReply(),c.getExecutionCount())))
                   .match(TestUpdateEvent.class, c -> List.of(new TestUpdateCommand(c.getKey(),c.isReply(),c.getExecutionCount()),new TestDeleteCommand(c.getKey(),c.isReply(),c.getExecutionCount())))
                   .match(TestDeleteEvent.class, c -> List.of(new TestDeleteCommand(c.getKey(),c.isReply(),c.getExecutionCount())))
                    .match(TestErrorEvent.class, c -> {throw new RuntimeException();});
        }
    }

}