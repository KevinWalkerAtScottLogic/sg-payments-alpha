package gov.scot.payments.application.component.processmanager;

import gov.scot.payments.application.MessageDeDuplicator;
import gov.scot.payments.application.kafka.KeyValueWithHeaders;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.HasKey;
import io.vavr.Tuple2;
import lombok.Builder;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.streams.kstream.KStream;

import java.util.function.Predicate;

import static gov.scot.payments.application.kafka.RichKStream.rich;

@Slf4j
@SuperBuilder
public abstract class ProcessManager {

    @NonNull private final MessageDeDuplicator<Event,Command> deDuplicator;
    @NonNull private final ProcessManagerDelegate delegate;
    @NonNull private final RetryHandler retryHandler;
    @Builder.Default protected final Predicate<KeyValueWithHeaders<byte[], Event>> externalEventFilter = v -> true;

    protected final void handle(KStream<byte[], Event> events, String method) throws Exception {
        KStream<byte[],Command> commands = rich(deDuplicator.deduplicate(events,delegate,method))
                .richMap(kvh -> {
                    if(kvh.value instanceof HasKey){
                        Object key = ((HasKey)kvh.value).getKey();
                        kvh.headers.remove(HasKey.PARTITION_KEY_HEADER);
                        if(key != null){
                            byte[] sKey = key.toString().getBytes();
                            kvh.headers.add(HasKey.PARTITION_KEY_HEADER,sKey);
                            return new Tuple2<>(sKey,kvh.value);
                        } else {
                            return new Tuple2<>(null,kvh.value);
                        }
                    } else {
                        return new Tuple2<>(kvh.key,kvh.value);
                    }
                });
        retryHandler.route(commands);
    }
}
