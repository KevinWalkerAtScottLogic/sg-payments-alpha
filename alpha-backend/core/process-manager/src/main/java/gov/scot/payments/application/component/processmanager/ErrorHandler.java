package gov.scot.payments.application.component.processmanager;

import gov.scot.payments.model.Event;

import java.util.function.BiConsumer;

public interface ErrorHandler extends BiConsumer<Event,Throwable> {
}
