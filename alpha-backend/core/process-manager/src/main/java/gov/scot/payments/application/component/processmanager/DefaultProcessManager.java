package gov.scot.payments.application.component.processmanager;

import gov.scot.payments.kafka.EmptyStream;
import gov.scot.payments.model.Event;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;

import static gov.scot.payments.application.kafka.RichKStream.rich;

@Slf4j
@SuperBuilder
public class DefaultProcessManager extends ProcessManager {

    @StreamListener
    public void handle(@Input("events-internal") KStream<byte[], Event> internalEvents
            , @Input("events-external") KStream<byte[], Event> externalEvents) throws Exception {
        final KStream<byte[], Event> mergedEvents;
        if(externalEvents instanceof EmptyStream){
            mergedEvents = internalEvents;
        } else{
            mergedEvents = internalEvents.merge(rich(externalEvents).richFilter(externalEventFilter).unwrap());
        }
        super.handle(mergedEvents,"handle");
    }
}
