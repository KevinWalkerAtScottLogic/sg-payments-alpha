package gov.scot.payments.application.component.processmanager;

import gov.scot.payments.application.kafka.DelayTransformer;
import gov.scot.payments.application.kafka.StreamsBuilderFactoryBeanDecorator;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Produced;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.core.env.Environment;
import org.springframework.kafka.config.KafkaStreamsConfiguration;
import org.springframework.kafka.core.CleanupConfig;

import java.time.Duration;
import java.util.function.Function;

@Slf4j
public class RetryProcessor extends StreamsBuilderFactoryBeanDecorator {

    @Getter(AccessLevel.PACKAGE) private final String mainTopic;
    @Getter(AccessLevel.PACKAGE) private final int numRetryTopics;
    private final Function<Integer,Duration> backoffFunction;
    private final Serde valueSerde;

    //do we need to customize the properties for streamsbuilder / kafka streams to ensure the timeouts are
    //higher than the largest delay time?

    public RetryProcessor(final KafkaStreamsConfiguration streamsConfig
            , final CleanupConfig cleanupConfig
            , final String mainTopic
            , final int numRetryTopics
            , final Function<Integer, Duration> backoffFunction
            , final BeanFactory beanFactory
            , final Serde valueSerde) {
        super(streamsConfig, cleanupConfig,beanFactory);
        this.mainTopic = mainTopic;
        this.numRetryTopics = numRetryTopics;
        this.backoffFunction = backoffFunction;
        this.valueSerde = valueSerde;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected StreamsBuilder createInstance() {
        StreamsBuilder streamsBuilder = super.createInstance();
        for(int i=0;i<numRetryTopics;i++){
            int topicNum = i + 1;
            String topic = getRetryTopic(topicNum);
            final Duration delay = backoffFunction.apply(topicNum);
            log.info("Building retry processor {} for topic {}. Delay: {}", topicNum,topic,delay);
            streamsBuilder.stream(topic, Consumed.with(Serdes.String(),valueSerde))
                          .transform(() -> new DelayTransformer<>(delay))
                          .to(mainTopic, Produced.with(Serdes.String(),valueSerde));
        }
        return streamsBuilder;
    }

    String getRetryTopic(int topicNum){
        return String.format("%s-retry-%s",mainTopic, topicNum);
    }
}
