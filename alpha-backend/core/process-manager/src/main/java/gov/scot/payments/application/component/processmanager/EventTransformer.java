package gov.scot.payments.application.component.processmanager;

import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.func.PatternMatchingFunctionDelegate;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import io.vavr.collection.List;

import java.util.function.Consumer;
import java.util.function.Function;

public interface EventTransformer extends Function<Event, List<Command>> {

    static EventTransformer patternMatching(Consumer<PatternMatcher.Builder<Event, List<Command>>> c){
        return new PatternMatching() {
            @Override
            protected void handlers(final PatternMatcher.Builder<Event, List<Command>> builder) {
                c.accept(builder);
            }
        };
    }

    abstract class PatternMatching extends PatternMatchingFunctionDelegate<Event, List<Command>> implements EventTransformer{

    }
}
