package gov.scot.payments.application.component.processmanager;

import gov.scot.payments.application.kafka.StreamJoiner;
import gov.scot.payments.kafka.EmptyStream;
import gov.scot.payments.model.Event;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Predicate;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;

import static gov.scot.payments.application.kafka.RichKStream.rich;

@Slf4j
@SuperBuilder
public class JoiningProcessManager extends ProcessManager {

    @NonNull private final StreamJoiner<Event,Event,Event> joiner;
    @NonNull private final Predicate<byte[],Event> joinPredicate;

    @StreamListener
    public void handle(@Input("events-internal") KStream<byte[], Event> internalEvents
            , @Input("events-external") KStream<byte[], Event> externalEvents) throws Exception {
        final KStream<byte[], Event> mergedEvents;
        if(externalEvents instanceof EmptyStream){
            mergedEvents = internalEvents;
        } else{
            final KStream<byte[], Event> filteredExternalEvents = rich(externalEvents).richFilter(externalEventFilter).unwrap();
            KStream<byte[],Event>[] branches = filteredExternalEvents.branch(joinPredicate ,(k,v) -> true);
            KStream<byte[],Event> joinEvents = branches[0];
            KStream<byte[],Event> events = internalEvents.merge(branches[1]);
            mergedEvents = joiner.join(events, joinEvents);
        }
        super.handle(mergedEvents,"handle");
    }
}
