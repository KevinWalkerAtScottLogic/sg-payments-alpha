package gov.scot.payments.application.component.processmanager;

import java.time.Duration;
import java.util.function.Function;

public interface BackoffFunction extends Function<Integer, Duration> {
}
