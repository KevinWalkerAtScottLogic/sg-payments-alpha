package gov.scot.payments.application.component.processmanager;

import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import io.vavr.collection.List;
import io.vavr.control.Try;
import lombok.Builder;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;

import java.util.function.BiConsumer;
import java.util.function.Function;

@Slf4j
@Builder
public class PerEventStatelessProcessManagerDelegate implements ProcessManagerDelegate {

    @Builder.Default private final BiConsumer<Event,Throwable> errorHandler = (e, t) -> {};
    @NonNull private final Function<Event, List<Command>> eventTransformer;
    @NonNull private final Metrics metrics;

    @Override
    public KStream<byte[], Command> apply(final KStream<byte[], Event> stream) {
        return stream.flatMapValues(this::wrap);
    }

    private Iterable<Command> wrap(final Event event) {
        log.info("Handling event: {}",event);
        return Try.ofSupplier(metrics.time("event.handle",() -> eventTransformer.apply(event)))
                  .onFailure(e -> log.warn("Handling error: ",e))
                  .onFailure(e -> metrics.increment("event.handle.error"))
                  .onFailure(e -> errorHandler.accept(event,e))
                  .get();
    }
}
