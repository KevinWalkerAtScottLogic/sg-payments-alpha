package gov.scot.payments.application.component.processmanager;

import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;

public interface ProcessManagerBinding {

    @Input("events-internal")
    KStream<?,?> internalEvents();

    @Input("events-external")
    KStream<?,?> externalEvents();
}
