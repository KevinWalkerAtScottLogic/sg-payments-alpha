package gov.scot.payments.application.component.processmanager;

import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import org.apache.kafka.streams.kstream.KStream;

import java.util.function.Function;

public interface ProcessManagerDelegate extends Function<KStream<byte[], Event>, KStream<byte[], Command>> {
}
