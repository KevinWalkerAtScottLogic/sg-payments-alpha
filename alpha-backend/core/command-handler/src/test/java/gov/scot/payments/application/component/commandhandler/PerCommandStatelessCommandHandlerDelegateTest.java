package gov.scot.payments.application.component.commandhandler;

import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.func.PatternMatchingFunctionDelegate;
import gov.scot.payments.application.kafka.MessageDuplicateFilterFactory;
import gov.scot.payments.application.kafka.MessageHeaderEnricher;
import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.GenericErrorEvent;
import gov.scot.payments.testing.command.TestCreateCommand;
import gov.scot.payments.testing.command.TestDeleteCommand;
import gov.scot.payments.testing.command.TestErrorCommand;
import gov.scot.payments.testing.command.TestUpdateCommand;
import gov.scot.payments.testing.event.TestCreateEvent;
import gov.scot.payments.testing.event.TestDeleteEvent;
import gov.scot.payments.testing.event.TestUpdateEvent;
import gov.scot.payments.testing.kafka.KafkaStreamsTestHarness;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.collection.List;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.ValueTransformerWithKey;
import org.apache.kafka.streams.state.Stores;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.function.BiFunction;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class PerCommandStatelessCommandHandlerDelegateTest {

    private PerCommandStatelessCommandHandlerDelegate<EventWithCauseImpl> processor;
    private KafkaStreamsTestHarness harness;
    private BiFunction<Command,Throwable,List<EventWithCauseImpl>> errorHandler;
    private TestHandler handler;

    @BeforeEach
    void setUp() throws Exception {
        handler = spy(new TestHandler());
        errorHandler = spy(new TestErrorHandler());

        final MicrometerMetrics metrics = new MicrometerMetrics(new SimpleMeterRegistry());
        processor = PerCommandStatelessCommandHandlerDelegate.<EventWithCauseImpl>builder()
                .errorHandler((BiFunction)errorHandler)
                .commandTransformer(handler)
                .metrics(metrics)
                .build();
        harness = KafkaStreamsTestHarness.builderWithMappings(TestCreateEvent.class, TestUpdateEvent.class, TestDeleteEvent.class, GenericErrorEvent.class
                , TestCreateCommand.class, TestUpdateCommand.class, TestDeleteCommand.class, TestErrorCommand.class)
                .build();

        processor.apply(harness.stream()).to(KafkaStreamsTestHarness.OUTPUT_TOPIC);
    }

    @Test
    public void test(){
        try (final TopologyTestDriver topology = harness.toTopology()) {
            KeyValueWithHeaders<String,TestCreateCommand> create1 = KeyValueWithHeaders.msg("1",new TestCreateCommand("1",false));
            KeyValueWithHeaders<String,TestUpdateCommand> update1 = KeyValueWithHeaders.msg("2",new TestUpdateCommand("2"));
            KeyValueWithHeaders<String,TestDeleteCommand> delete1 = KeyValueWithHeaders.msg("3",new TestDeleteCommand("3"));
            KeyValueWithHeaders<String, TestErrorCommand> error1 = KeyValueWithHeaders.msg("4",new TestErrorCommand("4"));
            harness.sendKeyValues(topology,create1,update1,delete1,error1);
            List<KeyValueWithHeaders<String,Event>> entities = harness.drainKeyValues(topology);
            assertEquals(4,entities.size());
            assertThat(entities.get(0).value).isEqualToIgnoringGivenFields(new TestCreateEvent(UUID.randomUUID(), Instant.now(),"1",create1.value.getMessageId(),1,false),"messageId","timestamp");
            assertThat(entities.get(1).value).isEqualToIgnoringGivenFields(new TestUpdateEvent(UUID.randomUUID(), Instant.now(),"2",update1.value.getMessageId(),1,true),"messageId","timestamp");
            assertThat(entities.get(2).value).isEqualToIgnoringGivenFields(new TestUpdateEvent(UUID.randomUUID(), Instant.now(),"2",update1.value.getMessageId(),1,true),"messageId","timestamp");
            assertThat(entities.get(3).value).isInstanceOf(GenericErrorEvent.class);
        }
        verify(handler,times(4)).apply(any());
        verify(errorHandler,times(1)).apply(any(),any());
    }

    public static class TestHandler extends PatternMatchingFunctionDelegate<Command, List<EventWithCauseImpl>> {

        @Override
        protected void handlers(final PatternMatcher.Builder<Command, List<EventWithCauseImpl>> builder) {
            builder.match(TestCreateCommand.class,c -> List.of(new TestCreateEvent(c.getKey())))
                   .match(TestUpdateCommand.class,c -> List.of(new TestUpdateEvent(c.getKey()),new TestUpdateEvent(c.getKey())))
                   .match(TestDeleteCommand.class,c -> List.empty())
                    .match(TestErrorCommand.class,c -> {throw new RuntimeException();});
        }
    }

    public static class TestErrorHandler implements BiFunction<Command,Throwable,List<EventWithCauseImpl>>{
        @Override
        public List<EventWithCauseImpl> apply(Command command, Throwable throwable) {
            return List.of(GenericErrorEvent.from(command, throwable));
        }
    }
}