package gov.scot.payments.application.component.commandhandler.stateful;

import gov.scot.payments.testing.command.TestUpdateCommand;
import gov.scot.payments.testing.event.TestUpdateEvent;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode(of = "id")
public class TestEntity {

    @NonNull private String id;

    private String data;

    public TestEntity merge(TestUpdateEvent e) {
        return toBuilder().data(e.getMessageId().toString()).build();
    }

    public TestEntity merge(TestUpdateCommand e) {
        return toBuilder().data(e.getMessageId().toString()).build();
    }

}
