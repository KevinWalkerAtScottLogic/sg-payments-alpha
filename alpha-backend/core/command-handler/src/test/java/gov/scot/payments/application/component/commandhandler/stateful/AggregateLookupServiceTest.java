package gov.scot.payments.application.component.commandhandler.stateful;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.metrics.MicrometerMetrics;
import gov.scot.payments.model.user.User;
import gov.scot.payments.testing.HttpsWiremockConfigFactory;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import io.vavr.collection.HashSet;
import org.apache.kafka.streams.state.HostInfo;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.cloud.stream.binder.kafka.streams.InteractiveQueryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;
import reactor.test.StepVerifier;
import ru.lanwen.wiremock.ext.WiremockResolver;
import ru.lanwen.wiremock.ext.WiremockUriResolver;

import javax.net.ssl.SSLException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith({
        WiremockResolver.class,
        WiremockUriResolver.class
})
class AggregateLookupServiceTest {

    private AggregateLookupService<TestEntity, TestEntityState> service;
    private InteractiveQueryService queryService;

    private ReadOnlyKeyValueStore<byte[], AggregateState<TestEntity>> stateStore;
    private WebClient client;
    private ObjectMapper mapper;

    @BeforeEach
    void setUp(@WiremockResolver.Wiremock(factory = HttpsWiremockConfigFactory.class) WireMockServer server) throws SSLException {
        configureFor("localhost", server.port());
        mapper = new ObjectMapper();
        stateStore = mock(ReadOnlyKeyValueStore.class);
        client = createWebClient();
        queryService = mock(InteractiveQueryService.class);
        when(queryService.getCurrentHostInfo()).thenReturn(new HostInfo("localhost",0));
        when(queryService.getHostInfo(anyString(),eq("remote".getBytes()),any())).thenReturn(new HostInfo("localhost",server.httpsPort()));
        when(queryService.getHostInfo(anyString(),eq("local".getBytes()),any())).thenReturn(new HostInfo("localhost",0));
        when(queryService.getQueryableStore(anyString(),any())).thenReturn(stateStore);
        service = AggregateLookupService.<TestEntity, TestEntityState>builder()
                .webClient(client)
                .queryService(queryService)
                .metrics(new MicrometerMetrics(new SimpleMeterRegistry()))
                .entityClass(TestEntityState.class)
                .aclCheck( (s,sub) -> sub == null || !sub.getName().equals("pass"))
                .build();
        service.init("test-store");
    }

    @Test
    public void testGetLocalPresent(){
        TestEntity entity = new TestEntity("a","b");
        TestEntityState state = new TestEntityState(mock(SerializedMessage.class), null, null, entity, 1L);
        when(stateStore.get("a".getBytes())).thenReturn(state);
        StepVerifier.create(service.getAggregateByIdLocal("a",null))
                    .expectNext(state)
                    .verifyComplete();
        verifyNoInteractions(client);
        verify(stateStore,times(1)).get("a".getBytes());
        verifyNoMoreInteractions(stateStore);
    }

    @Test
    public void testGetLocalNotPresent(){
        StepVerifier.create(service.getAggregateByIdLocal("a",null))
                    .verifyComplete();
        verifyNoInteractions(client);
        verify(stateStore,times(1)).get("a".getBytes());
        verifyNoMoreInteractions(stateStore);
    }

    @Test
    public void testGetPresentOnLocal(){
        TestEntity entity = new TestEntity("local","b");
        when(stateStore.get("local".getBytes())).thenReturn(new TestEntityState(mock(SerializedMessage.class),null,entity,null,1L));
        StepVerifier.create(service.getAggregateById("local",null))
                    .expectNextMatches(sr -> sr.getStatusCode() == HttpStatus.OK)
                    .verifyComplete();
        verifyNoInteractions(client);
        verify(stateStore,times(1)).get("local".getBytes());
        verifyNoMoreInteractions(stateStore);
    }

    @Test
    public void testGetPresentOnLocalAclFail(){
        User user = User.builder()
                .email("test@test.com")
                .name("pass")
                .roles(HashSet.of())
                .groups(HashSet.of())
                .build();

        TestEntity entity = new TestEntity("local","b");
        when(stateStore.get("local".getBytes())).thenReturn(new TestEntityState(mock(SerializedMessage.class),null,entity,null,1L));
        StepVerifier.create(service.getAggregateById("local", user))
                .verifyError(AccessDeniedException.class);
        verifyNoInteractions(client);
        verify(stateStore,times(1)).get("local".getBytes());
        verifyNoMoreInteractions(stateStore);
    }

    @Test
    public void testGetRemoteNotFound(){
        stubFor(get(urlEqualTo("/state/local/remote")).willReturn(notFound()));

        StepVerifier.create(service.getAggregateById("remote",null))
                    .expectNextMatches(sr -> sr.getStatusCode() == HttpStatus.NOT_FOUND)
                    .verifyComplete();
        verifyNoInteractions(stateStore);

    }

    @Test
    public void testGetRemotePresent() throws JsonProcessingException {
        TestEntity entity = new TestEntity("remote","b");
        stubFor(get(urlEqualTo("/state/local/remote")).willReturn(okJson(mapper.writeValueAsString(entity))));

        StepVerifier.create(service.getAggregateById("remote",null))
                    .expectNextMatches(sr -> sr.getStatusCode() == HttpStatus.OK)
                    .verifyComplete();
        verifyNoInteractions(stateStore);

    }

    @Test
    public void testGetRemotePresentAclFail() throws JsonProcessingException {
        User user = User.builder()
                .email("test@test.com")
                .name("pass")
                .roles(HashSet.of())
                .groups(HashSet.of())
                .build();

        TestEntity entity = new TestEntity("remote","b");
        stubFor(get(urlEqualTo("/state/local/remote")).willReturn(okJson(mapper.writeValueAsString(entity))));

        StepVerifier.create(service.getAggregateById("remote",user))
                .verifyError(AccessDeniedException.class);
        verifyNoInteractions(stateStore);

    }

    private WebClient createWebClient() throws SSLException {
        SslContext sslContext = SslContextBuilder
                .forClient()
                .trustManager(InsecureTrustManagerFactory.INSTANCE)
                .build();
        HttpClient httpClient = HttpClient.create().secure( t -> t.sslContext(sslContext) );
        return spy(WebClient.builder().clientConnector(new ReactorClientHttpConnector(httpClient)).build());
    }
}