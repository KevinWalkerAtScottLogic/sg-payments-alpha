package gov.scot.payments.application.component.commandhandler;

import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.func.PatternMatchingFunctionDelegate;
import gov.scot.payments.model.AccessDeniedEvent;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.testing.command.*;
import gov.scot.payments.testing.event.TestCreateEvent;
import gov.scot.payments.testing.event.TestEvent;
import gov.scot.payments.testing.event.TestUpdateEvent;
import io.vavr.collection.List;
import org.junit.jupiter.api.Test;

import java.util.function.Function;

import static gov.scot.payments.application.component.commandhandler.AclFilteringStatelessCommandHandlerFunction.acl;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class AclFilteringStatelessCommandHandlerFunctionTest {

    @Test
    public void testAcl(){
        Function<Command, List<EventWithCauseImpl>> handler = PatternMatchingFunctionDelegate.delegate(b ->
                b.match(TestCreateCommand.class,  acl(c -> false,c -> List.of(new TestCreateEvent(c.getKey()))))
                        .match(TestUpdateCommand.class, acl(c -> true,c -> List.of(new TestUpdateEvent(c.getKey()),new TestUpdateEvent(c.getKey()))))
                        .match(TestDeleteCommand.class, acl(c -> true,c -> List.empty()))
                        .match(TestErrorCommand.class, acl(c -> false,c -> {throw new RuntimeException();})));

        assertThat(handler.apply(new TestCreateCommand("1"))).hasSize(1).allMatch(e -> AccessDeniedEvent.class.isAssignableFrom(e.getClass()));
        assertThat(handler.apply(new TestUpdateCommand("1"))).hasSize(2).allMatch(e -> TestUpdateEvent.class.isAssignableFrom(e.getClass()));
        assertThat(handler.apply(new TestDeleteCommand("1"))).isEmpty();
        assertThat(handler.apply(new TestErrorCommand("1"))).hasSize(1).allMatch(e -> AccessDeniedEvent.class.isAssignableFrom(e.getClass()));
    }

}