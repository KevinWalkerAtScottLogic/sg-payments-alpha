package gov.scot.payments.application.component.commandhandler;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.application.ReflectiveMessageTypeRegistry;
import gov.scot.payments.application.security.ReactiveWebSecurityConfigurer;
import gov.scot.payments.model.*;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.testing.command.TestCommand;
import gov.scot.payments.testing.event.TestCreateEvent;
import gov.scot.payments.testing.event.TestEvent;
import gov.scot.payments.testing.security.MockSubjectAuthentication;
import io.vavr.Tuple2;
import io.vavr.Tuple4;
import io.vavr.collection.Map;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.stream.reactive.FluxSender;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.messaging.Message;
import org.springframework.mock.web.reactive.function.server.MockServerRequest;
import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.server.ServerRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.lang.reflect.Method;
import java.time.Duration;
import java.time.Instant;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import static gov.scot.payments.testing.security.MockSubjectAuthentication.user;
import static org.hamcrest.Matchers.emptyString;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockAuthentication;

@WebFluxTest(controllers = MessageTypeRegistryCommandHandlerResourceTest.TestConfiguration.class
        , properties = {"context.name=test","component.name=test"})
class MessageTypeRegistryCommandHandlerResourceTest {

    @Test
    public void testCommandRegistration(@Autowired MessageTypeRegistryCommandHandlerResource resource){
        Map<String, MessageTypeRegistry.MessageConstructorSpec<? extends Command,?>> commands = resource.getCommands();
        assertEquals(3,commands.size());
        MessageTypeRegistry.MessageConstructorSpec<? extends Command,?> info = commands.get("command1").get();
        assertEquals(String.class,info.getRequestArgumentType());
        assertEquals(TestCommand1.class,info.getMessageType());

    }

    @Test
    public void testCommandConstruction(@Autowired MessageTypeRegistryCommandHandlerResource resource){
        Method method = resource.getCommands().get("command1").get().getConstructor();

        TestCommand1 command = resource.constructCommand(TestCommand1.class, method,"body",5L,true,null);
        assertEquals(5L,command.getStateVersion());
        assertTrue(command.isReply());
        assertEquals("body",command.getKey());
    }



    @Test
    public void testHandlerCommandNotFound(@Autowired WebTestClient client, @Autowired FluxSender sender){
        when(sender.send(any())).thenReturn(Mono.empty());
        client
                .mutateWith(mockAuthentication(user("test")))
                .put()
                .uri("/command/command2")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @Test
    public void testHandlerNonReplyingSuccess(@Autowired WebTestClient client, @Autowired FluxSender sender){
        when(sender.send(any())).thenReturn(Mono.empty());
        client
                .mutateWith(mockAuthentication(user("test")))
                .put()
                .uri("/command/command1")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isOk();
    }

    @Test
    public void testHandlerNonReplyingError(@Autowired WebTestClient client, @Autowired FluxSender sender){
        when(sender.send(any())).thenReturn(Mono.error(new RuntimeException("msg")));
        client
                .mutateWith(mockAuthentication(user("test")))
                .put()
                .uri("/command/command1")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectBody(String.class)
                .isEqualTo("msg");
    }

    @Test
    public void testHandlerReplyingAccessDenied(@Autowired WebTestClient client, @Autowired MessageTypeRegistryCommandHandlerResource facade){
        doReturn(Mono.just(AccessDeniedEvent.from(new TestCommand1(),"hello"))).when(facade).sendCommandAndWait(any(),any());

        client
                .mutateWith(mockAuthentication(user("test")))
                .put()
                .uri(uri -> uri.path("/command/command1")
                        .queryParam("reply","true")
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isForbidden();
    }

    @Test
    public void testHandlerReplyingLockError(@Autowired WebTestClient client, @Autowired MessageTypeRegistryCommandHandlerResource facade){
        doReturn(Mono.just(OptimisticLockFailureEvent.from(new TestCommand1(),"hello"))).when(facade).sendCommandAndWait(any(),any());

        client
                .mutateWith(mockAuthentication(user("test")))
                .put()
                .uri(uri -> uri.path("/command/command1")
                        .queryParam("reply","true")
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.PRECONDITION_FAILED);
    }

    @Test
    public void testHandlerReplyingSuccessNoState(@Autowired WebTestClient client, @Autowired MessageTypeRegistryCommandHandlerResource facade){
        doReturn(Mono.just(new TestCreateEvent())).when(facade).sendCommandAndWait(any(),any());
        client
                .mutateWith(mockAuthentication(user("test")))
                .put()
                .uri(uri -> uri.path("/command/command1")
                        .queryParam("reply","true")
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(String.class)
                .value(Matchers.not(Matchers.emptyString()));
    }

    @Test
    public void testHandlerReplyingSuccessWithState(@Autowired WebTestClient client, @Autowired MessageTypeRegistryCommandHandlerResource facade){
        doReturn(Mono.just(new TestStateEvent("1",5L))).when(facade).sendCommandAndWait(any(),any());
        client
                .mutateWith(mockAuthentication(user("test")))
                .put()
                .uri(uri -> uri.path("/command/command1")
                        .queryParam("reply","true")
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .valueEquals(HttpHeaders.ETAG,"5")
                .expectBody(String.class)
                .value(Matchers.not(Matchers.emptyString()));
    }

    @Test
    public void testHandlerReplyingTimeoutError(@Autowired WebTestClient client, @Autowired MessageTypeRegistryCommandHandlerResource facade){
        doReturn(Mono.error(new TimeoutException())).when(facade).sendCommandAndWait(any(),any());
        client
                .mutateWith(mockAuthentication(user("test")))
                .put()
                .uri(uri -> uri.path("/command/command1")
                        .queryParam("reply","true")
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.REQUEST_TIMEOUT);
    }

    @Configuration
    @Import(ReactiveWebSecurityConfigurer.class)
    public static class TestConfiguration {

        @MockBean Flux<Message<Event>> responses;
        @MockBean FluxSender sender;
        @MockBean ReactiveJwtDecoder jwtDecoder;

        @Bean
        public Jackson2JsonDecoder jackson2JsonDecoder(){
            return new Jackson2JsonDecoder();
        }

        @Bean
        public MessageTypeRegistry messageTypeRegistry(){
            return new ReflectiveMessageTypeRegistry("gov.scot.payments.application.component.commandhandler");
        }

        @Bean
        public MessageTypeRegistryCommandHandlerResource registryCommandHandlerResource(MessageTypeRegistry messageTypeRegistry, Jackson2JsonDecoder decoder){
            return spy(new MessageTypeRegistryCommandHandlerResource(responses,sender,messageTypeRegistry,"test",decoder));
        }

    }

    @NoArgsConstructor
    @EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
    public static class TestStateEvent extends TestEvent implements HasStateVersion {

        @Getter
        private Long stateVersion;

        public TestStateEvent(String key, Long stateVersion) {
            super(key);
            this.stateVersion = stateVersion;
        }

        @JsonCreator
        public TestStateEvent(@JsonProperty("messageId") final UUID messageId
                , @JsonProperty("timestamp") final Instant timestamp
                , @JsonProperty("reply") final boolean reply
                , @JsonProperty("reply") final UUID correlationId
                , @JsonProperty("correlationId") final int executionCount
                , @JsonProperty("key") final String key
                , @JsonProperty("stateVersion") final Long stateVersion) {
            super(messageId, timestamp,key, correlationId,executionCount,reply);
            this.stateVersion = stateVersion;
        }

    }
}