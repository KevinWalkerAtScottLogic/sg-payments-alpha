package gov.scot.payments.application.component.commandhandler;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.model.HasStateVersion;
import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.testing.command.TestCommand;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.avro.reflect.Nullable;

import java.time.Instant;
import java.util.UUID;

@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@MessageType(context = "test",type = "command4")
public class TestCommand4 extends TestCommand implements HasStateVersion {

    @Getter
    @Nullable
    private Long stateVersion;

    public TestCommand4(String key, boolean reply, Long stateVersion) {
        super(key,reply);
        this.stateVersion = stateVersion;
    }

    @JsonCreator
    public TestCommand4(@JsonProperty("messageId") final UUID messageId
            , @JsonProperty("timestamp") final Instant timestamp
            , @JsonProperty("reply") final boolean reply
            , @JsonProperty("executionCount") final int executionCount
            , @JsonProperty("key") final String key
            , @JsonProperty("stateVersion") final Long stateVersion) {
        super(messageId, timestamp,reply,executionCount,key);
        this.stateVersion = stateVersion;
    }

    @MessageConstructor(role = "entity:Read")
    public static TestCommand4 fromRequest(String key, boolean reply, Long stateVersion, Subject principal){
        return new TestCommand4(key,reply,stateVersion);
    }

}