package gov.scot.payments.application.component.commandhandler;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.model.*;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.command.TestCommand;
import gov.scot.payments.testing.event.TestCreateEvent;
import gov.scot.payments.testing.event.TestEvent;
import gov.scot.payments.testing.kafka.KeyValueWithHeaders;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.vavr.collection.List;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.streams.KafkaStreams;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.messaging.MessageHeaders;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.Duration;

import static gov.scot.payments.testing.security.MockSubjectAuthentication.user;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockAuthentication;

@ApplicationIntegrationTest(classes = {DefaultCommandHandlerIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.COMMAND_HANDLER
        ,properties = {"command.package=gov.scot.payments.application.component.commandhandler"
        ,"component.name=test"
        ,"context.name=test"})
public class DefaultCommandHandlerIntegrationTest {

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, EmbeddedBrokerClient brokerClient){
        brokerClient.getBroker().addTopicsIfNotExists("test-test-command-errors");
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
    }

    @Test
    public void testCommandNoAuth(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        client
                .put()
                .uri("/command/command1")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isUnauthorized();

        assertEquals(0,brokerClient.readAllFromDestination("commands", TestCommand.class).size());
    }

    @Test
    public void testUnrecognizedCommand(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        client
                .mutateWith(mockAuthentication(user("test")))
                .put()
                .uri("/command/command2")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isNotFound();

        assertEquals(0,brokerClient.readAllFromDestination("commands", TestCommand.class).size());
    }

    @Test
    public void testCommandNoPermission(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        client
                .mutateWith(mockAuthentication(user("test")))
                .put()
                .uri("/command/command3")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isForbidden();

        assertEquals(0,brokerClient.readAllFromDestination("commands", TestCommand.class).size());
    }

    @Test
    public void testCommandNoReply(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        client
                .mutateWith(mockAuthentication(user("test", Role.parse("test/entity:Write"))))
                .put()
                .uri("/command/command3")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .isEmpty();

        List<KeyValueWithHeaders<String,TestCommand3>> commands = brokerClient.readAllKeyValuesFromDestination("commands", TestCommand3.class);
        assertEquals(1, commands.size());
        assertEquals("hello",commands.get(0).value.getKey());
        verifyHeaders(commands.get(0).headers);
        List<KeyValueWithHeaders<String,TestCreateEvent>> events = brokerClient.readAllKeyValuesFromDestination("events", TestCreateEvent.class);
        assertEquals(1, events.size());
        assertEquals("hello",events.get(0).value.getKey());
        assertNotNull(events.get(0).headers.lastHeader(Message.CONTEXT_HEADER));
        assertNotNull(events.get(0).headers.lastHeader(MessageHeaders.CONTENT_TYPE));
        assertEquals(0,brokerClient.readAllFromDestination("responses", TestCreateEvent.class).size());
        assertEquals(0,brokerClient.readAllFromTopic("test-test-command-errors", TestCommand.class).size());
    }

    @Test
    public void testCommandWithReplyError(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        client
                .mutateWith(mockAuthentication(user("test", Role.parse("test/entity:Write"))))
                .put()
                .uri(uri -> uri.path("/command/command1")
                    .queryParam("reply",true)
                    .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectBody()
                .jsonPath("$.error.errorMessage").isEqualTo("world");

        List<KeyValueWithHeaders<String,TestCommand1>> commands = brokerClient.readAllKeyValuesFromDestination("commands", TestCommand1.class);
        assertEquals(1, commands.size());
        assertEquals("hello",commands.get(0).value.getKey());
        verifyHeaders(commands.get(0).headers);
        List<KeyValueWithHeaders<String,GenericErrorEvent>> events = brokerClient.readAllKeyValuesFromDestination("events", GenericErrorEvent.class);
        assertEquals(1, events.size());
        assertEquals("world",events.get(0).value.getError().getErrorMessage());
        assertNotNull(events.get(0).headers.lastHeader(Message.CONTEXT_HEADER));
        assertNotNull(events.get(0).headers.lastHeader(MessageHeaders.CONTENT_TYPE));
        List<KeyValueWithHeaders<String,GenericErrorEvent>> responses = brokerClient.readAllKeyValuesFromDestination("responses", GenericErrorEvent.class);
        assertEquals(1, responses.size());
        assertEquals("world",responses.get(0).value.getError().getErrorMessage());
        assertNotNull(responses.get(0).headers.lastHeader(Message.CONTEXT_HEADER));
        assertNotNull(responses.get(0).headers.lastHeader(MessageHeaders.CONTENT_TYPE));
        assertEquals(0,brokerClient.readAllFromTopic("test-test-command-errors", TestCommand.class).size());
    }

    @Test
    public void testCommandWithReplyTimeout(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        client
                .mutateWith(mockAuthentication(user("test", Role.parse("test/entity:Read"))))
                .put()
                .uri(uri -> uri.path("/command/command4")
                        .queryParam("reply",true)
                        .queryParam("timeout","PT1S")
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.REQUEST_TIMEOUT);

        List<KeyValueWithHeaders<String,TestCommand4>> commands = brokerClient.readAllKeyValuesFromDestination("commands", TestCommand4.class);
        assertEquals(1, commands.size());
        assertEquals("hello",commands.get(0).value.getKey());
        verifyHeaders(commands.get(0).headers);
        assertEquals(0,brokerClient.readAllFromDestination("events", TestEvent.class).size());
        assertEquals(0,brokerClient.readAllFromDestination("responses", TestEvent.class).size());
        assertEquals(0,brokerClient.readAllFromTopic("test-test-command-errors", TestCommand.class).size());
    }

    @Test
    public void testCommandWithReplySuccess(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){
        client
                .mutateWith(mockAuthentication(user("test", Role.parse("test/entity:Write"))))
                .put()
                .uri(uri -> uri.path("/command/command3")
                        .queryParam("reply",true)
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("\"hello\"")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.key").isEqualTo("hello");

        List<KeyValueWithHeaders<String,TestCommand3>> commands = brokerClient.readAllKeyValuesFromDestination("commands", TestCommand3.class);
        assertEquals(1, commands.size());
        assertEquals("hello",commands.get(0).value.getKey());
        verifyHeaders(commands.get(0).headers);
        List<KeyValueWithHeaders<String,TestCreateEvent>> events = brokerClient.readAllKeyValuesFromDestination("events", TestCreateEvent.class);
        assertEquals(1, events.size());
        assertEquals("hello",events.get(0).value.getKey());
        assertNotNull(events.get(0).headers.lastHeader(Message.CONTEXT_HEADER));
        assertNotNull(events.get(0).headers.lastHeader(MessageHeaders.CONTENT_TYPE));
        List<KeyValueWithHeaders<String,TestCreateEvent>> responses = brokerClient.readAllKeyValuesFromDestination("responses", TestCreateEvent.class);
        assertEquals(1, responses.size());
        assertEquals("hello",responses.get(0).value.getKey());
        assertNotNull(responses.get(0).headers.lastHeader(Message.CONTEXT_HEADER));
        assertNotNull(responses.get(0).headers.lastHeader(MessageHeaders.CONTENT_TYPE));
        assertEquals(0,brokerClient.readAllFromTopic("test-test-command-errors", TestCommand.class).size());
    }

    private void verifyHeaders(Headers headers) {
        assertNotNull(headers.lastHeader(Message.TYPE_HEADER));
        assertNotNull(headers.lastHeader(Message.CONTEXT_HEADER));
        assertNotNull(headers.lastHeader(MessageHeaders.CONTENT_TYPE));
    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends CommandHandlerApplication<EventWithCauseImpl> {

        @Bean
        public TestHandler handler(){
            return new TestHandler();
        }

    }

    public static class TestHandler extends CommandHandlerFunction.PatternMatching<Event> {

        @Override
        protected void handlers(final PatternMatcher.Builder<Command, List<Event>> builder) {
            builder.match(TestCommand3.class,c -> List.of(new TestCreateEvent(c.getKey())))
                    .match(TestCommand4.class,c -> List.of())
                    .match(TestCommand1.class, c -> {throw new RuntimeException("world");});
        }
    }
}
