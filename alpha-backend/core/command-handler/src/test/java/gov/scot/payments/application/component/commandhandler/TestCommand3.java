package gov.scot.payments.application.component.commandhandler;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.model.HasStateVersion;
import gov.scot.payments.model.HasUser;
import gov.scot.payments.model.MessageConstructor;
import gov.scot.payments.model.MessageType;
import gov.scot.payments.model.user.Role;
import gov.scot.payments.model.user.Subject;
import gov.scot.payments.testing.command.TestCommand;
import io.vavr.collection.Set;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.avro.reflect.Nullable;

import java.time.Instant;
import java.util.UUID;

@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@MessageType(context = "test",type = "command3")
public class TestCommand3 extends TestCommand implements HasStateVersion, HasUser {

    @Getter @Nullable private Long stateVersion;
    @Getter @Nullable private String user;
    @Getter @Nullable private Set<Role> roles;

    public TestCommand3(String key, boolean reply, Long stateVersion) {
        super(key,reply);
        this.stateVersion = stateVersion;
    }

    public TestCommand3(String key, boolean reply, Long stateVersion,String user, Set<Role> roles) {
        super(key,reply);
        this.stateVersion = stateVersion;
        this.user = user;
        this.roles = roles;
    }

    @JsonCreator
    public TestCommand3(@JsonProperty("messageId") final UUID messageId
            , @JsonProperty("timestamp") final Instant timestamp
            , @JsonProperty("reply") final boolean reply
            , @JsonProperty("executionCount") final int executionCount
            , @JsonProperty("key") final String key
            , @JsonProperty("stateVersion") final Long stateVersion
            , @JsonProperty("user") final String user
            , @JsonProperty("roles") final Set<Role> roles) {
        super(messageId, timestamp,reply,executionCount,key);
        this.stateVersion = stateVersion;
        this.user = user;
        this.roles = roles;
    }

    @MessageConstructor(role = "entity:Write")
    public static TestCommand3 fromRequest(String key, boolean reply, Long stateVersion, Subject principal){
        return new TestCommand3(key
                ,reply
                ,stateVersion
                ,principal == null ? null : principal.getName()
                ,principal == null ? null : principal.getRoles());
    }

}