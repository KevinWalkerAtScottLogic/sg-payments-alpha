package gov.scot.payments.application.component.commandhandler.stateful;

import gov.scot.payments.model.Command;
import gov.scot.payments.model.HasStateVersion;
import io.vavr.Function2;
import io.vavr.control.Option;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.AccessDeniedException;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

@RequiredArgsConstructor
public class AclFilteringStatefulCommandHandlerFunction<K extends Command,State,AS extends AggregateState<State>> implements Function2<K, AS,State> {

    @NonNull private final BiFunction<K,State,State> delegate;
    @NonNull private final BiConsumer<K,AS> aclCheck;

    @Override
    public State apply(final K k, AS state) {
        aclCheck.accept(k,state);
        checkLock(k,Option.of(state).map(AggregateState::getCurrentVersion).getOrNull());
        return delegate.apply(k,Option.of(state).map(AggregateState::getCurrent).getOrNull());
    }

    private void checkLock(final K command, final Long currentVersion) {
        if(currentVersion != null && HasStateVersion.class.isAssignableFrom(command.getClass())){
            Long provided = ((HasStateVersion)command).getStateVersion();
            if(provided != null && provided != currentVersion.longValue()){
                throw new OptimisticLockException(currentVersion,provided);
            }
        }
    }

    public static <K extends Command,State,AS extends AggregateState<State>> Function2<K, AS,State> aclAndLock(BiPredicate<K,State> aclCheck, BiFunction<K,State,State> delegate){
        return new AclFilteringStatefulCommandHandlerFunction<>(delegate, (c,s) -> {
            if(!aclCheck.test(c,Option.of(s).map(AggregateState::getCurrent).getOrNull())){
                throw new AccessDeniedException("Access Denied");
            }
        } );
    }

    public static <K extends Command,State,AS extends AggregateState<State>> Function2<K, AS,State> lock(BiFunction<K,State,State> delegate){
        return new AclFilteringStatefulCommandHandlerFunction<>(delegate, (c,s) -> {});
    }

}
