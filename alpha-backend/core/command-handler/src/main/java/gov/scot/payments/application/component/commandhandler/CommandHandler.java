package gov.scot.payments.application.component.commandhandler;

import gov.scot.payments.application.MessageDeDuplicator;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.HasCause;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;

import static gov.scot.payments.application.kafka.RichKStream.rich;

@Slf4j
@SuperBuilder
public abstract class CommandHandler<T extends Event & HasCause> {

    @NonNull protected final MessageDeDuplicator<Command,T> deDuplicator;

    protected final KStream<?, ?>[] handleResponses(KStream<byte[],T> events){
        KStream<byte[],T> responses = rich(events.filter((k,v) -> v.isReply()))
                .richFilter(kvh -> kvh.headers.lastHeader(HasCause.CORRELATION_ID_HEADER) != null)
                .unwrap();
        return new KStream[] {events,responses};
    }
}
