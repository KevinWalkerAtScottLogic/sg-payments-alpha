package gov.scot.payments.application.component.commandhandler.stateful;

import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.func.PatternMatchingBiFunctionDelegate;
import gov.scot.payments.model.Command;
import io.vavr.Function2;
import io.vavr.collection.List;

import java.util.function.Consumer;

public interface EventGeneratorFunction<AS,T> extends Function2<Command,AS, List<T>> {

    static <AS,T> EventGeneratorFunction<AS,T> patternMatching(Consumer<PatternMatcher.Builder2<Command, AS,List<T>>> c){
        return new EventGeneratorFunction.PatternMatching<>() {
            @Override
            protected void handlers(final PatternMatcher.Builder2<Command, AS,List<T>> builder) {
                c.accept(builder);
            }
        };
    }

    abstract class PatternMatching<AS,T> extends PatternMatchingBiFunctionDelegate<Command,AS, List<T>> implements EventGeneratorFunction<AS,T>{

    }
}
