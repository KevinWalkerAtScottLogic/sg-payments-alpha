package gov.scot.payments.application.component.commandhandler;

import gov.scot.payments.model.Command;
import io.vavr.collection.List;

import java.util.function.BiFunction;

public interface ErrorHandler<T> extends BiFunction<Command,Throwable, List<T>> {
}
