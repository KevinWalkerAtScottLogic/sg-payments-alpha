package gov.scot.payments.application.component.commandhandler;

import lombok.Value;

import java.io.Serializable;

@Value
public class VoidObject implements Serializable {

    @Override
    public String toString() {
        return "void";
    }

}
