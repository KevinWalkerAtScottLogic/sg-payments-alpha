package gov.scot.payments.application.component.commandhandler;

import gov.scot.payments.application.MessageDeDuplicator;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.GenericErrorEvent;
import gov.scot.payments.model.HasCause;
import io.vavr.collection.List;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;

@EnableBinding(CommandHandlerBinding.class)
public abstract class CommandHandlerApplication<T extends Event & HasCause> extends CommandHandlerBaseApplication<T>{

    //commandHandler

    @Bean
    @ConditionalOnMissingBean
    public CommandHandler<T> commandHandler(MessageDeDuplicator<Command,T> deDuplicator
            , CommandHandlerFunction<T> commandHandler
            , ErrorHandler<T> errorHandler
            , Metrics metrics){
        CommandHandlerDelegate<T> delegate = PerCommandStatelessCommandHandlerDelegate.<T>builder()
                .metrics(metrics)
                .errorHandler(errorHandler)
                .commandTransformer(commandHandler)
                .build();
        return DefaultCommandHandler.<T>builder()
                .delegate(delegate)
                .deDuplicator(deDuplicator)
                .build();
    }

    @Bean
    @ConditionalOnMissingBean
    public ErrorHandler<?> defaultErrorHandler(){
        return (c,t) -> List.of(GenericErrorEvent.from(c,t));
    }

}
