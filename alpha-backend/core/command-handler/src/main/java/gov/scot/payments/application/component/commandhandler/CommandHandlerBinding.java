package gov.scot.payments.application.component.commandhandler;

import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface CommandHandlerBinding {

    //general processing
    @Input("commands")
    KStream<?,?> commands();

    @Output("events")
    KStream<?,?> events();

    @Output("responses")
    KStream<?,?> sendResponses();


    //request response gateway
    @Output("send-commands")
    MessageChannel requests();

    @Output("send-events")
    MessageChannel sendEvents();

    @Input("receive-events")
    SubscribableChannel responses();


    //error handling
    @Input("receive-command-errors")
    SubscribableChannel commandErrors();

    @Output("send-error-events")
    MessageChannel errorEvents();

    @Output("send-error-responses")
    MessageChannel errorResponses();

}
