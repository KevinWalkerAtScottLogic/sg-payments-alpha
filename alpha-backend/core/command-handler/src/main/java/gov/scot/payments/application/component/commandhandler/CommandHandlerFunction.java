package gov.scot.payments.application.component.commandhandler;

import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.func.PatternMatchingFunctionDelegate;
import gov.scot.payments.model.Command;
import io.vavr.collection.List;

import java.util.function.Consumer;
import java.util.function.Function;

public interface CommandHandlerFunction<T> extends Function<Command, List<T>> {

    static <T> CommandHandlerFunction<T> patternMatching(Consumer<PatternMatcher.Builder<Command, List<T>>> c){
        return new PatternMatching<>() {
            @Override
            protected void handlers(final PatternMatcher.Builder<Command, List<T>> builder) {
                c.accept(builder);
            }
        };
    }

    abstract class PatternMatching<T> extends PatternMatchingFunctionDelegate<Command, List<T>> implements CommandHandlerFunction<T>{

    }
}
