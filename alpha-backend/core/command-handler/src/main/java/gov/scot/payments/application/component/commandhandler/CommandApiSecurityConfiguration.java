package gov.scot.payments.application.component.commandhandler;

import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.application.security.SecurityCustomizer;
import gov.scot.payments.model.Command;
import io.vavr.Tuple2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authorization.AuthorityReactiveAuthorizationManager;

@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE + 100)
public class CommandApiSecurityConfiguration {

    @Bean
    public SecurityCustomizer commandEndpointRegistration(MessageTypeRegistry registry
            , @Value("${context.name}") String context) {
        return r -> registry.getAllConstructorsForContext(Command.class,context)
                        .map(c -> new Tuple2<>(registry.getInfo(c.getMessageType()),c.getAnnotation()))
                        .filter(t -> t._1.isDefined())
                        .map(t -> t.map1(o -> o.get().getType()))
                            .map(t -> t.map1(s -> String.format("/command/%s",s)))
                        .filter(t -> !t._2.authorized() || ( t._2.authorized() && !t._2.role().isEmpty()) )
                        .map(t -> t._2.authorized() ? r.pathMatchers(HttpMethod.PUT,t._1).access(AuthorityReactiveAuthorizationManager.hasAuthority(t._2.role()))
                        : r.pathMatchers(HttpMethod.PUT,t._1).permitAll() );
    }

}
