package gov.scot.payments.application.component.commandhandler.stateful;

import gov.scot.payments.application.func.PatternMatcher;
import gov.scot.payments.application.func.PatternMatchingBiFunctionDelegate;
import gov.scot.payments.model.Command;
import io.vavr.Function2;

import java.util.function.Consumer;

public interface StateUpdateFunction<AS,State> extends Function2<Command,AS, State> {

    static <AS,State> StateUpdateFunction<AS,State> patternMatching(Consumer<PatternMatcher.Builder2<Command, AS,State>> c){
        return new StateUpdateFunction.PatternMatching<>() {
            @Override
            protected void handlers(final PatternMatcher.Builder2<Command, AS,State> builder) {
                c.accept(builder);
            }
        };
    }

    abstract class PatternMatching<AS,State> extends PatternMatchingBiFunctionDelegate<Command,AS, State> implements StateUpdateFunction<AS,State>{

    }
}
