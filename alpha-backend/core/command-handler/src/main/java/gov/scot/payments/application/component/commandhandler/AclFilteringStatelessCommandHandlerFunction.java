package gov.scot.payments.application.component.commandhandler;

import gov.scot.payments.model.AccessDeniedEvent;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.HasCause;
import io.vavr.Function1;
import io.vavr.collection.List;
import io.vavr.control.Try;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.AccessDeniedException;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

@RequiredArgsConstructor
public class AclFilteringStatelessCommandHandlerFunction<K extends Command,T extends Event & HasCause> implements Function1<K, List<T>> {

    @NonNull private final Function<K,List<T>> delegate;
    @NonNull private final Consumer<K> aclCheck;

    @Override
    public List<T> apply(final K k) {
        return Try.ofSupplier(() -> {
            aclCheck.accept(k);
            return null;
        }).map(n -> delegate.apply(k))
           .getOrElseGet(e -> List.of(buildErrorEvent(k,e)));
    }

    @SuppressWarnings("unchecked")
    private T buildErrorEvent(Command command, final Throwable e) {
        return (T) AccessDeniedEvent.from(command,e.getMessage());
    }

    public static <K extends Command,T extends Event & HasCause> Function1<K, List<T>> acl(Predicate<K> aclCheck, Function<K,List<T>> delegate){
        return new AclFilteringStatelessCommandHandlerFunction<>(delegate, c -> {
            if(!aclCheck.test(c)){
                throw new AccessDeniedException("Access Denied");
            }
        } );
    }
}
