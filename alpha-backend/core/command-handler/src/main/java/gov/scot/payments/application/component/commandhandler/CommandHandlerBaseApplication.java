package gov.scot.payments.application.component.commandhandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.MessageTypeRegistry;
import gov.scot.payments.application.VavrModelResolver;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.HasCause;
import gov.scot.payments.model.HasKey;
import gov.scot.payments.model.user.ResourceRegisteredEvent;
import gov.scot.payments.model.user.ResourceWithVerbs;
import io.swagger.v3.core.converter.AnnotatedType;
import io.swagger.v3.core.converter.ModelConverter;
import io.swagger.v3.core.converter.ModelConverterContext;
import io.swagger.v3.core.converter.ModelConverterContextImpl;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.Paths;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.parameters.Parameter;
import io.swagger.v3.oas.models.parameters.RequestBody;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.responses.ApiResponses;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.api.OpenApiCustomiser;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.reactive.context.ReactiveWebServerInitializedEvent;
import org.springframework.cloud.stream.reactive.FluxSender;
import org.springframework.cloud.stream.reactive.MessageChannelToFluxSenderParameterAdapter;
import org.springframework.cloud.stream.reactive.MessageChannelToInputFluxParameterAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.event.EventListener;
import org.springframework.core.MethodParameter;
import org.springframework.core.codec.Decoder;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.messaging.converter.CompositeMessageConverter;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.util.ResourceUtils;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.stream.Collectors;


@Import(CommandApiSecurityConfiguration.class)
@PropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "application-command-handler.properties")
@Slf4j
public abstract class CommandHandlerBaseApplication<T extends Event & HasCause> extends BaseApplication<Command,T> {

    protected Set<String> registeredVerbs(){
        return HashSet.of();
    }

    @Bean
    public CommandErrorHandler commandErrorHandler(@Qualifier("send-error-events") MessageChannel events
            ,@Qualifier("send-error-responses") MessageChannel responses
            ,Metrics metrics){
        return new CommandErrorHandler(events,responses,metrics);
    }

    @SuppressWarnings("unchecked")
    @Bean
    public MessageTypeRegistryCommandHandlerResource commandHandlerResource(@Qualifier("send-commands") MessageChannel requests
            , @Qualifier("receive-events") SubscribableChannel responses
            , CompositeMessageConverter messageConverter
            , MessageTypeRegistry registry
            , @Value("${context.name}") String context
            , Jackson2JsonDecoder decoder) throws NoSuchMethodException {
        FluxSender commandSender = new MessageChannelToFluxSenderParameterAdapter().adapt(requests,null);

        Flux<Message<Event>> responseStream = (Flux<Message<Event>>) new MessageChannelToInputFluxParameterAdapter(messageConverter)
                .adapt(responses,new MethodParameter(CommandHandlerBaseApplication.class.getMethod("stubMethodForParameterResolution",Flux.class),0));

        return new MessageTypeRegistryCommandHandlerResource(responseStream,commandSender,registry,context,decoder);
    }


    @Bean
    public OpenApiCustomiser commandApiCustomizer(MessageTypeRegistryCommandHandlerResource commandHandlerResource, ObjectMapper mapper){
        ModelConverter converter = new VavrModelResolver(mapper);
        ModelConverterContext converterContext = new ModelConverterContextImpl(java.util.List.of(converter));
        return api -> {
            PathItem commandHandler = api.getPaths().get("/command/{command}");
            Operation operation = commandHandler.getPut();
            List<Parameter> parameters = operation.getParameters().stream().filter(p -> !p.getIn().equals("path")).collect(Collectors.toList());
            ApiResponses responses = operation.getResponses();
            commandHandlerResource.getCommands().forEach( (k,c) -> {
                Schema requestSchema = converterContext.resolve(new AnnotatedType().type(c.getRequestArgumentType()));
                ApiResponses endpointResponses = new ApiResponses();
                responses.entrySet().forEach( e -> endpointResponses.addApiResponse(e.getKey(),e.getValue()));
                endpointResponses.addApiResponse("200",new ApiResponse().content(new Content().addMediaType("application/json",new MediaType())));
                PathItem item = new PathItem().put(new Operation()
                .addTagsItem(k)
                .operationId(k)
                .requestBody(new RequestBody().content(new Content().addMediaType("application/json",new MediaType().schema(requestSchema))))
                .responses(endpointResponses)
                .parameters(parameters));
                api.getPaths().addPathItem(String.format("/command/%s",k),item);
            });
            converterContext.getDefinedModels().entrySet().forEach( e -> api.getComponents().addSchemas(e.getKey(),e.getValue()));
            api.getPaths().remove("/command/{command}");
            api.getComponents().getSchemas().remove("MonoResponseEntityObject");
        };
    }

    public void stubMethodForParameterResolution(Flux<Message<Event>> param){

    }

    @EventListener
    public void onApplicationStart(ReactiveWebServerInitializedEvent event){
        boolean registerResource = event.getApplicationContext().getEnvironment().getProperty("context.register-resource",Boolean.class,true);
        if(registerResource && !registeredVerbs().isEmpty()){
            MessageChannel channel = event.getApplicationContext().getBean("send-events",MessageChannel.class);
            String context = event.getApplicationContext().getEnvironment().getProperty("context.name");
            log.info("Registering resource: {}, with actions: {}",context,registeredVerbs());
            Message<ResourceRegisteredEvent> message = MessageBuilder.withPayload(new ResourceRegisteredEvent(new ResourceWithVerbs(context,registeredVerbs())))
              .setHeader(gov.scot.payments.model.Message.CONTEXT_HEADER, context)
                    .setHeader(gov.scot.payments.model.Message.TYPE_HEADER, "resourceRegistered")
                    .setHeader(KafkaHeaders.MESSAGE_KEY, context.getBytes())
                    .setHeader(HasKey.PARTITION_KEY_HEADER, context)
                    .build();
            channel.send(message);
        }
    }

}
