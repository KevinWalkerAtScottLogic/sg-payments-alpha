package gov.scot.payments.application.component.commandhandler.stateful;

import gov.scot.payments.application.component.commandhandler.CommandHandlerBinding;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;

public interface StatefulCommandHandlerBinding extends CommandHandlerBinding {

    @Input("receive-state-events")
    KStream<?,?> receiveStateEvents();

    @Output("send-state-events")
    KStream<?,?> sendStateEvents();

    @Input("receive-state")
    KTable<?,?> state();
}
