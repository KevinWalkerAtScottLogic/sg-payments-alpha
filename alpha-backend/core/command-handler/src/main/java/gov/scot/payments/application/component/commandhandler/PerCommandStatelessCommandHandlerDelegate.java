package gov.scot.payments.application.component.commandhandler;

import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.HasCause;
import io.vavr.collection.List;
import io.vavr.control.Try;
import lombok.Builder;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;

import java.util.function.BiFunction;
import java.util.function.Function;

@Slf4j
@Builder
public class PerCommandStatelessCommandHandlerDelegate<T extends Event & HasCause> implements CommandHandlerDelegate<T> {

    @NonNull private final BiFunction<Command,Throwable,List<T>> errorHandler;
    @NonNull private final Function<Command, List<T>> commandTransformer;
    @NonNull private final Metrics metrics;

    @Override
    public KStream<byte[], T> apply(final KStream<byte[], Command> stream) {
        return stream.flatMapValues(this::wrap);
    }

    private Iterable<T> wrap(final Command command) {
        log.info("Handling command: {}",command);
        return Try.ofSupplier(metrics.time("command.handle",() -> commandTransformer.apply(command)))
                  .recover(e -> {
                      log.warn("Handling error",e);
                      metrics.increment("command.handle.error");
                      return errorHandler.apply(command,e);
                  })
                  .peek(l -> l.forEach(e -> e.setCauseDetails(command)))
                  .get();
    }

}
