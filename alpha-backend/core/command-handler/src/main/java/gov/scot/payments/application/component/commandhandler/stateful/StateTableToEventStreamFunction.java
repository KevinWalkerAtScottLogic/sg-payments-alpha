package gov.scot.payments.application.component.commandhandler.stateful;

import gov.scot.payments.model.Event;
import gov.scot.payments.model.HasCause;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;

import java.util.function.Function;

public interface StateTableToEventStreamFunction<AS,T extends Event & HasCause> extends Function<KTable<byte[], AS>, KStream<byte[],T>> {
}
