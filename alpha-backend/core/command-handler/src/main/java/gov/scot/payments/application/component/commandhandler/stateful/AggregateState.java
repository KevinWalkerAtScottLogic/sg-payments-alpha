package gov.scot.payments.application.component.commandhandler.stateful;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.model.CommandFailureInfo;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;
import org.springframework.security.access.AccessDeniedException;

import java.util.function.Predicate;

@Getter
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@ToString
@EqualsAndHashCode
@SuperBuilder
public abstract class AggregateState<T> {

    public abstract T getPrevious();
    public abstract T getCurrent();
    private SerializedMessage message;
    @Nullable private CommandFailureInfo error;
    @Nullable private Long currentVersion;

    public AggregateState(){
        message = null;
        error = null;
        currentVersion = null;
    }

    @JsonIgnore
    public boolean isLockFail() {
        return error != null && OptimisticLockException.class.getSimpleName().equals(error.getErrorType());
    }

    @JsonIgnore
    public boolean isAccessDenied() {
        return error != null && AccessDeniedException.class.getSimpleName().equals(error.getErrorType());
    }

    public static <S,T extends AggregateState<S>> Predicate<T> success(){
        return as -> as.getError() == null;
    }

    public static <S,T extends AggregateState<S>> Predicate<T> failure(){
        return as -> as.getError() != null;
    }

    public static <S,T extends AggregateState<S>> Predicate<T> failure(Class<? extends Throwable> errorType){
        return as -> as.getError() != null && errorType.getSimpleName().equals(as.getError().getErrorType());
    }
}
