package gov.scot.payments.application.component.commandhandler.stateful;

import gov.scot.payments.application.component.commandhandler.CommandHandlerDelegate;
import gov.scot.payments.application.kafka.MessageHeaderEnricher;
import gov.scot.payments.application.kafka.MessageWithInfo;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.*;
import io.vavr.Function2;
import io.vavr.Function3;
import io.vavr.Function5;
import io.vavr.collection.List;
import io.vavr.control.Try;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;
import org.springframework.messaging.MessageHeaders;
import org.springframework.security.access.AccessDeniedException;

import java.util.function.Function;

import static gov.scot.payments.application.kafka.MessageKStream.messageStream;
import static gov.scot.payments.application.kafka.RichKStream.rich;
import static io.vavr.Predicates.instanceOf;

@Slf4j
@Builder
public class PerCommandStatefulCommandHandlerDelegate<State,AS extends AggregateState<State>,T extends Event & HasCause> implements CommandHandlerDelegate<T> {

    @NonNull private final AggregateLookupService<State,AS> aggregateQueryService;
    @NonNull private final Metrics metrics;
    @NonNull private final Function5<SerializedMessage, CommandFailureInfo,State,State,Long,AS> aggregateStateSupplier;
    private final Function2<Command,AS, List<T>> eventGenerateFunction;
    @NonNull private final Function3<Command, State,Throwable,List<T>> errorHandler;
    @NonNull private final Function2<Command, AS, State> stateUpdateFunction;
    @NonNull private final Serde<Command> commandSerde;
    @NonNull private final Serde<SerializedMessage> serializedMessageSerde;
    @NonNull private final Materialized<byte[], AS, KeyValueStore<Bytes, byte[]>> stateStore;
    @NonNull private final String stateTopic;
    @NonNull private final ValueTransformerWithKeySupplier<byte[], AS,AS> stateHeaderEnricher;
    private final Function<KTable<byte[], AS>,KStream<byte[],T>> stateToEvents;

    @Getter private String storeName;

    @Override
    public KStream<byte[], T> apply(final KStream<byte[], Command> stream) {
        final Grouped<byte[], SerializedMessage> grouped = Grouped.with(Serdes.ByteArray(), serializedMessageSerde);
        KTable<byte[], AS> stateEvents = messageStream(stream)
                .embedHeaders()
                .unwrap()
                .mapValues(mi -> mi.serialize(commandSerde))
                .groupByKey(grouped)
                .aggregate(() -> aggregateStateSupplier.apply(null,null,null,null,null), (k, v, s) -> updateState(k,v,s), stateStore);
        storeName = stateEvents.queryableStoreName();
        aggregateQueryService.init(storeName);
        if(stateToEvents != null){
            return stateToEvents.apply(stateEvents);
        } else {
            return generateEvents(stateEvents);
        }
    }

    private KStream<byte[],T> generateEvents(KTable<byte[], AS> stateEvents){
        final KStream<byte[], AS> stateEventStream = stateEvents
                .toStream();
        rich(stateEventStream)
                .enrich(stateHeaderEnricher)
                .unwrap()
                .mapValues(s -> s.getCurrent())
                .to(stateTopic, (Produced<byte[], State>) Produced.with(Serdes.ByteArray(),commandSerde));
        return stateEventStream.flatMapValues(this::generateEvents);
    }

    private AS updateState(byte[] key, final SerializedMessage message, final AS state) {
        Command command = message.deseralize(commandSerde);
        log.info("Updating {} state: {} with command: {}",key == null ? null : new String(key),state,command);
        return Try.ofSupplier(metrics.time("state.update",() -> {
            final State updated = stateUpdateFunction.apply(command,state);
            if(ReadOnlyState.class.isAssignableFrom(message.getClass())){
                return aggregateStateSupplier.apply(message,null, state.getPrevious(), state.getCurrent(),state.getCurrentVersion());
            } else{
                log.info("Updated state to: {} from command: {}",updated,command);
                return aggregateStateSupplier.apply(message,null, state.getCurrent(), updated,state.getCurrentVersion() == null ? 1 : state.getCurrentVersion()+1);
            }
        }))
                  .recover(e -> {
                      if(!AccessDeniedException.class.isAssignableFrom(e.getClass())
                              && !OptimisticLockException.class.isAssignableFrom(e.getClass())){
                          log.warn("Handling error updating state",e);
                          metrics.increment("state.update.error");
                      }
                      CommandFailureInfo error = CommandFailureInfo.from(e);
                      return aggregateStateSupplier.apply(message,error, state.getCurrent(), state.getCurrent(),state.getCurrentVersion());
                  })
                  .get();
    }

    @SuppressWarnings("unchecked")
    private Iterable<T> generateEvents(AS state) {
        log.info("Generating events from state {}",state);
        Command command = state.getMessage().deseralize(commandSerde);
        if(state.isAccessDenied()){
            return List.of( (T)AccessDeniedEvent.from(command,state.getError().getErrorMessage()));
        } if(state.isLockFail()){
            return List.of( (T) OptimisticLockFailureEvent.from(command,state.getError().getErrorMessage()));
        } else{
            return Try.ofSupplier(metrics.time("events.generate",() -> eventGenerateFunction.apply(command,state)))
                      .recover(e -> {
                          log.warn("Handling error generating events",e);
                          metrics.increment("events.generate.error");
                          return errorHandler.apply(command,state.getPrevious(),e);
                      })
                      .peek(l -> l.forEach(e -> e.setCauseDetails(command)))
                      .peek(l -> l.filter(instanceOf(HasState.class)).map(e -> (HasState)e).forEach(e -> e.setCarriedState(state.getCurrent(),state.getCurrentVersion())))
                      .get();
        }

    }

}
