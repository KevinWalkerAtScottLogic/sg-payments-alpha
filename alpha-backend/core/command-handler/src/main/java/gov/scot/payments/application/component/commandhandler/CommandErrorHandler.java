package gov.scot.payments.application.component.commandhandler;

import gov.scot.payments.application.metrics.Metrics;
import gov.scot.payments.model.CommandDeserializationErrorEvent;
import gov.scot.payments.model.CommandFailureInfo;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.HasCause;
import gov.scot.payments.model.HasKey;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.binder.kafka.KafkaMessageChannelBinder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@RequiredArgsConstructor
@Slf4j
public class CommandErrorHandler {

    private final MessageChannel events;
    private final MessageChannel responses;
    private final Metrics metrics;

    @StreamListener("receive-command-errors")
    // TODO - add this transactionality back in - currently this causes errors with inability to find 'Transaction Manager'
    //@Transactional
    public void handleCommandErrors(@Headers MessageHeaders headers, @Payload byte[] payload) {
        String exceptionType = Option.of(headers.get(KafkaMessageChannelBinder.X_EXCEPTION_FQCN,String.class)).getOrElse("");
        String exceptionMessage = Option.of(headers.get(KafkaMessageChannelBinder.X_EXCEPTION_MESSAGE,String.class)).getOrElse("");
        String exceptionDetail = Option.of(headers.get(KafkaMessageChannelBinder.X_EXCEPTION_STACKTRACE,String.class)).getOrElse("");
        String correlationId = headers.get(HasCause.CORRELATION_ID_HEADER,String.class);
        String partitionKey = headers.get(HasKey.PARTITION_KEY_HEADER,String.class);
        String executionCount = headers.get(HasCause.EXECUTION_COUNT_HEADER,String.class);

        if(correlationId == null || executionCount == null){
            String message = String.format("correlationId, and executionCount headers must be populated on all commands. Headers: %s",headers);
            log.error("Fatal: Handling deserialization error: {}",message);
            throw new IllegalArgumentException(message);
        }
        Event event = CommandDeserializationErrorEvent.builder()
                                                      .cause(payload)
                                                      .correlationId(UUID.fromString(correlationId))
                                                      .executionCount(Integer.parseInt(executionCount)+1)
                                                      .key(partitionKey)
                                                      .error(CommandFailureInfo.builder()
                                                                               .errorType(exceptionType)
                                                                               .errorMessage(exceptionMessage)
                                                                               .errorDetail(exceptionDetail)
                                                                               .build())
                                                      .build();
        Message<Event> message = new GenericMessage<>(event,headers);
        log.warn("Handling deserialization error: {}",event);
        metrics.increment("deserialization.error");
        events.send(message);
        responses.send(message);
    }
}
