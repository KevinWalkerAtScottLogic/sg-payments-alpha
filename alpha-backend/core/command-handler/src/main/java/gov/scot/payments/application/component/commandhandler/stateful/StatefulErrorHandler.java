package gov.scot.payments.application.component.commandhandler.stateful;

import gov.scot.payments.model.Command;
import io.vavr.Function3;
import io.vavr.collection.List;

public interface StatefulErrorHandler<State,T> extends Function3<Command,State,Throwable, List<T>> {
}
