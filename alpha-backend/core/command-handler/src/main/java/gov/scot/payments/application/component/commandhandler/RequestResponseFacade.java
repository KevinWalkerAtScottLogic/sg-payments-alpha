package gov.scot.payments.application.component.commandhandler;

import gov.scot.payments.model.Command;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.HasCause;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.stream.reactive.FluxSender;
import org.springframework.messaging.Message;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RequiredArgsConstructor
public class RequestResponseFacade {

    private final FluxSender requests;
    private final Flux<Message<Event>> responses;

    public Mono<Event> sendAndAwaitResponse(Command command, Duration timeout){
        if(!command.isReply()){
            return Mono.error(new IllegalArgumentException("Command is not replying"));
        }
        return Mono.defer(() -> requests.send(Flux.just(command)))
                .flatMapMany(c -> responses
                        .filter(m -> command.getMessageId().toString().equals(m.getHeaders().get(HasCause.CORRELATION_ID_HEADER,String.class)))
                        .map(Message::getPayload))
                .next()
                .timeout(timeout)
                .cache();
    }

}
