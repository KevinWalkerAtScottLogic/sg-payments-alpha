package @group@.ch.app;

import gov.scot.payments.application.ApplicationComponent;
import gov.scot.payments.application.BaseApplication;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.application.component.commandhandler.stateful.EventGeneratorFunction;
import gov.scot.payments.application.component.commandhandler.stateful.StateUpdateFunction;
import gov.scot.payments.application.component.commandhandler.stateful.StatefulCommandHandlerApplication;
import gov.scot.payments.model.CommandFailureInfo;
import gov.scot.payments.model.EventWithCauseImpl;
import gov.scot.payments.model.user.Subject;
import org.springframework.context.annotation.Bean;
import @group@.@namePackage@.model.@projectName.capitalize()@;

@ApplicationComponent
public class @projectName.capitalize()@CHApplication extends StatefulCommandHandlerApplication<@projectName.capitalize()@,@projectName.capitalize()@State, EventWithCauseImpl> {

    @Override
    protected Class<@projectName.capitalize()@State> stateClass() {
        return @projectName.capitalize()@State.class;
    }

    @Override
    protected @projectName.capitalize()@State createNewAggregateInstance(SerializedMessage command, CommandFailureInfo error, @projectName.capitalize()@ previous, @projectName.capitalize()@ current, Long currentVersion) {
        return new @projectName.capitalize()@State(command, error, previous, current, currentVersion);
    }

    @Override
    protected boolean aclCheck(@projectName.capitalize()@State state, Subject subject) {

    }

    @Bean
    public StateUpdateFunction<@projectName.capitalize()@State,@projectName.capitalize()@> stateUpdateFunction() {

    }

    @Bean
    public EventGeneratorFunction<@projectName.capitalize()@State, EventWithCauseImpl> eventGenerationFunction() {

    }

    public static void main(String[] args){
        BaseApplication.run(@projectName.capitalize()@CHApplication.class,args);
    }
}