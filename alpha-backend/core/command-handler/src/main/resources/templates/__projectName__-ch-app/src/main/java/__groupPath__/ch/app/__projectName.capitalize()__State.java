package @group@.ch.app;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import gov.scot.payments.application.component.commandhandler.stateful.AggregateState;
import gov.scot.payments.application.kafka.SerializedMessage;
import gov.scot.payments.model.CommandFailureInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.avro.reflect.Nullable;
import @group@.@namePackage@.model.@projectName.capitalize()@;

@SuperBuilder
@NoArgsConstructor
public class @projectName.capitalize()@State extends AggregateState<@projectName.capitalize()@> {

    @Getter @Nullable private @projectName.capitalize()@ previous;
    @Getter @Nullable private @projectName.capitalize()@ current;

    @JsonCreator
    public @projectName.capitalize()@State(@JsonProperty("message") SerializedMessage message
            , @JsonProperty("error") CommandFailureInfo error
            , @JsonProperty("previous") @projectName.capitalize()@ previous
            , @JsonProperty("current") @projectName.capitalize()@ current
            , @JsonProperty("currentVersion") Long currentVersion) {
        super(message, error, currentVersion);
        this.previous = previous;
        this.current = current;
    }

}
