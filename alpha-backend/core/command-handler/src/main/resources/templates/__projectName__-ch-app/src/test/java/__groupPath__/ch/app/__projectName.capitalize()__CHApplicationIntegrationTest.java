package @group@.ch.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import gov.scot.payments.application.ApplicationComponent;
import org.springframework.context.annotation.Import;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.apache.kafka.streams.KafkaStreams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.reactive.server.WebTestClient;
import gov.scot.payments.testing.EmbeddedBrokerClient;
import gov.scot.payments.testing.spring.KafkaStreamsStateWaiter;
import gov.scot.payments.testing.ApplicationIntegrationTest;
import gov.scot.payments.testing.spring.TestConfiguration;
import io.confluent.kafka.schemaregistry.client.MockSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;

import java.time.Duration;

@ApplicationIntegrationTest(classes = {@projectName.capitalize()@CHApplicationIntegrationTest.TestApplication.class}
        ,componentType = ApplicationIntegrationTest.ComponentType.COMMAND_HANDLER)
public class @projectName.capitalize()@CHApplicationIntegrationTest {

    @BeforeEach
    public void setUp(@Autowired KafkaStreamsStateWaiter kafkaStreamsWaiter, EmbeddedBrokerClient brokerClient, @Value("${namespace}") String namespace){
        brokerClient.getBroker().addTopicsIfNotExists(namespace+"-@contextName@-command-errors");
        kafkaStreamsWaiter.waitForState(KafkaStreams.State.RUNNING, Duration.ofSeconds(10));
    }

    @Test
    public void test(@Autowired WebTestClient client, EmbeddedBrokerClient brokerClient){

    }

    @ApplicationComponent
    @Import(TestConfiguration.class)
    public static class TestApplication extends @projectName.capitalize()@Application{

    }
}