package gov.scot.payments.adapters;

import gov.scot.payments.customers.model.api.CustomerPaymentBatch;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.UKBankAccount;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ILFAdapterIntegrationTest {

    @Autowired
    private WebTestClient webTestClient;

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    @Test
    @DisplayName("ILF parser generates correct payment instructions from file payments")
    public void testILFEndpoint() throws IOException {

        CompositeReference product = CompositeReference.parse("ilf.someProductRef");
        String fileName = "someFileName";

        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("ilf_examples/ILF_test.csv");
        var response = webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/ILF")
                        .queryParam("product", product.toString())
                        .queryParam("fileName", fileName)
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(CustomerPaymentBatch.class)
                .returnResult();

        assertEquals(1, response.getResponseBody().size());

        var customerPaymentBatch = response.getResponseBody().get(0);

        assertEquals(fileName, customerPaymentBatch.getId());
        assertEquals(3, customerPaymentBatch.getPayments().size());

        var paymentInstructions = customerPaymentBatch.getPayments();

        var recipient = paymentInstructions.get(0).getCreditorAccountAs(UKBankAccount.class);
        assertEquals("820001",recipient.getSortCode().getValue());
        assertEquals("Test 01",paymentInstructions.get(0).getCreditor().getName());
        assertEquals("12345678",recipient.getAccountNumber().getValue());
        Money money = Money.of(new BigDecimal("1234.56"), "GBP");
        assertEquals(money, paymentInstructions.get(0).getAmount());

        var recipient3 = paymentInstructions.get(2).getCreditorAccountAs(UKBankAccount.class);
        assertEquals("820003" ,recipient3.getSortCode().getValue());
        assertEquals("Test 03", paymentInstructions.get(2).getCreditor().getName());
        assertEquals("12345680", recipient3.getAccountNumber().getValue());
        money = Money.of(new BigDecimal("1048.23"), "GBP");
        assertEquals(money, paymentInstructions.get(2).getAmount());
    }

    @Test
    @DisplayName("ILF parser returns an error when given a file with invalid rows")
    public void testILFEndpointWithInvalidRows() throws IOException {
        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("ilf_examples/ILF_test_with_error_row.csv");
        webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/ILF")
                        .queryParam("product", "ilf.someProductRef")
                        .queryParam("fileName", "someFileName")
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    @DisplayName("ILF parser returns an error when file name parameter is not provided")
    public void testILFEndpointWithoutFileName() throws IOException {
        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("ilf_examples/ILF_test.csv");
        webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/ILF")
                        .queryParam("product", "ilf.someProductRef")
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    @DisplayName("ILF parser returns an error when product reference parameter is not provided")
    public void testILFEndpointWithoutProductRef() throws IOException {
        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("ilf_examples/ILF_test.csv");
        webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/ILF")
                        .queryParam("fileName", "someFileName")
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    @DisplayName("ILF parser returns an error when file is not provided")
    public void testILFEndpointWithoutBody() throws IOException {
        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("ilf_examples/ILF_test.csv");
        webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/ILF")
                        .queryParam("product", "ilf.someProductRef")
                        .queryParam("fileName", "someFileName")
                        .build())
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @Test
    @DisplayName("ILF parser endpoint returns an error when request method is not POST")
    public void testILFWithInvalidRequestMethod() {
        webTestClient
                .get()
                .uri("/files/parse/ILF")
                .exchange()
                .expectStatus()
                .isEqualTo(405);
    }
 
}
