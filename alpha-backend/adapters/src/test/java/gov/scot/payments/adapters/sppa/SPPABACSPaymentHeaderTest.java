package gov.scot.payments.adapters.sppa;

import gov.scot.payments.adapters.FileParseException;
import gov.scot.payments.adapters.InvalidCSVFieldException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SPPABACSPaymentHeaderTest {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    @Test
    @DisplayName("SPPA header is successfuly parsed from a valid string")
    void fromValidStringTest() throws IOException, InvalidCSVFieldException {
        String row = "PQY GRP    : 99709       NHSMXNTHLY 97TH BQCS              PQY DQTZ     : 02/02/2020                 PRXCZSSYNG DQTZ : 02/02/2020";
        SPPABACSPaymentHeader header = SPPABACSPaymentHeader.fromString(row, 6);

        assertEquals("99709 NHSMXNTHLY 97TH BQCS", header.getGroup());
        assertEquals(LocalDate.parse("02/02/2020", DATE_FORMATTER), header.getPayDate());
    }

    @Test
    @DisplayName("SPPA header throws an exception when the group is not provided")
    void fromGroupNotProvidedTest() throws IOException, InvalidCSVFieldException {
        String row = "                                                           PQY DQTZ     : 02/02/2020                 PRXCZSSYNG DQTZ : 02/02/2020";
        InvalidCSVFieldException e = assertThrows(InvalidCSVFieldException.class, () -> SPPABACSPaymentHeader.fromString(row, 6));
        assertEquals("[Row 6 Field Name:'Group'] Parsing error: Field not found", e.getMessage());
    }

    @Test
    @DisplayName("SPPA header throws an exception when the row is too long")
    void fromTooLongStringTest() throws IOException, InvalidCSVFieldException {
        String row = "PQY GRP    : 99709       NHSMXNTHLY 97TH BQCS              PQY DQTZ     : 02/02/2020                 PRXCZSSYNG DQTZ : 02/02/2020 ";
        FileParseException e = assertThrows(FileParseException.class, () -> SPPABACSPaymentHeader.fromString(row, 6));
        assertEquals("Header row 6 has 130 characters. Must be 129", e.getMessage());
    }

    @Test
    @DisplayName("SPPA header throws an exception when the row is too short")
    void fromTooShortStringTest() throws IOException, InvalidCSVFieldException {
        String row = "PQY GRP    : 99709       NHSMXNTHLY 97TH BQCS              PQY DQTZ     : 02/02/2020                PRXCZSSYNG DQTZ : 02/02/2020";
        FileParseException e = assertThrows(FileParseException.class, () -> SPPABACSPaymentHeader.fromString(row, 6));
        assertEquals("Header row 6 has 128 characters. Must be 129", e.getMessage());
    }

    @Test
    @DisplayName("SPPA header throws an exception when the pay date is not valid")
    void fromInvalidPayDateTest() throws IOException, InvalidCSVFieldException {
        String row = "PQY GRP    : 99709       NHSMXNTHLY 97TH BQCS              PQY DQTZ     : 02/99/9999                 PRXCZSSYNG DQTZ : 02/02/2020";
        InvalidCSVFieldException e = assertThrows(InvalidCSVFieldException.class, () -> SPPABACSPaymentHeader.fromString(row, 6));
        assertEquals("[Row 6 Field Name:'Pay Date'] Parsing error: Text '02/99/9999' could not be parsed: Invalid value for MonthOfYear (valid values 1 - 12): 99", e.getMessage());
    }

    @Test
    @DisplayName("SPPA header throws an exception when the pay date is not formatted correctly")
    void fromInvalidFormatPayDateTest() throws IOException, InvalidCSVFieldException {
        String row = "PQY GRP    : 99709       NHSMXNTHLY 97TH BQCS              PQY DQTZ     : 02-02-2029                 PRXCZSSYNG DQTZ : 02/02/2020";
        InvalidCSVFieldException e = assertThrows(InvalidCSVFieldException.class, () -> SPPABACSPaymentHeader.fromString(row, 6));
        assertEquals("[Row 6 Field Name:'Pay Date'] Parsing error: Text '02-02-2029' could not be parsed at index 2", e.getMessage());
    }

    @Test
    @DisplayName("SPPA header throws an exception when the pay date is not valid")
    void fromNullPayDateTest() throws IOException, InvalidCSVFieldException {
        String row = "PQY GRP    : 99709       NHSMXNTHLY 97TH BQCS              PQY DQTZ     :                            PRXCZSSYNG DQTZ : 02/02/2020";
        InvalidCSVFieldException e = assertThrows(InvalidCSVFieldException.class, () -> SPPABACSPaymentHeader.fromString(row, 6));
        assertEquals("[Row 6 Field Name:'Pay Date'] Parsing error: Field not found", e.getMessage());
    }
}
