package gov.scot.payments.adapters;

import gov.scot.payments.customers.model.api.CustomerPaymentBatch;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import org.hamcrest.MatcherAssert;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.ZoneId;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SSAAdapterIntegrationTest {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    @DisplayName("SSA file parser generates correct payment instructions from the example file")
    public void testSSAEndpointWithSinglePaymentFile() throws IOException, InvalidPaymentFieldException {

        String fileName = "someFileName";
        CompositeReference product = CompositeReference.parse("ssa.someProductRef");

        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("ssa_examples/example_payment_file.csv");
        var response = webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/SSA")
                        .queryParam("product", product.toString())
                        .queryParam("fileName", fileName)
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(CustomerPaymentBatch.class)
                .returnResult();

        assertEquals(1, response.getResponseBody().size());

        var customerPaymentBatch = response.getResponseBody().get(0);

        assertEquals(fileName, customerPaymentBatch.getId());;
        assertEquals(1, customerPaymentBatch.getPayments().size());

        var paymentInstruction = customerPaymentBatch.getPayments().get(0);

        MatcherAssert.assertThat(paymentInstruction.getCreditorAccountAs(UKBankAccount.class).getAccountNumber(), is(UKAccountNumber.fromString("11212200")));
        MatcherAssert.assertThat(paymentInstruction.getCreditorAccountAs(UKBankAccount.class).getSortCode(), is(SortCode.fromString("901487")));
        MatcherAssert.assertThat(paymentInstruction.getCreditorMetadata(MetadataField.CLIENT_REF).get().getValue(), is("LM111147A SSS BSG"));
        MatcherAssert.assertThat(paymentInstruction.getCreditor().getName(), is("Personal Bank Acc"));
        MatcherAssert.assertThat(paymentInstruction.getAmount(), is(Money.of(600, "GBP")));
        MatcherAssert.assertThat(LocalDate.ofInstant(paymentInstruction.getEarliestExecutionDate(), ZoneId.of("UTC")).toString(), is("2018-06-27"));
        MatcherAssert.assertThat(LocalDate.ofInstant(paymentInstruction.getLatestExecutionDate(), ZoneId.of("UTC")).toString(), is("2018-06-27"));
        MatcherAssert.assertThat(paymentInstruction.getMetadata("EmsPaymentRefNo").get().getValue(), is("12LM111147A00001"));
        MatcherAssert.assertThat(paymentInstruction.getMetadata("Interface Reference").get().getValue(), is("12SCO1001020180621"));
        MatcherAssert.assertThat(paymentInstruction.getMetadata("Date of file").get().getValue(), is("2018-06-21"));
        assertEquals(product, paymentInstruction.getProduct());
        assertEquals(fileName, paymentInstruction.getBatchId());
    }

    @Test
    @DisplayName("SSA parser generates correct payment instructions from file with three client payments")
    void generatePaymentInstructionsFromThreePaymentsFile() throws IOException, FileParseException, InvalidPaymentFieldException, URISyntaxException {

        String fileName = "someFileName";
        CompositeReference product = CompositeReference.parse("ssa.someProductRef");

        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("ssa_examples/three_payment_file.csv");
        var response = webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/SSA")
                        .queryParam("product", product.toString())
                        .queryParam("fileName", fileName)
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(CustomerPaymentBatch.class)
                .returnResult();

        assertEquals(1, response.getResponseBody().size());

        var customerPaymentBatch = response.getResponseBody().get(0);

        assertEquals(fileName, customerPaymentBatch.getId());
        assertEquals(3, customerPaymentBatch.getPayments().size());

        var paymentInstructions = customerPaymentBatch.getPayments();

        var paymentInstruction = paymentInstructions.get(0);

        MatcherAssert.assertThat(paymentInstruction.getCreditorAccountAs(UKBankAccount.class).getAccountNumber(), is(UKAccountNumber.fromString("11212201")));
        MatcherAssert.assertThat(paymentInstruction.getCreditorAccountAs(UKBankAccount.class).getSortCode(), is(SortCode.fromString("901487")));
        MatcherAssert.assertThat(paymentInstruction.getCreditorMetadata(MetadataField.CLIENT_REF).get().getValue(), is("LM111147A SSS BSG"));
        MatcherAssert.assertThat(paymentInstruction.getCreditor().getName(), is("Personal Bank Acc"));
        MatcherAssert.assertThat(paymentInstruction.getAmount(), is(Money.of(600, "GBP")));
        MatcherAssert.assertThat(LocalDate.ofInstant(paymentInstruction.getEarliestExecutionDate(), ZoneId.of("UTC")).toString(), is("2018-06-27"));
        MatcherAssert.assertThat(LocalDate.ofInstant(paymentInstruction.getLatestExecutionDate(), ZoneId.of("UTC")).toString(), is("2018-06-27"));
        MatcherAssert.assertThat(paymentInstruction.getMetadata("EmsPaymentRefNo").get().getValue(), is("12LM111147A00001"));
        MatcherAssert.assertThat(paymentInstruction.getMetadata("Interface Reference").get().getValue(), is("12SCO1001020180621"));
        MatcherAssert.assertThat(paymentInstruction.getMetadata("Date of file").get().getValue(), is("2018-06-21"));
        assertEquals(product, paymentInstruction.getProduct());
        assertEquals(fileName, paymentInstruction.getBatchId());

        paymentInstruction = paymentInstructions.get(1);
        MatcherAssert.assertThat(paymentInstruction.getCreditorAccountAs(UKBankAccount.class).getAccountNumber(), is(UKAccountNumber.fromString("11212202")));
        MatcherAssert.assertThat(paymentInstruction.getCreditorAccountAs(UKBankAccount.class).getSortCode(), is(SortCode.fromString("901487")));
        MatcherAssert.assertThat(paymentInstruction.getCreditorMetadata(MetadataField.CLIENT_REF).get().getValue(), is("LM111147A SSS BSG"));
        MatcherAssert.assertThat(paymentInstruction.getCreditor().getName(), is("Personal Bank Acc"));
        MatcherAssert.assertThat(paymentInstruction.getAmount(), is(Money.of(300, "GBP")));
        MatcherAssert.assertThat(LocalDate.ofInstant(paymentInstruction.getEarliestExecutionDate(), ZoneId.of("UTC")).toString(), is("2018-06-27"));
        MatcherAssert.assertThat(LocalDate.ofInstant(paymentInstruction.getEarliestExecutionDate(), ZoneId.of("UTC")).toString(), is("2018-06-27"));
        MatcherAssert.assertThat(paymentInstruction.getMetadata("EmsPaymentRefNo").get().getValue(), is("12LM111147A00002"));
        MatcherAssert.assertThat(paymentInstruction.getMetadata("Interface Reference").get().getValue(), is("12SCO1001020180621"));
        MatcherAssert.assertThat(paymentInstruction.getMetadata("Date of file").get().getValue(), is("2018-06-21"));
        assertEquals(product, paymentInstruction.getProduct());
        assertEquals(fileName, paymentInstruction.getBatchId());

        paymentInstruction = paymentInstructions.get(2);
        MatcherAssert.assertThat(paymentInstruction.getCreditorAccountAs(UKBankAccount.class).getAccountNumber(), is(UKAccountNumber.fromString("11212203")));
        MatcherAssert.assertThat(paymentInstruction.getCreditorAccountAs(UKBankAccount.class).getSortCode(), is(SortCode.fromString("901487")));
        MatcherAssert.assertThat(paymentInstruction.getCreditorMetadata(MetadataField.CLIENT_REF).get().getValue(), is("LM111147A SSS BSG"));
        MatcherAssert.assertThat(paymentInstruction.getCreditor().getName(), is("Personal Bank Acc"));
        MatcherAssert.assertThat(paymentInstruction.getAmount(), is(Money.of(600, "GBP")));
        MatcherAssert.assertThat(LocalDate.ofInstant(paymentInstruction.getEarliestExecutionDate(), ZoneId.of("UTC")).toString(), is("2018-06-27"));
        MatcherAssert.assertThat(LocalDate.ofInstant(paymentInstruction.getEarliestExecutionDate(), ZoneId.of("UTC")).toString(), is("2018-06-27"));
        MatcherAssert.assertThat(paymentInstruction.getMetadata("EmsPaymentRefNo").get().getValue(), is("12LM111147A00003"));
        MatcherAssert.assertThat(paymentInstruction.getMetadata("Interface Reference").get().getValue(), is("12SCO1001020180621"));
        MatcherAssert.assertThat(paymentInstruction.getMetadata("Date of file").get().getValue(), is("2018-06-21"));
        assertEquals(product, paymentInstruction.getProduct());
        assertEquals(fileName, paymentInstruction.getBatchId());
    }

    @Test
    @DisplayName("SSA parser returns an error response when given invalid file")
    void readingInvalidPaymentsFileReturnsErrorResponse() throws IOException, URISyntaxException {

        String fileName = "someFileName";
        CompositeReference product = CompositeReference.parse("ssa.someProductRef");

        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("ssa_examples/invalid_payment_file.csv");
        var response = webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/SSA")
                        .queryParam("product", product.toString())
                        .queryParam("fileName", fileName)
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    @DisplayName("SSA parser returns an error when file name parameter is not provided")
    public void testSSAEndpointWithoutFileName() throws IOException {
        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("ssa_examples/example_payment_file.csv");
        webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/SSA")
                        .queryParam("product", "ssa.someProductRef")
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    @DisplayName("SSA parser returns an error when product reference parameter is not provided")
    public void testSSAEndpointWithoutProductRef() throws IOException {
        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("ssa_examples/example_payment_file.csv");
        webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/SSA")
                        .queryParam("fileName", "someFileName")
                        .build())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    @DisplayName("SSA parser returns an error when file is not provided")
    public void testSSAEndpointWithoutBody() throws IOException {
        var bodyBuilder = MultiPartFileBodyBuilder.getBuilder("ssa_examples/example_payment_file.csv");
        webTestClient
                .post()
                .uri(builder -> builder.scheme("http")
                        .path("/files/parse/SSA")
                        .queryParam("product", "ssa.someProductRef")
                        .queryParam("fileName", "someFileName")
                        .build())
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @Test
    @DisplayName("SSA parser endpoint returns an error when request method is not POST")
    public void testSSAEndpointWithInvalidRequestMethod() {
        webTestClient
                .get()
                .uri("/files/parse/SSA")
                .exchange()
                .expectStatus()
                .isEqualTo(405);
    }
}
