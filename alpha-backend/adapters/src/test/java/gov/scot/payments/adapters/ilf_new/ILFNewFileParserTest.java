package gov.scot.payments.adapters.ilf_new;

import gov.scot.payments.adapters.FileParseException;
import gov.scot.payments.adapters.ilf_new.ILFNewBACSPayment;
import gov.scot.payments.adapters.ilf_new.ILFNewFileParser;
import io.vavr.collection.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ILFNewFileParserTest {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    @Test
    void generatePaymentTest() throws IOException, FileParseException {

        var path = getClass().getClassLoader().getResource("ilf_new_examples/ILF_new_example.csv").getFile();

        try (var fileReader = new FileInputStream(path)) {
            ILFNewFileParser parser = new ILFNewFileParser();
            List<ILFNewBACSPayment> paymentList = parser.generatePaymentList(fileReader);

            assertEquals(11, paymentList.size());

            var payment = paymentList.get(0);

            assertEquals("42311", payment.getClientRef());
            assertEquals("4006639", payment.getPaymentRef());
            assertEquals("2465", payment.getPayrunRef());
            assertEquals(new BigDecimal("1495.92"), payment.getAmount());
            assertEquals(LocalDate.parse("04/12/2019", DATE_FORMATTER), payment.getPaymentDate());
            assertEquals("Bob the builder", payment.getAccountHolderName());
            assertEquals(null, payment.getBuildingSocietyNumber());
            assertEquals("804874", payment.getSortCode().getValue());
            assertEquals("11024134", payment.getAccountNumber().getValue());
            assertEquals("EXTN", payment.getFund());
            assertEquals("S", payment.getCountry());

            payment = paymentList.get(1);
            assertEquals(null, payment.getBuildingSocietyNumber());

            payment = paymentList.get(3);
            assertEquals("2/18890777-5", payment.getBuildingSocietyNumber().getValue());

            payment = paymentList.get(10);

            assertEquals("108939", payment.getClientRef());
            assertEquals("4007478", payment.getPaymentRef());
            assertEquals("2467", payment.getPayrunRef());
            assertEquals(new BigDecimal("1701.2"), payment.getAmount());
            assertEquals(LocalDate.parse("04/12/2019", DATE_FORMATTER), payment.getPaymentDate());
            assertEquals("Santa Claus", payment.getAccountHolderName());
            assertEquals(null, payment.getBuildingSocietyNumber());
            assertEquals("804518", payment.getSortCode().getValue());
            assertEquals("10691208", payment.getAccountNumber().getValue());
            assertEquals("TRAN", payment.getFund());
            assertEquals("S", payment.getCountry());

        }
    }

    @Test
    void generatePaymentTestWithEmptyRows() throws IOException, FileParseException {

        var path = getClass().getClassLoader().getResource("ilf_new_examples/ILF_new_example_with_empty.csv").getFile();

        try (var fileReader = new FileInputStream(path)) {
            ILFNewFileParser parser = new ILFNewFileParser();
            List<ILFNewBACSPayment> paymentList = parser.generatePaymentList(fileReader);
            assertEquals(10, paymentList.size());
            assertEquals("108927", paymentList.get(9).getClientRef());
        }
    }

    @Test
    void generatePaymentTestWithNotEnoughColumns() throws IOException {

        String csvRow = "42311,4006639,2465,1495.92,04/12/2019,Bob the builder,,804874,11024134,EXTN";
        try (var targetStream = new ByteArrayInputStream(csvRow.getBytes())) {
            ILFNewFileParser parser = new ILFNewFileParser();
            FileParseException e = Assertions.assertThrows(FileParseException.class, () ->parser.generatePaymentList(targetStream));
            assertEquals("Row 1 has 10 columns. Must be 11" , e.getMessage());
        }
    }

    @Test
    void generatePaymentTestWithTooManyColumns() throws IOException {

        String csvRow = "42311,4006639,2465,1495.92,04/12/2019,Bob the builder,,804874,11024134,EXTN,S,Extra";
        try (var targetStream = new ByteArrayInputStream(csvRow.getBytes())) {
            ILFNewFileParser parser = new ILFNewFileParser();
            FileParseException e = Assertions.assertThrows(FileParseException.class, () ->parser.generatePaymentList(targetStream));
            assertEquals("Row 1 has 12 columns. Must be 11" , e.getMessage());
        }
    }

    @Test
    void generatePaymentTestWithErrorRow() throws IOException {

        var path = getClass().getClassLoader().getResource("ilf_new_examples/ILF_new_error_row.csv").getFile();
        try (var fileReader = new FileInputStream(path)) {
            ILFNewFileParser parser = new ILFNewFileParser();
            FileParseException e = Assertions.assertThrows(FileParseException.class, () -> parser.generatePaymentList(fileReader));
            assertEquals("[Row 2 Field Name:'Account Number'] Parsing error: Invalid account number 346365 - must be 8 digits" , e.getMessage());
        }
    }

}