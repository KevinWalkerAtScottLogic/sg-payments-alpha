package gov.scot.payments.adapters.sppa;

import gov.scot.payments.adapters.FileParseException;
import gov.scot.payments.adapters.InvalidCSVFieldException;
import gov.scot.payments.adapters.ilf.ILFBACSPayment;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.PaymentMethod;
import gov.scot.payments.payments.model.aggregate.UKBankAccount;
import gov.scot.payments.payments.model.aggregate.UKBuildingSocietyAccount;
import gov.scot.payments.payments.model.api.CreatePaymentRequest;
import org.apache.commons.csv.CSVRecord;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.money.Monetary;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SPPABACSFileTest {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private SPPABACSPaymentHeader createValidHeader(String group, String payDate) {
        return SPPABACSPaymentHeader.builder()
                .group(group)
                .payDate(LocalDate.parse(payDate, DATE_FORMATTER))
                .build();
    }

    @Test
    @DisplayName("SPPA file is successfuly parsed from a valid string")
    void fromValidStringTest() throws IOException, InvalidCSVFieldException {
        String row = "999999     99979988    MQSXN Y                     97.99  99-97-99   RYLSCT  DQRLYNGTXN                      D/99009099-9";
        SPPABACSPayment payment = SPPABACSPayment.fromString(row, 1);
        assertEquals("999999", payment.getEmployeeNumber());
        assertEquals("99979988", payment.getEmployeeAccountNumber().getValue());
        assertEquals("MQSXN Y", payment.getAccountName());
        assertEquals(new BigDecimal("97.99"), payment.getAmount());
        assertEquals("999799", payment.getSortCode().getValue());
        assertEquals("D/99009099-9", payment.getBuildingSocietyRollNumber().getValue());
    }

    @Test
    @DisplayName("SPPA file sets roll number to null when it is not provided")
    void fromValidStringWithNoRollNumberTest() throws IOException, InvalidCSVFieldException {
        String row = "999999     99979988    MQSXN Y                     97.99  99-97-99   RYLSCT  DQRLYNGTXN";
        SPPABACSPayment payment = SPPABACSPayment.fromString(row, 1);
        assertEquals(null, payment.getBuildingSocietyRollNumber());
    }

    @Test
    @DisplayName("SPPA building society payment instruction successfuly generated from a payment")
    void fromValidPaymentToPaymentInstructionForBuildingSociety() throws IOException, InvalidCSVFieldException {
        String row = "999999     99979988    MQSXN Y                     97.99  99-97-99   RYLSCT  DQRLYNGTXN                      D/99009099-9";
        SPPABACSPayment payment = SPPABACSPayment.fromString(row, 1);

        SPPABACSPaymentHeader header = createValidHeader("99709 NHSMXNTHLY 97TH BQCS", "02/02/2020");
        CompositeReference productReference = new CompositeReference("SPPA_ID", "NHS_PRODUCT_ID");

        CreatePaymentRequest instruction = payment.toPaymentInstruction(productReference, "someFile", header);

        assertEquals("99709 NHSMXNTHLY 97TH BQCS", instruction.getBatchId());
        assertNull(instruction.getAllowedMethods());
        assertEquals(Money.of(new BigDecimal("97.99"), Monetary.getCurrency("GBP")), instruction.getAmount());
        assertEquals(LocalDate.parse("02/02/2020", DATE_FORMATTER).atStartOfDay().toInstant(ZoneOffset.UTC), instruction.getLatestExecutionDate());
        assertEquals(productReference, instruction.getProduct());

        assertEquals(UKBuildingSocietyAccount.class, instruction.getCreditorAccount().getClass());
        UKBuildingSocietyAccount account = (UKBuildingSocietyAccount)instruction.getCreditorAccount();
        assertEquals("D/99009099-9", account.getRollNumber());
        assertEquals("999799", account.getSortCode().getValue());
        assertEquals("99979988", account.getAccountNumber().getValue());

        assertEquals("MQSXN Y", instruction.getCreditor().getName());
        assertEquals("999999", instruction.getCreditorMetadata().get(0).getValue());
    }

    @Test
    @DisplayName("SPPA UK bank account instruction successfuly generated from a payment")
    void fromValidPaymentToPaymentInstructionForUKBankAccount() throws IOException, InvalidCSVFieldException {
        String row = "999999     99979988    MQSXN Y                     97.99  99-97-99   RYLSCT  DQRLYNGTXN";
        SPPABACSPayment payment = SPPABACSPayment.fromString(row, 1);

        SPPABACSPaymentHeader header = createValidHeader("99709 NHSMXNTHLY 97TH BQCS", "02/02/2020");
        CompositeReference productReference = new CompositeReference("SPPA_ID", "NHS_PRODUCT_ID");

        CreatePaymentRequest instruction = payment.toPaymentInstruction(productReference, "someFile", header);

        assertEquals("99709 NHSMXNTHLY 97TH BQCS", instruction.getBatchId());
        assertNull(instruction.getAllowedMethods());
        assertEquals(Money.of(new BigDecimal("97.99"), Monetary.getCurrency("GBP")), instruction.getAmount());
        assertEquals(LocalDate.parse("02/02/2020", DATE_FORMATTER).atStartOfDay().toInstant(ZoneOffset.UTC), instruction.getLatestExecutionDate());
        assertEquals(productReference, instruction.getProduct());

        assertEquals(UKBankAccount.class, instruction.getCreditorAccount().getClass());
        UKBankAccount account = (UKBankAccount) instruction.getCreditorAccount();
        assertEquals("999799", account.getSortCode().getValue());
        assertEquals("99979988", account.getAccountNumber().getValue());

        assertEquals("MQSXN Y", instruction.getCreditor().getName());
        assertEquals("999999", instruction.getCreditorMetadata().get(0).getValue());
    }

    @Test
    @DisplayName("SPPA file throws an exception when row is too long")
    void fromInvalidRowTest() throws IOException, InvalidCSVFieldException {
        String row = "999999     99979988    MQSXN Y                     97.99  99-97-99   RYLSCT  DQRLYNGTXN                      D/99009099-9999999999999999999999999999999999";
        FileParseException e = assertThrows(FileParseException.class, () -> SPPABACSPayment.fromString(row, 1));
        assertEquals("File row has 154 characters. Must not be longer than 127", e.getMessage());
    }

    @Test
    @DisplayName("SPPA file throws an exception when employee number is empty")
    void fromEmployeeNumberNotProvidedTest() throws IOException, InvalidCSVFieldException {
        String row = "           99979988    MQSXN Y                     97.99  99-97-99   RYLSCT  DQRLYNGTXN";
        InvalidCSVFieldException e = assertThrows(InvalidCSVFieldException.class, () -> SPPABACSPayment.fromString(row, 1));
        assertEquals("Employee Number", e.getFieldName());
        assertEquals(1, e.getRowNumber());
        assertEquals("[Row 1 Field Name:'Employee Number'] Parsing error: Field not found", e.getMessage());
    }

    @Test
    @DisplayName("SPPA file throws an exception when account name is empty")
    void fromAccountNameNotProvidedTest() throws IOException, InvalidCSVFieldException {
        String row = "999999     99979988                                 97.99  99-97-99   RYLSCT  DQRLYNGTXN                      D/99009099-9";
        InvalidCSVFieldException e = assertThrows(InvalidCSVFieldException.class, () -> SPPABACSPayment.fromString(row, 1));
        assertEquals("Account Name", e.getFieldName());
        assertEquals(1, e.getRowNumber());
        assertEquals("[Row 1 Field Name:'Account Name'] Parsing error: Field not found", e.getMessage());
    }

    @Test
    @DisplayName("SPPA file throws an exception when account name is not provided")
    void fromAccountNumberNotProvided() throws IOException, InvalidCSVFieldException {
        String row = "999999                 MQSXN Y                     97.99  99-97-99   RYLSCT  DQRLYNGTXN                      D/99009099-9";
        InvalidCSVFieldException e = assertThrows(InvalidCSVFieldException.class, () -> SPPABACSPayment.fromString(row, 1));
        assertEquals("Employee A/C No.", e.getFieldName());
        assertEquals(1, e.getRowNumber());
        assertEquals("[Row 1 Field Name:'Employee A/C No.'] Parsing error: Field not found", e.getMessage());
    }

    @Test
    @DisplayName("SPPA file throws an exception when account number is empty")
    void fromAccountNumberNotNumericalTest() throws IOException, InvalidCSVFieldException {
        String row = "999999     ABCD1234    MQSXN Y                     97.99  99-97-99   RYLSCT  DQRLYNGTXN                      D/99009099-9";
        InvalidCSVFieldException e = assertThrows(InvalidCSVFieldException.class, () -> SPPABACSPayment.fromString(row, 1));
        assertEquals("Employee A/C No.", e.getFieldName());
        assertEquals(1, e.getRowNumber());
        assertEquals("[Row 1 Field Name:'Employee A/C No.'] Parsing error: Invalid account number ABCD1234 - must be numeric", e.getMessage());
    }

    @Test
    @DisplayName("SPPA file throws an exception when amount is negative")
    void fromAmountNotValidTest() throws IOException, InvalidCSVFieldException {
        String row = "999999     99979988    MQSXN Y                     -7.99  99-97-99   RYLSCT  DQRLYNGTXN                      D/99009099-9";
        InvalidCSVFieldException e = assertThrows(InvalidCSVFieldException.class, () -> SPPABACSPayment.fromString(row, 1));
        assertEquals("Amount", e.getFieldName());
        assertEquals(1, e.getRowNumber());
        assertEquals("[Row 1 Field Name:'Amount'] Parsing error: Value -7.99 not a valid positive number", e.getMessage());
    }

    @Test
    @DisplayName("SPPA file throws an exception when amount is not numeric")
    void fromAmountNotNumericTest() throws IOException, InvalidCSVFieldException {
        String row = "999999     99979988    MQSXN Y                     B7.99  99-97-99   RYLSCT  DQRLYNGTXN                      D/99009099-9";
        InvalidCSVFieldException e = assertThrows(InvalidCSVFieldException.class, () -> SPPABACSPayment.fromString(row, 1));
        assertEquals("Amount", e.getFieldName());
        assertEquals(1, e.getRowNumber());
        assertEquals("[Row 1 Field Name:'Amount'] Parsing error: Value B7.99 not a valid positive number", e.getMessage());
    }

    @Test
    @DisplayName("SPPA file throws an exception when sort code is invalid")
    void fromSortCodeNotValidTest() throws IOException, InvalidCSVFieldException {
        String row = "999999     99979988    MQSXN Y                     97.99  99897-99   RYLSCT  DQRLYNGTXN                      D/99009099-9";
        InvalidCSVFieldException e = assertThrows(InvalidCSVFieldException.class, () -> SPPABACSPayment.fromString(row, 1));
        assertEquals("Sort Code", e.getFieldName());
        assertEquals(1, e.getRowNumber());
        assertEquals("[Row 1 Field Name:'Sort Code'] Parsing error: Invalid sort-code number 9989799 - must be 6 digits", e.getMessage());
    }

    @Test
    @DisplayName("SPPA file throws an exception when sort code is not provided")
    void fromSortCodeNotProvidedTest() throws IOException, InvalidCSVFieldException {
        String row = "999999     99979988    MQSXN Y                     97.99             RYLSCT  DQRLYNGTXN";
        InvalidCSVFieldException e = assertThrows(InvalidCSVFieldException.class, () -> SPPABACSPayment.fromString(row, 1));
        assertEquals("Sort Code", e.getFieldName());
        assertEquals(1, e.getRowNumber());
        assertEquals("[Row 1 Field Name:'Sort Code'] Parsing error: Field not found", e.getMessage());
    }

}
