package gov.scot.payments.adapters.ilf;

import gov.scot.payments.adapters.InvalidCSVFieldException;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertEquals;


class ILFBACSPaymentTest {

    private CSVRecord generateCSVRecordFromString(String row) throws IOException {

        CSVFormat ILF_CSV_FORMAT = CSVFormat.RFC4180
                .withTrim()
                .withNullString("")
                .withIgnoreEmptyLines(true);

        return CSVParser.parse(row, ILF_CSV_FORMAT).getRecords().get(0);

    }

    @Test
    void fromCSVRecordValidTest() throws IOException, InvalidCSVFieldException {
        String csvRow = "Test 164,ILF 0000,800808,11232272,511.22";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        ILFBACSPayment payment = ILFBACSPayment.fromCSVRecord(record);
        assertEquals(new BigDecimal("511.22"), payment.getAmount());
        assertEquals("800808", payment.getSortCode().getValue());
        assertEquals("11232272", payment.getAccountNumber().getValue());
        assertEquals("ILF 0000", payment.getReference());
        assertEquals("Test 164", payment.getRecipient());

    }

    @Test
    void fromCSVRecordInvalidSortCodeTest() throws IOException {
        String csvRow = "Test 164,,808,11232272,-234.21,0";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        var exception = Assertions.assertThrows(InvalidCSVFieldException.class, () -> ILFBACSPayment.fromCSVRecord(record));
        assertEquals("[Row 1 Field Name:'Sort Code'] Parsing error: Invalid sort-code number 808 - must be 6 digits", exception.getMessage());
        assertEquals("Sort Code", exception.getFieldName());
    }


    @Test
    void fromCSVRecordMissingReferenceTest() throws IOException {
        String csvRow = "Test 164,,800808,11232272,511.22";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        var exception = Assertions.assertThrows(InvalidCSVFieldException.class, () -> ILFBACSPayment.fromCSVRecord(record));
        assertEquals("Reference", exception.getFieldName());
        assertEquals("[Row 1 Field Name:'Reference'] Parsing error: Field not found", exception.getMessage());
    }

    @Test
    void fromCSVRecordMissingAmountTest() throws IOException {
        String csvRow = "Test 164,,800808,11232272,,0";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        var exception = Assertions.assertThrows(InvalidCSVFieldException.class, () -> ILFBACSPayment.fromCSVRecord(record));
        assertEquals("[Row 1 Field Name:'Amount'] Parsing error: Field not found", exception.getMessage());
        assertEquals("Amount", exception.getFieldName());
    }

    @Test
    void fromCSVRecordNegativeAmountTest() throws IOException {
        String csvRow = "Test 164,ILF_REF,800808,11232272,-234.21,0";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        var exception = Assertions.assertThrows(InvalidCSVFieldException.class, () -> ILFBACSPayment.fromCSVRecord(record));
        assertEquals("[Row 1 Field Name:'Amount'] Parsing error: Value -234.21 not a valid positive number", exception.getMessage());
        assertEquals("Amount", exception.getFieldName());
    }

    @Test
    void toPaymentInstruction() throws URISyntaxException, InvalidPaymentFieldException {

        var payment = ILFBACSPayment.builder()
                .reference("TEST_REF")
                .recipient("jon")
                .accountNumber(new UKAccountNumber("00124589"))
                .sortCode(new SortCode("801208"))
                .amount(new BigDecimal("1234.21"))
                .build();

        var instruction = payment.toPaymentInstruction(CompositeReference.parse("ilf.someProduct"), "someFile");

        assertEquals("TEST_REF", instruction.getCreditorMetadata().get(0).getValue());
        assertEquals("jon", instruction.getCreditor().getName());
        assertEquals("00124589", instruction.getCreditorAccountAs(UKBankAccount.class).getAccountNumber().getValue());
        assertEquals("801208", instruction.getCreditorAccountAs(UKBankAccount.class).getSortCode().getValue());
        assertEquals("GBP 1234.21", instruction.getAmount().toString());
        assertEquals("someProduct", instruction.getProduct().getComponent1());
        assertEquals("someFile", instruction.getBatchId());
    }
}