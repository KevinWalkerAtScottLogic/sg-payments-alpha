package gov.scot.payments.adapters.ssa;

import gov.scot.payments.adapters.FileParseException;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;

import static gov.scot.payments.adapters.ssa.SSAFileParser.FIELD_SEPARATOR;
import  static gov.scot.payments.adapters.ssa.SSAFileParser.Records;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Test SSAFileParser")
class SSAFileParserTest {

    private SSAFileParser SSAFileParser;

    private static final String EMS_PAYMENT_REF_NO_1 = "12LM111147A00001";
    private static final String CLIENT_PAYMENT_LINE_1 = "CLN|12|SCO1|12LM111147A00001|LM111147A|Surname37|||||||||SCO|901487||11212200||||1|60000|Personal Bank Acc|||||||LM111147A SSS BSG|||1000000030||500001|03||20180627|GBP||||||20180621|20180621|99||||||||||||||||";
    private static final String COMPONENT_LINE_1_1 = "CMP|12|SCO1|12LM111147A00001|70:31|500001|11|0|0|20180621|20180621|60000";
    private static final String CONTROL_LINE = "CTL|12|SCO1|12SCO1001020180621|20180621|2|60000|0|0|60000";

    private static final String EMS_PAYMENT_REF_NO_2 = "12LM111147A00002";
    private static final String CLIENT_PAYMENT_LINE_2 = "CLN|12|SCO1|12LM111147A00002|LM111147A|Surname37|||||||||SCO|901487||11212200||||1|60000|Personal Bank Acc|||||||LM111147A SSS BSG|||1000000030||500001|03||20180627|GBP||||||20180621|20180621|99||||||||||||||||";
    private static final String COMPONENT_LINE_2_1 = "CMP|12|SCO1|12LM111147A00002|70:31|500001|21|0|0|20180621|20180621|40000";
    private static final String COMPONENT_LINE_2_2 = "CMP|12|SCO1|12LM111147A00002|70:31|500001|22|0|0|20180621|20180621|20000";

    private static final String EMS_PAYMENT_REF_NO_3 = "12LM111147A00003";
    private static final String CLIENT_PAYMENT_LINE_3 = "CLN|12|SCO1|12LM111147A00003|LM111147A|Surname37|||||||||SCO|901487||11212200||||1|60000|Personal Bank Acc|||||||LM111147A SSS BSG|||1000000030||500001|03||20180627|GBP||||||20180621|20180621|99||||||||||||||||";
    private static final String COMPONENT_LINE_3_1 = "CMP|12|SCO1|12LM111147A00003|70:31|500001|31|0|0|20180621|20180621|30000";
    private static final String COMPONENT_LINE_3_2 = "CMP|12|SCO1|12LM111147A00003|70:31|500001|32|0|0|20180621|20180621|20000";
    private static final String COMPONENT_LINE_3_3 = "CMP|12|SCO1|12LM111147A00003|70:31|500001|33|0|0|20180621|20180621|10000";

    private static final String CONTROL_LINE_THREE_PAYMENTS = "CTL|12|SCO1|12SCO1001020180621|20180621|2|180000|0|0|180000";

    private static final String TPP_LINE = "TPP|12|SCO1|12LM111147A00001|70:31|500001|41010|0|0|20180621|20180621|60000";
    private static final String RVY_LINE = "RVY|12|SCO1|12LM111147A00001|70:31|500001|41010|0|0|20180621|20180621|60000";
    private static final String UNKNOWN_TYPE_LINE = "XXX|12|SCO1|12LM111147A00001|70:31|500001|41010|0|0|20180621|20180621|60000";

    @BeforeEach
    void setupParser() {
        SSAFileParser = new SSAFileParser();
    }

    @Nested
    @DisplayName("Parse Record")
    class ParseRecord {

        private SSAFileParser.Records records;

        @BeforeEach
        void setupRecords() {
            records = new SSAFileParser.Records();
        }

        @Test
        @DisplayName("When parsing a control record line then a control record is added")
        void whenParsingControlRecordLineThenControlRecordIsAdded() throws FileParseException, RecordParseException {

            SSAFileParser.parseRecord(CONTROL_LINE, records, 1);

            assertThat(
                    records.controlRecords.get(0),
                    is(SSAControlRecord.fromFields(CONTROL_LINE.split(FIELD_SEPARATOR),1))
            );
        }

        @Test
        @DisplayName("When parsing a client payment record line then a client payment record is added")
        void whenParsingClientPaymentRecordLineThenClientPaymentRecordIsAdded() throws FileParseException, RecordParseException {

            SSAFileParser.parseRecord(CLIENT_PAYMENT_LINE_1, records, 1);

            assertThat(
                    records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_1).get(),
                    is(SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 1))
            );
        }

        @Test
        @DisplayName("When parsing a component record line then a component record is added")
        void whenParsingComponentRecordLineThenComponentRecordIsAdded() throws FileParseException, RecordParseException {

            SSAFileParser.parseRecord(COMPONENT_LINE_1_1, records, 1);

            assertThat(
                    records.componentRecords.get(0),
                    is(SSAComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), 1))
            );
        }

        @Test
        @DisplayName("When parsing a TPP line then exception is thrown")
        void whenParsingTPPLineThenExceptionIsThrown() {

            assertThrows(
                    FileParseException.class,
                    () -> SSAFileParser.parseRecord(TPP_LINE, records, 1)
            );
        }

        @Test
        @DisplayName("When parsing a RVY line then exception is thrown")
        void whenParsingRVYLineThenExceptionIsThrown() {

            assertThrows(
                    FileParseException.class,
                    () -> SSAFileParser.parseRecord(RVY_LINE, records, 1)
            );
        }

        @Test
        @DisplayName("When parsing a line with unknown type exception is thrown")
        void whenParsingLineWithUnknownTypeThenExceptionIsThrown() {

            assertThrows(
                    FileParseException.class,
                    () -> SSAFileParser.parseRecord(UNKNOWN_TYPE_LINE, records, 1)
            );
        }
    }

    @Nested
    @DisplayName("Parse Records")
    class ParseRecords {

        @Test
        @DisplayName("When parsing file with single client payment record and single component then correct records are returned")
        void whenParsingFileWithSingleClientPaymentRecordAndSingleComponentThenCorrectRecordsAreReturned() throws FileParseException, RecordParseException {

            val lines =
                    "" +
                            CLIENT_PAYMENT_LINE_1 + "\n" +
                            COMPONENT_LINE_1_1 + "\n" +
                            CONTROL_LINE + "\n";

            val inputStream = new ByteArrayInputStream(lines.getBytes());

            var lineNum = 1L;

            SSAClientPaymentRecord clientPaymentRecord = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), lineNum++);
            SSAComponentRecord componentRecord = SSAComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), lineNum++);
            SSAControlRecord controlRecord = SSAControlRecord.fromFields(CONTROL_LINE.split(FIELD_SEPARATOR), lineNum);

            val records = SSAFileParser.parseRecords(inputStream);

            assertThat(records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_1).get(), is(clientPaymentRecord));
            assertThat(records.componentRecords.get(0), is(componentRecord));
            assertThat(records.controlRecords.get(0), is(controlRecord));
        }

        @Test
        @DisplayName("When parsing file with blank lines then correct records are returned")
        void whenParsingFileWithBlankLinesThenCorrectRecordsAreReturned() throws FileParseException, RecordParseException {


            val lines =
                    "\n" +
                            CLIENT_PAYMENT_LINE_1 + "\n" +
                            "   \n" +
                            COMPONENT_LINE_1_1 + "\n" +
                            "    \n" +
                            CONTROL_LINE + "\n" +
                            "\n\n";


            val inputStream = new ByteArrayInputStream(lines.getBytes());
            var lineNum = 1L;

            lineNum++;
            SSAClientPaymentRecord clientPaymentRecord = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), lineNum++);
            lineNum++;
            SSAComponentRecord componentRecord = SSAComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), lineNum++);
            lineNum++;
            SSAControlRecord controlRecord = SSAControlRecord.fromFields(CONTROL_LINE.split(FIELD_SEPARATOR), lineNum);

            val records = SSAFileParser.parseRecords(inputStream);

            assertThat(records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_1).get(), is(clientPaymentRecord));
            assertThat(records.componentRecords.get(0), is(componentRecord));
            assertThat(records.controlRecords.get(0), is(controlRecord));
        }

        @Test
        @DisplayName("When parsing file with lines of unhandled types then exception is thrown")
        void whenParsingFileWithLinesOfUnhandledTypesThenExceptionIsThrown() {

            val lines =
                    "" +
                            CLIENT_PAYMENT_LINE_1 + "\n" +
                            TPP_LINE + "\n" +
                            COMPONENT_LINE_1_1 + "\n" +
                            RVY_LINE + "\n" +
                            UNKNOWN_TYPE_LINE + "\n" +
                            CONTROL_LINE + "\n";

            val inputStream = new ByteArrayInputStream(lines.getBytes());

            assertThrows(
                    FileParseException.class,
                    () -> SSAFileParser.parseRecords(inputStream)
            );
        }

        @Test
        @DisplayName("When parsing file with multiple client payment records and multiple components then correct records are returned")
        void whenParsingFileWithMultipleClientPaymentRecordsAndMultipleComponentThenCorrectRecordsAreReturned() throws FileParseException, RecordParseException {

            val lines =
                    "" +
                            CLIENT_PAYMENT_LINE_1 + "\n" +
                            COMPONENT_LINE_1_1 + "\n" +
                            CLIENT_PAYMENT_LINE_2 + "\n" +
                            COMPONENT_LINE_2_1 + "\n" +
                            COMPONENT_LINE_2_2 + "\n" +
                            CLIENT_PAYMENT_LINE_3 + "\n" +
                            COMPONENT_LINE_3_1 + "\n" +
                            COMPONENT_LINE_3_2 + "\n" +
                            COMPONENT_LINE_3_3 + "\n" +
                            CONTROL_LINE + "\n";

            val inputStream = new ByteArrayInputStream(lines.getBytes());
            var lineNum = 1L;

            SSAClientPaymentRecord clientPaymentRecord1 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), lineNum++);

            SSAComponentRecord componentRecord11 = SSAComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), lineNum++);

            SSAClientPaymentRecord clientPaymentRecord2 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_2.split(FIELD_SEPARATOR), lineNum++);

            SSAComponentRecord componentRecord21 = SSAComponentRecord.fromFields(COMPONENT_LINE_2_1.split(FIELD_SEPARATOR), lineNum++);

            SSAComponentRecord componentRecord22 = SSAComponentRecord.fromFields(COMPONENT_LINE_2_2.split(FIELD_SEPARATOR), lineNum++);

            SSAClientPaymentRecord clientPaymentRecord3 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_3.split(FIELD_SEPARATOR), lineNum++);

            SSAComponentRecord componentRecord31 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_1.split(FIELD_SEPARATOR), lineNum++);

            SSAComponentRecord componentRecord32 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_2.split(FIELD_SEPARATOR), lineNum++);

            SSAComponentRecord componentRecord33 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_3.split(FIELD_SEPARATOR), lineNum++);

            SSAControlRecord controlRecord = SSAControlRecord.fromFields(CONTROL_LINE.split(FIELD_SEPARATOR), lineNum);

            val records = SSAFileParser.parseRecords(inputStream);

            assertThat(records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_1).get(), is(clientPaymentRecord1));
            assertThat(records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_2).get(), is(clientPaymentRecord2));
            assertThat(records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_3).get(), is(clientPaymentRecord3));

            assertThat(records.componentRecords,
                    containsInAnyOrder(
                            componentRecord11,
                            componentRecord21,
                            componentRecord22,
                            componentRecord31,
                            componentRecord32,
                            componentRecord33
                    ));

            assertThat(records.controlRecords.get(0), is(controlRecord));
        }
    }

    @Nested
    @DisplayName("Link Records")
    class LinkRecords {

        @Test
        @DisplayName("When linking with single client payment record and single component then correct link is made")
        void whenLinkingWithSingleClientPaymentRecordAndSingleComponentThenCorrectLinkIsMade() throws FileParseException, RecordParseException {

            var records = new SSAFileParser.Records();
            val clientPayment = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 99);
            val component = SSAComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), 100);
            records.clientPaymentRecords = records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_1, clientPayment);
            records.componentRecords = records.componentRecords.append(component);

            records = SSAFileParser.linkRecords(records);

            assertThat(records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_1).get().getComponents(), contains(component));
        }

        @Test
        @DisplayName("When linking with single client payment record and mismatched single component then exception is thrown")
        void whenLinkingWithSingleClientPaymentRecordAndMismatchedComponentThenExceptionIsThrown() throws RecordParseException, FileParseException {

            var records = new Records();
            val clientPayment = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 99);
            val component = SSAComponentRecord.fromFields(COMPONENT_LINE_2_1.split(FIELD_SEPARATOR), 100);
            records.clientPaymentRecords = records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_1, clientPayment);
            records.componentRecords = records.componentRecords.append(component);

            assertThrows(
                    FileParseException.class,
                    () -> SSAFileParser.linkRecords(records)
            );
        }

        @Test
        @DisplayName("When linking with multiple client payment records and multiple components then correct links are made")
        void whenLinkingWithMultipleClientPaymentRecordsAndMultipleComponentsThenCorrectLinksAreMade() throws FileParseException, RecordParseException {

            var records = new SSAFileParser.Records();
            val clientPayment1 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 100);
            val component11 = SSAComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), 101);
            val clientPayment2 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_2.split(FIELD_SEPARATOR), 102);
            val component21 = SSAComponentRecord.fromFields(COMPONENT_LINE_2_1.split(FIELD_SEPARATOR), 103);
            val component22 = SSAComponentRecord.fromFields(COMPONENT_LINE_2_2.split(FIELD_SEPARATOR), 104);
            val clientPayment3 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_3.split(FIELD_SEPARATOR), 105);
            val component31 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_1.split(FIELD_SEPARATOR), 106);
            val component32 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_2.split(FIELD_SEPARATOR), 107);
            val component33 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_3.split(FIELD_SEPARATOR), 108);

            records.clientPaymentRecords = records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_1, clientPayment1)
                .put(EMS_PAYMENT_REF_NO_2, clientPayment2)
                .put(EMS_PAYMENT_REF_NO_3, clientPayment3);
            records.componentRecords = records.componentRecords.append(component32)
                .append(component31)
                .append(component33)
                .append(component11)
                .append(component21)
                .append(component22);

            records = SSAFileParser.linkRecords(records);

            assertThat(
                    records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_1).get().getComponents(),
                    contains(component11)
            );
            assertThat(
                    records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_2).get().getComponents(),
                    containsInAnyOrder(component21, component22)
            );
            assertThat(
                    records.clientPaymentRecords.get(EMS_PAYMENT_REF_NO_3).get().getComponents(),
                    containsInAnyOrder(component31, component32, component33)
            );
        }
    }

    @Nested
    @DisplayName("Validate Records")
    class ValidateRecords {

        @Test
        @DisplayName("When validating a valid set of records then no exception is thrown")
        void whenValidatingAValidSetOfRecordsThenNoExceptionIsThrown() throws FileParseException, RecordParseException {

            var records = new SSAFileParser.Records();
            val controlRecord = SSAControlRecord.fromFields(CONTROL_LINE_THREE_PAYMENTS.split(FIELD_SEPARATOR), 99);
            val clientPayment1 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 100);
            val component11 = SSAComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), 101);
            val clientPayment2 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_2.split(FIELD_SEPARATOR), 102);
            val component21 = SSAComponentRecord.fromFields(COMPONENT_LINE_2_1.split(FIELD_SEPARATOR), 103);
            val component22 = SSAComponentRecord.fromFields(COMPONENT_LINE_2_2.split(FIELD_SEPARATOR), 104);
            val clientPayment3 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_3.split(FIELD_SEPARATOR), 105);
            val component31 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_1.split(FIELD_SEPARATOR), 106);
            val component32 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_2.split(FIELD_SEPARATOR), 107);
            val component33 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_3.split(FIELD_SEPARATOR), 108);

            records.controlRecords = records.controlRecords.append(controlRecord);
            records.clientPaymentRecords = records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_1, clientPayment1)
                .put(EMS_PAYMENT_REF_NO_2, clientPayment2)
                .put(EMS_PAYMENT_REF_NO_3, clientPayment3);
            clientPayment1.addComponents(component11);
            clientPayment2.addComponents(component21, component22);
            clientPayment3.addComponents(component31, component32, component33);

            SSAFileParser.validateRecords(records);
        }

        @Test
        @DisplayName("When validating a set of records with a missing component then an exception is thrown")
        void whenValidatingASetOfRecordsWithAMissingComponentThenAnExceptionIsThrown() throws FileParseException, RecordParseException {

            var records = new Records();
            val controlRecord = SSAControlRecord.fromFields(CONTROL_LINE_THREE_PAYMENTS.split(FIELD_SEPARATOR), 99);
            val clientPayment1 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 100);
            val component11 = SSAComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), 101);
            val clientPayment2 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_2.split(FIELD_SEPARATOR), 102);
            val component21 = SSAComponentRecord.fromFields(COMPONENT_LINE_2_1.split(FIELD_SEPARATOR), 103);
            val component22 = SSAComponentRecord.fromFields(COMPONENT_LINE_2_2.split(FIELD_SEPARATOR), 104);
            val clientPayment3 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_3.split(FIELD_SEPARATOR), 105);
            val component31 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_1.split(FIELD_SEPARATOR), 106);
            val component33 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_3.split(FIELD_SEPARATOR), 107);

            records.controlRecords = records.controlRecords.append(controlRecord);
            records.clientPaymentRecords = records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_1, clientPayment1)
                .put(EMS_PAYMENT_REF_NO_2, clientPayment2)
                .put(EMS_PAYMENT_REF_NO_3, clientPayment3);
            clientPayment1.addComponents(component11);
            clientPayment2.addComponents(component21, component22);
            clientPayment3.addComponents(component31, component33);

            assertThrows(
                    FileParseException.class,
                    () -> SSAFileParser.validateRecords(records)
            );
        }

        @Test
        @DisplayName("When validating a set of records with incorrect hash net amount then an exception is thrown")
        void whenValidatingASetOfRecordsWithInvalidHashNetAmountThenAnExceptionIsThrown() throws FileParseException, RecordParseException {

            var records = new Records();
            val controlRecord = SSAControlRecord.fromFields(CONTROL_LINE.split(FIELD_SEPARATOR), 99);
            val clientPayment1 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 100);
            val component11 = SSAComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), 101);
            val clientPayment2 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_2.split(FIELD_SEPARATOR), 102);
            val component21 = SSAComponentRecord.fromFields(COMPONENT_LINE_2_1.split(FIELD_SEPARATOR), 103);
            val component22 = SSAComponentRecord.fromFields(COMPONENT_LINE_2_2.split(FIELD_SEPARATOR), 104);
            val clientPayment3 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_3.split(FIELD_SEPARATOR), 105);
            val component31 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_1.split(FIELD_SEPARATOR), 106);
            val component32 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_2.split(FIELD_SEPARATOR), 107);
            val component33 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_3.split(FIELD_SEPARATOR), 108);

            records.controlRecords = records.controlRecords.append(controlRecord);
            records.clientPaymentRecords = records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_1, clientPayment1)
                .put(EMS_PAYMENT_REF_NO_2, clientPayment2)
                .put(EMS_PAYMENT_REF_NO_3, clientPayment3);
            clientPayment1.addComponents(component11);
            clientPayment2.addComponents(component21, component22);
            clientPayment3.addComponents(component31, component32, component33);

            assertThrows(
                    FileParseException.class,
                    () -> SSAFileParser.validateRecords(records)
            );
        }

        @Test
        @DisplayName("When validating a set of records with no control record then an exception is thrown")
        void whenValidatingASetOfRecordsWithNoControlRecordThenAnExceptionIsThrown() throws FileParseException, RecordParseException {

            var records = new Records();
            val clientPayment1 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 100);
            val component11 = SSAComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), 101);
            val clientPayment2 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_2.split(FIELD_SEPARATOR), 102);
            val component21 = SSAComponentRecord.fromFields(COMPONENT_LINE_2_1.split(FIELD_SEPARATOR), 103);
            val component22 = SSAComponentRecord.fromFields(COMPONENT_LINE_2_2.split(FIELD_SEPARATOR), 104);
            val clientPayment3 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_3.split(FIELD_SEPARATOR), 105);
            val component31 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_1.split(FIELD_SEPARATOR), 106);
            val component33 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_3.split(FIELD_SEPARATOR), 107);

            records.clientPaymentRecords = records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_1, clientPayment1)
                .put(EMS_PAYMENT_REF_NO_2, clientPayment2)
                .put(EMS_PAYMENT_REF_NO_3, clientPayment3);
            clientPayment1.addComponents(component11);
            clientPayment2.addComponents(component21, component22);
            clientPayment3.addComponents(component31, component33);

            assertThrows(
                    FileParseException.class,
                    () -> SSAFileParser.validateRecords(records)
            );
        }

        @Test
        @DisplayName("When validating a set of records with multiple control records then an exception is thrown")
        void whenValidatingASetOfRecordsWithMultipleControlRecordsThenAnExceptionIsThrown() throws FileParseException, RecordParseException {

            var records = new Records();
            val controlRecord1 = SSAControlRecord.fromFields(CONTROL_LINE.split(FIELD_SEPARATOR), 99);
            val controlRecord2 = SSAControlRecord.fromFields(CONTROL_LINE_THREE_PAYMENTS.split(FIELD_SEPARATOR), 99);
            val clientPayment1 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_1.split(FIELD_SEPARATOR), 100);
            val component11 = SSAComponentRecord.fromFields(COMPONENT_LINE_1_1.split(FIELD_SEPARATOR), 101);
            val clientPayment2 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_2.split(FIELD_SEPARATOR), 102);
            val component21 = SSAComponentRecord.fromFields(COMPONENT_LINE_2_1.split(FIELD_SEPARATOR), 103);
            val component22 = SSAComponentRecord.fromFields(COMPONENT_LINE_2_2.split(FIELD_SEPARATOR), 104);
            val clientPayment3 = SSAClientPaymentRecord.fromFields(CLIENT_PAYMENT_LINE_3.split(FIELD_SEPARATOR), 105);
            val component31 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_1.split(FIELD_SEPARATOR), 106);
            val component33 = SSAComponentRecord.fromFields(COMPONENT_LINE_3_3.split(FIELD_SEPARATOR), 107);

            records.controlRecords = records.controlRecords.appendAll(asList(controlRecord1, controlRecord2));
            records.clientPaymentRecords = records.clientPaymentRecords.put(EMS_PAYMENT_REF_NO_1, clientPayment1)
                .put(EMS_PAYMENT_REF_NO_2, clientPayment2)
                .put(EMS_PAYMENT_REF_NO_3, clientPayment3);
            clientPayment1.addComponents(component11);
            clientPayment2.addComponents(component21, component22);
            clientPayment3.addComponents(component31, component33);

            assertThrows(
                    FileParseException.class,
                    () -> SSAFileParser.validateRecords(records)
            );
        }
    }
}
