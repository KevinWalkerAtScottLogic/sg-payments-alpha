package gov.scot.payments.adapters.ilf_new;

import gov.scot.payments.adapters.InvalidCSVFieldException;
import gov.scot.payments.adapters.RollNumber;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.InvalidPaymentFieldException;
import gov.scot.payments.payments.model.aggregate.MetadataField;
import gov.scot.payments.payments.model.aggregate.SortCode;
import gov.scot.payments.payments.model.aggregate.UKAccountNumber;
import gov.scot.payments.payments.model.aggregate.UKBuildingSocietyAccount;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ILFNewBACSPaymentTest {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private CSVRecord generateCSVRecordFromString(String row) throws IOException {

        CSVFormat ILF_CSV_FORMAT = CSVFormat.RFC4180
                .withTrim()
                .withNullString("")
                .withIgnoreEmptyLines(true);

        return CSVParser.parse(row, ILF_CSV_FORMAT).getRecords().get(0);

    }

    @Test
    void fromCSVRecordValidTest() throws IOException, InvalidCSVFieldException {
        String csvRow = "42311,4006639,2465,1495.92,04/12/2019,Bob the builder,2/55994511-2,804874,11024134,EXTN,S";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        ILFNewBACSPayment payment = ILFNewBACSPayment.fromCSVRecord(record);
        assertEquals("42311", payment.getClientRef());
        assertEquals("4006639", payment.getPaymentRef());
        assertEquals("2465", payment.getPayrunRef());
        assertEquals(new BigDecimal("1495.92"), payment.getAmount());
        assertEquals(LocalDate.parse("04/12/2019", DATE_FORMATTER), payment.getPaymentDate());
        assertEquals("Bob the builder", payment.getAccountHolderName());
        assertEquals("2/55994511-2", payment.getBuildingSocietyNumber().getValue());
        assertEquals("804874", payment.getSortCode().getValue());
        assertEquals("11024134", payment.getAccountNumber().getValue());
        assertEquals("EXTN", payment.getFund());
        assertEquals("S", payment.getCountry());
    }

    @Test
    void fromCSVRecordValidNullBuildingSocietyNumberTest() throws IOException, InvalidCSVFieldException {
        String csvRow = "107998,4007471,2467,3500,04/12/2019,Rodney the hamster,NULL,110836,00266735,TRAN,S";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        ILFNewBACSPayment payment = ILFNewBACSPayment.fromCSVRecord(record);
        assertEquals(null, payment.getBuildingSocietyNumber());
    }

    @Test
    void fromCSVRecordValidEmptyBuildingSocietyNumberTest() throws IOException, InvalidCSVFieldException {
        String csvRow = "107998,4007471,2467,3500,04/12/2019,Rodney the hamster,,110836,00266735,TRAN,S";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        ILFNewBACSPayment payment = ILFNewBACSPayment.fromCSVRecord(record);
        assertEquals(null, payment.getBuildingSocietyNumber());
    }

    @Test
    void fromCSVRecordInvalidAccountNumberTest() throws IOException {
        String csvRow = "42311,4006639,2465,1495.92,04/12/2019,Bob the builder,,804874,110241,EXTN,S";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        var exception = Assertions.assertThrows(InvalidCSVFieldException.class, () -> ILFNewBACSPayment.fromCSVRecord(record));
        assertEquals("[Row 1 Field Name:'Account Number'] Parsing error: Invalid account number 110241 - must be 8 digits", exception.getMessage());
        assertEquals("Account Number", exception.getFieldName());
    }

    @Test
    void fromCSVRecordInvalidDateFormatTest() throws IOException {
        String csvRow = "42311,4006639,2465,1495.92,2019/04/12,Bob the builder,,804874,11024122,EXTN,S";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        var exception = Assertions.assertThrows(InvalidCSVFieldException.class, () -> ILFNewBACSPayment.fromCSVRecord(record));
        assertEquals("[Row 1 Field Name:'Payment Date'] Parsing error: Text '2019/04/12' could not be parsed at index 2", exception.getMessage());
        assertEquals("Payment Date", exception.getFieldName());
    }

    @Test
    void fromCSVRecordInvalidSortCodeTest() throws IOException {
        String csvRow = "42311,4006639,2465,1495.92,04/12/2019,Bob the builder,,80487,11024134,EXTN,S";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        var exception = Assertions.assertThrows(InvalidCSVFieldException.class, () -> ILFNewBACSPayment.fromCSVRecord(record));
        assertEquals("[Row 1 Field Name:'Sort Code'] Parsing error: Invalid sort-code number 80487 - must be 6 digits", exception.getMessage());
        assertEquals("Sort Code", exception.getFieldName());
    }

    @Test
    void fromCSVRecordMissingAmountTest() throws IOException {
        String csvRow = "42311,4006639,2465,,04/12/2019,Bob the builder,,80487,11024134,EXTN,S";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        var exception = Assertions.assertThrows(InvalidCSVFieldException.class, () -> ILFNewBACSPayment.fromCSVRecord(record));
        assertEquals("[Row 1 Field Name:'Amount'] Parsing error: Field not found", exception.getMessage());
        assertEquals("Amount", exception.getFieldName());
    }

    @Test
    void fromCSVRecordNegativeAmountTest() throws IOException {
        String csvRow = "42311,4006639,2465,-234.21,04/12/2019,Bob the builder,,80487,11024134,EXTN,S";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        var exception = Assertions.assertThrows(InvalidCSVFieldException.class, () -> ILFNewBACSPayment.fromCSVRecord(record));
        assertEquals("[Row 1 Field Name:'Amount'] Parsing error: Value -234.21 not a valid positive number", exception.getMessage());
        assertEquals("Amount", exception.getFieldName());
    }

    @Test
    void fromCSVRecordMissingFundTest() throws IOException {
        String csvRow = "42311,4006639,2465,234.21,04/12/2019,Bob the builder,,804870,11024134,,S";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        var exception = Assertions.assertThrows(InvalidCSVFieldException.class, () -> ILFNewBACSPayment.fromCSVRecord(record));
        assertEquals("[Row 1 Field Name:'Fund'] Parsing error: Field not found", exception.getMessage());
        assertEquals("Fund", exception.getFieldName());
    }

    @Test
    void fromCSVRecordMissingCountryTest() throws IOException {
        String csvRow = "42311,4006639,2465,234.21,04/12/2019,Bob the builder,,804870,11024134,EXTN,";
        CSVRecord record = generateCSVRecordFromString(csvRow);
        var exception = Assertions.assertThrows(InvalidCSVFieldException.class, () -> ILFNewBACSPayment.fromCSVRecord(record));
        assertEquals("[Row 1 Field Name:'Country'] Parsing error: Field not found", exception.getMessage());
        assertEquals("Country", exception.getFieldName());
    }

    @Test
    void toPaymentInstruction() throws URISyntaxException, InvalidPaymentFieldException {

        var payment = ILFNewBACSPayment.builder()
                .clientRef("123")
                .paymentRef("456")
                .payrunRef("789")
                .amount(new BigDecimal("1234.21"))
                .paymentDate(LocalDate.parse("04/12/2019", DATE_FORMATTER))
                .accountHolderName("Jon")
                .buildingSocietyNumber(new RollNumber("123"))
                .sortCode(new SortCode("801208"))
                .accountNumber(new UKAccountNumber("00124589"))
                .fund("TRAN")
                .country("S")
                .build();

        var instruction = payment.toPaymentInstruction(CompositeReference.parse("ilf.someProduct"), "someFile");

        assertEquals("123", instruction.getCreditorMetadata(MetadataField.CLIENT_REF).get().getValue());
        assertEquals("Jon", instruction.getCreditor().getName());
        assertEquals("123", instruction.getCreditorAccountAs(UKBuildingSocietyAccount.class).getRollNumber());
        assertEquals("00124589", instruction.getCreditorAccountAs(UKBuildingSocietyAccount.class).getAccountNumber().getValue());
        assertEquals("801208", instruction.getCreditorAccountAs(UKBuildingSocietyAccount.class).getSortCode().getValue());
        assertEquals("GBP 1234.21", instruction.getAmount().toString());
        assertEquals("TRAN-S", instruction.getProduct().getComponent1());
        assertEquals("789", instruction.getBatchId());
    }
}
