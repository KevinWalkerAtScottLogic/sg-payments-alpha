package gov.scot.payments.adapters.ssa;

import gov.scot.payments.adapters.FileParseException;
import gov.scot.payments.adapters.RollNumber;
import gov.scot.payments.payments.model.aggregate.InvalidPaymentFieldException;
import gov.scot.payments.payments.model.aggregate.SortCode;
import gov.scot.payments.payments.model.aggregate.UKAccountNumber;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static gov.scot.payments.adapters.ssa.SSAClientPaymentRecord.DirectPaymentType.BANK;
import static gov.scot.payments.adapters.ssa.SSAClientPaymentRecord.DirectPaymentType.BUILDING_SOCIETY;
import static gov.scot.payments.adapters.ssa.SSAFileParser.FIELD_SEPARATOR;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Client payment record")
class SSAClientPaymentRecordTest {

    @Nested
    @DisplayName("Parse from fields")
    class FromFields {

        @Test
        @DisplayName("with example payment line then record is returned")
        void withExamplePaymentLineThenRecordIsReturned() throws FileParseException, RecordParseException, InvalidPaymentFieldException {

            var line = "CLN|12|SCO1|12LM111147A00001|LM111147A|Surname37|||||||||SCO|901487||11212200||||1|60000|Personal Bank Acc|||||||LM111147A SSS BSG|||1000000030||500001|03||20180627|GBP||||||20180621|20180621|99||||||||||||||||";
            var fields = line.split(FIELD_SEPARATOR);

            var record = SSAClientPaymentRecord.fromFields(fields, 99);

            assertThat(record.getEntitlementManagementSystem(), is("12"));
            assertThat(record.getServiceIdentifier(), is("SCO1"));
            assertThat(record.getEmsPaymentRefNo(), is("12LM111147A00001"));
            assertThat(record.getNino(), is("LM111147A"));
            assertThat(record.getSurname(), is("Surname37"));
            assertThat(record.getCountryCode(), is("SCO"));
            assertThat(record.getSortCode(), is(SortCode.fromString("901487")));
            assertThat(record.getAccountNumber(), is(UKAccountNumber.fromString("11212200")));
            assertThat(record.getRollNumber(), is(RollNumber.fromString("")));
            assertThat(record.getDirectPaymentType(), is(BANK));
            assertThat(record.getNetAmount(), is(new BigDecimal("600.00")));
            assertThat(record.getAccountName(), is("Personal Bank Acc"));
            assertThat(record.getDwpBacsReference(), is("LM111147A SSS BSG"));
            assertThat(record.getDwpBankAccountIdentifier(), is("1000000030"));
            assertThat(record.getIssuingOffice(), is("500001"));
            assertThat(record.getMethodOfPayment(), is("03"));
            assertThat(record.getDueDate(), is(LocalDate.of(2018, 6, 27)));
            assertThat(record.getCurrencyCode(), is("GBP"));
            assertThat(record.getPaymentPeriodStartDate(), is(LocalDate.of(2018, 6, 21)));
            assertThat(record.getPaymentPeriodEndDate(), is(LocalDate.of(2018, 6, 21)));
            assertThat(record.getPaymentPriority(), is("99"));
        }

        @Test()
        @DisplayName("with invalid net amount then an exception is thrown")
        void withInvalidNetAmountThenExceptionIsThrown() {

            var line = "CLN|12|SCO1|12LM111147A00001|LM111147A|Surname37|||||||||SCO|901487||11212200||||1|60000XX|Personal Bank Acc|||||||LM111147A SSS BSG|||1000000030||500001|03||20180627|GBP||||||20180621|20180621|99||||||||||||||||";
            var fields = line.split(FIELD_SEPARATOR);

            assertThrows(
                    RecordParseException.class,
                    () -> SSAClientPaymentRecord.fromFields(fields, 99)
            );
        }

        @Test
        @DisplayName("with bank direct payment type and account number then record is returned")
        void withBankDirectPaymentTypeAndAccountNumberThenRecordIsReturned() throws FileParseException, RecordParseException, InvalidPaymentFieldException {

            var line = "CLN|12|SCO1|12LM111147A00001|LM111147A|Surname37|||||||||SCO|901487||11212200||||1|60000|Personal Bank Acc|||||||LM111147A SSS BSG|||1000000030||500001|03||20180627|GBP||||||20180621|20180621|99||||||||||||||||";
            var fields = line.split(FIELD_SEPARATOR);

            var record = SSAClientPaymentRecord.fromFields(fields, 99);

            assertThat(record.getAccountNumber(), is(UKAccountNumber.fromString("11212200")));
            assertThat(record.getRollNumber(), is(RollNumber.fromString("")));
            assertThat(record.getDirectPaymentType(), is(BANK));
        }

        @Test
        @DisplayName("with building society direct payment type and roll number then record is returned")
        void withBuildingSocietyPaymentTypeAndAccountNumberAndRollNumberThenRecordIsReturned() throws FileParseException, RecordParseException, InvalidPaymentFieldException {

            var line = "CLN|12|SCO1|12LM111147A00001|LM111147A|Surname37|||||||||SCO|901487||11212200|||123XXX|3|60000|Personal Bank Acc|||||||LM111147A SSS BSG|||1000000030||500001|03||20180627|GBP||||||20180621|20180621|99||||||||||||||||";
            var fields = line.split(FIELD_SEPARATOR);

            var record = SSAClientPaymentRecord.fromFields(fields, 99);

            assertThat(record.getAccountNumber(), is(UKAccountNumber.fromString("11212200")));
            assertThat(record.getRollNumber(), is(RollNumber.fromString("123XXX")));
            assertThat(record.getDirectPaymentType(), is(BUILDING_SOCIETY));
        }

        @Test
        @DisplayName("with bank direct payment type and roll number then exception is thrown")
        void withBankDirectPaymentTypeAndRollNumberThenExceptionIsThrown() {

            var line = "CLN|12|SCO1|12LM111147A00001|LM111147A|Surname37|||||||||SCO|901487||11212200||||3|60000|Personal Bank Acc|||||||LM111147A SSS BSG|||1000000030||500001|03||20180627|GBP||||||20180621|20180621|99||||||||||||||||";
            var fields = line.split(FIELD_SEPARATOR);

            assertThrows(
                    FileParseException.class,
                    () -> SSAClientPaymentRecord.fromFields(fields, 99)
            );
        }

        @Test
        @DisplayName("with bank direct payment type and roll number then exception is thrown")
        void withBankPaymentTypeAndRollNumberThenExceptionIsThrown() {

            var line = "CLN|12|SCO1|12LM111147A00001|LM111147A|Surname37|||||||||SCO|901487|||||11212200|1|60000|Personal Bank Acc|||||||LM111147A SSS BSG|||1000000030||500001|03||20180627|GBP||||||20180621|20180621|99||||||||||||||||";
            var fields = line.split(FIELD_SEPARATOR);

            assertThrows(
                    IllegalArgumentException.class,
                    () -> SSAClientPaymentRecord.fromFields(fields, 99)
            );
        }
    }

    @Nested
    @DisplayName("Check component amount total matches net amount")
    class checkComponentAmountTotal {

        @Test
        @DisplayName("When component amounts total equals net amount then no exception is thrown")
        void whenComponentAmountsTotalEqualsNetAmountThenNoExceptionIsThrown() throws RecordParseException, FileParseException {

            String clientPaymentLine = "CLN|12|SCO1|12LM111147A00003|LM111147A|Surname37|||||||||SCO|901487||11212200||||1|60000|Personal Bank Acc|||||||LM111147A SSS BSG|||1000000030||500001|03||20180627|GBP||||||20180621|20180621|99||||||||||||||||";
            String componentLine1 = "CMP|12|SCO1|12LM111147A00003|70:31|500001|31|0|0|20180621|20180621|30000";
            String componentLine2 = "CMP|12|SCO1|12LM111147A00003|70:31|500001|32|0|0|20180621|20180621|20000";
            String componentLine3 = "CMP|12|SCO1|12LM111147A00003|70:31|500001|33|0|0|20180621|20180621|10000";

            var clientPayment = SSAClientPaymentRecord.fromFields(clientPaymentLine.split(FIELD_SEPARATOR), 101);
            var component1 = SSAComponentRecord.fromFields(componentLine1.split(FIELD_SEPARATOR), 102);
            var component2 = SSAComponentRecord.fromFields(componentLine2.split(FIELD_SEPARATOR), 102);
            var component3 = SSAComponentRecord.fromFields(componentLine3.split(FIELD_SEPARATOR), 102);

            clientPayment.addComponents(component1, component2, component3);

            clientPayment.checkComponentAmountTotal();
        }

        @Test
        @DisplayName("When component amounts total does not equal net amount then an exception is thrown")
        void whenComponentAmountsTotalDoesNotEqualNetAmountThenExceptionIsThrown() throws RecordParseException, FileParseException {

            String clientPaymentLine = "CLN|12|SCO1|12LM111147A00003|LM111147A|Surname37|||||||||SCO|901487||11212200||||1|60000|Personal Bank Acc|||||||LM111147A SSS BSG|||1000000030||500001|03||20180627|GBP||||||20180621|20180621|99||||||||||||||||";
            String componentLine1 = "CMP|12|SCO1|12LM111147A00003|70:31|500001|31|0|0|20180621|20180621|90000";
            String componentLine2 = "CMP|12|SCO1|12LM111147A00003|70:31|500001|32|0|0|20180621|20180621|20000";
            String componentLine3 = "CMP|12|SCO1|12LM111147A00003|70:31|500001|33|0|0|20180621|20180621|10000";

            var clientPayment = SSAClientPaymentRecord.fromFields(clientPaymentLine.split(FIELD_SEPARATOR), 101);
            var component1 = SSAComponentRecord.fromFields(componentLine1.split(FIELD_SEPARATOR), 102);
            var component2 = SSAComponentRecord.fromFields(componentLine2.split(FIELD_SEPARATOR), 102);
            var component3 = SSAComponentRecord.fromFields(componentLine3.split(FIELD_SEPARATOR), 102);

            clientPayment.addComponents(component1, component2, component3);

            assertThrows(
                    FileParseException.class,
                    () -> clientPayment.checkComponentAmountTotal()
            );
        }
    }
}
