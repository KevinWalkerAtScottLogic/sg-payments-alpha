package gov.scot.payments.adapters.sppa;

import gov.scot.payments.adapters.FileParseException;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

public class SPPAFileParserTest {

    private SPPAFileParser parser;

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private static final UKBankAccount INTERNATIONAL_ACCOUNT = UKBankAccount.builder()
            .accountNumber(UKAccountNumber.fromString("00000000"))
            .sortCode(SortCode.fromString("000000"))
            .currency("GBP")
            .name("account")
            .build();

    private static final Map<String, CashAccount> INTERNATIONAL_EMPLOYEE_ACCOUNTS = HashMap.of("999991", SepaBankAccount.builder()
                    .iban("DE75512108001245126199")
                    .currency("EUR")
                    .name("account")
                    .build()
            ,"789123"
            ,SepaBankAccount.builder()
                    .iban("FI1410093000123458")
                    .currency("EUR")
                    .name("account")
                    .build());

    @BeforeEach
    void setupParser() {
        parser = new SPPAFileParser(INTERNATIONAL_ACCOUNT,INTERNATIONAL_EMPLOYEE_ACCOUNTS);
    }

    @Test
    @DisplayName("SPPA file parser successfuly parses a valid single batch file")
    void parseSingleBatchFileTest() throws IOException, FileParseException {

        var path = getClass().getClassLoader().getResource("sppa_examples/SPPA_single_batch.txt").getFile();

        try (var fileReader = new FileInputStream(path)) {
            SPPAFileParser.SPPAPaymentBatch batch = parser.generatePaymentBatches(fileReader);

            assertNotNull(batch.header);
            var header = batch.header;
            assertEquals("99709 AAAMONTHLY 97TH BQCS", header.getGroup());
            assertEquals(LocalDate.parse("02/02/2020", DATE_FORMATTER), header.getPayDate());

            assertEquals(39, batch.payments.size());
            var payment = batch.payments.get(0);
            assertEquals("999999", payment.getEmployeeNumber());
            assertEquals("99979988", payment.getEmployeeAccountNumber().getValue());
            assertEquals("AAAAAAAA", payment.getAccountName());
            assertEquals(new BigDecimal("97.99"), payment.getAmount());
            assertEquals("999799", payment.getSortCode().getValue());
            assertEquals("D/99009099-9", payment.getBuildingSocietyRollNumber().getValue());

            assertEquals(SepaBankAccount.class,batch.payments.get(1).toPaymentInstruction(new CompositeReference("",""),null,batch.header,parser::findAccountDetails).getCreditorAccount().getClass());
            assertEquals(UKBankAccount.class,batch.payments.get(2).toPaymentInstruction(new CompositeReference("",""),null,batch.header,parser::findAccountDetails).getCreditorAccount().getClass());
        }
    }

    @Test
    @DisplayName("SPPA file parser throws an exception when the file has no header")
    void throwsExceptionWhenHeaderIsNotProvided() throws IOException, FileParseException {
        var path = getClass().getClassLoader().getResource("sppa_examples/SPPA_single_batch_with_empty_header.txt").getFile();
        var fileReader = new FileInputStream(path);
        SPPAFileParser parser = new SPPAFileParser(INTERNATIONAL_ACCOUNT,INTERNATIONAL_EMPLOYEE_ACCOUNTS);
        FileParseException e = assertThrows(FileParseException.class, () -> parser.generatePaymentBatches(fileReader));
        assertEquals("Invalid empty row at line 6", e.getMessage());
    }

    @Test
    @DisplayName("SPPA file parser throws an exception when the file contains an empty row")
    void throwsExceptionWhenRowIsEmpty() throws IOException, FileParseException {
        var path = getClass().getClassLoader().getResource("sppa_examples/SPPA_single_batch_with_empty_row.txt").getFile();
        var fileReader = new FileInputStream(path);
        SPPAFileParser parser = new SPPAFileParser(INTERNATIONAL_ACCOUNT,INTERNATIONAL_EMPLOYEE_ACCOUNTS);
        FileParseException e = assertThrows(FileParseException.class, () -> parser.generatePaymentBatches(fileReader));
        assertEquals("Invalid empty row at line 12", e.getMessage());
    }

    @Test
    @DisplayName("SPPA file parser throws an exception when a file has an invalid row")
    void throwsExceptionWhenRowInvalid() throws IOException, FileParseException {
        var path = getClass().getClassLoader().getResource("sppa_examples/SPPA_single_batch_with_error_row.txt").getFile();
        var fileReader = new FileInputStream(path);
        SPPAFileParser parser = new SPPAFileParser(INTERNATIONAL_ACCOUNT,INTERNATIONAL_EMPLOYEE_ACCOUNTS);
        FileParseException e = assertThrows(FileParseException.class, () -> parser.generatePaymentBatches(fileReader));
        assertEquals("Invalid field 'Account Name' at line 12", e.getMessage());
    }

}
