package gov.scot.payments.adapters;

import org.springframework.core.io.ClassPathResource;
import org.springframework.http.client.MultipartBodyBuilder;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

public class MultiPartFileBodyBuilder {

    static MultipartBodyBuilder getBuilder(String inputFilePath) throws IOException {
        var bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("file", new ClassPathResource(inputFilePath)
                .getInputStream()
                .readAllBytes())
                .header("Content-Disposition", "form-data; name=file; filename=file");
        return bodyBuilder;
    }
}
