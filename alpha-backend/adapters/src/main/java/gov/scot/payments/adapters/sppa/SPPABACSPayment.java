package gov.scot.payments.adapters.sppa;

import gov.scot.payments.adapters.FileParseException;
import gov.scot.payments.adapters.InvalidCSVFieldException;
import gov.scot.payments.adapters.RollNumber;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import gov.scot.payments.payments.model.api.CreatePaymentRequest;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import org.apache.avro.reflect.Nullable;
import org.javamoney.moneta.Money;

import javax.money.Monetary;
import java.math.BigDecimal;
import java.time.ZoneOffset;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Builder
@Value
public class SPPABACSPayment {

    @NonNull String employeeNumber;
    @NonNull UKAccountNumber employeeAccountNumber;
    @NonNull String accountName;
    @NonNull BigDecimal amount;
    @NonNull SortCode sortCode;
    @Nullable RollNumber buildingSocietyRollNumber;

    private final static int EMPLOYEE_NUMBER_INDEX = 1;
    private final static int EMPLOYEE_ACCOUNT_NUMBER_INDEX = 3;
    private final static int ACCOUNT_NAME_INDEX = 5;
    private final static int AMOUNT_INDEX = 7;
    private final static int SORT_CODE_INDEX = 9;
    private final static int ROLL_NUMBER_INDEX = 15;

    private static final Pattern pattern = Pattern.compile(
            "(.{10})(.{1})(.{8})(.{4})(.{20})(.{2})(.{11})(.{2})(.{8})(.{3})(.{6})(.{2})(.{30})(.{2})(.{18})"
    );

    private static final int RECORD_LENGTH = 127;

    private static final Map<Integer, String> COLUMN_INDEX_NAME_MAP =
            Map.of(EMPLOYEE_NUMBER_INDEX, "Employee Number",
                    EMPLOYEE_ACCOUNT_NUMBER_INDEX, "Employee A/C No.",
                    ACCOUNT_NAME_INDEX, "Account Name",
                    AMOUNT_INDEX, "Amount",
                    SORT_CODE_INDEX, "Sort Code",
                    ROLL_NUMBER_INDEX, "Building Society Roll Number");

    private static String addFinalColumnPadding(String paymentString) {
        return String.format("%1$-" + RECORD_LENGTH + "s", paymentString);
    }

    private static String getColumnNameFromIndex(int index){
        if(COLUMN_INDEX_NAME_MAP.containsKey(index)){
            return COLUMN_INDEX_NAME_MAP.get(index);
        }
        return "";
    }

    static SPPABACSPayment fromString(String paymentString, long lineNum) throws InvalidCSVFieldException {

       paymentString = addFinalColumnPadding(paymentString);

       Matcher matcher = pattern.matcher(paymentString);
       if (!matcher.matches()) {
           String errorMessage = "File row has " + paymentString.length() + " characters. Must not be longer than " + RECORD_LENGTH;
           throw new FileParseException(errorMessage);
       }

       String employeeNumber = getFieldValueFromString(matcher.group(EMPLOYEE_NUMBER_INDEX), lineNum, EMPLOYEE_NUMBER_INDEX);
       UKAccountNumber account = createUKAccountNumberFromString(matcher.group(EMPLOYEE_ACCOUNT_NUMBER_INDEX), lineNum);
       String accountName = getFieldValueFromString(matcher.group(ACCOUNT_NAME_INDEX), lineNum, ACCOUNT_NAME_INDEX);
       BigDecimal amount = getAmountFromString(matcher.group(AMOUNT_INDEX), lineNum);
       SortCode sortCode = createSortCodeFromString(matcher.group(SORT_CODE_INDEX), lineNum);
       RollNumber rollNumber = createRollNumberFromString(matcher.group(ROLL_NUMBER_INDEX), lineNum);

       return SPPABACSPayment.builder()
               .employeeNumber(employeeNumber)
               .employeeAccountNumber(account)
               .accountName(accountName)
               .amount(amount)
               .sortCode(sortCode)
               .buildingSocietyRollNumber(rollNumber)
               .build();
    }

    private static String getFieldValueFromString(String field, long lineNum, int index) throws InvalidCSVFieldException {
        String fieldValue = field.trim();
        if(fieldValue.equals("")){
            throw new InvalidCSVFieldException(lineNum, getColumnNameFromIndex(index), "Field not found");
        }
        return fieldValue;
    }

    private static UKAccountNumber createUKAccountNumberFromString(String accountStr, long lineNum) throws InvalidCSVFieldException {
        accountStr = getFieldValueFromString(accountStr, lineNum, EMPLOYEE_ACCOUNT_NUMBER_INDEX);
        try {
            return UKAccountNumber.fromString(accountStr);
        } catch (IllegalArgumentException e) {
            throw new InvalidCSVFieldException(lineNum, getColumnNameFromIndex(EMPLOYEE_ACCOUNT_NUMBER_INDEX), e);
        }
    }

    private static BigDecimal getAmountFromString(String amountStr, long lineNum) throws InvalidCSVFieldException{
        amountStr = getFieldValueFromString(amountStr, lineNum, AMOUNT_INDEX);
        if(!checkNumericAndPositive(amountStr)){
            String errorMessage = "Value "+ amountStr + " not a valid positive number";
            throw new InvalidCSVFieldException(lineNum, getColumnNameFromIndex(AMOUNT_INDEX), errorMessage);
        }
        return new BigDecimal(amountStr);
    }

    private static SortCode createSortCodeFromString(String sortCodeStr, long lineNum) throws InvalidCSVFieldException {
        sortCodeStr = getFieldValueFromString(sortCodeStr, lineNum, SORT_CODE_INDEX).replace("-", "");
        try {
            return SortCode.fromString(sortCodeStr);
        } catch(InvalidPaymentFieldException e){
            throw new InvalidCSVFieldException(lineNum, getColumnNameFromIndex(SORT_CODE_INDEX), e);
        }
    }

    private static RollNumber createRollNumberFromString(String rollNoStr, long lineNum) throws InvalidCSVFieldException {
        rollNoStr = rollNoStr.trim();
        return rollNoStr.equals("") ? null : RollNumber.fromString(rollNoStr);
    }

    private static boolean checkNumericAndPositive(String amountStr) {
        try{
            var bd = new BigDecimal(amountStr);
            if(bd.compareTo(BigDecimal.ZERO) < 0){
                return false;
            }
        } catch (NumberFormatException e){
            return false;
        }
        return true;
    }

    public CreatePaymentRequest toPaymentInstruction(CompositeReference productReference, String fileName, SPPABACSPaymentHeader header) {
        return toPaymentInstruction(productReference, fileName, header, SPPABACSPayment::getAccountDetails);
    }

    public CreatePaymentRequest toPaymentInstruction(CompositeReference productReference, String fileName, SPPABACSPaymentHeader header, Function<SPPABACSPayment,CashAccount> accountMapper) {

        CashAccount account = accountMapper.apply(this);
        var creditor = PartyIdentification.builder()
                .name(accountName)
                .build();

        return CreatePaymentRequest.builder()
                .amount(Money.of(amount, Monetary.getCurrency("GBP")))
                .allowedMethods(account instanceof UKBankAccount ? null : io.vavr.collection.List.of(PaymentMethod.Sepa_DC))
                .earliestExecutionDate(header.getPayDate().atStartOfDay().toInstant(ZoneOffset.UTC))
                .latestExecutionDate(header.getPayDate().atStartOfDay().toInstant(ZoneOffset.UTC))
                .batchId(header.getGroup())
                .product(productReference)
                .creditorAccount(account)
                .creditor(creditor)
                .creditorMetadata(io.vavr.collection.List.of(MetadataField.clientRef(employeeNumber)))
                .build();
    }

    public static CashAccount getAccountDetails(SPPABACSPayment payment) {
        CashAccount account;
        if (payment.buildingSocietyRollNumber != null) {
            account = UKBuildingSocietyAccount.builder()
                    .sortCode(payment.sortCode)
                    .rollNumber(payment.buildingSocietyRollNumber.getValue())
                    .accountNumber(payment.employeeAccountNumber)
                    .currency(Monetary.getCurrency("GBP").getCurrencyCode())
                    .name("account")
                    .build();
        } else {
            account = UKBankAccount.builder()
                    .sortCode(payment.sortCode)
                    .accountNumber(payment.employeeAccountNumber)
                    .currency(Monetary.getCurrency("GBP").getCurrencyCode())
                    .name("account")
                    .build();
        }
        return account;
    }
}
