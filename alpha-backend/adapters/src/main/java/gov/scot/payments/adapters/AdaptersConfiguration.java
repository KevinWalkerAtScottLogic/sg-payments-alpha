package gov.scot.payments.adapters;

import gov.scot.payments.adapters.ilf.ILFFileParser;
import gov.scot.payments.adapters.ilf_new.ILFNewFileParser;
import gov.scot.payments.adapters.sppa.SPPAFileParser;
import gov.scot.payments.adapters.sppa.SPPAProperties;
import gov.scot.payments.adapters.ssa.SSAFileParser;
import gov.scot.payments.model.CompositeReference;
import gov.scot.payments.payments.model.aggregate.*;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import io.vavr.jackson.datatype.VavrModule;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

@Configuration
@EnableConfigurationProperties(SPPAProperties.class)
public class AdaptersConfiguration {

    @Bean
    public ILFFileParser ilfFileParser(){
        return new ILFFileParser();
    }

    @Bean
    public ILFNewFileParser ilfNewFileParser() {
        return new ILFNewFileParser();
    }

    @Bean
    SSAFileParser ssaFileParser() {
        return new SSAFileParser();
    }

    @Bean
    SPPAFileParser sppaFileParser(SPPAProperties properties) {
        UKBankAccount account = UKBankAccount.builder()
                .accountNumber(UKAccountNumber.fromString(properties.getAccountNumber()))
                .sortCode(SortCode.fromString(properties.getSortCode()))
                .currency("GBP")
                .name("account")
                .build();
        Map<String, CashAccount> accounts = HashMap.ofAll(properties.getEmployeeAccounts())
                .mapValues(s -> SepaBankAccount.builder()
                        .iban(s)
                        .currency("EUR")
                        .name("account")
                        .build());
        return new SPPAFileParser(account,accounts);
    }

    @Bean
    public Converter<String, CompositeReference> compositeReferenceStringConverter(){
        return new Converter<String, CompositeReference>() {
            @Override
            public CompositeReference convert(String source) {
                return CompositeReference.parse(source);
            }
        };
    }
    @Bean
    public VavrModule vavrModule(){
        return new VavrModule();
    }

}
