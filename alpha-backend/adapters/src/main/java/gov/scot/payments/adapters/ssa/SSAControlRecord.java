package gov.scot.payments.adapters.ssa;

import gov.scot.payments.adapters.FileParseException;
import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import static java.time.format.DateTimeFormatter.BASIC_ISO_DATE;

/**
 * Represents a CPS control record.
 */
@Value
@Builder
public class SSAControlRecord {

    private static final int ENTITLEMENT_MANAGEMENT_SYSTEM_FIELD = 1;
    private static final int SERVICE_IDENTIFIER_FIELD = 2;
    private static final int INTERFACE_REFERENCE_FIELD = 3;
    private static final int DATE_OF_FILE_FIELD = 4;
    private static final int TOTAL_NUMBER_OF_RECORDS_FIELD = 5;
    private static final int HASH_GROSS_AMOUNT_FIELD = 6;
    private static final int HASH_NET_AMOUNT_FIELD = 9;

    private long lineNum;
    private String entitlementManagementSystem;
    private String serviceIdentifier;
    private String interfaceReference;
    private LocalDate dateOfFile;
    private long totalNumberOfRecords;
    private BigDecimal hashGrossAmount;
    private BigDecimal hashNetAmount;

    /**
     * Parses an array of fields (as strings) representing a CPS control record.
     *
     * @param fields the fields to parse into the record.
     * @param lineNum the line number of the line being parsed
     * @return the CPS control record matching the fields
     * @throws FileParseException if the record fields were invalid in some way
     */
    static SSAControlRecord fromFields(String[] fields, long lineNum) throws FileParseException, RecordParseException {

        LocalDate dateOfFile;
        try {
            dateOfFile = LocalDate.parse(fields[DATE_OF_FILE_FIELD], BASIC_ISO_DATE);
        } catch (DateTimeParseException cause) {
            throw new RecordParseException(lineNum, "date of file", cause);
        }

        long totalNumberOfRecords;
        try {
            totalNumberOfRecords = Long.parseLong(fields[TOTAL_NUMBER_OF_RECORDS_FIELD]);
        } catch (NumberFormatException cause) {
            throw new RecordParseException(lineNum, "total number of records", cause);
        }

        BigDecimal hashGrossAmount;
        try {
            hashGrossAmount = new BigDecimal(new BigInteger(fields[HASH_GROSS_AMOUNT_FIELD]), 2);
        } catch (NumberFormatException cause) {
            throw new RecordParseException(lineNum, "hash gross amount", cause);
        }

        BigDecimal hashNetAmount;
        try {
            hashNetAmount = new BigDecimal(new BigInteger(fields[HASH_NET_AMOUNT_FIELD]), 2);
        } catch (NumberFormatException cause) {
            throw new RecordParseException(lineNum, "hash net amount", cause);
        }

        var record = SSAControlRecord
                .builder()
                .lineNum(lineNum)
                .entitlementManagementSystem(fields[ENTITLEMENT_MANAGEMENT_SYSTEM_FIELD])
                .serviceIdentifier(fields[SERVICE_IDENTIFIER_FIELD])
                .interfaceReference(fields[INTERFACE_REFERENCE_FIELD])
                .dateOfFile(dateOfFile)
                .totalNumberOfRecords(totalNumberOfRecords)
                .hashGrossAmount(hashGrossAmount)
                .hashNetAmount(hashNetAmount)
                .build();

        if (record.getInterfaceReference().isBlank()) {
            throw new FileParseException("Line " + lineNum + ": Missing Interface Reference");
        }

        return record;
    }
}
