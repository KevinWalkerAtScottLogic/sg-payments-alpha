package gov.scot.payments.adapters.sppa;

import gov.scot.payments.adapters.FileParseException;
import gov.scot.payments.adapters.InvalidCSVFieldException;
import gov.scot.payments.adapters.ssa.SSAClientPaymentRecord;
import gov.scot.payments.adapters.ssa.SSAControlRecord;

import java.io.*;
import java.util.ArrayList;

import gov.scot.payments.payments.model.aggregate.CashAccount;
import gov.scot.payments.payments.model.aggregate.SepaBankAccount;
import gov.scot.payments.payments.model.aggregate.UKBankAccount;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.money.Monetary;

@RequiredArgsConstructor
public class SPPAFileParser {

    @NonNull private final UKBankAccount internationalPaymentAccount;
    @NonNull private final Map<String,CashAccount> internationalAccountLookup;

     private void validateRowContainsData(String row, long lineNum) {

        if (row.isEmpty()) {
            throw new FileParseException("Invalid empty row at line " + lineNum);
        }
     }

     public SPPAPaymentBatch generatePaymentBatches(InputStream paymentFileStream) {

         SPPAPaymentBatch batch = new SPPAPaymentBatch();

         long lineNum = 0;
         String line;

         var reader = new BufferedReader(new InputStreamReader(paymentFileStream));

         try {
             while(lineNum < 12) {
                 lineNum++;
                 line = reader.readLine();
                 if (lineNum == 6) {
                     validateRowContainsData(line, lineNum);
                     batch.header = SPPABACSPaymentHeader.fromString(line, lineNum);
                 }
             }
         } catch (InvalidCSVFieldException ie) {
             throw new FileParseException("Invalid field '" + ie.getFieldName() + "' at line " + lineNum, ie.getCause());
         } catch (IOException e ) {
             throw new UncheckedIOException(e);
         }

         try {
             while ((line = reader.readLine()) != null) {
                 validateRowContainsData(line, lineNum);
                 batch.payments = batch.payments.append(SPPABACSPayment.fromString(line, lineNum));
             }
         } catch (InvalidCSVFieldException ie) {
             throw new FileParseException("Invalid field '" + ie.getFieldName() + "' at line " + lineNum, ie.getCause());
         } catch (IOException e ) {
             throw new UncheckedIOException(e);
         }

         return batch;
     }

    public CashAccount findAccountDetails(SPPABACSPayment payment) {
        if(internationalPaymentAccount.getAccountNumber().equals(payment.getEmployeeAccountNumber()) && internationalPaymentAccount.getSortCode().equals(payment.getSortCode())){
            return internationalAccountLookup.getOrElse(payment.getEmployeeNumber(),internationalPaymentAccount);
        } else{
            return SPPABACSPayment.getAccountDetails(payment);
        }
    }

    public static class SPPAPaymentBatch {

         public SPPABACSPaymentHeader header;

         public List<SPPABACSPayment> payments = List.empty();
     }
}
