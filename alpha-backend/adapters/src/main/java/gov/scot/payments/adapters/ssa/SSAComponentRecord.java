package gov.scot.payments.adapters.ssa;

import gov.scot.payments.adapters.FileParseException;
import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import static java.time.format.DateTimeFormatter.BASIC_ISO_DATE;

/**
 * Represents a CPS component record.
 */
@Value
@Builder
class SSAComponentRecord {

    private static final int ENTITLEMENT_MANAGEMENT_SYSTEM_FIELD = 1;
    private static final int SERVICE_IDENTIFIER_FIELD = 2;
    private static final int EMS_PAYMENT_REF_NO_FIELD = 3;
    private static final int EXECUTIVE_AGENCY_FIELD = 4;
    private static final int ISSUING_OFFICE_FIELD = 5;
    private static final int BENEFIT_CODE_FIELD = 6;
    private static final int BENEFIT_CODE_START_DATE_FIELD = 9;
    private static final int BENEFIT_CODE_END_DATE_FIELD = 10;
    private static final int COMPONENT_AMOUNT_FIELD = 11;

    private long lineNum;
    private String entitlementManagementSystem;
    private String serviceIdentifier;
    private String emsPaymentRefNo;
    private String executiveAgency;
    private String issuingOffice;
    private String benefitCode;
    private LocalDate benefitCodeStartDate;
    private LocalDate benefitCodeEndDate;
    private BigDecimal componentAmount;

    /**
     * Parses an array of fields (as strings) representing a CPS component record.
     *
     * @param fields the fields to parse into the record.
     * @param lineNum the line number of the line being parsed
     * @return the CPS component record matching the fields
     * @throws FileParseException if the record fields were invalid in some way
     */
    static SSAComponentRecord fromFields(String[] fields, long lineNum) throws FileParseException, RecordParseException {

        LocalDate benefitCodeStartDate;
        try {
            benefitCodeStartDate = LocalDate.parse(fields[BENEFIT_CODE_START_DATE_FIELD], BASIC_ISO_DATE);
        } catch (DateTimeParseException cause) {
            throw new RecordParseException(lineNum, "benefit code start date", cause);
        }

        LocalDate benefitCodeEndDate;
        try {
            benefitCodeEndDate = LocalDate.parse(fields[BENEFIT_CODE_END_DATE_FIELD], BASIC_ISO_DATE);
        } catch (DateTimeParseException cause) {
            throw new RecordParseException(lineNum, "benefit code end date", cause);
        }

        BigDecimal componentAmount;
        try {
            componentAmount = new BigDecimal(new BigInteger(fields[COMPONENT_AMOUNT_FIELD]), 2);
        } catch (NumberFormatException cause) {
            throw new RecordParseException(lineNum, "component amount", cause);
        }

        var record = SSAComponentRecord
                .builder()
                .lineNum(lineNum)
                .entitlementManagementSystem(fields[ENTITLEMENT_MANAGEMENT_SYSTEM_FIELD])
                .serviceIdentifier(fields[SERVICE_IDENTIFIER_FIELD])
                .emsPaymentRefNo(fields[EMS_PAYMENT_REF_NO_FIELD])
                .executiveAgency(fields[EXECUTIVE_AGENCY_FIELD])
                .issuingOffice(fields[ISSUING_OFFICE_FIELD])
                .benefitCode(fields[BENEFIT_CODE_FIELD])
                .benefitCodeStartDate(benefitCodeStartDate)
                .benefitCodeEndDate(benefitCodeEndDate)
                .componentAmount(componentAmount)
                .build();

        if (record.getEmsPaymentRefNo().isBlank()) {
            throw new FileParseException("Line " + lineNum + ": Missing EMS Payment Reference No");
        }

        return record;
    }
}
