package gov.scot.payments.adapters.sppa;

import gov.scot.payments.adapters.FileParseException;
import gov.scot.payments.adapters.InvalidCSVFieldException;
import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Value
@Builder
public class SPPABACSPaymentHeader {

    @NotNull String group;
    @NotNull LocalDate payDate;

    private static final Pattern pattern = Pattern.compile(
            // GROUP
            "(.{11})(.{1})(.{1})(.{46})" +
                    // PAY DATE
                    "(.{8})(.{5})(.{1})(.{1})(.{10})" +
                    // PROCESSING DATE
                    "(.{17})(.{15})(.{1})(.{1})(.{1})(.{10})"
    );

    private static final int HEADER_LENGTH = 129;

    private static final int GROUP_INDEX = 4;
    private static final int PAY_DATE_INDEX = 9;

    private static final Map<Integer, String> COLUMN_INDEX_NAME_MAP =
            Map.of(GROUP_INDEX, "Group",
                    PAY_DATE_INDEX, "Pay Date");

    private static String getColumnNameFromIndex(int index){
        if(COLUMN_INDEX_NAME_MAP.containsKey(index)){
            return COLUMN_INDEX_NAME_MAP.get(index);
        }
        return "";
    }

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public static SPPABACSPaymentHeader fromString(String headerString, long lineNum) throws InvalidCSVFieldException {

        Matcher matcher = pattern.matcher(headerString);
        if (!matcher.matches()) {
            String errorMessage = "Header row " + lineNum + " has " + headerString.length() + " characters. Must be " + HEADER_LENGTH;
            throw new FileParseException(errorMessage);
        }

        String group = createGroupFromString(matcher.group(GROUP_INDEX), lineNum);
        LocalDate payDate = createPayDatefromString(matcher.group(PAY_DATE_INDEX), lineNum);

        return SPPABACSPaymentHeader.builder()
                .group(group)
                .payDate(payDate)
                .build();
    }

    private static String createGroupFromString (String groupStr, long lineNum) throws InvalidCSVFieldException {
        groupStr = groupStr.trim().replaceAll(" +", " ");
        if (groupStr.equals("")) {
            throw new InvalidCSVFieldException(lineNum, getColumnNameFromIndex(GROUP_INDEX), "Field not found");
        }
        return  groupStr;
    }

    private static LocalDate createPayDatefromString(String dateStr, long lineNum) throws InvalidCSVFieldException {
        dateStr = dateStr.trim();
        if (dateStr.equals("")) {
            throw new InvalidCSVFieldException(lineNum, getColumnNameFromIndex(PAY_DATE_INDEX), "Field not found");
        }

        try {
            return LocalDate.parse(dateStr, DATE_FORMATTER);
        } catch (DateTimeParseException cause) {
            throw new InvalidCSVFieldException(lineNum, getColumnNameFromIndex(PAY_DATE_INDEX), cause);
        }
    }

}
