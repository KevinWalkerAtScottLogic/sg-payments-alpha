package gov.scot.payments.adapters;

import gov.scot.payments.adapters.ilf.ILFFileParser;
import gov.scot.payments.adapters.ilf_new.ILFNewFileParser;
import gov.scot.payments.adapters.sppa.SPPAFileParser;
import gov.scot.payments.adapters.ssa.SSAFileParser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AdaptersApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdaptersApplication.class);
    }

    @Bean
    public AdaptersService adaptersService(
            ILFFileParser ilfFileParser,
            ILFNewFileParser ilfNewFileParser,
            SSAFileParser ssaFileParser,
            SPPAFileParser sppaFileParser) {
        return new AdaptersService(ilfFileParser, ilfNewFileParser, ssaFileParser, sppaFileParser);
    }
}