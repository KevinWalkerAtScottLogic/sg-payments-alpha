package gov.scot.payments.adapters.ssa;

import gov.scot.payments.adapters.FileParseException;
import io.vavr.collection.LinkedHashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;

/**
 * Parses CPS records
 */
public class SSAFileParser {

    static final String FIELD_SEPARATOR = "\\|";

    /**
     * Reads records from the {@code InputStream}, links component records to
     * client payment records and validates the set of records.
     *
     * @param inputStream the stream of lines to read records from
     * @return he collection of linked and validated records
     * @throws FileParseException if the input data is invalid
     */
    public Records readRecords(InputStream inputStream) throws FileParseException {
        var records = parseRecords(inputStream);
        records = linkRecords(records);
        records = validateRecords(records);

        return records;
    }

    /**
     * Parses lines from the {@code inputStream}, creating the appropriate type
     * of record for each line and adding it to the returned {@link Records} structure.
     *
     * @param inputStream the stream of lines to parse
     * @return the collection of parsed records
     * @throws FileParseException if the input data is invalid
     */
    Records parseRecords(InputStream inputStream) throws FileParseException {

        Records records = new Records();

        long lineNum = 0;

        try (var reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = reader.readLine()) != null) {
                lineNum++;
                if (!line.isBlank()) {
                    parseRecord(line, records, lineNum);
                }
            }

        } catch (IOException ex) {
            throw new FileParseException("Failed to read line " + lineNum, ex);
        } catch (RecordParseException ex) {
            throw new FileParseException("Invalid field '" + ex.getFieldName() + "' at line " + ex.getLineNum(), ex.getCause());
        }

        return records;
    }

    /**
     * Parses a single line into a record and adds it to the {@code Records}
     * structure.
     * <p>
     * Note that the input {@code Records} collection is updated.
     *
     * @param line    the line to parse
     * @param records the collection of parsed records
     * @param lineNum the line number of the line being parsed
     * @throws FileParseException if the input data is invalid
     */
    void parseRecord(String line, Records records, long lineNum) throws FileParseException, RecordParseException {

        var fields = line.split(FIELD_SEPARATOR);
        switch (fields[0]) {
            case "CTL":
                records.controlRecords = records.controlRecords.append(SSAControlRecord.fromFields(fields, lineNum));
                break;
            case "CLN":
                var record = SSAClientPaymentRecord.fromFields(fields, lineNum);
                records.clientPaymentRecords = records.clientPaymentRecords.put(record.getEmsPaymentRefNo(), record);
                break;
            case "CMP":
                records.componentRecords = records.componentRecords.append(SSAComponentRecord.fromFields(fields, lineNum));
                break;
            case "TPP":
                throw new FileParseException("Line " + lineNum + " : TPP (third party) records are not supported");
            case "RVY":
                throw new FileParseException("Line " + lineNum + " : RVY (recovery) records are not supported");
            default:
                throw new FileParseException("Line " + lineNum + " : Ignoring unknown record of type " + fields[0]);
        }
    }

    /**
     * Links {@link SSAComponentRecord}s to their containing
     * {@link SSAClientPaymentRecord}s.
     * <p>
     * Note that the input {@code Records} collection is updated.
     *
     * @param records the collection of parsed records
     * @return the updated collection of parsed records
     * @throws FileParseException if the input data is invalid
     */
    Records linkRecords(Records records) throws FileParseException {

        for (var component : records.componentRecords) {
            var clientPayment = records.clientPaymentRecords.get(component.getEmsPaymentRefNo());
            if (clientPayment.isEmpty()) {
                String msg = "Line " + component.getLineNum() +  ": No match for payment reference: " + component.getEmsPaymentRefNo();
                throw new FileParseException(msg);
            }
            SSAClientPaymentRecord ssaClientPaymentRecord = clientPayment.get();
            ssaClientPaymentRecord.addComponents(component);
        }

        return records;
    }

    /**
     * Validates the collection of parsed records.
     *
     * @param records the collection of parsed records to validate
     * @return the collection of parsed records
     * @throws FileParseException if the input data is invalid
     */
    Records validateRecords(Records records) throws FileParseException {

        if (records.controlRecords.size() == 0) {
            throw new FileParseException("No CTL (control) record");
        }

        if (records.controlRecords.size() > 1) {
            throw new FileParseException("Multiple CTL (control) records");
        }

        var paymentAmountTotal = BigDecimal.ZERO;

        for (var paymentRecord : records.clientPaymentRecords.values()) {
            paymentRecord.checkComponentAmountTotal();
            paymentAmountTotal = paymentAmountTotal.add(paymentRecord.getNetAmount());
        }

        var hashNetAmount = records.controlRecords.get(0).getHashNetAmount();
        if (!paymentAmountTotal.equals(hashNetAmount)) {
            throw new FileParseException(String.format(
                    "Hash Net Amount does not equal total of Payment Amounts  (Hash Net Amount=%.2f, Payment Total=%.2f)",
                    hashNetAmount,
                    paymentAmountTotal
                    )
            );
        }

        return records;
    }

    /**
     * A collection of parsed records.
     */
    public static class Records {

        /**
         * A list of the parsed control records (there should only be one).
         */
        public List<SSAControlRecord> controlRecords = List.empty();

        /**
         * A collection of parsed client payment records, indexed by the
         * {@code emsPaymentRefNo}.
         */
        public Map<String, SSAClientPaymentRecord> clientPaymentRecords = LinkedHashMap.empty();

        /**
         * A list of the parsed component records.
         */
        List<SSAComponentRecord> componentRecords = List.empty();
    }
}
