package gov.scot.payments.adapters.ilf_new;

import gov.scot.payments.adapters.FileParseException;
import gov.scot.payments.adapters.InvalidCSVFieldException;
import io.vavr.collection.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;

public class ILFNewFileParser {

    public final static CSVFormat ILF_CSV_FORMAT = CSVFormat.RFC4180
            .withTrim()
            .withNullString("")
            .withIgnoreEmptyLines(true);

    private final static int COLUMN_COUNT = 11;

    private boolean isEmptyCSVRecord(CSVRecord record) {

        for (String value : record) {
            if (value != null) {
                return false;
            }
        }
        return true;
    }

    private void checkNumberOfColumns(CSVRecord record) {

        if (record.size() != COLUMN_COUNT) {

            String errorMessage = "Row " + record.getRecordNumber() + " has " + record.size() + " columns. " +
                    "Must be " + COLUMN_COUNT;
            throw new FileParseException(errorMessage);
        }
    }

    public List<ILFNewBACSPayment> generatePaymentList(InputStream paymentFileStream) {

        var reader = new InputStreamReader(paymentFileStream);
        List<ILFNewBACSPayment> parsed = List.empty();

        try (var csvParser = new CSVParser(reader, ILF_CSV_FORMAT)) {
            for (CSVRecord csvRecord : csvParser) {
                if (isEmptyCSVRecord(csvRecord)) {
                    continue;
                }
                checkNumberOfColumns(csvRecord);
                try {
                    parsed = parsed.append(ILFNewBACSPayment.fromCSVRecord(csvRecord));
                } catch (InvalidCSVFieldException e){
                    throw new FileParseException(e.getMessage(),e);
                }
            }
        } catch(IOException e){
            throw new UncheckedIOException(e);
        }
        return parsed;
    }

}
