import { expect, request, use } from "chai";
import chaiHttp = require("chai-http");
import "mocha";

use(chaiHttp);

const BASE_URL: string = process.env.CI_BACKEND_URL || "http://localhost";
const USERS_CH: string = "/alpha/users/users-ch-app";

describe("User Context", () => {
  const ACCESS_TOKEN: string = process.env.ID_TOKEN ? process.env.ID_TOKEN : "";

  it("can list users", async () => {
    await request(BASE_URL + USERS_CH)
      .get("/details")
      .set("authorization", `Bearer ${ACCESS_TOKEN}`)
      .then((res) => {
        expect(res.status).to.be.equal(200);
        expect(res.body.name).to.be.equal("ci_user");
      });
  });
});
