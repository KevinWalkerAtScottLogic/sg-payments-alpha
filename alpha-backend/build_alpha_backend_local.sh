#!/bin/sh

set -eo pipefail

rm application/build/helm/output/sg-payments-alpha/requirements.yaml || true

./gradlew build -x test -PexcludeKafkaTests
./gradlew jibBuildTar

echo "Loading docker images"
for image in application/build/images/*.tar; do
  echo "Loading docker image for ${image}"
  docker load --input ${image}
done

echo "Loading docker image for adapters service"
docker load --input adapters/build/jib-image.tar

echo "Loading docker image for transferwise service"
docker load --input psps/transferwise/build/jib-image.tar
