package gov.scott.payment.sgmockservice.web;

import gov.scott.payment.sgmockservice.service.PaymentManager;
import gov.scott.payment.sgmockservice.type.FishingPermit;
import gov.scott.payment.sgmockservice.type.PaymentId;
import gov.scott.payment.sgmockservice.type.User;
import io.vavr.Tuple2;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.net.URI;
import java.security.Principal;

import static gov.scott.payment.sgmockservice.web.Utils.redirect;
import static gov.scott.payment.sgmockservice.web.Utils.response;

@Controller
@SessionAttributes(names = {"paymentId"})
public class FishingController {

    @Autowired
    @Qualifier("FishingPaymentManager")
    private PaymentManager paymentManager;

    @GetMapping("/fishing/buy")
    public String getFishingBuy(Model model){
        model.addAttribute("permits", FishingPermit.AllPermits);
        return "fishing-buy";
    }

    @PostMapping(path = "/fishing/buy", consumes = "application/x-www-form-urlencoded")
    public String postFishingBuy(Principal principal,
                                 @RequestParam String license,
                                 Model model) {

        val user = User.of(principal.getName());
        val items = FishingPermit.AllPermits
                .filter(p -> p.id().value().equals(license));

        val payment = paymentManager.newPayment(user, items, returnUrl())
                .map(p -> {
                   model.addAttribute("paymentId", p._1.id());
                   return p;
                });

        return redirect(payment.map(Tuple2::_2));
    }

    @GetMapping("/fishing/payment-return")
    public String getPaymentReturn(@ModelAttribute PaymentId paymentId, Model model) {
        val payment = paymentManager.proceedPayment(paymentId);

        val purchasedItem = payment.map(p -> p.items().headOption().flatMap(FishingPermit::findById));
        purchasedItem.map(item -> item.map(x -> {
            model.addAttribute("purchase", x);
            return x;
        }));


        return response(
                payment.map(p -> p.succeeded()
                        ? "fishing-payment-succeeded"
                        : "payment-failed")
        );
    }

    private static URI returnUrl() {
        return MvcUriComponentsBuilder
                .fromMethodName(FishingController.class, "getPaymentReturn", null, null)
                .build().toUri();
    }
}
