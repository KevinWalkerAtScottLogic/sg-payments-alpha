package gov.scott.payment.sgmockservice.web;

import gov.scott.payment.sgmockservice.type.PaymentError;
import io.vavr.control.Either;
import lombok.extern.java.Log;

import java.net.URI;

@Log
class Utils {

    static String redirectResult(URI uri) {
        return redirectResult(uri.toString());
    }

    static String redirectResult(String url) {
        return "redirect:" + url;
    }

    static String redirect(Either<PaymentError, URI> result) {
        return response(result.map(Utils::redirectResult));
    }

    static String redirectUri(Either<PaymentError, String> result) {
        return response(result.map(Utils::redirectResult));
    }

    static String response(Either<PaymentError, String> result) {
        return result
                .mapLeft(e -> {
                    log.warning(e.toString());
                    return e;
                })
                .getOrElseGet(Utils::errorToView);
    }

    private static String errorToView(PaymentError error) {
        if (error.code() == 404) {
            return "error/404";
        }

        return "error/500";
    }
}
