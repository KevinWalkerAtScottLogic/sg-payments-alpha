package gov.scott.payment.sgmockservice.web;

import gov.scott.payment.sgmockservice.service.DummyPaymentService;
import gov.scott.payment.sgmockservice.service.dto.GetPaymentResult;
import gov.scott.payment.sgmockservice.service.dto.MoneyDto;
import gov.scott.payment.sgmockservice.type.PaymentError;
import gov.scott.payment.sgmockservice.type.PaymentStatus;
import gov.scott.payment.sgmockservice.web.dto.PaymentDetailsRequest;
import lombok.val;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import static gov.scott.payment.sgmockservice.web.Utils.redirectUri;
import static gov.scott.payment.sgmockservice.web.Utils.response;

@Controller
@Profile("dev")
public class PaymentController {

    @Autowired
    private DummyPaymentService dummyPaymentService;

    @GetMapping("/payment/{paymentId}")
    public String getPayment(@PathVariable String paymentId, Model model) {
        val payment = dummyPaymentService.getPayment(paymentId);

        return response(
                payment.map(p -> {
                    model.addAttribute("payment", p);
                    model.addAttribute("amount", dtoToMoney(p.getAmount()));

                    return "payment";
                })
        );
    }

    @PostMapping(path = "/payment/{paymentId}", consumes = "application/x-www-form-urlencoded")
    public String postPayment(@PathVariable String paymentId, PaymentDetailsRequest detailsRequest) {

        PaymentStatus newStatus;
        switch (detailsRequest.getCvCode()) {
            case "777":
                newStatus = PaymentStatus.Failed;
                break;
            case "888":
                newStatus = PaymentStatus.Cancelled;
                break;
            case "999":
                newStatus = PaymentStatus.Error;
                break;
            case "222":
                newStatus = PaymentStatus.Submitted;
                break;
            case "111":
                newStatus = PaymentStatus.Started;
                break;
            default:
                newStatus = PaymentStatus.Success;
        }

        val result = dummyPaymentService.updatePayment(paymentId, newStatus);
        return redirectUri(
                result.map(GetPaymentResult::getReturnUrl).toEither(PaymentError.NotFound)
        );
    }

    private static Money dtoToMoney(MoneyDto dto) {
        return Money.ofMinor(CurrencyUnit.of(dto.getCurrency()), Integer.parseInt(dto.getAmount()));
    }
}
