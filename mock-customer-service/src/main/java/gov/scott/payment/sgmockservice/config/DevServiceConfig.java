package gov.scott.payment.sgmockservice.config;

import gov.scott.payment.sgmockservice.dao.PaymentRepository;
import gov.scott.payment.sgmockservice.service.DefaultPaymentManager;
import gov.scott.payment.sgmockservice.service.DummyPaymentService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("dev")
public class DevServiceConfig {

    @Bean({"FishingPaymentManager", "ParkingPaymentManager", "ParkingPaymentSubject"})
    public DefaultPaymentManager paymentManager(PaymentRepository repository, DummyPaymentService paymentService) {
        return new DefaultPaymentManager(repository, paymentService);
    }
}
