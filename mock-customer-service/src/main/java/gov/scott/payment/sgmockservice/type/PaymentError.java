package gov.scott.payment.sgmockservice.type;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;

@Value
@Accessors(fluent = true)
@RequiredArgsConstructor(staticName = "of")
public class PaymentError {

    private @NonNull Integer code;

    private @NonNull String description;

    public static final PaymentError NotFound = of(404, "Payment is not found.");

    public static final PaymentError invalidInput(String message) {
        return of(400, message);
    }

    public static final PaymentError serverError(String message) {
        return of(500, message);
    }
}
