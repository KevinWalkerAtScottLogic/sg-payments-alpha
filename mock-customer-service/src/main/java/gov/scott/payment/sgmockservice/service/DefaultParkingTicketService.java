package gov.scott.payment.sgmockservice.service;

import gov.scott.payment.sgmockservice.dao.ParkingTicketRepository;
import gov.scott.payment.sgmockservice.type.ItemId;
import gov.scott.payment.sgmockservice.type.ParkingTicket;
import gov.scott.payment.sgmockservice.type.Payment;
import gov.scott.payment.sgmockservice.type.User;
import io.vavr.collection.List;
import lombok.val;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DefaultParkingTicketService implements ParkingTicketService, PaymentObserver {

    private final ParkingTicketRepository repository;

    public DefaultParkingTicketService(
            ParkingTicketRepository repository,
            @Qualifier("ParkingPaymentSubject") PaymentSubject paymentSubject) {
        this.repository = repository;

        paymentSubject.addObserver(this);
    }

    @Override
    public List<ParkingTicket> getUnpaidParkingTickets(User user) {
        val entities = repository.findByUsernameAndPaymentId(user.username(), null);
        return List.ofAll(entities).map(e -> ParkingTicket.builder()
                .id(ItemId.of(e.getId()))
                .description(e.getDescription())
                .amount(Money.ofMinor(CurrencyUnit.GBP,e.getAmount()))
                .date(e.getDate())
                .user(user)
                .build()
            );
    }

    @Override
    public void update(Payment payment) {
        if (!payment.succeeded()) {
            return;
        }

        payment.items()
                .map(id -> repository.findById(id.value()))
                .filter(Optional::isPresent)
                .map(e -> e.get().withPaymentId(payment.id().value()))
                .forEach(repository::save);
    }
}
