package gov.scott.payment.sgmockservice.service.dto;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor(staticName = "of")
public class MoneyDto {

    private @NonNull String amount;

    private @NonNull String currency;

    public MoneyDto() {

    }
}
