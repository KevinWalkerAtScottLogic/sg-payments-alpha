package gov.scott.payment.sgmockservice.service;

import gov.scott.payment.sgmockservice.type.ItemId;
import io.vavr.collection.List;

import java.util.Arrays;

class Utils {

    static List<ItemId> referenceToItemIds(String reference) {
        return List.ofAll(Arrays.stream(reference.split(";")))
                .map(s -> ItemId.of(s.strip()));
    }

    static String itemIdsToReference(List<ItemId> itemIds) {
        return String.join(";", itemIds.map(ItemId::value));
    }
}
