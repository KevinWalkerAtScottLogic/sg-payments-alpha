package gov.scott.payment.sgmockservice.service;

public interface PaymentSubject {

    void addObserver(PaymentObserver observer);
}
