package gov.scott.payment.sgmockservice.type;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.experimental.Accessors;
import org.joda.money.Money;

import java.util.Date;

@Value
@Accessors(fluent = true)
@Builder
public class ParkingTicket implements Item {

    private @NonNull ItemId id;

    private @NonNull String description;

    private @NonNull Money amount;

    private @NonNull Date date;

    private @NonNull User user;
}
