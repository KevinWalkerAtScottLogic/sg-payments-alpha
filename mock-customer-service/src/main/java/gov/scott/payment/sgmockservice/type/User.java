package gov.scott.payment.sgmockservice.type;

import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.apache.logging.log4j.util.Strings;

@Value
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class User {

    private @NonNull String username;

    public static User of(String username) {
        if (Strings.isBlank(username)) {
            throw new IllegalArgumentException("username");
        }

        return new User(username);
    }
}
