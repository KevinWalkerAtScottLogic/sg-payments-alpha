package gov.scott.payment.sgmockservice.type;

import org.joda.money.Money;

public interface Item {

    ItemId id();

    String description();

    Money amount();
}
