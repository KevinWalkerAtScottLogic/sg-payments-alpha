package gov.scott.payment.sgmockservice.service;

import gov.scott.payment.sgmockservice.service.dto.CreatePaymentRequest;
import gov.scott.payment.sgmockservice.service.dto.CreatePaymentResult;
import gov.scott.payment.sgmockservice.service.dto.GetPaymentResult;
import gov.scott.payment.sgmockservice.type.PaymentError;
import io.vavr.control.Either;

public interface PaymentService {

    Either<PaymentError, CreatePaymentResult> newPayment(CreatePaymentRequest request);

    Either<PaymentError, GetPaymentResult> getPayment(String paymentId);
}
