package gov.scott.payment.sgmockservice.service;

import gov.scott.payment.sgmockservice.type.ParkingTicket;
import gov.scott.payment.sgmockservice.type.User;
import io.vavr.collection.List;

public interface ParkingTicketService {

    List<ParkingTicket> getUnpaidParkingTickets(User user);
}
