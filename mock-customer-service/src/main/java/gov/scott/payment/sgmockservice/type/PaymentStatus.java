package gov.scott.payment.sgmockservice.type;

public enum PaymentStatus {
    Created, Started, Submitted,
    Success, Failed, Cancelled, Error;

    public static PaymentStatus of(String value) {
        return Enum.valueOf(PaymentStatus.class, value);
    }
}
