package gov.scott.payment.sgmockservice.dao;

import gov.scott.payment.sgmockservice.dao.model.ParkingTicketEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ParkingTicketRepository extends CrudRepository<ParkingTicketEntity, String> {

    List<ParkingTicketEntity> findByUsername(String username);

    List<ParkingTicketEntity> findByUsernameAndPaymentId(String username, String paymentId);
}
