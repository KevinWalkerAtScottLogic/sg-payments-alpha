package gov.scott.payment.sgmockservice;

import com.beust.jcommander.JCommander;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.boot.web.context.WebServerPortFileWriter;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;

@SpringBootApplication
@Slf4j
public class SgMockServiceApplication extends SpringApplication {

	@Setter
	private MockCustomerSiteArguments config;

	public SgMockServiceApplication(){
		super(SgMockServiceApplication.class);
	}

	public static void main(String[] args) {
		MockCustomerSiteAwsArguments awsCommand = new MockCustomerSiteAwsArguments();
		MockCustomerSiteLocalArguments localCommand = new MockCustomerSiteLocalArguments();
		JCommander commander =JCommander.newBuilder()
				.addCommand("aws",awsCommand)
				.addCommand("local",localCommand)
				.build();

		HelpArgument helpConfig = new HelpArgument();
		JCommander.newBuilder()
				.addObject(helpConfig)
				.acceptUnknownOptions(true)
				.build()
				.parse(args);
		if(helpConfig.isHelp()){
			JCommander.newBuilder()
					.addObject(awsCommand)
					.build()
					.usage();
			System.exit(0);
		}

		commander.parse(args);
		String command = commander.getParsedCommand();
		SgMockServiceApplication application = new SgMockServiceApplication();
		if("aws".equals(command)){
			application.setConfig(awsCommand);
		} else {
			application.setConfig(localCommand);
		}
		application.run(args);
	}

	@Override
	protected void configurePropertySources(ConfigurableEnvironment environment, String[] args) {
		MutablePropertySources sources = environment.getPropertySources();
		sources.addFirst(new MapPropertySource("commandLineProperties", this.config.getProperties()));
	}

	@Override
	public ConfigurableApplicationContext run(String... args) {
		Set<String> violations = config.validate();
		if(!violations.isEmpty()){
			throw new IllegalArgumentException(String.join("\n",violations));
		}
		if(config.getPidFile() != null){
			addListeners(new ApplicationPidFileWriter(config.getPidFile()));
		}
		if(config.getPortFile() != null){
			addListeners(new WebServerPortFileWriter(config.getPortFile()));
		}
		Set<String> profiles = config.getProfiles();
		setAdditionalProfiles(profiles.toArray(new String[0]));
		return super.run(args);
	}

	@EventListener({WebServerInitializedEvent.class})
	public void onApplicationStarted(WebServerInitializedEvent event)  {
		String port = event.getApplicationContext().getEnvironment().getProperty("server.port");
		String openBrowserProp = event.getApplicationContext().getEnvironment().getProperty("browser.open");
		boolean openBrowser = openBrowserProp != null && Boolean.valueOf(openBrowserProp);
		if(openBrowser){
			try {
				URI homepage = new URI("http://localhost:"+port);
				Desktop.getDesktop().browse(homepage);
			} catch (Exception e) {
				log.warn("Could not open browser");
			}
		}
	}
}
