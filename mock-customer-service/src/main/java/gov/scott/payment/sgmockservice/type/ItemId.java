package gov.scott.payment.sgmockservice.type;

import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.apache.logging.log4j.util.Strings;

@Value
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ItemId {

    private @NonNull String value;

    public static ItemId of(String value) {
        if (Strings.isBlank(value)) {
            throw new IllegalArgumentException("value");
        }

        return new ItemId(value);
    }
}
