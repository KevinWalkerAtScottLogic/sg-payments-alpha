package gov.scott.payment.sgmockservice;

import com.beust.jcommander.Parameter;
import lombok.Data;

@Data
public class HelpArgument {

    @Parameter(names = "--help", help = true)
    private boolean help;
}
