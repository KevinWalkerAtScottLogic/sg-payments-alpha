package gov.scott.payment.sgmockservice.web;


import antlr.StringUtils;
import io.vavr.control.Option;
import lombok.val;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.servlet.http.HttpServletRequest;

import static io.vavr.control.Option.none;
import static io.vavr.control.Option.some;

@Controller
public class HomeController {

    private static final String FISHING_VIEW = "/fishing/buy";

    private static final String PARKING_VIEW = "/parking/buy";


    private final Option<String> fishingBaseUrl;

    private final Option<String> parkingBaseUrl;

    public HomeController(
            @Value("${sg.site.url.fishing:}") String fishingBaseUrlConfig,
            @Value("${sg.site.url.parking:}") String parkingBaseUrlConfig) {

        fishingBaseUrl = siteBaseUrl(fishingBaseUrlConfig);
        parkingBaseUrl = siteBaseUrl(parkingBaseUrlConfig);
    }

    @GetMapping("/")
    public String home(HttpServletRequest request,
                       @RequestHeader(name = "SG-SITE", defaultValue = "") String site) {

        val requestBaseUrl = ("." + request.getServerName()).toLowerCase();

        val headerView = headerToView(site);
        val fishingView = fishingBaseUrl.filter(requestBaseUrl::endsWith).map(x -> FISHING_VIEW);
        val parkingView = parkingBaseUrl.filter(requestBaseUrl::endsWith).map(x -> PARKING_VIEW);

        return headerView.orElse(fishingView).orElse(parkingView)
                .map(Utils::redirectResult)
                .getOrElse("home");
    }

    private static Option<String> siteBaseUrl(String config) {
        return Option.of(config).filter(Strings::isNotBlank)
                .map(s -> "." + StringUtils.stripFront(s, '.'));
    }

    private static Option<String> headerToView(String header) {
        if ("fishing".equalsIgnoreCase(header)) {
            return some(FISHING_VIEW);
        }

        if ("parking".equalsIgnoreCase(header)) {
            return some(PARKING_VIEW);
        }

        return none();
    }
}
