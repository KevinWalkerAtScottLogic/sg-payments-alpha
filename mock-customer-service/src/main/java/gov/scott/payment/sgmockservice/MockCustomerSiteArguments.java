package gov.scott.payment.sgmockservice;

import com.beust.jcommander.Parameter;
import lombok.Data;
import org.springframework.util.SocketUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Data
public abstract class MockCustomerSiteArguments {

    public abstract Set<String> getProfiles();

    public abstract Set<String> validate();

    @Parameter(names = "-fishing.service")
    private String fishingService = "fishing_permit";

    @Parameter(names = "-parking.service")
    private String parkingService = "parking_fine";

    @Parameter(names = "-port", description = "The port to launch the application on. By default a free port is chosen at random")
    private Integer port;

    @Parameter(names = "-browser", hidden = true)
    private boolean openBrowser;

    @Parameter(names = "-pidFile", hidden = true)
    private String pidFile;

    @Parameter(names = "-portFile", hidden = true)
    private String portFile;

    public Map<String, Object> getProperties(){
        if(port == null){
            port = SocketUtils.findAvailableTcpPort();
        }
        Map<String,Object> properties = new HashMap<>();
        properties.put("sg.config.fishing.service.id",fishingService);
        properties.put("sg.config.parking.service.id",parkingService);
        properties.put("server.port",this.port);
        properties.put("browser.open",openBrowser);
        return properties;
    }
}
