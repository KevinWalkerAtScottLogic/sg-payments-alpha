package gov.scott.payment.sgmockservice.dao.model;

import lombok.*;
import lombok.experimental.Wither;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "ParkingTickets")
@Getter
@Setter
@NoArgsConstructor
@Builder
public class ParkingTicketEntity {

    @Id
    private @NotNull String id;

    private @NotNull Integer amount;

    private @NotNull Date date;

    private @NotNull String description;

    private @NotNull String username;

    private @Wither String paymentId;

    private ParkingTicketEntity(String id, Integer amount, Date date, String description, String username,
                                String paymentId) {
        this.id = id;
        this.amount = amount;
        this.date = date;
        this.description = description;
        this.username = username;
        this.paymentId = paymentId;
    }
}
