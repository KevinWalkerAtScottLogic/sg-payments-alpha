package gov.scott.payment.sgmockservice.web;

import gov.scott.payment.sgmockservice.service.ParkingTicketService;
import gov.scott.payment.sgmockservice.service.PaymentManager;
import gov.scott.payment.sgmockservice.type.PaymentId;
import gov.scott.payment.sgmockservice.type.User;
import io.vavr.Tuple2;
import io.vavr.collection.HashSet;
import lombok.val;
import org.joda.money.format.MoneyFormatter;
import org.joda.money.format.MoneyFormatterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.net.URI;
import java.security.Principal;

import static gov.scott.payment.sgmockservice.web.Utils.redirect;
import static gov.scott.payment.sgmockservice.web.Utils.response;

@Controller
@SessionAttributes(names = {"paymentId"})
public class ParkingController {

    private static final MoneyFormatter MONEY_FORMATTER =
            new MoneyFormatterBuilder().appendCurrencySymbolLocalized().appendAmount().toFormatter();

    @Autowired
    private ParkingTicketService parkingTicketService;

    @Autowired
    @Qualifier("ParkingPaymentManager")
    private PaymentManager paymentManager;

    @GetMapping("/parking/buy")
    public String getParkingBuy(Principal principal, Model model) {

        val user = User.of(principal.getName());
        val tickets = parkingTicketService.getUnpaidParkingTickets(user);

        model.addAttribute("user", user);
        model.addAttribute("tickets", tickets);
        model.addAttribute("formatter", MONEY_FORMATTER);

        return "parking-buy";
    }

    @PostMapping(path = "/parking/buy", consumes = "application/x-www-form-urlencoded")
    public String postParkingBuy(Principal principal,
                                 @RequestParam java.util.List<String> ticket,
                                 Model model) {
        val input = HashSet.ofAll(ticket);

        val user = User.of(principal.getName());
        val items = parkingTicketService.getUnpaidParkingTickets(user)
                .filter(t -> input.contains(t.id().value()));

        val payment = paymentManager.newPayment(user, items, returnUrl())
                .map(p -> {
                    model.addAttribute("paymentId", p._1.id());
                    return p;
                });

        return redirect(payment.map(Tuple2::_2));
    }

    @GetMapping("/parking/payment-return")
    public String getPaymentReturn(@ModelAttribute PaymentId paymentId, Model model) {
        val payment = paymentManager.proceedPayment(paymentId)
                .map(p -> {
                   if (p.succeeded()) {
                        model.addAttribute("tickets", parkingTicketService.getUnpaidParkingTickets(p.user()));
                   }
                   return p;
                });

        return response(
                payment.map(p -> p.succeeded()
                        ? "parking-payment-succeeded"
                        : "payment-failed")
        );
    }

    private static URI returnUrl() {
        return MvcUriComponentsBuilder
                .fromMethodName(ParkingController.class, "getPaymentReturn", null, null)
                .build().toUri();
    }
}
