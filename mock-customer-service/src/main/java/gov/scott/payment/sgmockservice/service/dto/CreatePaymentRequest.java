package gov.scott.payment.sgmockservice.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Wither;

@Value
@RequiredArgsConstructor(staticName = "of")
public class CreatePaymentRequest {

    private @NonNull MoneyDto amount;

    private @NonNull String reference;

    @JsonProperty("returnURL")
    private @NonNull String returnUrl;

    @JsonIgnore
    private @NonNull String description;

    @JsonProperty("service")
    private @Wither String serviceId;
}
