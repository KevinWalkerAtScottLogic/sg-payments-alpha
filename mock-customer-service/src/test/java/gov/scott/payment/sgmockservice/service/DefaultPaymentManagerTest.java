package gov.scott.payment.sgmockservice.service;

import gov.scott.payment.sgmockservice.dao.PaymentRepository;
import gov.scott.payment.sgmockservice.dao.model.PaymentEntity;
import gov.scott.payment.sgmockservice.service.dto.CreatePaymentRequest;
import gov.scott.payment.sgmockservice.service.dto.CreatePaymentResult;
import gov.scott.payment.sgmockservice.service.dto.GetPaymentResult;
import gov.scott.payment.sgmockservice.service.dto.MoneyDto;
import gov.scott.payment.sgmockservice.type.*;
import io.vavr.collection.List;
import lombok.val;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.net.URI;
import java.util.Optional;
import java.util.UUID;

import static io.vavr.control.Either.left;
import static io.vavr.control.Either.right;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class DefaultPaymentManagerTest {

    private final PaymentRepository repository = mock(PaymentRepository.class);

    private final PaymentService paymentService = mock(PaymentService.class);

    private final PaymentObserver observer = mock(PaymentObserver.class);


    private final DefaultPaymentManager sut = new DefaultPaymentManager(repository, paymentService);


    private List<Item> items;


    private PaymentId paymentId = PaymentId.of(UUID.randomUUID().toString());

    private Money amount;

    private User user = User.of("test.user");

    private URI returnUrl = URI.create("http://localhost/test");

    private String reference;

    private String description;


    private MoneyDto amountDto;

    private CreatePaymentResult createPaymentResult;

    private GetPaymentResult getPaymentResult;


    private PaymentEntity entity;

    @Before
    public void setup() {

        val item1 = mock(Item.class);
        when(item1.id()).thenReturn(ItemId.of(UUID.randomUUID().toString()));
        when(item1.amount()).thenReturn(Money.ofMajor(CurrencyUnit.GBP, 1));
        when(item1.description()).thenReturn("Item1 description");

        val item2 = mock(Item.class);
        when(item2.id()).thenReturn(ItemId.of(UUID.randomUUID().toString()));
        when(item2.amount()).thenReturn(Money.ofMajor(CurrencyUnit.GBP, 10));
        when(item2.description()).thenReturn("Item2 description");

        val item3 = mock(Item.class);
        when(item3.id()).thenReturn(ItemId.of(UUID.randomUUID().toString()));
        when(item3.amount()).thenReturn(Money.ofMajor(CurrencyUnit.GBP, 100));
        when(item3.description()).thenReturn("Item3 description");

        items = List.of(item1, item2, item3);


        amount = Money.of(CurrencyUnit.GBP, items.map(i -> i.amount().getAmount()).reduce(BigDecimal::add));

        reference = String.join(";", items.map(i -> i.id().value()));

        description = String.join(";", items.map(Item::description));


        amountDto = MoneyDto.of(amount.getAmount().toPlainString(), amount.getCurrencyUnit().getCode());
        createPaymentResult = CreatePaymentResult
                .of(paymentId.value(), amountDto, "Created", reference,"http://localhost/next_url");

        getPaymentResult = GetPaymentResult
                .of(paymentId.value(), amountDto, "Submitted", reference, "");


        entity = PaymentEntity.builder()
            .id(paymentId.value())
            .amount(amount.getAmountMinorInt())
            .status(PaymentStatus.Created)
            .reference(reference)
            .username(user.username())
            .build();


        when(paymentService.newPayment(any())).thenReturn(right(createPaymentResult));
        when(paymentService.getPayment(eq(paymentId.value()))).thenReturn(right(getPaymentResult));

        when(repository.findById(eq(paymentId.value()))).thenReturn(Optional.of(entity));
    }

    @Test
    public void returns_payment_when_newPayment_succeed() {

        // act
        val result = sut.newPayment(user, items, returnUrl);

        // assert
        assertTrue(result.isRight());

        val payment = result.get()._1;
        val nextUrl = result.get()._2;

        assertEquals(createPaymentResult.getPaymentId(), payment.id().value());
        assertEquals(amount.getAmountMajorInt(), payment.amount().getAmountMajorInt());
        assertEquals(PaymentStatus.Created, payment.status());
        assertEquals(payment.items(), items.map(Item::id));
        assertEquals(user, payment.user());
        assertEquals(URI.create(createPaymentResult.getNextUrl()), nextUrl);
    }

    @Test
    public void returns_payment_error_when_empty_items_list() {
        // act
        val result = sut.newPayment(user, List.empty(), returnUrl);

        // assert
        assertTrue(result.isLeft());
        assertEquals(400, result.getLeft().code().intValue());
    }

    @Test
    public void returns_payment_error_when_items_currency_differ() {
        // assign
        val item4 = mock(Item.class);
        when(item4.id()).thenReturn(ItemId.of("Item4"));
        when(item4.description()).thenReturn("Item4 description");
        when(item4.amount()).thenReturn(Money.ofMajor(CurrencyUnit.EUR, 200));

        val newItems = items.append(item4);

        // act
        val result = sut.newPayment(user, newItems, returnUrl);

        // assert
        assertTrue(result.isLeft());
        assertEquals(400, result.getLeft().code().intValue());
    }

    @Test
    public void returns_payment_error_when_newPayment_fails() {
        // assign
        when(paymentService.newPayment(any())).thenReturn(
                left(PaymentError.of(500, "Internal server error"))
        );

        // act
        val result = sut.newPayment(user, items, returnUrl);

        // assert
        assertTrue(result.isLeft());

        assertEquals(500, result.getLeft().code().intValue());
    }

    @Test
    public void calls_payment_service_when_newPayment_succeed() {
        // act
        sut.newPayment(user, items, returnUrl);

        // assert
        val createRequest = CreatePaymentRequest.of(
                amountDto, reference, returnUrl.toString(), description, ""
        );

        verify(paymentService, times(1)).newPayment(eq(createRequest));
    }

    @Test
    public void calls_repository_save_when_newPayment_succeed() {
        // act
        sut.newPayment(user, items, returnUrl);

        // assert
        verify(repository, times(1)).save(notNull());
    }

    @Test
    public void notifies_observers_when_newPayment_succeed() {
        // assign
        sut.addObserver(observer);

        // act
        sut.newPayment(user, items, returnUrl);

        // assert
        val payment = Payment.builder()
                .id(paymentId)
                .status(PaymentStatus.Created)
                .amount(amount)
                .items(items.map(Item::id))
                .user(user)
                .build();

        verify(observer, times(1)).update(payment);
    }

    @Test
    public void return_valid_payment_when_getPayment_succeed() {
        // act
        val result = sut.proceedPayment(paymentId);

        // assert
        assertTrue(result.isRight());

        val payment = result.get();
        assertEquals(paymentId, payment.id());
        assertEquals(amount, payment.amount());
        assertEquals(PaymentStatus.of(getPaymentResult.getStatus()), payment.status());
        assertEquals(items.map(Item::id), payment.items());
        assertEquals(user, payment.user());
    }

    @Test
    public void calls_repository_save_if_payment_not_finished_and_status_change() {
        // act
        sut.proceedPayment(paymentId);

        // assert
        verify(repository, times(1)).save(any());
    }

    @Test
    public void notify_observers_if_payment_not_finished_and_status_change() {
        // assign
        sut.addObserver(observer);

        // act
        sut.proceedPayment(paymentId);

        // assert
        val payment = Payment.of(
                paymentId, amount, PaymentStatus.of(getPaymentResult.getStatus()), user, items.map(Item::id)
        );
        verify(observer, times(1)).update(eq(payment));
    }

    @Test
    public void do_not_change_payment_status_if_payment_already_finished() {
        // assign
        entity.setStatus(PaymentStatus.Success);

        // act
        val result = sut.proceedPayment(paymentId);

        // assert
        assertTrue(result.isRight());

        val payment = result.get();
        assertEquals(paymentId, payment.id());
        assertEquals(PaymentStatus.Success, payment.status());

        verify(repository, never()).save(any());
    }

    @Test
    public void do_not_notify_observers_if_payment_already_finished() {
        // assign
        entity.setStatus(PaymentStatus.Success);
        sut.addObserver(observer);

        // act
        sut.proceedPayment(paymentId);

        // assert
        verify(observer, never()).update(any());
    }

}
