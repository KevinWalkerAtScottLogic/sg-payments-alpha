package gov.scot.payments.gateway.inbound.cardpayment;

import gov.scot.payments.common.gateway.inbound.cardpayment.CardPaymentGateway;
import gov.scot.payments.model.cardpayment.CardPaymentGatewayAuthRequest;
import gov.scot.payments.model.cardpayment.CardPaymentGatewayCancelRequest;
import gov.scot.payments.model.cardpayment.CardPaymentGatewaySubmitRequest;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.mockito.Mockito.*;

public class GatewayCardPaymentServiceTest {

    private GatewayCardPaymentService service;
    private CardPaymentGateway gateway;

    @BeforeEach
    public void setUp(){
        gateway = mock(CardPaymentGateway.class);
        service = new GatewayCardPaymentService(gateway);
    }

    @Test
    public void testCancel(){
        var request = CardPaymentGatewayCancelRequest.builder()
                .ref("123")
                .paymentRef(UUID.randomUUID())
                .build();
        service.cancelCardPayment(request);
        verify(gateway,times(1)).cancel(request);
    }

    @Test
    public void testSubmit(){
        var request = CardPaymentGatewaySubmitRequest.builder()
                .ref("123")
                .paymentRef(UUID.randomUUID())
                .build();
        service.submitCardPayment(request);
        verify(gateway,times(1)).submit(request);
    }

    @Test
    public void testAuthorize(){
        var request = CardPaymentGatewayAuthRequest.builder()
                .amount(Money.of(1,"GBP"))
                .paymentRef(UUID.randomUUID())
                .build();
        service.authorizeCardPayment(request);
        verify(gateway,times(1)).authorize(request);
    }
}
