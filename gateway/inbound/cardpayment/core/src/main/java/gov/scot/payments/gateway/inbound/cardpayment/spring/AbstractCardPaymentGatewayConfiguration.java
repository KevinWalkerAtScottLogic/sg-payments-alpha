package gov.scot.payments.gateway.inbound.cardpayment.spring;

import gov.scot.payments.common.gateway.inbound.cardpayment.CardPaymentGateway;
import gov.scot.payments.common.gateway.spring.AbstractGatewayConfiguration;
import gov.scot.payments.gateway.inbound.cardpayment.GatewayCardPaymentService;
import gov.scot.payments.model.paymentinstruction.PaymentChannelDirection;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

public abstract class AbstractCardPaymentGatewayConfiguration extends AbstractGatewayConfiguration {


    @Override
    protected PaymentChannelDirection getGatewayDirection(){
        return PaymentChannelDirection.Inbound;
    }

    @Bean
    @Profile("!embedded-gateway & !local-card-gateway")
    public GatewayCardPaymentService cardPaymentService(CardPaymentGateway gateway){
        return new GatewayCardPaymentService(gateway);
    }

}
