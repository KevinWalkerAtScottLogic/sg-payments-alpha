package gov.scot.payments.gateway.inbound.cardpayment;

import gov.scot.payments.common.gateway.inbound.cardpayment.CardPaymentGateway;
import gov.scot.payments.model.cardpayment.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping
public class GatewayCardPaymentService{

    private final CardPaymentGateway gateway;

    public GatewayCardPaymentService(CardPaymentGateway gateway) {
        this.gateway = gateway;
    }

    @PostMapping("/cardPayment/authorize")
    @ResponseBody
    @PreAuthorize("principal.hasWildcardAccess('inbound-payments',T(gov.scot.payments.model.user.EntityOp).Write)")
    public CardPaymentGatewayAuthResponse authorizeCardPayment(@RequestBody CardPaymentGatewayAuthRequest request){
        return gateway.authorize(request);
    }

    @PostMapping("/cardPayment/submit")
    @ResponseBody
    @PreAuthorize("principal.hasWildcardAccess('inbound-payments',T(gov.scot.payments.model.user.EntityOp).Write)")
    public CardPaymentGatewaySubmitResponse submitCardPayment(@RequestBody CardPaymentGatewaySubmitRequest request){
        return gateway.submit(request);
    }

    @PostMapping("/cardPayment/cancel")
    @ResponseBody
    @PreAuthorize("principal.hasWildcardAccess('inbound-payments',T(gov.scot.payments.model.user.EntityOp).Write)")
    public CardPaymentGatewayCancelResponse cancelCardPayment(@RequestBody CardPaymentGatewayCancelRequest request){
        return gateway.cancel(request);
    }
}
