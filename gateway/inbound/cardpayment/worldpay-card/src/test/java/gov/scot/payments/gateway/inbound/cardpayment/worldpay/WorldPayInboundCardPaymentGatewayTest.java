package gov.scot.payments.gateway.inbound.cardpayment.worldpay;

import gov.scot.payments.gateway.inbound.cardpayment.worldpay.model.WorldPayCardToken;
import gov.scot.payments.gateway.inbound.cardpayment.worldpay.model.WorldPayPaymentMethod;
import gov.scot.payments.gateway.inbound.cardpayment.worldpay.restclient.*;
import gov.scot.payments.model.cardpayment.*;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;
import java.util.UUID;

import static gov.scot.payments.gateway.inbound.cardpayment.worldpay.WorldPayMetadata.WORLDPAY_ORDER_CODE_KEY;
import static gov.scot.payments.gateway.inbound.cardpayment.worldpay.WorldPayMetadata.WORLDPAY_TOKEN_KEY;
import static gov.scot.payments.gateway.inbound.cardpayment.worldpay.model.WorldPayPaymentMethod.PaymentType.ObfuscatedCard;
import static java.util.Map.entry;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class WorldPayInboundCardPaymentGatewayTest {

    private WorldPayClient worldPayClient;

    private WorldPayInboundCardPaymentGateway gateway;

    private static final String CUSTOMER_NAME = "A Customer";
    private static final int EXPIRY_YEAR = 2025;
    private static final int EXPIRY_MONTH = 12;
    private static final LocalDate EXPIRY_DATE = LocalDate.of(EXPIRY_YEAR, EXPIRY_MONTH, 1);
    private static final String CARD_NUMBER = "1234 5678 9012 3456";
    private static final String MASKED_CARD_NUMBER = "**** **** **** 3456";
    private static final String CARD_TYPE = "VISA_CREDIT";
    private static final String CVC = "246";
    private static final Money MONEY_AMOUNT = Money.of(new BigDecimal("543.21"), "GBP");
    private static final UUID PAYMENT_REF = UUID.randomUUID();
    private static final String WORLDPAY_ORDER_CODE = "wp-order-code-123-abc";
    private static final String TOKEN = "token_123456abcdef";
    private static final String CARD_ISSUER = "Card_Issuer";
    private static final String ERROR_MSG = "Error message";

    @BeforeEach
    void setUp() {
        worldPayClient = mock(WorldPayClient.class);
        gateway = new WorldPayInboundCardPaymentGateway("gateway", worldPayClient);
    }

    @Test
    void whenClientGetTokenOKAndAuthorizeThenGatewayAuthorizeReturnsClientAuthorizeResponse() throws WorldPayCreateTokenException, WorldPayOrderException {

        CardPaymentGatewayAuthRequest gatewayAuthRequest = getCardPaymentGatewayAuthRequest();
        WorldPayCardToken cardToken = getWorldPayCardTokenResponse();
        when(worldPayClient.getToken(gatewayAuthRequest)).thenReturn(cardToken);
        CardPaymentGatewayAuthResponse gatewayAuthResponse = getGatewayAuthResponse();
        when(worldPayClient.authorize(gatewayAuthRequest, cardToken)).thenReturn(gatewayAuthResponse);

        CardPaymentGatewayAuthResponse authResponse = gateway.authorize(gatewayAuthRequest);

        assertThat(authResponse, is(gatewayAuthResponse));
    }

    @Test
    void whenClientGetTokenThrowsExceptionThenGatewayAuthorizeReturnsErrorResponse() throws WorldPayCreateTokenException {

        CardPaymentGatewayAuthRequest gatewayAuthRequest = getCardPaymentGatewayAuthRequest();
        when(worldPayClient.getToken(gatewayAuthRequest)).thenThrow(new WorldPayCreateTokenException(ERROR_MSG));

        CardPaymentGatewayAuthResponse authResponse = gateway.authorize(gatewayAuthRequest);

        assertThat(authResponse.getStatus(), is(CardPaymentStatus.Error));
        assertThat(authResponse.getMessage(), is(ERROR_MSG));
        assertThat(authResponse.getRef(), is(PAYMENT_REF.toString()));
    }

    @Test
    void whenClientGetTokenOKAndAuthorizeThrowsExceptionThenGatewayAuthorizeReturnsErrorResponse() throws WorldPayCreateTokenException, WorldPayOrderException {

        CardPaymentGatewayAuthRequest gatewayAuthRequest = getCardPaymentGatewayAuthRequest();
        WorldPayCardToken cardToken = getWorldPayCardTokenResponse();
        when(worldPayClient.getToken(gatewayAuthRequest)).thenReturn(cardToken);
        when(worldPayClient.authorize(gatewayAuthRequest, cardToken)).thenThrow(new WorldPayOrderException(ERROR_MSG));

        CardPaymentGatewayAuthResponse authResponse = gateway.authorize(gatewayAuthRequest);

        assertThat(authResponse.getStatus(), is(CardPaymentStatus.Error));
        assertThat(authResponse.getMessage(), is(ERROR_MSG));
        assertThat(authResponse.getRef(), is(PAYMENT_REF.toString()));
    }

    @Test
    void whenClientSubmitOKThenGatewaySubmitReturnsClientSubmitResponse() throws WorldPayCaptureException {

        CardPaymentGatewaySubmitRequest gatewaySubmitRequest = getCardPaymentGatewaySubmitRequest();
        CardPaymentGatewaySubmitResponse gatewaySubmitResponse = getGatewaySubmitResponse();
        when(worldPayClient.capture(gatewaySubmitRequest)).thenReturn(gatewaySubmitResponse);

        CardPaymentGatewaySubmitResponse authResponse = gateway.submit(gatewaySubmitRequest);

        assertThat(authResponse, is(gatewaySubmitResponse));
    }

    @Test
    void whenClientSubmitThrowsExceptionThenGatewaySubmitReturnsErrorResponse() throws WorldPayCaptureException {

        CardPaymentGatewaySubmitRequest gatewaySubmitRequest = getCardPaymentGatewaySubmitRequest();
        when(worldPayClient.capture(gatewaySubmitRequest)).thenThrow(new WorldPayCaptureException(ERROR_MSG));

        CardPaymentGatewaySubmitResponse submitResponse = gateway.submit(gatewaySubmitRequest);

        assertThat(submitResponse.getStatus(), is(CardPaymentStatus.Error));
        assertThat(submitResponse.getMessage(), is(ERROR_MSG));
        assertThat(submitResponse.getRef(), is(PAYMENT_REF.toString()));
    }

    @Test
    void whenClientCancelOKThenGatewayCancelReturnsClientCancelResponse() throws WorldPayCancelException {

        CardPaymentGatewayCancelRequest gatewayCancelRequest = getCardPaymentGatewayCancelRequest();
        CardPaymentGatewayCancelResponse gatewayCancelResponse = getGatewayCancelResponse();
        when(worldPayClient.cancel(gatewayCancelRequest)).thenReturn(gatewayCancelResponse);

        CardPaymentGatewayCancelResponse cancelResponse = gateway.cancel(gatewayCancelRequest);

        assertThat(cancelResponse, is(gatewayCancelResponse));
    }

    @Test
    void whenClientCancelThrowsExceptionThenGatewayCancelReturnsErrorResponse() throws WorldPayCancelException {

        CardPaymentGatewayCancelRequest gatewayCancelRequest = getCardPaymentGatewayCancelRequest();
        when(worldPayClient.cancel(gatewayCancelRequest)).thenThrow(new WorldPayCancelException(ERROR_MSG));

        CardPaymentGatewayCancelResponse cancelResponse = gateway.cancel(gatewayCancelRequest);

        assertThat(cancelResponse.getStatus(), is(CardPaymentStatus.Error));
        assertThat(cancelResponse.getMessage(), is(ERROR_MSG));
        assertThat(cancelResponse.getRef(), is(PAYMENT_REF.toString()));
    }

    @Test
    void cancel() {
    }

    private CardPaymentGatewayAuthRequest getCardPaymentGatewayAuthRequest() {
        return CardPaymentGatewayAuthRequest.builder()
                .name(CUSTOMER_NAME)
                .cardNumber(CARD_NUMBER)
                .cvv(CVC)
                .expiry(EXPIRY_DATE)
                .amount(MONEY_AMOUNT)
                .paymentRef(PAYMENT_REF)
                .build();
    }

    private WorldPayCardToken getWorldPayCardTokenResponse() {
        return WorldPayCardToken.builder()
                .token(TOKEN)
                .reusable(false)
                .paymentMethod(getPaymentResponse())
                .build();
    }

    private WorldPayPaymentMethod getPaymentResponse() {
        return WorldPayPaymentMethod.builder()
                .type(ObfuscatedCard)
                .name(CUSTOMER_NAME)
                .expiryMonth(EXPIRY_MONTH)
                .expiryYear(EXPIRY_YEAR)
                .cardType(CARD_TYPE)
                .maskedCardNumber(MASKED_CARD_NUMBER)
                .cardIssuer(CARD_ISSUER)
                .build();
    }

    private CardPaymentGatewayAuthResponse getGatewayAuthResponse() {
        return  CardPaymentGatewayAuthResponse.builder()
                .status(CardPaymentStatus.Submitted)
                .ref(PAYMENT_REF.toString())
                .details(CardDetails.builder()
                        .issuer(CARD_ISSUER)
                        .type(CARD_TYPE)
                        .expiry(EXPIRY_DATE)
                        .maskedCardNumber(MASKED_CARD_NUMBER)
                        .name(CUSTOMER_NAME)
                        .build())
                .amount(MONEY_AMOUNT)
                .metadata(Map.ofEntries(
                        entry(WORLDPAY_TOKEN_KEY, TOKEN),
                        entry(WORLDPAY_ORDER_CODE_KEY, WORLDPAY_ORDER_CODE)))
                .build();
    }

    private CardPaymentGatewaySubmitRequest getCardPaymentGatewaySubmitRequest() {
        return CardPaymentGatewaySubmitRequest.builder()
                .paymentRef(PAYMENT_REF)
                .ref(PAYMENT_REF.toString())
                .build();
    }

    private CardPaymentGatewaySubmitResponse getGatewaySubmitResponse() {
        return CardPaymentGatewaySubmitResponse.builder()
                .status(CardPaymentStatus.Submitted)
                .ref(PAYMENT_REF.toString())
                .metadata(Map.ofEntries(
                        entry(WORLDPAY_TOKEN_KEY, TOKEN),
                        entry(WORLDPAY_ORDER_CODE_KEY, WORLDPAY_ORDER_CODE)))
                .build();
    }

    private CardPaymentGatewayCancelRequest getCardPaymentGatewayCancelRequest() {
        return CardPaymentGatewayCancelRequest.builder()
                .paymentRef(PAYMENT_REF)
                .ref(PAYMENT_REF.toString())
                .build();
    }


    private CardPaymentGatewayCancelResponse getGatewayCancelResponse() {
        return CardPaymentGatewayCancelResponse.builder()
                .status(CardPaymentStatus.Submitted)
                .ref(PAYMENT_REF.toString())
                .metadata(Map.ofEntries(
                        entry(WORLDPAY_TOKEN_KEY, TOKEN),
                        entry(WORLDPAY_ORDER_CODE_KEY, WORLDPAY_ORDER_CODE)))
                .build();
    }
}