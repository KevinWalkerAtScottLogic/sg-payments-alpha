package gov.scot.payments.gateway.inbound.cardpayment.worldpay.restclient;

import gov.scot.payments.gateway.inbound.cardpayment.worldpay.model.WorldPayCardToken;
import gov.scot.payments.gateway.inbound.cardpayment.worldpay.model.WorldPayOrder;
import gov.scot.payments.gateway.inbound.cardpayment.worldpay.model.WorldPayPaymentMethod;
import gov.scot.payments.model.cardpayment.*;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;
import java.util.UUID;

import static gov.scot.payments.gateway.inbound.cardpayment.worldpay.WorldPayMetadata.WORLDPAY_ORDER_CODE_KEY;
import static gov.scot.payments.gateway.inbound.cardpayment.worldpay.model.WorldPayPaymentMethod.PaymentType.Card;
import static gov.scot.payments.gateway.inbound.cardpayment.worldpay.model.WorldPayPaymentMethod.PaymentType.ObfuscatedCard;
import static java.util.Map.entry;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class WorldPayClientTest {

    private WorldPayAPI worldPayAPI;
    private WorldPayAuthorization worldPayAuthorization;
    private WorldPayClient worldPayClient;

    private static final String SERVICE_KEY = "service_key";
    private static final String CLIENT_KEY = "client_key";
    private static final String CUSTOMER_NAME = "A Customer";
    private static final int EXPIRY_YEAR = 2025;
    private static final int EXPIRY_MONTH = 12;
    private static final LocalDate EXPIRY_DATE = LocalDate.of(EXPIRY_YEAR, EXPIRY_MONTH, 1);
    private static final String CARD_NUMBER = "1234 5678 9012 3456";
    private static final String MASKED_CARD_NUMBER = "**** **** **** 3456";
    private static final String CARD_TYPE = "VISA_CREDIT";
    private static final String CVC = "246";
    private static final String CURRENCY_CODE = "GBP";
    private static final Money MONEY_AMOUNT = Money.of(new BigDecimal("543.21"), CURRENCY_CODE);
    private static final Integer AMOUNT = 54321;
    private static final UUID PAYMENT_REF = UUID.randomUUID();
    private static final String TOKEN = "token_123456abcdef";
    private static final String CARD_ISSUER = "Card_Issuer";
    private static final String WORLDPAY_ORDER_CODE = "wp-order-code-123-abc";
    private static final String PAYMENT_STATUS_REASON = "Reason for failed order";

    @BeforeEach
    void setUp() {
        worldPayAPI = mock(WorldPayAPI.class);
        worldPayAuthorization = new WorldPayAuthorization(SERVICE_KEY, CLIENT_KEY);
        worldPayClient = new WorldPayClient(worldPayAPI, worldPayAuthorization);
    }

    @Test
    void givenTokenApiRequestSuccessfulThenGetTokenReturnsAToken() throws WorldPayCreateTokenException {

        CardPaymentGatewayAuthRequest gatewayRequest = getCardPaymentGatewayAuthRequest();
        WorldPayCardToken requestCardToken = getWorldPayCardTokenRequest();
        WorldPayCardToken responseCardToken = getWorldPayCardTokenResponse();
        when(worldPayAPI.token(requestCardToken, worldPayAuthorization)).thenReturn(responseCardToken);

        WorldPayCardToken returnedCardToken = worldPayClient.getToken(gatewayRequest);

        assertThat(returnedCardToken, is(responseCardToken));
    }

    @Test
    void givenTokenApiRequestThrowsExceptionThenGetTokenThrowsException() throws WorldPayCreateTokenException {

        CardPaymentGatewayAuthRequest gatewayRequest = getCardPaymentGatewayAuthRequest();
        WorldPayCardToken cardToken = getWorldPayCardTokenRequest();
        when(worldPayAPI.token(cardToken, worldPayAuthorization)).thenThrow(WorldPayCreateTokenException.class);

        assertThrows(WorldPayCreateTokenException.class, () -> worldPayClient.getToken(gatewayRequest));
    }

    @Test
    void givenOrderApiResponseIsAuthorizedThenAuthorizeReturnsSubmittedAuthResponse() throws WorldPayOrderException {

        CardPaymentGatewayAuthRequest gatewayRequest = getCardPaymentGatewayAuthRequest();
        WorldPayCardToken cardToken = getWorldPayCardTokenResponse();
        when(worldPayAPI.order(any(), eq(worldPayAuthorization))).thenReturn(getWorldPayAuthorizeOrderResponse("AUTHORIZED"));

        CardPaymentGatewayAuthResponse authResponse = worldPayClient.authorize(gatewayRequest, cardToken);

        assertThat(authResponse.getStatus(), is(CardPaymentStatus.Submitted));
        assertThat(authResponse.getRef(), is(PAYMENT_REF.toString()));
        assertThat(authResponse.getDetails().getIssuer(), is(CARD_ISSUER));
        assertThat(authResponse.getDetails().getType(), is(CARD_TYPE));
        assertThat(authResponse.getDetails().getExpiry(), is(EXPIRY_DATE));
        assertThat(authResponse.getDetails().getMaskedCardNumber(), is(MASKED_CARD_NUMBER));
        assertThat(authResponse.getDetails().getName(), is(CUSTOMER_NAME));
        assertThat(authResponse.getAmount(), is(MONEY_AMOUNT));
        assertThat(authResponse.getMetadata(), hasEntry(WORLDPAY_ORDER_CODE_KEY, WORLDPAY_ORDER_CODE));
    }

    @Test
    void givenOrderApiResponseIsFailedThenAuthorizeReturnsFailedAuthResponse() throws WorldPayOrderException {

        CardPaymentGatewayAuthRequest gatewayRequest = getCardPaymentGatewayAuthRequest();
        WorldPayCardToken cardToken = getWorldPayCardTokenResponse();
        when(worldPayAPI.order(any(), eq(worldPayAuthorization))).thenReturn(getWorldPayAuthorizeOrderResponse("FAILED"));

        CardPaymentGatewayAuthResponse authResponse = worldPayClient.authorize(gatewayRequest, cardToken);

        assertThat(authResponse.getStatus(), is(CardPaymentStatus.Failed));
        assertThat(authResponse.getMessage(), is(PAYMENT_STATUS_REASON));
        assertThat(authResponse.getRef(), is(PAYMENT_REF.toString()));
        assertThat(authResponse.getDetails().getIssuer(), is(CARD_ISSUER));
        assertThat(authResponse.getDetails().getType(), is(CARD_TYPE));
        assertThat(authResponse.getDetails().getExpiry(), is(EXPIRY_DATE));
        assertThat(authResponse.getDetails().getMaskedCardNumber(), is(MASKED_CARD_NUMBER));
        assertThat(authResponse.getDetails().getName(), is(CUSTOMER_NAME));
        assertThat(authResponse.getMetadata(), hasEntry(WORLDPAY_ORDER_CODE_KEY, WORLDPAY_ORDER_CODE));
    }

    @Test
    void givenOrderApiRequestThrowsExceptionThenAuthorizeThrowsException() throws WorldPayOrderException {

        CardPaymentGatewayAuthRequest gatewayRequest = getCardPaymentGatewayAuthRequest();
        WorldPayCardToken cardToken = getWorldPayCardTokenResponse();
        when(worldPayAPI.order(any(), eq(worldPayAuthorization))).thenThrow(WorldPayOrderException.class);

        assertThrows(WorldPayOrderException.class, () -> worldPayClient.authorize(gatewayRequest, cardToken));
    }

    @Test
    void givenOrderApiResponseIsSuccessThenCaptureReturnsSuccessfulSubmitResponse() throws WorldPayCaptureException {

        CardPaymentGatewaySubmitRequest gatewayRequest = getCardPaymentGatewaySubmitRequest();
        when(worldPayAPI.capture(WORLDPAY_ORDER_CODE, worldPayAuthorization)).thenReturn(getWorldPayCaptureOrderResponse("SUCCESS"));

        CardPaymentGatewaySubmitResponse submitResponse = worldPayClient.capture(gatewayRequest);

        assertThat(submitResponse.getStatus(), is(CardPaymentStatus.Success));
        assertThat(submitResponse.getRef(), is(PAYMENT_REF.toString()));
        assertThat(submitResponse.getMetadata(), hasEntry(WORLDPAY_ORDER_CODE_KEY, WORLDPAY_ORDER_CODE));
    }

    @Test
    void givenOrderApiResponseIsExpiredThenCaptureReturnsExpiredSubmitResponse() throws WorldPayCaptureException {

        CardPaymentGatewaySubmitRequest gatewayRequest = getCardPaymentGatewaySubmitRequest();
        when(worldPayAPI.capture(WORLDPAY_ORDER_CODE, worldPayAuthorization)).thenReturn(getWorldPayCaptureOrderResponse("EXPIRED"));

        CardPaymentGatewaySubmitResponse submitResponse = worldPayClient.capture(gatewayRequest);

        assertThat(submitResponse.getStatus(), is(CardPaymentStatus.Expired));
        assertThat(submitResponse.getRef(), is(PAYMENT_REF.toString()));
        assertThat(submitResponse.getMetadata(), hasEntry(WORLDPAY_ORDER_CODE_KEY, WORLDPAY_ORDER_CODE));
    }

    @Test
    void givenOrderApiResponseIsFailedThenCaptureReturnsFailedSubmitResponse() throws WorldPayCaptureException {

        CardPaymentGatewaySubmitRequest gatewayRequest = getCardPaymentGatewaySubmitRequest();
        when(worldPayAPI.capture(WORLDPAY_ORDER_CODE, worldPayAuthorization)).thenReturn(getWorldPayCaptureOrderResponse("FAILED"));

        CardPaymentGatewaySubmitResponse submitResponse = worldPayClient.capture(gatewayRequest);

        assertThat(submitResponse.getStatus(), is(CardPaymentStatus.Failed));
        assertThat(submitResponse.getMessage(), is(PAYMENT_STATUS_REASON));
        assertThat(submitResponse.getRef(), is(PAYMENT_REF.toString()));
        assertThat(submitResponse.getMetadata(), hasEntry(WORLDPAY_ORDER_CODE_KEY, WORLDPAY_ORDER_CODE));
    }

    @Test
    void givenOrderApiRequestThrowsExceptionThenCaptureThrowsException() throws WorldPayCaptureException {

        CardPaymentGatewaySubmitRequest gatewayRequest = getCardPaymentGatewaySubmitRequest();
        when(worldPayAPI.capture(WORLDPAY_ORDER_CODE, worldPayAuthorization)).thenThrow(WorldPayCaptureException.class);

        assertThrows(WorldPayCaptureException.class, () -> worldPayClient.capture(gatewayRequest));
    }

    @Test
    void givenOrderApiResponseStatusIsCancelledThenCancelReturnsCancelResponse() throws WorldPayCancelException {

        CardPaymentGatewayCancelRequest gatewayRequest = getCardPaymentGatewayCancelRequest();

        CardPaymentGatewayCancelResponse cancelResponse = worldPayClient.cancel(gatewayRequest);

        assertThat(cancelResponse.getStatus(), is(CardPaymentStatus.Cancelled));
        assertThat(cancelResponse.getRef(), is(PAYMENT_REF.toString()));
        assertThat(cancelResponse.getMetadata(), hasEntry(WORLDPAY_ORDER_CODE_KEY, WORLDPAY_ORDER_CODE));
    }

    @Test
    void givenOrderApiRequestThrowsExceptionThenCancelThrowsException() throws WorldPayCancelException {

        CardPaymentGatewayCancelRequest gatewayRequest = getCardPaymentGatewayCancelRequest();
        doThrow(WorldPayCancelException.class).when(worldPayAPI).cancel(WORLDPAY_ORDER_CODE, worldPayAuthorization);

        assertThrows(WorldPayCancelException.class, () -> worldPayClient.cancel(gatewayRequest));
    }

    private WorldPayCardToken getWorldPayCardTokenRequest() {
        return WorldPayCardToken.builder()
                .reusable(false)
                .clientKey(CLIENT_KEY)
                .paymentMethod(
                        WorldPayPaymentMethod.builder()
                                .name(CUSTOMER_NAME)
                                .expiryMonth(EXPIRY_MONTH)
                                .expiryYear(EXPIRY_YEAR)
                                .cardNumber(CARD_NUMBER)
                                .type(Card)
                                .cvc(CVC)
                                .build()
                )
                .build();
    }

    private WorldPayCardToken getWorldPayCardTokenResponse() {
        return WorldPayCardToken.builder()
                .token(TOKEN)
                .reusable(false)
                .paymentMethod(getPaymentResponse())
                .build();
    }

    private WorldPayPaymentMethod getPaymentResponse() {
        return WorldPayPaymentMethod.builder()
                .type(ObfuscatedCard)
                .name(CUSTOMER_NAME)
                .expiryMonth(EXPIRY_MONTH)
                .expiryYear(EXPIRY_YEAR)
                .cardType(CARD_TYPE)
                .maskedCardNumber(MASKED_CARD_NUMBER)
                .cardIssuer(CARD_ISSUER)
                .build();
    }

    private CardPaymentGatewayAuthRequest getCardPaymentGatewayAuthRequest() {
        return CardPaymentGatewayAuthRequest.builder()
                .name(CUSTOMER_NAME)
                .cardNumber(CARD_NUMBER)
                .cvv(CVC)
                .expiry(EXPIRY_DATE)
                .amount(MONEY_AMOUNT)
                .paymentRef(PAYMENT_REF)
                .build();
    }

    private WorldPayOrder getWorldPayAuthorizeOrderResponse(String paymentStatus) {
        var builder = WorldPayOrder.builder()
                .paymentStatus(paymentStatus)
                .token(TOKEN)
                .amount(AMOUNT)
                .currencyCode(CURRENCY_CODE)
                .authorizeOnly(true)
                .authorizedAmount(AMOUNT)
                .orderDescription("Authorization")
                .customerOrderCode(PAYMENT_REF.toString())
                .orderCode(WORLDPAY_ORDER_CODE)
                .paymentResponse(getPaymentResponse());
        if (paymentStatus.equals("FAILED")) {
            builder = builder.paymentStatusReason(PAYMENT_STATUS_REASON);
        }
        return builder.build();
    }

    private CardPaymentGatewaySubmitRequest getCardPaymentGatewaySubmitRequest() {
        return CardPaymentGatewaySubmitRequest.builder()
                .paymentRef(PAYMENT_REF)
                .ref(PAYMENT_REF.toString())
                .metadata(Map.ofEntries(entry(WORLDPAY_ORDER_CODE_KEY, WORLDPAY_ORDER_CODE)))
                .build();
    }

    private WorldPayOrder getWorldPayCaptureOrderResponse(String paymentStatus) {
        var builder = WorldPayOrder.builder()
                .customerOrderCode(PAYMENT_REF.toString())
                .orderCode(WORLDPAY_ORDER_CODE)
                .token(TOKEN)
                .orderDescription("Authorization")
                .authorizedAmount(AMOUNT)
                .currencyCode(CURRENCY_CODE)
                .authorizeOnly(true) // According to the WorldPay example
                .paymentStatus(paymentStatus)
                .paymentResponse(getPaymentResponse());
        if (paymentStatus.equals("FAILED")) {
            builder = builder.paymentStatusReason(PAYMENT_STATUS_REASON);
        }
        return builder.build();
    }

    private CardPaymentGatewayCancelRequest getCardPaymentGatewayCancelRequest() {
        return CardPaymentGatewayCancelRequest.builder()
                .paymentRef(PAYMENT_REF)
                .ref(PAYMENT_REF.toString())
                .metadata(Map.ofEntries(
                        entry(WORLDPAY_ORDER_CODE_KEY, WORLDPAY_ORDER_CODE)))
                .build();
    }
}
