package gov.scot.payments.gateway.inbound.cardpayment.worldpay.restclient;

public class WorldPayCaptureException extends WorldPayException {

    public WorldPayCaptureException(String message) {
        super(message);
    }

}
