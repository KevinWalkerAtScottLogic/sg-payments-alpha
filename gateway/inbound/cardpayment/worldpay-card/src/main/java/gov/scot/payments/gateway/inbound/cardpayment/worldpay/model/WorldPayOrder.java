package gov.scot.payments.gateway.inbound.cardpayment.worldpay.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sun.istack.NotNull;
import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Getter
@ToString
@EqualsAndHashCode
@Builder
@JsonInclude(Include.NON_NULL)
public class WorldPayOrder {

    private String orderCode;
    private String token;
    private String orderType;
    @NotNull private Integer amount;
    private boolean authorizeOnly;
    @NotNull private Integer authorizedAmount;
    @NotNull private String currencyCode;
    @NotNull private String orderDescription;
    private String customerOrderCode;
    private String paymentStatus;
    private String paymentStatusReason;
    private WorldPayPaymentMethod paymentResponse;
    private String settlementCurrency;
    private String name;
    private String environment;
    private RiskScore riskScore;
    // billingAddress
    // deliveryAddress
    // shopperEmailAddress
    // shopperIpAddress
    // shopperSessionId

    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PUBLIC)
    @Getter
    @ToString
    @EqualsAndHashCode
    @Builder
    private static final class RiskScore {
        @NotNull String value;
    }
}
