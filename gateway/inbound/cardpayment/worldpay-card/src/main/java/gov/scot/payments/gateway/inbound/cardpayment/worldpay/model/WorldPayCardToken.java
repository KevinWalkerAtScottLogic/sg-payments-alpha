package gov.scot.payments.gateway.inbound.cardpayment.worldpay.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.*;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Getter
@ToString
@EqualsAndHashCode
@Builder
@JsonInclude(Include.NON_NULL)
public class WorldPayCardToken {

    private String token;
    private boolean reusable;
    private WorldPayPaymentMethod paymentMethod;
    private String clientKey;
}
