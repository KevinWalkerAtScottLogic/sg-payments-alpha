package gov.scot.payments.gateway.inbound.cardpayment.worldpay.restclient;

import gov.scot.payments.gateway.inbound.cardpayment.worldpay.model.WorldPayCardToken;

public class WorldPayCreateTokenException extends WorldPayException {

    public WorldPayCreateTokenException(String message) {
        super(message);
    }

}
