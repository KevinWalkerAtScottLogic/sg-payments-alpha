package gov.scot.payments.gateway.inbound.cardpayment.worldpay.restclient;

public class WorldPayOrderException extends WorldPayException {

    public WorldPayOrderException(String message) {
        super(message);
    }

}
