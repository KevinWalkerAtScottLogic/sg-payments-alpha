package gov.scot.payments.gateway.inbound.cardpayment.worldpay;

public interface WorldPayMetadata {

    String WORLDPAY_ORDER_CODE_KEY = "worldpayOrderCode";
    String WORLDPAY_TOKEN_KEY = "worldpayToken";
}
