package gov.scot.payments.gateway.inbound.cardpayment.worldpay.restclient;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.gateway.inbound.cardpayment.worldpay.model.WorldPayCardToken;
import gov.scot.payments.gateway.inbound.cardpayment.worldpay.model.WorldPayErrorMessage;
import gov.scot.payments.gateway.inbound.cardpayment.worldpay.model.WorldPayOrder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.RetryOperations;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestOperations;

import java.io.IOException;

@Slf4j
public class WorldPayAPI {

    private static final String TOKEN_ENDPOINT = "/tokens";
    private static final String ORDER_ENDPOINT = "/orders";
    private static final String CAPTURE_ENDPOINT = "/orders/{ORDER_CODE}/capture";
    private static final String CANCEL_ENDPOINT = "/orders/{ORDER_CODE}";

    private final RestOperations restOps;
    private final RetryOperations retryOps;
    private String apiEndpointBase;
    private ObjectMapper objectMapper;

    public WorldPayAPI(RestOperations operations, RetryOperations retryOperations, String apiEndpoint, ObjectMapper objectMapper) {
        this.restOps = operations;
        this.retryOps = retryOperations;
        apiEndpointBase = apiEndpoint;
        this.objectMapper = objectMapper;
    }

    public WorldPayCardToken token(WorldPayCardToken cardToken, WorldPayAuthorization authorization) throws WorldPayCreateTokenException {

        HttpHeaders headers = authorization.getNoAuthHttpHeaders();
        HttpEntity<WorldPayCardToken> entity = new HttpEntity<>(cardToken, headers);

        try{
            WorldPayCardToken body = retryOps.execute(c -> restOps.postForEntity(apiEndpointBase + TOKEN_ENDPOINT, entity, WorldPayCardToken.class)).getBody();
            return body;
        } catch(HttpStatusCodeException e) {
            WorldPayErrorMessage errorMessage;
            try {
                errorMessage = objectMapper.readValue(e.getResponseBodyAsString(), WorldPayErrorMessage.class);
            } catch (IOException e1) {
                log.warn("Failed to parse error response: " + e.getResponseBodyAsString());
                throw new WorldPayCreateTokenException("Error creating token. Failed to parse error response.");
            }
            log.warn("Error creating token {}", errorMessage);
            throw new WorldPayCreateTokenException("Error creating token: "+ errorMessage);
        }
    }

    public WorldPayOrder order(WorldPayOrder worldPayOrder, WorldPayAuthorization authorization) throws WorldPayOrderException {

        HttpHeaders headers = authorization.getServiceAuthHttpHeaders();
        HttpEntity<WorldPayOrder> entity = new HttpEntity<>(worldPayOrder, headers);

        try{
            return retryOps.execute(c -> restOps.postForEntity(apiEndpointBase + ORDER_ENDPOINT, entity, WorldPayOrder.class)).getBody();
        } catch(HttpStatusCodeException e) {
            WorldPayErrorMessage errorMessage;
            try {
                errorMessage = objectMapper.readValue(e.getResponseBodyAsString(), WorldPayErrorMessage.class);
            } catch (IOException e1) {
                log.warn("Failed to parse error response: " + e.getResponseBodyAsString() + "\n\n" + e1.getMessage());
                throw new WorldPayOrderException("Error submitting order. Failed to parse error response.");
            }
            log.warn("Error submitting order {}", errorMessage);
            throw new WorldPayOrderException("Error submitting order: "+ errorMessage);
        }
    }

    public WorldPayOrder capture(String orderCode, WorldPayAuthorization authorization) throws WorldPayCaptureException {

        HttpHeaders headers = authorization.getServiceAuthHttpHeaders();
        HttpEntity<?> entity = new HttpEntity<>(headers);

        try{
            return retryOps.execute(c -> restOps.postForEntity(apiEndpointBase + CAPTURE_ENDPOINT, entity, WorldPayOrder.class, orderCode)).getBody();
        } catch(HttpStatusCodeException e) {
            WorldPayErrorMessage errorMessage;
            try {
                errorMessage = objectMapper.readValue(e.getResponseBodyAsString(), WorldPayErrorMessage.class);
            } catch (IOException e1) {
                log.warn("Failed to parse error response: " + e.getResponseBodyAsString());
                throw new WorldPayCaptureException("Error capturing order. Failed to parse error response.");
            }
            log.warn("Error capturing order {}", errorMessage);
            throw new WorldPayCaptureException("Error capturing order: "+ errorMessage);
        }
    }

    public void cancel(String orderCode, WorldPayAuthorization authorization) throws WorldPayCancelException {
        HttpHeaders headers = authorization.getServiceAuthHttpHeaders();
        HttpEntity<?> entity = new HttpEntity<>(headers);

        // Cancel returns 200 on successful cancel, with no body

        try{
            ResponseEntity responseEntity = retryOps.execute(c -> restOps.exchange(apiEndpointBase + CANCEL_ENDPOINT, HttpMethod.DELETE, entity, String.class, orderCode));
            if (responseEntity.getStatusCode().isError()) {
                var msg = "Error cancelling order: " + responseEntity.getStatusCode().toString() +
                        ":" + responseEntity.getStatusCode().getReasonPhrase();
                log.warn(msg);
                throw new WorldPayCancelException(msg);
            }
        } catch(HttpStatusCodeException e) {
            WorldPayErrorMessage errorMessage;
            try {
                errorMessage = objectMapper.readValue(e.getResponseBodyAsString(), WorldPayErrorMessage.class);
            } catch (IOException e1) {
                log.warn("Failed to parse error response: " + e.getResponseBodyAsString());
                throw new WorldPayCancelException("Error capturing cancelling. Failed to parse error response.");
            }
            log.warn("Error capturing order {}", errorMessage);
            throw new WorldPayCancelException("Error cancelling order: "+ errorMessage);
        }
    }
}
