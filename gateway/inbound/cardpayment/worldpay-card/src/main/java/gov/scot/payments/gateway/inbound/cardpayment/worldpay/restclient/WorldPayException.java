package gov.scot.payments.gateway.inbound.cardpayment.worldpay.restclient;

public class WorldPayException extends Exception {

    public WorldPayException(String message) {
        super(message);
    }

    public WorldPayException(String message, Throwable cause) {
        super(message, cause);
    }

}
