package gov.scot.payments.gateway.inbound.cardpayment.mock.local;

import gov.scot.payments.MockWebSecurityConfigurationCore;
import gov.scot.payments.common.gateway.spring.CoreGatewayConfiguration;
import gov.scot.payments.common.local.LocalConfig;
import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.gateway.inbound.cardpayment.mock.spring.MockInboundCardPaymentGatewayConfiguration;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import lombok.Setter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.boot.web.context.WebServerPortFileWriter;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

import java.util.Set;

@SpringBootApplication(scanBasePackageClasses = LocalMockInboundCardPaymentGatewayApplication.class
        , exclude = {UserDetailsServiceAutoConfiguration.class
})
@Import({MockWebSecurityConfigurationCore.class, MockInboundCardPaymentGatewayConfiguration.class, CoreGatewayConfiguration.class})
@EnableSpringDataWebSupport
@EntityScan(basePackageClasses = PaymentProcessor.class)
@EnableJpaRepositories(basePackageClasses = PaymentProcessorRepository.class)
public class LocalMockInboundCardPaymentGatewayApplication extends SpringApplication{

    @Setter
    private LocalMockInboundCardPaymentGatewayConfig config;

    public LocalMockInboundCardPaymentGatewayApplication(){
        super(LocalMockInboundCardPaymentGatewayApplication.class);
    }

    public static void main(String[] args) throws Exception {
        LocalMockInboundCardPaymentGatewayConfig config = LocalConfig.parseArgs(LocalMockInboundCardPaymentGatewayConfig::new,args);
        LocalMockInboundCardPaymentGatewayApplication application = new LocalMockInboundCardPaymentGatewayApplication();
        application.setConfig(config);
        application.run(args);
    }
    @Override
    protected void configurePropertySources(ConfigurableEnvironment environment, String[] args) {
        MutablePropertySources sources = environment.getPropertySources();
        sources.addFirst(new MapPropertySource("commandLineProperties", this.config.getProperties()));
    }

    @Override
    public ConfigurableApplicationContext run(String... args) {
        if(config.getApp().getPidFile() != null){
            addListeners(new ApplicationPidFileWriter(config.getApp().getPidFile()));
        }
        if(config.getApp().getPortFile() != null){
            addListeners(new WebServerPortFileWriter(config.getApp().getPortFile()));
        }
        Set<String> profiles = config.getProfiles();
        setAdditionalProfiles(profiles.toArray(new String[0]));
        return super.run(args);
    }


}
