package gov.scot.payments.gateway.inbound.cardpayment.mock.spring;

import gov.scot.payments.common.gateway.inbound.cardpayment.CardPaymentGateway;
import gov.scot.payments.gateway.inbound.cardpayment.GatewayCardPaymentService;
import gov.scot.payments.gateway.inbound.cardpayment.mock.MockInboundCardPaymentGateway;
import gov.scot.payments.gateway.inbound.cardpayment.spring.AbstractCardPaymentGatewayConfiguration;
import gov.scot.payments.model.paymentinstruction.PaymentChannelDirection;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.web.bind.annotation.RequestMapping;

@Configuration
public class MockInboundCardPaymentGatewayConfiguration extends AbstractCardPaymentGatewayConfiguration {

    @Value("${payments.gateway.name}")
    String name;

    @Bean
    public MockInboundCardPaymentGateway mockGateway(@Value("${payments.gateway.submitExpression}") String submit
            , @Value("${payments.gateway.authExpression}") String auth){
        ExpressionParser mockGatewayExpressionParser = new SpelExpressionParser();
        Expression submitExpression = mockGatewayExpressionParser.parseExpression(submit);
        Expression authExpression = mockGatewayExpressionParser.parseExpression(auth);
        return new MockInboundCardPaymentGateway(name,authExpression, submitExpression);
    }

    @Override
    protected String getGatewayName() {
        return name;
    }

    @Bean
    public MockInboundGatewayPaymentService mockInboundGatewayPaymentService(@Qualifier("mockGateway") CardPaymentGateway gateway){
        return new MockInboundGatewayPaymentService(gateway);
    }

    @RequestMapping(value = "/${payments.gateway.name}")
    public class MockInboundGatewayPaymentService extends GatewayCardPaymentService {
        public MockInboundGatewayPaymentService(CardPaymentGateway gateway) {
            super(gateway);
        }
    }

}
