package gov.scot.payments.gateway.inbound.cardpayment.mock;


import gov.scot.payments.common.gateway.spring.CoreGatewayConfiguration;
import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.gateway.inbound.cardpayment.mock.spring.MockInboundCardPaymentGatewayConfiguration;
import gov.scot.payments.model.paymentinstruction.PaymentProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

@SpringBootApplication(scanBasePackageClasses = MockInboundCardPaymentGatewayApplication.class
        ,exclude = {UserDetailsServiceAutoConfiguration.class
})
@Import({MockInboundCardPaymentGatewayConfiguration.class, CoreGatewayConfiguration.class})
@EnableSpringDataWebSupport
@EntityScan(basePackageClasses = PaymentProcessor.class)
@EnableJpaRepositories(basePackageClasses = PaymentProcessorRepository.class)
public class MockInboundCardPaymentGatewayApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(MockInboundCardPaymentGatewayApplication.class);
        application.run(args);
    }
}