package gov.scot.payments.gateway.inbound.cardpayment.mock;

import gov.scot.payments.common.gateway.inbound.cardpayment.CardPaymentGateway;
import gov.scot.payments.model.cardpayment.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.expression.Expression;

import java.util.Collections;
import java.util.UUID;

import static org.springframework.util.StringUtils.isEmpty;

public class MockInboundCardPaymentGateway implements CardPaymentGateway {

    private final Expression submitExpression;
    private final Expression authExpression;
    private final String name;

    public MockInboundCardPaymentGateway(String name
            , Expression authExpression
            , Expression submitExpression) {
        this.name = name;
        this.authExpression = authExpression;
        this.submitExpression = submitExpression;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public CardPaymentGatewayAuthResponse authorize(CardPaymentGatewayAuthRequest gatewayRequest) {
        String result = authExpression.getValue(gatewayRequest,String.class);
        if(Boolean.parseBoolean(result)){
            return CardPaymentGatewayAuthResponse.builder()
                    .ref(name+"_"+ UUID.randomUUID().toString())
                    .details(CardDetails.builder()
                            .expiry(gatewayRequest.getExpiry())
                            .issuer("big bank")
                            .maskedCardNumber(maskCardNumber(gatewayRequest.getCardNumber()))
                            .name(gatewayRequest.getName())
                            .type("Visa")
                            .build())
                    .amount(gatewayRequest.getAmount())
                    .metadata(Collections.singletonMap("token",gatewayRequest.getPaymentRef().toString()))
                    .build();
        } else{
            return CardPaymentGatewayAuthResponse.builder()
                    .status(CardPaymentStatus.Failed)
                    .message("failed to authorize")
                    .build();
        }
    }

    @Override
    public CardPaymentGatewaySubmitResponse submit(CardPaymentGatewaySubmitRequest gatewayRequest) {
        String result = submitExpression.getValue(gatewayRequest,String.class);
        if(Boolean.parseBoolean(result)){
            return CardPaymentGatewaySubmitResponse.builder()
                    .ref(gatewayRequest.getRef())
                    .metadata(gatewayRequest.getMetadata())
                    .build();
        } else{
            return CardPaymentGatewaySubmitResponse.builder()
                    .status(CardPaymentStatus.Failed)
                    .message("failed to submit")
                    .build();
        }
    }

    @Override
    public CardPaymentGatewayCancelResponse cancel(CardPaymentGatewayCancelRequest gatewayRequest) {
        return CardPaymentGatewayCancelResponse.builder()
                .metadata(gatewayRequest.getMetadata())
                .ref(gatewayRequest.getRef())
                .build();
    }

    private String maskCardNumber(String card) {
        if(isEmpty(card)){
            return "";
        }
        String toMask = card.replaceAll("\\s","");
        int start = 1;
        int end = 12;
        if( end > toMask.length() ){
            end = toMask.length();
        }
        int maskLength = end - start;
        if(maskLength <= 0){
            return toMask;
        }
        String strMaskString = StringUtils.repeat("*", maskLength);
        return StringUtils.overlay(toMask, strMaskString, start, end);
    }
}
