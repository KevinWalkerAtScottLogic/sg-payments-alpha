package gov.scot.payments.gateway.outbound.bottomline.restclient;

import gov.scot.payments.gateway.outbound.bottomline.model.*;
import gov.scot.payments.model.paymentinstruction.*;
import io.micrometer.core.instrument.util.IOUtils;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class BottomLineClientTest {

    private String TEST_PROFILE_ID="0001";
    private String TEST_FPS_PROFILE_ID="0001";

    BottomLineAPI mockCaller;
    BottomLineClient bottomLineClient;
    AuthToken authToken;

    String jsonBatchResponse = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("batchResponse.json"));


    private class AuthTokenSupplierTest implements AuthTokenSupplier {

        @Override
        public AuthToken fromAdminCredentials(BottomLineAuth auth) {
            return AuthToken.builder().jsessionId("test").bottomLineToken("testtoken").xcsrfToken("xcrftest").build();
        }
    }

    @BeforeEach
    void setUp() throws BottomLineAuthException, BottomLineCreateBatchException, BottomLineAddPaymentException, BottomLineSubmitBatchException {
        mockCaller = Mockito.mock(BottomLineAPI.class);
        var authTokenSupplier = new AuthTokenSupplierTest();
        var auth = new BottomLineAuth("test", "testpwd");
        var bacsProfile = BottomLineProfile.builder().auth(auth).id(TEST_PROFILE_ID).build();
        var fpProfile = BottomLineProfile.builder().auth(auth).id(TEST_FPS_PROFILE_ID).build();
        var profileSelector = BottomlineProfileFactory.builder().bacsProfile(bacsProfile).fpsProfile(fpProfile).build();
        bottomLineClient = new BottomLineClient(profileSelector::getProfile, mockCaller, authTokenSupplier);
        this.authToken = authTokenSupplier.fromAdminCredentials(null);
        doAnswer(i -> i.getArgument(0,BottomLineBatch.class).toBuilder().status(BottomLineBatchStatus.Draft).submission(123).enteredDate(LocalDateTime.now()).id(456).build()).when(mockCaller).createBatch(any(), eq(authToken));
        doAnswer(i -> i.getArgument(0,BottomLinePaymentInstruction.class)).when(mockCaller).addInstruction(any(), eq(authToken));
        doAnswer(i -> Optional.of(i.getArgument(0,BottomLineBatch.class).toBuilder().status(BottomLineBatchStatus.Committed).build().getInfo())).when(mockCaller).enterBatch(any(),anyString(), eq(authToken));
    }

    @Test
    void submitBatch() throws BottomLineSubmitBatchException, BottomLineAuthException, BottomLineCreateBatchException, BottomLineAddPaymentException {
        PaymentInstruction payment = getPaymentInstruction("0.01");
        BottomLineBatch bottomLineBatch = createBatch("123", payment);

        var output = bottomLineClient.submitBatch(PaymentChannel.Bacs, bottomLineBatch);

        assertEquals("123",output.getDescription());
        assertEquals(TEST_PROFILE_ID,output.getApplication());
        assertEquals(1,output.getEntries().size());
        assertEquals(BottomLineBatchStatus.Committed,output.getStatus());
        assertEquals(1,output.getEntries().size());
        assertTrue(output.getEntries().get(0).isAccepted());
        verify(mockCaller, times(1)).createBatch(any(), any());
        verify(mockCaller, times(1)).addInstruction(any(), any());
        verify(mockCaller, times(1)).enterBatch(any(), eq(TEST_PROFILE_ID), any());
    }



    @Test
    void submitBatchWithPaymentError() throws BottomLineSubmitBatchException, BottomLineAuthException, BottomLineAddPaymentException, BottomLineCreateBatchException {
        String paymentValue = "0.01";
        PaymentInstruction payment = getPaymentInstruction(paymentValue);
        PaymentInstruction payment2 = getPaymentInstruction("10.01");
        BottomLineBatch bottomLineBatch = createBatch("123", payment,payment2);

        when(mockCaller.addInstruction(any(), eq(authToken)))
                .thenAnswer(i -> i.getArgument(0,BottomLinePaymentInstruction.class))
                .thenThrow(new BottomLineAddPaymentException("error"));


        var output = bottomLineClient.submitBatch(PaymentChannel.Bacs, bottomLineBatch);
        assertEquals("123",output.getDescription());
        assertEquals(TEST_PROFILE_ID,output.getApplication());
        assertEquals(BottomLineBatchStatus.Committed,output.getStatus());
        assertEquals(2,output.getEntries().size());
        assertTrue(output.getEntries().get(0).isAccepted());
        assertFalse(output.getEntries().get(1).isAccepted());
        verify(mockCaller, times(1)).createBatch(any(), any());
        verify(mockCaller, times(2)).addInstruction(any(), any());
        verify(mockCaller, times(1)).enterBatch(any(), eq(TEST_PROFILE_ID), any());

    }

    @Test
    void submitBatchApproveError() throws BottomLineSubmitBatchException, BottomLineCreateBatchException, BottomLineAddPaymentException {

        PaymentInstruction payment = getPaymentInstruction("0.01");
        PaymentInstruction payment2 = getPaymentInstruction("10.01");
        BottomLineBatch bottomLineBatch = createBatch("123", payment,payment2);

        doThrow(new BottomLineSubmitBatchException("error approve batch")).when(mockCaller).enterBatch(any(), eq(TEST_PROFILE_ID), eq(authToken));

        assertThrows( BottomLineSubmitBatchException.class, () -> bottomLineClient.submitBatch(PaymentChannel.Bacs, bottomLineBatch));

        verify(mockCaller, times(1)).createBatch(any(), any());
        verify(mockCaller, times(2)).addInstruction(any(), any());
        verify(mockCaller, times(1)).enterBatch(any(), eq(TEST_PROFILE_ID), any());

    }

    @Test
    void submitFasterPaymentsBatch() throws BottomLineSubmitBatchException, BottomLineAuthException, BottomLineCreateBatchException, BottomLineAddPaymentException {

        PaymentInstruction payment = getPaymentInstruction("0.01");
        BottomLineBatch bottomLineBatch = createBatch("123", payment);

        var output = bottomLineClient.submitBatch(PaymentChannel.FasterPayments, bottomLineBatch);

        assertEquals("123",output.getDescription());
        assertEquals(TEST_FPS_PROFILE_ID,output.getApplication());
        assertEquals(1,output.getEntries().size());
        assertEquals(BottomLineBatchStatus.Committed,output.getStatus());
        assertEquals(1,output.getEntries().size());
        assertTrue(output.getEntries().get(0).isAccepted());
        verify(mockCaller, times(1)).createBatch(any(), any());
        verify(mockCaller, times(1)).addInstruction(any(), any());
        verify(mockCaller, times(1)).enterBatch(any(), eq(TEST_FPS_PROFILE_ID), any());

    }

    @Test
    void submitBatchWithNoSuccessfulPayments() throws BottomLineSubmitBatchException, BottomLineAuthException, BottomLineAddPaymentException, BottomLineCreateBatchException {
        String paymentValue = "0.01";
        PaymentInstruction payment = getPaymentInstruction(paymentValue);
        PaymentInstruction payment2 = getPaymentInstruction("10.01");
        BottomLineBatch bottomLineBatch = createBatch("123", payment,payment2);

        when(mockCaller.addInstruction(any(), eq(authToken)))
                .thenThrow(new BottomLineAddPaymentException("error"));


        var output = bottomLineClient.submitBatch(PaymentChannel.Bacs, bottomLineBatch);
        assertEquals("123",output.getDescription());
        assertEquals(TEST_PROFILE_ID,output.getApplication());
        assertEquals(BottomLineBatchStatus.Draft,output.getStatus());
        assertEquals(2,output.getEntries().size());
        assertFalse(output.getEntries().get(0).isAccepted());
        assertFalse(output.getEntries().get(1).isAccepted());
        verify(mockCaller, times(1)).createBatch(any(), any());
        verify(mockCaller, times(2)).addInstruction(any(), any());
        verify(mockCaller, times(0)).enterBatch(any(), eq(TEST_PROFILE_ID), any());

    }


    private PaymentInstruction getPaymentInstruction(String paymentValue) {

        var r = Recipient.builder().accountNumber(new AccountNumber("00000000"))
                .name("TESTRecipient").ref("REF23")
                .sortCode(new SortCode("000000")).build();

        return PaymentInstruction.builder()
                .recipient(r)
                .targetChannelType(PaymentChannelType.EFT)
                .latestTargetSettlementDate(LocalDate.ofEpochDay(LocalDate.now().toEpochDay() + 5))
                .targetAmount(Money.of(new BigDecimal(paymentValue), "GBP"))
                .service("service1")
                .routing(Routing.builder().channel(PaymentChannel.Bacs).processor("bottomline-proc").build())
                .build();
    }

    private BottomLineBatch createBatch(String name, PaymentInstruction... payment) {
        PaymentInstructionBatch batch = PaymentInstructionBatch.builder()
                .payments(Arrays.asList(payment))
                .name(name)
                .build();
        return BottomLineBatch.fromBatch(batch);
    }

}