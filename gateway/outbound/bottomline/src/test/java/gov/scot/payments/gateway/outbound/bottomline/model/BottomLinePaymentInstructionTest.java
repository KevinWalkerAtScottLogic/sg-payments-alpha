package gov.scot.payments.gateway.outbound.bottomline.model;

import gov.scot.payments.model.paymentinstruction.*;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BottomLinePaymentInstructionTest {



    private PaymentInstruction getTestPaymentInstruction(){

        var r = Recipient.builder().accountNumber(new AccountNumber("00000000"))
                .name("test").ref("ref12")
                .sortCode(new SortCode("000000")).build();

        // creating UUID
        UUID uid = UUID.fromString("abc00000-def0-11bd-b23e-10b96e4ef00d");

        PaymentChannel paymentChannel = PaymentChannel.Bacs;
        return PaymentInstruction.builder()
                    .id(uid)
                    .recipient(r)
                    .targetChannelType(PaymentChannelType.EFT)
                    .latestTargetSettlementDate(LocalDate.ofEpochDay(LocalDate.now().toEpochDay()+3))
                    .targetAmount(Money.of(new BigDecimal("0.01"),"GBP"))
                    .service("service1")
                    .routing(Routing.builder().channel(paymentChannel).processor("bottomline").build())
                    .build();

    }


    @BeforeEach
    void setUp() {

    }

    @Test
    void testPaymentInstructionWhenLowerCaseName() {

        var test = getTestPaymentInstruction();
        test = test.toBuilder()
                .recipient(test.getRecipient().toBuilder()
                        .name("test recipient 123")
                        .ref("My.Ref. Test_19")
                        .build())
                .build();

        var blPayment = BottomLinePaymentInstruction.fromPaymentInstruction(test);

        assertPaymentsEqual(test, blPayment);

        assertEquals(blPayment.getReference(), "MY.REF. TEST19");
        assertEquals(blPayment.getName(), "TEST RECIPIENT 123");
    }

    private void assertPaymentsEqual(PaymentInstruction test, BottomLinePaymentInstruction blPayment) {
        var latestDateTime = test.getLatestTargetSettlementDate().atTime(LocalTime.MAX);

        assertEquals(blPayment.getText(), "SERVICE1");
        assertEquals(blPayment.getPaymentDate(), latestDateTime);
        assertEquals(blPayment.getAccountNumber(), test.getRecipient().getAccountNumber().toString());
        assertEquals(blPayment.getSortCode(), test.getRecipient().getSortCode().toString());
        assertEquals(blPayment.getAmount(), test.getTargetAmount().getNumberStripped());
        assertEquals(blPayment.getTransactionType(), "0");
        assertEquals(blPayment.getIncluded(), true);
    }

    @Test
    void testPaymentInstructionWhenBatchIdGiven() {

        var test = getTestPaymentInstruction();
        var blPayment = BottomLinePaymentInstruction.fromPaymentInstruction(test).withBatch(23);

        assertEquals(blPayment.getBatchId(), 23);

    }
}