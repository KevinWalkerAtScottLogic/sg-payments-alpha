package gov.scot.payments.gateway.outbound.bottomline;

import gov.scot.payments.AbstractKafkaTest;
import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.common.kafka.KafkaNullConfiguration;
import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.gateway.outbound.bottomline.model.BottomLineBatch;
import gov.scot.payments.gateway.outbound.bottomline.model.BottomLineBatchStatus;
import gov.scot.payments.gateway.outbound.bottomline.model.BottomLinePaymentInstruction;
import gov.scot.payments.gateway.outbound.bottomline.restclient.*;
import gov.scot.payments.gateway.outbound.bottomline.spring.BottomlineGatewayConfiguration;
import gov.scot.payments.gateway.outbound.spring.CoreOutboundGatewayBinding;
import gov.scot.payments.gateway.outbound.spring.OutboundGatewayInputBinding;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingSuccessEvent;
import gov.scot.payments.model.paymentinstruction.*;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@EnableAutoConfiguration(exclude = UserDetailsServiceAutoConfiguration.class)
@EmbeddedKafka(partitions = 1
        ,topics = {"test-bottomlinePaymentEvents"
        ,"test-paymentSubmitSuccessEvents"
        ,"test-paymentSubmitFailedEvents"
        ,"test-paymentAcceptedEvents"
        ,"test-paymentRejectedEvents"
        ,"test-paymentClearedEvents"
        ,"test-paymentFailedToClearEvents"
        ,"test-paymentSettledEvents"})
@SpringBootTest(classes = BottomlineGatewayIntegrationTest.TestConfiguration.class
        , properties = {"payments.environment=test"
        ,"payments.gateway.name=bottomline"
        ,"spring.cloud.stream.bindings.bottomline-events.destination=test-bottomlinePaymentEvents"
        ,"spring.cloud.stream.bindings.bottomline-events.binder=kafka"
        ,"payments.gateway.submitExpression=T(java.time.Duration).ofSeconds(1)"
        ,"payments.gateway.acceptExpression=T(java.time.Duration).ofSeconds(1)"
        ,"payments.gateway.clearExpression=T(java.time.Duration).ofSeconds(1)"
        ,"spring.cloud.zookeeper.discovery.instance-port=80"
        ,"payments.gateway.bacs.maxBatchSize=2"
        ,"payments.gateway.bacs.inactivityDuration=PT3S"
        ,"payments.gateway.bacs.inactivityResolution=PT1S"
        ,"payments.gateway.fp.maxBatchSize=3"
        ,"payments.gateway.fp.inactivityDuration=PT3S"
        ,"payments.gateway.fp.inactivityResolution=PT1S"
        ,"bottomline.maxBatchCount=2"
        ,"bottomline.maxTransactionValue=0.03"
        ,"bottomline.maxBatchValue=0.05"
        ,"bottomline.allowedServices=service,service1"
        ,"bottomline.period=PT5M"
        ,"bottomline.maxTotalCount=50"
        ,"bottomline.maxTotalValue=100"})
@Tag("kafka")
public class BottomlineGatewayIntegrationTest extends AbstractKafkaTest {

    @Autowired
    @Qualifier("bottomline-events")
    private MessageChannel bottomlineEvents;

    @Autowired
    private BottomLineClient bottomLineClient;

    @Autowired
    private BottomlineOutboundGateway gateway;

    @BeforeEach
    public void setUp() throws Exception{
        super.setUp();
        doAnswer(i -> submitBatch(i.getArgument(1, BottomLineBatch.class)))
                .when(bottomLineClient).submitBatch(any(), any());
        gateway.init();
    }

    @Test
    void testGateway() throws BottomLineSubmitBatchException, BottomLineAuthException, BottomLineCreateBatchException {

        PaymentInstruction payment = createPaymentInstruction("0.01");

        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment)));
        sleep(Duration.ofSeconds(6));
        //Mockito.verify(bottomLineClient, times(1)).submitBatch(eq(PaymentChannel.Bacs), any());
        verifyEvents("test-paymentSubmitSuccessEvents",l -> Assertions.assertEquals(1,l.size()));
    }

    @Test
    void testGatewayOnExceptionThrown() throws BottomLineSubmitBatchException, BottomLineAuthException, BottomLineCreateBatchException {

        String targetValue = "0.01";

        PaymentInstruction payment = createPaymentInstruction(targetValue);

        doThrow(new BottomLineSubmitBatchException("Invalid batch submission")).when(bottomLineClient).submitBatch(any(), any());

        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment)));
        sleep(Duration.ofSeconds(6));
        //Mockito.verify(bottomLineClient, times(1)).submitBatch(eq(PaymentChannel.Bacs), any());
        verifyEvents("test-paymentSubmitFailedEvents",l -> Assertions.assertEquals(1,l.size()));
    }

    @Test
    void testGatewayOnFailedPayments() throws BottomLineSubmitBatchException, BottomLineAuthException, BottomLineCreateBatchException{

        PaymentInstruction payment = createPaymentInstruction("0.01");
        PaymentInstruction payment2 = createPaymentInstruction("0.01");

        doAnswer(i -> submitBatchWithFailure(i.getArgument(1, BottomLineBatch.class),1)).when(bottomLineClient).submitBatch(any(), any());


        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment)));
        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment2)));
        sleep(Duration.ofSeconds(6));
       // Mockito.verify(bottomLineClient, times(1)).submitBatch(eq(PaymentChannel.Bacs), any());
        verifyEvents("test-paymentSubmitSuccessEvents",l -> Assertions.assertEquals(1,l.size()));
        verifyEvents("test-paymentSubmitFailedEvents",l -> Assertions.assertEquals(1,l.size()));
    }

    @Test
    void testGatewayRoutingToFasterPayments() throws BottomLineSubmitBatchException, BottomLineAuthException, BottomLineCreateBatchException {

        PaymentInstruction bacsPayment = createPaymentInstruction("0.01");
        PaymentInstruction payment = bacsPayment.toBuilder()
                .routing(bacsPayment.getRouting().toBuilder().channel(PaymentChannel.FasterPayments).build())
                .build();
        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment)));
        sleep(Duration.ofSeconds(5));
        //Mockito.verify(bottomLineClient, times(1)).submitBatch(eq(PaymentChannel.FasterPayments), any());
        verifyEvents("test-paymentSubmitSuccessEvents",l -> Assertions.assertEquals(1,l.size()));
    }

    @Test
    void testTooLargeBatch() throws BottomLineSubmitBatchException, BottomLineAuthException, BottomLineCreateBatchException {

        PaymentInstruction payment = createPaymentInstruction("0.01");
        PaymentInstruction payment2 = createPaymentInstruction("0.01");
        PaymentInstruction payment3 = createPaymentInstruction("0.01");

        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment.toBuilder()
                .routing(payment.getRouting().toBuilder().channel(PaymentChannel.FasterPayments).build())
                .build())));
        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment2.toBuilder()
                .routing(payment2.getRouting().toBuilder().channel(PaymentChannel.FasterPayments).build())
                .build())));
        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment3.toBuilder()
                .routing(payment3.getRouting().toBuilder().channel(PaymentChannel.FasterPayments).build())
                .build())));
        sleep(Duration.ofSeconds(6));
        //Mockito.verify(bottomLineClient, times(1)).submitBatch(eq(PaymentChannel.Bacs), any());
        verifyEvents("test-paymentSubmitFailedEvents",l -> Assertions.assertEquals(3,l.size()));
        verifyEvents("test-paymentSubmitSuccessEvents",l -> Assertions.assertEquals(0,l.size()));
    }

    @Test
    void testTooHighValueBatch() throws BottomLineSubmitBatchException, BottomLineAuthException, BottomLineCreateBatchException {

        PaymentInstruction payment = createPaymentInstruction("0.03");
        PaymentInstruction payment2 = createPaymentInstruction("0.03");

        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment)));
        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment2)));
        sleep(Duration.ofSeconds(6));
        //Mockito.verify(bottomLineClient, times(1)).submitBatch(eq(PaymentChannel.Bacs), any());
        verifyEvents("test-paymentSubmitFailedEvents",l -> Assertions.assertEquals(2,l.size()));
        verifyEvents("test-paymentSubmitSuccessEvents",l -> Assertions.assertEquals(0,l.size()));
    }

    @Test
    void testTooLargeTransaction() throws BottomLineSubmitBatchException, BottomLineAuthException, BottomLineCreateBatchException {

        PaymentInstruction payment = createPaymentInstruction("0.05");

        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment)));
        sleep(Duration.ofSeconds(6));
        //Mockito.verify(bottomLineClient, times(1)).submitBatch(eq(PaymentChannel.Bacs), any());
        verifyEvents("test-paymentSubmitFailedEvents",l -> Assertions.assertEquals(1,l.size()));
        verifyEvents("test-paymentSubmitSuccessEvents",l -> Assertions.assertEquals(0,l.size()));
    }

    @Test
    void testInvalidService() throws BottomLineSubmitBatchException, BottomLineAuthException, BottomLineCreateBatchException {

        PaymentInstruction payment = createPaymentInstruction("0.05").toBuilder().service("service2").build();

        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment)));
        sleep(Duration.ofSeconds(6));
        //Mockito.verify(bottomLineClient, times(1)).submitBatch(eq(PaymentChannel.Bacs), any());
        verifyEvents("test-paymentSubmitFailedEvents",l -> Assertions.assertEquals(1,l.size()));
        verifyEvents("test-paymentSubmitSuccessEvents",l -> Assertions.assertEquals(0,l.size()));
    }

    @Test
    void testSubmittedCountTooMany() throws BottomLineSubmitBatchException, BottomLineAuthException, BottomLineCreateBatchException {
        BottomLineBatch batch = BottomLineBatch.builder()
                .creditCount(49)
                .creditTotal(new BigDecimal("90"))
                .submission(123)
                .enteredDate(LocalDateTime.now().minusSeconds(1))
                .build();
        doReturn(Collections.singletonList(batch)).when(bottomLineClient).queryBatches(eq(PaymentChannel.Bacs),any(),any(),anyInt());
        gateway.init();
        PaymentInstruction payment = createPaymentInstruction("0.02");
        PaymentInstruction payment2 = createPaymentInstruction("0.02");
        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment)));
        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment2)));
        sleep(Duration.ofSeconds(6));
        //Mockito.verify(bottomLineClient, times(1)).submitBatch(eq(PaymentChannel.Bacs), any());
        verifyEvents("test-paymentSubmitFailedEvents",l -> Assertions.assertEquals(2,l.size()));
        verifyEvents("test-paymentSubmitSuccessEvents",l -> Assertions.assertEquals(0,l.size()));
    }

    @Test
    void testSubmittedTooLarge() throws BottomLineSubmitBatchException, BottomLineAuthException, BottomLineCreateBatchException {
        BottomLineBatch batch = BottomLineBatch.builder()
                .creditCount(40)
                .creditTotal(new BigDecimal("99.99"))
                .submission(123)
                .enteredDate(LocalDateTime.now().minusSeconds(1))
                .build();
        doReturn(Collections.singletonList(batch)).when(bottomLineClient).queryBatches(eq(PaymentChannel.Bacs),any(),any(),anyInt());
        gateway.init();
        PaymentInstruction payment = createPaymentInstruction("0.02");
        PaymentInstruction payment2 = createPaymentInstruction("0.02");
        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment)));
        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment2)));
        sleep(Duration.ofSeconds(6));
        //Mockito.verify(bottomLineClient, times(1)).submitBatch(eq(PaymentChannel.Bacs), any());
        verifyEvents("test-paymentSubmitFailedEvents",l -> Assertions.assertEquals(2,l.size()));
        verifyEvents("test-paymentSubmitSuccessEvents",l -> Assertions.assertEquals(0,l.size()));
    }

    @Test
    void testCacheEviction() throws BottomLineSubmitBatchException, BottomLineAuthException, BottomLineCreateBatchException {
        BottomLineBatch batch1 = BottomLineBatch.builder()
                .creditCount(70)
                .creditTotal(new BigDecimal("10"))
                .submission(123)
                .enteredDate(LocalDateTime.now().minusMinutes(10))
                .build();
        BottomLineBatch batch2 = BottomLineBatch.builder()
                .creditCount(40)
                .creditTotal(new BigDecimal("99.95"))
                .submission(123)
                .enteredDate(LocalDateTime.now())
                .build();
        doReturn(List.of(batch1,batch2)).when(bottomLineClient).queryBatches(eq(PaymentChannel.Bacs),any(),any(),anyInt());
        gateway.init();
        PaymentInstruction payment = createPaymentInstruction("0.02");
        PaymentInstruction payment2 = createPaymentInstruction("0.02");
        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment)));
        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment2)));
        sleep(Duration.ofSeconds(6));
        //Mockito.verify(bottomLineClient, times(1)).submitBatch(eq(PaymentChannel.Bacs), any());
        verifyEvents("test-paymentSubmitFailedEvents",l -> Assertions.assertEquals(0,l.size()));
        verifyEvents("test-paymentSubmitSuccessEvents",l -> Assertions.assertEquals(2,l.size()));
    }

    private PaymentInstruction createPaymentInstruction(String targetValue) {
        return PaymentInstruction.builder()
                .batchId("1")
                .recipient(Recipient.builder()
                        .accountNumber(new AccountNumber("00000000"))
                        .sortCode(new SortCode("000000"))
                        .name("test test")
                        .ref("ref")
                        .build())
                .targetChannelType(PaymentChannelType.EFT)
                .latestTargetSettlementDate(LocalDate.now())
                .targetAmount(Money.of(new BigDecimal(targetValue), "GBP"))
                .service("service1")
                .routing(Routing.builder()
                        .channel(PaymentChannel.Bacs)
                        .processor("bottomline")
                        .queue("test-bottomlinePaymentEvents")
                        .build())
                .build();
    }


    private BottomLineBatch submitBatch(BottomLineBatch argument) {
        return argument.toBuilder()
                .enteredDate(LocalDateTime.now())
                .status(BottomLineBatchStatus.Committed)
                .id(123)
                .submission(456)
                .description("456")
                .applicationName("test")
                .application("123")
                .entries(argument.getEntries().stream().map(p -> p.withAcceptance(p.getPaymentInstruction())).collect(Collectors.toList()))
                .creditCount(argument.getEntries().size())
                .creditTotal(argument.getEntries().stream().map(BottomLinePaymentInstruction::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add))
                .build();
    }

    private BottomLineBatch submitBatchWithFailure(BottomLineBatch argument, int index) {
        List<BottomLinePaymentInstruction> entries = argument.getEntries().stream().map(p -> p.withAcceptance(p.getPaymentInstruction())).collect(Collectors.toList());
        entries.set(index,argument.getEntries().get(index).withError(new BottomLineAddPaymentException("error")));
        return argument.toBuilder()
                .enteredDate(LocalDateTime.now())
                .status(BottomLineBatchStatus.Committed)
                .id(123)
                .submission(456)
                .description("456")
                .applicationName("test")
                .application("123")
                .entries(entries)
                .creditCount(argument.getEntries().size())
                .creditTotal(argument.getEntries().stream().map(BottomLinePaymentInstruction::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add))
                .build();
    }


    @Import({OptionalResponseControllerAdvice.class, KafkaNullConfiguration.class, BottomlineGatewayConfiguration.class})
    @EnableBinding({ CoreOutboundGatewayBinding.class, OutboundGatewayInputBinding.class, TestBinding.class})
    @EnableSpringDataWebSupport
    @EntityScan(basePackageClasses = PaymentProcessor.class)
    @EnableJpaRepositories(basePackageClasses = PaymentProcessorRepository.class)
    public static class TestConfiguration extends AbstractKafkaTest.TestConfiguration {

        @Bean
        @Primary
        public BottomLineClient mockBottomLineClient() throws BottomLineAuthException {
            BottomLineClient client = mock(BottomLineClient.class);

            BottomLineBatch batch = BottomLineBatch.builder()
                    .creditCount(0)
                    .creditTotal(new BigDecimal("0.1"))
                    .submission(123)
                    .enteredDate(LocalDateTime.now().minusSeconds(1))
                    .build();
            when(client.queryBatches(eq(PaymentChannel.Bacs),any(),any(),anyInt())).thenReturn(Collections.singletonList(batch));
            return client;

        }
    }

    public interface TestBinding {

        @Output("bottomline-events")
        MessageChannel bottomlineEvents();

    }
}
