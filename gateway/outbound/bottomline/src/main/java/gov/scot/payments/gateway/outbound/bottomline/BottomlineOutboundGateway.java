package gov.scot.payments.gateway.outbound.bottomline;

import gov.scot.payments.gateway.outbound.AbstractOutboundGateway;
import gov.scot.payments.gateway.outbound.PaymentInstructionBatcher;
import gov.scot.payments.gateway.outbound.bottomline.model.*;
import gov.scot.payments.gateway.outbound.bottomline.restclient.*;
import gov.scot.payments.gateway.outbound.bottomline.spring.BottomlineProperties;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingSuccessEvent;
import gov.scot.payments.model.paymentinstruction.*;
import gov.scot.payments.model.paymentinstruction.event.PaymentSubmitFailedEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentSubmitSuccessEvent;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.SendTo;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BottomlineOutboundGateway extends AbstractOutboundGateway<String,PaymentChannel> {

    private final BottomLineClient bottomLineClient;
    private final BottomlineProperties properties;

    private SortedMap<BottomLineSubmittedBatchKey, BottomLineBatchInfo> submittedBatches;

    public BottomlineOutboundGateway(MessageChannel acceptedEvents
            , MessageChannel rejectedEvents
            , MessageChannel clearedEvents
            , MessageChannel failedToClearEvents
            , MessageChannel settledEvents
            , BottomLineClient bottomLineClient
            , BottomlineProperties properties
            , PaymentInstructionBatcher... batchers) {
        super(acceptedEvents, rejectedEvents, clearedEvents, failedToClearEvents, settledEvents, batchers);
        this.bottomLineClient = bottomLineClient;
        this.properties = properties;
    }

    public void init(){
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime start = now.minus(properties.getPeriod());
        this.submittedBatches = Stream.of(PaymentChannel.Bacs,PaymentChannel.FasterPayments)
                .flatMap(p -> {
                    try {
                        return this.bottomLineClient.queryBatches(p,start,now,1000).stream();
                    } catch (Exception e) {
                        return Stream.empty();
                    }
                })
                .filter(b -> b.getStatus() != BottomLineBatchStatus.Rejected && b.getStatus() != BottomLineBatchStatus.Archived)
                .map(BottomLineBatch::getInfo)
                .collect(Collectors.toMap(BottomLineBatchInfo::batchKey
                        ,bi -> bi
                        ,(v1,v2) ->{ throw new RuntimeException(String.format("Duplicate key for values %s and %s", v1, v2));}
                        , TreeMap::new));


    }

    @StreamListener
    @SendTo({"payment-submit-success-events","payment-submit-failure-events"})
    public KStream<?, ?>[] handleGatewayPaymentEvents(@Input("payment-events") KStream<?, PaymentRoutingSuccessEvent> events) {
        return handlePaymentEvents(events);
    }

    @Override
    protected PaymentChannel convertPartitionKey(String key) {
        return PaymentChannel.valueOf(key);
    }

    @Override
    protected List<Event<PaymentInstruction>> submitPayments(PaymentChannel channel, PaymentInstructionBatch batch) {
        try {
            filterBatch(batch);
            var instructionsSubmitted = bottomLineClient.submitBatch(channel, BottomLineBatch.fromBatch(batch));
            updateCache(instructionsSubmitted);
            return instructionsSubmitted.getEntries()
                    .stream()
                    .map(p -> handleSubmit(p,channel))
                    .collect(Collectors.toList());
        } catch (BottomLineAuthException e) {
            log.error("Error authenticating with bottomline for batch {}",batch,e);
            return handleBatchError(channel,batch,e);
        } catch (BottomLineSubmitBatchException e) {
            log.error("Error submitting bottomline batch {}",batch,e);
            return handleBatchError(channel,batch,e);
        } catch (BottomLineCreateBatchException e) {
            log.error("Error creating bottomline batch {}",batch,e);
            return handleBatchError(channel,batch,e);
        } catch (BottomLineBatchValidationException e) {
            log.error("Error validating bottomline batch {}: {}",batch,e.getMessage());
            return handleBatchError(channel,batch,e);
        } catch (IllegalArgumentException e) {
            log.error("Bottomline can not handle channel {}",channel);
            return handleBatchError(channel,batch,e);
        }
    }

    private synchronized void updateCache(BottomLineBatch submitted) {
        BottomLineBatchInfo info = submitted.getAcceptedInfo();
        submittedBatches.put(info.batchKey(),info);
    }

    private synchronized void filterBatch(PaymentInstructionBatch batch) throws BottomLineBatchValidationException{
        if(!batch.allMatch(pi -> pi.getTargetAmount().getCurrency().equals(PaymentInstruction.GBP_CURRENCY))){
            throw new BottomLineBatchValidationException(String.format("Batch %s contains a non GBP transaction",batch));
        }

        if(!batch.allMatch(pi -> pi.getTargetAmount().getNumberStripped().compareTo(properties.getMaxTransactionValue()) <= 0)){
            throw new BottomLineBatchValidationException(String.format("Batch %s contains a transaction with a value > %s",batch,properties.getMaxTransactionValue().toPlainString()));
        }

        if(!batch.allMatch(pi -> properties.getAllowedServices().contains(pi.getService()))){
            throw new BottomLineBatchValidationException(String.format("Batch %s contains a transaction for an invalid service. Only %s services are allowed",batch, String.join(",", properties.getAllowedServices())));
        }

        if(batch.size() > properties.getMaxBatchCount()){
            throw new BottomLineBatchValidationException(String.format("Batch %s contains %s transactions, only %s allowed",batch,batch.size(),properties.getMaxBatchCount()));
        }

        if(batch.getTotalTargetValue().compareTo(properties.getMaxBatchValue()) > 0){
            throw new BottomLineBatchValidationException(String.format("Batch %s is too high value, only batches < %s are allowed",batch,properties.getMaxBatchValue().toPlainString()));
        }

        removeOldEntriesFromCache();

        Tuple2<Integer,BigDecimal> submitted = submittedBatches
                .values()
                .stream()
                .map(b -> Tuples.of(b.getCreditCount(),b.getCreditTotal()))
                .reduce(Tuples.of(0, BigDecimal.ZERO), (v1,v2) -> Tuples.of(v1.getT1()+v2.getT1(),v1.getT2().add(v2.getT2())));

        int proposedSubmissions = submitted.getT1() + batch.size();
        BigDecimal proposedSubmittedValue = submitted.getT2().add(batch.getTotalTargetValue());

        if(proposedSubmissions > properties.getMaxTotalCount()){
            throw new BottomLineBatchValidationException(String.format("Submission count for period %s would be breached by batch %s",properties.getMaxTotalCount(),batch));
        }
        if(proposedSubmittedValue.compareTo(properties.getMaxTotalValue()) > 0){
            throw new BottomLineBatchValidationException(String.format("Submission value %s for period would be breached by batch %s",properties.getMaxTotalValue().toPlainString(),batch));
        }
    }

    private void removeOldEntriesFromCache() {
        LocalDateTime now = LocalDateTime.now().minus(properties.getPeriod());
        SortedMap<BottomLineSubmittedBatchKey, BottomLineBatchInfo> toRemove = submittedBatches.headMap(new BottomLineSubmittedBatchKey(now,null));
        toRemove.clear();
    }

    private Event<PaymentInstruction> handleSubmit(BottomLinePaymentInstruction payment, PaymentChannel channel) {
        if(payment.isAccepted()){
            return new PaymentSubmitSuccessEvent(payment.toPaymentInstruction(channel,getName()));
        } else{
            return new PaymentSubmitFailedEvent(payment.toPaymentInstruction(channel,getName()));
        }
    }

    private List<Event<PaymentInstruction>> handleBatchError(PaymentChannel channel, PaymentInstructionBatch batch, Throwable error) {
        LocalDateTime now = LocalDateTime.now();
        return batch.getPayments()
                .stream()
                .map(payment -> payment.toBuilder()
                        .status(PaymentInstructionStatus.SubmissionFailed)
                        .addExecution(Execution.builder()
                                .executedAt(now)
                                .executedAmount(payment.getTargetAmount())
                                .channel(channel)
                                .processor(getName())
                                .status(ExecutionStatus.SubmissionFailed)
                                .statusMessage(String.format("Bottomline submission failed. Details: %s", error.getMessage()))
                                .build())
                        .timestamp(now)
                        .build())
                .map(PaymentSubmitFailedEvent::new)
                .collect(Collectors.toList());
    }

    @Override
    public String getName() {
        return "bottomline";
    }
}
