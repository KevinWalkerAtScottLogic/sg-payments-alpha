package gov.scot.payments.gateway.outbound.bottomline.restclient;

import gov.scot.payments.gateway.outbound.bottomline.model.BottomLineBatch;
import lombok.*;

import java.util.List;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PACKAGE)
@Getter
public class BottomLineQueryResponse {

    private List<BottomLineQueryResponseRow> rows;

    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PUBLIC)
    @Setter(value = AccessLevel.PACKAGE)
    @Getter
    public static class BottomLineQueryResponseRow {

        private List<BottomLineQueryResponseRowValue> values;

    }

    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PUBLIC)
    @Setter(value = AccessLevel.PACKAGE)
    @Getter
    public static class BottomLineQueryResponseRowValue {

        private List<BottomLineQueryResponseRowResultValue> resultValues;
    }

    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PUBLIC)
    @Setter(value = AccessLevel.PACKAGE)
    @Getter
    public static class BottomLineQueryResponseRowResultValue {

        private BottomLineSubmission value;
    }

    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PUBLIC)
    @Setter(value = AccessLevel.PACKAGE)
    @Getter
    public static class BottomLineSubmission extends BottomLineBatch {

        private List<BottomLineBatch> batches;
    }
}
