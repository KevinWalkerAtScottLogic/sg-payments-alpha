package gov.scot.payments.gateway.outbound.bottomline.model;

public enum BottomLineBatchStatus {

    Committed, //committed
    Approved, //approved
    Entered, //entered
    Draft, //draft
    Rejected, //rejected
    Archived//archived
}
