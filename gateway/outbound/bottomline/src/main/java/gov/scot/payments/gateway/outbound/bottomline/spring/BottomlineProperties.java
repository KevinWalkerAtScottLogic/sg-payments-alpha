package gov.scot.payments.gateway.outbound.bottomline.spring;

import gov.scot.payments.gateway.outbound.bottomline.model.BottomLineProfile;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Collections;
import java.util.Set;

@ConfigurationProperties(prefix = "bottomline")
@Data
public class BottomlineProperties {

    private int maxRetries = 3;
    private int retryWait = 10000;
    private BigDecimal maxTransactionValue = new BigDecimal("0.01");
    private BigDecimal maxBatchValue = new BigDecimal("0.1");
    private int maxBatchCount = 10;
    private BigDecimal maxTotalValue = BigDecimal.ONE;
    private int maxTotalCount = 100;
    private Duration period = Duration.ofDays(1);
    private Set<String> allowedServices = Collections.emptySet();
    private String apiEndpoint;
    private BottomLineProfile bacsProfile;
    private BottomLineProfile fpProfile;
}
