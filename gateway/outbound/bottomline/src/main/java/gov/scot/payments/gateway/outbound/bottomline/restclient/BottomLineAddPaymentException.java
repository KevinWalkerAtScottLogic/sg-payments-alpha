package gov.scot.payments.gateway.outbound.bottomline.restclient;

public class BottomLineAddPaymentException extends BottomLineException {

    public BottomLineAddPaymentException(String message) {
        super(message);
    }

}
