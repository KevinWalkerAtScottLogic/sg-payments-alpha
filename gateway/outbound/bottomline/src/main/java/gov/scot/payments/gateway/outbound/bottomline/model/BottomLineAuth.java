package gov.scot.payments.gateway.outbound.bottomline.model;

import lombok.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter
@Getter
public class BottomLineAuth {

    private static final String AUTH_BODY_TEMPLATE = "{\n" +
            "\"loginTokens\": [{\n" +
            "\"key\": \"com.bottomline.security.provider.login.email\",\n" +
            "\"value\": \"%s\"\n" +
            "}, {\n" +
            "\"key\": \"com.bottomline.security.provider.login.password\",\n" +
            "\"value\": \"%s\"\n" +
            "}],\n" +
            "\"apiVersion\": {\"major\": \"1\",\"minor\": \"0\",\"patch\": \"0\",\"build\": \"0\"},\n" +
            "\"purpose\": \"cpay-auth\",\n" +
            "\"tokenLocation\": \"HEADER\"\n" +
            "}";


    @NonNull private String username;
    @NonNull private String password;

    public HttpEntity<String> toAuthRequest(String jsessionId, String xcsrfToken){
        HttpHeaders headersAuth = new HttpHeaders();
        headersAuth.add("User-Agent", "Java/1.7.0_51");
        headersAuth.add("Content-Type", "application/json");
        headersAuth.add("Cookie", jsessionId);
        headersAuth.add("X-CSRF", xcsrfToken);

        return new HttpEntity<>(String.format(AUTH_BODY_TEMPLATE,username,password), headersAuth);
    }


}
