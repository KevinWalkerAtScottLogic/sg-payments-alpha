package gov.scot.payments.gateway.outbound.bottomline.restclient;

public class BottomLineCreateBatchException extends BottomLineException {

    public BottomLineCreateBatchException(String message) {
        super(message);
    }

}
