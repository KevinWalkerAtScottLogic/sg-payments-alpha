package gov.scot.payments.gateway.outbound.bottomline.restclient;

public class BottomLineSubmitBatchException extends BottomLineException {

    public BottomLineSubmitBatchException(String message) {
        super(message);
    }

    public BottomLineSubmitBatchException(String message, Throwable cause) {
        super(message, cause);
    }

}
