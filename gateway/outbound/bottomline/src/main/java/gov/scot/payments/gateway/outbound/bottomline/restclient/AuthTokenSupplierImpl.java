package gov.scot.payments.gateway.outbound.bottomline.restclient;

import gov.scot.payments.gateway.outbound.bottomline.model.BottomLineAuth;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.RetryOperations;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestOperations;

@Slf4j
class AuthTokenSupplierImpl implements AuthTokenSupplier {

    private final String apiEndPointBase;

    private static final String BOTTOMLINE_API_AUTHENTICATION = "/security/handshake";
    private static final String BOTTOMLINE_API_AUTHENTICATION_POST = "/security/login";

    private final RestOperations restOps;
    private final RetryOperations retryOps;

    public AuthTokenSupplierImpl(RestOperations restOperations, RetryOperations retryOperations, String apiEndPoint) {
        this.restOps = restOperations;
        this.retryOps = retryOperations;
        this.apiEndPointBase = apiEndPoint;
    }

    @Override
    public AuthToken fromAdminCredentials(BottomLineAuth bottomLineAuth) throws BottomLineAuthException {
        ResponseEntity<String> response = callHandshakeEndpoint();

        String xcsrfToken = getXcsrfToken(response);
        String jsessionId = getJsessionIdToken(response);

        ResponseEntity<String> authResponse = getAuthenticationResponse(bottomLineAuth,jsessionId,xcsrfToken);

        xcsrfToken =  getXcsrfToken(authResponse);
        String bottomLineToken = getBottomlineAuthToken(authResponse);

        return new AuthToken(jsessionId, xcsrfToken, bottomLineToken);
    }

    private String getXcsrfToken(ResponseEntity<String> response) {
        return response.getHeaders().get("X-CSRF").get(0);
    }

    private String getJsessionIdToken(ResponseEntity<String> response) {
        String jsessionID = "";
        String cookieString = response.getHeaders().get("Set-Cookie").get(0);
        var strings = cookieString.split(";");
        for(String s : strings){
            if(s.startsWith("JSESSIONID")){
                jsessionID = s+";";
            }
        }
        return jsessionID;
    }

    private String getBottomlineAuthToken(ResponseEntity<String> authResponse) {
        return authResponse.getHeaders().get("com.bottomline.auth.token").get(0);
    }

    private ResponseEntity<String> getAuthenticationResponse(BottomLineAuth bottomLineAuth, String jsessionId, String xcsrfToken) throws BottomLineAuthException {
        try {
            return retryOps.execute( c -> restOps.postForEntity(apiEndPointBase+BOTTOMLINE_API_AUTHENTICATION_POST, bottomLineAuth.toAuthRequest(jsessionId, xcsrfToken), String.class));
        }catch(HttpStatusCodeException e){
            log.warn("Could not get authentication response ", e.getResponseBodyAsString());
            throw new BottomLineAuthException("Could not create payment - got response "+e.getStatusCode());
        }
    }

    private ResponseEntity<String> callHandshakeEndpoint() throws BottomLineAuthException {

        try {
            return retryOps.execute(c -> restOps.getForEntity(apiEndPointBase+BOTTOMLINE_API_AUTHENTICATION, String.class));
        } catch(HttpStatusCodeException e){
            log.warn("Could not connect to bottomline for handshake");
            throw new BottomLineAuthException("Could not connect to bottomline for handshake");
        }
    }

}
