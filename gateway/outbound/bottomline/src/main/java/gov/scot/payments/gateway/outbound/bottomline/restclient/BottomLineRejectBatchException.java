package gov.scot.payments.gateway.outbound.bottomline.restclient;

public class BottomLineRejectBatchException extends BottomLineException {

    public BottomLineRejectBatchException(String message) {
        super(message);
    }

    public BottomLineRejectBatchException(String message, Throwable cause) {
        super(message, cause);
    }

}
