package gov.scot.payments.gateway.outbound.bottomline.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@Builder
public class BottomLineBatchInfo {

    private int id;
    private int submission;
    @JsonDeserialize(using = BottomLineDateDeserializer.class)
    private LocalDateTime enteredDate;
    @JsonProperty("statusDescription")
    private BottomLineBatchStatus status;
    private BigDecimal creditTotal;
    private int creditCount;

    public BottomLineSubmittedBatchKey batchKey() {
        return new BottomLineSubmittedBatchKey(enteredDate,id);
    }
}
