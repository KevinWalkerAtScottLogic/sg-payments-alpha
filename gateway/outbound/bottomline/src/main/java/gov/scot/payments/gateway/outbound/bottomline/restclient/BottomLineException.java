package gov.scot.payments.gateway.outbound.bottomline.restclient;

public class BottomLineException extends Exception {

    public BottomLineException(String message) {
        super(message);
    }

    public BottomLineException(String message, Throwable cause) {
        super(message, cause);
    }

}
