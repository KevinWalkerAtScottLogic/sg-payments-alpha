package gov.scot.payments.gateway.outbound.bottomline.restclient;

import gov.scot.payments.gateway.outbound.bottomline.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.retry.RetryOperations;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestOperations;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class BottomLineAPI {

    private static final String QUERY_ENDPOINT = "/query/execute";
    private static final String BATCH_ENDPOINT = "/bacs/batches";
    private static final String INSTRUCTIONS_ENDPOINT = "/bacs/instructions";
    private static final String SET_STATUS_ENDPOINT = "/bacs/submissions/%s/%s";
    private static final String SET_STATUS_ENDPOINT_WITH_PROFILE = "/bacs/submissions/%s/%s/%s";

    private final RestOperations restOps;
    private final RetryOperations retryOps;
    private String apiEndpointBase;


    public BottomLineAPI(RestOperations operations, RetryOperations retryOperations, String apiEndpoint){
        this.restOps = operations;
        this.retryOps = retryOperations;
        apiEndpointBase = apiEndpoint;
    }

    public BottomLineBatch createBatch(BottomLineBatch batch, AuthToken authenticator) throws BottomLineCreateBatchException {

        HttpHeaders headers = authenticator.getAuthHttpHeaders();
        HttpEntity<BottomLineBatch> entity = new HttpEntity<>(batch, headers);

        try{
            BottomLineBatch body = retryOps.execute(c -> restOps.postForEntity(apiEndpointBase + BATCH_ENDPOINT, entity, BottomLineBatch.class)).getBody();
            return batch.toBuilder()
                    .id(body.getId())
                    .enteredDate(body.getEnteredDate())
                    .status(BottomLineBatchStatus.Draft)
                    .submission(body.getSubmission())
                    .build();
        }catch(HttpStatusCodeException e) {
            var errorMessage = BottomLineErrorMessage.fromResponseJson(e.getResponseBodyAsString());
            log.warn("Error creating batch {}", errorMessage);
            throw new BottomLineCreateBatchException("Error creating batch: "+ errorMessage);
        }

    }

    public  Optional<BottomLineBatchInfo>  enterBatch(BottomLineBatch bottomLineBatch, String profileId,  AuthToken authenticator) throws BottomLineSubmitBatchException {
        return setBatchStatus(bottomLineBatch.getSubmission(),profileId,"enter",authenticator, BottomLineSubmitBatchException::new);
    }

    public  Optional<BottomLineBatchInfo>  uncommitBatch(int submission, String profileId, AuthToken authenticator) throws BottomLineUncommitBatchException{
        return setBatchStatus(submission,null,"uncommit",authenticator, BottomLineUncommitBatchException::new);
    }

    public  Optional<BottomLineBatchInfo>  unapproveBatch(int submission, String profileId, AuthToken authenticator) throws BottomLineUnapproveBatchException{
        return setBatchStatus(submission,null,"unapprove",authenticator, BottomLineUnapproveBatchException::new);
    }

    public  Optional<BottomLineBatchInfo>  rejectBatch(int submission, String profileId, AuthToken authenticator) throws BottomLineRejectBatchException{
        return setBatchStatus(submission,null,"reject",authenticator, BottomLineRejectBatchException::new);
    }

    public List<BottomLineBatch> queryBatchesBetween(LocalDateTime from, LocalDateTime to, int limit, AuthToken authenticator) {
        BottomLineQuery query = BottomLineQuery.submissionsBetween(from,to, limit);
        return queryBatches(query,authenticator);
    }

    public Optional<BottomLineBatch> getBatch(int id, AuthToken authenticator) {
        BottomLineQuery query = BottomLineQuery.batchId(id);
        HttpHeaders headers = authenticator.getAuthHttpHeaders();
        HttpEntity<BottomLineQuery> entity = new HttpEntity<>(query,headers);
        BottomLineQueryResponse body = retryOps.execute(c -> restOps.postForEntity(apiEndpointBase + QUERY_ENDPOINT, entity, BottomLineQueryResponse.class)).getBody();
        //BottomLineQueryResponse body = null;
        return Optional.ofNullable(body)
                .map(BottomLineQueryResponse::getRows)
                .filter(Objects::nonNull)
                .flatMap(r -> r.stream().findFirst())
                .map(r -> r.getValues())
                .filter(Objects::nonNull)
                 .flatMap(v -> v.stream().findFirst())
                .map(rv -> rv.getResultValues())
                .filter(Objects::nonNull)
                .flatMap(rrv -> rrv.stream().findFirst())
                .filter(Objects::nonNull)
                .map(rv -> rv.getValue())
                .map(s -> s.getBatches())
                .filter(Objects::nonNull)
                .flatMap(b -> b.stream().findFirst());
    }

    private List<BottomLineBatch> queryBatches(BottomLineQuery query, AuthToken authenticator) {
        HttpHeaders headers = authenticator.getAuthHttpHeaders();
        HttpEntity<BottomLineQuery> entity = new HttpEntity<>(query,headers);
        BottomLineQueryResponse body = retryOps.execute(c -> restOps.postForEntity(apiEndpointBase + QUERY_ENDPOINT, entity, BottomLineQueryResponse.class)).getBody();
        //BottomLineQueryResponse body = null;
        return body.getRows()
                .stream()
                .flatMap(r -> r.getValues().stream())
                .flatMap(rv -> rv.getResultValues().stream())
                .map(rrv -> rrv.getValue())
                .flatMap(s -> {
                    if(s.getBatches() == null){
                        return Stream.of(s);
                    } else{
                        return s.getBatches().stream();
                    }
                })
                .collect(Collectors.toList());

    }

    public BottomLinePaymentInstruction addInstruction(BottomLinePaymentInstruction instruction, AuthToken authenticator) throws BottomLineAddPaymentException {
        HttpHeaders headers = authenticator.getAuthHttpHeaders();
        HttpEntity<BottomLinePaymentInstruction> entity = new HttpEntity<>(instruction,headers);
        try {
            BottomLineAddPaymentResponse body = retryOps.execute(c -> restOps.postForEntity(apiEndpointBase + INSTRUCTIONS_ENDPOINT, entity, BottomLineAddPaymentResponse.class)).getBody();
            return body.getModel();
        } catch(HttpStatusCodeException e) {
            var errorMessage = BottomLineErrorMessage.fromResponseJson(e.getResponseBodyAsString());
            log.warn("Error creating payment instruction {}", errorMessage);
            throw new BottomLineAddPaymentException("Error creating payment instruction: "+ errorMessage);
        }

    }

    private <T extends BottomLineException> Optional<BottomLineBatchInfo> setBatchStatus(int submission, String profileId, String status, AuthToken authenticator, BiFunction<String,Exception,T> exceptionSupplier) throws T {
        HttpHeaders headers = authenticator.getAuthHttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>("",headers);

        String url;
        if(profileId == null){
            url = String.format(apiEndpointBase+SET_STATUS_ENDPOINT, submission, status);
        } else{
            url = String.format(apiEndpointBase+SET_STATUS_ENDPOINT_WITH_PROFILE, submission, status,profileId);
        }

        try {
            log.info("Calling {} to {} bottomline batch " , url,status);
            retryOps.execute(c ->restOps.postForEntity(url, entity,BottomLineBatchInfo.class));
            return getBatch(submission,authenticator).map(BottomLineBatch::getInfo);
        } catch(HttpStatusCodeException e) {
            throw exceptionSupplier.apply(String.format("Could not %s batch %s",status,submission), e);
        }

    }

}
