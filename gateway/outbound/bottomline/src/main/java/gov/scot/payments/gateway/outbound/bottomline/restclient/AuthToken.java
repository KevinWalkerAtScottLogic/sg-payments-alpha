package gov.scot.payments.gateway.outbound.bottomline.restclient;

import lombok.Builder;
import lombok.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@Value
@Builder
public class AuthToken {

    private String jsessionId;
    private String xcsrfToken;
    private String bottomLineToken;

    HttpHeaders getAuthHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie", this.jsessionId);
        headers.add("X-CSRF", this.xcsrfToken);
        headers.add("com.bottomline.auth.token", this.bottomLineToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

}
