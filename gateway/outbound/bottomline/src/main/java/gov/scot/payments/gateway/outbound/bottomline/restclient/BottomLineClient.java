package gov.scot.payments.gateway.outbound.bottomline.restclient;

import gov.scot.payments.gateway.outbound.bottomline.model.*;
import gov.scot.payments.model.paymentinstruction.*;
import lombok.extern.slf4j.Slf4j;
import org.javamoney.moneta.Money;
import org.springframework.retry.RetryOperations;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.ExpressionRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
public class BottomLineClient {

    private final Function<PaymentChannel,BottomLineProfile> profileSelector;

    private final BottomLineAPI api;
    private final AuthTokenSupplier authTokenSupplier;

    public BottomLineClient(Function<PaymentChannel,BottomLineProfile> profileSelector,
                            BottomLineAPI api,
                            AuthTokenSupplier supplier) {
        this.profileSelector = profileSelector;
        this.api = api;
        this.authTokenSupplier = supplier;
    }


    public BottomLineClient(Function<PaymentChannel,BottomLineProfile> profileSelector,
                            RestOperations restOperations,
                            RetryOperations retryOperations,
                            String apiEndPoint) {
        api = new BottomLineAPI(restOperations, retryOperations, apiEndPoint);
        authTokenSupplier = new AuthTokenSupplierImpl(restOperations, retryOperations, apiEndPoint);
        this.profileSelector = profileSelector;
    }

    public synchronized BottomLineBatch submitBatch(PaymentChannel channel, BottomLineBatch batch) throws BottomLineCreateBatchException,BottomLineSubmitBatchException, BottomLineAuthException {

        if(batch.getEntries().size() == 0){
            return batch;
        }

        var profile = profileSelector.apply(channel);
        AuthToken authToken = authTokenSupplier.fromAdminCredentials(profile.getAuth());
        var batchToSubmit = BottomLineBatch.builder()
                .application(profile.getId())
                .applicationDescription(profile.getDescription())
                .applicationName(profile.getDescription())
                .description(batch.getDescription())
                .build();
        var createdBatch = api.createBatch(batchToSubmit, authToken);

        var submittedBatch = createdBatch
                .toBuilder()
                .entries(batch.getEntries().stream()
                        .map(i -> addInstructionToBatch(createdBatch.getId(), i, authToken))
                        .collect(Collectors.toList()))
                .build();

        if(!submittedBatch.getAcceptedEntries().isEmpty()){
            try{
                Optional<BottomLineBatchInfo> batchInfoOpt = api.enterBatch(createdBatch, profile.getId(), authToken);
                BottomLineBatchInfo batchInfo = batchInfoOpt.orElseThrow(() -> new BottomLineSubmitBatchException("Could not enter batch"));
                return submittedBatch.toBuilder()
                        .status(batchInfo.getStatus())
                        .build();
            } catch(BottomLineSubmitBatchException e) {
                deleteBatch(submittedBatch,profile.getId(), authToken);
                throw e;
            }

        } else{
            deleteBatch(submittedBatch,profile.getId(), authToken);
            return submittedBatch;
        }
    }

    public synchronized List<BottomLineBatch> queryBatches(PaymentChannel channel,LocalDateTime from, LocalDateTime to, int limit) throws BottomLineAuthException {
        var profile = profileSelector.apply(channel);
        AuthToken authToken = authTokenSupplier.fromAdminCredentials(profile.getAuth());
        return api.queryBatchesBetween(from,to,limit, authToken)
                .stream()
                .filter(b -> b.getApplication().equals(profile.getId()))
                .collect(Collectors.toList());
    }

    public synchronized Optional<BottomLineBatch> getBatch(PaymentChannel channel,int batchId) throws BottomLineAuthException {
        var profile = profileSelector.apply(channel);
        AuthToken authToken = authTokenSupplier.fromAdminCredentials(profile.getAuth());
        return api.getBatch(batchId, authToken);
    }

    private void deleteBatch(BottomLineBatch batch, String profileId,AuthToken authToken) {
        BottomLineBatchInfo info = batch.getInfo();
        Optional<BottomLineBatchInfo> batchToUse = Optional.of(info);
        int initialStatusOrdinal = info.getStatus().ordinal();
        int rejectedOrdinal = BottomLineBatchStatus.Rejected.ordinal();
        int attempts = 0;
        while(batchToUse.isPresent() && attempts < rejectedOrdinal - initialStatusOrdinal  && batchToUse.get().getStatus().ordinal() < rejectedOrdinal){
            try{
                batchToUse = decrementBatchStatus(batchToUse.get(), profileId, authToken);
                attempts++;
            } catch(Exception e){
                return;
            }
        }
    }

    private Optional<BottomLineBatchInfo>decrementBatchStatus(BottomLineBatchInfo info, String profileId, AuthToken authToken) throws BottomLineException {
        switch (info.getStatus()) {
            case Committed:
                return api.uncommitBatch(info.getSubmission(),profileId,authToken);
            case Approved:
                return api.unapproveBatch(info.getSubmission(),profileId,authToken);
            case Entered:
            case Draft:
                return api.rejectBatch(info.getSubmission(),profileId,authToken);
            case Rejected:
            case Archived:
            default:
                return Optional.of(info);
        }
    }

    private BottomLinePaymentInstruction addInstructionToBatch(int batchId, BottomLinePaymentInstruction instruction, AuthToken auth) {
        var withBatch = instruction.withBatch(batchId);
        try {
            var resp = api.addInstruction(withBatch, auth);
            log.info("Added bottomline instruction {} to batch {}", withBatch, batchId);
            return resp.withAcceptance(withBatch.getPaymentInstruction());
        }catch (BottomLineAddPaymentException e){
            log.warn("Could not add bottomline instruction {} to batch {}", withBatch, batchId,e);
            return withBatch.withError(e);
        }
    }

    public static void main(String[] args) throws BottomLineSubmitBatchException,BottomLineCreateBatchException, BottomLineAuthException {

        String username = System.getenv("PTX_USER");
        String password = System.getenv("PTX_PASSWORD");
        var auth = new BottomLineAuth(username,password);
        String apiEndpoint = System.getenv("PTX_ENDPOINT");

        RetryTemplate retryTemplate = new RetryTemplate();
        RestTemplate restTemplate = new RestTemplate();
        FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
        backOffPolicy.setBackOffPeriod(1000);
        retryTemplate.setBackOffPolicy(backOffPolicy);
        var expressionRetryPolicy = new ExpressionRetryPolicy("#{statusCode.value() == 503}");
        expressionRetryPolicy.setMaxAttempts(10);
        retryTemplate.setRetryPolicy(expressionRetryPolicy);

        BottomLineProfile bacsProfile = BottomLineProfile.builder().id("9084").auth(auth).build();
        var profileSelector = BottomlineProfileFactory.builder().bacsProfile(bacsProfile).fpsProfile(BottomLineProfile.builder().id("9085").auth(auth).build()).build();

        BottomLineClient blc = new BottomLineClient(profileSelector::getProfile, restTemplate, retryTemplate, apiEndpoint);

        var r = Recipient.builder().accountNumber(new AccountNumber("00000000"))
                .name("John Smith").ref("REF/0000000")
                .sortCode(new SortCode("000000")).build();

        var paymentVals = List.of("0.01", "0.01");
        PaymentChannel paymentChannel = PaymentChannel.Bacs;

        var batch = PaymentInstructionBatch.builder()
                .name("007")
                .build();
        for(var value : paymentVals){
            PaymentInstruction payment = PaymentInstruction.builder()
                    .recipient(r)
                    .targetChannelType(PaymentChannelType.EFT)
                    .latestTargetSettlementDate(LocalDate.now().plusDays(6))
                    .targetAmount(Money.of(new BigDecimal(value),"GBP"))
                    .service("ILFService")
                    .routing(Routing.builder().channel(paymentChannel).processor("bottomline").build())
                    .build();
            batch = batch.addPayment(payment);

        }
        //blc.submitBatch(PaymentChannel.Bacs, BottomLineBatch.fromBatch(batch));
        BottomLineBatch b = blc.getBatch(paymentChannel,137707).get();
        blc.deleteBatch(b,bacsProfile.getId(),blc.authTokenSupplier.fromAdminCredentials(bacsProfile.getAuth()));
        //List<BottomLineBatch> info = blc.queryBatches(paymentChannel,LocalDateTime.now().minusDays(7),LocalDateTime.now(),100);
        //info.clear();

    }


}
