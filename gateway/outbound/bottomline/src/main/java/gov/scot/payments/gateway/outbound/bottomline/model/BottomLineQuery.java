package gov.scot.payments.gateway.outbound.bottomline.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;


@Value
@Builder
public class BottomLineQuery {

    private BottomLineQueryEntity entity;
    private BottomlineQueryCriteria criteria;
    private BottomLineQueryField resultFields;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private BottomLineResultsPage resultsPage;

    public static BottomLineQuery submissionsBetween(LocalDateTime from, LocalDateTime to, int limit) {
        BottomlineMultiSearchCriteria query = BottomlineMultiSearchCriteria.queryParam(BottomLineQueryField.submissionDate(),BottomLineQueryOperator.between(),BottomLineQueryValue.date(from),BottomLineQueryValue.date(to));
        return new BottomLineQuery(BottomLineQueryEntity.submissionInfo()
                ,new BottomlineQueryCriteria(query)
                ,BottomLineQueryField.submissionInfo()
                ,BottomLineResultsPage.limit(limit));
    }

    public static BottomLineQuery batchId(int id) {
        BottomlineSingleSearchCriteria query = BottomlineSingleSearchCriteria.queryParam(BottomLineQueryField.submissionId(),BottomLineQueryOperator.eq(),BottomLineQueryValue.integer(id));
        return new BottomLineQuery(BottomLineQueryEntity.submissionInfo()
                ,new BottomlineQueryCriteria(query)
                ,BottomLineQueryField.submissionInfo()
                ,null);
    }

    @Value
    private static class BottomLineResultsPage {
        private int firstResult;
        private int maxResults;

        public static BottomLineResultsPage limit(int limit){
            return new BottomLineResultsPage(0,limit);
        }
    }

    @Value
    private static class BottomLineQueryField {
        private String name;
        private String symbol;
        private String fieldType;
        private boolean key;

        public static BottomLineQueryField submissionDate(){
            return new BottomLineQueryField("enteredDate","com.bottomline.cpay.model.bacs.submission.enteredDate","DATE",false);
        }

        public static BottomLineQueryField submissionInfo(){
            return new BottomLineQueryField("Submission","com.bottomline.cpay.model.bacs.SubmissionResponse","OBJECT",false);
        }

        public static BottomLineQueryField submissionId() {
            return new BottomLineQueryField("id","com.bottomline.cpay.model.bacs.submission.id","INTEGER",false);
        }
    }

    @Value
    private static class BottomlineQueryCriteria {
        private List<BottomlineSearchCriteria> searchCriteria;

        public BottomlineQueryCriteria(BottomlineSearchCriteria... criteria){
            this.searchCriteria = Arrays.asList(criteria);
        }
    }

    @Value
    private static class BottomLineQueryEntity {
        private String name;
        private String symbol;
        private String key;

        public static BottomLineQueryEntity submissionInfo(){
            return new BottomLineQueryEntity("Submission","com.bottomline.cpay.model.bacs.submission","com.bottomline.cpay.model.bacs.submission");
        }
    }

    @AllArgsConstructor
    @Getter
    @JsonPropertyOrder("@type")
    private static class BottomlineSearchCriteria {
        @JsonProperty("@type")
        private String type;
        private BottomLineQueryField field;
        private BottomLineQueryOperator operator;
    }

    @Value
    @EqualsAndHashCode(callSuper = false)
    private static class BottomlineMultiSearchCriteria  extends BottomlineSearchCriteria{
        private List<BottomLineQueryValue> queryValues;

        public BottomlineMultiSearchCriteria(String type, BottomLineQueryField field, BottomLineQueryOperator operator, List<BottomLineQueryValue> queryValues) {
            super(type, field, operator);
            this.queryValues = queryValues;
        }

        public static BottomlineMultiSearchCriteria queryParam(BottomLineQueryField field, BottomLineQueryOperator operator, BottomLineQueryValue... values) {
            return new BottomlineMultiSearchCriteria("QueryParameter",field,operator,Arrays.asList(values));
        }
    }

    @Value
    @EqualsAndHashCode(callSuper = false)
    private static class BottomlineSingleSearchCriteria extends BottomlineSearchCriteria{
        private BottomLineQueryValue queryValue;

        public BottomlineSingleSearchCriteria(String type, BottomLineQueryField field, BottomLineQueryOperator operator, BottomLineQueryValue queryValue) {
            super(type, field, operator);
            this.queryValue = queryValue;
        }

        public static BottomlineSingleSearchCriteria queryParam(BottomLineQueryField field, BottomLineQueryOperator operator, BottomLineQueryValue value) {
            return new BottomlineSingleSearchCriteria("QueryParameter",field,operator,value);
        }
    }
    @Value
    private static class BottomLineQueryOperator {
        private String symbol;

        public static BottomLineQueryOperator between(){
            return new BottomLineQueryOperator("BETWEEN");
        }

        public static BottomLineQueryOperator eq() {
            return new BottomLineQueryOperator("=");
        }
    }

    @Value
    private static class BottomLineQueryValue {
        @JsonProperty("@type")
        private String type;
        @JsonProperty("$value")
        private Object value;

        public static BottomLineQueryValue date(LocalDateTime date){
            return new BottomLineQueryValue("date",date.toString());
        }

        public static BottomLineQueryValue integer(int id) {
            return new BottomLineQueryValue("integer",id);
        }
    }
}
