package gov.scot.payments.gateway.outbound.bottomline.spring;

import gov.scot.payments.common.gateway.spring.AbstractGatewayConfiguration;
import gov.scot.payments.gateway.outbound.PaymentInstructionBatcher;
import gov.scot.payments.gateway.outbound.PaymentInstructionBatchers;
import gov.scot.payments.gateway.outbound.bottomline.BottomlineOutboundGateway;
import gov.scot.payments.gateway.outbound.bottomline.restclient.BottomLineClient;
import gov.scot.payments.gateway.outbound.bottomline.restclient.BottomlineProfileFactory;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.stream.converter.CompositeMessageConverterFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.MessageChannel;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.ExpressionRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.Collection;


@Configuration
@EnableConfigurationProperties(BottomlineProperties.class)
public class BottomlineGatewayConfiguration extends AbstractGatewayConfiguration {

    @Bean
    BottomLineClient bottomLineClient(BottomlineProperties properties){
        RetryTemplate retryTemplate = new RetryTemplate();
        RestTemplate restTemplate = new RestTemplate();
        FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
        backOffPolicy.setBackOffPeriod(properties.getRetryWait());
        retryTemplate.setBackOffPolicy(backOffPolicy);
        var expressionRetryPolicy = new ExpressionRetryPolicy("#{statusCode.value() == 503}");
        expressionRetryPolicy.setMaxAttempts(properties.getMaxRetries());
        retryTemplate.setRetryPolicy(expressionRetryPolicy);
        var profileSelector = BottomlineProfileFactory.builder().bacsProfile(properties.getBacsProfile()).fpsProfile(properties.getFpProfile()).build();
        return new BottomLineClient(profileSelector::getProfile, restTemplate, retryTemplate, properties.getApiEndpoint());
    }


    @Bean
    PaymentInstructionBatcher fpBatcher(@Value("${payments.gateway.fp.maxBatchSize:10}") int maxBatchSize
            , @Value("${payments.gateway.fp.inactivityDuration:PT1M}") String duration
            , @Value("${payments.gateway.fp.inactivityResolution:PT1M}") String resolution
            , CompositeMessageConverterFactory compositeNonNativeSerde){
        return PaymentInstructionBatchers
                .<String,String>countWithTimeout(maxBatchSize,Duration.parse(duration),Duration.parse(resolution),false)
                .batchNameExtractor(PaymentInstructionBatchers.serviceBatchNaming())
                .branchPredicate(PaymentChannel.FasterPayments.name(), PaymentInstructionBatchers.channelTypePredicate())
                .groupingFunction(PaymentInstructionBatchers.groupedByService(compositeNonNativeSerde))
                .build();
    }

    @Bean
    PaymentInstructionBatcher bacsBatcher(@Value("${payments.gateway.bacs.maxBatchSize:10}") int maxBatchSize
            ,@Value("${payments.gateway.bacs.inactivityDuration:PT1M}") String duration
            , @Value("${payments.gateway.bacs.inactivityResolution:PT1M}") String resolution
            , CompositeMessageConverterFactory compositeNonNativeSerde){
        return PaymentInstructionBatchers
                .<String,String>countWithTimeout(maxBatchSize,Duration.parse(duration),Duration.parse(resolution),false)
                .batchNameExtractor(PaymentInstructionBatchers.serviceBatchNaming())
                .branchPredicate(PaymentChannel.Bacs.name(), PaymentInstructionBatchers.channelTypePredicate())
                .groupingFunction(PaymentInstructionBatchers.groupedByService(compositeNonNativeSerde))
                .build();
    }

    @Bean(initMethod = "init")
    public BottomlineOutboundGateway bottomlineGateway(@Qualifier("payment-accepted-events") MessageChannel acceptedEvents
            , @Qualifier("payment-rejected-events") MessageChannel rejectedEvents
            , @Qualifier("payment-cleared-events") MessageChannel clearedEvents
            , @Qualifier("payment-failedToClear-events") MessageChannel failedToClearEvents
            , @Qualifier("payment-settled-events") MessageChannel settledEvents
            , BottomLineClient bottomLineClient
            , Collection<PaymentInstructionBatcher> batchers
            , BottomlineProperties properties){
        return new BottomlineOutboundGateway(acceptedEvents
                ,rejectedEvents
                ,clearedEvents
                ,failedToClearEvents
                ,settledEvents
                ,bottomLineClient
                ,properties
                ,batchers.toArray(new PaymentInstructionBatcher[0]));
    }

    @Override
    protected String getGatewayName() {
        return "bottomline";
    }
}
