package gov.scot.payments.gateway.outbound.bottomline.restclient;

public class BottomLineUnapproveBatchException extends BottomLineException {

    public BottomLineUnapproveBatchException(String message) {
        super(message);
    }

    public BottomLineUnapproveBatchException(String message, Throwable cause) {
        super(message, cause);
    }

}
