package gov.scot.payments.gateway.outbound.bottomline.restclient;

import gov.scot.payments.gateway.outbound.bottomline.model.BottomLineAuth;

public interface AuthTokenSupplier {

    AuthToken fromAdminCredentials(BottomLineAuth auth) throws BottomLineAuthException;

}
