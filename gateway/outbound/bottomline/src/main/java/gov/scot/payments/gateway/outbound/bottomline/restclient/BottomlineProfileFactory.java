package gov.scot.payments.gateway.outbound.bottomline.restclient;


import gov.scot.payments.gateway.outbound.bottomline.model.BottomLineProfile;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import lombok.Builder;

@Builder
public class BottomlineProfileFactory {

    private BottomLineProfile bacsProfile;
    private BottomLineProfile fpsProfile;

    public BottomLineProfile getProfile(PaymentChannel channel) throws IllegalArgumentException {
        switch (channel) {
            case Bacs:
                return bacsProfile;
            case FasterPayments:
                return fpsProfile;
            case Chaps:
            case Cheque:
            case PostalOrder:
            default:
                throw new IllegalArgumentException("Could not find a valid profile for payment method "+ channel.name());
        }
    }

}
