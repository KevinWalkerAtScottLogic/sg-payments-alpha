package gov.scot.payments.gateway.outbound.bottomline.model;

import lombok.NonNull;
import lombok.Value;

import java.time.LocalDateTime;

@Value
public class BottomLineSubmittedBatchKey implements Comparable<BottomLineSubmittedBatchKey>{

    @NonNull private LocalDateTime submitted;
    private Integer id;

    @Override
    public int compareTo(BottomLineSubmittedBatchKey o) {
        return submitted.compareTo(o.submitted);
    }
}
