package gov.scot.payments.gateway.outbound;

import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.Cancellable;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Supplier;

public abstract class BaseTransformer<K> implements Transformer<K, PaymentInstruction, KeyValue<String, PaymentInstructionBatch>> {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    //TODO use a state store - https://stackoverflow.com/questions/53628143/how-to-process-a-kstream-in-a-batch-of-max-size-or-fallback-to-a-time-window
    protected final ConcurrentMap<K,PaymentInstructionBatch> currentBatches;
    protected ProcessorContext processorContext;

    public BaseTransformer() {
        this.currentBatches = new ConcurrentHashMap<>();
    }

    @Override
    public void init(ProcessorContext processorContext) {
        this.processorContext = processorContext;
    }

    @Override
    public final synchronized KeyValue<String, PaymentInstructionBatch> transform(K key, PaymentInstruction value) {
        PaymentInstructionBatch batch = currentBatches.compute(key, (k,v) -> {
            PaymentInstructionBatch b = v == null ? new PaymentInstructionBatch() : v;
            log.info("key: {}, Adding payment {} to batch {}",k,value,b);
            return b.addPayment(value);
        });
        handleBatch(key,batch,processorContext,currentBatches);
        return null;
    }

    protected void handleBatch(K key, PaymentInstructionBatch batch, ProcessorContext context, ConcurrentMap<K,PaymentInstructionBatch> currentBatches){

    }

    @Override
    public final void close() {

    }
}
