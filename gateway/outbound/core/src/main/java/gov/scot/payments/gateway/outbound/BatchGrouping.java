package gov.scot.payments.gateway.outbound;

import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.kstream.Grouped;

import java.util.function.Function;

@Value
@EqualsAndHashCode(callSuper = true)
public class BatchGrouping<K> extends BatchOperation<K,PaymentInstruction> implements Function<PaymentInstruction,K>{

    private Function<PaymentInstruction,K> function;

    public BatchGrouping(Serde<K> keySerde, Serde<PaymentInstruction> valueSerde, Function<PaymentInstruction, K> function) {
        super(keySerde, valueSerde);
        this.function = function;
    }

    @Override
    public K apply(PaymentInstruction paymentInstruction) {
        return function.apply(paymentInstruction);
    }

    public Grouped<K, PaymentInstruction> getGrouped() {
        return Grouped.with(keySerde, valueSerde);
    }
}
