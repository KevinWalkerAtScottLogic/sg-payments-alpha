package gov.scot.payments.gateway.outbound;

import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;

import java.time.Duration;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class PeriodicWithCountTransformer<K> extends CountTransformer<K> {

    private final Duration window;

    public PeriodicWithCountTransformer(Duration window, int maxCount) {
        super(maxCount);
        this.window = window;
    }

    @Override
    public void init(ProcessorContext processorContext) {
        super.init(processorContext);
        if(window != null){
            processorContext.schedule(window, PunctuationType.WALL_CLOCK_TIME, this::doPunctuate);
        }
    }

    private synchronized void doPunctuate(long timestamp) {
        Iterator<Map.Entry<K, PaymentInstructionBatch>> iterator = currentBatches.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<K, PaymentInstructionBatch> keyValue = iterator.next();
            processorContext.forward(keyValue.getKey(), keyValue.getValue());
            iterator.remove();
        }
    }

}
