package gov.scot.payments.gateway.outbound;

import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class CountWithTimeoutTransformer<K> extends CountTransformer<K>{

    private final Duration inactivityPeriod;
    private final Duration resolution;
    private final boolean useEarliestPayment;

    public CountWithTimeoutTransformer(Duration inactivityPeriod
            , int maxCount
            , Duration resolution
            , boolean useEarliestPayment) {
        super(maxCount);
        this.inactivityPeriod = inactivityPeriod;
        this.resolution = resolution;
        this.useEarliestPayment = useEarliestPayment;
    }

    @Override
    public void init(ProcessorContext processorContext) {
        super.init(processorContext);
        if(inactivityPeriod != null){
            processorContext.schedule(resolution, PunctuationType.WALL_CLOCK_TIME, this::doPunctuate);
        }
    }

    private synchronized void doPunctuate(long timestamp) {
        LocalDateTime currentTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneId.systemDefault());
        Iterator<Map.Entry<K, PaymentInstructionBatch>> iterator = currentBatches.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<K, PaymentInstructionBatch> keyValue = iterator.next();
            Optional<PaymentInstruction> paymentToUse = useEarliestPayment ? keyValue.getValue().getEarliestPayment() : keyValue.getValue().getLatestPayment();
            LocalDateTime timeToUse = paymentToUse
                    .map(PaymentInstruction::getTimestamp)
                    .orElse(LocalDateTime.MAX);
            if(Duration.between(timeToUse,currentTime).compareTo(inactivityPeriod) >= 0){
                processorContext.forward(keyValue.getKey(), keyValue.getValue());
                iterator.remove();
            }
        }
    }

}
