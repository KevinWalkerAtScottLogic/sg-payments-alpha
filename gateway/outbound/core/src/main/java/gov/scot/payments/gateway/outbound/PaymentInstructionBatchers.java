package gov.scot.payments.gateway.outbound;

import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import lombok.experimental.UtilityClass;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.springframework.cloud.stream.binder.kafka.streams.serde.CompositeNonNativeSerde;
import org.springframework.cloud.stream.converter.CompositeMessageConverterFactory;
import org.springframework.messaging.MessageHeaders;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.support.CronTrigger;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

@UtilityClass
public class PaymentInstructionBatchers {

    public Function<PaymentInstruction,String> channelTypePredicate(){
        return pi -> Optional.ofNullable(pi.getRouting())
                .map(r -> r.getChannel().name())
                .orElse("UNKNOWN_CHANNEL_TYPE");
    }

    public BatchGrouping<String> groupedByService(CompositeMessageConverterFactory valueSerde){
        return new BatchGrouping<>(Serdes.String(), createSerde(valueSerde,PaymentInstruction.class),PaymentInstruction::getService);
    }

    public BatchGrouping<String> noGrouping(CompositeMessageConverterFactory valueSerde){
        return new BatchGrouping<>(Serdes.String(),createSerde(valueSerde,PaymentInstruction.class),pi -> "DEFAULT_GROUP");
    }

    public BatchGrouping<String> groupedByBatchId(CompositeMessageConverterFactory valueSerde){
        return new BatchGrouping<>(Serdes.String(),createSerde(valueSerde,PaymentInstruction.class),pi -> Optional.ofNullable(pi.getBatchId()).orElse("NO_BATCH"));
    }

    public BatchGrouping<String> groupedByPaymentFile(CompositeMessageConverterFactory valueSerde){
        return new BatchGrouping<>(Serdes.String(),createSerde(valueSerde,PaymentInstruction.class),pi -> Optional.ofNullable(pi.getPaymentFile()).orElse("NO_PAYMENT_FILE"));
    }

    public Function<PaymentInstructionBatch,String> defaultBatchNaming(){
        return PaymentInstructionBatch::getName;
    }

    public Function<PaymentInstructionBatch,String> serviceBatchNaming(){
        return serviceBatchNaming(b -> "_"+b.getId().toString());
    }

    public Function<PaymentInstructionBatch,String> serviceBatchNaming(Function<PaymentInstructionBatch,String> suffixGenerator){
        return batch -> batch.getPayments()
                .stream()
                .findFirst()
                .map(PaymentInstruction::getService)
                .orElse(batch.getName())+suffixGenerator.apply(batch);
    }

    public Function<PaymentInstructionBatch,String> batchIdBatchNaming(){
        return batch -> batch.getPayments()
                .stream()
                .findFirst()
                .map(PaymentInstruction::getBatchId)
                .orElse(batch.getName());
    }

    public Function<PaymentInstructionBatch,String> paymentFileBatchNaming(){
        return batch -> batch.getPayments()
                .stream()
                .findFirst()
                .map(PaymentInstruction::getPaymentFile)
                .orElse(batch.getName());
    }

    public <K,PK> SessionWindowingPaymentInstructionBatcher.SessionWindowingPaymentInstructionBatcherBuilder<K,PK> session(Duration inactivity, Serde<K> keySerde, CompositeMessageConverterFactory valueSerde){
        BatchSessionWindowing<K> window = new BatchSessionWindowing<>(keySerde,createSerde(valueSerde,PaymentInstructionBatch.class),inactivity);
        return SessionWindowingPaymentInstructionBatcher.<K,PK>builder()
                .window(window);
    }

    public <K,PK> TimeWindowingPaymentInstructionBatcher.TimeWindowingPaymentInstructionBatcherBuilder<K,PK> time(Duration windowPeriod, Serde<K> keySerde, CompositeMessageConverterFactory valueSerde){
        BatchWindowing<K> window = new BatchWindowing<>(keySerde,createSerde(valueSerde,PaymentInstructionBatch.class),windowPeriod);
        return TimeWindowingPaymentInstructionBatcher.<K,PK>builder()
                .window(window);
    }

    public <K,PK> TransformerWindowingPaymentInstructionBatcher.TransformerWindowingPaymentInstructionBatcherBuilder<K,PK> countWithTimeout(int size, Duration maxPaymentAge, Duration resolution, boolean useEarliestPaymentInBatch){
        return TransformerWindowingPaymentInstructionBatcher.<K,PK>builder()
                .transformer(() -> new CountWithTimeoutTransformer<>(maxPaymentAge,size,resolution,useEarliestPaymentInBatch));
    }

    public <K,PK> TransformerWindowingPaymentInstructionBatcher.TransformerWindowingPaymentInstructionBatcherBuilder<K,PK> countWithTimeout(int size, Duration maxPaymentAge){
        return countWithTimeout(size, maxPaymentAge, Duration.ofMinutes(1),false);
    }


    public <K,PK> TransformerWindowingPaymentInstructionBatcher.TransformerWindowingPaymentInstructionBatcherBuilder<K,PK> count(int size){
        return TransformerWindowingPaymentInstructionBatcher.<K,PK>builder()
                .transformer(() -> new CountTransformer<>(size));
    }

    public <K,PK> TransformerWindowingPaymentInstructionBatcher.TransformerWindowingPaymentInstructionBatcherBuilder<K,PK> periodic(Duration frequency){
        return TransformerWindowingPaymentInstructionBatcher.<K,PK>builder()
                .transformer(() -> new PeriodicTransformer<>(frequency));
    }

    public <K,PK> TransformerWindowingPaymentInstructionBatcher.TransformerWindowingPaymentInstructionBatcherBuilder<K,PK> periodicWithLimit(int limit, Duration frequency){
        return TransformerWindowingPaymentInstructionBatcher.<K,PK>builder()
                .transformer(() -> new PeriodicWithCountTransformer<>(frequency,limit));
    }

    public <K,PK> TransformerWindowingPaymentInstructionBatcher.TransformerWindowingPaymentInstructionBatcherBuilder<K,PK> scheduled(String cron){
        Trigger trigger = new CronTrigger(cron);
        return TransformerWindowingPaymentInstructionBatcher.<K,PK>builder()
                .transformer(() -> new ScheduledTransformer<>(trigger));
    }

    private <V> Serde<V> createSerde(CompositeMessageConverterFactory valueSerde,Class<V> valueClass) {
        Serde<V> serde = new CompositeNonNativeSerde<>(valueSerde);
        Map<String,Object> map = new HashMap<>();
        map.put("valueClass",valueClass);
        map.put(MessageHeaders.CONTENT_TYPE, "application/json");
        serde.configure(map,false);
        return serde;
    }

}
