package gov.scot.payments.gateway.outbound.spring;

import gov.scot.payments.model.paymentfile.event.PaymentRoutingSuccessEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentSubmitFailedEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentSubmitSuccessEvent;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;

public interface OutboundGatewayInputBinding {

    @Input("payment-events")
    KStream<?, PaymentRoutingSuccessEvent> paymentEvents();

    @Output("payment-submit-failure-events")
    KStream<?, PaymentSubmitFailedEvent> submitFailureEvents();

    @Output("payment-submit-success-events")
    KStream<?, PaymentSubmitSuccessEvent> submitSuccessEvents();

}
