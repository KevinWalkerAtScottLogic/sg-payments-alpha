package gov.scot.payments.gateway.outbound.spring;

import gov.scot.payments.model.paymentinstruction.event.PaymentSubmitFailedEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentSubmitSuccessEvent;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface CoreOutboundGatewayBinding {

    @Output("payment-accepted-events")
    MessageChannel paymentAcceptedEvents();

    @Output("payment-rejected-events")
    MessageChannel paymentRejectedEvents();

    @Output("payment-cleared-events")
    MessageChannel paymentClearedEvents();

    @Output("payment-failedToClear-events")
    MessageChannel paymentFailedToClearEvents();

    @Output("payment-settled-events")
    MessageChannel paymentSettledEvents();
}
