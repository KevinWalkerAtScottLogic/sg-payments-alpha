package gov.scot.payments.gateway.outbound;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import lombok.*;

import java.util.UUID;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class PartitionedPaymentInstructionBatch<K> {

    private K key;
    private PaymentInstructionBatch batch;

    @JsonIgnore
    public UUID getId() {
        return batch.getId();
    }

    @JsonIgnore
    public int size() {
        return batch.size();
    }
}
