package gov.scot.payments.gateway.outbound;

import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;

import java.time.Duration;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class CountTransformer<K> extends BaseTransformer<K> {

    private final int maxCount;

    public CountTransformer(int maxCount) {
        this.maxCount = maxCount;
    }

    @Override
    protected void handleBatch(K key, PaymentInstructionBatch batch, ProcessorContext processorContext, ConcurrentMap<K,PaymentInstructionBatch> currentBatches) {
        if (batch.getPayments().size() >= maxCount) {
            processorContext.forward(key,batch);
            currentBatches.remove(key,batch);
        }
    }
}
