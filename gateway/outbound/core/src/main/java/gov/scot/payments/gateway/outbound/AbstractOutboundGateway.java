package gov.scot.payments.gateway.outbound;

import gov.scot.payments.model.Event;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingSuccessEvent;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import gov.scot.payments.model.paymentinstruction.event.*;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.util.Assert;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.apache.commons.lang.ArrayUtils.isEmpty;

public abstract class AbstractOutboundGateway<PK,CPK> {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    protected abstract CPK convertPartitionKey(PK key);
    protected abstract List<Event<PaymentInstruction>> submitPayments(CPK batchKey, PaymentInstructionBatch batch);
    public abstract String getName();

    private final MessageChannel acceptedEvents;
    private final MessageChannel rejectedEvents;
    private final MessageChannel clearedEvents;
    private final MessageChannel failedToClearEvents;
    private final MessageChannel settledEvents;
    private final PaymentInstructionBatcher[] batchers;

    public AbstractOutboundGateway(MessageChannel acceptedEvents
            , MessageChannel rejectedEvents
            , MessageChannel clearedEvents
            , MessageChannel failedToClearEvents
            , MessageChannel settledEvents
            , PaymentInstructionBatcher... batchers) {
        this.acceptedEvents = acceptedEvents;
        this.rejectedEvents = rejectedEvents;
        this.clearedEvents = clearedEvents;
        this.failedToClearEvents = failedToClearEvents;
        this.settledEvents = settledEvents;
        if(isEmpty(batchers)){
            this.batchers = null;
        } else{
            Assert.noNullElements(batchers,"no null batchers allowed");
            this.batchers = batchers;
        }

    }

    protected KStream<?,?>[] handlePaymentEvents(KStream<?,PaymentRoutingSuccessEvent> events){
        KStream<UUID, PartitionedPaymentInstructionBatch<PK>> mergedBatchStream = createBatches(events);

        Predicate<Object, Event> submitFailed = (k, v) -> PaymentSubmitFailedEvent.class.isAssignableFrom(v.getClass());
        Predicate<Object, Event> submitSucceeded = (k,v) -> PaymentSubmitSuccessEvent.class.isAssignableFrom(v.getClass());

        return mergedBatchStream
                .peek( (k,v) -> log.info("Handling batch {} of {} payments",v.getBatch(),v.size()))
                .flatMap( (k,v) -> submitPayments(convertPartitionKey(v.getKey()),v.getBatch())
                        .stream()
                        .map(e -> new KeyValue<>(null,e))
                        .collect(Collectors.toList()))
                .branch(submitSucceeded, submitFailed);
    }

    KStream<UUID, PartitionedPaymentInstructionBatch<PK>> createBatches(KStream<?, PaymentRoutingSuccessEvent> events) {
        KStream<?, PaymentInstruction> instructions = events
                .mapValues(PaymentRoutingSuccessEvent::getPayload)
                .peek( (k,v) -> log.info("Received payment instruction: {}",v));
        KStream<UUID,PartitionedPaymentInstructionBatch<PK>> mergedBatchStream;
        if(this.batchers != null){
            KStream<?,PaymentInstruction>[] partitionedEvents = instructions.branch(batchers);
            KStream<UUID,PartitionedPaymentInstructionBatch<PK>>[] batchStreams = new KStream[batchers.length];
            for(int i=0;i<batchers.length;i++){
                KStream<?,PaymentInstruction> stream = partitionedEvents[i];
                PaymentInstructionBatcher<?,PK> batcher = batchers[i];
                KStream<UUID,PartitionedPaymentInstructionBatch<PK>> batchStream = batcher.batch(stream);
                batchStreams[i] = batchStream;
            }
            mergedBatchStream = batchStreams[0];
            for(int i=1;i<batchStreams.length;i++){
                mergedBatchStream = mergedBatchStream.merge(batchStreams[i]);
            }
        } else{
            log.info("No batchers provided, defaulting to creating single payment batches");
            mergedBatchStream = instructions.map( (k,v) -> {
                PaymentInstructionBatch batch = PaymentInstructionBatch.builder()
                        .addPayment(v)
                        .build();
                return new KeyValue<>(batch.getId(),new PartitionedPaymentInstructionBatch<>(null,batch));
            });
        }
        return mergedBatchStream;
    }

    protected void sendAccepted(PaymentInstruction payment){
        acceptedEvents.send(new GenericMessage<>(new PaymentAcceptedEvent(payment)));
    }

    protected void sendRejected(PaymentInstruction payment){
        rejectedEvents.send(new GenericMessage<>(new PaymentRejectedEvent(payment)));
    }

    protected void sendCleared(PaymentInstruction payment){
        clearedEvents.send(new GenericMessage<>(new PaymentClearedEvent(payment)));
    }

    protected void sendFailedToClear(PaymentInstruction payment){
        failedToClearEvents.send(new GenericMessage<>(new PaymentFailedToClearEvent(payment)));
    }

    protected void sendSettled(PaymentInstruction payment){
        settledEvents.send(new GenericMessage<>(new PaymentSettledEvent(payment)));
    }

}
