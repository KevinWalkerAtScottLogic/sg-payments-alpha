package gov.scot.payments.gateway.outbound;

import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.processor.Cancellable;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.support.SimpleTriggerContext;

import java.time.Duration;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

public class ScheduledTransformer<K> extends BaseTransformer<K> {

    private final Trigger trigger;
    private Cancellable currentTask;

    public ScheduledTransformer(Trigger trigger) {
        this.trigger = trigger;
    }

    @Override
    public void init(ProcessorContext processorContext) {
        super.init(processorContext);
        if(trigger != null){
            Duration duration = getNextWait(null);
            if(duration != null){
                currentTask = processorContext.schedule(duration, PunctuationType.WALL_CLOCK_TIME, this::doPunctuate);
            }
        }
    }

    private Duration getNextWait(Long timestamp) {
        Date lastDate = timestamp == null ? null : new Date(timestamp);
        TriggerContext context = new SimpleTriggerContext();
        Date nextDate = trigger.nextExecutionTime(context);
        if(nextDate == null){
            return null;
        }
        return Duration.ofMillis(nextDate.toInstant().toEpochMilli() - System.currentTimeMillis());
    }

    private synchronized void doPunctuate(long timestamp) {
        currentTask.cancel();
        Iterator<Map.Entry<K, PaymentInstructionBatch>> iterator = currentBatches.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<K, PaymentInstructionBatch> keyValue = iterator.next();
            processorContext.forward(keyValue.getKey(), keyValue.getValue());
            iterator.remove();
        }
        Duration duration = getNextWait(timestamp);
        if(duration != null){
            currentTask = processorContext.schedule(duration, PunctuationType.WALL_CLOCK_TIME, this::doPunctuate);
        }
    }

}
