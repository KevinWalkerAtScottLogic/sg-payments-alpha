package gov.scot.payments.gateway.outbound;

import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.SessionStore;
import org.apache.kafka.streams.state.WindowStore;

import java.util.function.Function;

public class SessionWindowingPaymentInstructionBatcher<K,PK> extends PaymentInstructionBatcher<K,PK> {

    private final BatchSessionWindowing<K> window;

    public SessionWindowingPaymentInstructionBatcher(Function<PaymentInstruction,PK> branchPredicate
            , PK partitionKey
            , BatchGrouping<K> groupingFunction
            , Function<PaymentInstructionBatch, String> batchNameExtractor
            , BatchSessionWindowing<K> window) {
        super(branchPredicate, partitionKey,groupingFunction, batchNameExtractor);
        this.window = window;
    }

    @Override
    protected WindowedKStream<K> applyWindowing(KGroupedStream<K, PaymentInstruction> stream) {
        SessionWindowedKStream<K, PaymentInstruction> windowed = stream.windowedBy(window.getWindow());
        return (i,a,m) -> windowed.aggregate(i,a,m,(Materialized<K, PaymentInstructionBatch, SessionStore<Bytes, byte[]>>)window.getMaterialized());
    }

    public static <K,PK> SessionWindowingPaymentInstructionBatcherBuilder<K,PK> builder(){
        return new SessionWindowingPaymentInstructionBatcherBuilder<>();
    }

    public static class SessionWindowingPaymentInstructionBatcherBuilder<K,PK>
            extends PaymentInstructionBatcherBuilder<K,PK,SessionWindowingPaymentInstructionBatcher<K,PK>,SessionWindowingPaymentInstructionBatcherBuilder<K,PK>> {

        private BatchSessionWindowing<K> window;

        SessionWindowingPaymentInstructionBatcherBuilder() {
        }

        @Override
        public SessionWindowingPaymentInstructionBatcher<K, PK> build() {
            return new SessionWindowingPaymentInstructionBatcher<>(branchPredicate,partitionKey,groupingFunction,batchNameExtractor,window);
        }

        public SessionWindowingPaymentInstructionBatcherBuilder<K,PK> window(BatchSessionWindowing<K> window) {
            this.window = window;
            return this;
        }
    }

}
