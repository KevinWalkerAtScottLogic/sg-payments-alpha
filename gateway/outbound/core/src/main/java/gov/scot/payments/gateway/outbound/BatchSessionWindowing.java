package gov.scot.payments.gateway.outbound;

import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.SessionWindows;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.Windows;
import org.apache.kafka.streams.kstream.internals.TimeWindow;

import java.time.Duration;

@Value
@EqualsAndHashCode(callSuper = true)
public class BatchSessionWindowing<K> extends BatchOperation<K,PaymentInstructionBatch>{

    private SessionWindows window;
    private Duration inactivity;

    public BatchSessionWindowing(Serde<K> keySerde, Serde<PaymentInstructionBatch> valueSerde, Duration inactivity) {
        super(keySerde, valueSerde);
        this.inactivity = inactivity;
        this.window = SessionWindows.with(inactivity).grace(inactivity);
    }

    public Materialized getMaterialized(){
        return Materialized.with(keySerde, valueSerde)
                .withLoggingDisabled()
                .withCachingDisabled()
                .withRetention(inactivity.plus(inactivity));
    }
}
