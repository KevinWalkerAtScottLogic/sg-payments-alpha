package gov.scot.payments.gateway.outbound;

import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.apache.kafka.common.serialization.Serde;

@Getter
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
public class BatchOperation<K,V> {

    protected final Serde<K> keySerde;
    protected final Serde<V> valueSerde;
}
