package gov.scot.payments.gateway.outbound;

import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.Window;
import org.apache.kafka.streams.kstream.Windows;
import org.apache.kafka.streams.kstream.internals.TimeWindow;
import org.apache.kafka.streams.processor.StateStore;
import org.apache.kafka.streams.state.WindowStore;

import java.time.Duration;

@Value
@EqualsAndHashCode(callSuper = true)
public class BatchWindowing<K> extends BatchOperation<K,PaymentInstructionBatch>{

    private Windows<TimeWindow> window;
    private Duration windowPeriod;

    public BatchWindowing(Serde<K> keySerde, Serde<PaymentInstructionBatch> valueSerde, Duration windowPeriod) {
        super(keySerde, valueSerde);
        this.windowPeriod = windowPeriod;
        this.window = TimeWindows.of(windowPeriod).grace(Duration.ofSeconds(0));
    }

    public Materialized getMaterialized(){
        return Materialized.with(keySerde, valueSerde)
                .withLoggingDisabled()
                .withCachingDisabled()
                .withRetention(windowPeriod.plusSeconds(1));
    }
}
