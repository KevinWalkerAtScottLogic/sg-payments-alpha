package gov.scot.payments.gateway.outbound;

import gov.scot.payments.AbstractKafkaTest;
import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.common.kafka.KafkaNullConfiguration;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingSuccessEvent;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.converter.CompositeMessageConverterFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.messaging.handler.annotation.SendTo;

import java.time.Duration;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@EnableAutoConfiguration(exclude = UserDetailsServiceAutoConfiguration.class)
@EmbeddedKafka(partitions = 1
        ,topics = {"batches","paymentEvents"})
@SpringBootTest(classes = AbstractOutboundGatewayTest.TestConfiguration.class
        , properties = {"payments.environment=test"
        ,"spring.application.name=paymentBatcherTest"
        ,"spring.cloud.stream.bindings.output-batches.destination=batches"
        ,"spring.cloud.stream.kafka.streams.bindings.output-batches.producer.keySerde=org.apache.kafka.common.serialization.Serdes$UUIDSerde"
        ,"spring.cloud.stream.bindings.send-payment-events.destination=paymentEvents"
        ,"spring.cloud.stream.bindings.send-payment-events.binder=kafka"
        ,"spring.cloud.stream.bindings.recieve-payment-events.destination=paymentEvents"
        ,"spring.cloud.stream.bindings.recieve-payment-events.group=paymentBatcherTest"
        ,"spring.cloud.stream.bindings.recieve-payment-events.consumer.application-id=paymentBatcherTest"
        ,"payments.gateway.bacs.maxBatchSize=5"
        ,"payments.gateway.bacs.maxWindowDuration=PT5S"
        ,"payments.gateway.chaps.maxBatchSize=3"
        ,"payments.gateway.chaps.maxWindowDuration=PT5S"
        ,"payments.gateway.fp.maxBatchSize=2"
        ,"payments.gateway.fp.maxWindowDuration=PT5S"
})
@Tag("kafka")
public class AbstractOutboundGatewayTest extends AbstractPaymentBatchTest {

    @Test
    public void testBatching(){
        //fp = 3 = 2 batches
        sendPaymentEvents("file",PaymentChannel.FasterPayments,1,3);
        //bacs = 4 = 1 batch (but 2 groups)
        sendPaymentEvents("file1",PaymentChannel.Bacs,4,2);
        sendPaymentEvents("file2",PaymentChannel.Bacs,6,2);
        //chaps = 3 = 1 batch
        sendPaymentEvents("file",PaymentChannel.Chaps,8,3);
        sleep(Duration.ofSeconds(6));

        Comparator<PaymentInstructionBatch> comparator = Comparator
                .comparing( (PaymentInstructionBatch b)  -> b.getName())
                .thenComparing(b -> b.size())
                .thenComparing(b -> b.getEarliestPayment().orElse(null));
        List<PaymentInstructionBatch> batches = getAndVerifyBatches(5,10)
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
        verifyBatch(batches.get(0),1,3,s -> s.equals("file"),pi -> pi.getPaymentFile().equals("file"), pi -> pi.getRouting().getChannel() == PaymentChannel.FasterPayments);
        verifyBatch(batches.get(1),2,3,s -> s.equals("file"),pi -> pi.getPaymentFile().equals("file"), pi -> pi.getRouting().getChannel() == PaymentChannel.FasterPayments);
        verifyBatch(batches.get(2),3,27,s -> s.equals("file"),pi -> pi.getService().equals("file"), pi -> pi.getRouting().getChannel() == PaymentChannel.Chaps);
        verifyBatch(batches.get(3),2,9,s -> s.equals("file1"),pi -> pi.getService().equals("file1"), pi -> pi.getRouting().getChannel() == PaymentChannel.Bacs);
        verifyBatch(batches.get(4),2,13,s -> s.equals("file2"),pi -> pi.getService().equals("file2"), pi -> pi.getRouting().getChannel() == PaymentChannel.Bacs);


    }

    private List<PaymentInstruction> sendPaymentEvents(String service, PaymentChannel channel, int startingValue, int count){
        List<PaymentInstruction> instructions = createPayments(service, channel, startingValue, count);
        return instructions.stream()
                .map(PaymentRoutingSuccessEvent::new)
                .map(this::sendEvent)
                .map(PaymentRoutingSuccessEvent::getPayload)
                .collect(Collectors.toList());
    }

    @Import({OptionalResponseControllerAdvice.class, KafkaNullConfiguration.class})
    @EnableBinding({ AbstractPaymentBatchTest.TestBinding.class})
    @EnableSpringDataWebSupport
    public static class TestConfiguration extends AbstractKafkaTest.TestConfiguration {

        @Bean
        public TestOutboundGateway testOutboundGateway(Collection<PaymentInstructionBatcher> batchers){
            return new TestOutboundGateway(batchers.toArray(new PaymentInstructionBatcher[0]));
        }

        @Bean
        PaymentInstructionBatcher fpBatcher(@Value("${payments.gateway.fp.maxBatchSize:1}") int maxBatchSize,
                                            @Value("${payments.gateway.fp.maxWindowDuration:PT1M}") String duration,
                                            CompositeMessageConverterFactory compositeNonNativeSerde){
            return PaymentInstructionBatchers
                    .<String,String>periodicWithLimit(maxBatchSize, Duration.parse(duration))
                    .batchNameExtractor(PaymentInstructionBatchers.paymentFileBatchNaming())
                    .branchPredicate(PaymentChannel.FasterPayments.name(),PaymentInstructionBatchers.channelTypePredicate())
                    .groupingFunction(PaymentInstructionBatchers.groupedByPaymentFile(compositeNonNativeSerde))
                    .build();
        }

        @Bean
        PaymentInstructionBatcher bacsBatcher(@Value("${payments.gateway.bacs.maxBatchSize:1}") int maxBatchSize,
                                              @Value("${payments.gateway.bacs.maxWindowDuration:PT1M}") String duration,
                                              CompositeMessageConverterFactory compositeNonNativeSerde){
            return PaymentInstructionBatchers
                    .<String,String>periodicWithLimit(maxBatchSize, Duration.parse(duration))
                    .batchNameExtractor(PaymentInstructionBatchers.paymentFileBatchNaming())
                    .branchPredicate(PaymentChannel.Bacs.name(), PaymentInstructionBatchers.channelTypePredicate())
                    .groupingFunction(PaymentInstructionBatchers.groupedByPaymentFile(compositeNonNativeSerde))
                    .build();
        }

        @Bean
        PaymentInstructionBatcher chapsBatcher(@Value("${payments.gateway.chaps.maxBatchSize:1}") int maxBatchSize,
                                               @Value("${payments.gateway.chaps.maxWindowDuration:PT1M}") String duration,
                                               CompositeMessageConverterFactory compositeNonNativeSerde){
            return PaymentInstructionBatchers
                    .<String,String>periodicWithLimit(maxBatchSize, Duration.parse(duration))
                    .batchNameExtractor(PaymentInstructionBatchers.paymentFileBatchNaming())
                    .branchPredicate(PaymentChannel.Chaps.name(), PaymentInstructionBatchers.channelTypePredicate())
                    .groupingFunction(PaymentInstructionBatchers.groupedByPaymentFile(compositeNonNativeSerde))
                    .build();
        }
    }

    @Slf4j
    private static class TestOutboundGateway extends AbstractOutboundGateway<String,String>{

        public TestOutboundGateway(PaymentInstructionBatcher... batchers) {
            super(null, null, null, null, null, batchers);
        }

        @StreamListener
        @SendTo("output-batches")
        public KStream<UUID, PartitionedPaymentInstructionBatch<String>> wrap(@Input("recieve-payment-events") KStream<?, PaymentRoutingSuccessEvent> inboundPaymentEvents) {
            return createBatches(inboundPaymentEvents)
                    .peek( (k,v) -> log.info("BATCH: ID: {} VALUE: {}",k,v));
        }

        @Override
        protected String convertPartitionKey(String key) {
            return key;
        }

        @Override
        protected List<Event<PaymentInstruction>> submitPayments(String batchKey, PaymentInstructionBatch batch) {
            return null;
        }

        @Override
        public String getName() {
            return "test";
        }
    }
}
