package gov.scot.payments.gateway.outbound;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import gov.scot.payments.AbstractKafkaTest;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingSuccessEvent;
import gov.scot.payments.model.paymentinstruction.*;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.UUIDDeserializer;
import org.apache.kafka.streams.kstream.KStream;
import org.assertj.core.api.Assertions;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Tag("kafka")
public abstract class AbstractPaymentBatchTest extends AbstractKafkaTest {

    @Autowired
    @Qualifier("send-payment-events")
    private MessageChannel paymentEvents;

    @Autowired
    private ObjectMapper objectMapper;


    protected List<PaymentInstructionBatch> getAndVerifyBatches(int size, int totalSize) {
        List<PaymentInstructionBatch> batches = getBatches();
        verifyBatches(batches,size,totalSize);
        return batches;
    }

    protected void verifyBatch(PaymentInstructionBatch batch
            , int size
            , double total
            , Predicate<String> namePredicate
            , Predicate<PaymentInstruction>... paymentPredicates) {
        assertEquals(size,batch.getPayments().size());
        Assertions.assertThat(batch.getName()).matches(namePredicate);
        Stream.of(paymentPredicates).forEach(p -> Assertions.assertThat(batch.getPayments()).allMatch(p));
        assertEquals(total,batch.getPayments().stream().mapToDouble(pi -> pi.getTargetAmount().getNumberStripped().doubleValue()).sum(),0.1);
    }

    protected void verifyBatches(List<PaymentInstructionBatch> batches,int size, int totalSize) {
        assertEquals(size,batches.size());
        List<UUID> paymentIds = batches.stream()
                .flatMap(b -> b.getPayments().stream().map(PaymentInstruction::getId))
                .collect(Collectors.toList());
        assertEquals(totalSize,paymentIds.size());
        Assertions.assertThat(paymentIds).doesNotHaveDuplicates();

    }

    protected PaymentInstruction createPayment(String service, PaymentChannel channel, double amount, String currency){
        return PaymentInstruction.builder()
                .service(service)
                .paymentFile(service)
                .batchId(service)
                .recipient(Recipient.builder().build())
                .validation(Validation.success())
                .routing(Routing.builder()
                        .channel(channel)
                        .build())
                .targetAmount(Money.of(new BigDecimal(amount),currency))
                .build();
    }

    protected <T> T sendEvent(T payment) {
        paymentEvents.send(new GenericMessage<>(payment));
        return payment;
    }

    protected PaymentInstruction sendPayment(PaymentInstruction payment) {
       return sendEvent(payment);
    }

    protected List<PaymentInstruction> sendPayments(List<PaymentInstruction> payment) {
        return payment.stream().map(this::sendPayment).collect(Collectors.toList());
    }

    protected List<PaymentInstruction> sendPayments(String service, PaymentChannel channel, int startingValue, int count) {
        List<PaymentInstruction> instructions = createPayments(service, channel, startingValue, count);
        return sendPayments(instructions);
    }

    protected List<PaymentInstruction> createPayments(String service, PaymentChannel channel, int startingValue, int count) {
        List<PaymentInstruction> instructions = new ArrayList<>();
        for(int i=startingValue;i<startingValue+count;i++){
            instructions.add(createPayment(service,channel,i,"GBP"));
        }
        return instructions;
    }

    protected List<PaymentInstruction> sendPayments(String service, int startingValue, int count) {
        return sendPayments(service, PaymentChannel.Bacs,startingValue, count);
    }

    protected List<PaymentInstructionBatch> getBatches() {
        List<String> batches = getEvents("batches", UUIDDeserializer.class, StringDeserializer.class, Duration.ofSeconds(5));
        return batches
                .stream()
                .map(s -> {
                    try {
                        JsonNode jsonNode = objectMapper.readTree(s);
                        JsonNode batch = jsonNode.get("batch");
                        return objectMapper.convertValue(batch,PaymentInstructionBatch.class);
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                })
                .collect(Collectors.toList());
    }

    public interface TestBinding {

        @Output("send-payment-events")
        MessageChannel sendPaymentEvents();

        @Input("recieve-payment-events")
        KStream<?, PaymentInstruction> recievePaymentEvents();

        @Output("output-batches")
        KStream<UUID, PartitionedPaymentInstructionBatch> outputBatches();
    }
}
