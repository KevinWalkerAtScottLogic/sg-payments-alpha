package gov.scot.payments.gateway.outbound;

import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.common.kafka.KafkaNullConfiguration;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.converter.CompositeMessageConverterFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.kafka.test.context.EmbeddedKafka;

import java.time.Duration;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@EnableAutoConfiguration(exclude = UserDetailsServiceAutoConfiguration.class)
@EmbeddedKafka(partitions = 1
        ,topics = {"batches","paymentEvents"})
@SpringBootTest(classes = CountWindowingPaymentInstructionBatcherTest.TestConfiguration.class
        , properties = {"payments.environment=test"
        ,"spring.application.name=paymentBatcherTest"
        ,"spring.cloud.stream.bindings.output-batches.destination=batches"
        ,"spring.cloud.stream.kafka.streams.bindings.output-batches.producer.keySerde=org.apache.kafka.common.serialization.Serdes$UUIDSerde"
        ,"spring.cloud.stream.bindings.send-payment-events.destination=paymentEvents"
        ,"spring.cloud.stream.bindings.send-payment-events.binder=kafka"
        ,"spring.cloud.stream.bindings.recieve-payment-events.destination=paymentEvents"
        ,"spring.cloud.stream.bindings.recieve-payment-events.group=paymentBatcherTest"
        ,"spring.cloud.stream.bindings.recieve-payment-events.consumer.application-id=paymentBatcherTest"
})
@Tag("kafka")
public class CountWindowingPaymentInstructionBatcherTest extends PaymentInstructionBatcherTest {

    @Test
    public void testBasicBatching(){
        sendPayments("service",1,5);
        sendPayments("service",6,3);
        List<PaymentInstructionBatch> batches = getAndVerifyBatches(2,6);
        verifyBatch(batches.get(0),3,6,s -> s.startsWith("service"),pi -> pi.getService().equals("service"));
        verifyBatch(batches.get(1),3,15,s -> s.startsWith("service"),pi -> pi.getService().equals("service"));
    }

    @Test
    public void testGroupedBatching(){
        sendPayments("service1",1,3);
        sendPayments("service2",6,3);
        sendPayments("service3",9,2);
        List<PaymentInstructionBatch> batches = getAndVerifyBatches(2,6)
                .stream()
                .sorted(Comparator.comparing(PaymentInstructionBatch::getName))
                .collect(Collectors.toList());
        verifyBatch(batches.get(0),3,6,s -> s.startsWith("service1"),pi -> pi.getService().equals("service1"));
        verifyBatch(batches.get(1),3,21,s -> s.startsWith("service2"),pi -> pi.getService().equals("service2"));
    }

    @Import({OptionalResponseControllerAdvice.class, KafkaNullConfiguration.class})
    @EnableBinding({ TestBinding.class})
    @EnableSpringDataWebSupport
    public static class TestConfiguration extends PaymentInstructionBatcherTest.TestConfiguration {

        @Bean
        public PaymentInstructionBatcher batcher(CompositeMessageConverterFactory compositeNonNativeSerde){
            return PaymentInstructionBatchers.<String,Object>count(3)
                    .groupingFunction(PaymentInstructionBatchers.groupedByService(compositeNonNativeSerde))
                    .batchNameExtractor(PaymentInstructionBatchers.serviceBatchNaming())
                    .build();
        }

    }



}
