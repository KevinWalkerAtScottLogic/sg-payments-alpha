package gov.scot.payments.gateway.outbound;

import gov.scot.payments.AbstractKafkaTest;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingSuccessEvent;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.handler.annotation.SendTo;
import java.util.UUID;

public abstract class PaymentInstructionBatcherTest extends AbstractPaymentBatchTest {

    @Autowired
    protected PaymentInstructionBatcherWrapper batcher;


    public static class TestConfiguration extends AbstractKafkaTest.TestConfiguration {

        @Bean
        public PaymentInstructionBatcherWrapper batcherWrapper(PaymentInstructionBatcher batcher){
            return new PaymentInstructionBatcherWrapper(batcher);
        }
    }

    @Slf4j
    private static class PaymentInstructionBatcherWrapper {

        private final PaymentInstructionBatcher batcher;

        public PaymentInstructionBatcherWrapper(PaymentInstructionBatcher batcher) {
            this.batcher = batcher;
        }

        @StreamListener
        @SendTo("output-batches")
        public KStream<UUID, PartitionedPaymentInstructionBatch> wrap(@Input("recieve-payment-events") KStream<?, PaymentInstruction> inboundPaymentEvents) {
            return batcher.batch(inboundPaymentEvents)
                    .peek( (k,v) -> log.info("BATCH: ID: {} VALUE: {}",k,v));
        }
    }
}
