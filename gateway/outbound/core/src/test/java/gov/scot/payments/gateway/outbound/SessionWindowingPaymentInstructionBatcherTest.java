package gov.scot.payments.gateway.outbound;

import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.common.kafka.KafkaNullConfiguration;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import gov.scot.payments.model.paymentinstruction.PaymentInstruction;
import gov.scot.payments.model.paymentinstruction.PaymentInstructionBatch;
import org.apache.kafka.common.serialization.Serdes;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.converter.CompositeMessageConverterFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.kafka.test.context.EmbeddedKafka;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@EnableAutoConfiguration(exclude = UserDetailsServiceAutoConfiguration.class)
@EmbeddedKafka(partitions = 1
        ,topics = {"batches","paymentEvents"})
@SpringBootTest(classes = SessionWindowingPaymentInstructionBatcherTest.TestConfiguration.class
        , properties = {"payments.environment=test"
        ,"spring.application.name=paymentBatcherTest"
        ,"spring.cloud.stream.bindings.output-batches.destination=batches"
        ,"spring.cloud.stream.kafka.streams.bindings.output-batches.producer.keySerde=org.apache.kafka.common.serialization.Serdes$UUIDSerde"
        ,"spring.cloud.stream.kafka.streams.binder.configuration.commit.interval.ms=100"
        ,"spring.cloud.stream.bindings.send-payment-events.destination=paymentEvents"
        ,"spring.cloud.stream.bindings.send-payment-events.binder=kafka"
        ,"spring.cloud.stream.bindings.recieve-payment-events.destination=paymentEvents"
        ,"spring.cloud.stream.bindings.recieve-payment-events.group=paymentBatcherTest"
        ,"spring.cloud.stream.bindings.recieve-payment-events.consumer.application-id=paymentBatcherTest"
})
@Tag("kafka")
public class SessionWindowingPaymentInstructionBatcherTest extends PaymentInstructionBatcherTest {
    
    @Test
    public void testBatching(){
        sendPayments("service1",1,3);
        sendPayments("service2",4,2);
        sendPayments("service3",6,3);
        sleep(Duration.ofSeconds(5));
        sendPayments("service1",1,1);
        sendPayments("service2",1,1);
        sendPayments("service3",1,1);
        sleep(Duration.ofSeconds(5));
        List<PaymentInstructionBatch> batches = getBatches();
        verifyBatches(batches,3,8);
        verifyBatch(batches.get(0),3,6,s -> s.startsWith("service1"),pi -> pi.getService().equals("service1"));
        verifyBatch(batches.get(1),2,9,s -> s.startsWith("service2"),pi -> pi.getService().equals("service2"));
        verifyBatch(batches.get(2),3,21,s -> s.startsWith("service3"),pi -> pi.getService().equals("service3"));
    }

    private List<PaymentInstruction> sendPayments(String service, int startingValue, int count, Duration sleep) {
        List<PaymentInstruction> instructions = new ArrayList<>();
        for(int i=startingValue;i<=startingValue+count;i++){
            instructions.add(sendPayment(createPayment(service, PaymentChannel.Bacs,i,"GBP")));
            sleep(sleep);
        }
        return instructions;
    }

    @Import({OptionalResponseControllerAdvice.class, KafkaNullConfiguration.class})
    @EnableBinding({ TestBinding.class})
    @EnableSpringDataWebSupport
    public static class TestConfiguration extends PaymentInstructionBatcherTest.TestConfiguration {

        @Bean
        public PaymentInstructionBatcher batcher(CompositeMessageConverterFactory compositeNonNativeSerde){
            return PaymentInstructionBatchers.session(Duration.ofSeconds(3), Serdes.String(),compositeNonNativeSerde)
                    .groupingFunction(PaymentInstructionBatchers.groupedByService(compositeNonNativeSerde))
                    .batchNameExtractor(PaymentInstructionBatchers.serviceBatchNaming(b -> ""))
                    .build();
        }

    }



}
