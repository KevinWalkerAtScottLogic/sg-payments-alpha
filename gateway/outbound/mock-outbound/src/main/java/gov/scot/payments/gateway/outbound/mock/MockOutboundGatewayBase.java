package gov.scot.payments.gateway.outbound.mock;

import gov.scot.payments.gateway.outbound.AbstractOutboundGateway;
import gov.scot.payments.gateway.outbound.PaymentInstructionBatcher;
import gov.scot.payments.model.Event;
import gov.scot.payments.model.core.Clearing;
import gov.scot.payments.model.core.ClearingStatus;
import gov.scot.payments.model.core.Settlement;
import gov.scot.payments.model.paymentinstruction.*;
import gov.scot.payments.model.paymentinstruction.event.PaymentSubmitFailedEvent;
import gov.scot.payments.model.paymentinstruction.event.PaymentSubmitSuccessEvent;
import org.springframework.expression.Expression;
import org.springframework.messaging.MessageChannel;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public abstract class MockOutboundGatewayBase<PK> extends AbstractOutboundGateway<PK,PK> {

    private final Expression submitExpression;
    private final Expression acceptExpression;
    private final Expression clearExpression;
    private final ScheduledExecutorService taskExecutor;
    private final String name;

    public MockOutboundGatewayBase(MessageChannel acceptedEvents
            , MessageChannel rejectedEvents
            , MessageChannel clearedEvents
            , MessageChannel failedToClearEvents
            , MessageChannel settledEvents
            , ScheduledExecutorService taskExecutor
            , String name
            , Expression submitExpression
            , Expression acceptExpression
            , Expression clearExpression
            , PaymentInstructionBatcher... batchers) {
        super(acceptedEvents, rejectedEvents, clearedEvents, failedToClearEvents, settledEvents, batchers);
        this.name = name;
        this.taskExecutor = taskExecutor;
        this.submitExpression = submitExpression;
        this.acceptExpression = acceptExpression;
        this.clearExpression = clearExpression;
    }

    @Override
    protected PK convertPartitionKey(PK key) {
        return key;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    protected List<Event<PaymentInstruction>> submitPayments(PK batchKey, PaymentInstructionBatch batch) {
        log.info("Submitting batch: {} of {} payments for key: {}",batch,batch.size(),batchKey);
        return batch.getPayments()
                .stream()
                .map(this::handleSubmit)
                .collect(Collectors.toList());
    }

    private Event<PaymentInstruction> handleSubmit(PaymentInstruction payment) {
        log.info("Submitting payment {}",payment);
        Duration result = submitExpression.getValue(payment,Duration.class);
        LocalDateTime now = LocalDateTime.now();
        Event<PaymentInstruction> event;
        if(!result.isNegative() && !result.isZero()){
            PaymentInstruction submitted = payment.toBuilder()
                    .addExecution(Execution.builder()
                            .executedAt(now)
                            .executedAmount(payment.getTargetAmount())
                            .channel(payment.getRouting().getChannel())
                            .processor(payment.getRouting().getProcessor())
                            .build())
                    .status(PaymentInstructionStatus.Submitted)
                    .timestamp(now)
                    .build();
            log.info("Payment submission succeeded {}",submitted);
            event =  new PaymentSubmitSuccessEvent(submitted);
            taskExecutor.schedule(() -> handleAcceptanceTask(submitted),result.toMillis(), TimeUnit.MILLISECONDS);
        } else{
            PaymentInstruction failed = payment.toBuilder()
                    .status(PaymentInstructionStatus.SubmissionFailed)
                    .addExecution(Execution.builder()
                            .executedAt(now)
                            .executedAmount(payment.getTargetAmount())
                            .channel(payment.getRouting().getChannel())
                            .processor(payment.getRouting().getProcessor())
                            .status(ExecutionStatus.SubmissionFailed)
                            .statusMessage(String.format("Submission failed on %s", getName()))
                            .build())
                    .timestamp(now)
                    .build();
            log.info("Payment submission failed {}",failed);
            event =  new PaymentSubmitFailedEvent(payment);
        }
        return event;
    }

    private void handleAcceptanceTask(PaymentInstruction payment){
        Duration result = acceptExpression.getValue(payment,Duration.class);
        LocalDateTime now = LocalDateTime.now();
        if(!result.isNegative() && !result.isZero()){
            PaymentInstruction accepted = payment.toBuilder()
                    .addExecution(Execution.builder()
                            .executedAt(now)
                            .status(ExecutionStatus.Accepted)
                            .executedAmount(payment.getTargetAmount())
                            .channel(payment.getRouting().getChannel())
                            .processor(payment.getRouting().getProcessor())
                            .build())
                    .status(PaymentInstructionStatus.Accepted)
                    .timestamp(now)
                    .build();
            log.info("Payment accepted {}",accepted);
            sendAccepted(accepted);
            taskExecutor.schedule(() -> handleClearing(accepted),result.toMillis(),TimeUnit.MILLISECONDS);
        } else{
            PaymentInstruction rejected = payment.toBuilder()
                    .addExecution(Execution.builder()
                            .executedAt(now)
                            .executedAmount(payment.getTargetAmount())
                            .channel(payment.getRouting().getChannel())
                            .processor(payment.getRouting().getProcessor())
                            .status(ExecutionStatus.Rejected)
                            .statusMessage(String.format("Payment rejected by %s", getName()))
                            .build())
                    .status(PaymentInstructionStatus.Rejected)
                    .timestamp(now)
                    .build();
            log.info("Payment rejected {}",rejected);
            sendRejected(rejected);
        }
    }

    private void handleClearing(PaymentInstruction payment) {
        Duration result = clearExpression.getValue(payment,Duration.class);
        LocalDateTime now = LocalDateTime.now();
        if(!result.isNegative() && !result.isZero()){
            PaymentInstruction cleared = payment.toBuilder()
                    .clearing(Clearing.builder()
                            .clearedAt(now)
                            .clearedAmount(payment.getTargetAmount())
                            .build())
                    .timestamp(now)
                    .status(PaymentInstructionStatus.Cleared)
                    .build();
            log.info("Payment cleared {}",cleared);
            sendCleared(cleared);
            taskExecutor.schedule(() -> handleSettled(cleared),result.toMillis(),TimeUnit.MILLISECONDS);
        } else{
            PaymentInstruction failedToClear = payment.toBuilder()
                    .clearing(Clearing.builder()
                            .clearedAt(now)
                            .status(ClearingStatus.FailedToClear)
                            .statusMessage(String.format("Payment did not clear via %s", getName()))
                            .build())
                    .timestamp(now)
                    .status(PaymentInstructionStatus.FailedToClear)
                    .build();
            log.info("Payment failed to clear {}",failedToClear);
            sendFailedToClear(failedToClear);
        }
    }

    private void handleSettled(PaymentInstruction payment) {
        LocalDateTime now = LocalDateTime.now();
        PaymentInstruction settled = payment.toBuilder()
                .settlement(Settlement.builder()
                        .settledAt(now)
                        .settledAmount(payment.getClearing().getClearedAmount())
                        .build())
                .status(PaymentInstructionStatus.Settled)
                .timestamp(now)
                .build();
        log.info("payment settled {}",settled);
        sendSettled(settled);
    }

}
