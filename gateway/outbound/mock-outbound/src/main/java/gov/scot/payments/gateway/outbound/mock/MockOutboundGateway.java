package gov.scot.payments.gateway.outbound.mock;

import gov.scot.payments.gateway.outbound.PaymentInstructionBatcher;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingSuccessEvent;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.expression.Expression;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.SendTo;

import java.util.concurrent.ScheduledExecutorService;

public class MockOutboundGateway extends MockOutboundGatewayBase<PaymentChannel> {

    public MockOutboundGateway(MessageChannel acceptedEvents
            , MessageChannel rejectedEvents
            , MessageChannel clearedEvents
            , MessageChannel failedToClearEvents
            , MessageChannel settledEvents
            , ScheduledExecutorService taskExecutor
            , String name
            , Expression submitExpression
            , Expression acceptExpression
            , Expression clearExpression
            , PaymentInstructionBatcher... batchers) {
        super(acceptedEvents
                , rejectedEvents
                , clearedEvents
                , failedToClearEvents
                , settledEvents
                , taskExecutor
                , name
                , submitExpression
                , acceptExpression
                , clearExpression
                , batchers);
    }

    @StreamListener
    @SendTo({"payment-submit-success-events","payment-submit-failure-events"})
    public KStream<?, ?>[] handleGatewayPaymentEvents(@Input("payment-events") KStream<?, PaymentRoutingSuccessEvent> events) {
        return handlePaymentEvents(events);
    }
}
