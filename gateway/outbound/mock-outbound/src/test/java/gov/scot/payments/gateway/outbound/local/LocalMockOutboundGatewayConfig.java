package gov.scot.payments.gateway.outbound.local;

import com.beust.jcommander.DynamicParameter;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParametersDelegate;
import gov.scot.payments.common.local.*;
import gov.scot.payments.model.paymentinstruction.PaymentChannel;
import lombok.Data;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
public class LocalMockOutboundGatewayConfig implements LocalConfig{

    @ParametersDelegate private LocalAwsConfig aws = new LocalAwsConfig();
    @ParametersDelegate private LocalKafkaConfig kafka = new LocalKafkaConfig();
    @ParametersDelegate private LocalZookeeperConfig zookeeper = new LocalZookeeperConfig();
    @ParametersDelegate private LocalDatabaseConfig database = new LocalDatabaseConfig();
    @ParametersDelegate private LocalApplicationConfig app = new LocalApplicationConfig(true);
    @ParametersDelegate private LocalCognitoConfig cognito = new LocalCognitoConfig();

    @Parameter(required = true
            ,description = "The name of the gateway")
    private String name;

    @DynamicParameter(names = "-C"
            , description = "The channels this gateway should support. This option can appear multiple times. Format is Channel=Expression. Allowed Channel values are (Bacs|FasterPayments|Chaps). Expression values must be valid SpEL expressions against the PaymentInstruction type as the root")
    private Map<String, String> channels = new HashMap<>();

    @Override
    public Set<String> getProfiles() {
        Set<String> profiles = new HashSet<>();
        profiles.addAll(kafka.getProfiles());
        profiles.addAll(zookeeper.getProfiles());
        profiles.addAll(database.getProfiles());
        profiles.addAll(app.getProfiles());
        profiles.addAll(cognito.getProfiles());
        profiles.addAll(aws.getProfiles());
        return profiles;
    }

    @Override
    public Map<String, Object> getProperties() {
        Map<String,Object> properties = new HashMap<>();
        properties.put("GATEWAY_NAME",name);
        channels.forEach((key, value) -> properties.put(name + ".channel." + key, value));
        properties.putAll(kafka.getProperties());
        properties.putAll(zookeeper.getProperties());
        properties.putAll(database.getProperties());
        properties.putAll(app.getProperties());
        properties.putAll(cognito.getProperties());
        properties.putAll(aws.getProperties());
        return properties;
    }

    @Override
    public String[] toArgs() {
        return new String[0];
    }

    @Override
    public Set<String> validate() {
        Set<String> violations = new HashSet<>();
        violations.addAll(kafka.validate());
        violations.addAll(zookeeper.validate());
        violations.addAll(database.validate());
        violations.addAll(app.validate());
        violations.addAll(cognito.validate());
        violations.addAll(aws.validate());
        if(cognito.isCognito() && !aws.isAws()){
            violations.add("aws is required if using cognito");
        }
        try{
            channels.keySet().forEach(PaymentChannel::valueOf);
        } catch(Exception e){
            violations.add("channel name must be one of: "+ Stream.of(PaymentChannel.values()).map(PaymentChannel::name).collect(Collectors.joining(",")));
        }
        return violations;
    }
}
