package gov.scot.payments.gateway.outbound;

import gov.scot.payments.AbstractKafkaTest;
import gov.scot.payments.common.OptionalResponseControllerAdvice;
import gov.scot.payments.common.kafka.KafkaNullConfiguration;
import gov.scot.payments.common.processor.PaymentProcessorRepository;
import gov.scot.payments.gateway.outbound.mock.spring.MockOutboundGatewayConfiguration;
import gov.scot.payments.gateway.outbound.spring.CoreOutboundGatewayBinding;
import gov.scot.payments.gateway.outbound.spring.OutboundGatewayInputBinding;
import gov.scot.payments.model.paymentfile.event.PaymentRoutingSuccessEvent;
import gov.scot.payments.model.paymentinstruction.*;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;

@EnableAutoConfiguration(exclude = UserDetailsServiceAutoConfiguration.class)
@EmbeddedKafka(partitions = 1
        ,topics = {"test-bottomlinePaymentEvents"
        ,"test-paymentSubmitSuccessEvents"
        ,"test-paymentSubmitFailedEvents"
        ,"test-paymentAcceptedEvents"
        ,"test-paymentRejectedEvents"
        ,"test-paymentClearedEvents"
        ,"test-paymentFailedToClearEvents"
        ,"test-paymentSettledEvents"})
@SpringBootTest(classes = MockOutboundGatewayIntegrationTest.TestConfiguration.class
        , properties = {"payments.environment=test"
        ,"payments.gateway.name=bottomline"
        ,"spring.cloud.stream.bindings.bottomline-events.destination=test-bottomlinePaymentEvents"
        ,"spring.cloud.stream.bindings.bottomline-events.binder=kafka"
        ,"payments.gateway.submitExpression=T(java.time.Duration).ofSeconds(1)"
        ,"payments.gateway.acceptExpression=T(java.time.Duration).ofSeconds(1)"
        ,"payments.gateway.clearExpression=T(java.time.Duration).ofSeconds(1)"
        ,"spring.cloud.zookeeper.discovery.instance-port=80"
        ,"payments.gateway.bacs.maxBatchSize=1"
        ,"payments.gateway.bacs.maxWindowDuration=PT3S"
        ,"payments.gateway.chaps.maxBatchSize=1"
        ,"payments.gateway.chaps.maxWindowDuration=PT3S"
        ,"payments.gateway.fp.maxBatchSize=1"
        ,"payments.gateway.fp.maxWindowDuration=PT3S"})
@Tag("kafka")
public class MockOutboundGatewayIntegrationTest extends AbstractKafkaTest {

    @Autowired
    @Qualifier("bottomline-events")
    MessageChannel bottomlineEvents;

    @Test
    public void testGateway(){
        PaymentInstruction payment = PaymentInstruction.builder()
                .recipient(Recipient.builder().build())
                .targetChannelType(PaymentChannelType.EFT)
                .latestTargetSettlementDate(LocalDate.now())
                .targetAmount(Money.of(new BigDecimal("50"),"GBP"))
                .service("service1")
                .routing(Routing.builder()
                        .channel(PaymentChannel.Bacs)
                        .processor("bottomline")
                        .queue("test-bottomlinePaymentEvents")
                        .build())
                .build();
        bottomlineEvents.send(new GenericMessage<>(new PaymentRoutingSuccessEvent(payment)));
        sleep(Duration.ofSeconds(5));
        verifyEvents("test-paymentSubmitSuccessEvents",l -> Assertions.assertEquals(1,l.size()));
        verifyEvents("test-paymentAcceptedEvents",l -> Assertions.assertEquals(1,l.size()));
        verifyEvents("test-paymentClearedEvents",l -> Assertions.assertEquals(1,l.size()));
        verifyEvents("test-paymentSettledEvents",l -> Assertions.assertEquals(1,l.size()));
        verifyEvents("test-paymentSubmitFailedEvents",l -> Assertions.assertEquals(0,l.size()));
        verifyEvents("test-paymentRejectedEvents",l -> Assertions.assertEquals(0,l.size()));
        verifyEvents("test-paymentFailedToClearEvents",l -> Assertions.assertEquals(0,l.size()));
    }

    @Import({OptionalResponseControllerAdvice.class, KafkaNullConfiguration.class, MockOutboundGatewayConfiguration.class})
    @EnableBinding({ CoreOutboundGatewayBinding.class, OutboundGatewayInputBinding.class, TestBinding.class})
    @EnableSpringDataWebSupport
    @EntityScan(basePackageClasses = PaymentProcessor.class)
    @EnableJpaRepositories(basePackageClasses = PaymentProcessorRepository.class)
    public static class TestConfiguration extends AbstractKafkaTest.TestConfiguration {


    }

    public interface TestBinding {

        @Output("bottomline-events")
        MessageChannel bottomlineEvents();

    }
}
